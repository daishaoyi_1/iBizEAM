package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_O;
import cn.ibizlab.eam.webapi.dto.PLANSCHEDULE_ODTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiPLANSCHEDULE_OMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PLANSCHEDULE_OMapping extends MappingBase<PLANSCHEDULE_ODTO, PLANSCHEDULE_O> {


}

