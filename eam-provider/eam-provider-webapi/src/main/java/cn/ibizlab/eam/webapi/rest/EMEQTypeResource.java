package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQType;
import cn.ibizlab.eam.core.eam_core.service.IEMEQTypeService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQTypeSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备类型" })
@RestController("WebApi-emeqtype")
@RequestMapping("")
public class EMEQTypeResource {

    @Autowired
    public IEMEQTypeService emeqtypeService;

    @Autowired
    @Lazy
    public EMEQTypeMapping emeqtypeMapping;

    @PreAuthorize("hasPermission(this.emeqtypeMapping.toDomain(#emeqtypedto),'eam-EMEQType-Create')")
    @ApiOperation(value = "新建设备类型", tags = {"设备类型" },  notes = "新建设备类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqtypes")
    public ResponseEntity<EMEQTypeDTO> create(@Validated @RequestBody EMEQTypeDTO emeqtypedto) {
        EMEQType domain = emeqtypeMapping.toDomain(emeqtypedto);
		emeqtypeService.create(domain);
        EMEQTypeDTO dto = emeqtypeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqtypeMapping.toDomain(#emeqtypedtos),'eam-EMEQType-Create')")
    @ApiOperation(value = "批量新建设备类型", tags = {"设备类型" },  notes = "批量新建设备类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqtypes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQTypeDTO> emeqtypedtos) {
        emeqtypeService.createBatch(emeqtypeMapping.toDomain(emeqtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqtype" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqtypeService.get(#emeqtype_id),'eam-EMEQType-Update')")
    @ApiOperation(value = "更新设备类型", tags = {"设备类型" },  notes = "更新设备类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqtypes/{emeqtype_id}")
    public ResponseEntity<EMEQTypeDTO> update(@PathVariable("emeqtype_id") String emeqtype_id, @RequestBody EMEQTypeDTO emeqtypedto) {
		EMEQType domain  = emeqtypeMapping.toDomain(emeqtypedto);
        domain .setEmeqtypeid(emeqtype_id);
		emeqtypeService.update(domain );
		EMEQTypeDTO dto = emeqtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqtypeService.getEmeqtypeByEntities(this.emeqtypeMapping.toDomain(#emeqtypedtos)),'eam-EMEQType-Update')")
    @ApiOperation(value = "批量更新设备类型", tags = {"设备类型" },  notes = "批量更新设备类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqtypes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQTypeDTO> emeqtypedtos) {
        emeqtypeService.updateBatch(emeqtypeMapping.toDomain(emeqtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqtypeService.get(#emeqtype_id),'eam-EMEQType-Remove')")
    @ApiOperation(value = "删除设备类型", tags = {"设备类型" },  notes = "删除设备类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqtypes/{emeqtype_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqtype_id") String emeqtype_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqtypeService.remove(emeqtype_id));
    }

    @PreAuthorize("hasPermission(this.emeqtypeService.getEmeqtypeByIds(#ids),'eam-EMEQType-Remove')")
    @ApiOperation(value = "批量删除设备类型", tags = {"设备类型" },  notes = "批量删除设备类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqtypes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqtypeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqtypeMapping.toDomain(returnObject.body),'eam-EMEQType-Get')")
    @ApiOperation(value = "获取设备类型", tags = {"设备类型" },  notes = "获取设备类型")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqtypes/{emeqtype_id}")
    public ResponseEntity<EMEQTypeDTO> get(@PathVariable("emeqtype_id") String emeqtype_id) {
        EMEQType domain = emeqtypeService.get(emeqtype_id);
        EMEQTypeDTO dto = emeqtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备类型草稿", tags = {"设备类型" },  notes = "获取设备类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqtypes/getdraft")
    public ResponseEntity<EMEQTypeDTO> getDraft(EMEQTypeDTO dto) {
        EMEQType domain = emeqtypeMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqtypeMapping.toDto(emeqtypeService.getDraft(domain)));
    }

    @ApiOperation(value = "检查设备类型", tags = {"设备类型" },  notes = "检查设备类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqtypes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQTypeDTO emeqtypedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqtypeService.checkKey(emeqtypeMapping.toDomain(emeqtypedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-GenId-all')")
    @ApiOperation(value = "自动生成id", tags = {"设备类型" },  notes = "自动生成id")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqtypes/{emeqtype_id}/genid")
    public ResponseEntity<EMEQTypeDTO> genId(@PathVariable("emeqtype_id") String emeqtype_id, @RequestBody EMEQTypeDTO emeqtypedto) {
        EMEQType domain = emeqtypeMapping.toDomain(emeqtypedto);
        domain.setEmeqtypeid(emeqtype_id);
        domain = emeqtypeService.genId(domain);
        emeqtypedto = emeqtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqtypedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-GenId-all')")
    @ApiOperation(value = "批量处理[自动生成id]", tags = {"设备类型" },  notes = "批量处理[自动生成id]")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqtypes/genidbatch")
    public ResponseEntity<Boolean> genIdBatch(@RequestBody List<EMEQTypeDTO> emeqtypedtos) {
        List<EMEQType> domains = emeqtypeMapping.toDomain(emeqtypedtos);
        boolean result = emeqtypeService.genIdBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasPermission(this.emeqtypeMapping.toDomain(#emeqtypedto),'eam-EMEQType-Save')")
    @ApiOperation(value = "保存设备类型", tags = {"设备类型" },  notes = "保存设备类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqtypes/save")
    public ResponseEntity<EMEQTypeDTO> save(@RequestBody EMEQTypeDTO emeqtypedto) {
        EMEQType domain = emeqtypeMapping.toDomain(emeqtypedto);
        emeqtypeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqtypeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqtypeMapping.toDomain(#emeqtypedtos),'eam-EMEQType-Save')")
    @ApiOperation(value = "批量保存设备类型", tags = {"设备类型" },  notes = "批量保存设备类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqtypes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQTypeDTO> emeqtypedtos) {
        emeqtypeService.saveBatch(emeqtypeMapping.toDomain(emeqtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-searchDefault-all') and hasPermission(#context,'eam-EMEQType-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备类型" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqtypes/fetchdefault")
	public ResponseEntity<List<EMEQTypeDTO>> fetchDefault(EMEQTypeSearchContext context) {
        Page<EMEQType> domains = emeqtypeService.searchDefault(context) ;
        List<EMEQTypeDTO> list = emeqtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-searchDefault-all') and hasPermission(#context,'eam-EMEQType-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备类型" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqtypes/searchdefault")
	public ResponseEntity<Page<EMEQTypeDTO>> searchDefault(@RequestBody EMEQTypeSearchContext context) {
        Page<EMEQType> domains = emeqtypeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-searchEQTypeTree-all') and hasPermission(#context,'eam-EMEQType-Get')")
	@ApiOperation(value = "获取EQTypeTree", tags = {"设备类型" } ,notes = "获取EQTypeTree")
    @RequestMapping(method= RequestMethod.GET , value="/emeqtypes/fetcheqtypetree")
	public ResponseEntity<List<EMEQTypeDTO>> fetchEQTypeTree(EMEQTypeSearchContext context) {
        Page<EMEQType> domains = emeqtypeService.searchEQTypeTree(context) ;
        List<EMEQTypeDTO> list = emeqtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-searchEQTypeTree-all') and hasPermission(#context,'eam-EMEQType-Get')")
	@ApiOperation(value = "查询EQTypeTree", tags = {"设备类型" } ,notes = "查询EQTypeTree")
    @RequestMapping(method= RequestMethod.POST , value="/emeqtypes/searcheqtypetree")
	public ResponseEntity<Page<EMEQTypeDTO>> searchEQTypeTree(@RequestBody EMEQTypeSearchContext context) {
        Page<EMEQType> domains = emeqtypeService.searchEQTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-searchPeqType-all') and hasPermission(#context,'eam-EMEQType-Get')")
	@ApiOperation(value = "获取上级设备类型", tags = {"设备类型" } ,notes = "获取上级设备类型")
    @RequestMapping(method= RequestMethod.GET , value="/emeqtypes/fetchpeqtype")
	public ResponseEntity<List<EMEQTypeDTO>> fetchPeqType(EMEQTypeSearchContext context) {
        Page<EMEQType> domains = emeqtypeService.searchPeqType(context) ;
        List<EMEQTypeDTO> list = emeqtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQType-searchPeqType-all') and hasPermission(#context,'eam-EMEQType-Get')")
	@ApiOperation(value = "查询上级设备类型", tags = {"设备类型" } ,notes = "查询上级设备类型")
    @RequestMapping(method= RequestMethod.POST , value="/emeqtypes/searchpeqtype")
	public ResponseEntity<Page<EMEQTypeDTO>> searchPeqType(@RequestBody EMEQTypeSearchContext context) {
        Page<EMEQType> domains = emeqtypeService.searchPeqType(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

