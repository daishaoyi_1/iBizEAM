package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_DP;
import cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_DPSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"点检工单" })
@RestController("WebApi-emwo_dp")
@RequestMapping("")
public class EMWO_DPResource {

    @Autowired
    public IEMWO_DPService emwo_dpService;

    @Autowired
    @Lazy
    public EMWO_DPMapping emwo_dpMapping;

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "新建点检工单", tags = {"点检工单" },  notes = "新建点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps")
    public ResponseEntity<EMWO_DPDTO> create(@Validated @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
		emwo_dpService.create(domain);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "批量新建点检工单", tags = {"点检工单" },  notes = "批量新建点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        emwo_dpService.createBatch(emwo_dpMapping.toDomain(emwo_dpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_dp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "更新点检工单", tags = {"点检工单" },  notes = "更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> update(@PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
		EMWO_DP domain  = emwo_dpMapping.toDomain(emwo_dpdto);
        domain .setEmwoDpid(emwo_dp_id);
		emwo_dpService.update(domain );
		EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByEntities(this.emwo_dpMapping.toDomain(#emwo_dpdtos)),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "批量更新点检工单", tags = {"点检工单" },  notes = "批量更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_dps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        emwo_dpService.updateBatch(emwo_dpMapping.toDomain(emwo_dpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "删除点检工单", tags = {"点检工单" },  notes = "删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwo_dp_id") String emwo_dp_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.remove(emwo_dp_id));
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByIds(#ids),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "批量删除点检工单", tags = {"点检工单" },  notes = "批量删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_dps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwo_dpService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_dpMapping.toDomain(returnObject.body),'eam-EMWO_DP-Get')")
    @ApiOperation(value = "获取点检工单", tags = {"点检工单" },  notes = "获取点检工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> get(@PathVariable("emwo_dp_id") String emwo_dp_id) {
        EMWO_DP domain = emwo_dpService.get(emwo_dp_id);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取点检工单草稿", tags = {"点检工单" },  notes = "获取点检工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_dps/getdraft")
    public ResponseEntity<EMWO_DPDTO> getDraft(EMWO_DPDTO dto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(emwo_dpService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Acceptance-all')")
    @ApiOperation(value = "验收通过", tags = {"点检工单" },  notes = "验收通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/{emwo_dp_id}/acceptance")
    public ResponseEntity<EMWO_DPDTO> acceptance(@PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEmwoDpid(emwo_dp_id);
        domain = emwo_dpService.acceptance(domain);
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Acceptance-all')")
    @ApiOperation(value = "批量处理[验收通过]", tags = {"点检工单" },  notes = "批量处理[验收通过]")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceBatch(@RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domains = emwo_dpMapping.toDomain(emwo_dpdtos);
        boolean result = emwo_dpService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @ApiOperation(value = "检查点检工单", tags = {"点检工单" },  notes = "检查点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWO_DPDTO emwo_dpdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.checkKey(emwo_dpMapping.toDomain(emwo_dpdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-FormUpdateByEmquipId-all')")
    @ApiOperation(value = "表单更新", tags = {"点检工单" },  notes = "表单更新")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_dps/{emwo_dp_id}/formupdatebyemquipid")
    public ResponseEntity<EMWO_DPDTO> formUpdateByEmquipId(@PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEmwoDpid(emwo_dp_id);
        domain = emwo_dpService.formUpdateByEmquipId(domain);
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "保存点检工单", tags = {"点检工单" },  notes = "保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/save")
    public ResponseEntity<EMWO_DPDTO> save(@RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        emwo_dpService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "批量保存点检工单", tags = {"点检工单" },  notes = "批量保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        emwo_dpService.saveBatch(emwo_dpMapping.toDomain(emwo_dpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Submit-all')")
    @ApiOperation(value = "提交", tags = {"点检工单" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/{emwo_dp_id}/submit")
    public ResponseEntity<EMWO_DPDTO> submit(@PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEmwoDpid(emwo_dp_id);
        domain = emwo_dpService.submit(domain);
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-UnAcceptance-all')")
    @ApiOperation(value = "验收不通过", tags = {"点检工单" },  notes = "验收不通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_dps/{emwo_dp_id}/unacceptance")
    public ResponseEntity<EMWO_DPDTO> unAcceptance(@PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEmwoDpid(emwo_dp_id);
        domain = emwo_dpService.unAcceptance(domain);
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "获取日历", tags = {"点检工单" } ,notes = "获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_dps/fetchcalendar")
	public ResponseEntity<List<EMWO_DPDTO>> fetchCalendar(EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "查询日历", tags = {"点检工单" } ,notes = "查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_dps/searchcalendar")
	public ResponseEntity<Page<EMWO_DPDTO>> searchCalendar(@RequestBody EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "获取已完成", tags = {"点检工单" } ,notes = "获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_dps/fetchconfirmed")
	public ResponseEntity<List<EMWO_DPDTO>> fetchConfirmed(EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "查询已完成", tags = {"点检工单" } ,notes = "查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_dps/searchconfirmed")
	public ResponseEntity<Page<EMWO_DPDTO>> searchConfirmed(@RequestBody EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"点检工单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_dps/fetchdefault")
	public ResponseEntity<List<EMWO_DPDTO>> fetchDefault(EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"点检工单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_dps/searchdefault")
	public ResponseEntity<Page<EMWO_DPDTO>> searchDefault(@RequestBody EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "获取未提交", tags = {"点检工单" } ,notes = "获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_dps/fetchdraft")
	public ResponseEntity<List<EMWO_DPDTO>> fetchDraft(EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "查询未提交", tags = {"点检工单" } ,notes = "查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_dps/searchdraft")
	public ResponseEntity<Page<EMWO_DPDTO>> searchDraft(@RequestBody EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "获取执行中", tags = {"点检工单" } ,notes = "获取执行中")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_dps/fetchtoconfirm")
	public ResponseEntity<List<EMWO_DPDTO>> fetchToConfirm(EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "查询执行中", tags = {"点检工单" } ,notes = "查询执行中")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_dps/searchtoconfirm")
	public ResponseEntity<Page<EMWO_DPDTO>> searchToConfirm(@RequestBody EMWO_DPSearchContext context) {
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "根据设备档案建立点检工单", tags = {"点检工单" },  notes = "根据设备档案建立点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps")
    public ResponseEntity<EMWO_DPDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
		emwo_dpService.create(domain);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "根据设备档案批量建立点检工单", tags = {"点检工单" },  notes = "根据设备档案批量建立点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_dpService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_dp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "根据设备档案更新点检工单", tags = {"点检工单" },  notes = "根据设备档案更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoDpid(emwo_dp_id);
		emwo_dpService.update(domain);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByEntities(this.emwo_dpMapping.toDomain(#emwo_dpdtos)),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "根据设备档案批量更新点检工单", tags = {"点检工单" },  notes = "根据设备档案批量更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_dpService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "根据设备档案删除点检工单", tags = {"点检工单" },  notes = "根据设备档案删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.remove(emwo_dp_id));
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByIds(#ids),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "根据设备档案批量删除点检工单", tags = {"点检工单" },  notes = "根据设备档案批量删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emwo_dpService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_dpMapping.toDomain(returnObject.body),'eam-EMWO_DP-Get')")
    @ApiOperation(value = "根据设备档案获取点检工单", tags = {"点检工单" },  notes = "根据设备档案获取点检工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id) {
        EMWO_DP domain = emwo_dpService.get(emwo_dp_id);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取点检工单草稿", tags = {"点检工单" },  notes = "根据设备档案获取点检工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_dps/getdraft")
    public ResponseEntity<EMWO_DPDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMWO_DPDTO dto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(emwo_dpService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Acceptance-all')")
    @ApiOperation(value = "根据设备档案点检工单", tags = {"点检工单" },  notes = "根据设备档案点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/acceptance")
    public ResponseEntity<EMWO_DPDTO> acceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.acceptance(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @ApiOperation(value = "批量处理[根据设备档案点检工单]", tags = {"点检工单" },  notes = "批量处理[根据设备档案点检工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domains = emwo_dpMapping.toDomain(emwo_dpdtos);
        boolean result = emwo_dpService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @ApiOperation(value = "根据设备档案检查点检工单", tags = {"点检工单" },  notes = "根据设备档案检查点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.checkKey(emwo_dpMapping.toDomain(emwo_dpdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-FormUpdateByEmquipId-all')")
    @ApiOperation(value = "根据设备档案点检工单", tags = {"点检工单" },  notes = "根据设备档案点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/formupdatebyemquipid")
    public ResponseEntity<EMWO_DPDTO> formUpdateByEmquipIdByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.formUpdateByEmquipId(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "根据设备档案保存点检工单", tags = {"点检工单" },  notes = "根据设备档案保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/save")
    public ResponseEntity<EMWO_DPDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        emwo_dpService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "根据设备档案批量保存点检工单", tags = {"点检工单" },  notes = "根据设备档案批量保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_dpService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Submit-all')")
    @ApiOperation(value = "根据设备档案点检工单", tags = {"点检工单" },  notes = "根据设备档案点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/submit")
    public ResponseEntity<EMWO_DPDTO> submitByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.submit(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-UnAcceptance-all')")
    @ApiOperation(value = "根据设备档案点检工单", tags = {"点检工单" },  notes = "根据设备档案点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/unacceptance")
    public ResponseEntity<EMWO_DPDTO> unAcceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.unAcceptance(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案获取日历", tags = {"点检工单" } ,notes = "根据设备档案获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_dps/fetchcalendar")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案查询日历", tags = {"点检工单" } ,notes = "根据设备档案查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_dps/searchcalendar")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案获取已完成", tags = {"点检工单" } ,notes = "根据设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_dps/fetchconfirmed")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案查询已完成", tags = {"点检工单" } ,notes = "根据设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_dps/searchconfirmed")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"点检工单" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_dps/fetchdefault")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"点检工单" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_dps/searchdefault")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案获取未提交", tags = {"点检工单" } ,notes = "根据设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_dps/fetchdraft")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPDraftByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案查询未提交", tags = {"点检工单" } ,notes = "根据设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_dps/searchdraft")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案获取执行中", tags = {"点检工单" } ,notes = "根据设备档案获取执行中")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_dps/fetchtoconfirm")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据设备档案查询执行中", tags = {"点检工单" } ,notes = "根据设备档案查询执行中")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_dps/searchtoconfirm")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "根据班组建立点检工单", tags = {"点检工单" },  notes = "根据班组建立点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps")
    public ResponseEntity<EMWO_DPDTO> createByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setRteamid(pfteam_id);
		emwo_dpService.create(domain);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "根据班组批量建立点检工单", tags = {"点检工单" },  notes = "根据班组批量建立点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> createBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emwo_dpService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_dp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "根据班组更新点检工单", tags = {"点检工单" },  notes = "根据班组更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> updateByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setRteamid(pfteam_id);
        domain.setEmwoDpid(emwo_dp_id);
		emwo_dpService.update(domain);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByEntities(this.emwo_dpMapping.toDomain(#emwo_dpdtos)),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "根据班组批量更新点检工单", tags = {"点检工单" },  notes = "根据班组批量更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emwo_dpService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "根据班组删除点检工单", tags = {"点检工单" },  notes = "根据班组删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<Boolean> removeByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emwo_dp_id") String emwo_dp_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.remove(emwo_dp_id));
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByIds(#ids),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "根据班组批量删除点检工单", tags = {"点检工单" },  notes = "根据班组批量删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeam(@RequestBody List<String> ids) {
        emwo_dpService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_dpMapping.toDomain(returnObject.body),'eam-EMWO_DP-Get')")
    @ApiOperation(value = "根据班组获取点检工单", tags = {"点检工单" },  notes = "根据班组获取点检工单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> getByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emwo_dp_id") String emwo_dp_id) {
        EMWO_DP domain = emwo_dpService.get(emwo_dp_id);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组获取点检工单草稿", tags = {"点检工单" },  notes = "根据班组获取点检工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emwo_dps/getdraft")
    public ResponseEntity<EMWO_DPDTO> getDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id, EMWO_DPDTO dto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(dto);
        domain.setRteamid(pfteam_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(emwo_dpService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Acceptance-all')")
    @ApiOperation(value = "根据班组点检工单", tags = {"点检工单" },  notes = "根据班组点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/{emwo_dp_id}/acceptance")
    public ResponseEntity<EMWO_DPDTO> acceptanceByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setRteamid(pfteam_id);
        domain = emwo_dpService.acceptance(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @ApiOperation(value = "批量处理[根据班组点检工单]", tags = {"点检工单" },  notes = "批量处理[根据班组点检工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domains = emwo_dpMapping.toDomain(emwo_dpdtos);
        boolean result = emwo_dpService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @ApiOperation(value = "根据班组检查点检工单", tags = {"点检工单" },  notes = "根据班组检查点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.checkKey(emwo_dpMapping.toDomain(emwo_dpdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-FormUpdateByEmquipId-all')")
    @ApiOperation(value = "根据班组点检工单", tags = {"点检工单" },  notes = "根据班组点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emwo_dps/{emwo_dp_id}/formupdatebyemquipid")
    public ResponseEntity<EMWO_DPDTO> formUpdateByEmquipIdByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setRteamid(pfteam_id);
        domain = emwo_dpService.formUpdateByEmquipId(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "根据班组保存点检工单", tags = {"点检工单" },  notes = "根据班组保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/save")
    public ResponseEntity<EMWO_DPDTO> saveByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setRteamid(pfteam_id);
        emwo_dpService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "根据班组批量保存点检工单", tags = {"点检工单" },  notes = "根据班组批量保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
             domain.setRteamid(pfteam_id);
        }
        emwo_dpService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Submit-all')")
    @ApiOperation(value = "根据班组点检工单", tags = {"点检工单" },  notes = "根据班组点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/{emwo_dp_id}/submit")
    public ResponseEntity<EMWO_DPDTO> submitByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setRteamid(pfteam_id);
        domain = emwo_dpService.submit(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-UnAcceptance-all')")
    @ApiOperation(value = "根据班组点检工单", tags = {"点检工单" },  notes = "根据班组点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emwo_dps/{emwo_dp_id}/unacceptance")
    public ResponseEntity<EMWO_DPDTO> unAcceptanceByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setRteamid(pfteam_id);
        domain = emwo_dpService.unAcceptance(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组获取日历", tags = {"点检工单" } ,notes = "根据班组获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emwo_dps/fetchcalendar")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPCalendarByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组查询日历", tags = {"点检工单" } ,notes = "根据班组查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emwo_dps/searchcalendar")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPCalendarByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组获取已完成", tags = {"点检工单" } ,notes = "根据班组获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emwo_dps/fetchconfirmed")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPConfirmedByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组查询已完成", tags = {"点检工单" } ,notes = "根据班组查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emwo_dps/searchconfirmed")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPConfirmedByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组获取DEFAULT", tags = {"点检工单" } ,notes = "根据班组获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emwo_dps/fetchdefault")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组查询DEFAULT", tags = {"点检工单" } ,notes = "根据班组查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emwo_dps/searchdefault")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组获取未提交", tags = {"点检工单" } ,notes = "根据班组获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emwo_dps/fetchdraft")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组查询未提交", tags = {"点检工单" } ,notes = "根据班组查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emwo_dps/searchdraft")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组获取执行中", tags = {"点检工单" } ,notes = "根据班组获取执行中")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emwo_dps/fetchtoconfirm")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPToConfirmByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组查询执行中", tags = {"点检工单" } ,notes = "根据班组查询执行中")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emwo_dps/searchtoconfirm")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPToConfirmByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "根据班组设备档案建立点检工单", tags = {"点检工单" },  notes = "根据班组设备档案建立点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps")
    public ResponseEntity<EMWO_DPDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
		emwo_dpService.create(domain);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立点检工单", tags = {"点检工单" },  notes = "根据班组设备档案批量建立点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_dpService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_dp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "根据班组设备档案更新点检工单", tags = {"点检工单" },  notes = "根据班组设备档案更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoDpid(emwo_dp_id);
		emwo_dpService.update(domain);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByEntities(this.emwo_dpMapping.toDomain(#emwo_dpdtos)),'eam-EMWO_DP-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新点检工单", tags = {"点检工单" },  notes = "根据班组设备档案批量更新点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_dpService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.get(#emwo_dp_id),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "根据班组设备档案删除点检工单", tags = {"点检工单" },  notes = "根据班组设备档案删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.remove(emwo_dp_id));
    }

    @PreAuthorize("hasPermission(this.emwo_dpService.getEmwoDpByIds(#ids),'eam-EMWO_DP-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除点检工单", tags = {"点检工单" },  notes = "根据班组设备档案批量删除点检工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emwo_dpService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_dpMapping.toDomain(returnObject.body),'eam-EMWO_DP-Get')")
    @ApiOperation(value = "根据班组设备档案获取点检工单", tags = {"点检工单" },  notes = "根据班组设备档案获取点检工单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}")
    public ResponseEntity<EMWO_DPDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id) {
        EMWO_DP domain = emwo_dpService.get(emwo_dp_id);
        EMWO_DPDTO dto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取点检工单草稿", tags = {"点检工单" },  notes = "根据班组设备档案获取点检工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/getdraft")
    public ResponseEntity<EMWO_DPDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMWO_DPDTO dto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(emwo_dpService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Acceptance-all')")
    @ApiOperation(value = "根据班组设备档案点检工单", tags = {"点检工单" },  notes = "根据班组设备档案点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/acceptance")
    public ResponseEntity<EMWO_DPDTO> acceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.acceptance(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @ApiOperation(value = "批量处理[根据班组设备档案点检工单]", tags = {"点检工单" },  notes = "批量处理[根据班组设备档案点检工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domains = emwo_dpMapping.toDomain(emwo_dpdtos);
        boolean result = emwo_dpService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @ApiOperation(value = "根据班组设备档案检查点检工单", tags = {"点检工单" },  notes = "根据班组设备档案检查点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_dpService.checkKey(emwo_dpMapping.toDomain(emwo_dpdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-FormUpdateByEmquipId-all')")
    @ApiOperation(value = "根据班组设备档案点检工单", tags = {"点检工单" },  notes = "根据班组设备档案点检工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/formupdatebyemquipid")
    public ResponseEntity<EMWO_DPDTO> formUpdateByEmquipIdByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.formUpdateByEmquipId(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdto),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "根据班组设备档案保存点检工单", tags = {"点检工单" },  notes = "根据班组设备档案保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/save")
    public ResponseEntity<EMWO_DPDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        emwo_dpService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_dpMapping.toDomain(#emwo_dpdtos),'eam-EMWO_DP-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存点检工单", tags = {"点检工单" },  notes = "根据班组设备档案批量保存点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_DPDTO> emwo_dpdtos) {
        List<EMWO_DP> domainlist=emwo_dpMapping.toDomain(emwo_dpdtos);
        for(EMWO_DP domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_dpService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-Submit-all')")
    @ApiOperation(value = "根据班组设备档案点检工单", tags = {"点检工单" },  notes = "根据班组设备档案点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/submit")
    public ResponseEntity<EMWO_DPDTO> submitByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.submit(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-UnAcceptance-all')")
    @ApiOperation(value = "根据班组设备档案点检工单", tags = {"点检工单" },  notes = "根据班组设备档案点检工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/{emwo_dp_id}/unacceptance")
    public ResponseEntity<EMWO_DPDTO> unAcceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_dp_id") String emwo_dp_id, @RequestBody EMWO_DPDTO emwo_dpdto) {
        EMWO_DP domain = emwo_dpMapping.toDomain(emwo_dpdto);
        domain.setEquipid(emequip_id);
        domain = emwo_dpService.unAcceptance(domain) ;
        emwo_dpdto = emwo_dpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_dpdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历", tags = {"点检工单" } ,notes = "根据班组设备档案获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/fetchcalendar")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchCalendar-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历", tags = {"点检工单" } ,notes = "根据班组设备档案查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/searchcalendar")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案获取已完成", tags = {"点检工单" } ,notes = "根据班组设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/fetchconfirmed")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案查询已完成", tags = {"点检工单" } ,notes = "根据班组设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/searchconfirmed")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"点检工单" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/fetchdefault")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDefault-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"点检工单" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/searchdefault")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案获取未提交", tags = {"点检工单" } ,notes = "根据班组设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/fetchdraft")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchDraft-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案查询未提交", tags = {"点检工单" } ,notes = "根据班组设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/searchdraft")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案获取执行中", tags = {"点检工单" } ,notes = "根据班组设备档案获取执行中")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/fetchtoconfirm")
	public ResponseEntity<List<EMWO_DPDTO>> fetchEMWO_DPToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
        List<EMWO_DPDTO> list = emwo_dpMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_DP-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_DP-Get')")
	@ApiOperation(value = "根据班组设备档案查询执行中", tags = {"点检工单" } ,notes = "根据班组设备档案查询执行中")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_dps/searchtoconfirm")
	public ResponseEntity<Page<EMWO_DPDTO>> searchEMWO_DPToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_DPSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_DP> domains = emwo_dpService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_dpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

