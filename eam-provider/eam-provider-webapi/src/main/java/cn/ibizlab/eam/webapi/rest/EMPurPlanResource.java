package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPurPlan;
import cn.ibizlab.eam.core.eam_core.service.IEMPurPlanService;
import cn.ibizlab.eam.core.eam_core.filter.EMPurPlanSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划修理" })
@RestController("WebApi-empurplan")
@RequestMapping("")
public class EMPurPlanResource {

    @Autowired
    public IEMPurPlanService empurplanService;

    @Autowired
    @Lazy
    public EMPurPlanMapping empurplanMapping;

    @PreAuthorize("hasPermission(this.empurplanMapping.toDomain(#empurplandto),'eam-EMPurPlan-Create')")
    @ApiOperation(value = "新建计划修理", tags = {"计划修理" },  notes = "新建计划修理")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans")
    public ResponseEntity<EMPurPlanDTO> create(@Validated @RequestBody EMPurPlanDTO empurplandto) {
        EMPurPlan domain = empurplanMapping.toDomain(empurplandto);
		empurplanService.create(domain);
        EMPurPlanDTO dto = empurplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empurplanMapping.toDomain(#empurplandtos),'eam-EMPurPlan-Create')")
    @ApiOperation(value = "批量新建计划修理", tags = {"计划修理" },  notes = "批量新建计划修理")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPurPlanDTO> empurplandtos) {
        empurplanService.createBatch(empurplanMapping.toDomain(empurplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empurplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empurplanService.get(#empurplan_id),'eam-EMPurPlan-Update')")
    @ApiOperation(value = "更新计划修理", tags = {"计划修理" },  notes = "更新计划修理")
	@RequestMapping(method = RequestMethod.PUT, value = "/empurplans/{empurplan_id}")
    public ResponseEntity<EMPurPlanDTO> update(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMPurPlanDTO empurplandto) {
		EMPurPlan domain  = empurplanMapping.toDomain(empurplandto);
        domain .setEmpurplanid(empurplan_id);
		empurplanService.update(domain );
		EMPurPlanDTO dto = empurplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empurplanService.getEmpurplanByEntities(this.empurplanMapping.toDomain(#empurplandtos)),'eam-EMPurPlan-Update')")
    @ApiOperation(value = "批量更新计划修理", tags = {"计划修理" },  notes = "批量更新计划修理")
	@RequestMapping(method = RequestMethod.PUT, value = "/empurplans/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPurPlanDTO> empurplandtos) {
        empurplanService.updateBatch(empurplanMapping.toDomain(empurplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empurplanService.get(#empurplan_id),'eam-EMPurPlan-Remove')")
    @ApiOperation(value = "删除计划修理", tags = {"计划修理" },  notes = "删除计划修理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empurplans/{empurplan_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("empurplan_id") String empurplan_id) {
         return ResponseEntity.status(HttpStatus.OK).body(empurplanService.remove(empurplan_id));
    }

    @PreAuthorize("hasPermission(this.empurplanService.getEmpurplanByIds(#ids),'eam-EMPurPlan-Remove')")
    @ApiOperation(value = "批量删除计划修理", tags = {"计划修理" },  notes = "批量删除计划修理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empurplans/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        empurplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empurplanMapping.toDomain(returnObject.body),'eam-EMPurPlan-Get')")
    @ApiOperation(value = "获取计划修理", tags = {"计划修理" },  notes = "获取计划修理")
	@RequestMapping(method = RequestMethod.GET, value = "/empurplans/{empurplan_id}")
    public ResponseEntity<EMPurPlanDTO> get(@PathVariable("empurplan_id") String empurplan_id) {
        EMPurPlan domain = empurplanService.get(empurplan_id);
        EMPurPlanDTO dto = empurplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划修理草稿", tags = {"计划修理" },  notes = "获取计划修理草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/empurplans/getdraft")
    public ResponseEntity<EMPurPlanDTO> getDraft(EMPurPlanDTO dto) {
        EMPurPlan domain = empurplanMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(empurplanMapping.toDto(empurplanService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划修理", tags = {"计划修理" },  notes = "检查计划修理")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPurPlanDTO empurplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empurplanService.checkKey(empurplanMapping.toDomain(empurplandto)));
    }

    @PreAuthorize("hasPermission(this.empurplanMapping.toDomain(#empurplandto),'eam-EMPurPlan-Save')")
    @ApiOperation(value = "保存计划修理", tags = {"计划修理" },  notes = "保存计划修理")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/save")
    public ResponseEntity<EMPurPlanDTO> save(@RequestBody EMPurPlanDTO empurplandto) {
        EMPurPlan domain = empurplanMapping.toDomain(empurplandto);
        empurplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empurplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.empurplanMapping.toDomain(#empurplandtos),'eam-EMPurPlan-Save')")
    @ApiOperation(value = "批量保存计划修理", tags = {"计划修理" },  notes = "批量保存计划修理")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPurPlanDTO> empurplandtos) {
        empurplanService.saveBatch(empurplanMapping.toDomain(empurplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPurPlan-searchDefault-all') and hasPermission(#context,'eam-EMPurPlan-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划修理" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/empurplans/fetchdefault")
	public ResponseEntity<List<EMPurPlanDTO>> fetchDefault(EMPurPlanSearchContext context) {
        Page<EMPurPlan> domains = empurplanService.searchDefault(context) ;
        List<EMPurPlanDTO> list = empurplanMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPurPlan-searchDefault-all') and hasPermission(#context,'eam-EMPurPlan-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划修理" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/empurplans/searchdefault")
	public ResponseEntity<Page<EMPurPlanDTO>> searchDefault(@RequestBody EMPurPlanSearchContext context) {
        Page<EMPurPlan> domains = empurplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empurplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

