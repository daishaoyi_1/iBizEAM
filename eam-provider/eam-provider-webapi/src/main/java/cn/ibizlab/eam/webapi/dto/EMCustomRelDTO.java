package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMCustomRelDTO]
 */
@Data
public class EMCustomRelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CUSTOMSTYLE]
     *
     */
    @JSONField(name = "customstyle")
    @JsonProperty("customstyle")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String customstyle;

    /**
     * 属性 [CANYURENYUAN]
     *
     */
    @JSONField(name = "canyurenyuan")
    @JsonProperty("canyurenyuan")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String canyurenyuan;

    /**
     * 属性 [DEGREE]
     *
     */
    @JSONField(name = "degree")
    @JsonProperty("degree")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String degree;

    /**
     * 属性 [CHUANGONGSI]
     *
     */
    @JSONField(name = "chuangongsi")
    @JsonProperty("chuangongsi")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String chuangongsi;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [SHGLIANG]
     *
     */
    @JSONField(name = "shgliang")
    @JsonProperty("shgliang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String shgliang;

    /**
     * 属性 [QIYUNGANG]
     *
     */
    @JSONField(name = "qiyungang")
    @JsonProperty("qiyungang")
    @NotBlank(message = "[起运港]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String qiyungang;

    /**
     * 属性 [LYGLIANG]
     *
     */
    @JSONField(name = "lygliang")
    @JsonProperty("lygliang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lygliang;

    /**
     * 属性 [EMCUSTOMRELNAME]
     *
     */
    @JSONField(name = "emcustomrelname")
    @JsonProperty("emcustomrelname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emcustomrelname;

    /**
     * 属性 [HUOMING]
     *
     */
    @JSONField(name = "huoming")
    @JsonProperty("huoming")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String huoming;

    /**
     * 属性 [XIANGXING]
     *
     */
    @JSONField(name = "xiangxing")
    @JsonProperty("xiangxing")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String xiangxing;

    /**
     * 属性 [MUDIGANG]
     *
     */
    @JSONField(name = "mudigang")
    @JsonProperty("mudigang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mudigang;

    /**
     * 属性 [LIANXIREN]
     *
     */
    @JSONField(name = "lianxiren")
    @JsonProperty("lianxiren")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lianxiren;

    /**
     * 属性 [TJGLIANG]
     *
     */
    @JSONField(name = "tjgliang")
    @JsonProperty("tjgliang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tjgliang;

    /**
     * 属性 [HANGYEYINGXIANG]
     *
     */
    @JSONField(name = "hangyeyingxiang")
    @JsonProperty("hangyeyingxiang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String hangyeyingxiang;

    /**
     * 属性 [INOUT]
     *
     */
    @JSONField(name = "inout")
    @JsonProperty("inout")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String inout;

    /**
     * 属性 [BEIZHU]
     *
     */
    @JSONField(name = "beizhu")
    @JsonProperty("beizhu")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String beizhu;

    /**
     * 属性 [QIYEJIANJIE]
     *
     */
    @JSONField(name = "qiyejianjie")
    @JsonProperty("qiyejianjie")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String qiyejianjie;

    /**
     * 属性 [GUIMO]
     *
     */
    @JSONField(name = "guimo")
    @JsonProperty("guimo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String guimo;

    /**
     * 属性 [ADDRESS]
     *
     */
    @JSONField(name = "address")
    @JsonProperty("address")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String address;

    /**
     * 属性 [CHUANZHEN]
     *
     */
    @JSONField(name = "chuanzhen")
    @JsonProperty("chuanzhen")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String chuanzhen;

    /**
     * 属性 [YUNSHUTIAOKUAN]
     *
     */
    @JSONField(name = "yunshutiaokuan")
    @JsonProperty("yunshutiaokuan")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String yunshutiaokuan;

    /**
     * 属性 [YOUXIANG]
     *
     */
    @JSONField(name = "youxiang")
    @JsonProperty("youxiang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String youxiang;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CCOUNT]
     *
     */
    @JSONField(name = "ccount")
    @JsonProperty("ccount")
    private Double ccount;

    /**
     * 属性 [EMCUSTOMRELID]
     *
     */
    @JSONField(name = "emcustomrelid")
    @JsonProperty("emcustomrelid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emcustomrelid;

    /**
     * 属性 [LIANXIDIANHUA]
     *
     */
    @JSONField(name = "lianxidianhua")
    @JsonProperty("lianxidianhua")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lianxidianhua;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CWEIGHT]
     *
     */
    @JSONField(name = "cweight")
    @JsonProperty("cweight")
    private Double cweight;

    /**
     * 属性 [FAREN]
     *
     */
    @JSONField(name = "faren")
    @JsonProperty("faren")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String faren;

    /**
     * 属性 [KEHUXUQIU]
     *
     */
    @JSONField(name = "kehuxuqiu")
    @JsonProperty("kehuxuqiu")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kehuxuqiu;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [FARENLIANXI]
     *
     */
    @JSONField(name = "farenlianxi")
    @JsonProperty("farenlianxi")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String farenlianxi;

    /**
     * 属性 [HANGYE]
     *
     */
    @JSONField(name = "hangye")
    @JsonProperty("hangye")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String hangye;

    /**
     * 属性 [CHENGYUNREN]
     *
     */
    @JSONField(name = "chengyunren")
    @JsonProperty("chengyunren")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String chengyunren;

    /**
     * 属性 [YUEXIANGLIANG]
     *
     */
    @JSONField(name = "yuexiangliang")
    @JsonProperty("yuexiangliang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String yuexiangliang;

    /**
     * 属性 [CUSTOMAREA]
     *
     */
    @JSONField(name = "customarea")
    @JsonProperty("customarea")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String customarea;

    /**
     * 属性 [QDGLIANG]
     *
     */
    @JSONField(name = "qdgliang")
    @JsonProperty("qdgliang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String qdgliang;

    /**
     * 属性 [TRENDTO]
     *
     */
    @JSONField(name = "trendto")
    @JsonProperty("trendto")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String trendto;

    /**
     * 属性 [XINGZHI]
     *
     */
    @JSONField(name = "xingzhi")
    @JsonProperty("xingzhi")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String xingzhi;

    /**
     * 属性 [RECENTACCESS]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "recentaccess" , format="yyyy-MM-dd")
    @JsonProperty("recentaccess")
    private Timestamp recentaccess;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [TRANROAD]
     *
     */
    @JSONField(name = "tranroad")
    @JsonProperty("tranroad")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tranroad;

    /**
     * 属性 [YUNSHUFANGSHI]
     *
     */
    @JSONField(name = "yunshufangshi")
    @JsonProperty("yunshufangshi")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String yunshufangshi;


    /**
     * 设置 [CUSTOMSTYLE]
     */
    public void setCustomstyle(String  customstyle){
        this.customstyle = customstyle ;
        this.modify("customstyle",customstyle);
    }

    /**
     * 设置 [CANYURENYUAN]
     */
    public void setCanyurenyuan(String  canyurenyuan){
        this.canyurenyuan = canyurenyuan ;
        this.modify("canyurenyuan",canyurenyuan);
    }

    /**
     * 设置 [DEGREE]
     */
    public void setDegree(String  degree){
        this.degree = degree ;
        this.modify("degree",degree);
    }

    /**
     * 设置 [CHUANGONGSI]
     */
    public void setChuangongsi(String  chuangongsi){
        this.chuangongsi = chuangongsi ;
        this.modify("chuangongsi",chuangongsi);
    }

    /**
     * 设置 [SHGLIANG]
     */
    public void setShgliang(String  shgliang){
        this.shgliang = shgliang ;
        this.modify("shgliang",shgliang);
    }

    /**
     * 设置 [QIYUNGANG]
     */
    public void setQiyungang(String  qiyungang){
        this.qiyungang = qiyungang ;
        this.modify("qiyungang",qiyungang);
    }

    /**
     * 设置 [LYGLIANG]
     */
    public void setLygliang(String  lygliang){
        this.lygliang = lygliang ;
        this.modify("lygliang",lygliang);
    }

    /**
     * 设置 [EMCUSTOMRELNAME]
     */
    public void setEmcustomrelname(String  emcustomrelname){
        this.emcustomrelname = emcustomrelname ;
        this.modify("emcustomrelname",emcustomrelname);
    }

    /**
     * 设置 [HUOMING]
     */
    public void setHuoming(String  huoming){
        this.huoming = huoming ;
        this.modify("huoming",huoming);
    }

    /**
     * 设置 [XIANGXING]
     */
    public void setXiangxing(String  xiangxing){
        this.xiangxing = xiangxing ;
        this.modify("xiangxing",xiangxing);
    }

    /**
     * 设置 [MUDIGANG]
     */
    public void setMudigang(String  mudigang){
        this.mudigang = mudigang ;
        this.modify("mudigang",mudigang);
    }

    /**
     * 设置 [LIANXIREN]
     */
    public void setLianxiren(String  lianxiren){
        this.lianxiren = lianxiren ;
        this.modify("lianxiren",lianxiren);
    }

    /**
     * 设置 [TJGLIANG]
     */
    public void setTjgliang(String  tjgliang){
        this.tjgliang = tjgliang ;
        this.modify("tjgliang",tjgliang);
    }

    /**
     * 设置 [HANGYEYINGXIANG]
     */
    public void setHangyeyingxiang(String  hangyeyingxiang){
        this.hangyeyingxiang = hangyeyingxiang ;
        this.modify("hangyeyingxiang",hangyeyingxiang);
    }

    /**
     * 设置 [INOUT]
     */
    public void setInout(String  inout){
        this.inout = inout ;
        this.modify("inout",inout);
    }

    /**
     * 设置 [BEIZHU]
     */
    public void setBeizhu(String  beizhu){
        this.beizhu = beizhu ;
        this.modify("beizhu",beizhu);
    }

    /**
     * 设置 [QIYEJIANJIE]
     */
    public void setQiyejianjie(String  qiyejianjie){
        this.qiyejianjie = qiyejianjie ;
        this.modify("qiyejianjie",qiyejianjie);
    }

    /**
     * 设置 [GUIMO]
     */
    public void setGuimo(String  guimo){
        this.guimo = guimo ;
        this.modify("guimo",guimo);
    }

    /**
     * 设置 [ADDRESS]
     */
    public void setAddress(String  address){
        this.address = address ;
        this.modify("address",address);
    }

    /**
     * 设置 [CHUANZHEN]
     */
    public void setChuanzhen(String  chuanzhen){
        this.chuanzhen = chuanzhen ;
        this.modify("chuanzhen",chuanzhen);
    }

    /**
     * 设置 [YUNSHUTIAOKUAN]
     */
    public void setYunshutiaokuan(String  yunshutiaokuan){
        this.yunshutiaokuan = yunshutiaokuan ;
        this.modify("yunshutiaokuan",yunshutiaokuan);
    }

    /**
     * 设置 [YOUXIANG]
     */
    public void setYouxiang(String  youxiang){
        this.youxiang = youxiang ;
        this.modify("youxiang",youxiang);
    }

    /**
     * 设置 [CCOUNT]
     */
    public void setCcount(Double  ccount){
        this.ccount = ccount ;
        this.modify("ccount",ccount);
    }

    /**
     * 设置 [LIANXIDIANHUA]
     */
    public void setLianxidianhua(String  lianxidianhua){
        this.lianxidianhua = lianxidianhua ;
        this.modify("lianxidianhua",lianxidianhua);
    }

    /**
     * 设置 [CWEIGHT]
     */
    public void setCweight(Double  cweight){
        this.cweight = cweight ;
        this.modify("cweight",cweight);
    }

    /**
     * 设置 [FAREN]
     */
    public void setFaren(String  faren){
        this.faren = faren ;
        this.modify("faren",faren);
    }

    /**
     * 设置 [KEHUXUQIU]
     */
    public void setKehuxuqiu(String  kehuxuqiu){
        this.kehuxuqiu = kehuxuqiu ;
        this.modify("kehuxuqiu",kehuxuqiu);
    }

    /**
     * 设置 [FARENLIANXI]
     */
    public void setFarenlianxi(String  farenlianxi){
        this.farenlianxi = farenlianxi ;
        this.modify("farenlianxi",farenlianxi);
    }

    /**
     * 设置 [HANGYE]
     */
    public void setHangye(String  hangye){
        this.hangye = hangye ;
        this.modify("hangye",hangye);
    }

    /**
     * 设置 [CHENGYUNREN]
     */
    public void setChengyunren(String  chengyunren){
        this.chengyunren = chengyunren ;
        this.modify("chengyunren",chengyunren);
    }

    /**
     * 设置 [YUEXIANGLIANG]
     */
    public void setYuexiangliang(String  yuexiangliang){
        this.yuexiangliang = yuexiangliang ;
        this.modify("yuexiangliang",yuexiangliang);
    }

    /**
     * 设置 [CUSTOMAREA]
     */
    public void setCustomarea(String  customarea){
        this.customarea = customarea ;
        this.modify("customarea",customarea);
    }

    /**
     * 设置 [QDGLIANG]
     */
    public void setQdgliang(String  qdgliang){
        this.qdgliang = qdgliang ;
        this.modify("qdgliang",qdgliang);
    }

    /**
     * 设置 [TRENDTO]
     */
    public void setTrendto(String  trendto){
        this.trendto = trendto ;
        this.modify("trendto",trendto);
    }

    /**
     * 设置 [XINGZHI]
     */
    public void setXingzhi(String  xingzhi){
        this.xingzhi = xingzhi ;
        this.modify("xingzhi",xingzhi);
    }

    /**
     * 设置 [RECENTACCESS]
     */
    public void setRecentaccess(Timestamp  recentaccess){
        this.recentaccess = recentaccess ;
        this.modify("recentaccess",recentaccess);
    }

    /**
     * 设置 [TRANROAD]
     */
    public void setTranroad(String  tranroad){
        this.tranroad = tranroad ;
        this.modify("tranroad",tranroad);
    }

    /**
     * 设置 [YUNSHUFANGSHI]
     */
    public void setYunshufangshi(String  yunshufangshi){
        this.yunshufangshi = yunshufangshi ;
        this.modify("yunshufangshi",yunshufangshi);
    }


}


