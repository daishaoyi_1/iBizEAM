package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMWeeklyPlanDTO]
 */
@Data
public class EMWeeklyPlanDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [SETDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "setdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("setdate")
    private Timestamp setdate;

    /**
     * 属性 [NEXTWEEK]
     *
     */
    @JSONField(name = "nextweek")
    @JsonProperty("nextweek")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String nextweek;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @NotBlank(message = "[状态]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String state;

    /**
     * 属性 [LASTWEEK]
     *
     */
    @JSONField(name = "lastweek")
    @JsonProperty("lastweek")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String lastweek;

    /**
     * 属性 [EMWEEKLYPLANNAME]
     *
     */
    @JSONField(name = "emweeklyplanname")
    @JsonProperty("emweeklyplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emweeklyplanname;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMWEEKLYPLANID]
     *
     */
    @JSONField(name = "emweeklyplanid")
    @JsonProperty("emweeklyplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emweeklyplanid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [NUM]
     *
     */
    @JSONField(name = "num")
    @JsonProperty("num")
    private Integer num;

    /**
     * 属性 [WEEKPLAN]
     *
     */
    @JSONField(name = "weekplan")
    @JsonProperty("weekplan")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String weekplan;

    /**
     * 属性 [PFTEAMNAME]
     *
     */
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfteamname;

    /**
     * 属性 [PFTEAMID]
     *
     */
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfteamid;


    /**
     * 设置 [SETDATE]
     */
    public void setSetdate(Timestamp  setdate){
        this.setdate = setdate ;
        this.modify("setdate",setdate);
    }

    /**
     * 设置 [NEXTWEEK]
     */
    public void setNextweek(String  nextweek){
        this.nextweek = nextweek ;
        this.modify("nextweek",nextweek);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [LASTWEEK]
     */
    public void setLastweek(String  lastweek){
        this.lastweek = lastweek ;
        this.modify("lastweek",lastweek);
    }

    /**
     * 设置 [EMWEEKLYPLANNAME]
     */
    public void setEmweeklyplanname(String  emweeklyplanname){
        this.emweeklyplanname = emweeklyplanname ;
        this.modify("emweeklyplanname",emweeklyplanname);
    }

    /**
     * 设置 [NUM]
     */
    public void setNum(Integer  num){
        this.num = num ;
        this.modify("num",num);
    }

    /**
     * 设置 [WEEKPLAN]
     */
    public void setWeekplan(String  weekplan){
        this.weekplan = weekplan ;
        this.modify("weekplan",weekplan);
    }

    /**
     * 设置 [PFTEAMID]
     */
    public void setPfteamid(String  pfteamid){
        this.pfteamid = pfteamid ;
        this.modify("pfteamid",pfteamid);
    }


}


