package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareDetail;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSpareDetailService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareDetailSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"备件包明细" })
@RestController("WebApi-emeqsparedetail")
@RequestMapping("")
public class EMEQSpareDetailResource {

    @Autowired
    public IEMEQSpareDetailService emeqsparedetailService;

    @Autowired
    @Lazy
    public EMEQSpareDetailMapping emeqsparedetailMapping;

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "新建备件包明细", tags = {"备件包明细" },  notes = "新建备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparedetails")
    public ResponseEntity<EMEQSpareDetailDTO> create(@Validated @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
		emeqsparedetailService.create(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "批量新建备件包明细", tags = {"备件包明细" },  notes = "批量新建备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparedetails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        emeqsparedetailService.createBatch(emeqsparedetailMapping.toDomain(emeqsparedetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "更新备件包明细", tags = {"备件包明细" },  notes = "更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> update(@PathVariable("emeqsparedetail_id") String emeqsparedetail_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
		EMEQSpareDetail domain  = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain .setEmeqsparedetailid(emeqsparedetail_id);
		emeqsparedetailService.update(domain );
		EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByEntities(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos)),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "批量更新备件包明细", tags = {"备件包明细" },  notes = "批量更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqsparedetails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        emeqsparedetailService.updateBatch(emeqsparedetailMapping.toDomain(emeqsparedetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "删除备件包明细", tags = {"备件包明细" },  notes = "删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.remove(emeqsparedetail_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByIds(#ids),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "批量删除备件包明细", tags = {"备件包明细" },  notes = "批量删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqsparedetails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqsparedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(returnObject.body),'eam-EMEQSpareDetail-Get')")
    @ApiOperation(value = "获取备件包明细", tags = {"备件包明细" },  notes = "获取备件包明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> get(@PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
        EMEQSpareDetail domain = emeqsparedetailService.get(emeqsparedetail_id);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取备件包明细草稿", tags = {"备件包明细" },  notes = "获取备件包明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqsparedetails/getdraft")
    public ResponseEntity<EMEQSpareDetailDTO> getDraft(EMEQSpareDetailDTO dto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(emeqsparedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "检查备件包明细", tags = {"备件包明细" },  notes = "检查备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparedetails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.checkKey(emeqsparedetailMapping.toDomain(emeqsparedetaildto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "保存备件包明细", tags = {"备件包明细" },  notes = "保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparedetails/save")
    public ResponseEntity<EMEQSpareDetailDTO> save(@RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        emeqsparedetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "批量保存备件包明细", tags = {"备件包明细" },  notes = "批量保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparedetails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        emeqsparedetailService.saveBatch(emeqsparedetailMapping.toDomain(emeqsparedetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"备件包明细" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqsparedetails/fetchdefault")
	public ResponseEntity<List<EMEQSpareDetailDTO>> fetchDefault(EMEQSpareDetailSearchContext context) {
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
        List<EMEQSpareDetailDTO> list = emeqsparedetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"备件包明细" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqsparedetails/searchdefault")
	public ResponseEntity<Page<EMEQSpareDetailDTO>> searchDefault(@RequestBody EMEQSpareDetailSearchContext context) {
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据备件包建立备件包明细", tags = {"备件包明细" },  notes = "根据备件包建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparedetails")
    public ResponseEntity<EMEQSpareDetailDTO> createByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setEqspareid(emeqspare_id);
		emeqsparedetailService.create(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据备件包批量建立备件包明细", tags = {"备件包明细" },  notes = "根据备件包批量建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> createBatchByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setEqspareid(emeqspare_id);
        }
        emeqsparedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据备件包更新备件包明细", tags = {"备件包明细" },  notes = "根据备件包更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> updateByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setEqspareid(emeqspare_id);
        domain.setEmeqsparedetailid(emeqsparedetail_id);
		emeqsparedetailService.update(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByEntities(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos)),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据备件包批量更新备件包明细", tags = {"备件包明细" },  notes = "根据备件包批量更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setEqspareid(emeqspare_id);
        }
        emeqsparedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据备件包删除备件包明细", tags = {"备件包明细" },  notes = "根据备件包删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<Boolean> removeByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.remove(emeqsparedetail_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByIds(#ids),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据备件包批量删除备件包明细", tags = {"备件包明细" },  notes = "根据备件包批量删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMEQSpare(@RequestBody List<String> ids) {
        emeqsparedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(returnObject.body),'eam-EMEQSpareDetail-Get')")
    @ApiOperation(value = "根据备件包获取备件包明细", tags = {"备件包明细" },  notes = "根据备件包获取备件包明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> getByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
        EMEQSpareDetail domain = emeqsparedetailService.get(emeqsparedetail_id);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据备件包获取备件包明细草稿", tags = {"备件包明细" },  notes = "根据备件包获取备件包明细草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/getdraft")
    public ResponseEntity<EMEQSpareDetailDTO> getDraftByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, EMEQSpareDetailDTO dto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(dto);
        domain.setEqspareid(emeqspare_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(emeqsparedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据备件包检查备件包明细", tags = {"备件包明细" },  notes = "根据备件包检查备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.checkKey(emeqsparedetailMapping.toDomain(emeqsparedetaildto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据备件包保存备件包明细", tags = {"备件包明细" },  notes = "根据备件包保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/save")
    public ResponseEntity<EMEQSpareDetailDTO> saveByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setEqspareid(emeqspare_id);
        emeqsparedetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据备件包批量保存备件包明细", tags = {"备件包明细" },  notes = "根据备件包批量保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
             domain.setEqspareid(emeqspare_id);
        }
        emeqsparedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据备件包获取DEFAULT", tags = {"备件包明细" } ,notes = "根据备件包获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqspares/{emeqspare_id}/emeqsparedetails/fetchdefault")
	public ResponseEntity<List<EMEQSpareDetailDTO>> fetchEMEQSpareDetailDefaultByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id,EMEQSpareDetailSearchContext context) {
        context.setN_eqspareid_eq(emeqspare_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
        List<EMEQSpareDetailDTO> list = emeqsparedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据备件包查询DEFAULT", tags = {"备件包明细" } ,notes = "根据备件包查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqspares/{emeqspare_id}/emeqsparedetails/searchdefault")
	public ResponseEntity<Page<EMEQSpareDetailDTO>> searchEMEQSpareDetailDefaultByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareDetailSearchContext context) {
        context.setN_eqspareid_eq(emeqspare_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据物品建立备件包明细", tags = {"备件包明细" },  notes = "根据物品建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emeqsparedetails")
    public ResponseEntity<EMEQSpareDetailDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
		emeqsparedetailService.create(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据物品批量建立备件包明细", tags = {"备件包明细" },  notes = "根据物品批量建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据物品更新备件包明细", tags = {"备件包明细" },  notes = "根据物品更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        domain.setEmeqsparedetailid(emeqsparedetail_id);
		emeqsparedetailService.update(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByEntities(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos)),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据物品批量更新备件包明细", tags = {"备件包明细" },  notes = "根据物品批量更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据物品删除备件包明细", tags = {"备件包明细" },  notes = "根据物品删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.remove(emeqsparedetail_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByIds(#ids),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据物品批量删除备件包明细", tags = {"备件包明细" },  notes = "根据物品批量删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emeqsparedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(returnObject.body),'eam-EMEQSpareDetail-Get')")
    @ApiOperation(value = "根据物品获取备件包明细", tags = {"备件包明细" },  notes = "根据物品获取备件包明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
        EMEQSpareDetail domain = emeqsparedetailService.get(emeqsparedetail_id);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取备件包明细草稿", tags = {"备件包明细" },  notes = "根据物品获取备件包明细草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emeqsparedetails/getdraft")
    public ResponseEntity<EMEQSpareDetailDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMEQSpareDetailDTO dto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(emeqsparedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查备件包明细", tags = {"备件包明细" },  notes = "根据物品检查备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emeqsparedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.checkKey(emeqsparedetailMapping.toDomain(emeqsparedetaildto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据物品保存备件包明细", tags = {"备件包明细" },  notes = "根据物品保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emeqsparedetails/save")
    public ResponseEntity<EMEQSpareDetailDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        emeqsparedetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据物品批量保存备件包明细", tags = {"备件包明细" },  notes = "根据物品批量保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emeqsparedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emeqsparedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"备件包明细" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emeqsparedetails/fetchdefault")
	public ResponseEntity<List<EMEQSpareDetailDTO>> fetchEMEQSpareDetailDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
        List<EMEQSpareDetailDTO> list = emeqsparedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"备件包明细" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emeqsparedetails/searchdefault")
	public ResponseEntity<Page<EMEQSpareDetailDTO>> searchEMEQSpareDetailDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据服务商物品建立备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails")
    public ResponseEntity<EMEQSpareDetailDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
		emeqsparedetailService.create(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据服务商物品批量建立备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品批量建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据服务商物品更新备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        domain.setEmeqsparedetailid(emeqsparedetail_id);
		emeqsparedetailService.update(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByEntities(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos)),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据服务商物品批量更新备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品批量更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据服务商物品删除备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.remove(emeqsparedetail_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByIds(#ids),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品批量删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emeqsparedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(returnObject.body),'eam-EMEQSpareDetail-Get')")
    @ApiOperation(value = "根据服务商物品获取备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品获取备件包明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
        EMEQSpareDetail domain = emeqsparedetailService.get(emeqsparedetail_id);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取备件包明细草稿", tags = {"备件包明细" },  notes = "根据服务商物品获取备件包明细草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/getdraft")
    public ResponseEntity<EMEQSpareDetailDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMEQSpareDetailDTO dto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(emeqsparedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品检查备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.checkKey(emeqsparedetailMapping.toDomain(emeqsparedetaildto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据服务商物品保存备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/save")
    public ResponseEntity<EMEQSpareDetailDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        emeqsparedetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据服务商物品批量保存备件包明细", tags = {"备件包明细" },  notes = "根据服务商物品批量保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emeqsparedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"备件包明细" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/fetchdefault")
	public ResponseEntity<List<EMEQSpareDetailDTO>> fetchEMEQSpareDetailDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
        List<EMEQSpareDetailDTO> list = emeqsparedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"备件包明细" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emeqsparedetails/searchdefault")
	public ResponseEntity<Page<EMEQSpareDetailDTO>> searchEMEQSpareDetailDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据仓库库位物品建立备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails")
    public ResponseEntity<EMEQSpareDetailDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
		emeqsparedetailService.create(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品批量建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据仓库库位物品更新备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        domain.setEmeqsparedetailid(emeqsparedetail_id);
		emeqsparedetailService.update(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByEntities(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos)),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品批量更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.remove(emeqsparedetail_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByIds(#ids),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品批量删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emeqsparedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(returnObject.body),'eam-EMEQSpareDetail-Get')")
    @ApiOperation(value = "根据仓库库位物品获取备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品获取备件包明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
        EMEQSpareDetail domain = emeqsparedetailService.get(emeqsparedetail_id);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取备件包明细草稿", tags = {"备件包明细" },  notes = "根据仓库库位物品获取备件包明细草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/getdraft")
    public ResponseEntity<EMEQSpareDetailDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMEQSpareDetailDTO dto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(emeqsparedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品检查备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.checkKey(emeqsparedetailMapping.toDomain(emeqsparedetaildto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据仓库库位物品保存备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/save")
    public ResponseEntity<EMEQSpareDetailDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        emeqsparedetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存备件包明细", tags = {"备件包明细" },  notes = "根据仓库库位物品批量保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emeqsparedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"备件包明细" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/fetchdefault")
	public ResponseEntity<List<EMEQSpareDetailDTO>> fetchEMEQSpareDetailDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
        List<EMEQSpareDetailDTO> list = emeqsparedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"备件包明细" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/searchdefault")
	public ResponseEntity<Page<EMEQSpareDetailDTO>> searchEMEQSpareDetailDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据仓库物品建立备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails")
    public ResponseEntity<EMEQSpareDetailDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
		emeqsparedetailService.create(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据仓库物品批量建立备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品批量建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据仓库物品更新备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        domain.setEmeqsparedetailid(emeqsparedetail_id);
		emeqsparedetailService.update(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByEntities(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos)),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据仓库物品批量更新备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品批量更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据仓库物品删除备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.remove(emeqsparedetail_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByIds(#ids),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品批量删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emeqsparedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(returnObject.body),'eam-EMEQSpareDetail-Get')")
    @ApiOperation(value = "根据仓库物品获取备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品获取备件包明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
        EMEQSpareDetail domain = emeqsparedetailService.get(emeqsparedetail_id);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取备件包明细草稿", tags = {"备件包明细" },  notes = "根据仓库物品获取备件包明细草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/getdraft")
    public ResponseEntity<EMEQSpareDetailDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMEQSpareDetailDTO dto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(emeqsparedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品检查备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.checkKey(emeqsparedetailMapping.toDomain(emeqsparedetaildto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据仓库物品保存备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/save")
    public ResponseEntity<EMEQSpareDetailDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        emeqsparedetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据仓库物品批量保存备件包明细", tags = {"备件包明细" },  notes = "根据仓库物品批量保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emeqsparedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"备件包明细" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/fetchdefault")
	public ResponseEntity<List<EMEQSpareDetailDTO>> fetchEMEQSpareDetailDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
        List<EMEQSpareDetailDTO> list = emeqsparedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"备件包明细" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emeqsparedetails/searchdefault")
	public ResponseEntity<Page<EMEQSpareDetailDTO>> searchEMEQSpareDetailDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails")
    public ResponseEntity<EMEQSpareDetailDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
		emeqsparedetailService.create(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品批量建立备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparedetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        domain.setEmeqsparedetailid(emeqsparedetail_id);
		emeqsparedetailService.update(domain);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByEntities(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos)),'eam-EMEQSpareDetail-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品批量更新备件包明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emeqsparedetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.get(#emeqsparedetail_id),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.remove(emeqsparedetail_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailService.getEmeqsparedetailByIds(#ids),'eam-EMEQSpareDetail-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品批量删除备件包明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emeqsparedetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(returnObject.body),'eam-EMEQSpareDetail-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品获取备件包明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/{emeqsparedetail_id}")
    public ResponseEntity<EMEQSpareDetailDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emeqsparedetail_id") String emeqsparedetail_id) {
        EMEQSpareDetail domain = emeqsparedetailService.get(emeqsparedetail_id);
        EMEQSpareDetailDTO dto = emeqsparedetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取备件包明细草稿", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品获取备件包明细草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/getdraft")
    public ResponseEntity<EMEQSpareDetailDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMEQSpareDetailDTO dto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(emeqsparedetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品检查备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailService.checkKey(emeqsparedetailMapping.toDomain(emeqsparedetaildto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildto),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/save")
    public ResponseEntity<EMEQSpareDetailDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailDTO emeqsparedetaildto) {
        EMEQSpareDetail domain = emeqsparedetailMapping.toDomain(emeqsparedetaildto);
        domain.setItemid(emitem_id);
        emeqsparedetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparedetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparedetailMapping.toDomain(#emeqsparedetaildtos),'eam-EMEQSpareDetail-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存备件包明细", tags = {"备件包明细" },  notes = "根据仓库仓库库位物品批量保存备件包明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMEQSpareDetailDTO> emeqsparedetaildtos) {
        List<EMEQSpareDetail> domainlist=emeqsparedetailMapping.toDomain(emeqsparedetaildtos);
        for(EMEQSpareDetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emeqsparedetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"备件包明细" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/fetchdefault")
	public ResponseEntity<List<EMEQSpareDetailDTO>> fetchEMEQSpareDetailDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
        List<EMEQSpareDetailDTO> list = emeqsparedetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareDetail-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareDetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"备件包明细" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emeqsparedetails/searchdefault")
	public ResponseEntity<Page<EMEQSpareDetailDTO>> searchEMEQSpareDetailDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMEQSpareDetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEQSpareDetail> domains = emeqsparedetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparedetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

