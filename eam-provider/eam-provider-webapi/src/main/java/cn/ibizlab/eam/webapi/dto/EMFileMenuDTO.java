package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMFileMenuDTO]
 */
@Data
public class EMFileMenuDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMFILEMENUNAME]
     *
     */
    @JSONField(name = "emfilemenuname")
    @JsonProperty("emfilemenuname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emfilemenuname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EMFILEMENUID]
     *
     */
    @JSONField(name = "emfilemenuid")
    @JsonProperty("emfilemenuid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emfilemenuid;

    /**
     * 属性 [DEIDS]
     *
     */
    @JSONField(name = "deids")
    @JsonProperty("deids")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String deids;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;


    /**
     * 设置 [EMFILEMENUNAME]
     */
    public void setEmfilemenuname(String  emfilemenuname){
        this.emfilemenuname = emfilemenuname ;
        this.modify("emfilemenuname",emfilemenuname);
    }

    /**
     * 设置 [DEIDS]
     */
    public void setDeids(String  deids){
        this.deids = deids ;
        this.modify("deids",deids);
    }


}


