package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_M;
import cn.ibizlab.eam.webapi.dto.PLANSCHEDULE_MDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiPLANSCHEDULE_MMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PLANSCHEDULE_MMapping extends MappingBase<PLANSCHEDULE_MDTO, PLANSCHEDULE_M> {


}

