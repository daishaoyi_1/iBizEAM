package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQCheck;
import cn.ibizlab.eam.core.eam_core.service.IEMEQCheckService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQCheckSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"维修记录" })
@RestController("WebApi-emeqcheck")
@RequestMapping("")
public class EMEQCheckResource {

    @Autowired
    public IEMEQCheckService emeqcheckService;

    @Autowired
    @Lazy
    public EMEQCheckMapping emeqcheckMapping;

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdto),'eam-EMEQCheck-Create')")
    @ApiOperation(value = "新建维修记录", tags = {"维修记录" },  notes = "新建维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqchecks")
    public ResponseEntity<EMEQCheckDTO> create(@Validated @RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
		emeqcheckService.create(domain);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdtos),'eam-EMEQCheck-Create')")
    @ApiOperation(value = "批量新建维修记录", tags = {"维修记录" },  notes = "批量新建维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqchecks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        emeqcheckService.createBatch(emeqcheckMapping.toDomain(emeqcheckdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqcheck" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqcheckService.get(#emeqcheck_id),'eam-EMEQCheck-Update')")
    @ApiOperation(value = "更新维修记录", tags = {"维修记录" },  notes = "更新维修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<EMEQCheckDTO> update(@PathVariable("emeqcheck_id") String emeqcheck_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
		EMEQCheck domain  = emeqcheckMapping.toDomain(emeqcheckdto);
        domain .setEmeqcheckid(emeqcheck_id);
		emeqcheckService.update(domain );
		EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.getEmeqcheckByEntities(this.emeqcheckMapping.toDomain(#emeqcheckdtos)),'eam-EMEQCheck-Update')")
    @ApiOperation(value = "批量更新维修记录", tags = {"维修记录" },  notes = "批量更新维修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqchecks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        emeqcheckService.updateBatch(emeqcheckMapping.toDomain(emeqcheckdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.get(#emeqcheck_id),'eam-EMEQCheck-Remove')")
    @ApiOperation(value = "删除维修记录", tags = {"维修记录" },  notes = "删除维修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqcheck_id") String emeqcheck_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqcheckService.remove(emeqcheck_id));
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.getEmeqcheckByIds(#ids),'eam-EMEQCheck-Remove')")
    @ApiOperation(value = "批量删除维修记录", tags = {"维修记录" },  notes = "批量删除维修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqchecks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqcheckService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqcheckMapping.toDomain(returnObject.body),'eam-EMEQCheck-Get')")
    @ApiOperation(value = "获取维修记录", tags = {"维修记录" },  notes = "获取维修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<EMEQCheckDTO> get(@PathVariable("emeqcheck_id") String emeqcheck_id) {
        EMEQCheck domain = emeqcheckService.get(emeqcheck_id);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取维修记录草稿", tags = {"维修记录" },  notes = "获取维修记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqchecks/getdraft")
    public ResponseEntity<EMEQCheckDTO> getDraft(EMEQCheckDTO dto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqcheckMapping.toDto(emeqcheckService.getDraft(domain)));
    }

    @ApiOperation(value = "检查维修记录", tags = {"维修记录" },  notes = "检查维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqchecks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQCheckDTO emeqcheckdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqcheckService.checkKey(emeqcheckMapping.toDomain(emeqcheckdto)));
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdto),'eam-EMEQCheck-Save')")
    @ApiOperation(value = "保存维修记录", tags = {"维修记录" },  notes = "保存维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqchecks/save")
    public ResponseEntity<EMEQCheckDTO> save(@RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
        emeqcheckService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqcheckMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdtos),'eam-EMEQCheck-Save')")
    @ApiOperation(value = "批量保存维修记录", tags = {"维修记录" },  notes = "批量保存维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqchecks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        emeqcheckService.saveBatch(emeqcheckMapping.toDomain(emeqcheckdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchCalendar-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "获取日历查询", tags = {"维修记录" } ,notes = "获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emeqchecks/fetchcalendar")
	public ResponseEntity<List<EMEQCheckDTO>> fetchCalendar(EMEQCheckSearchContext context) {
        Page<EMEQCheck> domains = emeqcheckService.searchCalendar(context) ;
        List<EMEQCheckDTO> list = emeqcheckMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchCalendar-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "查询日历查询", tags = {"维修记录" } ,notes = "查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emeqchecks/searchcalendar")
	public ResponseEntity<Page<EMEQCheckDTO>> searchCalendar(@RequestBody EMEQCheckSearchContext context) {
        Page<EMEQCheck> domains = emeqcheckService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqcheckMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchDefault-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"维修记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqchecks/fetchdefault")
	public ResponseEntity<List<EMEQCheckDTO>> fetchDefault(EMEQCheckSearchContext context) {
        Page<EMEQCheck> domains = emeqcheckService.searchDefault(context) ;
        List<EMEQCheckDTO> list = emeqcheckMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchDefault-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"维修记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqchecks/searchdefault")
	public ResponseEntity<Page<EMEQCheckDTO>> searchDefault(@RequestBody EMEQCheckSearchContext context) {
        Page<EMEQCheck> domains = emeqcheckService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqcheckMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdto),'eam-EMEQCheck-Create')")
    @ApiOperation(value = "根据设备档案建立维修记录", tags = {"维修记录" },  notes = "根据设备档案建立维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqchecks")
    public ResponseEntity<EMEQCheckDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
        domain.setEquipid(emequip_id);
		emeqcheckService.create(domain);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdtos),'eam-EMEQCheck-Create')")
    @ApiOperation(value = "根据设备档案批量建立维修记录", tags = {"维修记录" },  notes = "根据设备档案批量建立维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqchecks/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        List<EMEQCheck> domainlist=emeqcheckMapping.toDomain(emeqcheckdtos);
        for(EMEQCheck domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqcheckService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqcheck" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqcheckService.get(#emeqcheck_id),'eam-EMEQCheck-Update')")
    @ApiOperation(value = "根据设备档案更新维修记录", tags = {"维修记录" },  notes = "根据设备档案更新维修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<EMEQCheckDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqcheck_id") String emeqcheck_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqcheckid(emeqcheck_id);
		emeqcheckService.update(domain);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.getEmeqcheckByEntities(this.emeqcheckMapping.toDomain(#emeqcheckdtos)),'eam-EMEQCheck-Update')")
    @ApiOperation(value = "根据设备档案批量更新维修记录", tags = {"维修记录" },  notes = "根据设备档案批量更新维修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqchecks/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        List<EMEQCheck> domainlist=emeqcheckMapping.toDomain(emeqcheckdtos);
        for(EMEQCheck domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqcheckService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.get(#emeqcheck_id),'eam-EMEQCheck-Remove')")
    @ApiOperation(value = "根据设备档案删除维修记录", tags = {"维修记录" },  notes = "根据设备档案删除维修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqcheck_id") String emeqcheck_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqcheckService.remove(emeqcheck_id));
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.getEmeqcheckByIds(#ids),'eam-EMEQCheck-Remove')")
    @ApiOperation(value = "根据设备档案批量删除维修记录", tags = {"维修记录" },  notes = "根据设备档案批量删除维修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqchecks/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emeqcheckService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqcheckMapping.toDomain(returnObject.body),'eam-EMEQCheck-Get')")
    @ApiOperation(value = "根据设备档案获取维修记录", tags = {"维修记录" },  notes = "根据设备档案获取维修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<EMEQCheckDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqcheck_id") String emeqcheck_id) {
        EMEQCheck domain = emeqcheckService.get(emeqcheck_id);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取维修记录草稿", tags = {"维修记录" },  notes = "根据设备档案获取维修记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqchecks/getdraft")
    public ResponseEntity<EMEQCheckDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMEQCheckDTO dto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqcheckMapping.toDto(emeqcheckService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查维修记录", tags = {"维修记录" },  notes = "根据设备档案检查维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqchecks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqcheckService.checkKey(emeqcheckMapping.toDomain(emeqcheckdto)));
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdto),'eam-EMEQCheck-Save')")
    @ApiOperation(value = "根据设备档案保存维修记录", tags = {"维修记录" },  notes = "根据设备档案保存维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqchecks/save")
    public ResponseEntity<EMEQCheckDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
        domain.setEquipid(emequip_id);
        emeqcheckService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqcheckMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdtos),'eam-EMEQCheck-Save')")
    @ApiOperation(value = "根据设备档案批量保存维修记录", tags = {"维修记录" },  notes = "根据设备档案批量保存维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqchecks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        List<EMEQCheck> domainlist=emeqcheckMapping.toDomain(emeqcheckdtos);
        for(EMEQCheck domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqcheckService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchCalendar-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据设备档案获取日历查询", tags = {"维修记录" } ,notes = "根据设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqchecks/fetchcalendar")
	public ResponseEntity<List<EMEQCheckDTO>> fetchEMEQCheckCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchCalendar(context) ;
        List<EMEQCheckDTO> list = emeqcheckMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchCalendar-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据设备档案查询日历查询", tags = {"维修记录" } ,notes = "根据设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqchecks/searchcalendar")
	public ResponseEntity<Page<EMEQCheckDTO>> searchEMEQCheckCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqcheckMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchDefault-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"维修记录" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqchecks/fetchdefault")
	public ResponseEntity<List<EMEQCheckDTO>> fetchEMEQCheckDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchDefault(context) ;
        List<EMEQCheckDTO> list = emeqcheckMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchDefault-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"维修记录" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqchecks/searchdefault")
	public ResponseEntity<Page<EMEQCheckDTO>> searchEMEQCheckDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqcheckMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdto),'eam-EMEQCheck-Create')")
    @ApiOperation(value = "根据班组设备档案建立维修记录", tags = {"维修记录" },  notes = "根据班组设备档案建立维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks")
    public ResponseEntity<EMEQCheckDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
        domain.setEquipid(emequip_id);
		emeqcheckService.create(domain);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdtos),'eam-EMEQCheck-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立维修记录", tags = {"维修记录" },  notes = "根据班组设备档案批量建立维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        List<EMEQCheck> domainlist=emeqcheckMapping.toDomain(emeqcheckdtos);
        for(EMEQCheck domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqcheckService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqcheck" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqcheckService.get(#emeqcheck_id),'eam-EMEQCheck-Update')")
    @ApiOperation(value = "根据班组设备档案更新维修记录", tags = {"维修记录" },  notes = "根据班组设备档案更新维修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<EMEQCheckDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqcheck_id") String emeqcheck_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqcheckid(emeqcheck_id);
		emeqcheckService.update(domain);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.getEmeqcheckByEntities(this.emeqcheckMapping.toDomain(#emeqcheckdtos)),'eam-EMEQCheck-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新维修记录", tags = {"维修记录" },  notes = "根据班组设备档案批量更新维修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        List<EMEQCheck> domainlist=emeqcheckMapping.toDomain(emeqcheckdtos);
        for(EMEQCheck domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqcheckService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.get(#emeqcheck_id),'eam-EMEQCheck-Remove')")
    @ApiOperation(value = "根据班组设备档案删除维修记录", tags = {"维修记录" },  notes = "根据班组设备档案删除维修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqcheck_id") String emeqcheck_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqcheckService.remove(emeqcheck_id));
    }

    @PreAuthorize("hasPermission(this.emeqcheckService.getEmeqcheckByIds(#ids),'eam-EMEQCheck-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除维修记录", tags = {"维修记录" },  notes = "根据班组设备档案批量删除维修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emeqcheckService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqcheckMapping.toDomain(returnObject.body),'eam-EMEQCheck-Get')")
    @ApiOperation(value = "根据班组设备档案获取维修记录", tags = {"维修记录" },  notes = "根据班组设备档案获取维修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/{emeqcheck_id}")
    public ResponseEntity<EMEQCheckDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqcheck_id") String emeqcheck_id) {
        EMEQCheck domain = emeqcheckService.get(emeqcheck_id);
        EMEQCheckDTO dto = emeqcheckMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取维修记录草稿", tags = {"维修记录" },  notes = "根据班组设备档案获取维修记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/getdraft")
    public ResponseEntity<EMEQCheckDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMEQCheckDTO dto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqcheckMapping.toDto(emeqcheckService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查维修记录", tags = {"维修记录" },  notes = "根据班组设备档案检查维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqcheckService.checkKey(emeqcheckMapping.toDomain(emeqcheckdto)));
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdto),'eam-EMEQCheck-Save')")
    @ApiOperation(value = "根据班组设备档案保存维修记录", tags = {"维修记录" },  notes = "根据班组设备档案保存维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/save")
    public ResponseEntity<EMEQCheckDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckDTO emeqcheckdto) {
        EMEQCheck domain = emeqcheckMapping.toDomain(emeqcheckdto);
        domain.setEquipid(emequip_id);
        emeqcheckService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqcheckMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqcheckMapping.toDomain(#emeqcheckdtos),'eam-EMEQCheck-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存维修记录", tags = {"维修记录" },  notes = "根据班组设备档案批量保存维修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQCheckDTO> emeqcheckdtos) {
        List<EMEQCheck> domainlist=emeqcheckMapping.toDomain(emeqcheckdtos);
        for(EMEQCheck domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqcheckService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchCalendar-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历查询", tags = {"维修记录" } ,notes = "根据班组设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/fetchcalendar")
	public ResponseEntity<List<EMEQCheckDTO>> fetchEMEQCheckCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchCalendar(context) ;
        List<EMEQCheckDTO> list = emeqcheckMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchCalendar-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历查询", tags = {"维修记录" } ,notes = "根据班组设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/searchcalendar")
	public ResponseEntity<Page<EMEQCheckDTO>> searchEMEQCheckCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqcheckMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchDefault-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"维修记录" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/fetchdefault")
	public ResponseEntity<List<EMEQCheckDTO>> fetchEMEQCheckDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchDefault(context) ;
        List<EMEQCheckDTO> list = emeqcheckMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQCheck-searchDefault-all') and hasPermission(#context,'eam-EMEQCheck-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"维修记录" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqchecks/searchdefault")
	public ResponseEntity<Page<EMEQCheckDTO>> searchEMEQCheckDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQCheckSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQCheck> domains = emeqcheckService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqcheckMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

