package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMP;
import cn.ibizlab.eam.core.eam_core.service.IEMEQMPService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMPSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备仪表" })
@RestController("WebApi-emeqmp")
@RequestMapping("")
public class EMEQMPResource {

    @Autowired
    public IEMEQMPService emeqmpService;

    @Autowired
    @Lazy
    public EMEQMPMapping emeqmpMapping;

    @PreAuthorize("hasPermission(this.emeqmpMapping.toDomain(#emeqmpdto),'eam-EMEQMP-Create')")
    @ApiOperation(value = "新建设备仪表", tags = {"设备仪表" },  notes = "新建设备仪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmps")
    public ResponseEntity<EMEQMPDTO> create(@Validated @RequestBody EMEQMPDTO emeqmpdto) {
        EMEQMP domain = emeqmpMapping.toDomain(emeqmpdto);
		emeqmpService.create(domain);
        EMEQMPDTO dto = emeqmpMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmpMapping.toDomain(#emeqmpdtos),'eam-EMEQMP-Create')")
    @ApiOperation(value = "批量新建设备仪表", tags = {"设备仪表" },  notes = "批量新建设备仪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQMPDTO> emeqmpdtos) {
        emeqmpService.createBatch(emeqmpMapping.toDomain(emeqmpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqmp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqmpService.get(#emeqmp_id),'eam-EMEQMP-Update')")
    @ApiOperation(value = "更新设备仪表", tags = {"设备仪表" },  notes = "更新设备仪表")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmps/{emeqmp_id}")
    public ResponseEntity<EMEQMPDTO> update(@PathVariable("emeqmp_id") String emeqmp_id, @RequestBody EMEQMPDTO emeqmpdto) {
		EMEQMP domain  = emeqmpMapping.toDomain(emeqmpdto);
        domain .setEmeqmpid(emeqmp_id);
		emeqmpService.update(domain );
		EMEQMPDTO dto = emeqmpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmpService.getEmeqmpByEntities(this.emeqmpMapping.toDomain(#emeqmpdtos)),'eam-EMEQMP-Update')")
    @ApiOperation(value = "批量更新设备仪表", tags = {"设备仪表" },  notes = "批量更新设备仪表")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQMPDTO> emeqmpdtos) {
        emeqmpService.updateBatch(emeqmpMapping.toDomain(emeqmpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqmpService.get(#emeqmp_id),'eam-EMEQMP-Remove')")
    @ApiOperation(value = "删除设备仪表", tags = {"设备仪表" },  notes = "删除设备仪表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmps/{emeqmp_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqmp_id") String emeqmp_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqmpService.remove(emeqmp_id));
    }

    @PreAuthorize("hasPermission(this.emeqmpService.getEmeqmpByIds(#ids),'eam-EMEQMP-Remove')")
    @ApiOperation(value = "批量删除设备仪表", tags = {"设备仪表" },  notes = "批量删除设备仪表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqmpService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqmpMapping.toDomain(returnObject.body),'eam-EMEQMP-Get')")
    @ApiOperation(value = "获取设备仪表", tags = {"设备仪表" },  notes = "获取设备仪表")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmps/{emeqmp_id}")
    public ResponseEntity<EMEQMPDTO> get(@PathVariable("emeqmp_id") String emeqmp_id) {
        EMEQMP domain = emeqmpService.get(emeqmp_id);
        EMEQMPDTO dto = emeqmpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备仪表草稿", tags = {"设备仪表" },  notes = "获取设备仪表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmps/getdraft")
    public ResponseEntity<EMEQMPDTO> getDraft(EMEQMPDTO dto) {
        EMEQMP domain = emeqmpMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmpMapping.toDto(emeqmpService.getDraft(domain)));
    }

    @ApiOperation(value = "检查设备仪表", tags = {"设备仪表" },  notes = "检查设备仪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQMPDTO emeqmpdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqmpService.checkKey(emeqmpMapping.toDomain(emeqmpdto)));
    }

    @PreAuthorize("hasPermission(this.emeqmpMapping.toDomain(#emeqmpdto),'eam-EMEQMP-Save')")
    @ApiOperation(value = "保存设备仪表", tags = {"设备仪表" },  notes = "保存设备仪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmps/save")
    public ResponseEntity<EMEQMPDTO> save(@RequestBody EMEQMPDTO emeqmpdto) {
        EMEQMP domain = emeqmpMapping.toDomain(emeqmpdto);
        emeqmpService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmpMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqmpMapping.toDomain(#emeqmpdtos),'eam-EMEQMP-Save')")
    @ApiOperation(value = "批量保存设备仪表", tags = {"设备仪表" },  notes = "批量保存设备仪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQMPDTO> emeqmpdtos) {
        emeqmpService.saveBatch(emeqmpMapping.toDomain(emeqmpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMP-searchDefault-all') and hasPermission(#context,'eam-EMEQMP-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备仪表" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqmps/fetchdefault")
	public ResponseEntity<List<EMEQMPDTO>> fetchDefault(EMEQMPSearchContext context) {
        Page<EMEQMP> domains = emeqmpService.searchDefault(context) ;
        List<EMEQMPDTO> list = emeqmpMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMP-searchDefault-all') and hasPermission(#context,'eam-EMEQMP-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备仪表" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqmps/searchdefault")
	public ResponseEntity<Page<EMEQMPDTO>> searchDefault(@RequestBody EMEQMPSearchContext context) {
        Page<EMEQMP> domains = emeqmpService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

