package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQSTopMoniDTO]
 */
@Data
public class EMEQSTopMoniDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [STIME]
     *
     */
    @JSONField(name = "stime")
    @JsonProperty("stime")
    @NotNull(message = "[停机时间]不允许为空!")
    private BigDecimal stime;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [SDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sdate" , format="yyyy-MM-dd")
    @JsonProperty("sdate")
    @NotNull(message = "[日期]不允许为空!")
    private Timestamp sdate;

    /**
     * 属性 [CAUSATION]
     *
     */
    @JSONField(name = "causation")
    @JsonProperty("causation")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String causation;

    /**
     * 属性 [STYPE]
     *
     */
    @JSONField(name = "stype")
    @JsonProperty("stype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stype;

    /**
     * 属性 [EMEQSTOPMONIID]
     *
     */
    @JSONField(name = "emeqstopmoniid")
    @JsonProperty("emeqstopmoniid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqstopmoniid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMEQSTOPMONINAME]
     *
     */
    @JSONField(name = "emeqstopmoniname")
    @JsonProperty("emeqstopmoniname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqstopmoniname;

    /**
     * 属性 [EMEQUIPNAME]
     *
     */
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emequipname;

    /**
     * 属性 [EMEQTYPENAME]
     *
     */
    @JSONField(name = "emeqtypename")
    @JsonProperty("emeqtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqtypename;

    /**
     * 属性 [EMEQSTOPNAME]
     *
     */
    @JSONField(name = "emeqstopname")
    @JsonProperty("emeqstopname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqstopname;

    /**
     * 属性 [EMEQUIPID]
     *
     */
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    @NotBlank(message = "[设备]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emequipid;

    /**
     * 属性 [EMEQSTOPID]
     *
     */
    @JSONField(name = "emeqstopid")
    @JsonProperty("emeqstopid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqstopid;

    /**
     * 属性 [EMEQTYPEID]
     *
     */
    @JSONField(name = "emeqtypeid")
    @JsonProperty("emeqtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqtypeid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [STIME]
     */
    public void setStime(BigDecimal  stime){
        this.stime = stime ;
        this.modify("stime",stime);
    }

    /**
     * 设置 [SDATE]
     */
    public void setSdate(Timestamp  sdate){
        this.sdate = sdate ;
        this.modify("sdate",sdate);
    }

    /**
     * 设置 [CAUSATION]
     */
    public void setCausation(String  causation){
        this.causation = causation ;
        this.modify("causation",causation);
    }

    /**
     * 设置 [STYPE]
     */
    public void setStype(String  stype){
        this.stype = stype ;
        this.modify("stype",stype);
    }

    /**
     * 设置 [EMEQSTOPMONINAME]
     */
    public void setEmeqstopmoniname(String  emeqstopmoniname){
        this.emeqstopmoniname = emeqstopmoniname ;
        this.modify("emeqstopmoniname",emeqstopmoniname);
    }

    /**
     * 设置 [EMEQUIPID]
     */
    public void setEmequipid(String  emequipid){
        this.emequipid = emequipid ;
        this.modify("emequipid",emequipid);
    }

    /**
     * 设置 [EMEQSTOPID]
     */
    public void setEmeqstopid(String  emeqstopid){
        this.emeqstopid = emeqstopid ;
        this.modify("emeqstopid",emeqstopid);
    }

    /**
     * 设置 [EMEQTYPEID]
     */
    public void setEmeqtypeid(String  emeqtypeid){
        this.emeqtypeid = emeqtypeid ;
        this.modify("emeqtypeid",emeqtypeid);
    }


}


