package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMResEmpDTO]
 */
@Data
public class EMResEmpDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @NotBlank(message = "[单价]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String price;

    /**
     * 属性 [EMRESEMPID]
     *
     */
    @JSONField(name = "emresempid")
    @JsonProperty("emresempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emresempid;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @NotBlank(message = "[总金额]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String amount;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "edate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    private Timestamp edate;

    /**
     * 属性 [RESID]
     *
     */
    @JSONField(name = "resid")
    @JsonProperty("resid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resid;

    /**
     * 属性 [RESNAME]
     *
     */
    @JSONField(name = "resname")
    @JsonProperty("resname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resname;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [PNUM]
     *
     */
    @JSONField(name = "pnum")
    @JsonProperty("pnum")
    private Double pnum;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [DATAFROM]
     *
     */
    @JSONField(name = "datafrom")
    @JsonProperty("datafrom")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String datafrom;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMRESEMPNAME]
     *
     */
    @JSONField(name = "emresempname")
    @JsonProperty("emresempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emresempname;

    /**
     * 属性 [BDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    private Timestamp bdate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [SNUM]
     *
     */
    @JSONField(name = "snum")
    @JsonProperty("snum")
    private Double snum;

    /**
     * 属性 [TEAM_D]
     *
     */
    @JSONField(name = "team_d")
    @JsonProperty("team_d")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamD;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [RESREFOBJNAME]
     *
     */
    @JSONField(name = "resrefobjname")
    @JsonProperty("resrefobjname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resrefobjname;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [RESREFOBJID]
     *
     */
    @JSONField(name = "resrefobjid")
    @JsonProperty("resrefobjid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resrefobjid;


    /**
     * 设置 [PRICE]
     */
    public void setPrice(String  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(String  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [EDATE]
     */
    public void setEdate(Timestamp  edate){
        this.edate = edate ;
        this.modify("edate",edate);
    }

    /**
     * 设置 [RESID]
     */
    public void setResid(String  resid){
        this.resid = resid ;
        this.modify("resid",resid);
    }

    /**
     * 设置 [RESNAME]
     */
    public void setResname(String  resname){
        this.resname = resname ;
        this.modify("resname",resname);
    }

    /**
     * 设置 [PNUM]
     */
    public void setPnum(Double  pnum){
        this.pnum = pnum ;
        this.modify("pnum",pnum);
    }

    /**
     * 设置 [DATAFROM]
     */
    public void setDatafrom(String  datafrom){
        this.datafrom = datafrom ;
        this.modify("datafrom",datafrom);
    }

    /**
     * 设置 [EMRESEMPNAME]
     */
    public void setEmresempname(String  emresempname){
        this.emresempname = emresempname ;
        this.modify("emresempname",emresempname);
    }

    /**
     * 设置 [BDATE]
     */
    public void setBdate(Timestamp  bdate){
        this.bdate = bdate ;
        this.modify("bdate",bdate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [SNUM]
     */
    public void setSnum(Double  snum){
        this.snum = snum ;
        this.modify("snum",snum);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [RESREFOBJID]
     */
    public void setResrefobjid(String  resrefobjid){
        this.resrefobjid = resrefobjid ;
        this.modify("resrefobjid",resrefobjid);
    }


}


