package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMWPListDTO]
 */
@Data
public class EMWPListDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [APPRDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "apprdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    private Timestamp apprdate;

    /**
     * 属性 [ADATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "adate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    private Timestamp adate;

    /**
     * 属性 [WPLISTIDS]
     *
     */
    @JSONField(name = "wplistids")
    @JsonProperty("wplistids")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wplistids;

    /**
     * 属性 [M3Q]
     *
     */
    @JSONField(name = "m3q")
    @JsonProperty("m3q")
    @NotNull(message = "[经理指定询价数]不允许为空!")
    private Integer m3q;

    /**
     * 属性 [NEED3Q]
     *
     */
    @JSONField(name = "need3q")
    @JsonProperty("need3q")
    private Integer need3q;

    /**
     * 属性 [EMWPLISTNAME]
     *
     */
    @JSONField(name = "emwplistname")
    @JsonProperty("emwplistname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emwplistname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [HDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "hdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("hdate")
    private Timestamp hdate;

    /**
     * 属性 [EQUIPS]
     *
     */
    @JSONField(name = "equips")
    @JsonProperty("equips")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String equips;

    /**
     * 属性 [QCNT]
     *
     */
    @JSONField(name = "qcnt")
    @JsonProperty("qcnt")
    private Integer qcnt;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [ITEMDESC]
     *
     */
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String itemdesc;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMWPLISTID]
     *
     */
    @JSONField(name = "emwplistid")
    @JsonProperty("emwplistid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emwplistid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [WPLISTTYPE]
     *
     */
    @JSONField(name = "wplisttype")
    @JsonProperty("wplisttype")
    @NotBlank(message = "[请购类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wplisttype;

    /**
     * 属性 [ITEMANDITEMDESC]
     *
     */
    @JSONField(name = "itemanditemdesc")
    @JsonProperty("itemanditemdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String itemanditemdesc;

    /**
     * 属性 [COSTAMOUNT]
     *
     */
    @JSONField(name = "costamount")
    @JsonProperty("costamount")
    private Double costamount;

    /**
     * 属性 [USETO]
     *
     */
    @JSONField(name = "useto")
    @JsonProperty("useto")
    @NotBlank(message = "[用途]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String useto;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [ISCANCEL]
     *
     */
    @JSONField(name = "iscancel")
    @JsonProperty("iscancel")
    private Integer iscancel;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [LASTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastdate")
    private Timestamp lastdate;

    /**
     * 属性 [APPRDESC]
     *
     */
    @JSONField(name = "apprdesc")
    @JsonProperty("apprdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String apprdesc;

    /**
     * 属性 [WPLISTSTATE]
     *
     */
    @JSONField(name = "wpliststate")
    @JsonProperty("wpliststate")
    private Integer wpliststate;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [WPLISTDP]
     *
     */
    @JSONField(name = "wplistdp")
    @JsonProperty("wplistdp")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wplistdp;

    /**
     * 属性 [ASUM]
     *
     */
    @JSONField(name = "asum")
    @JsonProperty("asum")
    private Double asum;

    /**
     * 属性 [WPLISTINFO]
     *
     */
    @JSONField(name = "wplistinfo")
    @JsonProperty("wplistinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wplistinfo;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [PAMOUNT]
     *
     */
    @JSONField(name = "pamount")
    @JsonProperty("pamount")
    private Double pamount;

    /**
     * 属性 [DELTYPE]
     *
     */
    @JSONField(name = "deltype")
    @JsonProperty("deltype")
    private Integer deltype;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [NO3Q]
     *
     */
    @JSONField(name = "no3q")
    @JsonProperty("no3q")
    private Integer no3q;

    /**
     * 属性 [ITEMCODE]
     *
     */
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemcode;

    /**
     * 属性 [WPLISTCOSTNAME]
     *
     */
    @JSONField(name = "wplistcostname")
    @JsonProperty("wplistcostname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wplistcostname;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [EMSERVICENAME]
     *
     */
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicename;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itembtypeid;

    /**
     * 属性 [ITEMNAME_SHOW]
     *
     */
    @JSONField(name = "itemname_show")
    @JsonProperty("itemname_show")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemnameShow;

    /**
     * 属性 [AVGPRICE]
     *
     */
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    private Double avgprice;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [TEAMNAME]
     *
     */
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitid;

    /**
     * 属性 [TEAMID]
     *
     */
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String teamid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [EMSERVICEID]
     *
     */
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceid;

    /**
     * 属性 [WPLISTCOSTID]
     *
     */
    @JSONField(name = "wplistcostid")
    @JsonProperty("wplistcostid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wplistcostid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [AEMPID]
     *
     */
    @JSONField(name = "aempid")
    @JsonProperty("aempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aempid;

    /**
     * 属性 [AEMPNAME]
     *
     */
    @JSONField(name = "aempname")
    @JsonProperty("aempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String aempname;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptid;

    /**
     * 属性 [DEPTNAME]
     *
     */
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String deptname;

    /**
     * 属性 [APPREMPID]
     *
     */
    @JSONField(name = "apprempid")
    @JsonProperty("apprempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String apprempid;

    /**
     * 属性 [APPREMPNAME]
     *
     */
    @JSONField(name = "apprempname")
    @JsonProperty("apprempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String apprempname;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rempname;


    /**
     * 设置 [APPRDATE]
     */
    public void setApprdate(Timestamp  apprdate){
        this.apprdate = apprdate ;
        this.modify("apprdate",apprdate);
    }

    /**
     * 设置 [ADATE]
     */
    public void setAdate(Timestamp  adate){
        this.adate = adate ;
        this.modify("adate",adate);
    }

    /**
     * 设置 [M3Q]
     */
    public void setM3q(Integer  m3q){
        this.m3q = m3q ;
        this.modify("m3q",m3q);
    }

    /**
     * 设置 [NEED3Q]
     */
    public void setNeed3q(Integer  need3q){
        this.need3q = need3q ;
        this.modify("need3q",need3q);
    }

    /**
     * 设置 [EMWPLISTNAME]
     */
    public void setEmwplistname(String  emwplistname){
        this.emwplistname = emwplistname ;
        this.modify("emwplistname",emwplistname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [HDATE]
     */
    public void setHdate(Timestamp  hdate){
        this.hdate = hdate ;
        this.modify("hdate",hdate);
    }

    /**
     * 设置 [EQUIPS]
     */
    public void setEquips(String  equips){
        this.equips = equips ;
        this.modify("equips",equips);
    }

    /**
     * 设置 [ITEMDESC]
     */
    public void setItemdesc(String  itemdesc){
        this.itemdesc = itemdesc ;
        this.modify("itemdesc",itemdesc);
    }

    /**
     * 设置 [WPLISTTYPE]
     */
    public void setWplisttype(String  wplisttype){
        this.wplisttype = wplisttype ;
        this.modify("wplisttype",wplisttype);
    }

    /**
     * 设置 [ITEMANDITEMDESC]
     */
    public void setItemanditemdesc(String  itemanditemdesc){
        this.itemanditemdesc = itemanditemdesc ;
        this.modify("itemanditemdesc",itemanditemdesc);
    }

    /**
     * 设置 [USETO]
     */
    public void setUseto(String  useto){
        this.useto = useto ;
        this.modify("useto",useto);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [ISCANCEL]
     */
    public void setIscancel(Integer  iscancel){
        this.iscancel = iscancel ;
        this.modify("iscancel",iscancel);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [LASTDATE]
     */
    public void setLastdate(Timestamp  lastdate){
        this.lastdate = lastdate ;
        this.modify("lastdate",lastdate);
    }

    /**
     * 设置 [APPRDESC]
     */
    public void setApprdesc(String  apprdesc){
        this.apprdesc = apprdesc ;
        this.modify("apprdesc",apprdesc);
    }

    /**
     * 设置 [WPLISTSTATE]
     */
    public void setWpliststate(Integer  wpliststate){
        this.wpliststate = wpliststate ;
        this.modify("wpliststate",wpliststate);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [WPLISTDP]
     */
    public void setWplistdp(String  wplistdp){
        this.wplistdp = wplistdp ;
        this.modify("wplistdp",wplistdp);
    }

    /**
     * 设置 [ASUM]
     */
    public void setAsum(Double  asum){
        this.asum = asum ;
        this.modify("asum",asum);
    }

    /**
     * 设置 [DELTYPE]
     */
    public void setDeltype(Integer  deltype){
        this.deltype = deltype ;
        this.modify("deltype",deltype);
    }

    /**
     * 设置 [TEAMID]
     */
    public void setTeamid(String  teamid){
        this.teamid = teamid ;
        this.modify("teamid",teamid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [EMSERVICEID]
     */
    public void setEmserviceid(String  emserviceid){
        this.emserviceid = emserviceid ;
        this.modify("emserviceid",emserviceid);
    }

    /**
     * 设置 [WPLISTCOSTID]
     */
    public void setWplistcostid(String  wplistcostid){
        this.wplistcostid = wplistcostid ;
        this.modify("wplistcostid",wplistcostid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [AEMPID]
     */
    public void setAempid(String  aempid){
        this.aempid = aempid ;
        this.modify("aempid",aempid);
    }

    /**
     * 设置 [DEPTID]
     */
    public void setDeptid(String  deptid){
        this.deptid = deptid ;
        this.modify("deptid",deptid);
    }

    /**
     * 设置 [APPREMPID]
     */
    public void setApprempid(String  apprempid){
        this.apprempid = apprempid ;
        this.modify("apprempid",apprempid);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }


}


