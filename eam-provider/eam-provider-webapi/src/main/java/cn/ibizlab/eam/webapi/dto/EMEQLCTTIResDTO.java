package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQLCTTIResDTO]
 */
@Data
public class EMEQLCTTIResDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [TIRESSTATE]
     *
     */
    @JSONField(name = "tiresstate")
    @JsonProperty("tiresstate")
    @NotBlank(message = "[轮胎状态]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String tiresstate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [PAR]
     *
     */
    @JSONField(name = "par")
    @JsonProperty("par")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String par;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String amount;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [PICPARAMS]
     *
     */
    @JSONField(name = "picparams")
    @JsonProperty("picparams")
    @NotBlank(message = "[图形8*8=11-88]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String picparams;

    /**
     * 属性 [VALVE]
     *
     */
    @JSONField(name = "valve")
    @JsonProperty("valve")
    private Integer valve;

    /**
     * 属性 [REPLACEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "replacedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("replacedate")
    private Timestamp replacedate;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EQMODELCODE]
     *
     */
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqmodelcode;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [NEWOLDFLAG]
     *
     */
    @JSONField(name = "newoldflag")
    @JsonProperty("newoldflag")
    @NotBlank(message = "[新旧标志]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String newoldflag;

    /**
     * 属性 [CHANGP]
     *
     */
    @JSONField(name = "changp")
    @JsonProperty("changp")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String changp;

    /**
     * 属性 [SYSTEMPARAM]
     *
     */
    @JSONField(name = "systemparam")
    @JsonProperty("systemparam")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String systemparam;

    /**
     * 属性 [REPLACEREASON]
     *
     */
    @JSONField(name = "replacereason")
    @JsonProperty("replacereason")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String replacereason;

    /**
     * 属性 [LCTDESC]
     *
     */
    @JSONField(name = "lctdesc")
    @JsonProperty("lctdesc")
    @NotBlank(message = "[轮胎备注]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lctdesc;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [TIRESTYPE]
     *
     */
    @JSONField(name = "tirestype")
    @JsonProperty("tirestype")
    @NotBlank(message = "[轮胎车型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String tirestype;

    /**
     * 属性 [LCTTIRESINFO]
     *
     */
    @JSONField(name = "lcttiresinfo")
    @JsonProperty("lcttiresinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lcttiresinfo;

    /**
     * 属性 [HAVEINNER]
     *
     */
    @JSONField(name = "haveinner")
    @JsonProperty("haveinner")
    @NotNull(message = "[有内胎]不允许为空!")
    private Integer haveinner;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [MCCODE]
     *
     */
    @JSONField(name = "mccode")
    @JsonProperty("mccode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mccode;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [MSERVICENAME]
     *
     */
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mservicename;

    /**
     * 属性 [EQLOCATIONINFO]
     *
     */
    @JSONField(name = "eqlocationinfo")
    @JsonProperty("eqlocationinfo")
    @NotBlank(message = "[位置信息]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationinfo;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [EMEQLOCATIONID]
     *
     */
    @JSONField(name = "emeqlocationid")
    @JsonProperty("emeqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqlocationid;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [MSERVICEID]
     *
     */
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mserviceid;


    /**
     * 设置 [TIRESSTATE]
     */
    public void setTiresstate(String  tiresstate){
        this.tiresstate = tiresstate ;
        this.modify("tiresstate",tiresstate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PAR]
     */
    public void setPar(String  par){
        this.par = par ;
        this.modify("par",par);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(String  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [PICPARAMS]
     */
    public void setPicparams(String  picparams){
        this.picparams = picparams ;
        this.modify("picparams",picparams);
    }

    /**
     * 设置 [VALVE]
     */
    public void setValve(Integer  valve){
        this.valve = valve ;
        this.modify("valve",valve);
    }

    /**
     * 设置 [EQMODELCODE]
     */
    public void setEqmodelcode(String  eqmodelcode){
        this.eqmodelcode = eqmodelcode ;
        this.modify("eqmodelcode",eqmodelcode);
    }

    /**
     * 设置 [NEWOLDFLAG]
     */
    public void setNewoldflag(String  newoldflag){
        this.newoldflag = newoldflag ;
        this.modify("newoldflag",newoldflag);
    }

    /**
     * 设置 [CHANGP]
     */
    public void setChangp(String  changp){
        this.changp = changp ;
        this.modify("changp",changp);
    }

    /**
     * 设置 [SYSTEMPARAM]
     */
    public void setSystemparam(String  systemparam){
        this.systemparam = systemparam ;
        this.modify("systemparam",systemparam);
    }

    /**
     * 设置 [LCTDESC]
     */
    public void setLctdesc(String  lctdesc){
        this.lctdesc = lctdesc ;
        this.modify("lctdesc",lctdesc);
    }

    /**
     * 设置 [TIRESTYPE]
     */
    public void setTirestype(String  tirestype){
        this.tirestype = tirestype ;
        this.modify("tirestype",tirestype);
    }

    /**
     * 设置 [HAVEINNER]
     */
    public void setHaveinner(Integer  haveinner){
        this.haveinner = haveinner ;
        this.modify("haveinner",haveinner);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [MSERVICEID]
     */
    public void setMserviceid(String  mserviceid){
        this.mserviceid = mserviceid ;
        this.modify("mserviceid",mserviceid);
    }


}


