package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[PFTeamDTO]
 */
@Data
public class PFTeamDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TEAMCODE]
     *
     */
    @JSONField(name = "teamcode")
    @JsonProperty("teamcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String teamcode;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PFTEAMID]
     *
     */
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfteamid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [TEAMTYPEID]
     *
     */
    @JSONField(name = "teamtypeid")
    @JsonProperty("teamtypeid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String teamtypeid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [TEAMFN]
     *
     */
    @JSONField(name = "teamfn")
    @JsonProperty("teamfn")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String teamfn;

    /**
     * 属性 [PFTEAMNAME]
     *
     */
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfteamname;

    /**
     * 属性 [TEAMINFO]
     *
     */
    @JSONField(name = "teaminfo")
    @JsonProperty("teaminfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teaminfo;

    /**
     * 属性 [TEAMCAPACITY]
     *
     */
    @JSONField(name = "teamcapacity")
    @JsonProperty("teamcapacity")
    private Integer teamcapacity;


    /**
     * 设置 [TEAMCODE]
     */
    public void setTeamcode(String  teamcode){
        this.teamcode = teamcode ;
        this.modify("teamcode",teamcode);
    }

    /**
     * 设置 [TEAMTYPEID]
     */
    public void setTeamtypeid(String  teamtypeid){
        this.teamtypeid = teamtypeid ;
        this.modify("teamtypeid",teamtypeid);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [TEAMFN]
     */
    public void setTeamfn(String  teamfn){
        this.teamfn = teamfn ;
        this.modify("teamfn",teamfn);
    }

    /**
     * 设置 [PFTEAMNAME]
     */
    public void setPfteamname(String  pfteamname){
        this.pfteamname = pfteamname ;
        this.modify("pfteamname",pfteamname);
    }

    /**
     * 设置 [TEAMCAPACITY]
     */
    public void setTeamcapacity(Integer  teamcapacity){
        this.teamcapacity = teamcapacity ;
        this.modify("teamcapacity",teamcapacity);
    }


}


