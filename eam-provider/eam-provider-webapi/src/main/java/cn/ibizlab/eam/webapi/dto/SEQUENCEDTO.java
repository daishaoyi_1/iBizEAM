package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[SEQUENCEDTO]
 */
@Data
public class SEQUENCEDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SEQUENCENAME]
     *
     */
    @JSONField(name = "sequencename")
    @JsonProperty("sequencename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sequencename;

    /**
     * 属性 [SEQUENCEID]
     *
     */
    @JSONField(name = "sequenceid")
    @JsonProperty("sequenceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sequenceid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    @NotBlank(message = "[实体名]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String code;

    /**
     * 属性 [IMPLEMENTATION]
     *
     */
    @JSONField(name = "implementation")
    @JsonProperty("implementation")
    @NotBlank(message = "[实现]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String implementation;

    /**
     * 属性 [PREFIX]
     *
     */
    @JSONField(name = "prefix")
    @JsonProperty("prefix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String prefix;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @NotNull(message = "[逻辑有效标志]不允许为空!")
    private Integer enable;

    /**
     * 属性 [SUFFIX]
     *
     */
    @JSONField(name = "suffix")
    @JsonProperty("suffix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String suffix;

    /**
     * 属性 [NUMINCREMENT]
     *
     */
    @JSONField(name = "numincrement")
    @JsonProperty("numincrement")
    @NotNull(message = "[序号增长]不允许为空!")
    private Integer numincrement;

    /**
     * 属性 [PADDING]
     *
     */
    @JSONField(name = "padding")
    @JsonProperty("padding")
    @NotNull(message = "[序号长度]不允许为空!")
    private Integer padding;

    /**
     * 属性 [NUMNEXT]
     *
     */
    @JSONField(name = "numnext")
    @JsonProperty("numnext")
    @NotNull(message = "[下一号码]不允许为空!")
    private Integer numnext;


    /**
     * 设置 [SEQUENCENAME]
     */
    public void setSequencename(String  sequencename){
        this.sequencename = sequencename ;
        this.modify("sequencename",sequencename);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [IMPLEMENTATION]
     */
    public void setImplementation(String  implementation){
        this.implementation = implementation ;
        this.modify("implementation",implementation);
    }

    /**
     * 设置 [PREFIX]
     */
    public void setPrefix(String  prefix){
        this.prefix = prefix ;
        this.modify("prefix",prefix);
    }

    /**
     * 设置 [ENABLE]
     */
    public void setEnable(Integer  enable){
        this.enable = enable ;
        this.modify("enable",enable);
    }

    /**
     * 设置 [SUFFIX]
     */
    public void setSuffix(String  suffix){
        this.suffix = suffix ;
        this.modify("suffix",suffix);
    }

    /**
     * 设置 [NUMINCREMENT]
     */
    public void setNumincrement(Integer  numincrement){
        this.numincrement = numincrement ;
        this.modify("numincrement",numincrement);
    }

    /**
     * 设置 [PADDING]
     */
    public void setPadding(Integer  padding){
        this.padding = padding ;
        this.modify("padding",padding);
    }

    /**
     * 设置 [NUMNEXT]
     */
    public void setNumnext(Integer  numnext){
        this.numnext = numnext ;
        this.modify("numnext",numnext);
    }


}


