package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSTopMoni;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSTopMoniService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSTopMoniSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备停机监控明细" })
@RestController("WebApi-emeqstopmoni")
@RequestMapping("")
public class EMEQSTopMoniResource {

    @Autowired
    public IEMEQSTopMoniService emeqstopmoniService;

    @Autowired
    @Lazy
    public EMEQSTopMoniMapping emeqstopmoniMapping;

    @PreAuthorize("hasPermission(this.emeqstopmoniMapping.toDomain(#emeqstopmonidto),'eam-EMEQSTopMoni-Create')")
    @ApiOperation(value = "新建设备停机监控明细", tags = {"设备停机监控明细" },  notes = "新建设备停机监控明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstopmonis")
    public ResponseEntity<EMEQSTopMoniDTO> create(@Validated @RequestBody EMEQSTopMoniDTO emeqstopmonidto) {
        EMEQSTopMoni domain = emeqstopmoniMapping.toDomain(emeqstopmonidto);
		emeqstopmoniService.create(domain);
        EMEQSTopMoniDTO dto = emeqstopmoniMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqstopmoniMapping.toDomain(#emeqstopmonidtos),'eam-EMEQSTopMoni-Create')")
    @ApiOperation(value = "批量新建设备停机监控明细", tags = {"设备停机监控明细" },  notes = "批量新建设备停机监控明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstopmonis/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQSTopMoniDTO> emeqstopmonidtos) {
        emeqstopmoniService.createBatch(emeqstopmoniMapping.toDomain(emeqstopmonidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqstopmoni" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqstopmoniService.get(#emeqstopmoni_id),'eam-EMEQSTopMoni-Update')")
    @ApiOperation(value = "更新设备停机监控明细", tags = {"设备停机监控明细" },  notes = "更新设备停机监控明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqstopmonis/{emeqstopmoni_id}")
    public ResponseEntity<EMEQSTopMoniDTO> update(@PathVariable("emeqstopmoni_id") String emeqstopmoni_id, @RequestBody EMEQSTopMoniDTO emeqstopmonidto) {
		EMEQSTopMoni domain  = emeqstopmoniMapping.toDomain(emeqstopmonidto);
        domain .setEmeqstopmoniid(emeqstopmoni_id);
		emeqstopmoniService.update(domain );
		EMEQSTopMoniDTO dto = emeqstopmoniMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqstopmoniService.getEmeqstopmoniByEntities(this.emeqstopmoniMapping.toDomain(#emeqstopmonidtos)),'eam-EMEQSTopMoni-Update')")
    @ApiOperation(value = "批量更新设备停机监控明细", tags = {"设备停机监控明细" },  notes = "批量更新设备停机监控明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqstopmonis/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQSTopMoniDTO> emeqstopmonidtos) {
        emeqstopmoniService.updateBatch(emeqstopmoniMapping.toDomain(emeqstopmonidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqstopmoniService.get(#emeqstopmoni_id),'eam-EMEQSTopMoni-Remove')")
    @ApiOperation(value = "删除设备停机监控明细", tags = {"设备停机监控明细" },  notes = "删除设备停机监控明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqstopmonis/{emeqstopmoni_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqstopmoni_id") String emeqstopmoni_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqstopmoniService.remove(emeqstopmoni_id));
    }

    @PreAuthorize("hasPermission(this.emeqstopmoniService.getEmeqstopmoniByIds(#ids),'eam-EMEQSTopMoni-Remove')")
    @ApiOperation(value = "批量删除设备停机监控明细", tags = {"设备停机监控明细" },  notes = "批量删除设备停机监控明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqstopmonis/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqstopmoniService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqstopmoniMapping.toDomain(returnObject.body),'eam-EMEQSTopMoni-Get')")
    @ApiOperation(value = "获取设备停机监控明细", tags = {"设备停机监控明细" },  notes = "获取设备停机监控明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqstopmonis/{emeqstopmoni_id}")
    public ResponseEntity<EMEQSTopMoniDTO> get(@PathVariable("emeqstopmoni_id") String emeqstopmoni_id) {
        EMEQSTopMoni domain = emeqstopmoniService.get(emeqstopmoni_id);
        EMEQSTopMoniDTO dto = emeqstopmoniMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备停机监控明细草稿", tags = {"设备停机监控明细" },  notes = "获取设备停机监控明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqstopmonis/getdraft")
    public ResponseEntity<EMEQSTopMoniDTO> getDraft(EMEQSTopMoniDTO dto) {
        EMEQSTopMoni domain = emeqstopmoniMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqstopmoniMapping.toDto(emeqstopmoniService.getDraft(domain)));
    }

    @ApiOperation(value = "检查设备停机监控明细", tags = {"设备停机监控明细" },  notes = "检查设备停机监控明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstopmonis/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQSTopMoniDTO emeqstopmonidto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqstopmoniService.checkKey(emeqstopmoniMapping.toDomain(emeqstopmonidto)));
    }

    @PreAuthorize("hasPermission(this.emeqstopmoniMapping.toDomain(#emeqstopmonidto),'eam-EMEQSTopMoni-Save')")
    @ApiOperation(value = "保存设备停机监控明细", tags = {"设备停机监控明细" },  notes = "保存设备停机监控明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstopmonis/save")
    public ResponseEntity<EMEQSTopMoniDTO> save(@RequestBody EMEQSTopMoniDTO emeqstopmonidto) {
        EMEQSTopMoni domain = emeqstopmoniMapping.toDomain(emeqstopmonidto);
        emeqstopmoniService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqstopmoniMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqstopmoniMapping.toDomain(#emeqstopmonidtos),'eam-EMEQSTopMoni-Save')")
    @ApiOperation(value = "批量保存设备停机监控明细", tags = {"设备停机监控明细" },  notes = "批量保存设备停机监控明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstopmonis/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQSTopMoniDTO> emeqstopmonidtos) {
        emeqstopmoniService.saveBatch(emeqstopmoniMapping.toDomain(emeqstopmonidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSTopMoni-searchDefault-all') and hasPermission(#context,'eam-EMEQSTopMoni-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备停机监控明细" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqstopmonis/fetchdefault")
	public ResponseEntity<List<EMEQSTopMoniDTO>> fetchDefault(EMEQSTopMoniSearchContext context) {
        Page<EMEQSTopMoni> domains = emeqstopmoniService.searchDefault(context) ;
        List<EMEQSTopMoniDTO> list = emeqstopmoniMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSTopMoni-searchDefault-all') and hasPermission(#context,'eam-EMEQSTopMoni-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备停机监控明细" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqstopmonis/searchdefault")
	public ResponseEntity<Page<EMEQSTopMoniDTO>> searchDefault(@RequestBody EMEQSTopMoniSearchContext context) {
        Page<EMEQSTopMoni> domains = emeqstopmoniService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqstopmoniMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

