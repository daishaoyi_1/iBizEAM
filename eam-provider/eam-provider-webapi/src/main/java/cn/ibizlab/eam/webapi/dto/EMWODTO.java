package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMWODTO]
 */
@Data
public class EMWODTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WOSTATE]
     *
     */
    @JSONField(name = "wostate")
    @JsonProperty("wostate")
    private Integer wostate;

    /**
     * 属性 [VRATE]
     *
     */
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    private Double vrate;

    /**
     * 属性 [MDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "mdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    private Timestamp mdate;

    /**
     * 属性 [WOTYPE]
     *
     */
    @JSONField(name = "wotype")
    @JsonProperty("wotype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wotype;

    /**
     * 属性 [CPLANFLAG]
     *
     */
    @JSONField(name = "cplanflag")
    @JsonProperty("cplanflag")
    private Integer cplanflag;

    /**
     * 属性 [WAITBUY]
     *
     */
    @JSONField(name = "waitbuy")
    @JsonProperty("waitbuy")
    private Double waitbuy;

    /**
     * 属性 [EMWOTYPE]
     *
     */
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @NotBlank(message = "[工单种类]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emwotype;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [PREFEE]
     *
     */
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    private Double prefee;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;

    /**
     * 属性 [WAITMODI]
     *
     */
    @JSONField(name = "waitmodi")
    @JsonProperty("waitmodi")
    private Double waitmodi;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMWOID]
     *
     */
    @JSONField(name = "emwoid")
    @JsonProperty("emwoid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emwoid;

    /**
     * 属性 [MFEE]
     *
     */
    @JSONField(name = "mfee")
    @JsonProperty("mfee")
    private Double mfee;

    /**
     * 属性 [REGIONENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "regionenddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionenddate")
    private Timestamp regionenddate;

    /**
     * 属性 [WOGROUP]
     *
     */
    @JSONField(name = "wogroup")
    @JsonProperty("wogroup")
    @NotBlank(message = "[工单分组]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wogroup;

    /**
     * 属性 [WORKLENGTH]
     *
     */
    @JSONField(name = "worklength")
    @JsonProperty("worklength")
    private Double worklength;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [WOTEAM]
     *
     */
    @JSONField(name = "woteam")
    @JsonProperty("woteam")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String woteam;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EXPIREDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expiredate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expiredate")
    private Timestamp expiredate;

    /**
     * 属性 [YXCB]
     *
     */
    @JSONField(name = "yxcb")
    @JsonProperty("yxcb")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String yxcb;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String priority;

    /**
     * 属性 [WOINFO]
     *
     */
    @JSONField(name = "woinfo")
    @JsonProperty("woinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String woinfo;

    /**
     * 属性 [NVAL]
     *
     */
    @JSONField(name = "nval")
    @JsonProperty("nval")
    private Double nval;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EMWONAME]
     *
     */
    @JSONField(name = "emwoname")
    @JsonProperty("emwoname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emwoname;

    /**
     * 属性 [EQSTOPLENGTH]
     *
     */
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;

    /**
     * 属性 [WODATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "wodate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("wodate")
    @NotNull(message = "[执行日期]不允许为空!")
    private Timestamp wodate;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [WRESULT]
     *
     */
    @JSONField(name = "wresult")
    @JsonProperty("wresult")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String wresult;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [WODESC]
     *
     */
    @JSONField(name = "wodesc")
    @JsonProperty("wodesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String wodesc;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [VAL]
     *
     */
    @JSONField(name = "val")
    @JsonProperty("val")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String val;

    /**
     * 属性 [ARCHIVE]
     *
     */
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String archive;

    /**
     * 属性 [REGIONBEGINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "regionbegindate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    private Timestamp regionbegindate;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rservicename;

    /**
     * 属性 [WOORINAME]
     *
     */
    @JSONField(name = "wooriname")
    @JsonProperty("wooriname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wooriname;

    /**
     * 属性 [EQUIPCODE]
     *
     */
    @JSONField(name = "equipcode")
    @JsonProperty("equipcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipcode;

    /**
     * 属性 [RFOACNAME]
     *
     */
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfoacname;

    /**
     * 属性 [RFOMONAME]
     *
     */
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfomoname;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String acclassname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [WOPNAME]
     *
     */
    @JSONField(name = "wopname")
    @JsonProperty("wopname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wopname;

    /**
     * 属性 [RFODENAME]
     *
     */
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfodename;

    /**
     * 属性 [RTEAMNAME]
     *
     */
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rteamname;

    /**
     * 属性 [STYPE]
     *
     */
    @JSONField(name = "stype")
    @JsonProperty("stype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stype;

    /**
     * 属性 [DPTYPE]
     *
     */
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dptype;

    /**
     * 属性 [DPNAME]
     *
     */
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String dpname;

    /**
     * 属性 [WOORITYPE]
     *
     */
    @JSONField(name = "wooritype")
    @JsonProperty("wooritype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wooritype;

    /**
     * 属性 [SNAME]
     *
     */
    @JSONField(name = "sname")
    @JsonProperty("sname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [RFOCANAME]
     *
     */
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfocaname;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rserviceid;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String acclassid;

    /**
     * 属性 [WOPID]
     *
     */
    @JSONField(name = "wopid")
    @JsonProperty("wopid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wopid;

    /**
     * 属性 [RFOACID]
     *
     */
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfoacid;

    /**
     * 属性 [RFODEID]
     *
     */
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfodeid;

    /**
     * 属性 [WOORIID]
     *
     */
    @JSONField(name = "wooriid")
    @JsonProperty("wooriid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wooriid;

    /**
     * 属性 [RFOMOID]
     *
     */
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfomoid;

    /**
     * 属性 [RTEAMID]
     *
     */
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rteamid;

    /**
     * 属性 [DPID]
     *
     */
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dpid;

    /**
     * 属性 [RFOCAID]
     *
     */
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfocaid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [RDEPTID]
     *
     */
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rdeptid;

    /**
     * 属性 [RDEPTNAME]
     *
     */
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rdeptname;

    /**
     * 属性 [WPERSONID]
     *
     */
    @JSONField(name = "wpersonid")
    @JsonProperty("wpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wpersonid;

    /**
     * 属性 [WPERSONNAME]
     *
     */
    @JSONField(name = "wpersonname")
    @JsonProperty("wpersonname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wpersonname;

    /**
     * 属性 [RECVPERSONID]
     *
     */
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recvpersonid;

    /**
     * 属性 [RECVPERSONNAME]
     *
     */
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String recvpersonname;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rempname;

    /**
     * 属性 [MPERSONID]
     *
     */
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mpersonid;

    /**
     * 属性 [MPERSONNAME]
     *
     */
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mpersonname;


    /**
     * 设置 [WOSTATE]
     */
    public void setWostate(Integer  wostate){
        this.wostate = wostate ;
        this.modify("wostate",wostate);
    }

    /**
     * 设置 [VRATE]
     */
    public void setVrate(Double  vrate){
        this.vrate = vrate ;
        this.modify("vrate",vrate);
    }

    /**
     * 设置 [MDATE]
     */
    public void setMdate(Timestamp  mdate){
        this.mdate = mdate ;
        this.modify("mdate",mdate);
    }

    /**
     * 设置 [WOTYPE]
     */
    public void setWotype(String  wotype){
        this.wotype = wotype ;
        this.modify("wotype",wotype);
    }

    /**
     * 设置 [CPLANFLAG]
     */
    public void setCplanflag(Integer  cplanflag){
        this.cplanflag = cplanflag ;
        this.modify("cplanflag",cplanflag);
    }

    /**
     * 设置 [WAITBUY]
     */
    public void setWaitbuy(Double  waitbuy){
        this.waitbuy = waitbuy ;
        this.modify("waitbuy",waitbuy);
    }

    /**
     * 设置 [EMWOTYPE]
     */
    public void setEmwotype(String  emwotype){
        this.emwotype = emwotype ;
        this.modify("emwotype",emwotype);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [PREFEE]
     */
    public void setPrefee(Double  prefee){
        this.prefee = prefee ;
        this.modify("prefee",prefee);
    }

    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [WAITMODI]
     */
    public void setWaitmodi(Double  waitmodi){
        this.waitmodi = waitmodi ;
        this.modify("waitmodi",waitmodi);
    }

    /**
     * 设置 [MFEE]
     */
    public void setMfee(Double  mfee){
        this.mfee = mfee ;
        this.modify("mfee",mfee);
    }

    /**
     * 设置 [REGIONENDDATE]
     */
    public void setRegionenddate(Timestamp  regionenddate){
        this.regionenddate = regionenddate ;
        this.modify("regionenddate",regionenddate);
    }

    /**
     * 设置 [WOGROUP]
     */
    public void setWogroup(String  wogroup){
        this.wogroup = wogroup ;
        this.modify("wogroup",wogroup);
    }

    /**
     * 设置 [WORKLENGTH]
     */
    public void setWorklength(Double  worklength){
        this.worklength = worklength ;
        this.modify("worklength",worklength);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [EXPIREDATE]
     */
    public void setExpiredate(Timestamp  expiredate){
        this.expiredate = expiredate ;
        this.modify("expiredate",expiredate);
    }

    /**
     * 设置 [YXCB]
     */
    public void setYxcb(String  yxcb){
        this.yxcb = yxcb ;
        this.modify("yxcb",yxcb);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [NVAL]
     */
    public void setNval(Double  nval){
        this.nval = nval ;
        this.modify("nval",nval);
    }

    /**
     * 设置 [EMWONAME]
     */
    public void setEmwoname(String  emwoname){
        this.emwoname = emwoname ;
        this.modify("emwoname",emwoname);
    }

    /**
     * 设置 [EQSTOPLENGTH]
     */
    public void setEqstoplength(Double  eqstoplength){
        this.eqstoplength = eqstoplength ;
        this.modify("eqstoplength",eqstoplength);
    }

    /**
     * 设置 [WODATE]
     */
    public void setWodate(Timestamp  wodate){
        this.wodate = wodate ;
        this.modify("wodate",wodate);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [WRESULT]
     */
    public void setWresult(String  wresult){
        this.wresult = wresult ;
        this.modify("wresult",wresult);
    }

    /**
     * 设置 [WODESC]
     */
    public void setWodesc(String  wodesc){
        this.wodesc = wodesc ;
        this.modify("wodesc",wodesc);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [VAL]
     */
    public void setVal(String  val){
        this.val = val ;
        this.modify("val",val);
    }

    /**
     * 设置 [ARCHIVE]
     */
    public void setArchive(String  archive){
        this.archive = archive ;
        this.modify("archive",archive);
    }

    /**
     * 设置 [REGIONBEGINDATE]
     */
    public void setRegionbegindate(Timestamp  regionbegindate){
        this.regionbegindate = regionbegindate ;
        this.modify("regionbegindate",regionbegindate);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [WOPID]
     */
    public void setWopid(String  wopid){
        this.wopid = wopid ;
        this.modify("wopid",wopid);
    }

    /**
     * 设置 [RFOACID]
     */
    public void setRfoacid(String  rfoacid){
        this.rfoacid = rfoacid ;
        this.modify("rfoacid",rfoacid);
    }

    /**
     * 设置 [RFODEID]
     */
    public void setRfodeid(String  rfodeid){
        this.rfodeid = rfodeid ;
        this.modify("rfodeid",rfodeid);
    }

    /**
     * 设置 [WOORIID]
     */
    public void setWooriid(String  wooriid){
        this.wooriid = wooriid ;
        this.modify("wooriid",wooriid);
    }

    /**
     * 设置 [RFOMOID]
     */
    public void setRfomoid(String  rfomoid){
        this.rfomoid = rfomoid ;
        this.modify("rfomoid",rfomoid);
    }

    /**
     * 设置 [RTEAMID]
     */
    public void setRteamid(String  rteamid){
        this.rteamid = rteamid ;
        this.modify("rteamid",rteamid);
    }

    /**
     * 设置 [DPID]
     */
    public void setDpid(String  dpid){
        this.dpid = dpid ;
        this.modify("dpid",dpid);
    }

    /**
     * 设置 [RFOCAID]
     */
    public void setRfocaid(String  rfocaid){
        this.rfocaid = rfocaid ;
        this.modify("rfocaid",rfocaid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [RDEPTID]
     */
    public void setRdeptid(String  rdeptid){
        this.rdeptid = rdeptid ;
        this.modify("rdeptid",rdeptid);
    }

    /**
     * 设置 [WPERSONID]
     */
    public void setWpersonid(String  wpersonid){
        this.wpersonid = wpersonid ;
        this.modify("wpersonid",wpersonid);
    }

    /**
     * 设置 [RECVPERSONID]
     */
    public void setRecvpersonid(String  recvpersonid){
        this.recvpersonid = recvpersonid ;
        this.modify("recvpersonid",recvpersonid);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [MPERSONID]
     */
    public void setMpersonid(String  mpersonid){
        this.mpersonid = mpersonid ;
        this.modify("mpersonid",mpersonid);
    }


}


