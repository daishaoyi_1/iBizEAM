package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPODTO]
 */
@Data
public class EMPODTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [POAMOUNT]
     *
     */
    @JSONField(name = "poamount")
    @JsonProperty("poamount")
    private Double poamount;

    /**
     * 属性 [EADATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "eadate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("eadate")
    private Timestamp eadate;

    /**
     * 属性 [EMPOID]
     *
     */
    @JSONField(name = "empoid")
    @JsonProperty("empoid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empoid;

    /**
     * 属性 [POSTATE]
     *
     */
    @JSONField(name = "postate")
    @JsonProperty("postate")
    @NotNull(message = "[订单状态]不允许为空!")
    private Integer postate;

    /**
     * 属性 [PDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "pdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("pdate")
    @NotNull(message = "[订购日期]不允许为空!")
    private Timestamp pdate;

    /**
     * 属性 [CIVO]
     *
     */
    @JSONField(name = "civo")
    @JsonProperty("civo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String civo;

    /**
     * 属性 [LABSERVICEDESC]
     *
     */
    @JSONField(name = "labservicedesc")
    @JsonProperty("labservicedesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String labservicedesc;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMPONAME]
     *
     */
    @JSONField(name = "emponame")
    @JsonProperty("emponame")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emponame;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [TSFEE]
     *
     */
    @JSONField(name = "tsfee")
    @JsonProperty("tsfee")
    private Double tsfee;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [TAXFEE]
     *
     */
    @JSONField(name = "taxfee")
    @JsonProperty("taxfee")
    private Double taxfee;

    /**
     * 属性 [HTJY]
     *
     */
    @JSONField(name = "htjy")
    @JsonProperty("htjy")
    private Integer htjy;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [APPRDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "apprdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    private Timestamp apprdate;

    /**
     * 属性 [TAXIVO]
     *
     */
    @JSONField(name = "taxivo")
    @JsonProperty("taxivo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taxivo;

    /**
     * 属性 [ATT]
     *
     */
    @JSONField(name = "att")
    @JsonProperty("att")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String att;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [MAXPRICE]
     *
     */
    @JSONField(name = "maxprice")
    @JsonProperty("maxprice")
    private Double maxprice;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [PAYWAY]
     *
     */
    @JSONField(name = "payway")
    @JsonProperty("payway")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String payway;

    /**
     * 属性 [POINFO]
     *
     */
    @JSONField(name = "poinfo")
    @JsonProperty("poinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String poinfo;

    /**
     * 属性 [TSIVO]
     *
     */
    @JSONField(name = "tsivo")
    @JsonProperty("tsivo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tsivo;

    /**
     * 属性 [LABSERVICETYPEID]
     *
     */
    @JSONField(name = "labservicetypeid")
    @JsonProperty("labservicetypeid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicetypeid;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [ZJLEMPID]
     *
     */
    @JSONField(name = "zjlempid")
    @JsonProperty("zjlempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String zjlempid;

    /**
     * 属性 [ZJLEMPNAME]
     *
     */
    @JSONField(name = "zjlempname")
    @JsonProperty("zjlempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String zjlempname;

    /**
     * 属性 [FGEMPID]
     *
     */
    @JSONField(name = "fgempid")
    @JsonProperty("fgempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String fgempid;

    /**
     * 属性 [FGEMPNAME]
     *
     */
    @JSONField(name = "fgempname")
    @JsonProperty("fgempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fgempname;

    /**
     * 属性 [APPREMPID]
     *
     */
    @JSONField(name = "apprempid")
    @JsonProperty("apprempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String apprempid;

    /**
     * 属性 [APPREMPNAME]
     *
     */
    @JSONField(name = "apprempname")
    @JsonProperty("apprempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String apprempname;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rempname;


    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [POAMOUNT]
     */
    public void setPoamount(Double  poamount){
        this.poamount = poamount ;
        this.modify("poamount",poamount);
    }

    /**
     * 设置 [EADATE]
     */
    public void setEadate(Timestamp  eadate){
        this.eadate = eadate ;
        this.modify("eadate",eadate);
    }

    /**
     * 设置 [POSTATE]
     */
    public void setPostate(Integer  postate){
        this.postate = postate ;
        this.modify("postate",postate);
    }

    /**
     * 设置 [PDATE]
     */
    public void setPdate(Timestamp  pdate){
        this.pdate = pdate ;
        this.modify("pdate",pdate);
    }

    /**
     * 设置 [CIVO]
     */
    public void setCivo(String  civo){
        this.civo = civo ;
        this.modify("civo",civo);
    }

    /**
     * 设置 [LABSERVICEDESC]
     */
    public void setLabservicedesc(String  labservicedesc){
        this.labservicedesc = labservicedesc ;
        this.modify("labservicedesc",labservicedesc);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMPONAME]
     */
    public void setEmponame(String  emponame){
        this.emponame = emponame ;
        this.modify("emponame",emponame);
    }

    /**
     * 设置 [TSFEE]
     */
    public void setTsfee(Double  tsfee){
        this.tsfee = tsfee ;
        this.modify("tsfee",tsfee);
    }

    /**
     * 设置 [TAXFEE]
     */
    public void setTaxfee(Double  taxfee){
        this.taxfee = taxfee ;
        this.modify("taxfee",taxfee);
    }

    /**
     * 设置 [APPRDATE]
     */
    public void setApprdate(Timestamp  apprdate){
        this.apprdate = apprdate ;
        this.modify("apprdate",apprdate);
    }

    /**
     * 设置 [TAXIVO]
     */
    public void setTaxivo(String  taxivo){
        this.taxivo = taxivo ;
        this.modify("taxivo",taxivo);
    }

    /**
     * 设置 [ATT]
     */
    public void setAtt(String  att){
        this.att = att ;
        this.modify("att",att);
    }

    /**
     * 设置 [MAXPRICE]
     */
    public void setMaxprice(Double  maxprice){
        this.maxprice = maxprice ;
        this.modify("maxprice",maxprice);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [PAYWAY]
     */
    public void setPayway(String  payway){
        this.payway = payway ;
        this.modify("payway",payway);
    }

    /**
     * 设置 [TSIVO]
     */
    public void setTsivo(String  tsivo){
        this.tsivo = tsivo ;
        this.modify("tsivo",tsivo);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [ZJLEMPID]
     */
    public void setZjlempid(String  zjlempid){
        this.zjlempid = zjlempid ;
        this.modify("zjlempid",zjlempid);
    }

    /**
     * 设置 [FGEMPID]
     */
    public void setFgempid(String  fgempid){
        this.fgempid = fgempid ;
        this.modify("fgempid",fgempid);
    }

    /**
     * 设置 [APPREMPID]
     */
    public void setApprempid(String  apprempid){
        this.apprempid = apprempid ;
        this.modify("apprempid",apprempid);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }


}


