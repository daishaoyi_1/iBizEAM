package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEquipDTO]
 */
@Data
public class EMEquipDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PIC9]
     *
     */
    @JSONField(name = "pic9")
    @JsonProperty("pic9")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic9;

    /**
     * 属性 [EFCHECK]
     *
     */
    @JSONField(name = "efcheck")
    @JsonProperty("efcheck")
    private Double efcheck;

    /**
     * 属性 [EFCHECKNDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "efcheckndate" , format="yyyy-MM-dd")
    @JsonProperty("efcheckndate")
    private Timestamp efcheckndate;

    /**
     * 属性 [REPLACECOST]
     *
     */
    @JSONField(name = "replacecost")
    @JsonProperty("replacecost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String replacecost;

    /**
     * 属性 [EQPRIORITY]
     *
     */
    @JSONField(name = "eqpriority")
    @JsonProperty("eqpriority")
    private Double eqpriority;

    /**
     * 属性 [EFFICIENCY_Y]
     *
     */
    @JSONField(name = "efficiency_y")
    @JsonProperty("efficiency_y")
    private Double efficiencyY;

    /**
     * 属性 [INTACTRATE_Y]
     *
     */
    @JSONField(name = "intactrate_y")
    @JsonProperty("intactrate_y")
    private Double intactrateY;

    /**
     * 属性 [INTACTRATE_Q]
     *
     */
    @JSONField(name = "intactrate_q")
    @JsonProperty("intactrate_q")
    private Double intactrateQ;

    /**
     * 属性 [EFFICIENCY_M]
     *
     */
    @JSONField(name = "efficiency_m")
    @JsonProperty("efficiency_m")
    private Double efficiencyM;

    /**
     * 属性 [EMEQUIPNAME]
     *
     */
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emequipname;

    /**
     * 属性 [EMEQUIPID]
     *
     */
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emequipid;

    /**
     * 属性 [EFCHECKDESC]
     *
     */
    @JSONField(name = "efcheckdesc")
    @JsonProperty("efcheckdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String efcheckdesc;

    /**
     * 属性 [FAILURERATE_M]
     *
     */
    @JSONField(name = "failurerate_m")
    @JsonProperty("failurerate_m")
    private Double failurerateM;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptid;

    /**
     * 属性 [PIC6]
     *
     */
    @JSONField(name = "pic6")
    @JsonProperty("pic6")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic6;

    /**
     * 属性 [MATERIALCOST]
     *
     */
    @JSONField(name = "materialcost")
    @JsonProperty("materialcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String materialcost;

    /**
     * 属性 [HALTSTATE]
     *
     */
    @JSONField(name = "haltstate")
    @JsonProperty("haltstate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String haltstate;

    /**
     * 属性 [INTACTRATE_M]
     *
     */
    @JSONField(name = "intactrate_m")
    @JsonProperty("intactrate_m")
    private Double intactrateM;

    /**
     * 属性 [PIC8]
     *
     */
    @JSONField(name = "pic8")
    @JsonProperty("pic8")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic8;

    /**
     * 属性 [PURCHDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "purchdate" , format="yyyy-MM-dd")
    @JsonProperty("purchdate")
    private Timestamp purchdate;

    /**
     * 属性 [HALTCAUSE]
     *
     */
    @JSONField(name = "haltcause")
    @JsonProperty("haltcause")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String haltcause;

    /**
     * 属性 [PRODUCTPARAM]
     *
     */
    @JSONField(name = "productparam")
    @JsonProperty("productparam")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productparam;

    /**
     * 属性 [PIC4]
     *
     */
    @JSONField(name = "pic4")
    @JsonProperty("pic4")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic4;

    /**
     * 属性 [EQUIP_BH]
     *
     */
    @JSONField(name = "equip_bh")
    @JsonProperty("equip_bh")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipBh;

    /**
     * 属性 [MAINTENANCECOST]
     *
     */
    @JSONField(name = "maintenancecost")
    @JsonProperty("maintenancecost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String maintenancecost;

    /**
     * 属性 [EQSTATE]
     *
     */
    @JSONField(name = "eqstate")
    @JsonProperty("eqstate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqstate;

    /**
     * 属性 [EQLIFE]
     *
     */
    @JSONField(name = "eqlife")
    @JsonProperty("eqlife")
    private Double eqlife;

    /**
     * 属性 [ORIGINALCOST]
     *
     */
    @JSONField(name = "originalcost")
    @JsonProperty("originalcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String originalcost;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PARAMS]
     *
     */
    @JSONField(name = "params")
    @JsonProperty("params")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String params;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [OUTPUTRCT_DJ]
     *
     */
    @JSONField(name = "outputrct_dj")
    @JsonProperty("outputrct_dj")
    private Double outputrctDj;

    /**
     * 属性 [EQSERIALCODE]
     *
     */
    @JSONField(name = "eqserialcode")
    @JsonProperty("eqserialcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqserialcode;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PIC2]
     *
     */
    @JSONField(name = "pic2")
    @JsonProperty("pic2")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic2;

    /**
     * 属性 [PIC3]
     *
     */
    @JSONField(name = "pic3")
    @JsonProperty("pic3")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic3;

    /**
     * 属性 [EQUIPCODE]
     *
     */
    @JSONField(name = "equipcode")
    @JsonProperty("equipcode")
    @NotBlank(message = "[设备代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipcode;

    /**
     * 属性 [OUTPUTRCT_DY]
     *
     */
    @JSONField(name = "outputrct_dy")
    @JsonProperty("outputrct_dy")
    private Double outputrctDy;

    /**
     * 属性 [COSTCENTERID]
     *
     */
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costcenterid;

    /**
     * 属性 [EQSTARTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "eqstartdate" , format="yyyy-MM-dd")
    @JsonProperty("eqstartdate")
    private Timestamp eqstartdate;

    /**
     * 属性 [WARRANTYDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warrantydate" , format="yyyy-MM-dd")
    @JsonProperty("warrantydate")
    private Timestamp warrantydate;

    /**
     * 属性 [EQISSERVICE1]
     *
     */
    @JSONField(name = "eqisservice1")
    @JsonProperty("eqisservice1")
    private Integer eqisservice1;

    /**
     * 属性 [FAILURERATE_Q]
     *
     */
    @JSONField(name = "failurerate_q")
    @JsonProperty("failurerate_q")
    private Double failurerateQ;

    /**
     * 属性 [PIC7]
     *
     */
    @JSONField(name = "pic7")
    @JsonProperty("pic7")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic7;

    /**
     * 属性 [PIC]
     *
     */
    @JSONField(name = "pic")
    @JsonProperty("pic")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic;

    /**
     * 属性 [BLSYSTEMDESC]
     *
     */
    @JSONField(name = "blsystemdesc")
    @JsonProperty("blsystemdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String blsystemdesc;

    /**
     * 属性 [EFFICIENCY_Q]
     *
     */
    @JSONField(name = "efficiency_q")
    @JsonProperty("efficiency_q")
    private Double efficiencyQ;

    /**
     * 属性 [EQMODELCODE]
     *
     */
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqmodelcode;

    /**
     * 属性 [PIC5]
     *
     */
    @JSONField(name = "pic5")
    @JsonProperty("pic5")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic5;

    /**
     * 属性 [EQUIPDESC]
     *
     */
    @JSONField(name = "equipdesc")
    @JsonProperty("equipdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipdesc;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [INNERLABORCOST]
     *
     */
    @JSONField(name = "innerlaborcost")
    @JsonProperty("innerlaborcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String innerlaborcost;

    /**
     * 属性 [KEYATTPARAM]
     *
     */
    @JSONField(name = "keyattparam")
    @JsonProperty("keyattparam")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String keyattparam;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [TECHCODE]
     *
     */
    @JSONField(name = "techcode")
    @JsonProperty("techcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String techcode;

    /**
     * 属性 [OUTPUTRCT_DN]
     *
     */
    @JSONField(name = "outputrct_dn")
    @JsonProperty("outputrct_dn")
    private Double outputrctDn;

    /**
     * 属性 [EQISSERVICE]
     *
     */
    @JSONField(name = "eqisservice")
    @JsonProperty("eqisservice")
    private Integer eqisservice;

    /**
     * 属性 [FAILURERATE_Y]
     *
     */
    @JSONField(name = "failurerate_y")
    @JsonProperty("failurerate_y")
    private Double failurerateY;

    /**
     * 属性 [EFCHECKDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "efcheckdate" , format="yyyy-MM-dd")
    @JsonProperty("efcheckdate")
    private Timestamp efcheckdate;

    /**
     * 属性 [FOREIGNLABORCOST]
     *
     */
    @JSONField(name = "foreignlaborcost")
    @JsonProperty("foreignlaborcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String foreignlaborcost;

    /**
     * 属性 [EQUIPGROUP]
     *
     */
    @JSONField(name = "equipgroup")
    @JsonProperty("equipgroup")
    @NotNull(message = "[设备分组]不允许为空!")
    private Integer equipgroup;

    /**
     * 属性 [DEPTNAME]
     *
     */
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptname;

    /**
     * 属性 [HALTSTATEINFO]
     *
     */
    @JSONField(name = "haltstateinfo")
    @JsonProperty("haltstateinfo")
    @Size(min = 0, max = 999999, message = "内容长度必须小于等于[999999]")
    private String haltstateinfo;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EFCHECKCDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "efcheckcdate" , format="yyyy-MM-dd")
    @JsonProperty("efcheckcdate")
    private Timestamp efcheckcdate;

    /**
     * 属性 [EQUIPINFO]
     *
     */
    @JSONField(name = "equipinfo")
    @JsonProperty("equipinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipinfo;

    /**
     * 属性 [EQSUMSTOPTIME]
     *
     */
    @JSONField(name = "eqsumstoptime")
    @JsonProperty("eqsumstoptime")
    private Double eqsumstoptime;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empname;

    /**
     * 属性 [EMBERTHCODE]
     *
     */
    @JSONField(name = "emberthcode")
    @JsonProperty("emberthcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emberthcode;

    /**
     * 属性 [RTEAMNAME]
     *
     */
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rteamname;

    /**
     * 属性 [JZBH1]
     *
     */
    @JSONField(name = "jzbh1")
    @JsonProperty("jzbh1")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jzbh1;

    /**
     * 属性 [EMMACHINECATEGORYNAME]
     *
     */
    @JSONField(name = "emmachinecategoryname")
    @JsonProperty("emmachinecategoryname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emmachinecategoryname;

    /**
     * 属性 [STYPE]
     *
     */
    @JSONField(name = "stype")
    @JsonProperty("stype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stype;

    /**
     * 属性 [MSERVICENAME]
     *
     */
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mservicename;

    /**
     * 属性 [EQTYPECODE]
     *
     */
    @JSONField(name = "eqtypecode")
    @JsonProperty("eqtypecode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqtypecode;

    /**
     * 属性 [ASSETCLASSCODE]
     *
     */
    @JSONField(name = "assetclasscode")
    @JsonProperty("assetclasscode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclasscode;

    /**
     * 属性 [EQUIPPCODE]
     *
     */
    @JSONField(name = "equippcode")
    @JsonProperty("equippcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equippcode;

    /**
     * 属性 [EQTYPENAME]
     *
     */
    @JSONField(name = "eqtypename")
    @JsonProperty("eqtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqtypename;

    /**
     * 属性 [EMMACHMODELNAME]
     *
     */
    @JSONField(name = "emmachmodelname")
    @JsonProperty("emmachmodelname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emmachmodelname;

    /**
     * 属性 [EMBRANDCODE]
     *
     */
    @JSONField(name = "embrandcode")
    @JsonProperty("embrandcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embrandcode;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String acclassname;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [ASSETNAME]
     *
     */
    @JSONField(name = "assetname")
    @JsonProperty("assetname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetname;

    /**
     * 属性 [ASSETCLASSNAME]
     *
     */
    @JSONField(name = "assetclassname")
    @JsonProperty("assetclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetclassname;

    /**
     * 属性 [EMBRANDNAME]
     *
     */
    @JSONField(name = "embrandname")
    @JsonProperty("embrandname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embrandname;

    /**
     * 属性 [EQLOCATIONNAME]
     *
     */
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationname;

    /**
     * 属性 [EQUIPPNAME]
     *
     */
    @JSONField(name = "equippname")
    @JsonProperty("equippname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equippname;

    /**
     * 属性 [ASSETCLASSID]
     *
     */
    @JSONField(name = "assetclassid")
    @JsonProperty("assetclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclassid;

    /**
     * 属性 [CONTRACTNAME]
     *
     */
    @JSONField(name = "contractname")
    @JsonProperty("contractname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String contractname;

    /**
     * 属性 [EMBERTHNAME]
     *
     */
    @JSONField(name = "emberthname")
    @JsonProperty("emberthname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emberthname;

    /**
     * 属性 [ASSETCODE]
     *
     */
    @JSONField(name = "assetcode")
    @JsonProperty("assetcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetcode;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rservicename;

    /**
     * 属性 [EQLOCATIONCODE]
     *
     */
    @JSONField(name = "eqlocationcode")
    @JsonProperty("eqlocationcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationcode;

    /**
     * 属性 [MACHTYPECODE]
     *
     */
    @JSONField(name = "machtypecode")
    @JsonProperty("machtypecode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String machtypecode;

    /**
     * 属性 [EQTYPEPID]
     *
     */
    @JSONField(name = "eqtypepid")
    @JsonProperty("eqtypepid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqtypepid;

    /**
     * 属性 [SNAME]
     *
     */
    @JSONField(name = "sname")
    @JsonProperty("sname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sname;

    /**
     * 属性 [EQLOCATIONID]
     *
     */
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationid;

    /**
     * 属性 [EMMACHINECATEGORYID]
     *
     */
    @JSONField(name = "emmachinecategoryid")
    @JsonProperty("emmachinecategoryid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emmachinecategoryid;

    /**
     * 属性 [EMBERTHID]
     *
     */
    @JSONField(name = "emberthid")
    @JsonProperty("emberthid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emberthid;

    /**
     * 属性 [RTEAMID]
     *
     */
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rteamid;

    /**
     * 属性 [EMBRANDID]
     *
     */
    @JSONField(name = "embrandid")
    @JsonProperty("embrandid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String embrandid;

    /**
     * 属性 [EQUIPPID]
     *
     */
    @JSONField(name = "equippid")
    @JsonProperty("equippid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equippid;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rserviceid;

    /**
     * 属性 [EMMACHMODELID]
     *
     */
    @JSONField(name = "emmachmodelid")
    @JsonProperty("emmachmodelid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emmachmodelid;

    /**
     * 属性 [CONTRACTID]
     *
     */
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contractid;

    /**
     * 属性 [EQTYPEID]
     *
     */
    @JSONField(name = "eqtypeid")
    @JsonProperty("eqtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqtypeid;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String acclassid;

    /**
     * 属性 [ASSETID]
     *
     */
    @JSONField(name = "assetid")
    @JsonProperty("assetid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetid;

    /**
     * 属性 [MSERVICEID]
     *
     */
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mserviceid;


    /**
     * 设置 [PIC9]
     */
    public void setPic9(String  pic9){
        this.pic9 = pic9 ;
        this.modify("pic9",pic9);
    }

    /**
     * 设置 [EFCHECK]
     */
    public void setEfcheck(Double  efcheck){
        this.efcheck = efcheck ;
        this.modify("efcheck",efcheck);
    }

    /**
     * 设置 [EFCHECKNDATE]
     */
    public void setEfcheckndate(Timestamp  efcheckndate){
        this.efcheckndate = efcheckndate ;
        this.modify("efcheckndate",efcheckndate);
    }

    /**
     * 设置 [REPLACECOST]
     */
    public void setReplacecost(String  replacecost){
        this.replacecost = replacecost ;
        this.modify("replacecost",replacecost);
    }

    /**
     * 设置 [EQPRIORITY]
     */
    public void setEqpriority(Double  eqpriority){
        this.eqpriority = eqpriority ;
        this.modify("eqpriority",eqpriority);
    }

    /**
     * 设置 [EFFICIENCY_Y]
     */
    public void setEfficiencyY(Double  efficiencyY){
        this.efficiencyY = efficiencyY ;
        this.modify("efficiency_y",efficiencyY);
    }

    /**
     * 设置 [INTACTRATE_Y]
     */
    public void setIntactrateY(Double  intactrateY){
        this.intactrateY = intactrateY ;
        this.modify("intactrate_y",intactrateY);
    }

    /**
     * 设置 [INTACTRATE_Q]
     */
    public void setIntactrateQ(Double  intactrateQ){
        this.intactrateQ = intactrateQ ;
        this.modify("intactrate_q",intactrateQ);
    }

    /**
     * 设置 [EFFICIENCY_M]
     */
    public void setEfficiencyM(Double  efficiencyM){
        this.efficiencyM = efficiencyM ;
        this.modify("efficiency_m",efficiencyM);
    }

    /**
     * 设置 [EMEQUIPNAME]
     */
    public void setEmequipname(String  emequipname){
        this.emequipname = emequipname ;
        this.modify("emequipname",emequipname);
    }

    /**
     * 设置 [EFCHECKDESC]
     */
    public void setEfcheckdesc(String  efcheckdesc){
        this.efcheckdesc = efcheckdesc ;
        this.modify("efcheckdesc",efcheckdesc);
    }

    /**
     * 设置 [FAILURERATE_M]
     */
    public void setFailurerateM(Double  failurerateM){
        this.failurerateM = failurerateM ;
        this.modify("failurerate_m",failurerateM);
    }

    /**
     * 设置 [DEPTID]
     */
    public void setDeptid(String  deptid){
        this.deptid = deptid ;
        this.modify("deptid",deptid);
    }

    /**
     * 设置 [PIC6]
     */
    public void setPic6(String  pic6){
        this.pic6 = pic6 ;
        this.modify("pic6",pic6);
    }

    /**
     * 设置 [MATERIALCOST]
     */
    public void setMaterialcost(String  materialcost){
        this.materialcost = materialcost ;
        this.modify("materialcost",materialcost);
    }

    /**
     * 设置 [HALTSTATE]
     */
    public void setHaltstate(String  haltstate){
        this.haltstate = haltstate ;
        this.modify("haltstate",haltstate);
    }

    /**
     * 设置 [INTACTRATE_M]
     */
    public void setIntactrateM(Double  intactrateM){
        this.intactrateM = intactrateM ;
        this.modify("intactrate_m",intactrateM);
    }

    /**
     * 设置 [PIC8]
     */
    public void setPic8(String  pic8){
        this.pic8 = pic8 ;
        this.modify("pic8",pic8);
    }

    /**
     * 设置 [PURCHDATE]
     */
    public void setPurchdate(Timestamp  purchdate){
        this.purchdate = purchdate ;
        this.modify("purchdate",purchdate);
    }

    /**
     * 设置 [HALTCAUSE]
     */
    public void setHaltcause(String  haltcause){
        this.haltcause = haltcause ;
        this.modify("haltcause",haltcause);
    }

    /**
     * 设置 [PRODUCTPARAM]
     */
    public void setProductparam(String  productparam){
        this.productparam = productparam ;
        this.modify("productparam",productparam);
    }

    /**
     * 设置 [PIC4]
     */
    public void setPic4(String  pic4){
        this.pic4 = pic4 ;
        this.modify("pic4",pic4);
    }

    /**
     * 设置 [EQUIP_BH]
     */
    public void setEquipBh(String  equipBh){
        this.equipBh = equipBh ;
        this.modify("equip_bh",equipBh);
    }

    /**
     * 设置 [MAINTENANCECOST]
     */
    public void setMaintenancecost(String  maintenancecost){
        this.maintenancecost = maintenancecost ;
        this.modify("maintenancecost",maintenancecost);
    }

    /**
     * 设置 [EQSTATE]
     */
    public void setEqstate(String  eqstate){
        this.eqstate = eqstate ;
        this.modify("eqstate",eqstate);
    }

    /**
     * 设置 [EQLIFE]
     */
    public void setEqlife(Double  eqlife){
        this.eqlife = eqlife ;
        this.modify("eqlife",eqlife);
    }

    /**
     * 设置 [ORIGINALCOST]
     */
    public void setOriginalcost(String  originalcost){
        this.originalcost = originalcost ;
        this.modify("originalcost",originalcost);
    }

    /**
     * 设置 [PARAMS]
     */
    public void setParams(String  params){
        this.params = params ;
        this.modify("params",params);
    }

    /**
     * 设置 [OUTPUTRCT_DJ]
     */
    public void setOutputrctDj(Double  outputrctDj){
        this.outputrctDj = outputrctDj ;
        this.modify("outputrct_dj",outputrctDj);
    }

    /**
     * 设置 [EQSERIALCODE]
     */
    public void setEqserialcode(String  eqserialcode){
        this.eqserialcode = eqserialcode ;
        this.modify("eqserialcode",eqserialcode);
    }

    /**
     * 设置 [PIC2]
     */
    public void setPic2(String  pic2){
        this.pic2 = pic2 ;
        this.modify("pic2",pic2);
    }

    /**
     * 设置 [PIC3]
     */
    public void setPic3(String  pic3){
        this.pic3 = pic3 ;
        this.modify("pic3",pic3);
    }

    /**
     * 设置 [EQUIPCODE]
     */
    public void setEquipcode(String  equipcode){
        this.equipcode = equipcode ;
        this.modify("equipcode",equipcode);
    }

    /**
     * 设置 [OUTPUTRCT_DY]
     */
    public void setOutputrctDy(Double  outputrctDy){
        this.outputrctDy = outputrctDy ;
        this.modify("outputrct_dy",outputrctDy);
    }

    /**
     * 设置 [COSTCENTERID]
     */
    public void setCostcenterid(String  costcenterid){
        this.costcenterid = costcenterid ;
        this.modify("costcenterid",costcenterid);
    }

    /**
     * 设置 [EQSTARTDATE]
     */
    public void setEqstartdate(Timestamp  eqstartdate){
        this.eqstartdate = eqstartdate ;
        this.modify("eqstartdate",eqstartdate);
    }

    /**
     * 设置 [WARRANTYDATE]
     */
    public void setWarrantydate(Timestamp  warrantydate){
        this.warrantydate = warrantydate ;
        this.modify("warrantydate",warrantydate);
    }

    /**
     * 设置 [EQISSERVICE1]
     */
    public void setEqisservice1(Integer  eqisservice1){
        this.eqisservice1 = eqisservice1 ;
        this.modify("eqisservice1",eqisservice1);
    }

    /**
     * 设置 [FAILURERATE_Q]
     */
    public void setFailurerateQ(Double  failurerateQ){
        this.failurerateQ = failurerateQ ;
        this.modify("failurerate_q",failurerateQ);
    }

    /**
     * 设置 [PIC7]
     */
    public void setPic7(String  pic7){
        this.pic7 = pic7 ;
        this.modify("pic7",pic7);
    }

    /**
     * 设置 [PIC]
     */
    public void setPic(String  pic){
        this.pic = pic ;
        this.modify("pic",pic);
    }

    /**
     * 设置 [BLSYSTEMDESC]
     */
    public void setBlsystemdesc(String  blsystemdesc){
        this.blsystemdesc = blsystemdesc ;
        this.modify("blsystemdesc",blsystemdesc);
    }

    /**
     * 设置 [EFFICIENCY_Q]
     */
    public void setEfficiencyQ(Double  efficiencyQ){
        this.efficiencyQ = efficiencyQ ;
        this.modify("efficiency_q",efficiencyQ);
    }

    /**
     * 设置 [EQMODELCODE]
     */
    public void setEqmodelcode(String  eqmodelcode){
        this.eqmodelcode = eqmodelcode ;
        this.modify("eqmodelcode",eqmodelcode);
    }

    /**
     * 设置 [PIC5]
     */
    public void setPic5(String  pic5){
        this.pic5 = pic5 ;
        this.modify("pic5",pic5);
    }

    /**
     * 设置 [EQUIPDESC]
     */
    public void setEquipdesc(String  equipdesc){
        this.equipdesc = equipdesc ;
        this.modify("equipdesc",equipdesc);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [INNERLABORCOST]
     */
    public void setInnerlaborcost(String  innerlaborcost){
        this.innerlaborcost = innerlaborcost ;
        this.modify("innerlaborcost",innerlaborcost);
    }

    /**
     * 设置 [KEYATTPARAM]
     */
    public void setKeyattparam(String  keyattparam){
        this.keyattparam = keyattparam ;
        this.modify("keyattparam",keyattparam);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [TECHCODE]
     */
    public void setTechcode(String  techcode){
        this.techcode = techcode ;
        this.modify("techcode",techcode);
    }

    /**
     * 设置 [OUTPUTRCT_DN]
     */
    public void setOutputrctDn(Double  outputrctDn){
        this.outputrctDn = outputrctDn ;
        this.modify("outputrct_dn",outputrctDn);
    }

    /**
     * 设置 [EQISSERVICE]
     */
    public void setEqisservice(Integer  eqisservice){
        this.eqisservice = eqisservice ;
        this.modify("eqisservice",eqisservice);
    }

    /**
     * 设置 [FAILURERATE_Y]
     */
    public void setFailurerateY(Double  failurerateY){
        this.failurerateY = failurerateY ;
        this.modify("failurerate_y",failurerateY);
    }

    /**
     * 设置 [EFCHECKDATE]
     */
    public void setEfcheckdate(Timestamp  efcheckdate){
        this.efcheckdate = efcheckdate ;
        this.modify("efcheckdate",efcheckdate);
    }

    /**
     * 设置 [FOREIGNLABORCOST]
     */
    public void setForeignlaborcost(String  foreignlaborcost){
        this.foreignlaborcost = foreignlaborcost ;
        this.modify("foreignlaborcost",foreignlaborcost);
    }

    /**
     * 设置 [EQUIPGROUP]
     */
    public void setEquipgroup(Integer  equipgroup){
        this.equipgroup = equipgroup ;
        this.modify("equipgroup",equipgroup);
    }

    /**
     * 设置 [DEPTNAME]
     */
    public void setDeptname(String  deptname){
        this.deptname = deptname ;
        this.modify("deptname",deptname);
    }

    /**
     * 设置 [EFCHECKCDATE]
     */
    public void setEfcheckcdate(Timestamp  efcheckcdate){
        this.efcheckcdate = efcheckcdate ;
        this.modify("efcheckcdate",efcheckcdate);
    }

    /**
     * 设置 [EQSUMSTOPTIME]
     */
    public void setEqsumstoptime(Double  eqsumstoptime){
        this.eqsumstoptime = eqsumstoptime ;
        this.modify("eqsumstoptime",eqsumstoptime);
    }

    /**
     * 设置 [EMPNAME]
     */
    public void setEmpname(String  empname){
        this.empname = empname ;
        this.modify("empname",empname);
    }

    /**
     * 设置 [EQLOCATIONID]
     */
    public void setEqlocationid(String  eqlocationid){
        this.eqlocationid = eqlocationid ;
        this.modify("eqlocationid",eqlocationid);
    }

    /**
     * 设置 [EMMACHINECATEGORYID]
     */
    public void setEmmachinecategoryid(String  emmachinecategoryid){
        this.emmachinecategoryid = emmachinecategoryid ;
        this.modify("emmachinecategoryid",emmachinecategoryid);
    }

    /**
     * 设置 [EMBERTHID]
     */
    public void setEmberthid(String  emberthid){
        this.emberthid = emberthid ;
        this.modify("emberthid",emberthid);
    }

    /**
     * 设置 [RTEAMID]
     */
    public void setRteamid(String  rteamid){
        this.rteamid = rteamid ;
        this.modify("rteamid",rteamid);
    }

    /**
     * 设置 [EMBRANDID]
     */
    public void setEmbrandid(String  embrandid){
        this.embrandid = embrandid ;
        this.modify("embrandid",embrandid);
    }

    /**
     * 设置 [EQUIPPID]
     */
    public void setEquippid(String  equippid){
        this.equippid = equippid ;
        this.modify("equippid",equippid);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [EMMACHMODELID]
     */
    public void setEmmachmodelid(String  emmachmodelid){
        this.emmachmodelid = emmachmodelid ;
        this.modify("emmachmodelid",emmachmodelid);
    }

    /**
     * 设置 [CONTRACTID]
     */
    public void setContractid(String  contractid){
        this.contractid = contractid ;
        this.modify("contractid",contractid);
    }

    /**
     * 设置 [EQTYPEID]
     */
    public void setEqtypeid(String  eqtypeid){
        this.eqtypeid = eqtypeid ;
        this.modify("eqtypeid",eqtypeid);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [ASSETID]
     */
    public void setAssetid(String  assetid){
        this.assetid = assetid ;
        this.modify("assetid",assetid);
    }

    /**
     * 设置 [MSERVICEID]
     */
    public void setMserviceid(String  mserviceid){
        this.mserviceid = mserviceid ;
        this.modify("mserviceid",mserviceid);
    }


}


