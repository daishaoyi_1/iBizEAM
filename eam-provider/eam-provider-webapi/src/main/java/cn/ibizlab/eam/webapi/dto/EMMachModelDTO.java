package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMMachModelDTO]
 */
@Data
public class EMMachModelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [MACHTYPECODE]
     *
     */
    @JSONField(name = "machtypecode")
    @JsonProperty("machtypecode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String machtypecode;

    /**
     * 属性 [EMMACHMODELID]
     *
     */
    @JSONField(name = "emmachmodelid")
    @JsonProperty("emmachmodelid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emmachmodelid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [REMARKS]
     *
     */
    @JSONField(name = "remarks")
    @JsonProperty("remarks")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String remarks;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMMACHMODELNAME]
     *
     */
    @JSONField(name = "emmachmodelname")
    @JsonProperty("emmachmodelname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emmachmodelname;


    /**
     * 设置 [MACHTYPECODE]
     */
    public void setMachtypecode(String  machtypecode){
        this.machtypecode = machtypecode ;
        this.modify("machtypecode",machtypecode);
    }

    /**
     * 设置 [REMARKS]
     */
    public void setRemarks(String  remarks){
        this.remarks = remarks ;
        this.modify("remarks",remarks);
    }

    /**
     * 设置 [EMMACHMODELNAME]
     */
    public void setEmmachmodelname(String  emmachmodelname){
        this.emmachmodelname = emmachmodelname ;
        this.modify("emmachmodelname",emmachmodelname);
    }


}


