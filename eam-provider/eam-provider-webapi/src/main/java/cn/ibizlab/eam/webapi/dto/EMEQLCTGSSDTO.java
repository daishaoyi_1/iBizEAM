package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQLCTGSSDTO]
 */
@Data
public class EMEQLCTGSSDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [GSSXM]
     *
     */
    @JSONField(name = "gssxm")
    @JsonProperty("gssxm")
    @NotBlank(message = "[项目]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String gssxm;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EQMODELCODE]
     *
     */
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqmodelcode;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [VALVE]
     *
     */
    @JSONField(name = "valve")
    @JsonProperty("valve")
    private Integer valve;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [LEN]
     *
     */
    @JSONField(name = "len")
    @JsonProperty("len")
    @NotNull(message = "[长度]不允许为空!")
    private Double len;

    /**
     * 属性 [ZJ]
     *
     */
    @JSONField(name = "zj")
    @JsonProperty("zj")
    @NotNull(message = "[直径]不允许为空!")
    private Double zj;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [EQLOCATIONINFO]
     *
     */
    @JSONField(name = "eqlocationinfo")
    @JsonProperty("eqlocationinfo")
    @NotBlank(message = "[钢丝绳信息]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationinfo;

    /**
     * 属性 [EMEQLOCATIONID]
     *
     */
    @JSONField(name = "emeqlocationid")
    @JsonProperty("emeqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqlocationid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [GSSXM]
     */
    public void setGssxm(String  gssxm){
        this.gssxm = gssxm ;
        this.modify("gssxm",gssxm);
    }

    /**
     * 设置 [EQMODELCODE]
     */
    public void setEqmodelcode(String  eqmodelcode){
        this.eqmodelcode = eqmodelcode ;
        this.modify("eqmodelcode",eqmodelcode);
    }

    /**
     * 设置 [VALVE]
     */
    public void setValve(Integer  valve){
        this.valve = valve ;
        this.modify("valve",valve);
    }

    /**
     * 设置 [LEN]
     */
    public void setLen(Double  len){
        this.len = len ;
        this.modify("len",len);
    }

    /**
     * 设置 [ZJ]
     */
    public void setZj(Double  zj){
        this.zj = zj ;
        this.modify("zj",zj);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }


}


