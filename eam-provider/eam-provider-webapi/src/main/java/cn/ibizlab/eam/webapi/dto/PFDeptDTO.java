package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[PFDeptDTO]
 */
@Data
public class PFDeptDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DEPTCODE]
     *
     */
    @JSONField(name = "deptcode")
    @JsonProperty("deptcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptcode;

    /**
     * 属性 [MGREMPNAME]
     *
     */
    @JSONField(name = "mgrempname")
    @JsonProperty("mgrempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mgrempname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [DEPTINFO]
     *
     */
    @JSONField(name = "deptinfo")
    @JsonProperty("deptinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String deptinfo;

    /**
     * 属性 [DEPTFN]
     *
     */
    @JSONField(name = "deptfn")
    @JsonProperty("deptfn")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String deptfn;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [MAINDEPTCODE]
     *
     */
    @JSONField(name = "maindeptcode")
    @JsonProperty("maindeptcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String maindeptcode;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [DEPTPID]
     *
     */
    @JSONField(name = "deptpid")
    @JsonProperty("deptpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptpid;

    /**
     * 属性 [PFDEPTID]
     *
     */
    @JSONField(name = "pfdeptid")
    @JsonProperty("pfdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfdeptid;

    /**
     * 属性 [MGREMPID]
     *
     */
    @JSONField(name = "mgrempid")
    @JsonProperty("mgrempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mgrempid;

    /**
     * 属性 [PFDEPTNAME]
     *
     */
    @JSONField(name = "pfdeptname")
    @JsonProperty("pfdeptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfdeptname;

    /**
     * 属性 [SDEPT]
     *
     */
    @JSONField(name = "sdept")
    @JsonProperty("sdept")
    private Integer sdept;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [DEPTPNAME]
     *
     */
    @JSONField(name = "deptpname")
    @JsonProperty("deptpname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptpname;


    /**
     * 设置 [DEPTCODE]
     */
    public void setDeptcode(String  deptcode){
        this.deptcode = deptcode ;
        this.modify("deptcode",deptcode);
    }

    /**
     * 设置 [MGREMPNAME]
     */
    public void setMgrempname(String  mgrempname){
        this.mgrempname = mgrempname ;
        this.modify("mgrempname",mgrempname);
    }

    /**
     * 设置 [DEPTFN]
     */
    public void setDeptfn(String  deptfn){
        this.deptfn = deptfn ;
        this.modify("deptfn",deptfn);
    }

    /**
     * 设置 [MAINDEPTCODE]
     */
    public void setMaindeptcode(String  maindeptcode){
        this.maindeptcode = maindeptcode ;
        this.modify("maindeptcode",maindeptcode);
    }

    /**
     * 设置 [DEPTPID]
     */
    public void setDeptpid(String  deptpid){
        this.deptpid = deptpid ;
        this.modify("deptpid",deptpid);
    }

    /**
     * 设置 [MGREMPID]
     */
    public void setMgrempid(String  mgrempid){
        this.mgrempid = mgrempid ;
        this.modify("mgrempid",mgrempid);
    }

    /**
     * 设置 [PFDEPTNAME]
     */
    public void setPfdeptname(String  pfdeptname){
        this.pfdeptname = pfdeptname ;
        this.modify("pfdeptname",pfdeptname);
    }

    /**
     * 设置 [SDEPT]
     */
    public void setSdept(Integer  sdept){
        this.sdept = sdept ;
        this.modify("sdept",sdept);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [DEPTPNAME]
     */
    public void setDeptpname(String  deptpname){
        this.deptpname = deptpname ;
        this.modify("deptpname",deptpname);
    }


}


