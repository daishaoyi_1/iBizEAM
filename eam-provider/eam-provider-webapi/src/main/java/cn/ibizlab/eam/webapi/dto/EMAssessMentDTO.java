package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMAssessMentDTO]
 */
@Data
public class EMAssessMentDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMASSESSMENTNAME]
     *
     */
    @JSONField(name = "emassessmentname")
    @JsonProperty("emassessmentname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassessmentname;

    /**
     * 属性 [YEARMONTH]
     *
     */
    @JSONField(name = "yearmonth")
    @JsonProperty("yearmonth")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String yearmonth;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [EMASSESSMENTID]
     *
     */
    @JSONField(name = "emassessmentid")
    @JsonProperty("emassessmentid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassessmentid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [PFEMPID]
     *
     */
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfempid;

    /**
     * 属性 [PFEMPNAME]
     *
     */
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfempname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [MAKTABDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "maktabdate" , format="yyyy-MM-dd")
    @JsonProperty("maktabdate")
    private Timestamp maktabdate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PFTEAMNAME]
     *
     */
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfteamname;

    /**
     * 属性 [PFTEAMID]
     *
     */
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfteamid;


    /**
     * 设置 [EMASSESSMENTNAME]
     */
    public void setEmassessmentname(String  emassessmentname){
        this.emassessmentname = emassessmentname ;
        this.modify("emassessmentname",emassessmentname);
    }

    /**
     * 设置 [YEARMONTH]
     */
    public void setYearmonth(String  yearmonth){
        this.yearmonth = yearmonth ;
        this.modify("yearmonth",yearmonth);
    }

    /**
     * 设置 [PFEMPID]
     */
    public void setPfempid(String  pfempid){
        this.pfempid = pfempid ;
        this.modify("pfempid",pfempid);
    }

    /**
     * 设置 [PFEMPNAME]
     */
    public void setPfempname(String  pfempname){
        this.pfempname = pfempname ;
        this.modify("pfempname",pfempname);
    }

    /**
     * 设置 [MAKTABDATE]
     */
    public void setMaktabdate(Timestamp  maktabdate){
        this.maktabdate = maktabdate ;
        this.modify("maktabdate",maktabdate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PFTEAMID]
     */
    public void setPfteamid(String  pfteamid){
        this.pfteamid = pfteamid ;
        this.modify("pfteamid",pfteamid);
    }


}


