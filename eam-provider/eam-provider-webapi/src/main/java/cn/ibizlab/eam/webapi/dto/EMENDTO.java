package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMENDTO]
 */
@Data
public class EMENDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ENERGYDESC]
     *
     */
    @JSONField(name = "energydesc")
    @JsonProperty("energydesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String energydesc;

    /**
     * 属性 [EMENID]
     *
     */
    @JSONField(name = "emenid")
    @JsonProperty("emenid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emenid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ENERGYCODE]
     *
     */
    @JSONField(name = "energycode")
    @JsonProperty("energycode")
    @NotBlank(message = "[能源代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String energycode;

    /**
     * 属性 [ENERGYTYPEID]
     *
     */
    @JSONField(name = "energytypeid")
    @JsonProperty("energytypeid")
    @NotBlank(message = "[能源类型]不允许为空!")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String energytypeid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [VRATE]
     *
     */
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    @NotNull(message = "[倍率]不允许为空!")
    private Double vrate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [EMENNAME]
     *
     */
    @JSONField(name = "emenname")
    @JsonProperty("emenname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emenname;

    /**
     * 属性 [ENERGYINFO]
     *
     */
    @JSONField(name = "energyinfo")
    @JsonProperty("energyinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String energyinfo;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [ITEMMTYPEID]
     *
     */
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemmtypeid;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [ITEMPRICE]
     *
     */
    @JSONField(name = "itemprice")
    @JsonProperty("itemprice")
    private Double itemprice;

    /**
     * 属性 [ITEMMTYPENAME]
     *
     */
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemmtypename;

    /**
     * 属性 [ITEMTYPEID]
     *
     */
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemtypeid;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itembtypeid;

    /**
     * 属性 [ITEMBTYPENAME]
     *
     */
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itembtypename;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;


    /**
     * 设置 [ENERGYDESC]
     */
    public void setEnergydesc(String  energydesc){
        this.energydesc = energydesc ;
        this.modify("energydesc",energydesc);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ENERGYCODE]
     */
    public void setEnergycode(String  energycode){
        this.energycode = energycode ;
        this.modify("energycode",energycode);
    }

    /**
     * 设置 [ENERGYTYPEID]
     */
    public void setEnergytypeid(String  energytypeid){
        this.energytypeid = energytypeid ;
        this.modify("energytypeid",energytypeid);
    }

    /**
     * 设置 [VRATE]
     */
    public void setVrate(Double  vrate){
        this.vrate = vrate ;
        this.modify("vrate",vrate);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [EMENNAME]
     */
    public void setEmenname(String  emenname){
        this.emenname = emenname ;
        this.modify("emenname",emenname);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }


}


