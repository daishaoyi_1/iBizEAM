package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMBidinquiryDTO]
 */
@Data
public class EMBidinquiryDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [ADATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "adate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    @NotNull(message = "[录入时间]不允许为空!")
    private Timestamp adate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [TARIFF]
     *
     */
    @JSONField(name = "tariff")
    @JsonProperty("tariff")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tariff;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [ACHIEVEMENT]
     *
     */
    @JSONField(name = "achievement")
    @JsonProperty("achievement")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String achievement;

    /**
     * 属性 [EMBIDINQUIRYNAME]
     *
     */
    @JSONField(name = "embidinquiryname")
    @JsonProperty("embidinquiryname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embidinquiryname;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [AFTERSERVICE]
     *
     */
    @JSONField(name = "afterservice")
    @JsonProperty("afterservice")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String afterservice;

    /**
     * 属性 [EMBIDRESULT]
     *
     */
    @JSONField(name = "embidresult")
    @JsonProperty("embidresult")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embidresult;

    /**
     * 属性 [NATURAL]
     *
     */
    @JSONField(name = "natural")
    @JsonProperty("natural")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String natural;

    /**
     * 属性 [EMBIDINQUIRYID]
     *
     */
    @JSONField(name = "embidinquiryid")
    @JsonProperty("embidinquiryid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String embidinquiryid;

    /**
     * 属性 [TECHPARAM]
     *
     */
    @JSONField(name = "techparam")
    @JsonProperty("techparam")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String techparam;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [EMSERVICENAME]
     *
     */
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicename;

    /**
     * 属性 [EMPURPLANNAME]
     *
     */
    @JSONField(name = "empurplanname")
    @JsonProperty("empurplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empurplanname;

    /**
     * 属性 [EMSERVICEID]
     *
     */
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceid;

    /**
     * 属性 [EMPURPLANID]
     *
     */
    @JSONField(name = "empurplanid")
    @JsonProperty("empurplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empurplanid;


    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [ADATE]
     */
    public void setAdate(Timestamp  adate){
        this.adate = adate ;
        this.modify("adate",adate);
    }

    /**
     * 设置 [TARIFF]
     */
    public void setTariff(String  tariff){
        this.tariff = tariff ;
        this.modify("tariff",tariff);
    }

    /**
     * 设置 [ACHIEVEMENT]
     */
    public void setAchievement(String  achievement){
        this.achievement = achievement ;
        this.modify("achievement",achievement);
    }

    /**
     * 设置 [EMBIDINQUIRYNAME]
     */
    public void setEmbidinquiryname(String  embidinquiryname){
        this.embidinquiryname = embidinquiryname ;
        this.modify("embidinquiryname",embidinquiryname);
    }

    /**
     * 设置 [AFTERSERVICE]
     */
    public void setAfterservice(String  afterservice){
        this.afterservice = afterservice ;
        this.modify("afterservice",afterservice);
    }

    /**
     * 设置 [NATURAL]
     */
    public void setNatural(String  natural){
        this.natural = natural ;
        this.modify("natural",natural);
    }

    /**
     * 设置 [TECHPARAM]
     */
    public void setTechparam(String  techparam){
        this.techparam = techparam ;
        this.modify("techparam",techparam);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [EMSERVICEID]
     */
    public void setEmserviceid(String  emserviceid){
        this.emserviceid = emserviceid ;
        this.modify("emserviceid",emserviceid);
    }

    /**
     * 设置 [EMPURPLANID]
     */
    public void setEmpurplanid(String  empurplanid){
        this.empurplanid = empurplanid ;
        this.modify("empurplanid",empurplanid);
    }


}


