package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMentMX;
import cn.ibizlab.eam.core.eam_core.service.IEMAssessMentMXService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMentMXSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划及项目进程考核明细" })
@RestController("WebApi-emassessmentmx")
@RequestMapping("")
public class EMAssessMentMXResource {

    @Autowired
    public IEMAssessMentMXService emassessmentmxService;

    @Autowired
    @Lazy
    public EMAssessMentMXMapping emassessmentmxMapping;

    @PreAuthorize("hasPermission(this.emassessmentmxMapping.toDomain(#emassessmentmxdto),'eam-EMAssessMentMX-Create')")
    @ApiOperation(value = "新建计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "新建计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmentmxes")
    public ResponseEntity<EMAssessMentMXDTO> create(@Validated @RequestBody EMAssessMentMXDTO emassessmentmxdto) {
        EMAssessMentMX domain = emassessmentmxMapping.toDomain(emassessmentmxdto);
		emassessmentmxService.create(domain);
        EMAssessMentMXDTO dto = emassessmentmxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessmentmxMapping.toDomain(#emassessmentmxdtos),'eam-EMAssessMentMX-Create')")
    @ApiOperation(value = "批量新建计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "批量新建计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmentmxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssessMentMXDTO> emassessmentmxdtos) {
        emassessmentmxService.createBatch(emassessmentmxMapping.toDomain(emassessmentmxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassessmentmx" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassessmentmxService.get(#emassessmentmx_id),'eam-EMAssessMentMX-Update')")
    @ApiOperation(value = "更新计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "更新计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassessmentmxes/{emassessmentmx_id}")
    public ResponseEntity<EMAssessMentMXDTO> update(@PathVariable("emassessmentmx_id") String emassessmentmx_id, @RequestBody EMAssessMentMXDTO emassessmentmxdto) {
		EMAssessMentMX domain  = emassessmentmxMapping.toDomain(emassessmentmxdto);
        domain .setEmassessmentmxid(emassessmentmx_id);
		emassessmentmxService.update(domain );
		EMAssessMentMXDTO dto = emassessmentmxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessmentmxService.getEmassessmentmxByEntities(this.emassessmentmxMapping.toDomain(#emassessmentmxdtos)),'eam-EMAssessMentMX-Update')")
    @ApiOperation(value = "批量更新计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "批量更新计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassessmentmxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssessMentMXDTO> emassessmentmxdtos) {
        emassessmentmxService.updateBatch(emassessmentmxMapping.toDomain(emassessmentmxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassessmentmxService.get(#emassessmentmx_id),'eam-EMAssessMentMX-Remove')")
    @ApiOperation(value = "删除计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "删除计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassessmentmxes/{emassessmentmx_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emassessmentmx_id") String emassessmentmx_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassessmentmxService.remove(emassessmentmx_id));
    }

    @PreAuthorize("hasPermission(this.emassessmentmxService.getEmassessmentmxByIds(#ids),'eam-EMAssessMentMX-Remove')")
    @ApiOperation(value = "批量删除计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "批量删除计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassessmentmxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassessmentmxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassessmentmxMapping.toDomain(returnObject.body),'eam-EMAssessMentMX-Get')")
    @ApiOperation(value = "获取计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "获取计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emassessmentmxes/{emassessmentmx_id}")
    public ResponseEntity<EMAssessMentMXDTO> get(@PathVariable("emassessmentmx_id") String emassessmentmx_id) {
        EMAssessMentMX domain = emassessmentmxService.get(emassessmentmx_id);
        EMAssessMentMXDTO dto = emassessmentmxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划及项目进程考核明细草稿", tags = {"计划及项目进程考核明细" },  notes = "获取计划及项目进程考核明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassessmentmxes/getdraft")
    public ResponseEntity<EMAssessMentMXDTO> getDraft(EMAssessMentMXDTO dto) {
        EMAssessMentMX domain = emassessmentmxMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emassessmentmxMapping.toDto(emassessmentmxService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "检查计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmentmxes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssessMentMXDTO emassessmentmxdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassessmentmxService.checkKey(emassessmentmxMapping.toDomain(emassessmentmxdto)));
    }

    @PreAuthorize("hasPermission(this.emassessmentmxMapping.toDomain(#emassessmentmxdto),'eam-EMAssessMentMX-Save')")
    @ApiOperation(value = "保存计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "保存计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmentmxes/save")
    public ResponseEntity<EMAssessMentMXDTO> save(@RequestBody EMAssessMentMXDTO emassessmentmxdto) {
        EMAssessMentMX domain = emassessmentmxMapping.toDomain(emassessmentmxdto);
        emassessmentmxService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassessmentmxMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassessmentmxMapping.toDomain(#emassessmentmxdtos),'eam-EMAssessMentMX-Save')")
    @ApiOperation(value = "批量保存计划及项目进程考核明细", tags = {"计划及项目进程考核明细" },  notes = "批量保存计划及项目进程考核明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmentmxes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssessMentMXDTO> emassessmentmxdtos) {
        emassessmentmxService.saveBatch(emassessmentmxMapping.toDomain(emassessmentmxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssessMentMX-searchDefault-all') and hasPermission(#context,'eam-EMAssessMentMX-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划及项目进程考核明细" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassessmentmxes/fetchdefault")
	public ResponseEntity<List<EMAssessMentMXDTO>> fetchDefault(EMAssessMentMXSearchContext context) {
        Page<EMAssessMentMX> domains = emassessmentmxService.searchDefault(context) ;
        List<EMAssessMentMXDTO> list = emassessmentmxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssessMentMX-searchDefault-all') and hasPermission(#context,'eam-EMAssessMentMX-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划及项目进程考核明细" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassessmentmxes/searchdefault")
	public ResponseEntity<Page<EMAssessMentMXDTO>> searchDefault(@RequestBody EMAssessMentMXSearchContext context) {
        Page<EMAssessMentMX> domains = emassessmentmxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassessmentmxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

