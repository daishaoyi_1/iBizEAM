package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMProjectTrackDTO]
 */
@Data
public class EMProjectTrackDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [TRACKDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "trackdate" , format="yyyy-MM-dd")
    @JsonProperty("trackdate")
    private Timestamp trackdate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EMPROJECTTRACKID]
     *
     */
    @JSONField(name = "emprojecttrackid")
    @JsonProperty("emprojecttrackid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emprojecttrackid;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [TRACKINFO]
     *
     */
    @JSONField(name = "trackinfo")
    @JsonProperty("trackinfo")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String trackinfo;

    /**
     * 属性 [EMPROJECTTRACKNAME]
     *
     */
    @JSONField(name = "emprojecttrackname")
    @JsonProperty("emprojecttrackname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emprojecttrackname;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMSERVICENAME]
     *
     */
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicename;

    /**
     * 属性 [EMPURPLANNAME]
     *
     */
    @JSONField(name = "empurplanname")
    @JsonProperty("empurplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empurplanname;

    /**
     * 属性 [EMSERVICEID]
     *
     */
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @NotBlank(message = "[服务商]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceid;

    /**
     * 属性 [EMPURPLANID]
     *
     */
    @JSONField(name = "empurplanid")
    @JsonProperty("empurplanid")
    @NotBlank(message = "[项目内容]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empurplanid;


    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [TRACKDATE]
     */
    public void setTrackdate(Timestamp  trackdate){
        this.trackdate = trackdate ;
        this.modify("trackdate",trackdate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [TRACKINFO]
     */
    public void setTrackinfo(String  trackinfo){
        this.trackinfo = trackinfo ;
        this.modify("trackinfo",trackinfo);
    }

    /**
     * 设置 [EMPROJECTTRACKNAME]
     */
    public void setEmprojecttrackname(String  emprojecttrackname){
        this.emprojecttrackname = emprojecttrackname ;
        this.modify("emprojecttrackname",emprojecttrackname);
    }

    /**
     * 设置 [EMSERVICEID]
     */
    public void setEmserviceid(String  emserviceid){
        this.emserviceid = emserviceid ;
        this.modify("emserviceid",emserviceid);
    }

    /**
     * 设置 [EMPURPLANID]
     */
    public void setEmpurplanid(String  empurplanid){
        this.empurplanid = empurplanid ;
        this.modify("empurplanid",empurplanid);
    }


}


