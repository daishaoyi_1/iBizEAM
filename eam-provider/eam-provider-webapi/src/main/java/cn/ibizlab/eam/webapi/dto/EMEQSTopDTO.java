package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQSTopDTO]
 */
@Data
public class EMEQSTopDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DAY28]
     *
     */
    @JSONField(name = "day28")
    @JsonProperty("day28")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day28;

    /**
     * 属性 [DAY3]
     *
     */
    @JSONField(name = "day3")
    @JsonProperty("day3")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day3;

    /**
     * 属性 [DAY17]
     *
     */
    @JSONField(name = "day17")
    @JsonProperty("day17")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day17;

    /**
     * 属性 [DAY12]
     *
     */
    @JSONField(name = "day12")
    @JsonProperty("day12")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day12;

    /**
     * 属性 [DAY6]
     *
     */
    @JSONField(name = "day6")
    @JsonProperty("day6")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day6;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DAY26]
     *
     */
    @JSONField(name = "day26")
    @JsonProperty("day26")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day26;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [DAY7]
     *
     */
    @JSONField(name = "day7")
    @JsonProperty("day7")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day7;

    /**
     * 属性 [DAY4]
     *
     */
    @JSONField(name = "day4")
    @JsonProperty("day4")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day4;

    /**
     * 属性 [HSUM]
     *
     */
    @JSONField(name = "hsum")
    @JsonProperty("hsum")
    private BigDecimal hsum;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [DAY1]
     *
     */
    @JSONField(name = "day1")
    @JsonProperty("day1")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day1;

    /**
     * 属性 [DAY21]
     *
     */
    @JSONField(name = "day21")
    @JsonProperty("day21")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day21;

    /**
     * 属性 [DAY25]
     *
     */
    @JSONField(name = "day25")
    @JsonProperty("day25")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day25;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [DAY19]
     *
     */
    @JSONField(name = "day19")
    @JsonProperty("day19")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day19;

    /**
     * 属性 [DAY30]
     *
     */
    @JSONField(name = "day30")
    @JsonProperty("day30")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day30;

    /**
     * 属性 [DAY11]
     *
     */
    @JSONField(name = "day11")
    @JsonProperty("day11")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day11;

    /**
     * 属性 [DAY29]
     *
     */
    @JSONField(name = "day29")
    @JsonProperty("day29")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day29;

    /**
     * 属性 [DAY16]
     *
     */
    @JSONField(name = "day16")
    @JsonProperty("day16")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day16;

    /**
     * 属性 [DAY8]
     *
     */
    @JSONField(name = "day8")
    @JsonProperty("day8")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day8;

    /**
     * 属性 [EMEQSTOPID]
     *
     */
    @JSONField(name = "emeqstopid")
    @JsonProperty("emeqstopid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqstopid;

    /**
     * 属性 [DAY14]
     *
     */
    @JSONField(name = "day14")
    @JsonProperty("day14")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day14;

    /**
     * 属性 [DAY10]
     *
     */
    @JSONField(name = "day10")
    @JsonProperty("day10")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day10;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [DAY15]
     *
     */
    @JSONField(name = "day15")
    @JsonProperty("day15")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day15;

    /**
     * 属性 [DAY24]
     *
     */
    @JSONField(name = "day24")
    @JsonProperty("day24")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day24;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [DAY5]
     *
     */
    @JSONField(name = "day5")
    @JsonProperty("day5")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day5;

    /**
     * 属性 [DAY22]
     *
     */
    @JSONField(name = "day22")
    @JsonProperty("day22")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day22;

    /**
     * 属性 [DAY18]
     *
     */
    @JSONField(name = "day18")
    @JsonProperty("day18")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day18;

    /**
     * 属性 [DAY23]
     *
     */
    @JSONField(name = "day23")
    @JsonProperty("day23")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day23;

    /**
     * 属性 [DAY27]
     *
     */
    @JSONField(name = "day27")
    @JsonProperty("day27")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day27;

    /**
     * 属性 [DAY20]
     *
     */
    @JSONField(name = "day20")
    @JsonProperty("day20")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day20;

    /**
     * 属性 [DAY31]
     *
     */
    @JSONField(name = "day31")
    @JsonProperty("day31")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day31;

    /**
     * 属性 [EMEQSTOPNAME]
     *
     */
    @JSONField(name = "emeqstopname")
    @JsonProperty("emeqstopname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqstopname;

    /**
     * 属性 [MSUM]
     *
     */
    @JSONField(name = "msum")
    @JsonProperty("msum")
    private BigDecimal msum;

    /**
     * 属性 [DAY9]
     *
     */
    @JSONField(name = "day9")
    @JsonProperty("day9")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day9;

    /**
     * 属性 [DAY13]
     *
     */
    @JSONField(name = "day13")
    @JsonProperty("day13")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day13;

    /**
     * 属性 [DAY2]
     *
     */
    @JSONField(name = "day2")
    @JsonProperty("day2")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String day2;

    /**
     * 属性 [EMEQTYPENAME]
     *
     */
    @JSONField(name = "emeqtypename")
    @JsonProperty("emeqtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqtypename;

    /**
     * 属性 [EMEQUIPNAME]
     *
     */
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emequipname;

    /**
     * 属性 [EMEQTYPEID]
     *
     */
    @JSONField(name = "emeqtypeid")
    @JsonProperty("emeqtypeid")
    @NotBlank(message = "[设备类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqtypeid;

    /**
     * 属性 [EMEQUIPID]
     *
     */
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    @NotBlank(message = "[设备]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emequipid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMEQSTOPNAME]
     */
    public void setEmeqstopname(String  emeqstopname){
        this.emeqstopname = emeqstopname ;
        this.modify("emeqstopname",emeqstopname);
    }

    /**
     * 设置 [EMEQTYPEID]
     */
    public void setEmeqtypeid(String  emeqtypeid){
        this.emeqtypeid = emeqtypeid ;
        this.modify("emeqtypeid",emeqtypeid);
    }

    /**
     * 设置 [EMEQUIPID]
     */
    public void setEmequipid(String  emequipid){
        this.emequipid = emequipid ;
        this.modify("emequipid",emequipid);
    }


}


