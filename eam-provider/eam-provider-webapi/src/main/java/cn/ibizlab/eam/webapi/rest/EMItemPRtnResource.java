package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;
import cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPRtnSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"还料单" })
@RestController("WebApi-emitemprtn")
@RequestMapping("")
public class EMItemPRtnResource {

    @Autowired
    public IEMItemPRtnService emitemprtnService;

    @Autowired
    @Lazy
    public EMItemPRtnMapping emitemprtnMapping;

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "新建还料单", tags = {"还料单" },  notes = "新建还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns")
    public ResponseEntity<EMItemPRtnDTO> create(@Validated @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
		emitemprtnService.create(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "批量新建还料单", tags = {"还料单" },  notes = "批量新建还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        emitemprtnService.createBatch(emitemprtnMapping.toDomain(emitemprtndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemprtn" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "更新还料单", tags = {"还料单" },  notes = "更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> update(@PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
		EMItemPRtn domain  = emitemprtnMapping.toDomain(emitemprtndto);
        domain .setEmitemprtnid(emitemprtn_id);
		emitemprtnService.update(domain );
		EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByEntities(this.emitemprtnMapping.toDomain(#emitemprtndtos)),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "批量更新还料单", tags = {"还料单" },  notes = "批量更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemprtns/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        emitemprtnService.updateBatch(emitemprtnMapping.toDomain(emitemprtndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "删除还料单", tags = {"还料单" },  notes = "删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitemprtn_id") String emitemprtn_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.remove(emitemprtn_id));
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByIds(#ids),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "批量删除还料单", tags = {"还料单" },  notes = "批量删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemprtns/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemprtnService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemprtnMapping.toDomain(returnObject.body),'eam-EMItemPRtn-Get')")
    @ApiOperation(value = "获取还料单", tags = {"还料单" },  notes = "获取还料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> get(@PathVariable("emitemprtn_id") String emitemprtn_id) {
        EMItemPRtn domain = emitemprtnService.get(emitemprtn_id);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取还料单草稿", tags = {"还料单" },  notes = "获取还料单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemprtns/getdraft")
    public ResponseEntity<EMItemPRtnDTO> getDraft(EMItemPRtnDTO dto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(emitemprtnService.getDraft(domain)));
    }

    @ApiOperation(value = "检查还料单", tags = {"还料单" },  notes = "检查还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemPRtnDTO emitemprtndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.checkKey(emitemprtnMapping.toDomain(emitemprtndto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Confirm-all')")
    @ApiOperation(value = "确认", tags = {"还料单" },  notes = "确认")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns/{emitemprtn_id}/confirm")
    public ResponseEntity<EMItemPRtnDTO> confirm(@PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setEmitemprtnid(emitemprtn_id);
        domain = emitemprtnService.confirm(domain);
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Confirm-all')")
    @ApiOperation(value = "批量处理[确认]", tags = {"还料单" },  notes = "批量处理[确认]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns/confirmbatch")
    public ResponseEntity<Boolean> confirmBatch(@RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domains = emitemprtnMapping.toDomain(emitemprtndtos);
        boolean result = emitemprtnService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-FormUpdateByEmitempuseid-all')")
    @ApiOperation(value = "表单跟新-Emitempuseid", tags = {"还料单" },  notes = "表单跟新-Emitempuseid")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemprtns/{emitemprtn_id}/formupdatebyemitempuseid")
    public ResponseEntity<EMItemPRtnDTO> formUpdateByEmitempuseid(@PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setEmitemprtnid(emitemprtn_id);
        domain = emitemprtnService.formUpdateByEmitempuseid(domain);
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Rejected-all')")
    @ApiOperation(value = "驳回", tags = {"还料单" },  notes = "驳回")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns/{emitemprtn_id}/rejected")
    public ResponseEntity<EMItemPRtnDTO> rejected(@PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setEmitemprtnid(emitemprtn_id);
        domain = emitemprtnService.rejected(domain);
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "保存还料单", tags = {"还料单" },  notes = "保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns/save")
    public ResponseEntity<EMItemPRtnDTO> save(@RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        emitemprtnService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "批量保存还料单", tags = {"还料单" },  notes = "批量保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemprtns/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        emitemprtnService.saveBatch(emitemprtnMapping.toDomain(emitemprtndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "获取已确认", tags = {"还料单" } ,notes = "获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitemprtns/fetchconfirmed")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchConfirmed(EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "查询已确认", tags = {"还料单" } ,notes = "查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitemprtns/searchconfirmed")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchConfirmed(@RequestBody EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"还料单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitemprtns/fetchdefault")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchDefault(EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"还料单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitemprtns/searchdefault")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchDefault(@RequestBody EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "获取草稿", tags = {"还料单" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitemprtns/fetchdraft")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchDraft(EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "查询草稿", tags = {"还料单" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitemprtns/searchdraft")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchDraft(@RequestBody EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "获取待确认", tags = {"还料单" } ,notes = "获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitemprtns/fetchtoconfirm")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchToConfirm(EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "查询待确认", tags = {"还料单" } ,notes = "查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitemprtns/searchtoconfirm")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchToConfirm(@RequestBody EMItemPRtnSearchContext context) {
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据物品建立还料单", tags = {"还料单" },  notes = "根据物品建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns")
    public ResponseEntity<EMItemPRtnDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
		emitemprtnService.create(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据物品批量建立还料单", tags = {"还料单" },  notes = "根据物品批量建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemprtn" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据物品更新还料单", tags = {"还料单" },  notes = "根据物品更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain.setEmitemprtnid(emitemprtn_id);
		emitemprtnService.update(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByEntities(this.emitemprtnMapping.toDomain(#emitemprtndtos)),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据物品批量更新还料单", tags = {"还料单" },  notes = "根据物品批量更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据物品删除还料单", tags = {"还料单" },  notes = "根据物品删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.remove(emitemprtn_id));
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByIds(#ids),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据物品批量删除还料单", tags = {"还料单" },  notes = "根据物品批量删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emitemprtnService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemprtnMapping.toDomain(returnObject.body),'eam-EMItemPRtn-Get')")
    @ApiOperation(value = "根据物品获取还料单", tags = {"还料单" },  notes = "根据物品获取还料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
        EMItemPRtn domain = emitemprtnService.get(emitemprtn_id);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取还料单草稿", tags = {"还料单" },  notes = "根据物品获取还料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemprtns/getdraft")
    public ResponseEntity<EMItemPRtnDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMItemPRtnDTO dto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(emitemprtnService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查还料单", tags = {"还料单" },  notes = "根据物品检查还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.checkKey(emitemprtnMapping.toDomain(emitemprtndto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Confirm-all')")
    @ApiOperation(value = "根据物品还料单", tags = {"还料单" },  notes = "根据物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/confirm")
    public ResponseEntity<EMItemPRtnDTO> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.confirm(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @ApiOperation(value = "批量处理[根据物品还料单]", tags = {"还料单" },  notes = "批量处理[根据物品还料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domains = emitemprtnMapping.toDomain(emitemprtndtos);
        boolean result = emitemprtnService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-FormUpdateByEmitempuseid-all')")
    @ApiOperation(value = "根据物品还料单", tags = {"还料单" },  notes = "根据物品还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/formupdatebyemitempuseid")
    public ResponseEntity<EMItemPRtnDTO> formUpdateByEmitempuseidByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.formUpdateByEmitempuseid(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Rejected-all')")
    @ApiOperation(value = "根据物品还料单", tags = {"还料单" },  notes = "根据物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/rejected")
    public ResponseEntity<EMItemPRtnDTO> rejectedByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.rejected(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据物品保存还料单", tags = {"还料单" },  notes = "根据物品保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns/save")
    public ResponseEntity<EMItemPRtnDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        emitemprtnService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据物品批量保存还料单", tags = {"还料单" },  notes = "根据物品批量保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemprtns/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemprtnService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品获取已确认", tags = {"还料单" } ,notes = "根据物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemprtns/fetchconfirmed")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnConfirmedByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品查询已确认", tags = {"还料单" } ,notes = "根据物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemprtns/searchconfirmed")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnConfirmedByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"还料单" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemprtns/fetchdefault")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"还料单" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemprtns/searchdefault")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品获取草稿", tags = {"还料单" } ,notes = "根据物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemprtns/fetchdraft")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDraftByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品查询草稿", tags = {"还料单" } ,notes = "根据物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemprtns/searchdraft")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDraftByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品获取待确认", tags = {"还料单" } ,notes = "根据物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemprtns/fetchtoconfirm")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnToConfirmByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据物品查询待确认", tags = {"还料单" } ,notes = "根据物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemprtns/searchtoconfirm")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnToConfirmByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据服务商物品建立还料单", tags = {"还料单" },  notes = "根据服务商物品建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns")
    public ResponseEntity<EMItemPRtnDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
		emitemprtnService.create(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据服务商物品批量建立还料单", tags = {"还料单" },  notes = "根据服务商物品批量建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemprtn" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据服务商物品更新还料单", tags = {"还料单" },  notes = "根据服务商物品更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain.setEmitemprtnid(emitemprtn_id);
		emitemprtnService.update(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByEntities(this.emitemprtnMapping.toDomain(#emitemprtndtos)),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据服务商物品批量更新还料单", tags = {"还料单" },  notes = "根据服务商物品批量更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据服务商物品删除还料单", tags = {"还料单" },  notes = "根据服务商物品删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.remove(emitemprtn_id));
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByIds(#ids),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除还料单", tags = {"还料单" },  notes = "根据服务商物品批量删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emitemprtnService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemprtnMapping.toDomain(returnObject.body),'eam-EMItemPRtn-Get')")
    @ApiOperation(value = "根据服务商物品获取还料单", tags = {"还料单" },  notes = "根据服务商物品获取还料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
        EMItemPRtn domain = emitemprtnService.get(emitemprtn_id);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取还料单草稿", tags = {"还料单" },  notes = "根据服务商物品获取还料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/getdraft")
    public ResponseEntity<EMItemPRtnDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMItemPRtnDTO dto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(emitemprtnService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查还料单", tags = {"还料单" },  notes = "根据服务商物品检查还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.checkKey(emitemprtnMapping.toDomain(emitemprtndto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Confirm-all')")
    @ApiOperation(value = "根据服务商物品还料单", tags = {"还料单" },  notes = "根据服务商物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/confirm")
    public ResponseEntity<EMItemPRtnDTO> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.confirm(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @ApiOperation(value = "批量处理[根据服务商物品还料单]", tags = {"还料单" },  notes = "批量处理[根据服务商物品还料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domains = emitemprtnMapping.toDomain(emitemprtndtos);
        boolean result = emitemprtnService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-FormUpdateByEmitempuseid-all')")
    @ApiOperation(value = "根据服务商物品还料单", tags = {"还料单" },  notes = "根据服务商物品还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/formupdatebyemitempuseid")
    public ResponseEntity<EMItemPRtnDTO> formUpdateByEmitempuseidByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.formUpdateByEmitempuseid(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Rejected-all')")
    @ApiOperation(value = "根据服务商物品还料单", tags = {"还料单" },  notes = "根据服务商物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/rejected")
    public ResponseEntity<EMItemPRtnDTO> rejectedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.rejected(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据服务商物品保存还料单", tags = {"还料单" },  notes = "根据服务商物品保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/save")
    public ResponseEntity<EMItemPRtnDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        emitemprtnService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据服务商物品批量保存还料单", tags = {"还料单" },  notes = "根据服务商物品批量保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemprtnService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品获取已确认", tags = {"还料单" } ,notes = "根据服务商物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/fetchconfirmed")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnConfirmedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品查询已确认", tags = {"还料单" } ,notes = "根据服务商物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/searchconfirmed")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnConfirmedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"还料单" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/fetchdefault")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"还料单" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/searchdefault")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品获取草稿", tags = {"还料单" } ,notes = "根据服务商物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/fetchdraft")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品查询草稿", tags = {"还料单" } ,notes = "根据服务商物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/searchdraft")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品获取待确认", tags = {"还料单" } ,notes = "根据服务商物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/fetchtoconfirm")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnToConfirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据服务商物品查询待确认", tags = {"还料单" } ,notes = "根据服务商物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemprtns/searchtoconfirm")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnToConfirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据仓库库位物品建立还料单", tags = {"还料单" },  notes = "根据仓库库位物品建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns")
    public ResponseEntity<EMItemPRtnDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
		emitemprtnService.create(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立还料单", tags = {"还料单" },  notes = "根据仓库库位物品批量建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemprtn" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据仓库库位物品更新还料单", tags = {"还料单" },  notes = "根据仓库库位物品更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain.setEmitemprtnid(emitemprtn_id);
		emitemprtnService.update(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByEntities(this.emitemprtnMapping.toDomain(#emitemprtndtos)),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新还料单", tags = {"还料单" },  notes = "根据仓库库位物品批量更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除还料单", tags = {"还料单" },  notes = "根据仓库库位物品删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.remove(emitemprtn_id));
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByIds(#ids),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除还料单", tags = {"还料单" },  notes = "根据仓库库位物品批量删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemprtnService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemprtnMapping.toDomain(returnObject.body),'eam-EMItemPRtn-Get')")
    @ApiOperation(value = "根据仓库库位物品获取还料单", tags = {"还料单" },  notes = "根据仓库库位物品获取还料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
        EMItemPRtn domain = emitemprtnService.get(emitemprtn_id);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取还料单草稿", tags = {"还料单" },  notes = "根据仓库库位物品获取还料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/getdraft")
    public ResponseEntity<EMItemPRtnDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemPRtnDTO dto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(emitemprtnService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查还料单", tags = {"还料单" },  notes = "根据仓库库位物品检查还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.checkKey(emitemprtnMapping.toDomain(emitemprtndto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Confirm-all')")
    @ApiOperation(value = "根据仓库库位物品还料单", tags = {"还料单" },  notes = "根据仓库库位物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/confirm")
    public ResponseEntity<EMItemPRtnDTO> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.confirm(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位物品还料单]", tags = {"还料单" },  notes = "批量处理[根据仓库库位物品还料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domains = emitemprtnMapping.toDomain(emitemprtndtos);
        boolean result = emitemprtnService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-FormUpdateByEmitempuseid-all')")
    @ApiOperation(value = "根据仓库库位物品还料单", tags = {"还料单" },  notes = "根据仓库库位物品还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/formupdatebyemitempuseid")
    public ResponseEntity<EMItemPRtnDTO> formUpdateByEmitempuseidByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.formUpdateByEmitempuseid(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Rejected-all')")
    @ApiOperation(value = "根据仓库库位物品还料单", tags = {"还料单" },  notes = "根据仓库库位物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/rejected")
    public ResponseEntity<EMItemPRtnDTO> rejectedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.rejected(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据仓库库位物品保存还料单", tags = {"还料单" },  notes = "根据仓库库位物品保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/save")
    public ResponseEntity<EMItemPRtnDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        emitemprtnService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存还料单", tags = {"还料单" },  notes = "根据仓库库位物品批量保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemprtnService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已确认", tags = {"还料单" } ,notes = "根据仓库库位物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchconfirmed")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnConfirmedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已确认", tags = {"还料单" } ,notes = "根据仓库库位物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchconfirmed")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnConfirmedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"还料单" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchdefault")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"还料单" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchdefault")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品获取草稿", tags = {"还料单" } ,notes = "根据仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchdraft")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品查询草稿", tags = {"还料单" } ,notes = "根据仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchdraft")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待确认", tags = {"还料单" } ,notes = "根据仓库库位物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchtoconfirm")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnToConfirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待确认", tags = {"还料单" } ,notes = "根据仓库库位物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchtoconfirm")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnToConfirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据仓库物品建立还料单", tags = {"还料单" },  notes = "根据仓库物品建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns")
    public ResponseEntity<EMItemPRtnDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
		emitemprtnService.create(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据仓库物品批量建立还料单", tags = {"还料单" },  notes = "根据仓库物品批量建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemprtn" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据仓库物品更新还料单", tags = {"还料单" },  notes = "根据仓库物品更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain.setEmitemprtnid(emitemprtn_id);
		emitemprtnService.update(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByEntities(this.emitemprtnMapping.toDomain(#emitemprtndtos)),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据仓库物品批量更新还料单", tags = {"还料单" },  notes = "根据仓库物品批量更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据仓库物品删除还料单", tags = {"还料单" },  notes = "根据仓库物品删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.remove(emitemprtn_id));
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByIds(#ids),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除还料单", tags = {"还料单" },  notes = "根据仓库物品批量删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emitemprtnService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemprtnMapping.toDomain(returnObject.body),'eam-EMItemPRtn-Get')")
    @ApiOperation(value = "根据仓库物品获取还料单", tags = {"还料单" },  notes = "根据仓库物品获取还料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
        EMItemPRtn domain = emitemprtnService.get(emitemprtn_id);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取还料单草稿", tags = {"还料单" },  notes = "根据仓库物品获取还料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/getdraft")
    public ResponseEntity<EMItemPRtnDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMItemPRtnDTO dto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(emitemprtnService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查还料单", tags = {"还料单" },  notes = "根据仓库物品检查还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.checkKey(emitemprtnMapping.toDomain(emitemprtndto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Confirm-all')")
    @ApiOperation(value = "根据仓库物品还料单", tags = {"还料单" },  notes = "根据仓库物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/confirm")
    public ResponseEntity<EMItemPRtnDTO> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.confirm(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @ApiOperation(value = "批量处理[根据仓库物品还料单]", tags = {"还料单" },  notes = "批量处理[根据仓库物品还料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domains = emitemprtnMapping.toDomain(emitemprtndtos);
        boolean result = emitemprtnService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-FormUpdateByEmitempuseid-all')")
    @ApiOperation(value = "根据仓库物品还料单", tags = {"还料单" },  notes = "根据仓库物品还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/formupdatebyemitempuseid")
    public ResponseEntity<EMItemPRtnDTO> formUpdateByEmitempuseidByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.formUpdateByEmitempuseid(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Rejected-all')")
    @ApiOperation(value = "根据仓库物品还料单", tags = {"还料单" },  notes = "根据仓库物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/rejected")
    public ResponseEntity<EMItemPRtnDTO> rejectedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.rejected(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据仓库物品保存还料单", tags = {"还料单" },  notes = "根据仓库物品保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/save")
    public ResponseEntity<EMItemPRtnDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        emitemprtnService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据仓库物品批量保存还料单", tags = {"还料单" },  notes = "根据仓库物品批量保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemprtnService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品获取已确认", tags = {"还料单" } ,notes = "根据仓库物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/fetchconfirmed")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnConfirmedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品查询已确认", tags = {"还料单" } ,notes = "根据仓库物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/searchconfirmed")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnConfirmedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"还料单" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/fetchdefault")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"还料单" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/searchdefault")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品获取草稿", tags = {"还料单" } ,notes = "根据仓库物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/fetchdraft")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品查询草稿", tags = {"还料单" } ,notes = "根据仓库物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/searchdraft")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品获取待确认", tags = {"还料单" } ,notes = "根据仓库物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/fetchtoconfirm")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnToConfirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库物品查询待确认", tags = {"还料单" } ,notes = "根据仓库物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemprtns/searchtoconfirm")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnToConfirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns")
    public ResponseEntity<EMItemPRtnDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
		emitemprtnService.create(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品批量建立还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemprtn" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain.setEmitemprtnid(emitemprtn_id);
		emitemprtnService.update(domain);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByEntities(this.emitemprtnMapping.toDomain(#emitemprtndtos)),'eam-EMItemPRtn-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品批量更新还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemprtnService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.get(#emitemprtn_id),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.remove(emitemprtn_id));
    }

    @PreAuthorize("hasPermission(this.emitemprtnService.getEmitemprtnByIds(#ids),'eam-EMItemPRtn-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品批量删除还料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemprtnService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemprtnMapping.toDomain(returnObject.body),'eam-EMItemPRtn-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品获取还料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}")
    public ResponseEntity<EMItemPRtnDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id) {
        EMItemPRtn domain = emitemprtnService.get(emitemprtn_id);
        EMItemPRtnDTO dto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取还料单草稿", tags = {"还料单" },  notes = "根据仓库仓库库位物品获取还料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/getdraft")
    public ResponseEntity<EMItemPRtnDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemPRtnDTO dto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(emitemprtnService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品检查还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemprtnService.checkKey(emitemprtnMapping.toDomain(emitemprtndto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Confirm-all')")
    @ApiOperation(value = "根据仓库仓库库位物品还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/confirm")
    public ResponseEntity<EMItemPRtnDTO> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.confirm(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位物品还料单]", tags = {"还料单" },  notes = "批量处理[根据仓库仓库库位物品还料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domains = emitemprtnMapping.toDomain(emitemprtndtos);
        boolean result = emitemprtnService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-FormUpdateByEmitempuseid-all')")
    @ApiOperation(value = "根据仓库仓库库位物品还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品还料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/formupdatebyemitempuseid")
    public ResponseEntity<EMItemPRtnDTO> formUpdateByEmitempuseidByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.formUpdateByEmitempuseid(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-Rejected-all')")
    @ApiOperation(value = "根据仓库仓库库位物品还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/{emitemprtn_id}/rejected")
    public ResponseEntity<EMItemPRtnDTO> rejectedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemprtn_id") String emitemprtn_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        domain = emitemprtnService.rejected(domain) ;
        emitemprtndto = emitemprtnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtndto);
    }
    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndto),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/save")
    public ResponseEntity<EMItemPRtnDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnDTO emitemprtndto) {
        EMItemPRtn domain = emitemprtnMapping.toDomain(emitemprtndto);
        domain.setItemid(emitem_id);
        emitemprtnService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemprtnMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemprtnMapping.toDomain(#emitemprtndtos),'eam-EMItemPRtn-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存还料单", tags = {"还料单" },  notes = "根据仓库仓库库位物品批量保存还料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPRtnDTO> emitemprtndtos) {
        List<EMItemPRtn> domainlist=emitemprtnMapping.toDomain(emitemprtndtos);
        for(EMItemPRtn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemprtnService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已确认", tags = {"还料单" } ,notes = "根据仓库仓库库位物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchconfirmed")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnConfirmedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已确认", tags = {"还料单" } ,notes = "根据仓库仓库库位物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchconfirmed")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnConfirmedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"还料单" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchdefault")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDefault-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"还料单" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchdefault")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取草稿", tags = {"还料单" } ,notes = "根据仓库仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchdraft")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchDraft-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询草稿", tags = {"还料单" } ,notes = "根据仓库仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchdraft")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待确认", tags = {"还料单" } ,notes = "根据仓库仓库库位物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/fetchtoconfirm")
	public ResponseEntity<List<EMItemPRtnDTO>> fetchEMItemPRtnToConfirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
        List<EMItemPRtnDTO> list = emitemprtnMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPRtn-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPRtn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待确认", tags = {"还料单" } ,notes = "根据仓库仓库库位物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemprtns/searchtoconfirm")
	public ResponseEntity<Page<EMItemPRtnDTO>> searchEMItemPRtnToConfirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPRtnSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPRtn> domains = emitemprtnService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemprtnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

