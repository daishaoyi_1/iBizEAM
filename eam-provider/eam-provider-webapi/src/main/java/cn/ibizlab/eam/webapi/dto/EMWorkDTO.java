package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMWorkDTO]
 */
@Data
public class EMWorkDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [JBGZNR]
     *
     */
    @JSONField(name = "jbgznr")
    @JsonProperty("jbgznr")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jbgznr;

    /**
     * 属性 [WORKDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "workdate" , format="yyyy-MM-dd")
    @JsonProperty("workdate")
    private Timestamp workdate;

    /**
     * 属性 [OVERTIME]
     *
     */
    @JSONField(name = "overtime")
    @JsonProperty("overtime")
    private Double overtime;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empname;

    /**
     * 属性 [WORKSTATE]
     *
     */
    @JSONField(name = "workstate")
    @JsonProperty("workstate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String workstate;

    /**
     * 属性 [AMENDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "amendtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("amendtime")
    private Timestamp amendtime;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [AMBEGINTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "ambegintime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("ambegintime")
    private Timestamp ambegintime;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMWORKSTATE]
     *
     */
    @JSONField(name = "emworkstate")
    @JsonProperty("emworkstate")
    private Integer emworkstate;

    /**
     * 属性 [EMWORKNAME]
     *
     */
    @JSONField(name = "emworkname")
    @JsonProperty("emworkname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emworkname;

    /**
     * 属性 [PMENDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "pmendtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("pmendtime")
    private Timestamp pmendtime;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMWORKID]
     *
     */
    @JSONField(name = "emworkid")
    @JsonProperty("emworkid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emworkid;

    /**
     * 属性 [POST]
     *
     */
    @JSONField(name = "post")
    @JsonProperty("post")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String post;

    /**
     * 属性 [WORKPLACE]
     *
     */
    @JSONField(name = "workplace")
    @JsonProperty("workplace")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String workplace;

    /**
     * 属性 [PMBEGINTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "pmbegintime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("pmbegintime")
    private Timestamp pmbegintime;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;


    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [JBGZNR]
     */
    public void setJbgznr(String  jbgznr){
        this.jbgznr = jbgznr ;
        this.modify("jbgznr",jbgznr);
    }

    /**
     * 设置 [WORKDATE]
     */
    public void setWorkdate(Timestamp  workdate){
        this.workdate = workdate ;
        this.modify("workdate",workdate);
    }

    /**
     * 设置 [OVERTIME]
     */
    public void setOvertime(Double  overtime){
        this.overtime = overtime ;
        this.modify("overtime",overtime);
    }

    /**
     * 设置 [EMPNAME]
     */
    public void setEmpname(String  empname){
        this.empname = empname ;
        this.modify("empname",empname);
    }

    /**
     * 设置 [WORKSTATE]
     */
    public void setWorkstate(String  workstate){
        this.workstate = workstate ;
        this.modify("workstate",workstate);
    }

    /**
     * 设置 [AMENDTIME]
     */
    public void setAmendtime(Timestamp  amendtime){
        this.amendtime = amendtime ;
        this.modify("amendtime",amendtime);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [AMBEGINTIME]
     */
    public void setAmbegintime(Timestamp  ambegintime){
        this.ambegintime = ambegintime ;
        this.modify("ambegintime",ambegintime);
    }

    /**
     * 设置 [EMWORKSTATE]
     */
    public void setEmworkstate(Integer  emworkstate){
        this.emworkstate = emworkstate ;
        this.modify("emworkstate",emworkstate);
    }

    /**
     * 设置 [EMWORKNAME]
     */
    public void setEmworkname(String  emworkname){
        this.emworkname = emworkname ;
        this.modify("emworkname",emworkname);
    }

    /**
     * 设置 [PMENDTIME]
     */
    public void setPmendtime(Timestamp  pmendtime){
        this.pmendtime = pmendtime ;
        this.modify("pmendtime",pmendtime);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [POST]
     */
    public void setPost(String  post){
        this.post = post ;
        this.modify("post",post);
    }

    /**
     * 设置 [WORKPLACE]
     */
    public void setWorkplace(String  workplace){
        this.workplace = workplace ;
        this.modify("workplace",workplace);
    }

    /**
     * 设置 [PMBEGINTIME]
     */
    public void setPmbegintime(Timestamp  pmbegintime){
        this.pmbegintime = pmbegintime ;
        this.modify("pmbegintime",pmbegintime);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }


}


