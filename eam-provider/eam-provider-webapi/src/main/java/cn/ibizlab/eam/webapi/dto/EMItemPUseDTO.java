package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMItemPUseDTO]
 */
@Data
public class EMItemPUseDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [REMARK]
     *
     */
    @JSONField(name = "remark")
    @JsonProperty("remark")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String remark;

    /**
     * 属性 [EMITEMPUSENAME]
     *
     */
    @JSONField(name = "emitempusename")
    @JsonProperty("emitempusename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitempusename;

    /**
     * 属性 [ITEMPUSEINFO]
     *
     */
    @JSONField(name = "itempuseinfo")
    @JsonProperty("itempuseinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itempuseinfo;

    /**
     * 属性 [DELTYPE]
     *
     */
    @JSONField(name = "deltype")
    @JsonProperty("deltype")
    private Integer deltype;

    /**
     * 属性 [OPINION]
     *
     */
    @JSONField(name = "opinion")
    @JsonProperty("opinion")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String opinion;

    /**
     * 属性 [STOCKNUM]
     *
     */
    @JSONField(name = "stocknum")
    @JsonProperty("stocknum")
    private Double stocknum;

    /**
     * 属性 [APPROKNUM]
     *
     */
    @JSONField(name = "approknum")
    @JsonProperty("approknum")
    private Integer approknum;

    /**
     * 属性 [EQUIPS]
     *
     */
    @JSONField(name = "equips")
    @JsonProperty("equips")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String equips;

    /**
     * 属性 [PSUM]
     *
     */
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [BATCODE]
     *
     */
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String batcode;

    /**
     * 属性 [BZ]
     *
     */
    @JSONField(name = "bz")
    @JsonProperty("bz")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String bz;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [SAPREASON1]
     *
     */
    @JSONField(name = "sapreason1")
    @JsonProperty("sapreason1")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sapreason1;

    /**
     * 属性 [PUSESTATE]
     *
     */
    @JSONField(name = "pusestate")
    @JsonProperty("pusestate")
    private Integer pusestate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [APPRDESC]
     *
     */
    @JSONField(name = "apprdesc")
    @JsonProperty("apprdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String apprdesc;

    /**
     * 属性 [SAPCBZX]
     *
     */
    @JSONField(name = "sapcbzx")
    @JsonProperty("sapcbzx")
    @NotBlank(message = "[sap成本中心]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sapcbzx;

    /**
     * 属性 [SAPLLYT]
     *
     */
    @JSONField(name = "sapllyt")
    @JsonProperty("sapllyt")
    @NotBlank(message = "[sap领料用途]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sapllyt;

    /**
     * 属性 [NUMDIFF]
     *
     */
    @JSONField(name = "numdiff")
    @JsonProperty("numdiff")
    private Double numdiff;

    /**
     * 属性 [ASUM]
     *
     */
    @JSONField(name = "asum")
    @JsonProperty("asum")
    @NotNull(message = "[请领数]不允许为空!")
    private Double asum;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [SAP]
     *
     */
    @JSONField(name = "sap")
    @JsonProperty("sap")
    private Integer sap;

    /**
     * 属性 [SDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    private Timestamp sdate;

    /**
     * 属性 [LIFE2]
     *
     */
    @JSONField(name = "life2")
    @JsonProperty("life2")
    private Integer life2;

    /**
     * 属性 [EMITEMPUSEID]
     *
     */
    @JSONField(name = "emitempuseid")
    @JsonProperty("emitempuseid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitempuseid;

    /**
     * 属性 [STOCK2NUM]
     *
     */
    @JSONField(name = "stock2num")
    @JsonProperty("stock2num")
    private Double stock2num;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [USETO]
     *
     */
    @JSONField(name = "useto")
    @JsonProperty("useto")
    @NotBlank(message = "[用途]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String useto;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [PUSETYPE]
     *
     */
    @JSONField(name = "pusetype")
    @JsonProperty("pusetype")
    @NotBlank(message = "[领料分类]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pusetype;

    /**
     * 属性 [SAPCONTROL]
     *
     */
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    private Integer sapcontrol;

    /**
     * 属性 [ADATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "adate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    private Timestamp adate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [APPRDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "apprdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    private Timestamp apprdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [LIFE]
     *
     */
    @JSONField(name = "life")
    @JsonProperty("life")
    private Integer life;

    /**
     * 属性 [STORENAME]
     *
     */
    @JSONField(name = "storename")
    @JsonProperty("storename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storename;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitid;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itembtypeid;

    /**
     * 属性 [TEAMNAME]
     *
     */
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamname;

    /**
     * 属性 [ITEMGROUP]
     *
     */
    @JSONField(name = "itemgroup")
    @JsonProperty("itemgroup")
    private Integer itemgroup;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [AVGPRICE]
     *
     */
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    private Double avgprice;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String itemname;

    /**
     * 属性 [MSERVICENAME]
     *
     */
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mservicename;

    /**
     * 属性 [STOREPARTNAME]
     *
     */
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storepartname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [PURPLANNAME]
     *
     */
    @JSONField(name = "purplanname")
    @JsonProperty("purplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purplanname;

    /**
     * 属性 [WONAME]
     *
     */
    @JSONField(name = "woname")
    @JsonProperty("woname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String woname;

    /**
     * 属性 [STOREID]
     *
     */
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storeid;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [TEAMID]
     *
     */
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String teamid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [WOID]
     *
     */
    @JSONField(name = "woid")
    @JsonProperty("woid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String woid;

    /**
     * 属性 [MSERVICEID]
     *
     */
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mserviceid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [PURPLANID]
     *
     */
    @JSONField(name = "purplanid")
    @JsonProperty("purplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String purplanid;

    /**
     * 属性 [STOREPARTID]
     *
     */
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storepartid;

    /**
     * 属性 [AEMPID]
     *
     */
    @JSONField(name = "aempid")
    @JsonProperty("aempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aempid;

    /**
     * 属性 [AEMPNAME]
     *
     */
    @JSONField(name = "aempname")
    @JsonProperty("aempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String aempname;

    /**
     * 属性 [SEMPID]
     *
     */
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sempid;

    /**
     * 属性 [SEMPNAME]
     *
     */
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sempname;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empname;

    /**
     * 属性 [APPREMPID]
     *
     */
    @JSONField(name = "apprempid")
    @JsonProperty("apprempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String apprempid;

    /**
     * 属性 [APPREMPNAME]
     *
     */
    @JSONField(name = "apprempname")
    @JsonProperty("apprempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String apprempname;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptid;

    /**
     * 属性 [DEPTNAME]
     *
     */
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String deptname;


    /**
     * 设置 [REMARK]
     */
    public void setRemark(String  remark){
        this.remark = remark ;
        this.modify("remark",remark);
    }

    /**
     * 设置 [EMITEMPUSENAME]
     */
    public void setEmitempusename(String  emitempusename){
        this.emitempusename = emitempusename ;
        this.modify("emitempusename",emitempusename);
    }

    /**
     * 设置 [DELTYPE]
     */
    public void setDeltype(Integer  deltype){
        this.deltype = deltype ;
        this.modify("deltype",deltype);
    }

    /**
     * 设置 [OPINION]
     */
    public void setOpinion(String  opinion){
        this.opinion = opinion ;
        this.modify("opinion",opinion);
    }

    /**
     * 设置 [APPROKNUM]
     */
    public void setApproknum(Integer  approknum){
        this.approknum = approknum ;
        this.modify("approknum",approknum);
    }

    /**
     * 设置 [EQUIPS]
     */
    public void setEquips(String  equips){
        this.equips = equips ;
        this.modify("equips",equips);
    }

    /**
     * 设置 [PSUM]
     */
    public void setPsum(Double  psum){
        this.psum = psum ;
        this.modify("psum",psum);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [BATCODE]
     */
    public void setBatcode(String  batcode){
        this.batcode = batcode ;
        this.modify("batcode",batcode);
    }

    /**
     * 设置 [BZ]
     */
    public void setBz(String  bz){
        this.bz = bz ;
        this.modify("bz",bz);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [SAPREASON1]
     */
    public void setSapreason1(String  sapreason1){
        this.sapreason1 = sapreason1 ;
        this.modify("sapreason1",sapreason1);
    }

    /**
     * 设置 [PUSESTATE]
     */
    public void setPusestate(Integer  pusestate){
        this.pusestate = pusestate ;
        this.modify("pusestate",pusestate);
    }

    /**
     * 设置 [APPRDESC]
     */
    public void setApprdesc(String  apprdesc){
        this.apprdesc = apprdesc ;
        this.modify("apprdesc",apprdesc);
    }

    /**
     * 设置 [SAPCBZX]
     */
    public void setSapcbzx(String  sapcbzx){
        this.sapcbzx = sapcbzx ;
        this.modify("sapcbzx",sapcbzx);
    }

    /**
     * 设置 [SAPLLYT]
     */
    public void setSapllyt(String  sapllyt){
        this.sapllyt = sapllyt ;
        this.modify("sapllyt",sapllyt);
    }

    /**
     * 设置 [ASUM]
     */
    public void setAsum(Double  asum){
        this.asum = asum ;
        this.modify("asum",asum);
    }

    /**
     * 设置 [SAP]
     */
    public void setSap(Integer  sap){
        this.sap = sap ;
        this.modify("sap",sap);
    }

    /**
     * 设置 [SDATE]
     */
    public void setSdate(Timestamp  sdate){
        this.sdate = sdate ;
        this.modify("sdate",sdate);
    }

    /**
     * 设置 [LIFE2]
     */
    public void setLife2(Integer  life2){
        this.life2 = life2 ;
        this.modify("life2",life2);
    }

    /**
     * 设置 [STOCK2NUM]
     */
    public void setStock2num(Double  stock2num){
        this.stock2num = stock2num ;
        this.modify("stock2num",stock2num);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [USETO]
     */
    public void setUseto(String  useto){
        this.useto = useto ;
        this.modify("useto",useto);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [PUSETYPE]
     */
    public void setPusetype(String  pusetype){
        this.pusetype = pusetype ;
        this.modify("pusetype",pusetype);
    }

    /**
     * 设置 [SAPCONTROL]
     */
    public void setSapcontrol(Integer  sapcontrol){
        this.sapcontrol = sapcontrol ;
        this.modify("sapcontrol",sapcontrol);
    }

    /**
     * 设置 [ADATE]
     */
    public void setAdate(Timestamp  adate){
        this.adate = adate ;
        this.modify("adate",adate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [APPRDATE]
     */
    public void setApprdate(Timestamp  apprdate){
        this.apprdate = apprdate ;
        this.modify("apprdate",apprdate);
    }

    /**
     * 设置 [STOREID]
     */
    public void setStoreid(String  storeid){
        this.storeid = storeid ;
        this.modify("storeid",storeid);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [TEAMID]
     */
    public void setTeamid(String  teamid){
        this.teamid = teamid ;
        this.modify("teamid",teamid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [WOID]
     */
    public void setWoid(String  woid){
        this.woid = woid ;
        this.modify("woid",woid);
    }

    /**
     * 设置 [MSERVICEID]
     */
    public void setMserviceid(String  mserviceid){
        this.mserviceid = mserviceid ;
        this.modify("mserviceid",mserviceid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [PURPLANID]
     */
    public void setPurplanid(String  purplanid){
        this.purplanid = purplanid ;
        this.modify("purplanid",purplanid);
    }

    /**
     * 设置 [STOREPARTID]
     */
    public void setStorepartid(String  storepartid){
        this.storepartid = storepartid ;
        this.modify("storepartid",storepartid);
    }

    /**
     * 设置 [AEMPID]
     */
    public void setAempid(String  aempid){
        this.aempid = aempid ;
        this.modify("aempid",aempid);
    }

    /**
     * 设置 [SEMPID]
     */
    public void setSempid(String  sempid){
        this.sempid = sempid ;
        this.modify("sempid",sempid);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [APPREMPID]
     */
    public void setApprempid(String  apprempid){
        this.apprempid = apprempid ;
        this.modify("apprempid",apprempid);
    }

    /**
     * 设置 [DEPTID]
     */
    public void setDeptid(String  deptid){
        this.deptid = deptid ;
        this.modify("deptid",deptid);
    }


}


