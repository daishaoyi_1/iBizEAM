package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQTypeDTO]
 */
@Data
public class EMEQTypeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EQTYPEGROUP]
     *
     */
    @JSONField(name = "eqtypegroup")
    @JsonProperty("eqtypegroup")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqtypegroup;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EQSTATEINFO]
     *
     */
    @JSONField(name = "eqstateinfo")
    @JsonProperty("eqstateinfo")
    @Size(min = 0, max = 99999999, message = "内容长度必须小于等于[99999999]")
    private String eqstateinfo;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EQTYPECODE]
     *
     */
    @JSONField(name = "eqtypecode")
    @JsonProperty("eqtypecode")
    @NotBlank(message = "[类型代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqtypecode;

    /**
     * 属性 [STYPE]
     *
     */
    @JSONField(name = "stype")
    @JsonProperty("stype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stype;

    /**
     * 属性 [EQTYPEINFO]
     *
     */
    @JSONField(name = "eqtypeinfo")
    @JsonProperty("eqtypeinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqtypeinfo;

    /**
     * 属性 [ARG]
     *
     */
    @JSONField(name = "arg")
    @JsonProperty("arg")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String arg;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [EMEQTYPEID]
     *
     */
    @JSONField(name = "emeqtypeid")
    @JsonProperty("emeqtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqtypeid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [SNAME]
     *
     */
    @JSONField(name = "sname")
    @JsonProperty("sname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sname;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMEQTYPENAME]
     *
     */
    @JSONField(name = "emeqtypename")
    @JsonProperty("emeqtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqtypename;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EQTYPEPCODE]
     *
     */
    @JSONField(name = "eqtypepcode")
    @JsonProperty("eqtypepcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqtypepcode;

    /**
     * 属性 [EQTYPEPNAME]
     *
     */
    @JSONField(name = "eqtypepname")
    @JsonProperty("eqtypepname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqtypepname;

    /**
     * 属性 [EQTYPEPID]
     *
     */
    @JSONField(name = "eqtypepid")
    @JsonProperty("eqtypepid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqtypepid;


    /**
     * 设置 [EQTYPEGROUP]
     */
    public void setEqtypegroup(String  eqtypegroup){
        this.eqtypegroup = eqtypegroup ;
        this.modify("eqtypegroup",eqtypegroup);
    }

    /**
     * 设置 [EQTYPECODE]
     */
    public void setEqtypecode(String  eqtypecode){
        this.eqtypecode = eqtypecode ;
        this.modify("eqtypecode",eqtypecode);
    }

    /**
     * 设置 [STYPE]
     */
    public void setStype(String  stype){
        this.stype = stype ;
        this.modify("stype",stype);
    }

    /**
     * 设置 [ARG]
     */
    public void setArg(String  arg){
        this.arg = arg ;
        this.modify("arg",arg);
    }

    /**
     * 设置 [SNAME]
     */
    public void setSname(String  sname){
        this.sname = sname ;
        this.modify("sname",sname);
    }

    /**
     * 设置 [EMEQTYPENAME]
     */
    public void setEmeqtypename(String  emeqtypename){
        this.emeqtypename = emeqtypename ;
        this.modify("emeqtypename",emeqtypename);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EQTYPEPID]
     */
    public void setEqtypepid(String  eqtypepid){
        this.eqtypepid = eqtypepid ;
        this.modify("eqtypepid",eqtypepid);
    }


}


