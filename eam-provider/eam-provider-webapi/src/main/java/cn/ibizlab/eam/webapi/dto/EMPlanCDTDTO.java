package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPlanCDTDTO]
 */
@Data
public class EMPlanCDTDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMPLANCDTID]
     *
     */
    @JSONField(name = "emplancdtid")
    @JsonProperty("emplancdtid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emplancdtid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [TRIGGERVAL]
     *
     */
    @JSONField(name = "triggerval")
    @JsonProperty("triggerval")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String triggerval;

    /**
     * 属性 [DPVALTYPE]
     *
     */
    @JSONField(name = "dpvaltype")
    @JsonProperty("dpvaltype")
    @NotBlank(message = "[测点值类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String dpvaltype;

    /**
     * 属性 [LASTVAL]
     *
     */
    @JSONField(name = "lastval")
    @JsonProperty("lastval")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String lastval;

    /**
     * 属性 [NOWVAL]
     *
     */
    @JSONField(name = "nowval")
    @JsonProperty("nowval")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String nowval;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [TRIGGERDP]
     *
     */
    @JSONField(name = "triggerdp")
    @JsonProperty("triggerdp")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String triggerdp;

    /**
     * 属性 [EMPLANCDTNAME]
     *
     */
    @JSONField(name = "emplancdtname")
    @JsonProperty("emplancdtname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emplancdtname;

    /**
     * 属性 [DPTYPE]
     *
     */
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    @NotBlank(message = "[测点类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dptype;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [DPNAME]
     *
     */
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String dpname;

    /**
     * 属性 [PLANNAME]
     *
     */
    @JSONField(name = "planname")
    @JsonProperty("planname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String planname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [DPID]
     *
     */
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dpid;

    /**
     * 属性 [PLANID]
     *
     */
    @JSONField(name = "planid")
    @JsonProperty("planid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String planid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;


    /**
     * 设置 [TRIGGERVAL]
     */
    public void setTriggerval(String  triggerval){
        this.triggerval = triggerval ;
        this.modify("triggerval",triggerval);
    }

    /**
     * 设置 [DPVALTYPE]
     */
    public void setDpvaltype(String  dpvaltype){
        this.dpvaltype = dpvaltype ;
        this.modify("dpvaltype",dpvaltype);
    }

    /**
     * 设置 [LASTVAL]
     */
    public void setLastval(String  lastval){
        this.lastval = lastval ;
        this.modify("lastval",lastval);
    }

    /**
     * 设置 [NOWVAL]
     */
    public void setNowval(String  nowval){
        this.nowval = nowval ;
        this.modify("nowval",nowval);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [TRIGGERDP]
     */
    public void setTriggerdp(String  triggerdp){
        this.triggerdp = triggerdp ;
        this.modify("triggerdp",triggerdp);
    }

    /**
     * 设置 [EMPLANCDTNAME]
     */
    public void setEmplancdtname(String  emplancdtname){
        this.emplancdtname = emplancdtname ;
        this.modify("emplancdtname",emplancdtname);
    }

    /**
     * 设置 [DPID]
     */
    public void setDpid(String  dpid){
        this.dpid = dpid ;
        this.modify("dpid",dpid);
    }

    /**
     * 设置 [PLANID]
     */
    public void setPlanid(String  planid){
        this.planid = planid ;
        this.modify("planid",planid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }


}


