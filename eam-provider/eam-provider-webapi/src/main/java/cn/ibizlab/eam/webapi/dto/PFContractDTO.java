package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[PFContractDTO]
 */
@Data
public class PFContractDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ATTENTION]
     *
     */
    @JSONField(name = "attention")
    @JsonProperty("attention")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String attention;

    /**
     * 属性 [CONTRACTOBJGROUP]
     *
     */
    @JSONField(name = "contractobjgroup")
    @JsonProperty("contractobjgroup")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String contractobjgroup;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [PAYAMOUNT]
     *
     */
    @JSONField(name = "payamount")
    @JsonProperty("payamount")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String payamount;

    /**
     * 属性 [CERTDESC]
     *
     */
    @JSONField(name = "certdesc")
    @JsonProperty("certdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String certdesc;

    /**
     * 属性 [ATT]
     *
     */
    @JSONField(name = "att")
    @JsonProperty("att")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String att;

    /**
     * 属性 [PFCONTRACTNAME]
     *
     */
    @JSONField(name = "pfcontractname")
    @JsonProperty("pfcontractname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfcontractname;

    /**
     * 属性 [PERFORMRECORD]
     *
     */
    @JSONField(name = "performrecord")
    @JsonProperty("performrecord")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String performrecord;

    /**
     * 属性 [CONTRACTDESC]
     *
     */
    @JSONField(name = "contractdesc")
    @JsonProperty("contractdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String contractdesc;

    /**
     * 属性 [CHECKSTANDARD]
     *
     */
    @JSONField(name = "checkstandard")
    @JsonProperty("checkstandard")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String checkstandard;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [APPRSTATE]
     *
     */
    @JSONField(name = "apprstate")
    @JsonProperty("apprstate")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String apprstate;

    /**
     * 属性 [PERAMOUNT]
     *
     */
    @JSONField(name = "peramount")
    @JsonProperty("peramount")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String peramount;

    /**
     * 属性 [PFCONTRACTID]
     *
     */
    @JSONField(name = "pfcontractid")
    @JsonProperty("pfcontractid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfcontractid;

    /**
     * 属性 [PAYDESC]
     *
     */
    @JSONField(name = "paydesc")
    @JsonProperty("paydesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String paydesc;

    /**
     * 属性 [PAYCNT]
     *
     */
    @JSONField(name = "paycnt")
    @JsonProperty("paycnt")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String paycnt;

    /**
     * 属性 [PERFORMLINE]
     *
     */
    @JSONField(name = "performline")
    @JsonProperty("performline")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String performline;

    /**
     * 属性 [CONTRACTNUM]
     *
     */
    @JSONField(name = "contractnum")
    @JsonProperty("contractnum")
    private Integer contractnum;

    /**
     * 属性 [ATTDESC]
     *
     */
    @JSONField(name = "attdesc")
    @JsonProperty("attdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String attdesc;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [CONTRACTDOC]
     *
     */
    @JSONField(name = "contractdoc")
    @JsonProperty("contractdoc")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contractdoc;

    /**
     * 属性 [CHANGEDESC]
     *
     */
    @JSONField(name = "changedesc")
    @JsonProperty("changedesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String changedesc;

    /**
     * 属性 [CONTRACTNO]
     *
     */
    @JSONField(name = "contractno")
    @JsonProperty("contractno")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contractno;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [RORGID]
     *
     */
    @JSONField(name = "rorgid")
    @JsonProperty("rorgid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rorgid;

    /**
     * 属性 [PERFORMSTATE]
     *
     */
    @JSONField(name = "performstate")
    @JsonProperty("performstate")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String performstate;

    /**
     * 属性 [DECLARATION]
     *
     */
    @JSONField(name = "declaration")
    @JsonProperty("declaration")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String declaration;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [PAYREASON]
     *
     */
    @JSONField(name = "payreason")
    @JsonProperty("payreason")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String payreason;

    /**
     * 属性 [LICDESC]
     *
     */
    @JSONField(name = "licdesc")
    @JsonProperty("licdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String licdesc;

    /**
     * 属性 [SDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sdate" , format="yyyy-MM-dd")
    @JsonProperty("sdate")
    private Timestamp sdate;

    /**
     * 属性 [APPRDESC]
     *
     */
    @JSONField(name = "apprdesc")
    @JsonProperty("apprdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String apprdesc;

    /**
     * 属性 [CONTRACTPRICE]
     *
     */
    @JSONField(name = "contractprice")
    @JsonProperty("contractprice")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String contractprice;

    /**
     * 属性 [CONTRACTTYPEID]
     *
     */
    @JSONField(name = "contracttypeid")
    @JsonProperty("contracttypeid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String contracttypeid;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String amount;

    /**
     * 属性 [CONTRACTGROUP]
     *
     */
    @JSONField(name = "contractgroup")
    @JsonProperty("contractgroup")
    private Integer contractgroup;

    /**
     * 属性 [DOCDESC]
     *
     */
    @JSONField(name = "docdesc")
    @JsonProperty("docdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String docdesc;

    /**
     * 属性 [CONTRACTQA]
     *
     */
    @JSONField(name = "contractqa")
    @JsonProperty("contractqa")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String contractqa;

    /**
     * 属性 [CONTRACTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "contractdate" , format="yyyy-MM-dd")
    @JsonProperty("contractdate")
    private Timestamp contractdate;

    /**
     * 属性 [RECVDOC]
     *
     */
    @JSONField(name = "recvdoc")
    @JsonProperty("recvdoc")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recvdoc;

    /**
     * 属性 [APPRDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "apprdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    private Timestamp apprdate;

    /**
     * 属性 [PERFORMPLACE]
     *
     */
    @JSONField(name = "performplace")
    @JsonProperty("performplace")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String performplace;

    /**
     * 属性 [CONTRACTINFO]
     *
     */
    @JSONField(name = "contractinfo")
    @JsonProperty("contractinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String contractinfo;

    /**
     * 属性 [CONTRACTOBJID]
     *
     */
    @JSONField(name = "contractobjid")
    @JsonProperty("contractobjid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contractobjid;

    /**
     * 属性 [PERFORMWAY]
     *
     */
    @JSONField(name = "performway")
    @JsonProperty("performway")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String performway;

    /**
     * 属性 [CHANGECONTENT]
     *
     */
    @JSONField(name = "changecontent")
    @JsonProperty("changecontent")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String changecontent;

    /**
     * 属性 [CONTRACTCODE]
     *
     */
    @JSONField(name = "contractcode")
    @JsonProperty("contractcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contractcode;

    /**
     * 属性 [CONTRACTTENDER]
     *
     */
    @JSONField(name = "contracttender")
    @JsonProperty("contracttender")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String contracttender;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMSERVICENAME]
     *
     */
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicename;

    /**
     * 属性 [EMSERVICEID]
     *
     */
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceid;


    /**
     * 设置 [ATTENTION]
     */
    public void setAttention(String  attention){
        this.attention = attention ;
        this.modify("attention",attention);
    }

    /**
     * 设置 [CONTRACTOBJGROUP]
     */
    public void setContractobjgroup(String  contractobjgroup){
        this.contractobjgroup = contractobjgroup ;
        this.modify("contractobjgroup",contractobjgroup);
    }

    /**
     * 设置 [PAYAMOUNT]
     */
    public void setPayamount(String  payamount){
        this.payamount = payamount ;
        this.modify("payamount",payamount);
    }

    /**
     * 设置 [CERTDESC]
     */
    public void setCertdesc(String  certdesc){
        this.certdesc = certdesc ;
        this.modify("certdesc",certdesc);
    }

    /**
     * 设置 [ATT]
     */
    public void setAtt(String  att){
        this.att = att ;
        this.modify("att",att);
    }

    /**
     * 设置 [PFCONTRACTNAME]
     */
    public void setPfcontractname(String  pfcontractname){
        this.pfcontractname = pfcontractname ;
        this.modify("pfcontractname",pfcontractname);
    }

    /**
     * 设置 [PERFORMRECORD]
     */
    public void setPerformrecord(String  performrecord){
        this.performrecord = performrecord ;
        this.modify("performrecord",performrecord);
    }

    /**
     * 设置 [CONTRACTDESC]
     */
    public void setContractdesc(String  contractdesc){
        this.contractdesc = contractdesc ;
        this.modify("contractdesc",contractdesc);
    }

    /**
     * 设置 [CHECKSTANDARD]
     */
    public void setCheckstandard(String  checkstandard){
        this.checkstandard = checkstandard ;
        this.modify("checkstandard",checkstandard);
    }

    /**
     * 设置 [APPRSTATE]
     */
    public void setApprstate(String  apprstate){
        this.apprstate = apprstate ;
        this.modify("apprstate",apprstate);
    }

    /**
     * 设置 [PERAMOUNT]
     */
    public void setPeramount(String  peramount){
        this.peramount = peramount ;
        this.modify("peramount",peramount);
    }

    /**
     * 设置 [PAYDESC]
     */
    public void setPaydesc(String  paydesc){
        this.paydesc = paydesc ;
        this.modify("paydesc",paydesc);
    }

    /**
     * 设置 [PAYCNT]
     */
    public void setPaycnt(String  paycnt){
        this.paycnt = paycnt ;
        this.modify("paycnt",paycnt);
    }

    /**
     * 设置 [PERFORMLINE]
     */
    public void setPerformline(String  performline){
        this.performline = performline ;
        this.modify("performline",performline);
    }

    /**
     * 设置 [CONTRACTNUM]
     */
    public void setContractnum(Integer  contractnum){
        this.contractnum = contractnum ;
        this.modify("contractnum",contractnum);
    }

    /**
     * 设置 [ATTDESC]
     */
    public void setAttdesc(String  attdesc){
        this.attdesc = attdesc ;
        this.modify("attdesc",attdesc);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [CONTRACTDOC]
     */
    public void setContractdoc(String  contractdoc){
        this.contractdoc = contractdoc ;
        this.modify("contractdoc",contractdoc);
    }

    /**
     * 设置 [CHANGEDESC]
     */
    public void setChangedesc(String  changedesc){
        this.changedesc = changedesc ;
        this.modify("changedesc",changedesc);
    }

    /**
     * 设置 [CONTRACTNO]
     */
    public void setContractno(String  contractno){
        this.contractno = contractno ;
        this.modify("contractno",contractno);
    }

    /**
     * 设置 [RORGID]
     */
    public void setRorgid(String  rorgid){
        this.rorgid = rorgid ;
        this.modify("rorgid",rorgid);
    }

    /**
     * 设置 [PERFORMSTATE]
     */
    public void setPerformstate(String  performstate){
        this.performstate = performstate ;
        this.modify("performstate",performstate);
    }

    /**
     * 设置 [DECLARATION]
     */
    public void setDeclaration(String  declaration){
        this.declaration = declaration ;
        this.modify("declaration",declaration);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [PAYREASON]
     */
    public void setPayreason(String  payreason){
        this.payreason = payreason ;
        this.modify("payreason",payreason);
    }

    /**
     * 设置 [LICDESC]
     */
    public void setLicdesc(String  licdesc){
        this.licdesc = licdesc ;
        this.modify("licdesc",licdesc);
    }

    /**
     * 设置 [SDATE]
     */
    public void setSdate(Timestamp  sdate){
        this.sdate = sdate ;
        this.modify("sdate",sdate);
    }

    /**
     * 设置 [APPRDESC]
     */
    public void setApprdesc(String  apprdesc){
        this.apprdesc = apprdesc ;
        this.modify("apprdesc",apprdesc);
    }

    /**
     * 设置 [CONTRACTPRICE]
     */
    public void setContractprice(String  contractprice){
        this.contractprice = contractprice ;
        this.modify("contractprice",contractprice);
    }

    /**
     * 设置 [CONTRACTTYPEID]
     */
    public void setContracttypeid(String  contracttypeid){
        this.contracttypeid = contracttypeid ;
        this.modify("contracttypeid",contracttypeid);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(String  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [CONTRACTGROUP]
     */
    public void setContractgroup(Integer  contractgroup){
        this.contractgroup = contractgroup ;
        this.modify("contractgroup",contractgroup);
    }

    /**
     * 设置 [DOCDESC]
     */
    public void setDocdesc(String  docdesc){
        this.docdesc = docdesc ;
        this.modify("docdesc",docdesc);
    }

    /**
     * 设置 [CONTRACTQA]
     */
    public void setContractqa(String  contractqa){
        this.contractqa = contractqa ;
        this.modify("contractqa",contractqa);
    }

    /**
     * 设置 [CONTRACTDATE]
     */
    public void setContractdate(Timestamp  contractdate){
        this.contractdate = contractdate ;
        this.modify("contractdate",contractdate);
    }

    /**
     * 设置 [RECVDOC]
     */
    public void setRecvdoc(String  recvdoc){
        this.recvdoc = recvdoc ;
        this.modify("recvdoc",recvdoc);
    }

    /**
     * 设置 [APPRDATE]
     */
    public void setApprdate(Timestamp  apprdate){
        this.apprdate = apprdate ;
        this.modify("apprdate",apprdate);
    }

    /**
     * 设置 [PERFORMPLACE]
     */
    public void setPerformplace(String  performplace){
        this.performplace = performplace ;
        this.modify("performplace",performplace);
    }

    /**
     * 设置 [CONTRACTOBJID]
     */
    public void setContractobjid(String  contractobjid){
        this.contractobjid = contractobjid ;
        this.modify("contractobjid",contractobjid);
    }

    /**
     * 设置 [PERFORMWAY]
     */
    public void setPerformway(String  performway){
        this.performway = performway ;
        this.modify("performway",performway);
    }

    /**
     * 设置 [CHANGECONTENT]
     */
    public void setChangecontent(String  changecontent){
        this.changecontent = changecontent ;
        this.modify("changecontent",changecontent);
    }

    /**
     * 设置 [CONTRACTCODE]
     */
    public void setContractcode(String  contractcode){
        this.contractcode = contractcode ;
        this.modify("contractcode",contractcode);
    }

    /**
     * 设置 [CONTRACTTENDER]
     */
    public void setContracttender(String  contracttender){
        this.contracttender = contracttender ;
        this.modify("contracttender",contracttender);
    }

    /**
     * 设置 [EMSERVICEID]
     */
    public void setEmserviceid(String  emserviceid){
        this.emserviceid = emserviceid ;
        this.modify("emserviceid",emserviceid);
    }


}


