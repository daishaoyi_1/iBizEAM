package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMObjMapDTO]
 */
@Data
public class EMObjMapDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMOBJMAPID]
     *
     */
    @JSONField(name = "emobjmapid")
    @JsonProperty("emobjmapid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emobjmapid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipid;

    /**
     * 属性 [EMOBJMAPTYPE]
     *
     */
    @JSONField(name = "emobjmaptype")
    @JsonProperty("emobjmaptype")
    @NotBlank(message = "[对象关系类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emobjmaptype;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMOBJMAPNAME]
     *
     */
    @JSONField(name = "emobjmapname")
    @JsonProperty("emobjmapname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emobjmapname;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [OBJPTYPE]
     *
     */
    @JSONField(name = "objptype")
    @JsonProperty("objptype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objptype;

    /**
     * 属性 [MAJOREQUIPNAME]
     *
     */
    @JSONField(name = "majorequipname")
    @JsonProperty("majorequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String majorequipname;

    /**
     * 属性 [MAJOREQUIPID]
     *
     */
    @JSONField(name = "majorequipid")
    @JsonProperty("majorequipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String majorequipid;

    /**
     * 属性 [OBJPNAME]
     *
     */
    @JSONField(name = "objpname")
    @JsonProperty("objpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objpname;

    /**
     * 属性 [OBJTYPE]
     *
     */
    @JSONField(name = "objtype")
    @JsonProperty("objtype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objtype;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [OBJPID]
     *
     */
    @JSONField(name = "objpid")
    @JsonProperty("objpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objpid;


    /**
     * 设置 [EMOBJMAPTYPE]
     */
    public void setEmobjmaptype(String  emobjmaptype){
        this.emobjmaptype = emobjmaptype ;
        this.modify("emobjmaptype",emobjmaptype);
    }

    /**
     * 设置 [EMOBJMAPNAME]
     */
    public void setEmobjmapname(String  emobjmapname){
        this.emobjmapname = emobjmapname ;
        this.modify("emobjmapname",emobjmapname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [OBJPID]
     */
    public void setObjpid(String  objpid){
        this.objpid = objpid ;
        this.modify("objpid",objpid);
    }


}


