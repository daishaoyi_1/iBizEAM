package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService;
import cn.ibizlab.eam.core.eam_core.filter.EMDPRCTSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测点记录" })
@RestController("WebApi-emdprct")
@RequestMapping("")
public class EMDPRCTResource {

    @Autowired
    public IEMDPRCTService emdprctService;

    @Autowired
    @Lazy
    public EMDPRCTMapping emdprctMapping;

    @PreAuthorize("hasPermission(this.emdprctMapping.toDomain(#emdprctdto),'eam-EMDPRCT-Create')")
    @ApiOperation(value = "新建测点记录", tags = {"测点记录" },  notes = "新建测点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emdprcts")
    public ResponseEntity<EMDPRCTDTO> create(@Validated @RequestBody EMDPRCTDTO emdprctdto) {
        EMDPRCT domain = emdprctMapping.toDomain(emdprctdto);
		emdprctService.create(domain);
        EMDPRCTDTO dto = emdprctMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdprctMapping.toDomain(#emdprctdtos),'eam-EMDPRCT-Create')")
    @ApiOperation(value = "批量新建测点记录", tags = {"测点记录" },  notes = "批量新建测点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emdprcts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMDPRCTDTO> emdprctdtos) {
        emdprctService.createBatch(emdprctMapping.toDomain(emdprctdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emdprct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emdprctService.get(#emdprct_id),'eam-EMDPRCT-Update')")
    @ApiOperation(value = "更新测点记录", tags = {"测点记录" },  notes = "更新测点记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdprcts/{emdprct_id}")
    public ResponseEntity<EMDPRCTDTO> update(@PathVariable("emdprct_id") String emdprct_id, @RequestBody EMDPRCTDTO emdprctdto) {
		EMDPRCT domain  = emdprctMapping.toDomain(emdprctdto);
        domain .setEmdprctid(emdprct_id);
		emdprctService.update(domain );
		EMDPRCTDTO dto = emdprctMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdprctService.getEmdprctByEntities(this.emdprctMapping.toDomain(#emdprctdtos)),'eam-EMDPRCT-Update')")
    @ApiOperation(value = "批量更新测点记录", tags = {"测点记录" },  notes = "批量更新测点记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdprcts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMDPRCTDTO> emdprctdtos) {
        emdprctService.updateBatch(emdprctMapping.toDomain(emdprctdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emdprctService.get(#emdprct_id),'eam-EMDPRCT-Remove')")
    @ApiOperation(value = "删除测点记录", tags = {"测点记录" },  notes = "删除测点记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdprcts/{emdprct_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emdprct_id") String emdprct_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emdprctService.remove(emdprct_id));
    }

    @PreAuthorize("hasPermission(this.emdprctService.getEmdprctByIds(#ids),'eam-EMDPRCT-Remove')")
    @ApiOperation(value = "批量删除测点记录", tags = {"测点记录" },  notes = "批量删除测点记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdprcts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emdprctService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emdprctMapping.toDomain(returnObject.body),'eam-EMDPRCT-Get')")
    @ApiOperation(value = "获取测点记录", tags = {"测点记录" },  notes = "获取测点记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emdprcts/{emdprct_id}")
    public ResponseEntity<EMDPRCTDTO> get(@PathVariable("emdprct_id") String emdprct_id) {
        EMDPRCT domain = emdprctService.get(emdprct_id);
        EMDPRCTDTO dto = emdprctMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测点记录草稿", tags = {"测点记录" },  notes = "获取测点记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emdprcts/getdraft")
    public ResponseEntity<EMDPRCTDTO> getDraft(EMDPRCTDTO dto) {
        EMDPRCT domain = emdprctMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emdprctMapping.toDto(emdprctService.getDraft(domain)));
    }

    @ApiOperation(value = "检查测点记录", tags = {"测点记录" },  notes = "检查测点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emdprcts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMDPRCTDTO emdprctdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emdprctService.checkKey(emdprctMapping.toDomain(emdprctdto)));
    }

    @PreAuthorize("hasPermission(this.emdprctMapping.toDomain(#emdprctdto),'eam-EMDPRCT-Save')")
    @ApiOperation(value = "保存测点记录", tags = {"测点记录" },  notes = "保存测点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emdprcts/save")
    public ResponseEntity<EMDPRCTDTO> save(@RequestBody EMDPRCTDTO emdprctdto) {
        EMDPRCT domain = emdprctMapping.toDomain(emdprctdto);
        emdprctService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emdprctMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emdprctMapping.toDomain(#emdprctdtos),'eam-EMDPRCT-Save')")
    @ApiOperation(value = "批量保存测点记录", tags = {"测点记录" },  notes = "批量保存测点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emdprcts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMDPRCTDTO> emdprctdtos) {
        emdprctService.saveBatch(emdprctMapping.toDomain(emdprctdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDPRCT-searchDefault-all') and hasPermission(#context,'eam-EMDPRCT-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"测点记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emdprcts/fetchdefault")
	public ResponseEntity<List<EMDPRCTDTO>> fetchDefault(EMDPRCTSearchContext context) {
        Page<EMDPRCT> domains = emdprctService.searchDefault(context) ;
        List<EMDPRCTDTO> list = emdprctMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDPRCT-searchDefault-all') and hasPermission(#context,'eam-EMDPRCT-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"测点记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emdprcts/searchdefault")
	public ResponseEntity<Page<EMDPRCTDTO>> searchDefault(@RequestBody EMDPRCTSearchContext context) {
        Page<EMDPRCT> domains = emdprctService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emdprctMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDPRCT-searchIndexDER-all') and hasPermission(#context,'eam-EMDPRCT-Get')")
	@ApiOperation(value = "获取IndexDER", tags = {"测点记录" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emdprcts/fetchindexder")
	public ResponseEntity<List<EMDPRCTDTO>> fetchIndexDER(EMDPRCTSearchContext context) {
        Page<EMDPRCT> domains = emdprctService.searchIndexDER(context) ;
        List<EMDPRCTDTO> list = emdprctMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDPRCT-searchIndexDER-all') and hasPermission(#context,'eam-EMDPRCT-Get')")
	@ApiOperation(value = "查询IndexDER", tags = {"测点记录" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emdprcts/searchindexder")
	public ResponseEntity<Page<EMDPRCTDTO>> searchIndexDER(@RequestBody EMDPRCTSearchContext context) {
        Page<EMDPRCT> domains = emdprctService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emdprctMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

