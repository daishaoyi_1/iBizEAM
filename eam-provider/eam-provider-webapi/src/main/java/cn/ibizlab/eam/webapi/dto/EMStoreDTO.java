package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMStoreDTO]
 */
@Data
public class EMStoreDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [STORECODE]
     *
     */
    @JSONField(name = "storecode")
    @JsonProperty("storecode")
    @NotBlank(message = "[仓库代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storecode;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [STOREINFO]
     *
     */
    @JSONField(name = "storeinfo")
    @JsonProperty("storeinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storeinfo;

    /**
     * 属性 [STANDPRICEFLAG]
     *
     */
    @JSONField(name = "standpriceflag")
    @JsonProperty("standpriceflag")
    private Integer standpriceflag;

    /**
     * 属性 [POWERAVGFLAG]
     *
     */
    @JSONField(name = "poweravgflag")
    @JsonProperty("poweravgflag")
    private Integer poweravgflag;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMSTOREID]
     *
     */
    @JSONField(name = "emstoreid")
    @JsonProperty("emstoreid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emstoreid;

    /**
     * 属性 [NEWSTORETYPEID]
     *
     */
    @JSONField(name = "newstoretypeid")
    @JsonProperty("newstoretypeid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String newstoretypeid;

    /**
     * 属性 [COSTCENTERID]
     *
     */
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costcenterid;

    /**
     * 属性 [MGRPERSONID]
     *
     */
    @JSONField(name = "mgrpersonid")
    @JsonProperty("mgrpersonid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mgrpersonid;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [IOALGO]
     *
     */
    @JSONField(name = "ioalgo")
    @JsonProperty("ioalgo")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String ioalgo;

    /**
     * 属性 [STORETYPEID]
     *
     */
    @JSONField(name = "storetypeid")
    @JsonProperty("storetypeid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storetypeid;

    /**
     * 属性 [STOREADDR]
     *
     */
    @JSONField(name = "storeaddr")
    @JsonProperty("storeaddr")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storeaddr;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EMSTORENAME]
     *
     */
    @JSONField(name = "emstorename")
    @JsonProperty("emstorename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emstorename;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [STORETEL]
     *
     */
    @JSONField(name = "storetel")
    @JsonProperty("storetel")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storetel;

    /**
     * 属性 [STOREFAX]
     *
     */
    @JSONField(name = "storefax")
    @JsonProperty("storefax")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storefax;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empname;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [STORECODE]
     */
    public void setStorecode(String  storecode){
        this.storecode = storecode ;
        this.modify("storecode",storecode);
    }

    /**
     * 设置 [STANDPRICEFLAG]
     */
    public void setStandpriceflag(Integer  standpriceflag){
        this.standpriceflag = standpriceflag ;
        this.modify("standpriceflag",standpriceflag);
    }

    /**
     * 设置 [POWERAVGFLAG]
     */
    public void setPoweravgflag(Integer  poweravgflag){
        this.poweravgflag = poweravgflag ;
        this.modify("poweravgflag",poweravgflag);
    }

    /**
     * 设置 [NEWSTORETYPEID]
     */
    public void setNewstoretypeid(String  newstoretypeid){
        this.newstoretypeid = newstoretypeid ;
        this.modify("newstoretypeid",newstoretypeid);
    }

    /**
     * 设置 [COSTCENTERID]
     */
    public void setCostcenterid(String  costcenterid){
        this.costcenterid = costcenterid ;
        this.modify("costcenterid",costcenterid);
    }

    /**
     * 设置 [MGRPERSONID]
     */
    public void setMgrpersonid(String  mgrpersonid){
        this.mgrpersonid = mgrpersonid ;
        this.modify("mgrpersonid",mgrpersonid);
    }

    /**
     * 设置 [IOALGO]
     */
    public void setIoalgo(String  ioalgo){
        this.ioalgo = ioalgo ;
        this.modify("ioalgo",ioalgo);
    }

    /**
     * 设置 [STORETYPEID]
     */
    public void setStoretypeid(String  storetypeid){
        this.storetypeid = storetypeid ;
        this.modify("storetypeid",storetypeid);
    }

    /**
     * 设置 [STOREADDR]
     */
    public void setStoreaddr(String  storeaddr){
        this.storeaddr = storeaddr ;
        this.modify("storeaddr",storeaddr);
    }

    /**
     * 设置 [EMSTORENAME]
     */
    public void setEmstorename(String  emstorename){
        this.emstorename = emstorename ;
        this.modify("emstorename",emstorename);
    }

    /**
     * 设置 [STORETEL]
     */
    public void setStoretel(String  storetel){
        this.storetel = storetel ;
        this.modify("storetel",storetel);
    }

    /**
     * 设置 [STOREFAX]
     */
    public void setStorefax(String  storefax){
        this.storefax = storefax ;
        this.modify("storefax",storefax);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }


}


