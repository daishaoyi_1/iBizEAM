package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEICellSetupDTO]
 */
@Data
public class EMEICellSetupDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMEICELLSETUPID]
     *
     */
    @JSONField(name = "emeicellsetupid")
    @JsonProperty("emeicellsetupid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeicellsetupid;

    /**
     * 属性 [CELLSTYPE]
     *
     */
    @JSONField(name = "cellstype")
    @JsonProperty("cellstype")
    @NotBlank(message = "[领用类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String cellstype;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMEICELLSETUPNAME]
     *
     */
    @JSONField(name = "emeicellsetupname")
    @JsonProperty("emeicellsetupname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeicellsetupname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [ACTIVEINFO]
     *
     */
    @JSONField(name = "activeinfo")
    @JsonProperty("activeinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activeinfo;

    /**
     * 属性 [ACTIVEDESC]
     *
     */
    @JSONField(name = "activedesc")
    @JsonProperty("activedesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String activedesc;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [ACTIVEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activedate")
    @NotNull(message = "[分发时间]不允许为空!")
    private Timestamp activedate;

    /**
     * 属性 [EIOBJNAME]
     *
     */
    @JSONField(name = "eiobjname")
    @JsonProperty("eiobjname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eiobjname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [EQLOCATIONNAME]
     *
     */
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationname;

    /**
     * 属性 [EQLOCATIONID]
     *
     */
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationid;

    /**
     * 属性 [EIOBJID]
     *
     */
    @JSONField(name = "eiobjid")
    @JsonProperty("eiobjid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eiobjid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;


    /**
     * 设置 [CELLSTYPE]
     */
    public void setCellstype(String  cellstype){
        this.cellstype = cellstype ;
        this.modify("cellstype",cellstype);
    }

    /**
     * 设置 [EMPNAME]
     */
    public void setEmpname(String  empname){
        this.empname = empname ;
        this.modify("empname",empname);
    }

    /**
     * 设置 [EMEICELLSETUPNAME]
     */
    public void setEmeicellsetupname(String  emeicellsetupname){
        this.emeicellsetupname = emeicellsetupname ;
        this.modify("emeicellsetupname",emeicellsetupname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [ACTIVEDESC]
     */
    public void setActivedesc(String  activedesc){
        this.activedesc = activedesc ;
        this.modify("activedesc",activedesc);
    }

    /**
     * 设置 [ACTIVEDATE]
     */
    public void setActivedate(Timestamp  activedate){
        this.activedate = activedate ;
        this.modify("activedate",activedate);
    }

    /**
     * 设置 [EQLOCATIONID]
     */
    public void setEqlocationid(String  eqlocationid){
        this.eqlocationid = eqlocationid ;
        this.modify("eqlocationid",eqlocationid);
    }

    /**
     * 设置 [EIOBJID]
     */
    public void setEiobjid(String  eiobjid){
        this.eiobjid = eiobjid ;
        this.modify("eiobjid",eiobjid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }


}


