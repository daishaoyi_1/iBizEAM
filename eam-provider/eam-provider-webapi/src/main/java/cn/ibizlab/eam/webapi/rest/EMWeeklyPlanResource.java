package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWeeklyPlan;
import cn.ibizlab.eam.core.eam_core.service.IEMWeeklyPlanService;
import cn.ibizlab.eam.core.eam_core.filter.EMWeeklyPlanSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"维修中心班组周计划" })
@RestController("WebApi-emweeklyplan")
@RequestMapping("")
public class EMWeeklyPlanResource {

    @Autowired
    public IEMWeeklyPlanService emweeklyplanService;

    @Autowired
    @Lazy
    public EMWeeklyPlanMapping emweeklyplanMapping;

    @PreAuthorize("hasPermission(this.emweeklyplanMapping.toDomain(#emweeklyplandto),'eam-EMWeeklyPlan-Create')")
    @ApiOperation(value = "新建维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "新建维修中心班组周计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emweeklyplans")
    public ResponseEntity<EMWeeklyPlanDTO> create(@Validated @RequestBody EMWeeklyPlanDTO emweeklyplandto) {
        EMWeeklyPlan domain = emweeklyplanMapping.toDomain(emweeklyplandto);
		emweeklyplanService.create(domain);
        EMWeeklyPlanDTO dto = emweeklyplanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emweeklyplanMapping.toDomain(#emweeklyplandtos),'eam-EMWeeklyPlan-Create')")
    @ApiOperation(value = "批量新建维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "批量新建维修中心班组周计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emweeklyplans/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWeeklyPlanDTO> emweeklyplandtos) {
        emweeklyplanService.createBatch(emweeklyplanMapping.toDomain(emweeklyplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emweeklyplan" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emweeklyplanService.get(#emweeklyplan_id),'eam-EMWeeklyPlan-Update')")
    @ApiOperation(value = "更新维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "更新维修中心班组周计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emweeklyplans/{emweeklyplan_id}")
    public ResponseEntity<EMWeeklyPlanDTO> update(@PathVariable("emweeklyplan_id") String emweeklyplan_id, @RequestBody EMWeeklyPlanDTO emweeklyplandto) {
		EMWeeklyPlan domain  = emweeklyplanMapping.toDomain(emweeklyplandto);
        domain .setEmweeklyplanid(emweeklyplan_id);
		emweeklyplanService.update(domain );
		EMWeeklyPlanDTO dto = emweeklyplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emweeklyplanService.getEmweeklyplanByEntities(this.emweeklyplanMapping.toDomain(#emweeklyplandtos)),'eam-EMWeeklyPlan-Update')")
    @ApiOperation(value = "批量更新维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "批量更新维修中心班组周计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emweeklyplans/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWeeklyPlanDTO> emweeklyplandtos) {
        emweeklyplanService.updateBatch(emweeklyplanMapping.toDomain(emweeklyplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emweeklyplanService.get(#emweeklyplan_id),'eam-EMWeeklyPlan-Remove')")
    @ApiOperation(value = "删除维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "删除维修中心班组周计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emweeklyplans/{emweeklyplan_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emweeklyplan_id") String emweeklyplan_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emweeklyplanService.remove(emweeklyplan_id));
    }

    @PreAuthorize("hasPermission(this.emweeklyplanService.getEmweeklyplanByIds(#ids),'eam-EMWeeklyPlan-Remove')")
    @ApiOperation(value = "批量删除维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "批量删除维修中心班组周计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emweeklyplans/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emweeklyplanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emweeklyplanMapping.toDomain(returnObject.body),'eam-EMWeeklyPlan-Get')")
    @ApiOperation(value = "获取维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "获取维修中心班组周计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emweeklyplans/{emweeklyplan_id}")
    public ResponseEntity<EMWeeklyPlanDTO> get(@PathVariable("emweeklyplan_id") String emweeklyplan_id) {
        EMWeeklyPlan domain = emweeklyplanService.get(emweeklyplan_id);
        EMWeeklyPlanDTO dto = emweeklyplanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取维修中心班组周计划草稿", tags = {"维修中心班组周计划" },  notes = "获取维修中心班组周计划草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emweeklyplans/getdraft")
    public ResponseEntity<EMWeeklyPlanDTO> getDraft(EMWeeklyPlanDTO dto) {
        EMWeeklyPlan domain = emweeklyplanMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emweeklyplanMapping.toDto(emweeklyplanService.getDraft(domain)));
    }

    @ApiOperation(value = "检查维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "检查维修中心班组周计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emweeklyplans/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWeeklyPlanDTO emweeklyplandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emweeklyplanService.checkKey(emweeklyplanMapping.toDomain(emweeklyplandto)));
    }

    @PreAuthorize("hasPermission(this.emweeklyplanMapping.toDomain(#emweeklyplandto),'eam-EMWeeklyPlan-Save')")
    @ApiOperation(value = "保存维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "保存维修中心班组周计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emweeklyplans/save")
    public ResponseEntity<EMWeeklyPlanDTO> save(@RequestBody EMWeeklyPlanDTO emweeklyplandto) {
        EMWeeklyPlan domain = emweeklyplanMapping.toDomain(emweeklyplandto);
        emweeklyplanService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emweeklyplanMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emweeklyplanMapping.toDomain(#emweeklyplandtos),'eam-EMWeeklyPlan-Save')")
    @ApiOperation(value = "批量保存维修中心班组周计划", tags = {"维修中心班组周计划" },  notes = "批量保存维修中心班组周计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emweeklyplans/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWeeklyPlanDTO> emweeklyplandtos) {
        emweeklyplanService.saveBatch(emweeklyplanMapping.toDomain(emweeklyplandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWeeklyPlan-searchDefault-all') and hasPermission(#context,'eam-EMWeeklyPlan-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"维修中心班组周计划" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emweeklyplans/fetchdefault")
	public ResponseEntity<List<EMWeeklyPlanDTO>> fetchDefault(EMWeeklyPlanSearchContext context) {
        Page<EMWeeklyPlan> domains = emweeklyplanService.searchDefault(context) ;
        List<EMWeeklyPlanDTO> list = emweeklyplanMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWeeklyPlan-searchDefault-all') and hasPermission(#context,'eam-EMWeeklyPlan-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"维修中心班组周计划" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emweeklyplans/searchdefault")
	public ResponseEntity<Page<EMWeeklyPlanDTO>> searchDefault(@RequestBody EMWeeklyPlanSearchContext context) {
        Page<EMWeeklyPlan> domains = emweeklyplanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emweeklyplanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

