package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFTeam;
import cn.ibizlab.eam.core.eam_pf.service.IPFTeamService;
import cn.ibizlab.eam.core.eam_pf.filter.PFTeamSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"班组" })
@RestController("WebApi-pfteam")
@RequestMapping("")
public class PFTeamResource {

    @Autowired
    public IPFTeamService pfteamService;

    @Autowired
    @Lazy
    public PFTeamMapping pfteamMapping;

    @PreAuthorize("hasPermission(this.pfteamMapping.toDomain(#pfteamdto),'eam-PFTeam-Create')")
    @ApiOperation(value = "新建班组", tags = {"班组" },  notes = "新建班组")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams")
    public ResponseEntity<PFTeamDTO> create(@Validated @RequestBody PFTeamDTO pfteamdto) {
        PFTeam domain = pfteamMapping.toDomain(pfteamdto);
		pfteamService.create(domain);
        PFTeamDTO dto = pfteamMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfteamMapping.toDomain(#pfteamdtos),'eam-PFTeam-Create')")
    @ApiOperation(value = "批量新建班组", tags = {"班组" },  notes = "批量新建班组")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFTeamDTO> pfteamdtos) {
        pfteamService.createBatch(pfteamMapping.toDomain(pfteamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pfteam" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pfteamService.get(#pfteam_id),'eam-PFTeam-Update')")
    @ApiOperation(value = "更新班组", tags = {"班组" },  notes = "更新班组")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}")
    public ResponseEntity<PFTeamDTO> update(@PathVariable("pfteam_id") String pfteam_id, @RequestBody PFTeamDTO pfteamdto) {
		PFTeam domain  = pfteamMapping.toDomain(pfteamdto);
        domain .setPfteamid(pfteam_id);
		pfteamService.update(domain );
		PFTeamDTO dto = pfteamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfteamService.getPfteamByEntities(this.pfteamMapping.toDomain(#pfteamdtos)),'eam-PFTeam-Update')")
    @ApiOperation(value = "批量更新班组", tags = {"班组" },  notes = "批量更新班组")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFTeamDTO> pfteamdtos) {
        pfteamService.updateBatch(pfteamMapping.toDomain(pfteamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pfteamService.get(#pfteam_id),'eam-PFTeam-Remove')")
    @ApiOperation(value = "删除班组", tags = {"班组" },  notes = "删除班组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pfteam_id") String pfteam_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pfteamService.remove(pfteam_id));
    }

    @PreAuthorize("hasPermission(this.pfteamService.getPfteamByIds(#ids),'eam-PFTeam-Remove')")
    @ApiOperation(value = "批量删除班组", tags = {"班组" },  notes = "批量删除班组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pfteamService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pfteamMapping.toDomain(returnObject.body),'eam-PFTeam-Get')")
    @ApiOperation(value = "获取班组", tags = {"班组" },  notes = "获取班组")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}")
    public ResponseEntity<PFTeamDTO> get(@PathVariable("pfteam_id") String pfteam_id) {
        PFTeam domain = pfteamService.get(pfteam_id);
        PFTeamDTO dto = pfteamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取班组草稿", tags = {"班组" },  notes = "获取班组草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/getdraft")
    public ResponseEntity<PFTeamDTO> getDraft(PFTeamDTO dto) {
        PFTeam domain = pfteamMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(pfteamMapping.toDto(pfteamService.getDraft(domain)));
    }

    @ApiOperation(value = "检查班组", tags = {"班组" },  notes = "检查班组")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFTeamDTO pfteamdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pfteamService.checkKey(pfteamMapping.toDomain(pfteamdto)));
    }

    @PreAuthorize("hasPermission(this.pfteamMapping.toDomain(#pfteamdto),'eam-PFTeam-Save')")
    @ApiOperation(value = "保存班组", tags = {"班组" },  notes = "保存班组")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/save")
    public ResponseEntity<PFTeamDTO> save(@RequestBody PFTeamDTO pfteamdto) {
        PFTeam domain = pfteamMapping.toDomain(pfteamdto);
        pfteamService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(pfteamMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.pfteamMapping.toDomain(#pfteamdtos),'eam-PFTeam-Save')")
    @ApiOperation(value = "批量保存班组", tags = {"班组" },  notes = "批量保存班组")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFTeamDTO> pfteamdtos) {
        pfteamService.saveBatch(pfteamMapping.toDomain(pfteamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFTeam-searchDefault-all') and hasPermission(#context,'eam-PFTeam-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"班组" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/fetchdefault")
	public ResponseEntity<List<PFTeamDTO>> fetchDefault(PFTeamSearchContext context) {
        Page<PFTeam> domains = pfteamService.searchDefault(context) ;
        List<PFTeamDTO> list = pfteamMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFTeam-searchDefault-all') and hasPermission(#context,'eam-PFTeam-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"班组" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/searchdefault")
	public ResponseEntity<Page<PFTeamDTO>> searchDefault(@RequestBody PFTeamSearchContext context) {
        Page<PFTeam> domains = pfteamService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfteamMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

