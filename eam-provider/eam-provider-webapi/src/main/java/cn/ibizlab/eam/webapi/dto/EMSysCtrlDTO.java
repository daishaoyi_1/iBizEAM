package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMSysCtrlDTO]
 */
@Data
public class EMSysCtrlDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WHSJ]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "whsj" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("whsj")
    @NotNull(message = "[维护时间]不允许为空!")
    private Timestamp whsj;

    /**
     * 属性 [EMSYSCTRLID]
     *
     */
    @JSONField(name = "emsysctrlid")
    @JsonProperty("emsysctrlid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emsysctrlid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [WHINFO]
     *
     */
    @JSONField(name = "whinfo")
    @JsonProperty("whinfo")
    @NotBlank(message = "[维护信息]不允许为空!")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String whinfo;

    /**
     * 属性 [EMSYSCTRLNAME]
     *
     */
    @JSONField(name = "emsysctrlname")
    @JsonProperty("emsysctrlname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emsysctrlname;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;


    /**
     * 设置 [WHSJ]
     */
    public void setWhsj(Timestamp  whsj){
        this.whsj = whsj ;
        this.modify("whsj",whsj);
    }

    /**
     * 设置 [WHINFO]
     */
    public void setWhinfo(String  whinfo){
        this.whinfo = whinfo ;
        this.modify("whinfo",whinfo);
    }

    /**
     * 设置 [EMSYSCTRLNAME]
     */
    public void setEmsysctrlname(String  emsysctrlname){
        this.emsysctrlname = emsysctrlname ;
        this.modify("emsysctrlname",emsysctrlname);
    }


}


