package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemTradeSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"物品交易" })
@RestController("WebApi-emitemtrade")
@RequestMapping("")
public class EMItemTradeResource {

    @Autowired
    public IEMItemTradeService emitemtradeService;

    @Autowired
    @Lazy
    public EMItemTradeMapping emitemtradeMapping;

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "新建物品交易", tags = {"物品交易" },  notes = "新建物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtrades")
    public ResponseEntity<EMItemTradeDTO> create(@Validated @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
		emitemtradeService.create(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "批量新建物品交易", tags = {"物品交易" },  notes = "批量新建物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtrades/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        emitemtradeService.createBatch(emitemtradeMapping.toDomain(emitemtradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemtrade" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "更新物品交易", tags = {"物品交易" },  notes = "更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> update(@PathVariable("emitemtrade_id") String emitemtrade_id, @RequestBody EMItemTradeDTO emitemtradedto) {
		EMItemTrade domain  = emitemtradeMapping.toDomain(emitemtradedto);
        domain .setEmitemtradeid(emitemtrade_id);
		emitemtradeService.update(domain );
		EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByEntities(this.emitemtradeMapping.toDomain(#emitemtradedtos)),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "批量更新物品交易", tags = {"物品交易" },  notes = "批量更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemtrades/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        emitemtradeService.updateBatch(emitemtradeMapping.toDomain(emitemtradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "删除物品交易", tags = {"物品交易" },  notes = "删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitemtrade_id") String emitemtrade_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.remove(emitemtrade_id));
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByIds(#ids),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "批量删除物品交易", tags = {"物品交易" },  notes = "批量删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemtrades/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemtradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemtradeMapping.toDomain(returnObject.body),'eam-EMItemTrade-Get')")
    @ApiOperation(value = "获取物品交易", tags = {"物品交易" },  notes = "获取物品交易")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> get(@PathVariable("emitemtrade_id") String emitemtrade_id) {
        EMItemTrade domain = emitemtradeService.get(emitemtrade_id);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取物品交易草稿", tags = {"物品交易" },  notes = "获取物品交易草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemtrades/getdraft")
    public ResponseEntity<EMItemTradeDTO> getDraft(EMItemTradeDTO dto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(emitemtradeService.getDraft(domain)));
    }

    @ApiOperation(value = "检查物品交易", tags = {"物品交易" },  notes = "检查物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtrades/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemTradeDTO emitemtradedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.checkKey(emitemtradeMapping.toDomain(emitemtradedto)));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "保存物品交易", tags = {"物品交易" },  notes = "保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtrades/save")
    public ResponseEntity<EMItemTradeDTO> save(@RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        emitemtradeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "批量保存物品交易", tags = {"物品交易" },  notes = "批量保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtrades/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        emitemtradeService.saveBatch(emitemtradeMapping.toDomain(emitemtradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"物品交易" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitemtrades/fetchdefault")
	public ResponseEntity<List<EMItemTradeDTO>> fetchDefault(EMItemTradeSearchContext context) {
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"物品交易" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitemtrades/searchdefault")
	public ResponseEntity<Page<EMItemTradeDTO>> searchDefault(@RequestBody EMItemTradeSearchContext context) {
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "获取IndexDER", tags = {"物品交易" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emitemtrades/fetchindexder")
	public ResponseEntity<List<EMItemTradeDTO>> fetchIndexDER(EMItemTradeSearchContext context) {
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "查询IndexDER", tags = {"物品交易" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emitemtrades/searchindexder")
	public ResponseEntity<Page<EMItemTradeDTO>> searchIndexDER(@RequestBody EMItemTradeSearchContext context) {
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取年度出入库数量", tags = {"物品交易" } ,notes = "获取年度出入库数量")
    @RequestMapping(method= RequestMethod.GET , value="/emitemtrades/fetchyearnumbyitem")
	public ResponseEntity<List<Map>> fetchYearNumByItem(EMItemTradeSearchContext context) {
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询年度出入库数量", tags = {"物品交易" } ,notes = "查询年度出入库数量")
    @RequestMapping(method= RequestMethod.POST , value="/emitemtrades/searchyearnumbyitem")
	public ResponseEntity<Page<Map>> searchYearNumByItem(@RequestBody EMItemTradeSearchContext context) {
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据物品建立物品交易", tags = {"物品交易" },  notes = "根据物品建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemtrades")
    public ResponseEntity<EMItemTradeDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
		emitemtradeService.create(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据物品批量建立物品交易", tags = {"物品交易" },  notes = "根据物品批量建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemtrade" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据物品更新物品交易", tags = {"物品交易" },  notes = "根据物品更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        domain.setEmitemtradeid(emitemtrade_id);
		emitemtradeService.update(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByEntities(this.emitemtradeMapping.toDomain(#emitemtradedtos)),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据物品批量更新物品交易", tags = {"物品交易" },  notes = "根据物品批量更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据物品删除物品交易", tags = {"物品交易" },  notes = "根据物品删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.remove(emitemtrade_id));
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByIds(#ids),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据物品批量删除物品交易", tags = {"物品交易" },  notes = "根据物品批量删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emitemtradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemtradeMapping.toDomain(returnObject.body),'eam-EMItemTrade-Get')")
    @ApiOperation(value = "根据物品获取物品交易", tags = {"物品交易" },  notes = "根据物品获取物品交易")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
        EMItemTrade domain = emitemtradeService.get(emitemtrade_id);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取物品交易草稿", tags = {"物品交易" },  notes = "根据物品获取物品交易草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemtrades/getdraft")
    public ResponseEntity<EMItemTradeDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMItemTradeDTO dto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(emitemtradeService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查物品交易", tags = {"物品交易" },  notes = "根据物品检查物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemtrades/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.checkKey(emitemtradeMapping.toDomain(emitemtradedto)));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据物品保存物品交易", tags = {"物品交易" },  notes = "根据物品保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemtrades/save")
    public ResponseEntity<EMItemTradeDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        emitemtradeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据物品批量保存物品交易", tags = {"物品交易" },  notes = "根据物品批量保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemtrades/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemtradeService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"物品交易" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemtrades/fetchdefault")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"物品交易" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemtrades/searchdefault")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据物品获取IndexDER", tags = {"物品交易" } ,notes = "根据物品获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemtrades/fetchindexder")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeIndexDERByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据物品查询IndexDER", tags = {"物品交易" } ,notes = "根据物品查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemtrades/searchindexder")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeIndexDERByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据物品获取年度出入库数量", tags = {"物品交易" } ,notes = "根据物品获取年度出入库数量")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemtrades/fetchyearnumbyitem")
	public ResponseEntity<List<Map>> fetchEMItemTradeYearNumByItemByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据物品查询年度出入库数量", tags = {"物品交易" } ,notes = "根据物品查询年度出入库数量")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemtrades/searchyearnumbyitem")
	public ResponseEntity<Page<Map>> searchEMItemTradeYearNumByItemByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据服务商物品建立物品交易", tags = {"物品交易" },  notes = "根据服务商物品建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades")
    public ResponseEntity<EMItemTradeDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
		emitemtradeService.create(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据服务商物品批量建立物品交易", tags = {"物品交易" },  notes = "根据服务商物品批量建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemtrade" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据服务商物品更新物品交易", tags = {"物品交易" },  notes = "根据服务商物品更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        domain.setEmitemtradeid(emitemtrade_id);
		emitemtradeService.update(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByEntities(this.emitemtradeMapping.toDomain(#emitemtradedtos)),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据服务商物品批量更新物品交易", tags = {"物品交易" },  notes = "根据服务商物品批量更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据服务商物品删除物品交易", tags = {"物品交易" },  notes = "根据服务商物品删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.remove(emitemtrade_id));
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByIds(#ids),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除物品交易", tags = {"物品交易" },  notes = "根据服务商物品批量删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emitemtradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemtradeMapping.toDomain(returnObject.body),'eam-EMItemTrade-Get')")
    @ApiOperation(value = "根据服务商物品获取物品交易", tags = {"物品交易" },  notes = "根据服务商物品获取物品交易")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
        EMItemTrade domain = emitemtradeService.get(emitemtrade_id);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取物品交易草稿", tags = {"物品交易" },  notes = "根据服务商物品获取物品交易草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/getdraft")
    public ResponseEntity<EMItemTradeDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMItemTradeDTO dto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(emitemtradeService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查物品交易", tags = {"物品交易" },  notes = "根据服务商物品检查物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.checkKey(emitemtradeMapping.toDomain(emitemtradedto)));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据服务商物品保存物品交易", tags = {"物品交易" },  notes = "根据服务商物品保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/save")
    public ResponseEntity<EMItemTradeDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        emitemtradeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据服务商物品批量保存物品交易", tags = {"物品交易" },  notes = "根据服务商物品批量保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemtradeService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"物品交易" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/fetchdefault")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"物品交易" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/searchdefault")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据服务商物品获取IndexDER", tags = {"物品交易" } ,notes = "根据服务商物品获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/fetchindexder")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeIndexDERByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据服务商物品查询IndexDER", tags = {"物品交易" } ,notes = "根据服务商物品查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/searchindexder")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeIndexDERByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商物品获取年度出入库数量", tags = {"物品交易" } ,notes = "根据服务商物品获取年度出入库数量")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/fetchyearnumbyitem")
	public ResponseEntity<List<Map>> fetchEMItemTradeYearNumByItemByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商物品查询年度出入库数量", tags = {"物品交易" } ,notes = "根据服务商物品查询年度出入库数量")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemtrades/searchyearnumbyitem")
	public ResponseEntity<Page<Map>> searchEMItemTradeYearNumByItemByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据仓库库位物品建立物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades")
    public ResponseEntity<EMItemTradeDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
		emitemtradeService.create(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品批量建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemtrade" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据仓库库位物品更新物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        domain.setEmitemtradeid(emitemtrade_id);
		emitemtradeService.update(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByEntities(this.emitemtradeMapping.toDomain(#emitemtradedtos)),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品批量更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.remove(emitemtrade_id));
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByIds(#ids),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品批量删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemtradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemtradeMapping.toDomain(returnObject.body),'eam-EMItemTrade-Get')")
    @ApiOperation(value = "根据仓库库位物品获取物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品获取物品交易")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
        EMItemTrade domain = emitemtradeService.get(emitemtrade_id);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取物品交易草稿", tags = {"物品交易" },  notes = "根据仓库库位物品获取物品交易草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/getdraft")
    public ResponseEntity<EMItemTradeDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemTradeDTO dto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(emitemtradeService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品检查物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.checkKey(emitemtradeMapping.toDomain(emitemtradedto)));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据仓库库位物品保存物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/save")
    public ResponseEntity<EMItemTradeDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        emitemtradeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存物品交易", tags = {"物品交易" },  notes = "根据仓库库位物品批量保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemtradeService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"物品交易" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/fetchdefault")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"物品交易" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/searchdefault")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库库位物品获取IndexDER", tags = {"物品交易" } ,notes = "根据仓库库位物品获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/fetchindexder")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeIndexDERByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库库位物品查询IndexDER", tags = {"物品交易" } ,notes = "根据仓库库位物品查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/searchindexder")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeIndexDERByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库库位物品获取年度出入库数量", tags = {"物品交易" } ,notes = "根据仓库库位物品获取年度出入库数量")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/fetchyearnumbyitem")
	public ResponseEntity<List<Map>> fetchEMItemTradeYearNumByItemByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库库位物品查询年度出入库数量", tags = {"物品交易" } ,notes = "根据仓库库位物品查询年度出入库数量")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/searchyearnumbyitem")
	public ResponseEntity<Page<Map>> searchEMItemTradeYearNumByItemByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据仓库物品建立物品交易", tags = {"物品交易" },  notes = "根据仓库物品建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades")
    public ResponseEntity<EMItemTradeDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
		emitemtradeService.create(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据仓库物品批量建立物品交易", tags = {"物品交易" },  notes = "根据仓库物品批量建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemtrade" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据仓库物品更新物品交易", tags = {"物品交易" },  notes = "根据仓库物品更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        domain.setEmitemtradeid(emitemtrade_id);
		emitemtradeService.update(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByEntities(this.emitemtradeMapping.toDomain(#emitemtradedtos)),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据仓库物品批量更新物品交易", tags = {"物品交易" },  notes = "根据仓库物品批量更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据仓库物品删除物品交易", tags = {"物品交易" },  notes = "根据仓库物品删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.remove(emitemtrade_id));
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByIds(#ids),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除物品交易", tags = {"物品交易" },  notes = "根据仓库物品批量删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emitemtradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemtradeMapping.toDomain(returnObject.body),'eam-EMItemTrade-Get')")
    @ApiOperation(value = "根据仓库物品获取物品交易", tags = {"物品交易" },  notes = "根据仓库物品获取物品交易")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
        EMItemTrade domain = emitemtradeService.get(emitemtrade_id);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取物品交易草稿", tags = {"物品交易" },  notes = "根据仓库物品获取物品交易草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/getdraft")
    public ResponseEntity<EMItemTradeDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMItemTradeDTO dto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(emitemtradeService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查物品交易", tags = {"物品交易" },  notes = "根据仓库物品检查物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.checkKey(emitemtradeMapping.toDomain(emitemtradedto)));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据仓库物品保存物品交易", tags = {"物品交易" },  notes = "根据仓库物品保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/save")
    public ResponseEntity<EMItemTradeDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        emitemtradeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据仓库物品批量保存物品交易", tags = {"物品交易" },  notes = "根据仓库物品批量保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemtradeService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"物品交易" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/fetchdefault")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"物品交易" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/searchdefault")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库物品获取IndexDER", tags = {"物品交易" } ,notes = "根据仓库物品获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/fetchindexder")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeIndexDERByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库物品查询IndexDER", tags = {"物品交易" } ,notes = "根据仓库物品查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/searchindexder")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeIndexDERByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库物品获取年度出入库数量", tags = {"物品交易" } ,notes = "根据仓库物品获取年度出入库数量")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/fetchyearnumbyitem")
	public ResponseEntity<List<Map>> fetchEMItemTradeYearNumByItemByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库物品查询年度出入库数量", tags = {"物品交易" } ,notes = "根据仓库物品查询年度出入库数量")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemtrades/searchyearnumbyitem")
	public ResponseEntity<Page<Map>> searchEMItemTradeYearNumByItemByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades")
    public ResponseEntity<EMItemTradeDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
		emitemtradeService.create(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品批量建立物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemtrade" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        domain.setEmitemtradeid(emitemtrade_id);
		emitemtradeService.update(domain);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByEntities(this.emitemtradeMapping.toDomain(#emitemtradedtos)),'eam-EMItemTrade-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品批量更新物品交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemtradeService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.get(#emitemtrade_id),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.remove(emitemtrade_id));
    }

    @PreAuthorize("hasPermission(this.emitemtradeService.getEmitemtradeByIds(#ids),'eam-EMItemTrade-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品批量删除物品交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemtradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemtradeMapping.toDomain(returnObject.body),'eam-EMItemTrade-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品获取物品交易")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/{emitemtrade_id}")
    public ResponseEntity<EMItemTradeDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemtrade_id") String emitemtrade_id) {
        EMItemTrade domain = emitemtradeService.get(emitemtrade_id);
        EMItemTradeDTO dto = emitemtradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取物品交易草稿", tags = {"物品交易" },  notes = "根据仓库仓库库位物品获取物品交易草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/getdraft")
    public ResponseEntity<EMItemTradeDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemTradeDTO dto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(emitemtradeService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品检查物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemtradeService.checkKey(emitemtradeMapping.toDomain(emitemtradedto)));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedto),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/save")
    public ResponseEntity<EMItemTradeDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeDTO emitemtradedto) {
        EMItemTrade domain = emitemtradeMapping.toDomain(emitemtradedto);
        domain.setItemid(emitem_id);
        emitemtradeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtradeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemtradeMapping.toDomain(#emitemtradedtos),'eam-EMItemTrade-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存物品交易", tags = {"物品交易" },  notes = "根据仓库仓库库位物品批量保存物品交易")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemTradeDTO> emitemtradedtos) {
        List<EMItemTrade> domainlist=emitemtradeMapping.toDomain(emitemtradedtos);
        for(EMItemTrade domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemtradeService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"物品交易" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/fetchdefault")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchDefault-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"物品交易" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/searchdefault")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取IndexDER", tags = {"物品交易" } ,notes = "根据仓库仓库库位物品获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/fetchindexder")
	public ResponseEntity<List<EMItemTradeDTO>> fetchEMItemTradeIndexDERByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
        List<EMItemTradeDTO> list = emitemtradeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemTrade-searchIndexDER-all') and hasPermission(#context,'eam-EMItemTrade-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询IndexDER", tags = {"物品交易" } ,notes = "根据仓库仓库库位物品查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/searchindexder")
	public ResponseEntity<Page<EMItemTradeDTO>> searchEMItemTradeIndexDERByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemTrade> domains = emitemtradeService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库仓库库位物品获取年度出入库数量", tags = {"物品交易" } ,notes = "根据仓库仓库库位物品获取年度出入库数量")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/fetchyearnumbyitem")
	public ResponseEntity<List<Map>> fetchEMItemTradeYearNumByItemByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库仓库库位物品查询年度出入库数量", tags = {"物品交易" } ,notes = "根据仓库仓库库位物品查询年度出入库数量")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemtrades/searchyearnumbyitem")
	public ResponseEntity<Page<Map>> searchEMItemTradeYearNumByItemByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemTradeSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emitemtradeService.searchYearNumByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
}

