package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[PFUnitDTO]
 */
@Data
public class PFUnitDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [PFUNITNAME]
     *
     */
    @JSONField(name = "pfunitname")
    @JsonProperty("pfunitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfunitname;

    /**
     * 属性 [UNITCODE]
     *
     */
    @JSONField(name = "unitcode")
    @JsonProperty("unitcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitcode;

    /**
     * 属性 [UNITINFO]
     *
     */
    @JSONField(name = "unitinfo")
    @JsonProperty("unitinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitinfo;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PFUNITID]
     *
     */
    @JSONField(name = "pfunitid")
    @JsonProperty("pfunitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfunitid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PFUNITNAME]
     */
    public void setPfunitname(String  pfunitname){
        this.pfunitname = pfunitname ;
        this.modify("pfunitname",pfunitname);
    }

    /**
     * 设置 [UNITCODE]
     */
    public void setUnitcode(String  unitcode){
        this.unitcode = unitcode ;
        this.modify("unitcode",unitcode);
    }


}


