package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMResRefObjDTO]
 */
@Data
public class EMResRefObjDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [REGIONBEGINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "regionbegindate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    private Timestamp regionbegindate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMRESREFOBJNAME]
     *
     */
    @JSONField(name = "emresrefobjname")
    @JsonProperty("emresrefobjname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emresrefobjname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [RESREFOBJINFO]
     *
     */
    @JSONField(name = "resrefobjinfo")
    @JsonProperty("resrefobjinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resrefobjinfo;

    /**
     * 属性 [EMRESREFOBJID]
     *
     */
    @JSONField(name = "emresrefobjid")
    @JsonProperty("emresrefobjid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emresrefobjid;

    /**
     * 属性 [EMRESREFOBJTYPE]
     *
     */
    @JSONField(name = "emresrefobjtype")
    @JsonProperty("emresrefobjtype")
    @NotBlank(message = "[引用对象类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emresrefobjtype;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [RESREFOBJPNAME]
     *
     */
    @JSONField(name = "resrefobjpname")
    @JsonProperty("resrefobjpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resrefobjpname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [RESREFOBJPID]
     *
     */
    @JSONField(name = "resrefobjpid")
    @JsonProperty("resrefobjpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resrefobjpid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;


    /**
     * 设置 [REGIONBEGINDATE]
     */
    public void setRegionbegindate(Timestamp  regionbegindate){
        this.regionbegindate = regionbegindate ;
        this.modify("regionbegindate",regionbegindate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMRESREFOBJNAME]
     */
    public void setEmresrefobjname(String  emresrefobjname){
        this.emresrefobjname = emresrefobjname ;
        this.modify("emresrefobjname",emresrefobjname);
    }

    /**
     * 设置 [EMRESREFOBJTYPE]
     */
    public void setEmresrefobjtype(String  emresrefobjtype){
        this.emresrefobjtype = emresrefobjtype ;
        this.modify("emresrefobjtype",emresrefobjtype);
    }

    /**
     * 设置 [RESREFOBJPID]
     */
    public void setResrefobjpid(String  resrefobjpid){
        this.resrefobjpid = resrefobjpid ;
        this.modify("resrefobjpid",resrefobjpid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }


}


