package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMItemSubMapDTO]
 */
@Data
public class EMItemSubMapDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMITEMSUBMAPID]
     *
     */
    @JSONField(name = "emitemsubmapid")
    @JsonProperty("emitemsubmapid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitemsubmapid;

    /**
     * 属性 [EMITEMSUBMAPNAME]
     *
     */
    @JSONField(name = "emitemsubmapname")
    @JsonProperty("emitemsubmapname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitemsubmapname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [SUBCOEFF]
     *
     */
    @JSONField(name = "subcoeff")
    @JsonProperty("subcoeff")
    private Double subcoeff;

    /**
     * 属性 [EXCFLAG]
     *
     */
    @JSONField(name = "excflag")
    @JsonProperty("excflag")
    private Integer excflag;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [SUBITEMNAME]
     *
     */
    @JSONField(name = "subitemname")
    @JsonProperty("subitemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String subitemname;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [SUBITEMID]
     *
     */
    @JSONField(name = "subitemid")
    @JsonProperty("subitemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String subitemid;


    /**
     * 设置 [EMITEMSUBMAPNAME]
     */
    public void setEmitemsubmapname(String  emitemsubmapname){
        this.emitemsubmapname = emitemsubmapname ;
        this.modify("emitemsubmapname",emitemsubmapname);
    }

    /**
     * 设置 [SUBCOEFF]
     */
    public void setSubcoeff(Double  subcoeff){
        this.subcoeff = subcoeff ;
        this.modify("subcoeff",subcoeff);
    }

    /**
     * 设置 [EXCFLAG]
     */
    public void setExcflag(Integer  excflag){
        this.excflag = excflag ;
        this.modify("excflag",excflag);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [SUBITEMID]
     */
    public void setSubitemid(String  subitemid){
        this.subitemid = subitemid ;
        this.modify("subitemid",subitemid);
    }


}


