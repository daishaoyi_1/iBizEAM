package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBattery;
import cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryService;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatterySearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"电瓶" })
@RestController("WebApi-emeibattery")
@RequestMapping("")
public class EMEIBatteryResource {

    @Autowired
    public IEMEIBatteryService emeibatteryService;

    @Autowired
    @Lazy
    public EMEIBatteryMapping emeibatteryMapping;

    @PreAuthorize("hasPermission(this.emeibatteryMapping.toDomain(#emeibatterydto),'eam-EMEIBattery-Create')")
    @ApiOperation(value = "新建电瓶", tags = {"电瓶" },  notes = "新建电瓶")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteries")
    public ResponseEntity<EMEIBatteryDTO> create(@Validated @RequestBody EMEIBatteryDTO emeibatterydto) {
        EMEIBattery domain = emeibatteryMapping.toDomain(emeibatterydto);
		emeibatteryService.create(domain);
        EMEIBatteryDTO dto = emeibatteryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatteryMapping.toDomain(#emeibatterydtos),'eam-EMEIBattery-Create')")
    @ApiOperation(value = "批量新建电瓶", tags = {"电瓶" },  notes = "批量新建电瓶")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteries/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEIBatteryDTO> emeibatterydtos) {
        emeibatteryService.createBatch(emeibatteryMapping.toDomain(emeibatterydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeibattery" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeibatteryService.get(#emeibattery_id),'eam-EMEIBattery-Update')")
    @ApiOperation(value = "更新电瓶", tags = {"电瓶" },  notes = "更新电瓶")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatteries/{emeibattery_id}")
    public ResponseEntity<EMEIBatteryDTO> update(@PathVariable("emeibattery_id") String emeibattery_id, @RequestBody EMEIBatteryDTO emeibatterydto) {
		EMEIBattery domain  = emeibatteryMapping.toDomain(emeibatterydto);
        domain .setEmeibatteryid(emeibattery_id);
		emeibatteryService.update(domain );
		EMEIBatteryDTO dto = emeibatteryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatteryService.getEmeibatteryByEntities(this.emeibatteryMapping.toDomain(#emeibatterydtos)),'eam-EMEIBattery-Update')")
    @ApiOperation(value = "批量更新电瓶", tags = {"电瓶" },  notes = "批量更新电瓶")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatteries/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEIBatteryDTO> emeibatterydtos) {
        emeibatteryService.updateBatch(emeibatteryMapping.toDomain(emeibatterydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeibatteryService.get(#emeibattery_id),'eam-EMEIBattery-Remove')")
    @ApiOperation(value = "删除电瓶", tags = {"电瓶" },  notes = "删除电瓶")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatteries/{emeibattery_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeibattery_id") String emeibattery_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeibatteryService.remove(emeibattery_id));
    }

    @PreAuthorize("hasPermission(this.emeibatteryService.getEmeibatteryByIds(#ids),'eam-EMEIBattery-Remove')")
    @ApiOperation(value = "批量删除电瓶", tags = {"电瓶" },  notes = "批量删除电瓶")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatteries/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeibatteryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeibatteryMapping.toDomain(returnObject.body),'eam-EMEIBattery-Get')")
    @ApiOperation(value = "获取电瓶", tags = {"电瓶" },  notes = "获取电瓶")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatteries/{emeibattery_id}")
    public ResponseEntity<EMEIBatteryDTO> get(@PathVariable("emeibattery_id") String emeibattery_id) {
        EMEIBattery domain = emeibatteryService.get(emeibattery_id);
        EMEIBatteryDTO dto = emeibatteryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取电瓶草稿", tags = {"电瓶" },  notes = "获取电瓶草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatteries/getdraft")
    public ResponseEntity<EMEIBatteryDTO> getDraft(EMEIBatteryDTO dto) {
        EMEIBattery domain = emeibatteryMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeibatteryMapping.toDto(emeibatteryService.getDraft(domain)));
    }

    @ApiOperation(value = "检查电瓶", tags = {"电瓶" },  notes = "检查电瓶")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteries/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEIBatteryDTO emeibatterydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeibatteryService.checkKey(emeibatteryMapping.toDomain(emeibatterydto)));
    }

    @PreAuthorize("hasPermission(this.emeibatteryMapping.toDomain(#emeibatterydto),'eam-EMEIBattery-Save')")
    @ApiOperation(value = "保存电瓶", tags = {"电瓶" },  notes = "保存电瓶")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteries/save")
    public ResponseEntity<EMEIBatteryDTO> save(@RequestBody EMEIBatteryDTO emeibatterydto) {
        EMEIBattery domain = emeibatteryMapping.toDomain(emeibatterydto);
        emeibatteryService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeibatteryMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeibatteryMapping.toDomain(#emeibatterydtos),'eam-EMEIBattery-Save')")
    @ApiOperation(value = "批量保存电瓶", tags = {"电瓶" },  notes = "批量保存电瓶")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteries/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEIBatteryDTO> emeibatterydtos) {
        emeibatteryService.saveBatch(emeibatteryMapping.toDomain(emeibatterydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBattery-searchDefault-all') and hasPermission(#context,'eam-EMEIBattery-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"电瓶" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeibatteries/fetchdefault")
	public ResponseEntity<List<EMEIBatteryDTO>> fetchDefault(EMEIBatterySearchContext context) {
        Page<EMEIBattery> domains = emeibatteryService.searchDefault(context) ;
        List<EMEIBatteryDTO> list = emeibatteryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBattery-searchDefault-all') and hasPermission(#context,'eam-EMEIBattery-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"电瓶" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeibatteries/searchdefault")
	public ResponseEntity<Page<EMEIBatteryDTO>> searchDefault(@RequestBody EMEIBatterySearchContext context) {
        Page<EMEIBattery> domains = emeibatteryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeibatteryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

