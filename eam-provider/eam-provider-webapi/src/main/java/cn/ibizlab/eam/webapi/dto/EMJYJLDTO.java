package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMJYJLDTO]
 */
@Data
public class EMJYJLDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [JIAYOUPZ]
     *
     */
    @JSONField(name = "jiayoupz")
    @JsonProperty("jiayoupz")
    @NotBlank(message = "[加油品种]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jiayoupz;

    /**
     * 属性 [JIAYOUSHEBEI]
     *
     */
    @JSONField(name = "jiayoushebei")
    @JsonProperty("jiayoushebei")
    @NotBlank(message = "[加油设备]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jiayoushebei;

    /**
     * 属性 [ISZBSC]
     *
     */
    @JSONField(name = "iszbsc")
    @JsonProperty("iszbsc")
    @NotNull(message = "[暂不生产领料单]不允许为空!")
    private Integer iszbsc;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [JIAYOUNAME]
     *
     */
    @JSONField(name = "jiayouname")
    @JsonProperty("jiayouname")
    @NotBlank(message = "[加油人]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jiayouname;

    /**
     * 属性 [JIAYOUSHIJIAN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "jiayoushijian" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("jiayoushijian")
    @NotNull(message = "[加油时间]不允许为空!")
    private Timestamp jiayoushijian;

    /**
     * 属性 [DENS]
     *
     */
    @JSONField(name = "dens")
    @JsonProperty("dens")
    private Double dens;

    /**
     * 属性 [JIAYOULIANG]
     *
     */
    @JSONField(name = "jiayouliang")
    @JsonProperty("jiayouliang")
    @NotNull(message = "[加油量（升）]不允许为空!")
    private Double jiayouliang;

    /**
     * 属性 [ISCPUSE]
     *
     */
    @JSONField(name = "iscpuse")
    @JsonProperty("iscpuse")
    @NotNull(message = "[已生成领料单]不允许为空!")
    private Integer iscpuse;

    /**
     * 属性 [DEPTNAME]
     *
     */
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptname;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [EMJYJLID]
     *
     */
    @JSONField(name = "emjyjlid")
    @JsonProperty("emjyjlid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emjyjlid;

    /**
     * 属性 [EMJYJLNAME]
     *
     */
    @JSONField(name = "emjyjlname")
    @JsonProperty("emjyjlname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emjyjlname;

    /**
     * 属性 [JIAYOUID]
     *
     */
    @JSONField(name = "jiayouid")
    @JsonProperty("jiayouid")
    @NotBlank(message = "[加油人ID]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jiayouid;

    /**
     * 属性 [PSUM]
     *
     */
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empname;

    /**
     * 属性 [ITEMDENS]
     *
     */
    @JSONField(name = "itemdens")
    @JsonProperty("itemdens")
    private Double itemdens;

    /**
     * 属性 [TEAMNAME]
     *
     */
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamname;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [EQORGID]
     *
     */
    @JSONField(name = "eqorgid")
    @JsonProperty("eqorgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String eqorgid;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [ITEMPUSENAME]
     *
     */
    @JSONField(name = "itempusename")
    @JsonProperty("itempusename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itempusename;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [ITEMPUSEID]
     *
     */
    @JSONField(name = "itempuseid")
    @JsonProperty("itempuseid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itempuseid;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [TEAMID]
     *
     */
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String teamid;


    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [JIAYOUPZ]
     */
    public void setJiayoupz(String  jiayoupz){
        this.jiayoupz = jiayoupz ;
        this.modify("jiayoupz",jiayoupz);
    }

    /**
     * 设置 [JIAYOUSHEBEI]
     */
    public void setJiayoushebei(String  jiayoushebei){
        this.jiayoushebei = jiayoushebei ;
        this.modify("jiayoushebei",jiayoushebei);
    }

    /**
     * 设置 [ISZBSC]
     */
    public void setIszbsc(Integer  iszbsc){
        this.iszbsc = iszbsc ;
        this.modify("iszbsc",iszbsc);
    }

    /**
     * 设置 [DEPTID]
     */
    public void setDeptid(String  deptid){
        this.deptid = deptid ;
        this.modify("deptid",deptid);
    }

    /**
     * 设置 [JIAYOUNAME]
     */
    public void setJiayouname(String  jiayouname){
        this.jiayouname = jiayouname ;
        this.modify("jiayouname",jiayouname);
    }

    /**
     * 设置 [JIAYOUSHIJIAN]
     */
    public void setJiayoushijian(Timestamp  jiayoushijian){
        this.jiayoushijian = jiayoushijian ;
        this.modify("jiayoushijian",jiayoushijian);
    }

    /**
     * 设置 [DENS]
     */
    public void setDens(Double  dens){
        this.dens = dens ;
        this.modify("dens",dens);
    }

    /**
     * 设置 [JIAYOULIANG]
     */
    public void setJiayouliang(Double  jiayouliang){
        this.jiayouliang = jiayouliang ;
        this.modify("jiayouliang",jiayouliang);
    }

    /**
     * 设置 [ISCPUSE]
     */
    public void setIscpuse(Integer  iscpuse){
        this.iscpuse = iscpuse ;
        this.modify("iscpuse",iscpuse);
    }

    /**
     * 设置 [DEPTNAME]
     */
    public void setDeptname(String  deptname){
        this.deptname = deptname ;
        this.modify("deptname",deptname);
    }

    /**
     * 设置 [EMJYJLNAME]
     */
    public void setEmjyjlname(String  emjyjlname){
        this.emjyjlname = emjyjlname ;
        this.modify("emjyjlname",emjyjlname);
    }

    /**
     * 设置 [JIAYOUID]
     */
    public void setJiayouid(String  jiayouid){
        this.jiayouid = jiayouid ;
        this.modify("jiayouid",jiayouid);
    }

    /**
     * 设置 [EMPNAME]
     */
    public void setEmpname(String  empname){
        this.empname = empname ;
        this.modify("empname",empname);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [ITEMPUSEID]
     */
    public void setItempuseid(String  itempuseid){
        this.itempuseid = itempuseid ;
        this.modify("itempuseid",itempuseid);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [TEAMID]
     */
    public void setTeamid(String  teamid){
        this.teamid = teamid ;
        this.modify("teamid",teamid);
    }


}


