package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPL;
import cn.ibizlab.eam.core.eam_core.service.IEMItemPLService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPLSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"损溢单" })
@RestController("WebApi-emitempl")
@RequestMapping("")
public class EMItemPLResource {

    @Autowired
    public IEMItemPLService emitemplService;

    @Autowired
    @Lazy
    public EMItemPLMapping emitemplMapping;

    @PreAuthorize("hasPermission(this.emitemplMapping.toDomain(#emitempldto),'eam-EMItemPL-Create')")
    @ApiOperation(value = "新建损溢单", tags = {"损溢单" },  notes = "新建损溢单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls")
    public ResponseEntity<EMItemPLDTO> create(@Validated @RequestBody EMItemPLDTO emitempldto) {
        EMItemPL domain = emitemplMapping.toDomain(emitempldto);
		emitemplService.create(domain);
        EMItemPLDTO dto = emitemplMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemplMapping.toDomain(#emitempldtos),'eam-EMItemPL-Create')")
    @ApiOperation(value = "批量新建损溢单", tags = {"损溢单" },  notes = "批量新建损溢单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemPLDTO> emitempldtos) {
        emitemplService.createBatch(emitemplMapping.toDomain(emitempldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemplService.get(#emitempl_id),'eam-EMItemPL-Update')")
    @ApiOperation(value = "更新损溢单", tags = {"损溢单" },  notes = "更新损溢单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitempls/{emitempl_id}")
    public ResponseEntity<EMItemPLDTO> update(@PathVariable("emitempl_id") String emitempl_id, @RequestBody EMItemPLDTO emitempldto) {
		EMItemPL domain  = emitemplMapping.toDomain(emitempldto);
        domain .setEmitemplid(emitempl_id);
		emitemplService.update(domain );
		EMItemPLDTO dto = emitemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemplService.getEmitemplByEntities(this.emitemplMapping.toDomain(#emitempldtos)),'eam-EMItemPL-Update')")
    @ApiOperation(value = "批量更新损溢单", tags = {"损溢单" },  notes = "批量更新损溢单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitempls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemPLDTO> emitempldtos) {
        emitemplService.updateBatch(emitemplMapping.toDomain(emitempldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemplService.get(#emitempl_id),'eam-EMItemPL-Remove')")
    @ApiOperation(value = "删除损溢单", tags = {"损溢单" },  notes = "删除损溢单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitempls/{emitempl_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitempl_id") String emitempl_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemplService.remove(emitempl_id));
    }

    @PreAuthorize("hasPermission(this.emitemplService.getEmitemplByIds(#ids),'eam-EMItemPL-Remove')")
    @ApiOperation(value = "批量删除损溢单", tags = {"损溢单" },  notes = "批量删除损溢单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitempls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemplService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemplMapping.toDomain(returnObject.body),'eam-EMItemPL-Get')")
    @ApiOperation(value = "获取损溢单", tags = {"损溢单" },  notes = "获取损溢单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitempls/{emitempl_id}")
    public ResponseEntity<EMItemPLDTO> get(@PathVariable("emitempl_id") String emitempl_id) {
        EMItemPL domain = emitemplService.get(emitempl_id);
        EMItemPLDTO dto = emitemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取损溢单草稿", tags = {"损溢单" },  notes = "获取损溢单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitempls/getdraft")
    public ResponseEntity<EMItemPLDTO> getDraft(EMItemPLDTO dto) {
        EMItemPL domain = emitemplMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemplMapping.toDto(emitemplService.getDraft(domain)));
    }

    @ApiOperation(value = "检查损溢单", tags = {"损溢单" },  notes = "检查损溢单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemPLDTO emitempldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemplService.checkKey(emitemplMapping.toDomain(emitempldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-Confirm-all')")
    @ApiOperation(value = "确认", tags = {"损溢单" },  notes = "确认")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/{emitempl_id}/confirm")
    public ResponseEntity<EMItemPLDTO> confirm(@PathVariable("emitempl_id") String emitempl_id, @RequestBody EMItemPLDTO emitempldto) {
        EMItemPL domain = emitemplMapping.toDomain(emitempldto);
        domain.setEmitemplid(emitempl_id);
        domain = emitemplService.confirm(domain);
        emitempldto = emitemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempldto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-Confirm-all')")
    @ApiOperation(value = "批量处理[确认]", tags = {"损溢单" },  notes = "批量处理[确认]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/confirmbatch")
    public ResponseEntity<Boolean> confirmBatch(@RequestBody List<EMItemPLDTO> emitempldtos) {
        List<EMItemPL> domains = emitemplMapping.toDomain(emitempldtos);
        boolean result = emitemplService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-FormUpdateByItem-all')")
    @ApiOperation(value = "表单更新-item", tags = {"损溢单" },  notes = "表单更新-item")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitempls/{emitempl_id}/formupdatebyitem")
    public ResponseEntity<EMItemPLDTO> formUpdateByItem(@PathVariable("emitempl_id") String emitempl_id, @RequestBody EMItemPLDTO emitempldto) {
        EMItemPL domain = emitemplMapping.toDomain(emitempldto);
        domain.setEmitemplid(emitempl_id);
        domain = emitemplService.formUpdateByItem(domain);
        emitempldto = emitemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempldto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-Rejected-all')")
    @ApiOperation(value = "驳回", tags = {"损溢单" },  notes = "驳回")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/{emitempl_id}/rejected")
    public ResponseEntity<EMItemPLDTO> rejected(@PathVariable("emitempl_id") String emitempl_id, @RequestBody EMItemPLDTO emitempldto) {
        EMItemPL domain = emitemplMapping.toDomain(emitempldto);
        domain.setEmitemplid(emitempl_id);
        domain = emitemplService.rejected(domain);
        emitempldto = emitemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempldto);
    }

    @PreAuthorize("hasPermission(this.emitemplMapping.toDomain(#emitempldto),'eam-EMItemPL-Save')")
    @ApiOperation(value = "保存损溢单", tags = {"损溢单" },  notes = "保存损溢单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/save")
    public ResponseEntity<EMItemPLDTO> save(@RequestBody EMItemPLDTO emitempldto) {
        EMItemPL domain = emitemplMapping.toDomain(emitempldto);
        emitemplService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemplMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemplMapping.toDomain(#emitempldtos),'eam-EMItemPL-Save')")
    @ApiOperation(value = "批量保存损溢单", tags = {"损溢单" },  notes = "批量保存损溢单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemPLDTO> emitempldtos) {
        emitemplService.saveBatch(emitemplMapping.toDomain(emitempldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-Submit-all')")
    @ApiOperation(value = "提交", tags = {"损溢单" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempls/{emitempl_id}/submit")
    public ResponseEntity<EMItemPLDTO> submit(@PathVariable("emitempl_id") String emitempl_id, @RequestBody EMItemPLDTO emitempldto) {
        EMItemPL domain = emitemplMapping.toDomain(emitempldto);
        domain.setEmitemplid(emitempl_id);
        domain = emitemplService.submit(domain);
        emitempldto = emitemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempldto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "获取已确认", tags = {"损溢单" } ,notes = "获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitempls/fetchconfirmed")
	public ResponseEntity<List<EMItemPLDTO>> fetchConfirmed(EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchConfirmed(context) ;
        List<EMItemPLDTO> list = emitemplMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchConfirmed-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "查询已确认", tags = {"损溢单" } ,notes = "查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitempls/searchconfirmed")
	public ResponseEntity<Page<EMItemPLDTO>> searchConfirmed(@RequestBody EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchDefault-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"损溢单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitempls/fetchdefault")
	public ResponseEntity<List<EMItemPLDTO>> fetchDefault(EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchDefault(context) ;
        List<EMItemPLDTO> list = emitemplMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchDefault-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"损溢单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitempls/searchdefault")
	public ResponseEntity<Page<EMItemPLDTO>> searchDefault(@RequestBody EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchDraft-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "获取草稿", tags = {"损溢单" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitempls/fetchdraft")
	public ResponseEntity<List<EMItemPLDTO>> fetchDraft(EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchDraft(context) ;
        List<EMItemPLDTO> list = emitemplMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchDraft-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "查询草稿", tags = {"损溢单" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitempls/searchdraft")
	public ResponseEntity<Page<EMItemPLDTO>> searchDraft(@RequestBody EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "获取待确认", tags = {"损溢单" } ,notes = "获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitempls/fetchtoconfirm")
	public ResponseEntity<List<EMItemPLDTO>> fetchToConfirm(EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchToConfirm(context) ;
        List<EMItemPLDTO> list = emitemplMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPL-searchToConfirm-all') and hasPermission(#context,'eam-EMItemPL-Get')")
	@ApiOperation(value = "查询待确认", tags = {"损溢单" } ,notes = "查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitempls/searchtoconfirm")
	public ResponseEntity<Page<EMItemPLDTO>> searchToConfirm(@RequestBody EMItemPLSearchContext context) {
        Page<EMItemPL> domains = emitemplService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

