package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPlanTemplDTO]
 */
@Data
public class EMPlanTemplDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PLANTEMPLINFO]
     *
     */
    @JSONField(name = "plantemplinfo")
    @JsonProperty("plantemplinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String plantemplinfo;

    /**
     * 属性 [MPERSONNAME]
     *
     */
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mpersonname;

    /**
     * 属性 [EMPLANTEMPLNAME]
     *
     */
    @JSONField(name = "emplantemplname")
    @JsonProperty("emplantemplname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emplantemplname;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [PREFEE]
     *
     */
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String prefee;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [PLANDESC]
     *
     */
    @JSONField(name = "plandesc")
    @JsonProperty("plandesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String plandesc;

    /**
     * 属性 [MPERSONID]
     *
     */
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mpersonid;

    /**
     * 属性 [PLANTYPE]
     *
     */
    @JSONField(name = "plantype")
    @JsonProperty("plantype")
    @NotBlank(message = "[计划类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String plantype;

    /**
     * 属性 [MDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "mdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    private Timestamp mdate;

    /**
     * 属性 [RECVPERSONID]
     *
     */
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recvpersonid;

    /**
     * 属性 [EQSTOPLENGTH]
     *
     */
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;

    /**
     * 属性 [EMPLANTEMPLID]
     *
     */
    @JSONField(name = "emplantemplid")
    @JsonProperty("emplantemplid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emplantemplid;

    /**
     * 属性 [MTFLAG]
     *
     */
    @JSONField(name = "mtflag")
    @JsonProperty("mtflag")
    private Integer mtflag;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [EMWOTYPE]
     *
     */
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emwotype;

    /**
     * 属性 [PLANCVL]
     *
     */
    @JSONField(name = "plancvl")
    @JsonProperty("plancvl")
    private Double plancvl;

    /**
     * 属性 [ARCHIVE]
     *
     */
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String archive;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [RDEPTNAME]
     *
     */
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rdeptname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [RDEPTID]
     *
     */
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rdeptid;

    /**
     * 属性 [RECVPERSONNAME]
     *
     */
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recvpersonname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PLANSTATE]
     *
     */
    @JSONField(name = "planstate")
    @JsonProperty("planstate")
    @NotNull(message = "[计划状态]不允许为空!")
    private Integer planstate;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rservicename;

    /**
     * 属性 [RTEAMNAME]
     *
     */
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rteamname;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String acclassname;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String acclassid;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rserviceid;

    /**
     * 属性 [RTEAMID]
     *
     */
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rteamid;


    /**
     * 设置 [MPERSONNAME]
     */
    public void setMpersonname(String  mpersonname){
        this.mpersonname = mpersonname ;
        this.modify("mpersonname",mpersonname);
    }

    /**
     * 设置 [EMPLANTEMPLNAME]
     */
    public void setEmplantemplname(String  emplantemplname){
        this.emplantemplname = emplantemplname ;
        this.modify("emplantemplname",emplantemplname);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [PREFEE]
     */
    public void setPrefee(String  prefee){
        this.prefee = prefee ;
        this.modify("prefee",prefee);
    }

    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [PLANDESC]
     */
    public void setPlandesc(String  plandesc){
        this.plandesc = plandesc ;
        this.modify("plandesc",plandesc);
    }

    /**
     * 设置 [MPERSONID]
     */
    public void setMpersonid(String  mpersonid){
        this.mpersonid = mpersonid ;
        this.modify("mpersonid",mpersonid);
    }

    /**
     * 设置 [PLANTYPE]
     */
    public void setPlantype(String  plantype){
        this.plantype = plantype ;
        this.modify("plantype",plantype);
    }

    /**
     * 设置 [MDATE]
     */
    public void setMdate(Timestamp  mdate){
        this.mdate = mdate ;
        this.modify("mdate",mdate);
    }

    /**
     * 设置 [RECVPERSONID]
     */
    public void setRecvpersonid(String  recvpersonid){
        this.recvpersonid = recvpersonid ;
        this.modify("recvpersonid",recvpersonid);
    }

    /**
     * 设置 [EQSTOPLENGTH]
     */
    public void setEqstoplength(Double  eqstoplength){
        this.eqstoplength = eqstoplength ;
        this.modify("eqstoplength",eqstoplength);
    }

    /**
     * 设置 [MTFLAG]
     */
    public void setMtflag(Integer  mtflag){
        this.mtflag = mtflag ;
        this.modify("mtflag",mtflag);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [EMWOTYPE]
     */
    public void setEmwotype(String  emwotype){
        this.emwotype = emwotype ;
        this.modify("emwotype",emwotype);
    }

    /**
     * 设置 [PLANCVL]
     */
    public void setPlancvl(Double  plancvl){
        this.plancvl = plancvl ;
        this.modify("plancvl",plancvl);
    }

    /**
     * 设置 [ARCHIVE]
     */
    public void setArchive(String  archive){
        this.archive = archive ;
        this.modify("archive",archive);
    }

    /**
     * 设置 [RDEPTNAME]
     */
    public void setRdeptname(String  rdeptname){
        this.rdeptname = rdeptname ;
        this.modify("rdeptname",rdeptname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [RDEPTID]
     */
    public void setRdeptid(String  rdeptid){
        this.rdeptid = rdeptid ;
        this.modify("rdeptid",rdeptid);
    }

    /**
     * 设置 [RECVPERSONNAME]
     */
    public void setRecvpersonname(String  recvpersonname){
        this.recvpersonname = recvpersonname ;
        this.modify("recvpersonname",recvpersonname);
    }

    /**
     * 设置 [PLANSTATE]
     */
    public void setPlanstate(Integer  planstate){
        this.planstate = planstate ;
        this.modify("planstate",planstate);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [RTEAMID]
     */
    public void setRteamid(String  rteamid){
        this.rteamid = rteamid ;
        this.modify("rteamid",rteamid);
    }


}


