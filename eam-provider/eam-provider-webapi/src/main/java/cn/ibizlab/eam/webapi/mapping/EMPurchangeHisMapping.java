package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPurchangeHis;
import cn.ibizlab.eam.webapi.dto.EMPurchangeHisDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMPurchangeHisMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMPurchangeHisMapping extends MappingBase<EMPurchangeHisDTO, EMPurchangeHis> {


}

