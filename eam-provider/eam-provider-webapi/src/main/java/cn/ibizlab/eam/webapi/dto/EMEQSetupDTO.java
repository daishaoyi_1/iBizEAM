package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQSetupDTO]
 */
@Data
public class EMEQSetupDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVEADESC]
     *
     */
    @JSONField(name = "activeadesc")
    @JsonProperty("activeadesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String activeadesc;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMEQSETUPNAME]
     *
     */
    @JSONField(name = "emeqsetupname")
    @JsonProperty("emeqsetupname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqsetupname;

    /**
     * 属性 [REGIONENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "regionenddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionenddate")
    private Timestamp regionenddate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PREFEE]
     *
     */
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    private Double prefee;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [ACTIVEDESC]
     *
     */
    @JSONField(name = "activedesc")
    @JsonProperty("activedesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String activedesc;

    /**
     * 属性 [ACTIVEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activedate" , format="yyyy-MM-dd")
    @JsonProperty("activedate")
    @NotNull(message = "[更换安装日期]不允许为空!")
    private Timestamp activedate;

    /**
     * 属性 [MFEE]
     *
     */
    @JSONField(name = "mfee")
    @JsonProperty("mfee")
    private Double mfee;

    /**
     * 属性 [REGIONBEGINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "regionbegindate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    private Timestamp regionbegindate;

    /**
     * 属性 [PIC5]
     *
     */
    @JSONField(name = "pic5")
    @JsonProperty("pic5")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic5;

    /**
     * 属性 [SFEE]
     *
     */
    @JSONField(name = "sfee")
    @JsonProperty("sfee")
    private Double sfee;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EQSTOPLENGTH]
     *
     */
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ACTIVEBDESC]
     *
     */
    @JSONField(name = "activebdesc")
    @JsonProperty("activebdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String activebdesc;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [PIC2]
     *
     */
    @JSONField(name = "pic2")
    @JsonProperty("pic2")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic2;

    /**
     * 属性 [PIC]
     *
     */
    @JSONField(name = "pic")
    @JsonProperty("pic")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic;

    /**
     * 属性 [PIC3]
     *
     */
    @JSONField(name = "pic3")
    @JsonProperty("pic3")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic3;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMEQSETUPID]
     *
     */
    @JSONField(name = "emeqsetupid")
    @JsonProperty("emeqsetupid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqsetupid;

    /**
     * 属性 [RDEPTNAME]
     *
     */
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rdeptname;

    /**
     * 属性 [PIC1]
     *
     */
    @JSONField(name = "pic1")
    @JsonProperty("pic1")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic1;

    /**
     * 属性 [RDEPTID]
     *
     */
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rdeptid;

    /**
     * 属性 [PIC4]
     *
     */
    @JSONField(name = "pic4")
    @JsonProperty("pic4")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pic4;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [PFEE]
     *
     */
    @JSONField(name = "pfee")
    @JsonProperty("pfee")
    private Double pfee;

    /**
     * 属性 [RTEAMNAME]
     *
     */
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rteamname;

    /**
     * 属性 [RFODENAME]
     *
     */
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfodename;

    /**
     * 属性 [RFOCANAME]
     *
     */
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfocaname;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String acclassname;

    /**
     * 属性 [WONAME]
     *
     */
    @JSONField(name = "woname")
    @JsonProperty("woname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String woname;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rservicename;

    /**
     * 属性 [RFOACNAME]
     *
     */
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfoacname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [RFOMONAME]
     *
     */
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfomoname;

    /**
     * 属性 [EITIRESNAME]
     *
     */
    @JSONField(name = "eitiresname")
    @JsonProperty("eitiresname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eitiresname;

    /**
     * 属性 [EITIRESID]
     *
     */
    @JSONField(name = "eitiresid")
    @JsonProperty("eitiresid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eitiresid;

    /**
     * 属性 [RFODEID]
     *
     */
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfodeid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [RFOCAID]
     *
     */
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfocaid;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String acclassid;

    /**
     * 属性 [RFOMOID]
     *
     */
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfomoid;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rserviceid;

    /**
     * 属性 [RTEAMID]
     *
     */
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rteamid;

    /**
     * 属性 [WOID]
     *
     */
    @JSONField(name = "woid")
    @JsonProperty("woid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String woid;

    /**
     * 属性 [RFOACID]
     *
     */
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfoacid;


    /**
     * 设置 [ACTIVEADESC]
     */
    public void setActiveadesc(String  activeadesc){
        this.activeadesc = activeadesc ;
        this.modify("activeadesc",activeadesc);
    }

    /**
     * 设置 [EMEQSETUPNAME]
     */
    public void setEmeqsetupname(String  emeqsetupname){
        this.emeqsetupname = emeqsetupname ;
        this.modify("emeqsetupname",emeqsetupname);
    }

    /**
     * 设置 [REGIONENDDATE]
     */
    public void setRegionenddate(Timestamp  regionenddate){
        this.regionenddate = regionenddate ;
        this.modify("regionenddate",regionenddate);
    }

    /**
     * 设置 [PREFEE]
     */
    public void setPrefee(Double  prefee){
        this.prefee = prefee ;
        this.modify("prefee",prefee);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [ACTIVEDESC]
     */
    public void setActivedesc(String  activedesc){
        this.activedesc = activedesc ;
        this.modify("activedesc",activedesc);
    }

    /**
     * 设置 [ACTIVEDATE]
     */
    public void setActivedate(Timestamp  activedate){
        this.activedate = activedate ;
        this.modify("activedate",activedate);
    }

    /**
     * 设置 [MFEE]
     */
    public void setMfee(Double  mfee){
        this.mfee = mfee ;
        this.modify("mfee",mfee);
    }

    /**
     * 设置 [REGIONBEGINDATE]
     */
    public void setRegionbegindate(Timestamp  regionbegindate){
        this.regionbegindate = regionbegindate ;
        this.modify("regionbegindate",regionbegindate);
    }

    /**
     * 设置 [PIC5]
     */
    public void setPic5(String  pic5){
        this.pic5 = pic5 ;
        this.modify("pic5",pic5);
    }

    /**
     * 设置 [SFEE]
     */
    public void setSfee(Double  sfee){
        this.sfee = sfee ;
        this.modify("sfee",sfee);
    }

    /**
     * 设置 [EQSTOPLENGTH]
     */
    public void setEqstoplength(Double  eqstoplength){
        this.eqstoplength = eqstoplength ;
        this.modify("eqstoplength",eqstoplength);
    }

    /**
     * 设置 [ACTIVEBDESC]
     */
    public void setActivebdesc(String  activebdesc){
        this.activebdesc = activebdesc ;
        this.modify("activebdesc",activebdesc);
    }

    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [PIC2]
     */
    public void setPic2(String  pic2){
        this.pic2 = pic2 ;
        this.modify("pic2",pic2);
    }

    /**
     * 设置 [PIC]
     */
    public void setPic(String  pic){
        this.pic = pic ;
        this.modify("pic",pic);
    }

    /**
     * 设置 [PIC3]
     */
    public void setPic3(String  pic3){
        this.pic3 = pic3 ;
        this.modify("pic3",pic3);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [RDEPTNAME]
     */
    public void setRdeptname(String  rdeptname){
        this.rdeptname = rdeptname ;
        this.modify("rdeptname",rdeptname);
    }

    /**
     * 设置 [PIC1]
     */
    public void setPic1(String  pic1){
        this.pic1 = pic1 ;
        this.modify("pic1",pic1);
    }

    /**
     * 设置 [RDEPTID]
     */
    public void setRdeptid(String  rdeptid){
        this.rdeptid = rdeptid ;
        this.modify("rdeptid",rdeptid);
    }

    /**
     * 设置 [PIC4]
     */
    public void setPic4(String  pic4){
        this.pic4 = pic4 ;
        this.modify("pic4",pic4);
    }

    /**
     * 设置 [PFEE]
     */
    public void setPfee(Double  pfee){
        this.pfee = pfee ;
        this.modify("pfee",pfee);
    }

    /**
     * 设置 [EITIRESID]
     */
    public void setEitiresid(String  eitiresid){
        this.eitiresid = eitiresid ;
        this.modify("eitiresid",eitiresid);
    }

    /**
     * 设置 [RFODEID]
     */
    public void setRfodeid(String  rfodeid){
        this.rfodeid = rfodeid ;
        this.modify("rfodeid",rfodeid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [RFOCAID]
     */
    public void setRfocaid(String  rfocaid){
        this.rfocaid = rfocaid ;
        this.modify("rfocaid",rfocaid);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [RFOMOID]
     */
    public void setRfomoid(String  rfomoid){
        this.rfomoid = rfomoid ;
        this.modify("rfomoid",rfomoid);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [RTEAMID]
     */
    public void setRteamid(String  rteamid){
        this.rteamid = rteamid ;
        this.modify("rteamid",rteamid);
    }

    /**
     * 设置 [WOID]
     */
    public void setWoid(String  woid){
        this.woid = woid ;
        this.modify("woid",woid);
    }

    /**
     * 设置 [RFOACID]
     */
    public void setRfoacid(String  rfoacid){
        this.rfoacid = rfoacid ;
        this.modify("rfoacid",rfoacid);
    }


}


