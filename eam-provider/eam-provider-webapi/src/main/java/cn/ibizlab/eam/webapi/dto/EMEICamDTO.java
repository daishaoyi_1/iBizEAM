package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEICamDTO]
 */
@Data
public class EMEICamDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMEICAMNAME]
     *
     */
    @JSONField(name = "emeicamname")
    @JsonProperty("emeicamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeicamname;

    /**
     * 属性 [EICAMINFO]
     *
     */
    @JSONField(name = "eicaminfo")
    @JsonProperty("eicaminfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eicaminfo;

    /**
     * 属性 [EQMODELCODE]
     *
     */
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqmodelcode;

    /**
     * 属性 [EMEICAMID]
     *
     */
    @JSONField(name = "emeicamid")
    @JsonProperty("emeicamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeicamid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [DISDESC]
     *
     */
    @JSONField(name = "disdesc")
    @JsonProperty("disdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String disdesc;

    /**
     * 属性 [EISTATE]
     *
     */
    @JSONField(name = "eistate")
    @JsonProperty("eistate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eistate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [MACADDR]
     *
     */
    @JSONField(name = "macaddr")
    @JsonProperty("macaddr")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String macaddr;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [DISDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "disdate" , format="yyyy-MM-dd")
    @JsonProperty("disdate")
    private Timestamp disdate;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ITEMPUSENAME]
     *
     */
    @JSONField(name = "itempusename")
    @JsonProperty("itempusename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itempusename;

    /**
     * 属性 [EQLOCATIONNAME]
     *
     */
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [ITEMPUSEID]
     *
     */
    @JSONField(name = "itempuseid")
    @JsonProperty("itempuseid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itempuseid;

    /**
     * 属性 [EQLOCATIONID]
     *
     */
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;


    /**
     * 设置 [EMEICAMNAME]
     */
    public void setEmeicamname(String  emeicamname){
        this.emeicamname = emeicamname ;
        this.modify("emeicamname",emeicamname);
    }

    /**
     * 设置 [EQMODELCODE]
     */
    public void setEqmodelcode(String  eqmodelcode){
        this.eqmodelcode = eqmodelcode ;
        this.modify("eqmodelcode",eqmodelcode);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [DISDESC]
     */
    public void setDisdesc(String  disdesc){
        this.disdesc = disdesc ;
        this.modify("disdesc",disdesc);
    }

    /**
     * 设置 [EISTATE]
     */
    public void setEistate(String  eistate){
        this.eistate = eistate ;
        this.modify("eistate",eistate);
    }

    /**
     * 设置 [MACADDR]
     */
    public void setMacaddr(String  macaddr){
        this.macaddr = macaddr ;
        this.modify("macaddr",macaddr);
    }

    /**
     * 设置 [DISDATE]
     */
    public void setDisdate(Timestamp  disdate){
        this.disdate = disdate ;
        this.modify("disdate",disdate);
    }

    /**
     * 设置 [EMPNAME]
     */
    public void setEmpname(String  empname){
        this.empname = empname ;
        this.modify("empname",empname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ITEMPUSEID]
     */
    public void setItempuseid(String  itempuseid){
        this.itempuseid = itempuseid ;
        this.modify("itempuseid",itempuseid);
    }

    /**
     * 设置 [EQLOCATIONID]
     */
    public void setEqlocationid(String  eqlocationid){
        this.eqlocationid = eqlocationid ;
        this.modify("eqlocationid",eqlocationid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }


}


