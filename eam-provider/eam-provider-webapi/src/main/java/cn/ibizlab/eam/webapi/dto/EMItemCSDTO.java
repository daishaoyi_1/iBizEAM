package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMItemCSDTO]
 */
@Data
public class EMItemCSDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ITEMROUTINFO]
     *
     */
    @JSONField(name = "itemroutinfo")
    @JsonProperty("itemroutinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemroutinfo;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [SDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    private Timestamp sdate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [EMITEMCSNAME]
     *
     */
    @JSONField(name = "emitemcsname")
    @JsonProperty("emitemcsname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitemcsname;

    /**
     * 属性 [EMITEMCSID]
     *
     */
    @JSONField(name = "emitemcsid")
    @JsonProperty("emitemcsid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitemcsid;

    /**
     * 属性 [BATCODE]
     *
     */
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    @NotBlank(message = "[批次]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String batcode;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [TRADESTATE]
     *
     */
    @JSONField(name = "tradestate")
    @JsonProperty("tradestate")
    private Integer tradestate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [PSUM]
     *
     */
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [STOREPARTNAME]
     *
     */
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storepartname;

    /**
     * 属性 [RNAME]
     *
     */
    @JSONField(name = "rname")
    @JsonProperty("rname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rname;

    /**
     * 属性 [STORENAME]
     *
     */
    @JSONField(name = "storename")
    @JsonProperty("storename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storename;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 400, message = "内容长度必须小于等于[400]")
    private String itemname;

    /**
     * 属性 [STOCKNAME]
     *
     */
    @JSONField(name = "stockname")
    @JsonProperty("stockname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stockname;

    /**
     * 属性 [STOREID]
     *
     */
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storeid;

    /**
     * 属性 [RID]
     *
     */
    @JSONField(name = "rid")
    @JsonProperty("rid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rid;

    /**
     * 属性 [STOREPARTID]
     *
     */
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storepartid;

    /**
     * 属性 [STOCKID]
     *
     */
    @JSONField(name = "stockid")
    @JsonProperty("stockid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String stockid;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [SEMPID]
     *
     */
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sempid;

    /**
     * 属性 [SEMPNAME]
     *
     */
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sempname;


    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [SDATE]
     */
    public void setSdate(Timestamp  sdate){
        this.sdate = sdate ;
        this.modify("sdate",sdate);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [EMITEMCSNAME]
     */
    public void setEmitemcsname(String  emitemcsname){
        this.emitemcsname = emitemcsname ;
        this.modify("emitemcsname",emitemcsname);
    }

    /**
     * 设置 [BATCODE]
     */
    public void setBatcode(String  batcode){
        this.batcode = batcode ;
        this.modify("batcode",batcode);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [TRADESTATE]
     */
    public void setTradestate(Integer  tradestate){
        this.tradestate = tradestate ;
        this.modify("tradestate",tradestate);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [PSUM]
     */
    public void setPsum(Double  psum){
        this.psum = psum ;
        this.modify("psum",psum);
    }

    /**
     * 设置 [STOREID]
     */
    public void setStoreid(String  storeid){
        this.storeid = storeid ;
        this.modify("storeid",storeid);
    }

    /**
     * 设置 [RID]
     */
    public void setRid(String  rid){
        this.rid = rid ;
        this.modify("rid",rid);
    }

    /**
     * 设置 [STOREPARTID]
     */
    public void setStorepartid(String  storepartid){
        this.storepartid = storepartid ;
        this.modify("storepartid",storepartid);
    }

    /**
     * 设置 [STOCKID]
     */
    public void setStockid(String  stockid){
        this.stockid = stockid ;
        this.modify("stockid",stockid);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [SEMPID]
     */
    public void setSempid(String  sempid){
        this.sempid = sempid ;
        this.modify("sempid",sempid);
    }


}


