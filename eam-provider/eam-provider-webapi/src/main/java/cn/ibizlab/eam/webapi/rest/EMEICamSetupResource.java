package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICamSetup;
import cn.ibizlab.eam.core.eam_core.service.IEMEICamSetupService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamSetupSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"探头安装记录" })
@RestController("WebApi-emeicamsetup")
@RequestMapping("")
public class EMEICamSetupResource {

    @Autowired
    public IEMEICamSetupService emeicamsetupService;

    @Autowired
    @Lazy
    public EMEICamSetupMapping emeicamsetupMapping;

    @PreAuthorize("hasPermission(this.emeicamsetupMapping.toDomain(#emeicamsetupdto),'eam-EMEICamSetup-Create')")
    @ApiOperation(value = "新建探头安装记录", tags = {"探头安装记录" },  notes = "新建探头安装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamsetups")
    public ResponseEntity<EMEICamSetupDTO> create(@Validated @RequestBody EMEICamSetupDTO emeicamsetupdto) {
        EMEICamSetup domain = emeicamsetupMapping.toDomain(emeicamsetupdto);
		emeicamsetupService.create(domain);
        EMEICamSetupDTO dto = emeicamsetupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamsetupMapping.toDomain(#emeicamsetupdtos),'eam-EMEICamSetup-Create')")
    @ApiOperation(value = "批量新建探头安装记录", tags = {"探头安装记录" },  notes = "批量新建探头安装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamsetups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICamSetupDTO> emeicamsetupdtos) {
        emeicamsetupService.createBatch(emeicamsetupMapping.toDomain(emeicamsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicamsetup" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicamsetupService.get(#emeicamsetup_id),'eam-EMEICamSetup-Update')")
    @ApiOperation(value = "更新探头安装记录", tags = {"探头安装记录" },  notes = "更新探头安装记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicamsetups/{emeicamsetup_id}")
    public ResponseEntity<EMEICamSetupDTO> update(@PathVariable("emeicamsetup_id") String emeicamsetup_id, @RequestBody EMEICamSetupDTO emeicamsetupdto) {
		EMEICamSetup domain  = emeicamsetupMapping.toDomain(emeicamsetupdto);
        domain .setEmeicamsetupid(emeicamsetup_id);
		emeicamsetupService.update(domain );
		EMEICamSetupDTO dto = emeicamsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamsetupService.getEmeicamsetupByEntities(this.emeicamsetupMapping.toDomain(#emeicamsetupdtos)),'eam-EMEICamSetup-Update')")
    @ApiOperation(value = "批量更新探头安装记录", tags = {"探头安装记录" },  notes = "批量更新探头安装记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicamsetups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICamSetupDTO> emeicamsetupdtos) {
        emeicamsetupService.updateBatch(emeicamsetupMapping.toDomain(emeicamsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicamsetupService.get(#emeicamsetup_id),'eam-EMEICamSetup-Remove')")
    @ApiOperation(value = "删除探头安装记录", tags = {"探头安装记录" },  notes = "删除探头安装记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicamsetups/{emeicamsetup_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicamsetup_id") String emeicamsetup_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicamsetupService.remove(emeicamsetup_id));
    }

    @PreAuthorize("hasPermission(this.emeicamsetupService.getEmeicamsetupByIds(#ids),'eam-EMEICamSetup-Remove')")
    @ApiOperation(value = "批量删除探头安装记录", tags = {"探头安装记录" },  notes = "批量删除探头安装记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicamsetups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicamsetupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicamsetupMapping.toDomain(returnObject.body),'eam-EMEICamSetup-Get')")
    @ApiOperation(value = "获取探头安装记录", tags = {"探头安装记录" },  notes = "获取探头安装记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicamsetups/{emeicamsetup_id}")
    public ResponseEntity<EMEICamSetupDTO> get(@PathVariable("emeicamsetup_id") String emeicamsetup_id) {
        EMEICamSetup domain = emeicamsetupService.get(emeicamsetup_id);
        EMEICamSetupDTO dto = emeicamsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取探头安装记录草稿", tags = {"探头安装记录" },  notes = "获取探头安装记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicamsetups/getdraft")
    public ResponseEntity<EMEICamSetupDTO> getDraft(EMEICamSetupDTO dto) {
        EMEICamSetup domain = emeicamsetupMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeicamsetupMapping.toDto(emeicamsetupService.getDraft(domain)));
    }

    @ApiOperation(value = "检查探头安装记录", tags = {"探头安装记录" },  notes = "检查探头安装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamsetups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICamSetupDTO emeicamsetupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicamsetupService.checkKey(emeicamsetupMapping.toDomain(emeicamsetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeicamsetupMapping.toDomain(#emeicamsetupdto),'eam-EMEICamSetup-Save')")
    @ApiOperation(value = "保存探头安装记录", tags = {"探头安装记录" },  notes = "保存探头安装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamsetups/save")
    public ResponseEntity<EMEICamSetupDTO> save(@RequestBody EMEICamSetupDTO emeicamsetupdto) {
        EMEICamSetup domain = emeicamsetupMapping.toDomain(emeicamsetupdto);
        emeicamsetupService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeicamsetupMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeicamsetupMapping.toDomain(#emeicamsetupdtos),'eam-EMEICamSetup-Save')")
    @ApiOperation(value = "批量保存探头安装记录", tags = {"探头安装记录" },  notes = "批量保存探头安装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamsetups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICamSetupDTO> emeicamsetupdtos) {
        emeicamsetupService.saveBatch(emeicamsetupMapping.toDomain(emeicamsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICamSetup-searchDefault-all') and hasPermission(#context,'eam-EMEICamSetup-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"探头安装记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicamsetups/fetchdefault")
	public ResponseEntity<List<EMEICamSetupDTO>> fetchDefault(EMEICamSetupSearchContext context) {
        Page<EMEICamSetup> domains = emeicamsetupService.searchDefault(context) ;
        List<EMEICamSetupDTO> list = emeicamsetupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICamSetup-searchDefault-all') and hasPermission(#context,'eam-EMEICamSetup-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"探头安装记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicamsetups/searchdefault")
	public ResponseEntity<Page<EMEICamSetupDTO>> searchDefault(@RequestBody EMEICamSetupSearchContext context) {
        Page<EMEICamSetup> domains = emeicamsetupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicamsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

