package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMRiggingDTO]
 */
@Data
public class EMRiggingDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String description;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ORGANIZATION]
     *
     */
    @JSONField(name = "organization")
    @JsonProperty("organization")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String organization;

    /**
     * 属性 [EMRIGGINGNAME]
     *
     */
    @JSONField(name = "emriggingname")
    @JsonProperty("emriggingname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emriggingname;

    /**
     * 属性 [EMRIGGINGID]
     *
     */
    @JSONField(name = "emriggingid")
    @JsonProperty("emriggingid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emriggingid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * 属性 [EMITEMPUSENAME]
     *
     */
    @JSONField(name = "emitempusename")
    @JsonProperty("emitempusename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitempusename;

    /**
     * 属性 [EMITEMPUSEID]
     *
     */
    @JSONField(name = "emitempuseid")
    @JsonProperty("emitempuseid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitempuseid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ORGANIZATION]
     */
    public void setOrganization(String  organization){
        this.organization = organization ;
        this.modify("organization",organization);
    }

    /**
     * 设置 [EMRIGGINGNAME]
     */
    public void setEmriggingname(String  emriggingname){
        this.emriggingname = emriggingname ;
        this.modify("emriggingname",emriggingname);
    }

    /**
     * 设置 [EMITEMPUSEID]
     */
    public void setEmitempuseid(String  emitempuseid){
        this.emitempuseid = emitempuseid ;
        this.modify("emitempuseid",emitempuseid);
    }


}


