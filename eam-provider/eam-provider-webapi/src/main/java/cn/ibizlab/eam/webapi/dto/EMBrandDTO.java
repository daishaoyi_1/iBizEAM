package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMBrandDTO]
 */
@Data
public class EMBrandDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMBRANDID]
     *
     */
    @JSONField(name = "embrandid")
    @JsonProperty("embrandid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String embrandid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMBRANDNAME]
     *
     */
    @JSONField(name = "embrandname")
    @JsonProperty("embrandname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embrandname;

    /**
     * 属性 [EMBRANDCODE]
     *
     */
    @JSONField(name = "embrandcode")
    @JsonProperty("embrandcode")
    @NotBlank(message = "[品牌编码]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embrandcode;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [MACHTYPECODE]
     *
     */
    @JSONField(name = "machtypecode")
    @JsonProperty("machtypecode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String machtypecode;


    /**
     * 设置 [EMBRANDNAME]
     */
    public void setEmbrandname(String  embrandname){
        this.embrandname = embrandname ;
        this.modify("embrandname",embrandname);
    }

    /**
     * 设置 [EMBRANDCODE]
     */
    public void setEmbrandcode(String  embrandcode){
        this.embrandcode = embrandcode ;
        this.modify("embrandcode",embrandcode);
    }

    /**
     * 设置 [MACHTYPECODE]
     */
    public void setMachtypecode(String  machtypecode){
        this.machtypecode = machtypecode ;
        this.modify("machtypecode",machtypecode);
    }


}


