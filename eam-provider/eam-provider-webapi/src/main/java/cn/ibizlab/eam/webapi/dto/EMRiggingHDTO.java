package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMRiggingHDTO]
 */
@Data
public class EMRiggingHDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMRIGGINGHNAME]
     *
     */
    @JSONField(name = "emrigginghname")
    @JsonProperty("emrigginghname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emrigginghname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMRIGGINGHID]
     *
     */
    @JSONField(name = "emrigginghid")
    @JsonProperty("emrigginghid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emrigginghid;

    /**
     * 属性 [IODATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "iodate" , format="yyyy-MM-dd")
    @JsonProperty("iodate")
    private Timestamp iodate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [NUMB]
     *
     */
    @JSONField(name = "numb")
    @JsonProperty("numb")
    @NotNull(message = "[数量]不允许为空!")
    private Integer numb;

    /**
     * 属性 [IOSTATE]
     *
     */
    @JSONField(name = "iostate")
    @JsonProperty("iostate")
    @NotBlank(message = "[出入库状态]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String iostate;

    /**
     * 属性 [EMRIGGINGNAME]
     *
     */
    @JSONField(name = "emriggingname")
    @JsonProperty("emriggingname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emriggingname;

    /**
     * 属性 [EMITEMPUSENAME]
     *
     */
    @JSONField(name = "emitempusename")
    @JsonProperty("emitempusename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitempusename;

    /**
     * 属性 [EMRIGGINGID]
     *
     */
    @JSONField(name = "emriggingid")
    @JsonProperty("emriggingid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emriggingid;

    /**
     * 属性 [EMITEMPUSEID]
     *
     */
    @JSONField(name = "emitempuseid")
    @JsonProperty("emitempuseid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitempuseid;


    /**
     * 设置 [EMRIGGINGHNAME]
     */
    public void setEmrigginghname(String  emrigginghname){
        this.emrigginghname = emrigginghname ;
        this.modify("emrigginghname",emrigginghname);
    }

    /**
     * 设置 [IODATE]
     */
    public void setIodate(Timestamp  iodate){
        this.iodate = iodate ;
        this.modify("iodate",iodate);
    }

    /**
     * 设置 [NUMB]
     */
    public void setNumb(Integer  numb){
        this.numb = numb ;
        this.modify("numb",numb);
    }

    /**
     * 设置 [IOSTATE]
     */
    public void setIostate(String  iostate){
        this.iostate = iostate ;
        this.modify("iostate",iostate);
    }

    /**
     * 设置 [EMRIGGINGID]
     */
    public void setEmriggingid(String  emriggingid){
        this.emriggingid = emriggingid ;
        this.modify("emriggingid",emriggingid);
    }

    /**
     * 设置 [EMITEMPUSEID]
     */
    public void setEmitempuseid(String  emitempuseid){
        this.emitempuseid = emitempuseid ;
        this.modify("emitempuseid",emitempuseid);
    }


}


