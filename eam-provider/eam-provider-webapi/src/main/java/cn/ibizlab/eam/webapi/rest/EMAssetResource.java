package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAsset;
import cn.ibizlab.eam.core.eam_core.service.IEMAssetService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资产" })
@RestController("WebApi-emasset")
@RequestMapping("")
public class EMAssetResource {

    @Autowired
    public IEMAssetService emassetService;

    @Autowired
    @Lazy
    public EMAssetMapping emassetMapping;

    @PreAuthorize("hasPermission(this.emassetMapping.toDomain(#emassetdto),'eam-EMAsset-Create')")
    @ApiOperation(value = "新建资产", tags = {"资产" },  notes = "新建资产")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets")
    public ResponseEntity<EMAssetDTO> create(@Validated @RequestBody EMAssetDTO emassetdto) {
        EMAsset domain = emassetMapping.toDomain(emassetdto);
		emassetService.create(domain);
        EMAssetDTO dto = emassetMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetMapping.toDomain(#emassetdtos),'eam-EMAsset-Create')")
    @ApiOperation(value = "批量新建资产", tags = {"资产" },  notes = "批量新建资产")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssetDTO> emassetdtos) {
        emassetService.createBatch(emassetMapping.toDomain(emassetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emasset" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassetService.get(#emasset_id),'eam-EMAsset-Update')")
    @ApiOperation(value = "更新资产", tags = {"资产" },  notes = "更新资产")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassets/{emasset_id}")
    public ResponseEntity<EMAssetDTO> update(@PathVariable("emasset_id") String emasset_id, @RequestBody EMAssetDTO emassetdto) {
		EMAsset domain  = emassetMapping.toDomain(emassetdto);
        domain .setEmassetid(emasset_id);
		emassetService.update(domain );
		EMAssetDTO dto = emassetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetService.getEmassetByEntities(this.emassetMapping.toDomain(#emassetdtos)),'eam-EMAsset-Update')")
    @ApiOperation(value = "批量更新资产", tags = {"资产" },  notes = "批量更新资产")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssetDTO> emassetdtos) {
        emassetService.updateBatch(emassetMapping.toDomain(emassetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassetService.get(#emasset_id),'eam-EMAsset-Remove')")
    @ApiOperation(value = "删除资产", tags = {"资产" },  notes = "删除资产")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassets/{emasset_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emasset_id") String emasset_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassetService.remove(emasset_id));
    }

    @PreAuthorize("hasPermission(this.emassetService.getEmassetByIds(#ids),'eam-EMAsset-Remove')")
    @ApiOperation(value = "批量删除资产", tags = {"资产" },  notes = "批量删除资产")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassetService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassetMapping.toDomain(returnObject.body),'eam-EMAsset-Get')")
    @ApiOperation(value = "获取资产", tags = {"资产" },  notes = "获取资产")
	@RequestMapping(method = RequestMethod.GET, value = "/emassets/{emasset_id}")
    public ResponseEntity<EMAssetDTO> get(@PathVariable("emasset_id") String emasset_id) {
        EMAsset domain = emassetService.get(emasset_id);
        EMAssetDTO dto = emassetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资产草稿", tags = {"资产" },  notes = "获取资产草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassets/getdraft")
    public ResponseEntity<EMAssetDTO> getDraft(EMAssetDTO dto) {
        EMAsset domain = emassetMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emassetMapping.toDto(emassetService.getDraft(domain)));
    }

    @ApiOperation(value = "检查资产", tags = {"资产" },  notes = "检查资产")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssetDTO emassetdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassetService.checkKey(emassetMapping.toDomain(emassetdto)));
    }

    @PreAuthorize("hasPermission(this.emassetMapping.toDomain(#emassetdto),'eam-EMAsset-Save')")
    @ApiOperation(value = "保存资产", tags = {"资产" },  notes = "保存资产")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/save")
    public ResponseEntity<EMAssetDTO> save(@RequestBody EMAssetDTO emassetdto) {
        EMAsset domain = emassetMapping.toDomain(emassetdto);
        emassetService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassetMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassetMapping.toDomain(#emassetdtos),'eam-EMAsset-Save')")
    @ApiOperation(value = "批量保存资产", tags = {"资产" },  notes = "批量保存资产")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssetDTO> emassetdtos) {
        emassetService.saveBatch(emassetMapping.toDomain(emassetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAsset-searchAssetBf-all') and hasPermission(#context,'eam-EMAsset-Get')")
	@ApiOperation(value = "获取报废资产", tags = {"资产" } ,notes = "获取报废资产")
    @RequestMapping(method= RequestMethod.GET , value="/emassets/fetchassetbf")
	public ResponseEntity<List<EMAssetDTO>> fetchAssetBf(EMAssetSearchContext context) {
        Page<EMAsset> domains = emassetService.searchAssetBf(context) ;
        List<EMAssetDTO> list = emassetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAsset-searchAssetBf-all') and hasPermission(#context,'eam-EMAsset-Get')")
	@ApiOperation(value = "查询报废资产", tags = {"资产" } ,notes = "查询报废资产")
    @RequestMapping(method= RequestMethod.POST , value="/emassets/searchassetbf")
	public ResponseEntity<Page<EMAssetDTO>> searchAssetBf(@RequestBody EMAssetSearchContext context) {
        Page<EMAsset> domains = emassetService.searchAssetBf(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAsset-searchDefault-all') and hasPermission(#context,'eam-EMAsset-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"资产" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassets/fetchdefault")
	public ResponseEntity<List<EMAssetDTO>> fetchDefault(EMAssetSearchContext context) {
        Page<EMAsset> domains = emassetService.searchDefault(context) ;
        List<EMAssetDTO> list = emassetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAsset-searchDefault-all') and hasPermission(#context,'eam-EMAsset-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"资产" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassets/searchdefault")
	public ResponseEntity<Page<EMAssetDTO>> searchDefault(@RequestBody EMAssetSearchContext context) {
        Page<EMAsset> domains = emassetService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

