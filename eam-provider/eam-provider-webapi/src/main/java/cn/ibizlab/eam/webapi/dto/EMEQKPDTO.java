package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQKPDTO]
 */
@Data
public class EMEQKPDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMEQKPID]
     *
     */
    @JSONField(name = "emeqkpid")
    @JsonProperty("emeqkpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqkpid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [KPSCOPE]
     *
     */
    @JSONField(name = "kpscope")
    @JsonProperty("kpscope")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kpscope;

    /**
     * 属性 [KPDESC]
     *
     */
    @JSONField(name = "kpdesc")
    @JsonProperty("kpdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kpdesc;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [NORMALREFVAL]
     *
     */
    @JSONField(name = "normalrefval")
    @JsonProperty("normalrefval")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String normalrefval;

    /**
     * 属性 [KPINFO]
     *
     */
    @JSONField(name = "kpinfo")
    @JsonProperty("kpinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kpinfo;

    /**
     * 属性 [KPTYPEID]
     *
     */
    @JSONField(name = "kptypeid")
    @JsonProperty("kptypeid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kptypeid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [KPCODE]
     *
     */
    @JSONField(name = "kpcode")
    @JsonProperty("kpcode")
    @NotBlank(message = "[关键点代码]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kpcode;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMEQKPNAME]
     *
     */
    @JSONField(name = "emeqkpname")
    @JsonProperty("emeqkpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqkpname;


    /**
     * 设置 [KPSCOPE]
     */
    public void setKpscope(String  kpscope){
        this.kpscope = kpscope ;
        this.modify("kpscope",kpscope);
    }

    /**
     * 设置 [KPDESC]
     */
    public void setKpdesc(String  kpdesc){
        this.kpdesc = kpdesc ;
        this.modify("kpdesc",kpdesc);
    }

    /**
     * 设置 [NORMALREFVAL]
     */
    public void setNormalrefval(String  normalrefval){
        this.normalrefval = normalrefval ;
        this.modify("normalrefval",normalrefval);
    }

    /**
     * 设置 [KPTYPEID]
     */
    public void setKptypeid(String  kptypeid){
        this.kptypeid = kptypeid ;
        this.modify("kptypeid",kptypeid);
    }

    /**
     * 设置 [KPCODE]
     */
    public void setKpcode(String  kpcode){
        this.kpcode = kpcode ;
        this.modify("kpcode",kpcode);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMEQKPNAME]
     */
    public void setEmeqkpname(String  emeqkpname){
        this.emeqkpname = emeqkpname ;
        this.modify("emeqkpname",emeqkpname);
    }


}


