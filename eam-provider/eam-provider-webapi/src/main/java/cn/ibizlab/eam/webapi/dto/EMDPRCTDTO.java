package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMDPRCTDTO]
 */
@Data
public class EMDPRCTDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMDPRCTTYPE]
     *
     */
    @JSONField(name = "emdprcttype")
    @JsonProperty("emdprcttype")
    @NotBlank(message = "[测点记录类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emdprcttype;

    /**
     * 属性 [EMDPRCTNAME]
     *
     */
    @JSONField(name = "emdprctname")
    @JsonProperty("emdprctname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emdprctname;

    /**
     * 属性 [BDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    private Timestamp bdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "edate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    private Timestamp edate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [VAL]
     *
     */
    @JSONField(name = "val")
    @JsonProperty("val")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String val;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [NVAL]
     *
     */
    @JSONField(name = "nval")
    @JsonProperty("nval")
    private Double nval;

    /**
     * 属性 [EMDPRCTID]
     *
     */
    @JSONField(name = "emdprctid")
    @JsonProperty("emdprctid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emdprctid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [WONAME]
     *
     */
    @JSONField(name = "woname")
    @JsonProperty("woname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String woname;

    /**
     * 属性 [DPNAME]
     *
     */
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String dpname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [WOID]
     *
     */
    @JSONField(name = "woid")
    @JsonProperty("woid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String woid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [DPID]
     *
     */
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dpid;


    /**
     * 设置 [EMDPRCTTYPE]
     */
    public void setEmdprcttype(String  emdprcttype){
        this.emdprcttype = emdprcttype ;
        this.modify("emdprcttype",emdprcttype);
    }

    /**
     * 设置 [EMDPRCTNAME]
     */
    public void setEmdprctname(String  emdprctname){
        this.emdprctname = emdprctname ;
        this.modify("emdprctname",emdprctname);
    }

    /**
     * 设置 [BDATE]
     */
    public void setBdate(Timestamp  bdate){
        this.bdate = bdate ;
        this.modify("bdate",bdate);
    }

    /**
     * 设置 [EDATE]
     */
    public void setEdate(Timestamp  edate){
        this.edate = edate ;
        this.modify("edate",edate);
    }

    /**
     * 设置 [VAL]
     */
    public void setVal(String  val){
        this.val = val ;
        this.modify("val",val);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [NVAL]
     */
    public void setNval(Double  nval){
        this.nval = nval ;
        this.modify("nval",nval);
    }

    /**
     * 设置 [WOID]
     */
    public void setWoid(String  woid){
        this.woid = woid ;
        this.modify("woid",woid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [DPID]
     */
    public void setDpid(String  dpid){
        this.dpid = dpid ;
        this.modify("dpid",dpid);
    }


}


