package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPlanDetailDTO]
 */
@Data
public class EMPlanDetailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMPLANDETAILNAME]
     *
     */
    @JSONField(name = "emplandetailname")
    @JsonProperty("emplandetailname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emplandetailname;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [RECVPERSONNAME]
     *
     */
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recvpersonname;

    /**
     * 属性 [ORDERFLAG]
     *
     */
    @JSONField(name = "orderflag")
    @JsonProperty("orderflag")
    private Integer orderflag;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EMPLANDETAILID]
     *
     */
    @JSONField(name = "emplandetailid")
    @JsonProperty("emplandetailid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emplandetailid;

    /**
     * 属性 [RECVPERSONID]
     *
     */
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recvpersonid;

    /**
     * 属性 [ARCHIVE]
     *
     */
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String archive;

    /**
     * 属性 [EMWOTYPE]
     *
     */
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emwotype;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [EQSTOPLENGTH]
     *
     */
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;

    /**
     * 属性 [PLANDETAILDESC]
     *
     */
    @JSONField(name = "plandetaildesc")
    @JsonProperty("plandetaildesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String plandetaildesc;

    /**
     * 属性 [PLANNAME]
     *
     */
    @JSONField(name = "planname")
    @JsonProperty("planname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String planname;

    /**
     * 属性 [DPTYPE]
     *
     */
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    @NotBlank(message = "[测点类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dptype;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [DPNAME]
     *
     */
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String dpname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [DPID]
     *
     */
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dpid;

    /**
     * 属性 [PLANID]
     *
     */
    @JSONField(name = "planid")
    @JsonProperty("planid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String planid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMPLANDETAILNAME]
     */
    public void setEmplandetailname(String  emplandetailname){
        this.emplandetailname = emplandetailname ;
        this.modify("emplandetailname",emplandetailname);
    }

    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [RECVPERSONNAME]
     */
    public void setRecvpersonname(String  recvpersonname){
        this.recvpersonname = recvpersonname ;
        this.modify("recvpersonname",recvpersonname);
    }

    /**
     * 设置 [ORDERFLAG]
     */
    public void setOrderflag(Integer  orderflag){
        this.orderflag = orderflag ;
        this.modify("orderflag",orderflag);
    }

    /**
     * 设置 [RECVPERSONID]
     */
    public void setRecvpersonid(String  recvpersonid){
        this.recvpersonid = recvpersonid ;
        this.modify("recvpersonid",recvpersonid);
    }

    /**
     * 设置 [ARCHIVE]
     */
    public void setArchive(String  archive){
        this.archive = archive ;
        this.modify("archive",archive);
    }

    /**
     * 设置 [EMWOTYPE]
     */
    public void setEmwotype(String  emwotype){
        this.emwotype = emwotype ;
        this.modify("emwotype",emwotype);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [EQSTOPLENGTH]
     */
    public void setEqstoplength(Double  eqstoplength){
        this.eqstoplength = eqstoplength ;
        this.modify("eqstoplength",eqstoplength);
    }

    /**
     * 设置 [PLANDETAILDESC]
     */
    public void setPlandetaildesc(String  plandetaildesc){
        this.plandetaildesc = plandetaildesc ;
        this.modify("plandetaildesc",plandetaildesc);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [DPID]
     */
    public void setDpid(String  dpid){
        this.dpid = dpid ;
        this.modify("dpid",dpid);
    }

    /**
     * 设置 [PLANID]
     */
    public void setPlanid(String  planid){
        this.planid = planid ;
        this.modify("planid",planid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }


}


