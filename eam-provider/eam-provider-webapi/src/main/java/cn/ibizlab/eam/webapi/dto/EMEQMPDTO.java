package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQMPDTO]
 */
@Data
public class EMEQMPDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [MPCODE]
     *
     */
    @JSONField(name = "mpcode")
    @JsonProperty("mpcode")
    @NotBlank(message = "[设备仪表代码]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mpcode;

    /**
     * 属性 [EMEQMPNAME]
     *
     */
    @JSONField(name = "emeqmpname")
    @JsonProperty("emeqmpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqmpname;

    /**
     * 属性 [MPDESC]
     *
     */
    @JSONField(name = "mpdesc")
    @JsonProperty("mpdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mpdesc;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [MPINFO]
     *
     */
    @JSONField(name = "mpinfo")
    @JsonProperty("mpinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mpinfo;

    /**
     * 属性 [EMEQMPID]
     *
     */
    @JSONField(name = "emeqmpid")
    @JsonProperty("emeqmpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqmpid;

    /**
     * 属性 [NORMALREFVAL]
     *
     */
    @JSONField(name = "normalrefval")
    @JsonProperty("normalrefval")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String normalrefval;

    /**
     * 属性 [MPTYPEID]
     *
     */
    @JSONField(name = "mptypeid")
    @JsonProperty("mptypeid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mptypeid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [MPSCOPE]
     *
     */
    @JSONField(name = "mpscope")
    @JsonProperty("mpscope")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mpscope;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;


    /**
     * 设置 [MPCODE]
     */
    public void setMpcode(String  mpcode){
        this.mpcode = mpcode ;
        this.modify("mpcode",mpcode);
    }

    /**
     * 设置 [EMEQMPNAME]
     */
    public void setEmeqmpname(String  emeqmpname){
        this.emeqmpname = emeqmpname ;
        this.modify("emeqmpname",emeqmpname);
    }

    /**
     * 设置 [MPDESC]
     */
    public void setMpdesc(String  mpdesc){
        this.mpdesc = mpdesc ;
        this.modify("mpdesc",mpdesc);
    }

    /**
     * 设置 [NORMALREFVAL]
     */
    public void setNormalrefval(String  normalrefval){
        this.normalrefval = normalrefval ;
        this.modify("normalrefval",normalrefval);
    }

    /**
     * 设置 [MPTYPEID]
     */
    public void setMptypeid(String  mptypeid){
        this.mptypeid = mptypeid ;
        this.modify("mptypeid",mptypeid);
    }

    /**
     * 设置 [MPSCOPE]
     */
    public void setMpscope(String  mpscope){
        this.mpscope = mpscope ;
        this.modify("mpscope",mpscope);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }


}


