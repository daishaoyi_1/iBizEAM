package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRiggingH;
import cn.ibizlab.eam.core.eam_core.service.IEMRiggingHService;
import cn.ibizlab.eam.core.eam_core.filter.EMRiggingHSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工具库出入记录" })
@RestController("WebApi-emriggingh")
@RequestMapping("")
public class EMRiggingHResource {

    @Autowired
    public IEMRiggingHService emrigginghService;

    @Autowired
    @Lazy
    public EMRiggingHMapping emrigginghMapping;

    @PreAuthorize("hasPermission(this.emrigginghMapping.toDomain(#emrigginghdto),'eam-EMRiggingH-Create')")
    @ApiOperation(value = "新建工具库出入记录", tags = {"工具库出入记录" },  notes = "新建工具库出入记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emrigginghs")
    public ResponseEntity<EMRiggingHDTO> create(@Validated @RequestBody EMRiggingHDTO emrigginghdto) {
        EMRiggingH domain = emrigginghMapping.toDomain(emrigginghdto);
		emrigginghService.create(domain);
        EMRiggingHDTO dto = emrigginghMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrigginghMapping.toDomain(#emrigginghdtos),'eam-EMRiggingH-Create')")
    @ApiOperation(value = "批量新建工具库出入记录", tags = {"工具库出入记录" },  notes = "批量新建工具库出入记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emrigginghs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRiggingHDTO> emrigginghdtos) {
        emrigginghService.createBatch(emrigginghMapping.toDomain(emrigginghdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emriggingh" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrigginghService.get(#emriggingh_id),'eam-EMRiggingH-Update')")
    @ApiOperation(value = "更新工具库出入记录", tags = {"工具库出入记录" },  notes = "更新工具库出入记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrigginghs/{emriggingh_id}")
    public ResponseEntity<EMRiggingHDTO> update(@PathVariable("emriggingh_id") String emriggingh_id, @RequestBody EMRiggingHDTO emrigginghdto) {
		EMRiggingH domain  = emrigginghMapping.toDomain(emrigginghdto);
        domain .setEmrigginghid(emriggingh_id);
		emrigginghService.update(domain );
		EMRiggingHDTO dto = emrigginghMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrigginghService.getEmrigginghByEntities(this.emrigginghMapping.toDomain(#emrigginghdtos)),'eam-EMRiggingH-Update')")
    @ApiOperation(value = "批量更新工具库出入记录", tags = {"工具库出入记录" },  notes = "批量更新工具库出入记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrigginghs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRiggingHDTO> emrigginghdtos) {
        emrigginghService.updateBatch(emrigginghMapping.toDomain(emrigginghdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrigginghService.get(#emriggingh_id),'eam-EMRiggingH-Remove')")
    @ApiOperation(value = "删除工具库出入记录", tags = {"工具库出入记录" },  notes = "删除工具库出入记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrigginghs/{emriggingh_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emriggingh_id") String emriggingh_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrigginghService.remove(emriggingh_id));
    }

    @PreAuthorize("hasPermission(this.emrigginghService.getEmrigginghByIds(#ids),'eam-EMRiggingH-Remove')")
    @ApiOperation(value = "批量删除工具库出入记录", tags = {"工具库出入记录" },  notes = "批量删除工具库出入记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrigginghs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrigginghService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrigginghMapping.toDomain(returnObject.body),'eam-EMRiggingH-Get')")
    @ApiOperation(value = "获取工具库出入记录", tags = {"工具库出入记录" },  notes = "获取工具库出入记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emrigginghs/{emriggingh_id}")
    public ResponseEntity<EMRiggingHDTO> get(@PathVariable("emriggingh_id") String emriggingh_id) {
        EMRiggingH domain = emrigginghService.get(emriggingh_id);
        EMRiggingHDTO dto = emrigginghMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工具库出入记录草稿", tags = {"工具库出入记录" },  notes = "获取工具库出入记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrigginghs/getdraft")
    public ResponseEntity<EMRiggingHDTO> getDraft(EMRiggingHDTO dto) {
        EMRiggingH domain = emrigginghMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emrigginghMapping.toDto(emrigginghService.getDraft(domain)));
    }

    @ApiOperation(value = "检查工具库出入记录", tags = {"工具库出入记录" },  notes = "检查工具库出入记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emrigginghs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRiggingHDTO emrigginghdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrigginghService.checkKey(emrigginghMapping.toDomain(emrigginghdto)));
    }

    @PreAuthorize("hasPermission(this.emrigginghMapping.toDomain(#emrigginghdto),'eam-EMRiggingH-Save')")
    @ApiOperation(value = "保存工具库出入记录", tags = {"工具库出入记录" },  notes = "保存工具库出入记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emrigginghs/save")
    public ResponseEntity<EMRiggingHDTO> save(@RequestBody EMRiggingHDTO emrigginghdto) {
        EMRiggingH domain = emrigginghMapping.toDomain(emrigginghdto);
        emrigginghService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrigginghMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrigginghMapping.toDomain(#emrigginghdtos),'eam-EMRiggingH-Save')")
    @ApiOperation(value = "批量保存工具库出入记录", tags = {"工具库出入记录" },  notes = "批量保存工具库出入记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emrigginghs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRiggingHDTO> emrigginghdtos) {
        emrigginghService.saveBatch(emrigginghMapping.toDomain(emrigginghdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRiggingH-searchDefault-all') and hasPermission(#context,'eam-EMRiggingH-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"工具库出入记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrigginghs/fetchdefault")
	public ResponseEntity<List<EMRiggingHDTO>> fetchDefault(EMRiggingHSearchContext context) {
        Page<EMRiggingH> domains = emrigginghService.searchDefault(context) ;
        List<EMRiggingHDTO> list = emrigginghMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRiggingH-searchDefault-all') and hasPermission(#context,'eam-EMRiggingH-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"工具库出入记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrigginghs/searchdefault")
	public ResponseEntity<Page<EMRiggingHDTO>> searchDefault(@RequestBody EMRiggingHSearchContext context) {
        Page<EMRiggingH> domains = emrigginghService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrigginghMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

