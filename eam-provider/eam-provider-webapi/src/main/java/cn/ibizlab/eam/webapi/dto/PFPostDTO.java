package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[PFPostDTO]
 */
@Data
public class PFPostDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [SUMEMPCNT]
     *
     */
    @JSONField(name = "sumempcnt")
    @JsonProperty("sumempcnt")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sumempcnt;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [POSTCODE]
     *
     */
    @JSONField(name = "postcode")
    @JsonProperty("postcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String postcode;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [WAGESLEVELID]
     *
     */
    @JSONField(name = "wageslevelid")
    @JsonProperty("wageslevelid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String wageslevelid;

    /**
     * 属性 [PFPOSTID]
     *
     */
    @JSONField(name = "pfpostid")
    @JsonProperty("pfpostid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfpostid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [POSTLEVEL]
     *
     */
    @JSONField(name = "postlevel")
    @JsonProperty("postlevel")
    private Integer postlevel;

    /**
     * 属性 [PFPOSTNAME]
     *
     */
    @JSONField(name = "pfpostname")
    @JsonProperty("pfpostname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfpostname;

    /**
     * 属性 [POSTTYPEID]
     *
     */
    @JSONField(name = "posttypeid")
    @JsonProperty("posttypeid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String posttypeid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [POSTINFO]
     *
     */
    @JSONField(name = "postinfo")
    @JsonProperty("postinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String postinfo;


    /**
     * 设置 [SUMEMPCNT]
     */
    public void setSumempcnt(String  sumempcnt){
        this.sumempcnt = sumempcnt ;
        this.modify("sumempcnt",sumempcnt);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [POSTCODE]
     */
    public void setPostcode(String  postcode){
        this.postcode = postcode ;
        this.modify("postcode",postcode);
    }

    /**
     * 设置 [WAGESLEVELID]
     */
    public void setWageslevelid(String  wageslevelid){
        this.wageslevelid = wageslevelid ;
        this.modify("wageslevelid",wageslevelid);
    }

    /**
     * 设置 [POSTLEVEL]
     */
    public void setPostlevel(Integer  postlevel){
        this.postlevel = postlevel ;
        this.modify("postlevel",postlevel);
    }

    /**
     * 设置 [PFPOSTNAME]
     */
    public void setPfpostname(String  pfpostname){
        this.pfpostname = pfpostname ;
        this.modify("pfpostname",pfpostname);
    }

    /**
     * 设置 [POSTTYPEID]
     */
    public void setPosttypeid(String  posttypeid){
        this.posttypeid = posttypeid ;
        this.modify("posttypeid",posttypeid);
    }


}


