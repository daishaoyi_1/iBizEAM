package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMBiddingDTO]
 */
@Data
public class EMBiddingDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMBIDDINGID]
     *
     */
    @JSONField(name = "embiddingid")
    @JsonProperty("embiddingid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String embiddingid;

    /**
     * 属性 [PARAMETER]
     *
     */
    @JSONField(name = "parameter")
    @JsonProperty("parameter")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String parameter;

    /**
     * 属性 [QUALIFICATION]
     *
     */
    @JSONField(name = "qualification")
    @JsonProperty("qualification")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String qualification;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String price;

    /**
     * 属性 [ACHIEVEMENT]
     *
     */
    @JSONField(name = "achievement")
    @JsonProperty("achievement")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String achievement;

    /**
     * 属性 [EMBIDDINGNAME]
     *
     */
    @JSONField(name = "embiddingname")
    @JsonProperty("embiddingname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embiddingname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMSERVICENAME]
     *
     */
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicename;

    /**
     * 属性 [EMSERVICEID]
     *
     */
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceid;


    /**
     * 设置 [PARAMETER]
     */
    public void setParameter(String  parameter){
        this.parameter = parameter ;
        this.modify("parameter",parameter);
    }

    /**
     * 设置 [QUALIFICATION]
     */
    public void setQualification(String  qualification){
        this.qualification = qualification ;
        this.modify("qualification",qualification);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(String  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [ACHIEVEMENT]
     */
    public void setAchievement(String  achievement){
        this.achievement = achievement ;
        this.modify("achievement",achievement);
    }

    /**
     * 设置 [EMBIDDINGNAME]
     */
    public void setEmbiddingname(String  embiddingname){
        this.embiddingname = embiddingname ;
        this.modify("embiddingname",embiddingname);
    }

    /**
     * 设置 [EMSERVICEID]
     */
    public void setEmserviceid(String  emserviceid){
        this.emserviceid = emserviceid ;
        this.modify("emserviceid",emserviceid);
    }


}


