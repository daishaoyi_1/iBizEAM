package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMAssessMXDTO]
 */
@Data
public class EMAssessMXDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMASSESSMXNAME]
     *
     */
    @JSONField(name = "emassessmxname")
    @JsonProperty("emassessmxname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassessmxname;

    /**
     * 属性 [EMASSESSMXID]
     *
     */
    @JSONField(name = "emassessmxid")
    @JsonProperty("emassessmxid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassessmxid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [FATE]
     *
     */
    @JSONField(name = "fate")
    @JsonProperty("fate")
    private Double fate;

    /**
     * 属性 [ASSESSDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "assessdate" , format="yyyy-MM-dd")
    @JsonProperty("assessdate")
    private Timestamp assessdate;

    /**
     * 属性 [EMDISABLEASSESSNAME]
     *
     */
    @JSONField(name = "emdisableassessname")
    @JsonProperty("emdisableassessname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emdisableassessname;

    /**
     * 属性 [EMEQUIPNAME]
     *
     */
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emequipname;

    /**
     * 属性 [EMDISABLEASSESSID]
     *
     */
    @JSONField(name = "emdisableassessid")
    @JsonProperty("emdisableassessid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emdisableassessid;

    /**
     * 属性 [EMEQUIPID]
     *
     */
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emequipid;


    /**
     * 设置 [EMASSESSMXNAME]
     */
    public void setEmassessmxname(String  emassessmxname){
        this.emassessmxname = emassessmxname ;
        this.modify("emassessmxname",emassessmxname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [FATE]
     */
    public void setFate(Double  fate){
        this.fate = fate ;
        this.modify("fate",fate);
    }

    /**
     * 设置 [ASSESSDATE]
     */
    public void setAssessdate(Timestamp  assessdate){
        this.assessdate = assessdate ;
        this.modify("assessdate",assessdate);
    }

    /**
     * 设置 [EMDISABLEASSESSID]
     */
    public void setEmdisableassessid(String  emdisableassessid){
        this.emdisableassessid = emdisableassessid ;
        this.modify("emdisableassessid",emdisableassessid);
    }

    /**
     * 设置 [EMEQUIPID]
     */
    public void setEmequipid(String  emequipid){
        this.emequipid = emequipid ;
        this.modify("emequipid",emequipid);
    }


}


