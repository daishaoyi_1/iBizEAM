package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMAssetDTO]
 */
@Data
public class EMAssetDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NUM]
     *
     */
    @JSONField(name = "num")
    @JsonProperty("num")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String num;

    /**
     * 属性 [EQISSERVICE]
     *
     */
    @JSONField(name = "eqisservice")
    @JsonProperty("eqisservice")
    private Integer eqisservice;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empname;

    /**
     * 属性 [WARRANTYDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warrantydate" , format="yyyy-MM-dd")
    @JsonProperty("warrantydate")
    private Timestamp warrantydate;

    /**
     * 属性 [INNERLABORCOST]
     *
     */
    @JSONField(name = "innerlaborcost")
    @JsonProperty("innerlaborcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String innerlaborcost;

    /**
     * 属性 [KEYATTPARAM]
     *
     */
    @JSONField(name = "keyattparam")
    @JsonProperty("keyattparam")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String keyattparam;

    /**
     * 属性 [FOREIGNLABORCOST]
     *
     */
    @JSONField(name = "foreignlaborcost")
    @JsonProperty("foreignlaborcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String foreignlaborcost;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [JTSB]
     *
     */
    @JSONField(name = "jtsb")
    @JsonProperty("jtsb")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jtsb;

    /**
     * 属性 [EQLIFE]
     *
     */
    @JSONField(name = "eqlife")
    @JsonProperty("eqlife")
    @NotNull(message = "[使用期限]不允许为空!")
    private Double eqlife;

    /**
     * 属性 [LASTZJDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastzjdate" , format="yyyy-MM-dd")
    @JsonProperty("lastzjdate")
    private Timestamp lastzjdate;

    /**
     * 属性 [DEPTNAME]
     *
     */
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptname;

    /**
     * 属性 [ASSETTYPE]
     *
     */
    @JSONField(name = "assettype")
    @JsonProperty("assettype")
    @NotBlank(message = "[资产类别]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assettype;

    /**
     * 属性 [NOW]
     *
     */
    @JSONField(name = "now")
    @JsonProperty("now")
    private Double now;

    /**
     * 属性 [YTZJ]
     *
     */
    @JSONField(name = "ytzj")
    @JsonProperty("ytzj")
    private Double ytzj;

    /**
     * 属性 [DISDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "disdate" , format="yyyy-MM-dd")
    @JsonProperty("disdate")
    private Timestamp disdate;

    /**
     * 属性 [ASSETSTATE]
     *
     */
    @JSONField(name = "assetstate")
    @JsonProperty("assetstate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetstate;

    /**
     * 属性 [ORIGINALCOST]
     *
     */
    @JSONField(name = "originalcost")
    @JsonProperty("originalcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String originalcost;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptid;

    /**
     * 属性 [TECHCODE]
     *
     */
    @JSONField(name = "techcode")
    @JsonProperty("techcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String techcode;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [MATERIALCOST]
     *
     */
    @JSONField(name = "materialcost")
    @JsonProperty("materialcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String materialcost;

    /**
     * 属性 [DISCOST]
     *
     */
    @JSONField(name = "discost")
    @JsonProperty("discost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String discost;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [USEDYEAR]
     *
     */
    @JSONField(name = "usedyear")
    @JsonProperty("usedyear")
    private Integer usedyear;

    /**
     * 属性 [EMASSETNAME]
     *
     */
    @JSONField(name = "emassetname")
    @JsonProperty("emassetname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassetname;

    /**
     * 属性 [REPLACECOST]
     *
     */
    @JSONField(name = "replacecost")
    @JsonProperty("replacecost")
    private Double replacecost;

    /**
     * 属性 [ASSETEQUIPID]
     *
     */
    @JSONField(name = "assetequipid")
    @JsonProperty("assetequipid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetequipid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [EQLIFEYEAR]
     *
     */
    @JSONField(name = "eqlifeyear")
    @JsonProperty("eqlifeyear")
    private Integer eqlifeyear;

    /**
     * 属性 [ASSETSORT]
     *
     */
    @JSONField(name = "assetsort")
    @JsonProperty("assetsort")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetsort;

    /**
     * 属性 [MGRDEPTID]
     *
     */
    @JSONField(name = "mgrdeptid")
    @JsonProperty("mgrdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mgrdeptid;

    /**
     * 属性 [BLSYSTEMDESC]
     *
     */
    @JSONField(name = "blsystemdesc")
    @JsonProperty("blsystemdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String blsystemdesc;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [COSTCENTERID]
     *
     */
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costcenterid;

    /**
     * 属性 [PURCHDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "purchdate" , format="yyyy-MM-dd")
    @JsonProperty("purchdate")
    @NotNull(message = "[采购日期]不允许为空!")
    private Timestamp purchdate;

    /**
     * 属性 [EQSTARTDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "eqstartdate" , format="yyyy-MM-dd")
    @JsonProperty("eqstartdate")
    private Timestamp eqstartdate;

    /**
     * 属性 [EQPRIORITY]
     *
     */
    @JSONField(name = "eqpriority")
    @JsonProperty("eqpriority")
    private Double eqpriority;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DISDESC]
     *
     */
    @JSONField(name = "disdesc")
    @JsonProperty("disdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String disdesc;

    /**
     * 属性 [SYQX]
     *
     */
    @JSONField(name = "syqx")
    @JsonProperty("syqx")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String syqx;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [REPLACERATE]
     *
     */
    @JSONField(name = "replacerate")
    @JsonProperty("replacerate")
    @NotNull(message = "[残值率(%)]不允许为空!")
    private Double replacerate;

    /**
     * 属性 [ASSETINFO]
     *
     */
    @JSONField(name = "assetinfo")
    @JsonProperty("assetinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetinfo;

    /**
     * 属性 [EMASSETID]
     *
     */
    @JSONField(name = "emassetid")
    @JsonProperty("emassetid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassetid;

    /**
     * 属性 [HJ]
     *
     */
    @JSONField(name = "hj")
    @JsonProperty("hj")
    private Double hj;

    /**
     * 属性 [ASSETCODE]
     *
     */
    @JSONField(name = "assetcode")
    @JsonProperty("assetcode")
    @NotBlank(message = "[资产代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetcode;

    /**
     * 属性 [ASSETDESC]
     *
     */
    @JSONField(name = "assetdesc")
    @JsonProperty("assetdesc")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String assetdesc;

    /**
     * 属性 [MGRDEPTNAME]
     *
     */
    @JSONField(name = "mgrdeptname")
    @JsonProperty("mgrdeptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mgrdeptname;

    /**
     * 属性 [EQSERIALCODE]
     *
     */
    @JSONField(name = "eqserialcode")
    @JsonProperty("eqserialcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqserialcode;

    /**
     * 属性 [ASSETLCT]
     *
     */
    @JSONField(name = "assetlct")
    @JsonProperty("assetlct")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetlct;

    /**
     * 属性 [EQMODELCODE]
     *
     */
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqmodelcode;

    /**
     * 属性 [EQSUMSTOPTIME]
     *
     */
    @JSONField(name = "eqsumstoptime")
    @JsonProperty("eqsumstoptime")
    private Double eqsumstoptime;

    /**
     * 属性 [SF]
     *
     */
    @JSONField(name = "sf")
    @JsonProperty("sf")
    @NotBlank(message = "[税费]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sf;

    /**
     * 属性 [PPLACE]
     *
     */
    @JSONField(name = "pplace")
    @JsonProperty("pplace")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pplace;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [CONTRACTNAME]
     *
     */
    @JSONField(name = "contractname")
    @JsonProperty("contractname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String contractname;

    /**
     * 属性 [ASSETCLASSNAME]
     *
     */
    @JSONField(name = "assetclassname")
    @JsonProperty("assetclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetclassname;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String acclassname;

    /**
     * 属性 [EQLOCATIONNAME]
     *
     */
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationname;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rservicename;

    /**
     * 属性 [ASSETCLASSCODE]
     *
     */
    @JSONField(name = "assetclasscode")
    @JsonProperty("assetclasscode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclasscode;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [MSERVICENAME]
     *
     */
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mservicename;

    /**
     * 属性 [ASSETCLASSID]
     *
     */
    @JSONField(name = "assetclassid")
    @JsonProperty("assetclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclassid;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String acclassid;

    /**
     * 属性 [EQLOCATIONID]
     *
     */
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationid;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitid;

    /**
     * 属性 [MSERVICEID]
     *
     */
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mserviceid;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rserviceid;

    /**
     * 属性 [CONTRACTID]
     *
     */
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contractid;


    /**
     * 设置 [NUM]
     */
    public void setNum(String  num){
        this.num = num ;
        this.modify("num",num);
    }

    /**
     * 设置 [EQISSERVICE]
     */
    public void setEqisservice(Integer  eqisservice){
        this.eqisservice = eqisservice ;
        this.modify("eqisservice",eqisservice);
    }

    /**
     * 设置 [EMPNAME]
     */
    public void setEmpname(String  empname){
        this.empname = empname ;
        this.modify("empname",empname);
    }

    /**
     * 设置 [WARRANTYDATE]
     */
    public void setWarrantydate(Timestamp  warrantydate){
        this.warrantydate = warrantydate ;
        this.modify("warrantydate",warrantydate);
    }

    /**
     * 设置 [INNERLABORCOST]
     */
    public void setInnerlaborcost(String  innerlaborcost){
        this.innerlaborcost = innerlaborcost ;
        this.modify("innerlaborcost",innerlaborcost);
    }

    /**
     * 设置 [KEYATTPARAM]
     */
    public void setKeyattparam(String  keyattparam){
        this.keyattparam = keyattparam ;
        this.modify("keyattparam",keyattparam);
    }

    /**
     * 设置 [FOREIGNLABORCOST]
     */
    public void setForeignlaborcost(String  foreignlaborcost){
        this.foreignlaborcost = foreignlaborcost ;
        this.modify("foreignlaborcost",foreignlaborcost);
    }

    /**
     * 设置 [JTSB]
     */
    public void setJtsb(String  jtsb){
        this.jtsb = jtsb ;
        this.modify("jtsb",jtsb);
    }

    /**
     * 设置 [EQLIFE]
     */
    public void setEqlife(Double  eqlife){
        this.eqlife = eqlife ;
        this.modify("eqlife",eqlife);
    }

    /**
     * 设置 [LASTZJDATE]
     */
    public void setLastzjdate(Timestamp  lastzjdate){
        this.lastzjdate = lastzjdate ;
        this.modify("lastzjdate",lastzjdate);
    }

    /**
     * 设置 [DEPTNAME]
     */
    public void setDeptname(String  deptname){
        this.deptname = deptname ;
        this.modify("deptname",deptname);
    }

    /**
     * 设置 [ASSETTYPE]
     */
    public void setAssettype(String  assettype){
        this.assettype = assettype ;
        this.modify("assettype",assettype);
    }

    /**
     * 设置 [YTZJ]
     */
    public void setYtzj(Double  ytzj){
        this.ytzj = ytzj ;
        this.modify("ytzj",ytzj);
    }

    /**
     * 设置 [DISDATE]
     */
    public void setDisdate(Timestamp  disdate){
        this.disdate = disdate ;
        this.modify("disdate",disdate);
    }

    /**
     * 设置 [ASSETSTATE]
     */
    public void setAssetstate(String  assetstate){
        this.assetstate = assetstate ;
        this.modify("assetstate",assetstate);
    }

    /**
     * 设置 [ORIGINALCOST]
     */
    public void setOriginalcost(String  originalcost){
        this.originalcost = originalcost ;
        this.modify("originalcost",originalcost);
    }

    /**
     * 设置 [DEPTID]
     */
    public void setDeptid(String  deptid){
        this.deptid = deptid ;
        this.modify("deptid",deptid);
    }

    /**
     * 设置 [TECHCODE]
     */
    public void setTechcode(String  techcode){
        this.techcode = techcode ;
        this.modify("techcode",techcode);
    }

    /**
     * 设置 [MATERIALCOST]
     */
    public void setMaterialcost(String  materialcost){
        this.materialcost = materialcost ;
        this.modify("materialcost",materialcost);
    }

    /**
     * 设置 [DISCOST]
     */
    public void setDiscost(String  discost){
        this.discost = discost ;
        this.modify("discost",discost);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMASSETNAME]
     */
    public void setEmassetname(String  emassetname){
        this.emassetname = emassetname ;
        this.modify("emassetname",emassetname);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [MGRDEPTID]
     */
    public void setMgrdeptid(String  mgrdeptid){
        this.mgrdeptid = mgrdeptid ;
        this.modify("mgrdeptid",mgrdeptid);
    }

    /**
     * 设置 [BLSYSTEMDESC]
     */
    public void setBlsystemdesc(String  blsystemdesc){
        this.blsystemdesc = blsystemdesc ;
        this.modify("blsystemdesc",blsystemdesc);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [COSTCENTERID]
     */
    public void setCostcenterid(String  costcenterid){
        this.costcenterid = costcenterid ;
        this.modify("costcenterid",costcenterid);
    }

    /**
     * 设置 [PURCHDATE]
     */
    public void setPurchdate(Timestamp  purchdate){
        this.purchdate = purchdate ;
        this.modify("purchdate",purchdate);
    }

    /**
     * 设置 [EQSTARTDATE]
     */
    public void setEqstartdate(Timestamp  eqstartdate){
        this.eqstartdate = eqstartdate ;
        this.modify("eqstartdate",eqstartdate);
    }

    /**
     * 设置 [EQPRIORITY]
     */
    public void setEqpriority(Double  eqpriority){
        this.eqpriority = eqpriority ;
        this.modify("eqpriority",eqpriority);
    }

    /**
     * 设置 [DISDESC]
     */
    public void setDisdesc(String  disdesc){
        this.disdesc = disdesc ;
        this.modify("disdesc",disdesc);
    }

    /**
     * 设置 [REPLACERATE]
     */
    public void setReplacerate(Double  replacerate){
        this.replacerate = replacerate ;
        this.modify("replacerate",replacerate);
    }

    /**
     * 设置 [ASSETCODE]
     */
    public void setAssetcode(String  assetcode){
        this.assetcode = assetcode ;
        this.modify("assetcode",assetcode);
    }

    /**
     * 设置 [ASSETDESC]
     */
    public void setAssetdesc(String  assetdesc){
        this.assetdesc = assetdesc ;
        this.modify("assetdesc",assetdesc);
    }

    /**
     * 设置 [MGRDEPTNAME]
     */
    public void setMgrdeptname(String  mgrdeptname){
        this.mgrdeptname = mgrdeptname ;
        this.modify("mgrdeptname",mgrdeptname);
    }

    /**
     * 设置 [EQSERIALCODE]
     */
    public void setEqserialcode(String  eqserialcode){
        this.eqserialcode = eqserialcode ;
        this.modify("eqserialcode",eqserialcode);
    }

    /**
     * 设置 [ASSETLCT]
     */
    public void setAssetlct(String  assetlct){
        this.assetlct = assetlct ;
        this.modify("assetlct",assetlct);
    }

    /**
     * 设置 [EQMODELCODE]
     */
    public void setEqmodelcode(String  eqmodelcode){
        this.eqmodelcode = eqmodelcode ;
        this.modify("eqmodelcode",eqmodelcode);
    }

    /**
     * 设置 [EQSUMSTOPTIME]
     */
    public void setEqsumstoptime(Double  eqsumstoptime){
        this.eqsumstoptime = eqsumstoptime ;
        this.modify("eqsumstoptime",eqsumstoptime);
    }

    /**
     * 设置 [SF]
     */
    public void setSf(String  sf){
        this.sf = sf ;
        this.modify("sf",sf);
    }

    /**
     * 设置 [PPLACE]
     */
    public void setPplace(String  pplace){
        this.pplace = pplace ;
        this.modify("pplace",pplace);
    }

    /**
     * 设置 [ASSETCLASSID]
     */
    public void setAssetclassid(String  assetclassid){
        this.assetclassid = assetclassid ;
        this.modify("assetclassid",assetclassid);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [EQLOCATIONID]
     */
    public void setEqlocationid(String  eqlocationid){
        this.eqlocationid = eqlocationid ;
        this.modify("eqlocationid",eqlocationid);
    }

    /**
     * 设置 [UNITID]
     */
    public void setUnitid(String  unitid){
        this.unitid = unitid ;
        this.modify("unitid",unitid);
    }

    /**
     * 设置 [MSERVICEID]
     */
    public void setMserviceid(String  mserviceid){
        this.mserviceid = mserviceid ;
        this.modify("mserviceid",mserviceid);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [CONTRACTID]
     */
    public void setContractid(String  contractid){
        this.contractid = contractid ;
        this.modify("contractid",contractid);
    }


}


