package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMDisableAssessDTO]
 */
@Data
public class EMDisableAssessDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMDISABLEASSESSNAME]
     *
     */
    @JSONField(name = "emdisableassessname")
    @JsonProperty("emdisableassessname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emdisableassessname;

    /**
     * 属性 [ASSESSNSME]
     *
     */
    @JSONField(name = "assessnsme")
    @JsonProperty("assessnsme")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assessnsme;

    /**
     * 属性 [EMDISABLEASSESSID]
     *
     */
    @JSONField(name = "emdisableassessid")
    @JsonProperty("emdisableassessid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emdisableassessid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [TABLEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "tabledate" , format="yyyy-MM-dd")
    @JsonProperty("tabledate")
    private Timestamp tabledate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String amount;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PFTEAMNAME]
     *
     */
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pfteamname;

    /**
     * 属性 [PFTEAMID]
     *
     */
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfteamid;


    /**
     * 设置 [EMDISABLEASSESSNAME]
     */
    public void setEmdisableassessname(String  emdisableassessname){
        this.emdisableassessname = emdisableassessname ;
        this.modify("emdisableassessname",emdisableassessname);
    }

    /**
     * 设置 [ASSESSNSME]
     */
    public void setAssessnsme(String  assessnsme){
        this.assessnsme = assessnsme ;
        this.modify("assessnsme",assessnsme);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [TABLEDATE]
     */
    public void setTabledate(Timestamp  tabledate){
        this.tabledate = tabledate ;
        this.modify("tabledate",tabledate);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(String  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [PFTEAMID]
     */
    public void setPfteamid(String  pfteamid){
        this.pfteamid = pfteamid ;
        this.modify("pfteamid",pfteamid);
    }


}


