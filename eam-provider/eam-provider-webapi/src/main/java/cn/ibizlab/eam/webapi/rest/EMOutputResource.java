package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMOutput;
import cn.ibizlab.eam.core.eam_core.service.IEMOutputService;
import cn.ibizlab.eam.core.eam_core.filter.EMOutputSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"能力" })
@RestController("WebApi-emoutput")
@RequestMapping("")
public class EMOutputResource {

    @Autowired
    public IEMOutputService emoutputService;

    @Autowired
    @Lazy
    public EMOutputMapping emoutputMapping;

    @PreAuthorize("hasPermission(this.emoutputMapping.toDomain(#emoutputdto),'eam-EMOutput-Create')")
    @ApiOperation(value = "新建能力", tags = {"能力" },  notes = "新建能力")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs")
    public ResponseEntity<EMOutputDTO> create(@Validated @RequestBody EMOutputDTO emoutputdto) {
        EMOutput domain = emoutputMapping.toDomain(emoutputdto);
		emoutputService.create(domain);
        EMOutputDTO dto = emoutputMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emoutputMapping.toDomain(#emoutputdtos),'eam-EMOutput-Create')")
    @ApiOperation(value = "批量新建能力", tags = {"能力" },  notes = "批量新建能力")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMOutputDTO> emoutputdtos) {
        emoutputService.createBatch(emoutputMapping.toDomain(emoutputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emoutput" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emoutputService.get(#emoutput_id),'eam-EMOutput-Update')")
    @ApiOperation(value = "更新能力", tags = {"能力" },  notes = "更新能力")
	@RequestMapping(method = RequestMethod.PUT, value = "/emoutputs/{emoutput_id}")
    public ResponseEntity<EMOutputDTO> update(@PathVariable("emoutput_id") String emoutput_id, @RequestBody EMOutputDTO emoutputdto) {
		EMOutput domain  = emoutputMapping.toDomain(emoutputdto);
        domain .setEmoutputid(emoutput_id);
		emoutputService.update(domain );
		EMOutputDTO dto = emoutputMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emoutputService.getEmoutputByEntities(this.emoutputMapping.toDomain(#emoutputdtos)),'eam-EMOutput-Update')")
    @ApiOperation(value = "批量更新能力", tags = {"能力" },  notes = "批量更新能力")
	@RequestMapping(method = RequestMethod.PUT, value = "/emoutputs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMOutputDTO> emoutputdtos) {
        emoutputService.updateBatch(emoutputMapping.toDomain(emoutputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emoutputService.get(#emoutput_id),'eam-EMOutput-Remove')")
    @ApiOperation(value = "删除能力", tags = {"能力" },  notes = "删除能力")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emoutputs/{emoutput_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emoutput_id") String emoutput_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emoutputService.remove(emoutput_id));
    }

    @PreAuthorize("hasPermission(this.emoutputService.getEmoutputByIds(#ids),'eam-EMOutput-Remove')")
    @ApiOperation(value = "批量删除能力", tags = {"能力" },  notes = "批量删除能力")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emoutputs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emoutputService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emoutputMapping.toDomain(returnObject.body),'eam-EMOutput-Get')")
    @ApiOperation(value = "获取能力", tags = {"能力" },  notes = "获取能力")
	@RequestMapping(method = RequestMethod.GET, value = "/emoutputs/{emoutput_id}")
    public ResponseEntity<EMOutputDTO> get(@PathVariable("emoutput_id") String emoutput_id) {
        EMOutput domain = emoutputService.get(emoutput_id);
        EMOutputDTO dto = emoutputMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取能力草稿", tags = {"能力" },  notes = "获取能力草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emoutputs/getdraft")
    public ResponseEntity<EMOutputDTO> getDraft(EMOutputDTO dto) {
        EMOutput domain = emoutputMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emoutputMapping.toDto(emoutputService.getDraft(domain)));
    }

    @ApiOperation(value = "检查能力", tags = {"能力" },  notes = "检查能力")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMOutputDTO emoutputdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emoutputService.checkKey(emoutputMapping.toDomain(emoutputdto)));
    }

    @PreAuthorize("hasPermission(this.emoutputMapping.toDomain(#emoutputdto),'eam-EMOutput-Save')")
    @ApiOperation(value = "保存能力", tags = {"能力" },  notes = "保存能力")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/save")
    public ResponseEntity<EMOutputDTO> save(@RequestBody EMOutputDTO emoutputdto) {
        EMOutput domain = emoutputMapping.toDomain(emoutputdto);
        emoutputService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emoutputMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emoutputMapping.toDomain(#emoutputdtos),'eam-EMOutput-Save')")
    @ApiOperation(value = "批量保存能力", tags = {"能力" },  notes = "批量保存能力")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMOutputDTO> emoutputdtos) {
        emoutputService.saveBatch(emoutputMapping.toDomain(emoutputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMOutput-searchDefault-all') and hasPermission(#context,'eam-EMOutput-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"能力" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emoutputs/fetchdefault")
	public ResponseEntity<List<EMOutputDTO>> fetchDefault(EMOutputSearchContext context) {
        Page<EMOutput> domains = emoutputService.searchDefault(context) ;
        List<EMOutputDTO> list = emoutputMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMOutput-searchDefault-all') and hasPermission(#context,'eam-EMOutput-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"能力" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emoutputs/searchdefault")
	public ResponseEntity<Page<EMOutputDTO>> searchDefault(@RequestBody EMOutputSearchContext context) {
        Page<EMOutput> domains = emoutputService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emoutputMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

