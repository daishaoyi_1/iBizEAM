package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQLocationDTO]
 */
@Data
public class EMEQLocationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EQLOCATIONINFO]
     *
     */
    @JSONField(name = "eqlocationinfo")
    @JsonProperty("eqlocationinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationinfo;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMEQLOCATIONNAME]
     *
     */
    @JSONField(name = "emeqlocationname")
    @JsonProperty("emeqlocationname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqlocationname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMEQLOCATIONID]
     *
     */
    @JSONField(name = "emeqlocationid")
    @JsonProperty("emeqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqlocationid;

    /**
     * 属性 [EQLOCATIONCODE]
     *
     */
    @JSONField(name = "eqlocationcode")
    @JsonProperty("eqlocationcode")
    @NotBlank(message = "[位置代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationcode;

    /**
     * 属性 [EQLOCATIONTYPE]
     *
     */
    @JSONField(name = "eqlocationtype")
    @JsonProperty("eqlocationtype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationtype;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [MAJOREQUIPNAME]
     *
     */
    @JSONField(name = "majorequipname")
    @JsonProperty("majorequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String majorequipname;

    /**
     * 属性 [MAJOREQUIPID]
     *
     */
    @JSONField(name = "majorequipid")
    @JsonProperty("majorequipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String majorequipid;


    /**
     * 设置 [EMEQLOCATIONNAME]
     */
    public void setEmeqlocationname(String  emeqlocationname){
        this.emeqlocationname = emeqlocationname ;
        this.modify("emeqlocationname",emeqlocationname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EQLOCATIONCODE]
     */
    public void setEqlocationcode(String  eqlocationcode){
        this.eqlocationcode = eqlocationcode ;
        this.modify("eqlocationcode",eqlocationcode);
    }

    /**
     * 设置 [EQLOCATIONTYPE]
     */
    public void setEqlocationtype(String  eqlocationtype){
        this.eqlocationtype = eqlocationtype ;
        this.modify("eqlocationtype",eqlocationtype);
    }

    /**
     * 设置 [MAJOREQUIPID]
     */
    public void setMajorequipid(String  majorequipid){
        this.majorequipid = majorequipid ;
        this.modify("majorequipid",majorequipid);
    }


}


