package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMMonthlyDetailDTO]
 */
@Data
public class EMMonthlyDetailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FINISHTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "finishtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("finishtime")
    private Timestamp finishtime;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EXECUTION]
     *
     */
    @JSONField(name = "execution")
    @JsonProperty("execution")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String execution;

    /**
     * 属性 [REMARKS]
     *
     */
    @JSONField(name = "remarks")
    @JsonProperty("remarks")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String remarks;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [NUM]
     *
     */
    @JSONField(name = "num")
    @JsonProperty("num")
    private Integer num;

    /**
     * 属性 [MAINTENANCECONTENT]
     *
     */
    @JSONField(name = "maintenancecontent")
    @JsonProperty("maintenancecontent")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String maintenancecontent;

    /**
     * 属性 [EMMONTHLYDETAILNAME]
     *
     */
    @JSONField(name = "emmonthlydetailname")
    @JsonProperty("emmonthlydetailname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emmonthlydetailname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMMONTHLYDETAILID]
     *
     */
    @JSONField(name = "emmonthlydetailid")
    @JsonProperty("emmonthlydetailid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emmonthlydetailid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMMONTHLYNAME]
     *
     */
    @JSONField(name = "emmonthlyname")
    @JsonProperty("emmonthlyname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emmonthlyname;

    /**
     * 属性 [EMMONTHLYID]
     *
     */
    @JSONField(name = "emmonthlyid")
    @JsonProperty("emmonthlyid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emmonthlyid;


    /**
     * 设置 [FINISHTIME]
     */
    public void setFinishtime(Timestamp  finishtime){
        this.finishtime = finishtime ;
        this.modify("finishtime",finishtime);
    }

    /**
     * 设置 [EXECUTION]
     */
    public void setExecution(String  execution){
        this.execution = execution ;
        this.modify("execution",execution);
    }

    /**
     * 设置 [REMARKS]
     */
    public void setRemarks(String  remarks){
        this.remarks = remarks ;
        this.modify("remarks",remarks);
    }

    /**
     * 设置 [NUM]
     */
    public void setNum(Integer  num){
        this.num = num ;
        this.modify("num",num);
    }

    /**
     * 设置 [MAINTENANCECONTENT]
     */
    public void setMaintenancecontent(String  maintenancecontent){
        this.maintenancecontent = maintenancecontent ;
        this.modify("maintenancecontent",maintenancecontent);
    }

    /**
     * 设置 [EMMONTHLYDETAILNAME]
     */
    public void setEmmonthlydetailname(String  emmonthlydetailname){
        this.emmonthlydetailname = emmonthlydetailname ;
        this.modify("emmonthlydetailname",emmonthlydetailname);
    }

    /**
     * 设置 [EMMONTHLYID]
     */
    public void setEmmonthlyid(String  emmonthlyid){
        this.emmonthlyid = emmonthlyid ;
        this.modify("emmonthlyid",emmonthlyid);
    }


}


