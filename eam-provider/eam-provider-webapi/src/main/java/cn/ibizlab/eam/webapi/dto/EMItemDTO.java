package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMItemDTO]
 */
@Data
public class EMItemDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [LASTPRICE]
     *
     */
    @JSONField(name = "lastprice")
    @JsonProperty("lastprice")
    private Double lastprice;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objid;

    /**
     * 属性 [ITEMINFO]
     *
     */
    @JSONField(name = "iteminfo")
    @JsonProperty("iteminfo")
    @Size(min = 0, max = 400, message = "内容长度必须小于等于[400]")
    private String iteminfo;

    /**
     * 属性 [ISASSETFLAG]
     *
     */
    @JSONField(name = "isassetflag")
    @JsonProperty("isassetflag")
    private Integer isassetflag;

    /**
     * 属性 [HIGHSUM]
     *
     */
    @JSONField(name = "highsum")
    @JsonProperty("highsum")
    private Double highsum;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [ISBATCHFLAG]
     *
     */
    @JSONField(name = "isbatchflag")
    @JsonProperty("isbatchflag")
    private Integer isbatchflag;

    /**
     * 属性 [CHECKMETHOD]
     *
     */
    @JSONField(name = "checkmethod")
    @JsonProperty("checkmethod")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String checkmethod;

    /**
     * 属性 [EMITEMID]
     *
     */
    @JSONField(name = "emitemid")
    @JsonProperty("emitemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitemid;

    /**
     * 属性 [STOCKDESUM]
     *
     */
    @JSONField(name = "stockdesum")
    @JsonProperty("stockdesum")
    private Double stockdesum;

    /**
     * 属性 [ABCTYPE]
     *
     */
    @JSONField(name = "abctype")
    @JsonProperty("abctype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String abctype;

    /**
     * 属性 [SHFPRICE]
     *
     */
    @JSONField(name = "shfprice")
    @JsonProperty("shfprice")
    private Double shfprice;

    /**
     * 属性 [REPSUM]
     *
     */
    @JSONField(name = "repsum")
    @JsonProperty("repsum")
    private Double repsum;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [STOCKEXTIME]
     *
     */
    @JSONField(name = "stockextime")
    @JsonProperty("stockextime")
    private Double stockextime;

    /**
     * 属性 [ISNEW]
     *
     */
    @JSONField(name = "isnew")
    @JsonProperty("isnew")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String isnew;

    /**
     * 属性 [ITEMNID]
     *
     */
    @JSONField(name = "itemnid")
    @JsonProperty("itemnid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemnid;

    /**
     * 属性 [ITEMSERIALCODE]
     *
     */
    @JSONField(name = "itemserialcode")
    @JsonProperty("itemserialcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemserialcode;

    /**
     * 属性 [REGISTERDAT]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "registerdat" , format="yyyy-MM-dd")
    @JsonProperty("registerdat")
    private Timestamp registerdat;

    /**
     * 属性 [ITEMMODELCODE]
     *
     */
    @JSONField(name = "itemmodelcode")
    @JsonProperty("itemmodelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemmodelcode;

    /**
     * 属性 [ITEMNCODE]
     *
     */
    @JSONField(name = "itemncode")
    @JsonProperty("itemncode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemncode;

    /**
     * 属性 [ITEMCODE]
     *
     */
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    @NotBlank(message = "[物品代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemcode;

    /**
     * 属性 [NO3Q]
     *
     */
    @JSONField(name = "no3q")
    @JsonProperty("no3q")
    private Integer no3q;

    /**
     * 属性 [EMITEMNAME]
     *
     */
    @JSONField(name = "emitemname")
    @JsonProperty("emitemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitemname;

    /**
     * 属性 [DENS]
     *
     */
    @JSONField(name = "dens")
    @JsonProperty("dens")
    private Double dens;

    /**
     * 属性 [SAPCONTROL]
     *
     */
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    private Integer sapcontrol;

    /**
     * 属性 [BATCHTYPE]
     *
     */
    @JSONField(name = "batchtype")
    @JsonProperty("batchtype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String batchtype;

    /**
     * 属性 [ITEMGROUP]
     *
     */
    @JSONField(name = "itemgroup")
    @JsonProperty("itemgroup")
    @NotNull(message = "[物品分组]不允许为空!")
    private Integer itemgroup;

    /**
     * 属性 [LASTSUM]
     *
     */
    @JSONField(name = "lastsum")
    @JsonProperty("lastsum")
    private Double lastsum;

    /**
     * 属性 [STOCKSUM]
     *
     */
    @JSONField(name = "stocksum")
    @JsonProperty("stocksum")
    private Double stocksum;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [LASTINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "lastindate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastindate")
    private Timestamp lastindate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [SAPCONTROLCODE]
     *
     */
    @JSONField(name = "sapcontrolcode")
    @JsonProperty("sapcontrolcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sapcontrolcode;

    /**
     * 属性 [LIFE]
     *
     */
    @JSONField(name = "life")
    @JsonProperty("life")
    private Integer life;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [COSTCENTERID]
     *
     */
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costcenterid;

    /**
     * 属性 [STOCKINL]
     *
     */
    @JSONField(name = "stockinl")
    @JsonProperty("stockinl")
    private Double stockinl;

    /**
     * 属性 [ITEMDESC]
     *
     */
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemdesc;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [WARRANTYDAY]
     *
     */
    @JSONField(name = "warrantyday")
    @JsonProperty("warrantyday")
    private Double warrantyday;

    /**
     * 属性 [STORENAME]
     *
     */
    @JSONField(name = "storename")
    @JsonProperty("storename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storename;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [EMCABNAME]
     *
     */
    @JSONField(name = "emcabname")
    @JsonProperty("emcabname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emcabname;

    /**
     * 属性 [STORECODE]
     *
     */
    @JSONField(name = "storecode")
    @JsonProperty("storecode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storecode;

    /**
     * 属性 [STOREPARTNAME]
     *
     */
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storepartname;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [ITEMMTYPEID]
     *
     */
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemmtypeid;

    /**
     * 属性 [MSERVICENAME]
     *
     */
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mservicename;

    /**
     * 属性 [ITEMBTYPENAME]
     *
     */
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itembtypename;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itembtypeid;

    /**
     * 属性 [ITEMTYPECODE]
     *
     */
    @JSONField(name = "itemtypecode")
    @JsonProperty("itemtypecode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemtypecode;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String acclassname;

    /**
     * 属性 [ITEMMTYPENAME]
     *
     */
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemmtypename;

    /**
     * 属性 [ITEMTYPENAME]
     *
     */
    @JSONField(name = "itemtypename")
    @JsonProperty("itemtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemtypename;

    /**
     * 属性 [EMCABID]
     *
     */
    @JSONField(name = "emcabid")
    @JsonProperty("emcabid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emcabid;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @NotBlank(message = "[单位]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitid;

    /**
     * 属性 [STOREPARTID]
     *
     */
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    @NotBlank(message = "[最新存储库位]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storepartid;

    /**
     * 属性 [ITEMTYPEID]
     *
     */
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    @NotBlank(message = "[物品类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemtypeid;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String acclassid;

    /**
     * 属性 [MSERVICEID]
     *
     */
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mserviceid;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [STOREID]
     *
     */
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    @NotBlank(message = "[最新存储仓库]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storeid;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empname;

    /**
     * 属性 [SEMPID]
     *
     */
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sempid;

    /**
     * 属性 [SEMPNAME]
     *
     */
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sempname;

    /**
     * 属性 [LASTAEMPID]
     *
     */
    @JSONField(name = "lastaempid")
    @JsonProperty("lastaempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String lastaempid;

    /**
     * 属性 [LASTAEMPNAME]
     *
     */
    @JSONField(name = "lastaempname")
    @JsonProperty("lastaempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String lastaempname;


    /**
     * 设置 [LASTPRICE]
     */
    public void setLastprice(Double  lastprice){
        this.lastprice = lastprice ;
        this.modify("lastprice",lastprice);
    }

    /**
     * 设置 [ISASSETFLAG]
     */
    public void setIsassetflag(Integer  isassetflag){
        this.isassetflag = isassetflag ;
        this.modify("isassetflag",isassetflag);
    }

    /**
     * 设置 [HIGHSUM]
     */
    public void setHighsum(Double  highsum){
        this.highsum = highsum ;
        this.modify("highsum",highsum);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [ISBATCHFLAG]
     */
    public void setIsbatchflag(Integer  isbatchflag){
        this.isbatchflag = isbatchflag ;
        this.modify("isbatchflag",isbatchflag);
    }

    /**
     * 设置 [CHECKMETHOD]
     */
    public void setCheckmethod(String  checkmethod){
        this.checkmethod = checkmethod ;
        this.modify("checkmethod",checkmethod);
    }

    /**
     * 设置 [ABCTYPE]
     */
    public void setAbctype(String  abctype){
        this.abctype = abctype ;
        this.modify("abctype",abctype);
    }

    /**
     * 设置 [SHFPRICE]
     */
    public void setShfprice(Double  shfprice){
        this.shfprice = shfprice ;
        this.modify("shfprice",shfprice);
    }

    /**
     * 设置 [REPSUM]
     */
    public void setRepsum(Double  repsum){
        this.repsum = repsum ;
        this.modify("repsum",repsum);
    }

    /**
     * 设置 [ISNEW]
     */
    public void setIsnew(String  isnew){
        this.isnew = isnew ;
        this.modify("isnew",isnew);
    }

    /**
     * 设置 [ITEMNID]
     */
    public void setItemnid(String  itemnid){
        this.itemnid = itemnid ;
        this.modify("itemnid",itemnid);
    }

    /**
     * 设置 [ITEMSERIALCODE]
     */
    public void setItemserialcode(String  itemserialcode){
        this.itemserialcode = itemserialcode ;
        this.modify("itemserialcode",itemserialcode);
    }

    /**
     * 设置 [REGISTERDAT]
     */
    public void setRegisterdat(Timestamp  registerdat){
        this.registerdat = registerdat ;
        this.modify("registerdat",registerdat);
    }

    /**
     * 设置 [ITEMMODELCODE]
     */
    public void setItemmodelcode(String  itemmodelcode){
        this.itemmodelcode = itemmodelcode ;
        this.modify("itemmodelcode",itemmodelcode);
    }

    /**
     * 设置 [ITEMNCODE]
     */
    public void setItemncode(String  itemncode){
        this.itemncode = itemncode ;
        this.modify("itemncode",itemncode);
    }

    /**
     * 设置 [ITEMCODE]
     */
    public void setItemcode(String  itemcode){
        this.itemcode = itemcode ;
        this.modify("itemcode",itemcode);
    }

    /**
     * 设置 [NO3Q]
     */
    public void setNo3q(Integer  no3q){
        this.no3q = no3q ;
        this.modify("no3q",no3q);
    }

    /**
     * 设置 [EMITEMNAME]
     */
    public void setEmitemname(String  emitemname){
        this.emitemname = emitemname ;
        this.modify("emitemname",emitemname);
    }

    /**
     * 设置 [DENS]
     */
    public void setDens(Double  dens){
        this.dens = dens ;
        this.modify("dens",dens);
    }

    /**
     * 设置 [SAPCONTROL]
     */
    public void setSapcontrol(Integer  sapcontrol){
        this.sapcontrol = sapcontrol ;
        this.modify("sapcontrol",sapcontrol);
    }

    /**
     * 设置 [BATCHTYPE]
     */
    public void setBatchtype(String  batchtype){
        this.batchtype = batchtype ;
        this.modify("batchtype",batchtype);
    }

    /**
     * 设置 [ITEMGROUP]
     */
    public void setItemgroup(Integer  itemgroup){
        this.itemgroup = itemgroup ;
        this.modify("itemgroup",itemgroup);
    }

    /**
     * 设置 [LASTSUM]
     */
    public void setLastsum(Double  lastsum){
        this.lastsum = lastsum ;
        this.modify("lastsum",lastsum);
    }

    /**
     * 设置 [STOCKSUM]
     */
    public void setStocksum(Double  stocksum){
        this.stocksum = stocksum ;
        this.modify("stocksum",stocksum);
    }

    /**
     * 设置 [LASTINDATE]
     */
    public void setLastindate(Timestamp  lastindate){
        this.lastindate = lastindate ;
        this.modify("lastindate",lastindate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [SAPCONTROLCODE]
     */
    public void setSapcontrolcode(String  sapcontrolcode){
        this.sapcontrolcode = sapcontrolcode ;
        this.modify("sapcontrolcode",sapcontrolcode);
    }

    /**
     * 设置 [LIFE]
     */
    public void setLife(Integer  life){
        this.life = life ;
        this.modify("life",life);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [COSTCENTERID]
     */
    public void setCostcenterid(String  costcenterid){
        this.costcenterid = costcenterid ;
        this.modify("costcenterid",costcenterid);
    }

    /**
     * 设置 [STOCKINL]
     */
    public void setStockinl(Double  stockinl){
        this.stockinl = stockinl ;
        this.modify("stockinl",stockinl);
    }

    /**
     * 设置 [ITEMDESC]
     */
    public void setItemdesc(String  itemdesc){
        this.itemdesc = itemdesc ;
        this.modify("itemdesc",itemdesc);
    }

    /**
     * 设置 [WARRANTYDAY]
     */
    public void setWarrantyday(Double  warrantyday){
        this.warrantyday = warrantyday ;
        this.modify("warrantyday",warrantyday);
    }

    /**
     * 设置 [EMCABID]
     */
    public void setEmcabid(String  emcabid){
        this.emcabid = emcabid ;
        this.modify("emcabid",emcabid);
    }

    /**
     * 设置 [UNITID]
     */
    public void setUnitid(String  unitid){
        this.unitid = unitid ;
        this.modify("unitid",unitid);
    }

    /**
     * 设置 [STOREPARTID]
     */
    public void setStorepartid(String  storepartid){
        this.storepartid = storepartid ;
        this.modify("storepartid",storepartid);
    }

    /**
     * 设置 [ITEMTYPEID]
     */
    public void setItemtypeid(String  itemtypeid){
        this.itemtypeid = itemtypeid ;
        this.modify("itemtypeid",itemtypeid);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [MSERVICEID]
     */
    public void setMserviceid(String  mserviceid){
        this.mserviceid = mserviceid ;
        this.modify("mserviceid",mserviceid);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [STOREID]
     */
    public void setStoreid(String  storeid){
        this.storeid = storeid ;
        this.modify("storeid",storeid);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [SEMPID]
     */
    public void setSempid(String  sempid){
        this.sempid = sempid ;
        this.modify("sempid",sempid);
    }

    /**
     * 设置 [LASTAEMPID]
     */
    public void setLastaempid(String  lastaempid){
        this.lastaempid = lastaempid ;
        this.modify("lastaempid",lastaempid);
    }

    /**
     * 设置 [LASTAEMPNAME]
     */
    public void setLastaempname(String  lastaempname){
        this.lastaempname = lastaempname ;
        this.modify("lastaempname",lastaempname);
    }


}


