package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMServiceHistDTO]
 */
@Data
public class EMServiceHistDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACCODEDESC]
     *
     */
    @JSONField(name = "accodedesc")
    @JsonProperty("accodedesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accodedesc;

    /**
     * 属性 [RANGE]
     *
     */
    @JSONField(name = "range")
    @JsonProperty("range")
    private Integer range;

    /**
     * 属性 [ZIP]
     *
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String zip;

    /**
     * 属性 [TEL]
     *
     */
    @JSONField(name = "tel")
    @JsonProperty("tel")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tel;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [LSAREAID]
     *
     */
    @JSONField(name = "lsareaid")
    @JsonProperty("lsareaid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lsareaid;

    /**
     * 属性 [PAYWAYDESC]
     *
     */
    @JSONField(name = "paywaydesc")
    @JsonProperty("paywaydesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paywaydesc;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [PRMAN]
     *
     */
    @JSONField(name = "prman")
    @JsonProperty("prman")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String prman;

    /**
     * 属性 [QUALIFICATIONS]
     *
     */
    @JSONField(name = "qualifications")
    @JsonProperty("qualifications")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String qualifications;

    /**
     * 属性 [LABSERVICELEVELID]
     *
     */
    @JSONField(name = "labservicelevelid")
    @JsonProperty("labservicelevelid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicelevelid;

    /**
     * 属性 [TAXCODE]
     *
     */
    @JSONField(name = "taxcode")
    @JsonProperty("taxcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taxcode;

    /**
     * 属性 [SHDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "shdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("shdate")
    private Timestamp shdate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMSERVICEHISTNAME]
     *
     */
    @JSONField(name = "emservicehistname")
    @JsonProperty("emservicehistname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicehistname;

    /**
     * 属性 [ATT]
     *
     */
    @JSONField(name = "att")
    @JsonProperty("att")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String att;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [TAXTYPEID]
     *
     */
    @JSONField(name = "taxtypeid")
    @JsonProperty("taxtypeid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taxtypeid;

    /**
     * 属性 [LABSERVICETYPEID]
     *
     */
    @JSONField(name = "labservicetypeid")
    @JsonProperty("labservicetypeid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicetypeid;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PAYWAY]
     *
     */
    @JSONField(name = "payway")
    @JsonProperty("payway")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String payway;

    /**
     * 属性 [EMSERVICEHISTID]
     *
     */
    @JSONField(name = "emservicehistid")
    @JsonProperty("emservicehistid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emservicehistid;

    /**
     * 属性 [WEBSITE]
     *
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    private String website;

    /**
     * 属性 [TAXDESC]
     *
     */
    @JSONField(name = "taxdesc")
    @JsonProperty("taxdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taxdesc;

    /**
     * 属性 [SERVICESTATE]
     *
     */
    @JSONField(name = "servicestate")
    @JsonProperty("servicestate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String servicestate;

    /**
     * 属性 [SERVICEGROUP]
     *
     */
    @JSONField(name = "servicegroup")
    @JsonProperty("servicegroup")
    @NotNull(message = "[服务商分组]不允许为空!")
    private Integer servicegroup;

    /**
     * 属性 [ADDR]
     *
     */
    @JSONField(name = "addr")
    @JsonProperty("addr")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String addr;

    /**
     * 属性 [ACCODE]
     *
     */
    @JSONField(name = "accode")
    @JsonProperty("accode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accode;

    /**
     * 属性 [QUALITYMANA]
     *
     */
    @JSONField(name = "qualitymana")
    @JsonProperty("qualitymana")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String qualitymana;

    /**
     * 属性 [FAX]
     *
     */
    @JSONField(name = "fax")
    @JsonProperty("fax")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fax;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [SUMS]
     *
     */
    @JSONField(name = "sums")
    @JsonProperty("sums")
    private Integer sums;

    /**
     * 属性 [EMSERVICENAME]
     *
     */
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicename;

    /**
     * 属性 [EMSERVICEID]
     *
     */
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceid;


    /**
     * 设置 [ACCODEDESC]
     */
    public void setAccodedesc(String  accodedesc){
        this.accodedesc = accodedesc ;
        this.modify("accodedesc",accodedesc);
    }

    /**
     * 设置 [RANGE]
     */
    public void setRange(Integer  range){
        this.range = range ;
        this.modify("range",range);
    }

    /**
     * 设置 [ZIP]
     */
    public void setZip(String  zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }

    /**
     * 设置 [TEL]
     */
    public void setTel(String  tel){
        this.tel = tel ;
        this.modify("tel",tel);
    }

    /**
     * 设置 [LSAREAID]
     */
    public void setLsareaid(String  lsareaid){
        this.lsareaid = lsareaid ;
        this.modify("lsareaid",lsareaid);
    }

    /**
     * 设置 [PAYWAYDESC]
     */
    public void setPaywaydesc(String  paywaydesc){
        this.paywaydesc = paywaydesc ;
        this.modify("paywaydesc",paywaydesc);
    }

    /**
     * 设置 [PRMAN]
     */
    public void setPrman(String  prman){
        this.prman = prman ;
        this.modify("prman",prman);
    }

    /**
     * 设置 [QUALIFICATIONS]
     */
    public void setQualifications(String  qualifications){
        this.qualifications = qualifications ;
        this.modify("qualifications",qualifications);
    }

    /**
     * 设置 [LABSERVICELEVELID]
     */
    public void setLabservicelevelid(String  labservicelevelid){
        this.labservicelevelid = labservicelevelid ;
        this.modify("labservicelevelid",labservicelevelid);
    }

    /**
     * 设置 [TAXCODE]
     */
    public void setTaxcode(String  taxcode){
        this.taxcode = taxcode ;
        this.modify("taxcode",taxcode);
    }

    /**
     * 设置 [SHDATE]
     */
    public void setShdate(Timestamp  shdate){
        this.shdate = shdate ;
        this.modify("shdate",shdate);
    }

    /**
     * 设置 [EMSERVICEHISTNAME]
     */
    public void setEmservicehistname(String  emservicehistname){
        this.emservicehistname = emservicehistname ;
        this.modify("emservicehistname",emservicehistname);
    }

    /**
     * 设置 [ATT]
     */
    public void setAtt(String  att){
        this.att = att ;
        this.modify("att",att);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [TAXTYPEID]
     */
    public void setTaxtypeid(String  taxtypeid){
        this.taxtypeid = taxtypeid ;
        this.modify("taxtypeid",taxtypeid);
    }

    /**
     * 设置 [LABSERVICETYPEID]
     */
    public void setLabservicetypeid(String  labservicetypeid){
        this.labservicetypeid = labservicetypeid ;
        this.modify("labservicetypeid",labservicetypeid);
    }

    /**
     * 设置 [PAYWAY]
     */
    public void setPayway(String  payway){
        this.payway = payway ;
        this.modify("payway",payway);
    }

    /**
     * 设置 [WEBSITE]
     */
    public void setWebsite(String  website){
        this.website = website ;
        this.modify("website",website);
    }

    /**
     * 设置 [TAXDESC]
     */
    public void setTaxdesc(String  taxdesc){
        this.taxdesc = taxdesc ;
        this.modify("taxdesc",taxdesc);
    }

    /**
     * 设置 [SERVICESTATE]
     */
    public void setServicestate(String  servicestate){
        this.servicestate = servicestate ;
        this.modify("servicestate",servicestate);
    }

    /**
     * 设置 [SERVICEGROUP]
     */
    public void setServicegroup(Integer  servicegroup){
        this.servicegroup = servicegroup ;
        this.modify("servicegroup",servicegroup);
    }

    /**
     * 设置 [ADDR]
     */
    public void setAddr(String  addr){
        this.addr = addr ;
        this.modify("addr",addr);
    }

    /**
     * 设置 [ACCODE]
     */
    public void setAccode(String  accode){
        this.accode = accode ;
        this.modify("accode",accode);
    }

    /**
     * 设置 [QUALITYMANA]
     */
    public void setQualitymana(String  qualitymana){
        this.qualitymana = qualitymana ;
        this.modify("qualitymana",qualitymana);
    }

    /**
     * 设置 [FAX]
     */
    public void setFax(String  fax){
        this.fax = fax ;
        this.modify("fax",fax);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [EMSERVICEID]
     */
    public void setEmserviceid(String  emserviceid){
        this.emserviceid = emserviceid ;
        this.modify("emserviceid",emserviceid);
    }


}


