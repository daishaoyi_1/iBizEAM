package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEIGSJRBDTO]
 */
@Data
public class EMEIGSJRBDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMEIGSJRBNAME]
     *
     */
    @JSONField(name = "emeigsjrbname")
    @JsonProperty("emeigsjrbname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeigsjrbname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMEIGSJRBID]
     *
     */
    @JSONField(name = "emeigsjrbid")
    @JsonProperty("emeigsjrbid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeigsjrbid;

    /**
     * 属性 [LDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "ldate" , format="yyyy-MM-dd")
    @JsonProperty("ldate")
    private Timestamp ldate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [DISCNT]
     *
     */
    @JSONField(name = "discnt")
    @JsonProperty("discnt")
    private Double discnt;

    /**
     * 属性 [LASTSTOCKCNT]
     *
     */
    @JSONField(name = "laststockcnt")
    @JsonProperty("laststockcnt")
    @NotNull(message = "[上期存量]不允许为空!")
    private Double laststockcnt;

    /**
     * 属性 [LCNT]
     *
     */
    @JSONField(name = "lcnt")
    @JsonProperty("lcnt")
    private Integer lcnt;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [BEGINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "begindate" , format="yyyy-MM-dd")
    @JsonProperty("begindate")
    @NotNull(message = "[日期]不允许为空!")
    private Timestamp begindate;

    /**
     * 属性 [HDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "hdate" , format="yyyy-MM-dd")
    @JsonProperty("hdate")
    private Timestamp hdate;

    /**
     * 属性 [ENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enddate" , format="yyyy-MM-dd")
    @JsonProperty("enddate")
    @NotNull(message = "[结束日期]不允许为空!")
    private Timestamp enddate;

    /**
     * 属性 [WCNT]
     *
     */
    @JSONField(name = "wcnt")
    @JsonProperty("wcnt")
    private Integer wcnt;

    /**
     * 属性 [STOCKCNT]
     *
     */
    @JSONField(name = "stockcnt")
    @JsonProperty("stockcnt")
    @NotNull(message = "[本期存量]不允许为空!")
    private Double stockcnt;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [GSJRBDESC]
     *
     */
    @JSONField(name = "gsjrbdesc")
    @JsonProperty("gsjrbdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String gsjrbdesc;

    /**
     * 属性 [BCNT]
     *
     */
    @JSONField(name = "bcnt")
    @JsonProperty("bcnt")
    private Integer bcnt;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [MODELCODE]
     *
     */
    @JSONField(name = "modelcode")
    @JsonProperty("modelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String modelcode;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;


    /**
     * 设置 [EMEIGSJRBNAME]
     */
    public void setEmeigsjrbname(String  emeigsjrbname){
        this.emeigsjrbname = emeigsjrbname ;
        this.modify("emeigsjrbname",emeigsjrbname);
    }

    /**
     * 设置 [LDATE]
     */
    public void setLdate(Timestamp  ldate){
        this.ldate = ldate ;
        this.modify("ldate",ldate);
    }

    /**
     * 设置 [DISCNT]
     */
    public void setDiscnt(Double  discnt){
        this.discnt = discnt ;
        this.modify("discnt",discnt);
    }

    /**
     * 设置 [LASTSTOCKCNT]
     */
    public void setLaststockcnt(Double  laststockcnt){
        this.laststockcnt = laststockcnt ;
        this.modify("laststockcnt",laststockcnt);
    }

    /**
     * 设置 [LCNT]
     */
    public void setLcnt(Integer  lcnt){
        this.lcnt = lcnt ;
        this.modify("lcnt",lcnt);
    }

    /**
     * 设置 [BEGINDATE]
     */
    public void setBegindate(Timestamp  begindate){
        this.begindate = begindate ;
        this.modify("begindate",begindate);
    }

    /**
     * 设置 [HDATE]
     */
    public void setHdate(Timestamp  hdate){
        this.hdate = hdate ;
        this.modify("hdate",hdate);
    }

    /**
     * 设置 [ENDDATE]
     */
    public void setEnddate(Timestamp  enddate){
        this.enddate = enddate ;
        this.modify("enddate",enddate);
    }

    /**
     * 设置 [WCNT]
     */
    public void setWcnt(Integer  wcnt){
        this.wcnt = wcnt ;
        this.modify("wcnt",wcnt);
    }

    /**
     * 设置 [STOCKCNT]
     */
    public void setStockcnt(Double  stockcnt){
        this.stockcnt = stockcnt ;
        this.modify("stockcnt",stockcnt);
    }

    /**
     * 设置 [GSJRBDESC]
     */
    public void setGsjrbdesc(String  gsjrbdesc){
        this.gsjrbdesc = gsjrbdesc ;
        this.modify("gsjrbdesc",gsjrbdesc);
    }

    /**
     * 设置 [BCNT]
     */
    public void setBcnt(Integer  bcnt){
        this.bcnt = bcnt ;
        this.modify("bcnt",bcnt);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }


}


