package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMApply;
import cn.ibizlab.eam.webapi.dto.EMApplyDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMApplyMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMApplyMapping extends MappingBase<EMApplyDTO, EMApply> {


}

