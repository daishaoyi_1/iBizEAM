package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMApplyDTO]
 */
@Data
public class EMApplyDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ENTRUSTLIST]
     *
     */
    @JSONField(name = "entrustlist")
    @JsonProperty("entrustlist")
    @NotBlank(message = "[外委类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String entrustlist;

    /**
     * 属性 [APPLYDESC]
     *
     */
    @JSONField(name = "applydesc")
    @JsonProperty("applydesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String applydesc;

    /**
     * 属性 [APPLYSTATE]
     *
     */
    @JSONField(name = "applystate")
    @JsonProperty("applystate")
    private Integer applystate;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [DPDESC]
     *
     */
    @JSONField(name = "dpdesc")
    @JsonProperty("dpdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String dpdesc;

    /**
     * 属性 [MFEE]
     *
     */
    @JSONField(name = "mfee")
    @JsonProperty("mfee")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mfee;

    /**
     * 属性 [APPLYINFO]
     *
     */
    @JSONField(name = "applyinfo")
    @JsonProperty("applyinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String applyinfo;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [SHUIFEI]
     *
     */
    @JSONField(name = "shuifei")
    @JsonProperty("shuifei")
    private Double shuifei;

    /**
     * 属性 [SFEE]
     *
     */
    @JSONField(name = "sfee")
    @JsonProperty("sfee")
    private Double sfee;

    /**
     * 属性 [EMAPPLYNAME]
     *
     */
    @JSONField(name = "emapplyname")
    @JsonProperty("emapplyname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emapplyname;

    /**
     * 属性 [APPLYTYPE]
     *
     */
    @JSONField(name = "applytype")
    @JsonProperty("applytype")
    @NotBlank(message = "[申请类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String applytype;

    /**
     * 属性 [APPLYEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "applyedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("applyedate")
    private Timestamp applyedate;

    /**
     * 属性 [MPERSONID]
     *
     */
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mpersonid;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [PFEE]
     *
     */
    @JSONField(name = "pfee")
    @JsonProperty("pfee")
    private Double pfee;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [PREFEE1]
     *
     */
    @JSONField(name = "prefee1")
    @JsonProperty("prefee1")
    private Double prefee1;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [FP]
     *
     */
    @JSONField(name = "fp")
    @JsonProperty("fp")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fp;

    /**
     * 属性 [ZFY]
     *
     */
    @JSONField(name = "zfy")
    @JsonProperty("zfy")
    private Double zfy;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [APPLYBDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "applybdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("applybdate")
    private Timestamp applybdate;

    /**
     * 属性 [PREFEE]
     *
     */
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    private Double prefee;

    /**
     * 属性 [CLOSEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "closedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("closedate")
    private Timestamp closedate;

    /**
     * 属性 [EMAPPLYID]
     *
     */
    @JSONField(name = "emapplyid")
    @JsonProperty("emapplyid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emapplyid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [PLANTYPE]
     *
     */
    @JSONField(name = "plantype")
    @JsonProperty("plantype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String plantype;

    /**
     * 属性 [CLOSEEMPID]
     *
     */
    @JSONField(name = "closeempid")
    @JsonProperty("closeempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String closeempid;

    /**
     * 属性 [APPLYDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "applydate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("applydate")
    @NotNull(message = "[申请日期]不允许为空!")
    private Timestamp applydate;

    /**
     * 属性 [RDEPTNAME]
     *
     */
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rdeptname;

    /**
     * 属性 [MPERSONNAME]
     *
     */
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mpersonname;

    /**
     * 属性 [CLOSEEMPNAME]
     *
     */
    @JSONField(name = "closeempname")
    @JsonProperty("closeempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String closeempname;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [INVOICEATTACH]
     *
     */
    @JSONField(name = "invoiceattach")
    @JsonProperty("invoiceattach")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceattach;

    /**
     * 属性 [SPYJ]
     *
     */
    @JSONField(name = "spyj")
    @JsonProperty("spyj")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String spyj;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String priority;

    /**
     * 属性 [RDEPTID]
     *
     */
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rdeptid;

    /**
     * 属性 [RFOMONAME]
     *
     */
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfomoname;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rservicename;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [RFODENAME]
     *
     */
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfodename;

    /**
     * 属性 [RFOACNAME]
     *
     */
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfoacname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [RTEAMNAME]
     *
     */
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rteamname;

    /**
     * 属性 [RFOCANAME]
     *
     */
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rfocaname;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [RTEAMID]
     *
     */
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rteamid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [RFOACID]
     *
     */
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfoacid;

    /**
     * 属性 [RFOMOID]
     *
     */
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfomoid;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rserviceid;

    /**
     * 属性 [RFOCAID]
     *
     */
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfocaid;

    /**
     * 属性 [RFODEID]
     *
     */
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rfodeid;


    /**
     * 设置 [ENTRUSTLIST]
     */
    public void setEntrustlist(String  entrustlist){
        this.entrustlist = entrustlist ;
        this.modify("entrustlist",entrustlist);
    }

    /**
     * 设置 [APPLYDESC]
     */
    public void setApplydesc(String  applydesc){
        this.applydesc = applydesc ;
        this.modify("applydesc",applydesc);
    }

    /**
     * 设置 [APPLYSTATE]
     */
    public void setApplystate(Integer  applystate){
        this.applystate = applystate ;
        this.modify("applystate",applystate);
    }

    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [DPDESC]
     */
    public void setDpdesc(String  dpdesc){
        this.dpdesc = dpdesc ;
        this.modify("dpdesc",dpdesc);
    }

    /**
     * 设置 [MFEE]
     */
    public void setMfee(String  mfee){
        this.mfee = mfee ;
        this.modify("mfee",mfee);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [SHUIFEI]
     */
    public void setShuifei(Double  shuifei){
        this.shuifei = shuifei ;
        this.modify("shuifei",shuifei);
    }

    /**
     * 设置 [SFEE]
     */
    public void setSfee(Double  sfee){
        this.sfee = sfee ;
        this.modify("sfee",sfee);
    }

    /**
     * 设置 [EMAPPLYNAME]
     */
    public void setEmapplyname(String  emapplyname){
        this.emapplyname = emapplyname ;
        this.modify("emapplyname",emapplyname);
    }

    /**
     * 设置 [APPLYTYPE]
     */
    public void setApplytype(String  applytype){
        this.applytype = applytype ;
        this.modify("applytype",applytype);
    }

    /**
     * 设置 [APPLYEDATE]
     */
    public void setApplyedate(Timestamp  applyedate){
        this.applyedate = applyedate ;
        this.modify("applyedate",applyedate);
    }

    /**
     * 设置 [MPERSONID]
     */
    public void setMpersonid(String  mpersonid){
        this.mpersonid = mpersonid ;
        this.modify("mpersonid",mpersonid);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PFEE]
     */
    public void setPfee(Double  pfee){
        this.pfee = pfee ;
        this.modify("pfee",pfee);
    }

    /**
     * 设置 [PREFEE1]
     */
    public void setPrefee1(Double  prefee1){
        this.prefee1 = prefee1 ;
        this.modify("prefee1",prefee1);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [FP]
     */
    public void setFp(String  fp){
        this.fp = fp ;
        this.modify("fp",fp);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [APPLYBDATE]
     */
    public void setApplybdate(Timestamp  applybdate){
        this.applybdate = applybdate ;
        this.modify("applybdate",applybdate);
    }

    /**
     * 设置 [PREFEE]
     */
    public void setPrefee(Double  prefee){
        this.prefee = prefee ;
        this.modify("prefee",prefee);
    }

    /**
     * 设置 [CLOSEDATE]
     */
    public void setClosedate(Timestamp  closedate){
        this.closedate = closedate ;
        this.modify("closedate",closedate);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [PLANTYPE]
     */
    public void setPlantype(String  plantype){
        this.plantype = plantype ;
        this.modify("plantype",plantype);
    }

    /**
     * 设置 [CLOSEEMPID]
     */
    public void setCloseempid(String  closeempid){
        this.closeempid = closeempid ;
        this.modify("closeempid",closeempid);
    }

    /**
     * 设置 [APPLYDATE]
     */
    public void setApplydate(Timestamp  applydate){
        this.applydate = applydate ;
        this.modify("applydate",applydate);
    }

    /**
     * 设置 [RDEPTNAME]
     */
    public void setRdeptname(String  rdeptname){
        this.rdeptname = rdeptname ;
        this.modify("rdeptname",rdeptname);
    }

    /**
     * 设置 [MPERSONNAME]
     */
    public void setMpersonname(String  mpersonname){
        this.mpersonname = mpersonname ;
        this.modify("mpersonname",mpersonname);
    }

    /**
     * 设置 [CLOSEEMPNAME]
     */
    public void setCloseempname(String  closeempname){
        this.closeempname = closeempname ;
        this.modify("closeempname",closeempname);
    }

    /**
     * 设置 [INVOICEATTACH]
     */
    public void setInvoiceattach(String  invoiceattach){
        this.invoiceattach = invoiceattach ;
        this.modify("invoiceattach",invoiceattach);
    }

    /**
     * 设置 [SPYJ]
     */
    public void setSpyj(String  spyj){
        this.spyj = spyj ;
        this.modify("spyj",spyj);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [RDEPTID]
     */
    public void setRdeptid(String  rdeptid){
        this.rdeptid = rdeptid ;
        this.modify("rdeptid",rdeptid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [RTEAMID]
     */
    public void setRteamid(String  rteamid){
        this.rteamid = rteamid ;
        this.modify("rteamid",rteamid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [RFOACID]
     */
    public void setRfoacid(String  rfoacid){
        this.rfoacid = rfoacid ;
        this.modify("rfoacid",rfoacid);
    }

    /**
     * 设置 [RFOMOID]
     */
    public void setRfomoid(String  rfomoid){
        this.rfomoid = rfomoid ;
        this.modify("rfomoid",rfomoid);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [RFOCAID]
     */
    public void setRfocaid(String  rfocaid){
        this.rfocaid = rfocaid ;
        this.modify("rfocaid",rfocaid);
    }

    /**
     * 设置 [RFODEID]
     */
    public void setRfodeid(String  rfodeid){
        this.rfodeid = rfodeid ;
        this.modify("rfodeid",rfodeid);
    }


}


