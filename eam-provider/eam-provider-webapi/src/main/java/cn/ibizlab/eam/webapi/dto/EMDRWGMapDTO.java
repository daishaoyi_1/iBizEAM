package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMDRWGMapDTO]
 */
@Data
public class EMDRWGMapDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMDRWGMAPNAME]
     *
     */
    @JSONField(name = "emdrwgmapname")
    @JsonProperty("emdrwgmapname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emdrwgmapname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMDRWGMAPID]
     *
     */
    @JSONField(name = "emdrwgmapid")
    @JsonProperty("emdrwgmapid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emdrwgmapid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [REFOBJNAME]
     *
     */
    @JSONField(name = "refobjname")
    @JsonProperty("refobjname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String refobjname;

    /**
     * 属性 [DRWGNAME]
     *
     */
    @JSONField(name = "drwgname")
    @JsonProperty("drwgname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String drwgname;

    /**
     * 属性 [REFOBJID]
     *
     */
    @JSONField(name = "refobjid")
    @JsonProperty("refobjid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String refobjid;

    /**
     * 属性 [DRWGID]
     *
     */
    @JSONField(name = "drwgid")
    @JsonProperty("drwgid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String drwgid;


    /**
     * 设置 [EMDRWGMAPNAME]
     */
    public void setEmdrwgmapname(String  emdrwgmapname){
        this.emdrwgmapname = emdrwgmapname ;
        this.modify("emdrwgmapname",emdrwgmapname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [REFOBJID]
     */
    public void setRefobjid(String  refobjid){
        this.refobjid = refobjid ;
        this.modify("refobjid",refobjid);
    }

    /**
     * 设置 [DRWGID]
     */
    public void setDrwgid(String  drwgid){
        this.drwgid = drwgid ;
        this.modify("drwgid",drwgid);
    }


}


