package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[PLANSCHEDULEDTO]
 */
@Data
public class PLANSCHEDULEDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PLANSCHEDULEID]
     *
     */
    @JSONField(name = "planscheduleid")
    @JsonProperty("planscheduleid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String planscheduleid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [PLANSCHEDULENAME]
     *
     */
    @JSONField(name = "planschedulename")
    @JsonProperty("planschedulename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String planschedulename;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [SCHEDULETYPE]
     *
     */
    @JSONField(name = "scheduletype")
    @JsonProperty("scheduletype")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String scheduletype;

    /**
     * 属性 [CYCLESTARTTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "cyclestarttime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cyclestarttime")
    private Timestamp cyclestarttime;

    /**
     * 属性 [CYCLEENDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "cycleendtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cycleendtime")
    private Timestamp cycleendtime;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [INTERVALMINUTE]
     *
     */
    @JSONField(name = "intervalminute")
    @JsonProperty("intervalminute")
    private Integer intervalminute;

    /**
     * 属性 [RUNDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rundate" , format="yyyy-MM-dd")
    @JsonProperty("rundate")
    private Timestamp rundate;

    /**
     * 属性 [RUNTIME]
     *
     */
    @JsonFormat(pattern="HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "runtime" , format="HH:mm:ss")
    @JsonProperty("runtime")
    private Timestamp runtime;

    /**
     * 属性 [SCHEDULEPARAM]
     *
     */
    @JSONField(name = "scheduleparam")
    @JsonProperty("scheduleparam")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam;

    /**
     * 属性 [SCHEDULEPARAM2]
     *
     */
    @JSONField(name = "scheduleparam2")
    @JsonProperty("scheduleparam2")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam2;

    /**
     * 属性 [SCHEDULEPARAM3]
     *
     */
    @JSONField(name = "scheduleparam3")
    @JsonProperty("scheduleparam3")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam3;

    /**
     * 属性 [SCHEDULEPARAM4]
     *
     */
    @JSONField(name = "scheduleparam4")
    @JsonProperty("scheduleparam4")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam4;

    /**
     * 属性 [SCHEDULESTATE]
     *
     */
    @JSONField(name = "schedulestate")
    @JsonProperty("schedulestate")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String schedulestate;

    /**
     * 属性 [EMPLANID]
     *
     */
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emplanid;

    /**
     * 属性 [EMPLANNAME]
     *
     */
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emplanname;

    /**
     * 属性 [LASTMINUTE]
     *
     */
    @JSONField(name = "lastminute")
    @JsonProperty("lastminute")
    private Integer lastminute;

    /**
     * 属性 [TASKID]
     *
     */
    @JSONField(name = "taskid")
    @JsonProperty("taskid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String taskid;


    /**
     * 设置 [PLANSCHEDULENAME]
     */
    public void setPlanschedulename(String  planschedulename){
        this.planschedulename = planschedulename ;
        this.modify("planschedulename",planschedulename);
    }

    /**
     * 设置 [SCHEDULETYPE]
     */
    public void setScheduletype(String  scheduletype){
        this.scheduletype = scheduletype ;
        this.modify("scheduletype",scheduletype);
    }

    /**
     * 设置 [CYCLESTARTTIME]
     */
    public void setCyclestarttime(Timestamp  cyclestarttime){
        this.cyclestarttime = cyclestarttime ;
        this.modify("cyclestarttime",cyclestarttime);
    }

    /**
     * 设置 [CYCLEENDTIME]
     */
    public void setCycleendtime(Timestamp  cycleendtime){
        this.cycleendtime = cycleendtime ;
        this.modify("cycleendtime",cycleendtime);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [INTERVALMINUTE]
     */
    public void setIntervalminute(Integer  intervalminute){
        this.intervalminute = intervalminute ;
        this.modify("intervalminute",intervalminute);
    }

    /**
     * 设置 [RUNDATE]
     */
    public void setRundate(Timestamp  rundate){
        this.rundate = rundate ;
        this.modify("rundate",rundate);
    }

    /**
     * 设置 [RUNTIME]
     */
    public void setRuntime(Timestamp  runtime){
        this.runtime = runtime ;
        this.modify("runtime",runtime);
    }

    /**
     * 设置 [SCHEDULEPARAM]
     */
    public void setScheduleparam(String  scheduleparam){
        this.scheduleparam = scheduleparam ;
        this.modify("scheduleparam",scheduleparam);
    }

    /**
     * 设置 [SCHEDULEPARAM2]
     */
    public void setScheduleparam2(String  scheduleparam2){
        this.scheduleparam2 = scheduleparam2 ;
        this.modify("scheduleparam2",scheduleparam2);
    }

    /**
     * 设置 [SCHEDULEPARAM3]
     */
    public void setScheduleparam3(String  scheduleparam3){
        this.scheduleparam3 = scheduleparam3 ;
        this.modify("scheduleparam3",scheduleparam3);
    }

    /**
     * 设置 [SCHEDULEPARAM4]
     */
    public void setScheduleparam4(String  scheduleparam4){
        this.scheduleparam4 = scheduleparam4 ;
        this.modify("scheduleparam4",scheduleparam4);
    }

    /**
     * 设置 [SCHEDULESTATE]
     */
    public void setSchedulestate(String  schedulestate){
        this.schedulestate = schedulestate ;
        this.modify("schedulestate",schedulestate);
    }

    /**
     * 设置 [EMPLANID]
     */
    public void setEmplanid(String  emplanid){
        this.emplanid = emplanid ;
        this.modify("emplanid",emplanid);
    }

    /**
     * 设置 [LASTMINUTE]
     */
    public void setLastminute(Integer  lastminute){
        this.lastminute = lastminute ;
        this.modify("lastminute",lastminute);
    }

    /**
     * 设置 [TASKID]
     */
    public void setTaskid(String  taskid){
        this.taskid = taskid ;
        this.modify("taskid",taskid);
    }


}


