package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMAssetClassDTO]
 */
@Data
public class EMAssetClassDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ASSETCLASSCODE]
     *
     */
    @JSONField(name = "assetclasscode")
    @JsonProperty("assetclasscode")
    @NotBlank(message = "[资产科目代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclasscode;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [RESERVER]
     *
     */
    @JSONField(name = "reserver")
    @JsonProperty("reserver")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String reserver;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [RESERVER2]
     *
     */
    @JSONField(name = "reserver2")
    @JsonProperty("reserver2")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String reserver2;

    /**
     * 属性 [EMASSETCLASSNAME]
     *
     */
    @JSONField(name = "emassetclassname")
    @JsonProperty("emassetclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassetclassname;

    /**
     * 属性 [ASSETCLASSGROUP]
     *
     */
    @JSONField(name = "assetclassgroup")
    @JsonProperty("assetclassgroup")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclassgroup;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMASSETCLASSID]
     *
     */
    @JSONField(name = "emassetclassid")
    @JsonProperty("emassetclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassetclassid;

    /**
     * 属性 [LIFE]
     *
     */
    @JSONField(name = "life")
    @JsonProperty("life")
    private Double life;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ASSETCLASSINFO]
     *
     */
    @JSONField(name = "assetclassinfo")
    @JsonProperty("assetclassinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetclassinfo;

    /**
     * 属性 [ASSETCLASSPNAME]
     *
     */
    @JSONField(name = "assetclasspname")
    @JsonProperty("assetclasspname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetclasspname;

    /**
     * 属性 [ASSETCLASSPCODE]
     *
     */
    @JSONField(name = "assetclasspcode")
    @JsonProperty("assetclasspcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclasspcode;

    /**
     * 属性 [ASSETCLASSPID]
     *
     */
    @JSONField(name = "assetclasspid")
    @JsonProperty("assetclasspid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclasspid;


    /**
     * 设置 [ASSETCLASSCODE]
     */
    public void setAssetclasscode(String  assetclasscode){
        this.assetclasscode = assetclasscode ;
        this.modify("assetclasscode",assetclasscode);
    }

    /**
     * 设置 [RESERVER]
     */
    public void setReserver(String  reserver){
        this.reserver = reserver ;
        this.modify("reserver",reserver);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [RESERVER2]
     */
    public void setReserver2(String  reserver2){
        this.reserver2 = reserver2 ;
        this.modify("reserver2",reserver2);
    }

    /**
     * 设置 [EMASSETCLASSNAME]
     */
    public void setEmassetclassname(String  emassetclassname){
        this.emassetclassname = emassetclassname ;
        this.modify("emassetclassname",emassetclassname);
    }

    /**
     * 设置 [ASSETCLASSGROUP]
     */
    public void setAssetclassgroup(String  assetclassgroup){
        this.assetclassgroup = assetclassgroup ;
        this.modify("assetclassgroup",assetclassgroup);
    }

    /**
     * 设置 [LIFE]
     */
    public void setLife(Double  life){
        this.life = life ;
        this.modify("life",life);
    }

    /**
     * 设置 [ASSETCLASSPID]
     */
    public void setAssetclasspid(String  assetclasspid){
        this.assetclasspid = assetclasspid ;
        this.modify("assetclasspid",assetclasspid);
    }


}


