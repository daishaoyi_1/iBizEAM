package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMItemRInDTO]
 */
@Data
public class EMItemRInDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMITEMRINNAME]
     *
     */
    @JSONField(name = "emitemrinname")
    @JsonProperty("emitemrinname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitemrinname;

    /**
     * 属性 [SAPREASON1]
     *
     */
    @JSONField(name = "sapreason1")
    @JsonProperty("sapreason1")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sapreason1;

    /**
     * 属性 [PSUM]
     *
     */
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [ITEMRININFO]
     *
     */
    @JSONField(name = "itemrininfo")
    @JsonProperty("itemrininfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemrininfo;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [RIN_PSUM]
     *
     */
    @JSONField(name = "rin_psum")
    @JsonProperty("rin_psum")
    private Double rinPsum;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMITEMRINID]
     *
     */
    @JSONField(name = "emitemrinid")
    @JsonProperty("emitemrinid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitemrinid;

    /**
     * 属性 [BATCODE]
     *
     */
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    @NotBlank(message = "[批次]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String batcode;

    /**
     * 属性 [RINSTATE]
     *
     */
    @JSONField(name = "rinstate")
    @JsonProperty("rinstate")
    private Integer rinstate;

    /**
     * 属性 [SAPREASON]
     *
     */
    @JSONField(name = "sapreason")
    @JsonProperty("sapreason")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String sapreason;

    /**
     * 属性 [SUMALL]
     *
     */
    @JSONField(name = "sumall")
    @JsonProperty("sumall")
    private Double sumall;

    /**
     * 属性 [SAP]
     *
     */
    @JSONField(name = "sap")
    @JsonProperty("sap")
    private Integer sap;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [SAPSM]
     *
     */
    @JSONField(name = "sapsm")
    @JsonProperty("sapsm")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sapsm;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [SDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    private Timestamp sdate;

    /**
     * 属性 [SAPCONTROL]
     *
     */
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    private Integer sapcontrol;

    /**
     * 属性 [STOREPARTNAME]
     *
     */
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storepartname;

    /**
     * 属性 [ITEMCODE]
     *
     */
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemcode;

    /**
     * 属性 [TEAMID]
     *
     */
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String teamid;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitid;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [CIVO]
     *
     */
    @JSONField(name = "civo")
    @JsonProperty("civo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String civo;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [POID]
     *
     */
    @JSONField(name = "poid")
    @JsonProperty("poid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String poid;

    /**
     * 属性 [AVGTSFEE]
     *
     */
    @JSONField(name = "avgtsfee")
    @JsonProperty("avgtsfee")
    private Double avgtsfee;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [WPLISTID]
     *
     */
    @JSONField(name = "wplistid")
    @JsonProperty("wplistid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wplistid;

    /**
     * 属性 [PODETAILNAME]
     *
     */
    @JSONField(name = "podetailname")
    @JsonProperty("podetailname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String podetailname;

    /**
     * 属性 [ITEMTYPENAME]
     *
     */
    @JSONField(name = "itemtypename")
    @JsonProperty("itemtypename")
    @Size(min = 0, max = 400, message = "内容长度必须小于等于[400]")
    private String itemtypename;

    /**
     * 属性 [AVGPRICE]
     *
     */
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    private Double avgprice;

    /**
     * 属性 [EMSERVICENAME]
     *
     */
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emservicename;

    /**
     * 属性 [SHF]
     *
     */
    @JSONField(name = "shf")
    @JsonProperty("shf")
    private Double shf;

    /**
     * 属性 [STORENAME]
     *
     */
    @JSONField(name = "storename")
    @JsonProperty("storename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storename;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itembtypeid;

    /**
     * 属性 [EMSERVICEID]
     *
     */
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceid;

    /**
     * 属性 [PODETAILID]
     *
     */
    @JSONField(name = "podetailid")
    @JsonProperty("podetailid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String podetailid;

    /**
     * 属性 [STOREID]
     *
     */
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storeid;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [STOREPARTID]
     *
     */
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storepartid;

    /**
     * 属性 [POREMPNAME]
     *
     */
    @JSONField(name = "porempname")
    @JsonProperty("porempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String porempname;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empname;

    /**
     * 属性 [SEMPID]
     *
     */
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sempid;

    /**
     * 属性 [SEMPNAME]
     *
     */
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sempname;


    /**
     * 设置 [EMITEMRINNAME]
     */
    public void setEmitemrinname(String  emitemrinname){
        this.emitemrinname = emitemrinname ;
        this.modify("emitemrinname",emitemrinname);
    }

    /**
     * 设置 [SAPREASON1]
     */
    public void setSapreason1(String  sapreason1){
        this.sapreason1 = sapreason1 ;
        this.modify("sapreason1",sapreason1);
    }

    /**
     * 设置 [PSUM]
     */
    public void setPsum(Double  psum){
        this.psum = psum ;
        this.modify("psum",psum);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [BATCODE]
     */
    public void setBatcode(String  batcode){
        this.batcode = batcode ;
        this.modify("batcode",batcode);
    }

    /**
     * 设置 [RINSTATE]
     */
    public void setRinstate(Integer  rinstate){
        this.rinstate = rinstate ;
        this.modify("rinstate",rinstate);
    }

    /**
     * 设置 [SAPREASON]
     */
    public void setSapreason(String  sapreason){
        this.sapreason = sapreason ;
        this.modify("sapreason",sapreason);
    }

    /**
     * 设置 [SAP]
     */
    public void setSap(Integer  sap){
        this.sap = sap ;
        this.modify("sap",sap);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [SAPSM]
     */
    public void setSapsm(String  sapsm){
        this.sapsm = sapsm ;
        this.modify("sapsm",sapsm);
    }

    /**
     * 设置 [SDATE]
     */
    public void setSdate(Timestamp  sdate){
        this.sdate = sdate ;
        this.modify("sdate",sdate);
    }

    /**
     * 设置 [SAPCONTROL]
     */
    public void setSapcontrol(Integer  sapcontrol){
        this.sapcontrol = sapcontrol ;
        this.modify("sapcontrol",sapcontrol);
    }

    /**
     * 设置 [EMSERVICEID]
     */
    public void setEmserviceid(String  emserviceid){
        this.emserviceid = emserviceid ;
        this.modify("emserviceid",emserviceid);
    }

    /**
     * 设置 [PODETAILID]
     */
    public void setPodetailid(String  podetailid){
        this.podetailid = podetailid ;
        this.modify("podetailid",podetailid);
    }

    /**
     * 设置 [STOREID]
     */
    public void setStoreid(String  storeid){
        this.storeid = storeid ;
        this.modify("storeid",storeid);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [STOREPARTID]
     */
    public void setStorepartid(String  storepartid){
        this.storepartid = storepartid ;
        this.modify("storepartid",storepartid);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [SEMPID]
     */
    public void setSempid(String  sempid){
        this.sempid = sempid ;
        this.modify("sempid",sempid);
    }


}


