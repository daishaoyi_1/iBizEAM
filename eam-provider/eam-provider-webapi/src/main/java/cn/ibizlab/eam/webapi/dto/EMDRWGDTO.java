package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMDRWGDTO]
 */
@Data
public class EMDRWGDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [BPERSONID]
     *
     */
    @JSONField(name = "bpersonid")
    @JsonProperty("bpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String bpersonid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [DRWGCODE]
     *
     */
    @JSONField(name = "drwgcode")
    @JsonProperty("drwgcode")
    @NotBlank(message = "[文档代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String drwgcode;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [EFILECONTENT]
     *
     */
    @JSONField(name = "efilecontent")
    @JsonProperty("efilecontent")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String efilecontent;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [DRWGTYPE]
     *
     */
    @JSONField(name = "drwgtype")
    @JsonProperty("drwgtype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String drwgtype;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [LCT]
     *
     */
    @JSONField(name = "lct")
    @JsonProperty("lct")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lct;

    /**
     * 属性 [EMDRWGNAME]
     *
     */
    @JSONField(name = "emdrwgname")
    @JsonProperty("emdrwgname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emdrwgname;

    /**
     * 属性 [DRWGSTATE]
     *
     */
    @JSONField(name = "drwgstate")
    @JsonProperty("drwgstate")
    @NotBlank(message = "[文档状态]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String drwgstate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [EMDRWGID]
     *
     */
    @JSONField(name = "emdrwgid")
    @JsonProperty("emdrwgid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emdrwgid;

    /**
     * 属性 [DRWGINFO]
     *
     */
    @JSONField(name = "drwginfo")
    @JsonProperty("drwginfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String drwginfo;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [BPERSONNAME]
     *
     */
    @JSONField(name = "bpersonname")
    @JsonProperty("bpersonname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String bpersonname;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String deptid;


    /**
     * 设置 [BPERSONID]
     */
    public void setBpersonid(String  bpersonid){
        this.bpersonid = bpersonid ;
        this.modify("bpersonid",bpersonid);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [DRWGCODE]
     */
    public void setDrwgcode(String  drwgcode){
        this.drwgcode = drwgcode ;
        this.modify("drwgcode",drwgcode);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [EFILECONTENT]
     */
    public void setEfilecontent(String  efilecontent){
        this.efilecontent = efilecontent ;
        this.modify("efilecontent",efilecontent);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [DRWGTYPE]
     */
    public void setDrwgtype(String  drwgtype){
        this.drwgtype = drwgtype ;
        this.modify("drwgtype",drwgtype);
    }

    /**
     * 设置 [LCT]
     */
    public void setLct(String  lct){
        this.lct = lct ;
        this.modify("lct",lct);
    }

    /**
     * 设置 [EMDRWGNAME]
     */
    public void setEmdrwgname(String  emdrwgname){
        this.emdrwgname = emdrwgname ;
        this.modify("emdrwgname",emdrwgname);
    }

    /**
     * 设置 [DRWGSTATE]
     */
    public void setDrwgstate(String  drwgstate){
        this.drwgstate = drwgstate ;
        this.modify("drwgstate",drwgstate);
    }

    /**
     * 设置 [BPERSONNAME]
     */
    public void setBpersonname(String  bpersonname){
        this.bpersonname = bpersonname ;
        this.modify("bpersonname",bpersonname);
    }


}


