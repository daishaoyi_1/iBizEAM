package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMMonthlyDetail;
import cn.ibizlab.eam.core.eam_core.service.IEMMonthlyDetailService;
import cn.ibizlab.eam.core.eam_core.filter.EMMonthlyDetailSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"维修中心月度计划明细" })
@RestController("WebApi-emmonthlydetail")
@RequestMapping("")
public class EMMonthlyDetailResource {

    @Autowired
    public IEMMonthlyDetailService emmonthlydetailService;

    @Autowired
    @Lazy
    public EMMonthlyDetailMapping emmonthlydetailMapping;

    @PreAuthorize("hasPermission(this.emmonthlydetailMapping.toDomain(#emmonthlydetaildto),'eam-EMMonthlyDetail-Create')")
    @ApiOperation(value = "新建维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "新建维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlydetails")
    public ResponseEntity<EMMonthlyDetailDTO> create(@Validated @RequestBody EMMonthlyDetailDTO emmonthlydetaildto) {
        EMMonthlyDetail domain = emmonthlydetailMapping.toDomain(emmonthlydetaildto);
		emmonthlydetailService.create(domain);
        EMMonthlyDetailDTO dto = emmonthlydetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmonthlydetailMapping.toDomain(#emmonthlydetaildtos),'eam-EMMonthlyDetail-Create')")
    @ApiOperation(value = "批量新建维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "批量新建维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlydetails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMMonthlyDetailDTO> emmonthlydetaildtos) {
        emmonthlydetailService.createBatch(emmonthlydetailMapping.toDomain(emmonthlydetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emmonthlydetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emmonthlydetailService.get(#emmonthlydetail_id),'eam-EMMonthlyDetail-Update')")
    @ApiOperation(value = "更新维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "更新维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmonthlydetails/{emmonthlydetail_id}")
    public ResponseEntity<EMMonthlyDetailDTO> update(@PathVariable("emmonthlydetail_id") String emmonthlydetail_id, @RequestBody EMMonthlyDetailDTO emmonthlydetaildto) {
		EMMonthlyDetail domain  = emmonthlydetailMapping.toDomain(emmonthlydetaildto);
        domain .setEmmonthlydetailid(emmonthlydetail_id);
		emmonthlydetailService.update(domain );
		EMMonthlyDetailDTO dto = emmonthlydetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmonthlydetailService.getEmmonthlydetailByEntities(this.emmonthlydetailMapping.toDomain(#emmonthlydetaildtos)),'eam-EMMonthlyDetail-Update')")
    @ApiOperation(value = "批量更新维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "批量更新维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmonthlydetails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMMonthlyDetailDTO> emmonthlydetaildtos) {
        emmonthlydetailService.updateBatch(emmonthlydetailMapping.toDomain(emmonthlydetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emmonthlydetailService.get(#emmonthlydetail_id),'eam-EMMonthlyDetail-Remove')")
    @ApiOperation(value = "删除维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "删除维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmonthlydetails/{emmonthlydetail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emmonthlydetail_id") String emmonthlydetail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emmonthlydetailService.remove(emmonthlydetail_id));
    }

    @PreAuthorize("hasPermission(this.emmonthlydetailService.getEmmonthlydetailByIds(#ids),'eam-EMMonthlyDetail-Remove')")
    @ApiOperation(value = "批量删除维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "批量删除维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmonthlydetails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emmonthlydetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emmonthlydetailMapping.toDomain(returnObject.body),'eam-EMMonthlyDetail-Get')")
    @ApiOperation(value = "获取维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "获取维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.GET, value = "/emmonthlydetails/{emmonthlydetail_id}")
    public ResponseEntity<EMMonthlyDetailDTO> get(@PathVariable("emmonthlydetail_id") String emmonthlydetail_id) {
        EMMonthlyDetail domain = emmonthlydetailService.get(emmonthlydetail_id);
        EMMonthlyDetailDTO dto = emmonthlydetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取维修中心月度计划明细草稿", tags = {"维修中心月度计划明细" },  notes = "获取维修中心月度计划明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emmonthlydetails/getdraft")
    public ResponseEntity<EMMonthlyDetailDTO> getDraft(EMMonthlyDetailDTO dto) {
        EMMonthlyDetail domain = emmonthlydetailMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emmonthlydetailMapping.toDto(emmonthlydetailService.getDraft(domain)));
    }

    @ApiOperation(value = "检查维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "检查维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlydetails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMMonthlyDetailDTO emmonthlydetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emmonthlydetailService.checkKey(emmonthlydetailMapping.toDomain(emmonthlydetaildto)));
    }

    @PreAuthorize("hasPermission(this.emmonthlydetailMapping.toDomain(#emmonthlydetaildto),'eam-EMMonthlyDetail-Save')")
    @ApiOperation(value = "保存维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "保存维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlydetails/save")
    public ResponseEntity<EMMonthlyDetailDTO> save(@RequestBody EMMonthlyDetailDTO emmonthlydetaildto) {
        EMMonthlyDetail domain = emmonthlydetailMapping.toDomain(emmonthlydetaildto);
        emmonthlydetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emmonthlydetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emmonthlydetailMapping.toDomain(#emmonthlydetaildtos),'eam-EMMonthlyDetail-Save')")
    @ApiOperation(value = "批量保存维修中心月度计划明细", tags = {"维修中心月度计划明细" },  notes = "批量保存维修中心月度计划明细")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlydetails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMMonthlyDetailDTO> emmonthlydetaildtos) {
        emmonthlydetailService.saveBatch(emmonthlydetailMapping.toDomain(emmonthlydetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMonthlyDetail-searchDefault-all') and hasPermission(#context,'eam-EMMonthlyDetail-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"维修中心月度计划明细" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emmonthlydetails/fetchdefault")
	public ResponseEntity<List<EMMonthlyDetailDTO>> fetchDefault(EMMonthlyDetailSearchContext context) {
        Page<EMMonthlyDetail> domains = emmonthlydetailService.searchDefault(context) ;
        List<EMMonthlyDetailDTO> list = emmonthlydetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMonthlyDetail-searchDefault-all') and hasPermission(#context,'eam-EMMonthlyDetail-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"维修中心月度计划明细" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emmonthlydetails/searchdefault")
	public ResponseEntity<Page<EMMonthlyDetailDTO>> searchDefault(@RequestBody EMMonthlyDetailSearchContext context) {
        Page<EMMonthlyDetail> domains = emmonthlydetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emmonthlydetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

