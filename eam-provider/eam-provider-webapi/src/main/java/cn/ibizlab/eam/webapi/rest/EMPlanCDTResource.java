package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanCDT;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanCDTService;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanCDTSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划条件" })
@RestController("WebApi-emplancdt")
@RequestMapping("")
public class EMPlanCDTResource {

    @Autowired
    public IEMPlanCDTService emplancdtService;

    @Autowired
    @Lazy
    public EMPlanCDTMapping emplancdtMapping;

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "新建计划条件", tags = {"计划条件" },  notes = "新建计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> create(@Validated @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "批量新建计划条件", tags = {"计划条件" },  notes = "批量新建计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplancdts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        emplancdtService.createBatch(emplancdtMapping.toDomain(emplancdtdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "更新计划条件", tags = {"计划条件" },  notes = "更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> update(@PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
		EMPlanCDT domain  = emplancdtMapping.toDomain(emplancdtdto);
        domain .setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain );
		EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "批量更新计划条件", tags = {"计划条件" },  notes = "批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        emplancdtService.updateBatch(emplancdtMapping.toDomain(emplancdtdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "删除计划条件", tags = {"计划条件" },  notes = "删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emplancdt_id") String emplancdt_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "批量删除计划条件", tags = {"计划条件" },  notes = "批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "获取计划条件", tags = {"计划条件" },  notes = "获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> get(@PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划条件草稿", tags = {"计划条件" },  notes = "获取计划条件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraft(EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划条件", tags = {"计划条件" },  notes = "检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "保存计划条件", tags = {"计划条件" },  notes = "保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> save(@RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "批量保存计划条件", tags = {"计划条件" },  notes = "批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        emplancdtService.saveBatch(emplancdtMapping.toDomain(emplancdtdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划条件" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchDefault(EMPlanCDTSearchContext context) {
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划条件" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchDefault(@RequestBody EMPlanCDTSearchContext context) {
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据计划建立计划条件", tags = {"计划条件" },  notes = "根据计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据计划批量建立计划条件", tags = {"计划条件" },  notes = "根据计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据计划更新计划条件", tags = {"计划条件" },  notes = "根据计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据计划批量更新计划条件", tags = {"计划条件" },  notes = "根据计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据计划删除计划条件", tags = {"计划条件" },  notes = "根据计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据计划批量删除计划条件", tags = {"计划条件" },  notes = "根据计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据计划获取计划条件", tags = {"计划条件" },  notes = "根据计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByEMPlan(@PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划检查计划条件", tags = {"计划条件" },  notes = "根据计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据计划保存计划条件", tags = {"计划条件" },  notes = "根据计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据计划批量保存计划条件", tags = {"计划条件" },  notes = "根据计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据总帐科目计划建立计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据总帐科目计划批量建立计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据总帐科目计划更新计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据总帐科目计划批量更新计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据总帐科目计划删除计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据总帐科目计划批量删除计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据总帐科目计划获取计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据总帐科目计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划检查计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据总帐科目计划保存计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据总帐科目计划批量保存计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据总帐科目计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据总帐科目计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据总帐科目计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据总帐科目计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据设备档案计划建立计划条件", tags = {"计划条件" },  notes = "根据设备档案计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据设备档案计划批量建立计划条件", tags = {"计划条件" },  notes = "根据设备档案计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据设备档案计划更新计划条件", tags = {"计划条件" },  notes = "根据设备档案计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据设备档案计划批量更新计划条件", tags = {"计划条件" },  notes = "根据设备档案计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据设备档案计划删除计划条件", tags = {"计划条件" },  notes = "根据设备档案计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据设备档案计划批量删除计划条件", tags = {"计划条件" },  notes = "根据设备档案计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquipEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据设备档案计划获取计划条件", tags = {"计划条件" },  notes = "根据设备档案计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据设备档案计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案计划检查计划条件", tags = {"计划条件" },  notes = "根据设备档案计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据设备档案计划保存计划条件", tags = {"计划条件" },  notes = "根据设备档案计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据设备档案计划批量保存计划条件", tags = {"计划条件" },  notes = "根据设备档案计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据设备档案计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据设备档案计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据设备档案计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据设备档案计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据计划模板计划建立计划条件", tags = {"计划条件" },  notes = "根据计划模板计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据计划模板计划批量建立计划条件", tags = {"计划条件" },  notes = "根据计划模板计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据计划模板计划更新计划条件", tags = {"计划条件" },  notes = "根据计划模板计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据计划模板计划批量更新计划条件", tags = {"计划条件" },  notes = "根据计划模板计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据计划模板计划删除计划条件", tags = {"计划条件" },  notes = "根据计划模板计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据计划模板计划批量删除计划条件", tags = {"计划条件" },  notes = "根据计划模板计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据计划模板计划获取计划条件", tags = {"计划条件" },  notes = "根据计划模板计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划模板计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据计划模板计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划模板计划检查计划条件", tags = {"计划条件" },  notes = "根据计划模板计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据计划模板计划保存计划条件", tags = {"计划条件" },  notes = "根据计划模板计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据计划模板计划批量保存计划条件", tags = {"计划条件" },  notes = "根据计划模板计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据计划模板计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据计划模板计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据服务商计划建立计划条件", tags = {"计划条件" },  notes = "根据服务商计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据服务商计划批量建立计划条件", tags = {"计划条件" },  notes = "根据服务商计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据服务商计划更新计划条件", tags = {"计划条件" },  notes = "根据服务商计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据服务商计划批量更新计划条件", tags = {"计划条件" },  notes = "根据服务商计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据服务商计划删除计划条件", tags = {"计划条件" },  notes = "根据服务商计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据服务商计划批量删除计划条件", tags = {"计划条件" },  notes = "根据服务商计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据服务商计划获取计划条件", tags = {"计划条件" },  notes = "根据服务商计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据服务商计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划检查计划条件", tags = {"计划条件" },  notes = "根据服务商计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据服务商计划保存计划条件", tags = {"计划条件" },  notes = "根据服务商计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据服务商计划批量保存计划条件", tags = {"计划条件" },  notes = "根据服务商计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据服务商计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据服务商计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据服务商计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据服务商计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据班组计划建立计划条件", tags = {"计划条件" },  notes = "根据班组计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据班组计划批量建立计划条件", tags = {"计划条件" },  notes = "根据班组计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据班组计划更新计划条件", tags = {"计划条件" },  notes = "根据班组计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据班组计划批量更新计划条件", tags = {"计划条件" },  notes = "根据班组计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据班组计划删除计划条件", tags = {"计划条件" },  notes = "根据班组计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据班组计划批量删除计划条件", tags = {"计划条件" },  notes = "根据班组计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据班组计划获取计划条件", tags = {"计划条件" },  notes = "根据班组计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据班组计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划检查计划条件", tags = {"计划条件" },  notes = "根据班组计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据班组计划保存计划条件", tags = {"计划条件" },  notes = "根据班组计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据班组计划批量保存计划条件", tags = {"计划条件" },  notes = "根据班组计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据班组计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据班组计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据班组计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据班组计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据班组设备档案计划建立计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据班组设备档案计划批量建立计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据班组设备档案计划更新计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据班组设备档案计划批量更新计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据班组设备档案计划删除计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据班组设备档案计划批量删除计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquipEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据班组设备档案计划获取计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据班组设备档案计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案计划检查计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据班组设备档案计划保存计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据班组设备档案计划批量保存计划条件", tags = {"计划条件" },  notes = "根据班组设备档案计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据班组设备档案计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据班组设备档案计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据班组设备档案计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据班组设备档案计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划建立计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量建立计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划更新计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量更新计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划删除计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量删除计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据总帐科目计划模板计划获取计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划模板计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划模板计划检查计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划保存计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量保存计划条件", tags = {"计划条件" },  notes = "根据总帐科目计划模板计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据总帐科目计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据总帐科目计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据服务商计划模板计划建立计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据服务商计划模板计划批量建立计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据服务商计划模板计划更新计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据服务商计划模板计划批量更新计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划删除计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划批量删除计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据服务商计划模板计划获取计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划模板计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据服务商计划模板计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划模板计划检查计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据服务商计划模板计划保存计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据服务商计划模板计划批量保存计划条件", tags = {"计划条件" },  notes = "根据服务商计划模板计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据服务商计划模板计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据服务商计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据服务商计划模板计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据服务商计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据班组计划模板计划建立计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts")
    public ResponseEntity<EMPlanCDTDTO> createByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
		emplancdtService.create(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Create')")
    @ApiOperation(value = "根据班组计划模板计划批量建立计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划批量建立计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplancdt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据班组计划模板计划更新计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> updateByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        domain.setEmplancdtid(emplancdt_id);
		emplancdtService.update(domain);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByEntities(this.emplancdtMapping.toDomain(#emplancdtdtos)),'eam-EMPlanCDT-Update')")
    @ApiOperation(value = "根据班组计划模板计划批量更新计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划批量更新计划条件")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplancdtService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplancdtService.get(#emplancdt_id),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据班组计划模板计划删除计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplancdtService.remove(emplancdt_id));
    }

    @PreAuthorize("hasPermission(this.emplancdtService.getEmplancdtByIds(#ids),'eam-EMPlanCDT-Remove')")
    @ApiOperation(value = "根据班组计划模板计划批量删除计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划批量删除计划条件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplancdtService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplancdtMapping.toDomain(returnObject.body),'eam-EMPlanCDT-Get')")
    @ApiOperation(value = "根据班组计划模板计划获取计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划获取计划条件")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/{emplancdt_id}")
    public ResponseEntity<EMPlanCDTDTO> getByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplancdt_id") String emplancdt_id) {
        EMPlanCDT domain = emplancdtService.get(emplancdt_id);
        EMPlanCDTDTO dto = emplancdtMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划模板计划获取计划条件草稿", tags = {"计划条件" },  notes = "根据班组计划模板计划获取计划条件草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/getdraft")
    public ResponseEntity<EMPlanCDTDTO> getDraftByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanCDTDTO dto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(emplancdtService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划模板计划检查计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划检查计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplancdtService.checkKey(emplancdtMapping.toDomain(emplancdtdto)));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdto),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据班组计划模板计划保存计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/save")
    public ResponseEntity<EMPlanCDTDTO> saveByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTDTO emplancdtdto) {
        EMPlanCDT domain = emplancdtMapping.toDomain(emplancdtdto);
        domain.setPlanid(emplan_id);
        emplancdtService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplancdtMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplancdtMapping.toDomain(#emplancdtdtos),'eam-EMPlanCDT-Save')")
    @ApiOperation(value = "根据班组计划模板计划批量保存计划条件", tags = {"计划条件" },  notes = "根据班组计划模板计划批量保存计划条件")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanCDTDTO> emplancdtdtos) {
        List<EMPlanCDT> domainlist=emplancdtMapping.toDomain(emplancdtdtos);
        for(EMPlanCDT domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplancdtService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据班组计划模板计划获取DEFAULT", tags = {"计划条件" } ,notes = "根据班组计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/fetchdefault")
	public ResponseEntity<List<EMPlanCDTDTO>> fetchEMPlanCDTDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
        List<EMPlanCDTDTO> list = emplancdtMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanCDT-searchDefault-all') and hasPermission(#context,'eam-EMPlanCDT-Get')")
	@ApiOperation(value = "根据班组计划模板计划查询DEFAULT", tags = {"计划条件" } ,notes = "根据班组计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplancdts/searchdefault")
	public ResponseEntity<Page<EMPlanCDTDTO>> searchEMPlanCDTDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanCDTSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanCDT> domains = emplancdtService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplancdtMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

