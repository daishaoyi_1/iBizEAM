package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPLANRECORDDTO]
 */
@Data
public class EMPLANRECORDDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMPLANRECORDID]
     *
     */
    @JSONField(name = "emplanrecordid")
    @JsonProperty("emplanrecordid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emplanrecordid;

    /**
     * 属性 [EMPLANRECORDNAME]
     *
     */
    @JSONField(name = "emplanrecordname")
    @JsonProperty("emplanrecordname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emplanrecordname;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [ISTRIGGER]
     *
     */
    @JSONField(name = "istrigger")
    @JsonProperty("istrigger")
    private Integer istrigger;

    /**
     * 属性 [EMPLANID]
     *
     */
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emplanid;

    /**
     * 属性 [EMPLANNAME]
     *
     */
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emplanname;

    /**
     * 属性 [TRIGGERCA]
     *
     */
    @JSONField(name = "triggerca")
    @JsonProperty("triggerca")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String triggerca;

    /**
     * 属性 [TRIGGERRE]
     *
     */
    @JSONField(name = "triggerre")
    @JsonProperty("triggerre")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String triggerre;

    /**
     * 属性 [REMARK]
     *
     */
    @JSONField(name = "remark")
    @JsonProperty("remark")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String remark;

    /**
     * 属性 [TRIGGERDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "triggerdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("triggerdate")
    private Timestamp triggerdate;


    /**
     * 设置 [EMPLANRECORDNAME]
     */
    public void setEmplanrecordname(String  emplanrecordname){
        this.emplanrecordname = emplanrecordname ;
        this.modify("emplanrecordname",emplanrecordname);
    }

    /**
     * 设置 [ISTRIGGER]
     */
    public void setIstrigger(Integer  istrigger){
        this.istrigger = istrigger ;
        this.modify("istrigger",istrigger);
    }

    /**
     * 设置 [EMPLANID]
     */
    public void setEmplanid(String  emplanid){
        this.emplanid = emplanid ;
        this.modify("emplanid",emplanid);
    }

    /**
     * 设置 [TRIGGERCA]
     */
    public void setTriggerca(String  triggerca){
        this.triggerca = triggerca ;
        this.modify("triggerca",triggerca);
    }

    /**
     * 设置 [TRIGGERRE]
     */
    public void setTriggerre(String  triggerre){
        this.triggerre = triggerre ;
        this.modify("triggerre",triggerre);
    }

    /**
     * 设置 [REMARK]
     */
    public void setRemark(String  remark){
        this.remark = remark ;
        this.modify("remark",remark);
    }

    /**
     * 设置 [TRIGGERDATE]
     */
    public void setTriggerdate(Timestamp  triggerdate){
        this.triggerdate = triggerdate ;
        this.modify("triggerdate",triggerdate);
    }


}


