package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[PLANSCHEDULE_MDTO]
 */
@Data
public class PLANSCHEDULE_MDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PLANSCHEDULE_MID]
     *
     */
    @JSONField(name = "planschedule_mid")
    @JsonProperty("planschedule_mid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String planscheduleMid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [PLANSCHEDULE_MNAME]
     *
     */
    @JSONField(name = "planschedule_mname")
    @JsonProperty("planschedule_mname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String planscheduleMname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EMPLANID]
     *
     */
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emplanid;

    /**
     * 属性 [CYCLESTARTTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "cyclestarttime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cyclestarttime")
    private Timestamp cyclestarttime;

    /**
     * 属性 [INTERVALMINUTE]
     *
     */
    @JSONField(name = "intervalminute")
    @JsonProperty("intervalminute")
    private Integer intervalminute;

    /**
     * 属性 [SCHEDULEPARAM]
     *
     */
    @JSONField(name = "scheduleparam")
    @JsonProperty("scheduleparam")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam;

    /**
     * 属性 [SCHEDULETYPE]
     *
     */
    @JSONField(name = "scheduletype")
    @JsonProperty("scheduletype")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String scheduletype;

    /**
     * 属性 [CYCLEENDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "cycleendtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cycleendtime")
    private Timestamp cycleendtime;

    /**
     * 属性 [EMPLANNAME]
     *
     */
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emplanname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [SCHEDULESTATE]
     *
     */
    @JSONField(name = "schedulestate")
    @JsonProperty("schedulestate")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String schedulestate;

    /**
     * 属性 [SCHEDULEPARAM4]
     *
     */
    @JSONField(name = "scheduleparam4")
    @JsonProperty("scheduleparam4")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam4;

    /**
     * 属性 [SCHEDULEPARAM2]
     *
     */
    @JSONField(name = "scheduleparam2")
    @JsonProperty("scheduleparam2")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam2;

    /**
     * 属性 [RUNTIME]
     *
     */
    @JsonFormat(pattern="HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "runtime" , format="HH:mm:ss")
    @JsonProperty("runtime")
    private Timestamp runtime;

    /**
     * 属性 [RUNDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rundate" , format="yyyy-MM-dd")
    @JsonProperty("rundate")
    private Timestamp rundate;

    /**
     * 属性 [SCHEDULEPARAM3]
     *
     */
    @JSONField(name = "scheduleparam3")
    @JsonProperty("scheduleparam3")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String scheduleparam3;

    /**
     * 属性 [NOCODE]
     *
     */
    @JSONField(name = "nocode")
    @JsonProperty("nocode")
    private Integer nocode;

    /**
     * 属性 [NOSEQ]
     *
     */
    @JSONField(name = "noseq")
    @JsonProperty("noseq")
    private Integer noseq;

    /**
     * 属性 [NOSEQ2]
     *
     */
    @JSONField(name = "noseq2")
    @JsonProperty("noseq2")
    private Integer noseq2;

    /**
     * 属性 [LASTMINUTE]
     *
     */
    @JSONField(name = "lastminute")
    @JsonProperty("lastminute")
    private Integer lastminute;

    /**
     * 属性 [TASKID]
     *
     */
    @JSONField(name = "taskid")
    @JsonProperty("taskid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String taskid;


    /**
     * 设置 [PLANSCHEDULE_MNAME]
     */
    public void setPlanscheduleMname(String  planscheduleMname){
        this.planscheduleMname = planscheduleMname ;
        this.modify("planschedule_mname",planscheduleMname);
    }

    /**
     * 设置 [NOCODE]
     */
    public void setNocode(Integer  nocode){
        this.nocode = nocode ;
        this.modify("nocode",nocode);
    }

    /**
     * 设置 [NOSEQ]
     */
    public void setNoseq(Integer  noseq){
        this.noseq = noseq ;
        this.modify("noseq",noseq);
    }

    /**
     * 设置 [NOSEQ2]
     */
    public void setNoseq2(Integer  noseq2){
        this.noseq2 = noseq2 ;
        this.modify("noseq2",noseq2);
    }


}


