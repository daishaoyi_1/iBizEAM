package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMCabDTO]
 */
@Data
public class EMCabDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ARG2]
     *
     */
    @JSONField(name = "arg2")
    @JsonProperty("arg2")
    private Integer arg2;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [STOREPARTGL]
     *
     */
    @JSONField(name = "storepartgl")
    @JsonProperty("storepartgl")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storepartgl;

    /**
     * 属性 [ARG1]
     *
     */
    @JSONField(name = "arg1")
    @JsonProperty("arg1")
    private Integer arg1;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [ARG0]
     *
     */
    @JSONField(name = "arg0")
    @JsonProperty("arg0")
    private Integer arg0;

    /**
     * 属性 [EMCABID]
     *
     */
    @JSONField(name = "emcabid")
    @JsonProperty("emcabid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emcabid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [EMCABNAME]
     *
     */
    @JSONField(name = "emcabname")
    @JsonProperty("emcabname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emcabname;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [GRAPHPARAM]
     *
     */
    @JSONField(name = "graphparam")
    @JsonProperty("graphparam")
    private Integer graphparam;

    /**
     * 属性 [EMSTOREPARTNAME]
     *
     */
    @JSONField(name = "emstorepartname")
    @JsonProperty("emstorepartname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emstorepartname;

    /**
     * 属性 [EMSTORENAME]
     *
     */
    @JSONField(name = "emstorename")
    @JsonProperty("emstorename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emstorename;

    /**
     * 属性 [EMSTOREPARTID]
     *
     */
    @JSONField(name = "emstorepartid")
    @JsonProperty("emstorepartid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emstorepartid;

    /**
     * 属性 [EMSTOREID]
     *
     */
    @JSONField(name = "emstoreid")
    @JsonProperty("emstoreid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emstoreid;


    /**
     * 设置 [ARG2]
     */
    public void setArg2(Integer  arg2){
        this.arg2 = arg2 ;
        this.modify("arg2",arg2);
    }

    /**
     * 设置 [STOREPARTGL]
     */
    public void setStorepartgl(String  storepartgl){
        this.storepartgl = storepartgl ;
        this.modify("storepartgl",storepartgl);
    }

    /**
     * 设置 [ARG1]
     */
    public void setArg1(Integer  arg1){
        this.arg1 = arg1 ;
        this.modify("arg1",arg1);
    }

    /**
     * 设置 [ARG0]
     */
    public void setArg0(Integer  arg0){
        this.arg0 = arg0 ;
        this.modify("arg0",arg0);
    }

    /**
     * 设置 [EMCABNAME]
     */
    public void setEmcabname(String  emcabname){
        this.emcabname = emcabname ;
        this.modify("emcabname",emcabname);
    }

    /**
     * 设置 [GRAPHPARAM]
     */
    public void setGraphparam(Integer  graphparam){
        this.graphparam = graphparam ;
        this.modify("graphparam",graphparam);
    }

    /**
     * 设置 [EMSTOREPARTID]
     */
    public void setEmstorepartid(String  emstorepartid){
        this.emstorepartid = emstorepartid ;
        this.modify("emstorepartid",emstorepartid);
    }

    /**
     * 设置 [EMSTOREID]
     */
    public void setEmstoreid(String  emstoreid){
        this.emstoreid = emstoreid ;
        this.modify("emstoreid",emstoreid);
    }


}


