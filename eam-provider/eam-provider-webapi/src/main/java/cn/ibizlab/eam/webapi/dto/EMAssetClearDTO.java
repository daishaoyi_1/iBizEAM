package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMAssetClearDTO]
 */
@Data
public class EMAssetClearDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ASSETINPRICE]
     *
     */
    @JSONField(name = "assetinprice")
    @JsonProperty("assetinprice")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetinprice;

    /**
     * 属性 [ASSETCHECKPRICE]
     *
     */
    @JSONField(name = "assetcheckprice")
    @JsonProperty("assetcheckprice")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetcheckprice;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [ISNEW]
     *
     */
    @JSONField(name = "isnew")
    @JsonProperty("isnew")
    @NotNull(message = "[最新清盘]不允许为空!")
    private Integer isnew;

    /**
     * 属性 [ASSETOUTPRICE]
     *
     */
    @JSONField(name = "assetoutprice")
    @JsonProperty("assetoutprice")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetoutprice;

    /**
     * 属性 [ASSETCLEARLCT]
     *
     */
    @JSONField(name = "assetclearlct")
    @JsonProperty("assetclearlct")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetclearlct;

    /**
     * 属性 [EMASSETCLEARNAME]
     *
     */
    @JSONField(name = "emassetclearname")
    @JsonProperty("emassetclearname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassetclearname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ASSETOUTNUM]
     *
     */
    @JSONField(name = "assetoutnum")
    @JsonProperty("assetoutnum")
    private Integer assetoutnum;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [ASSETCLEARSTATE]
     *
     */
    @JSONField(name = "assetclearstate")
    @JsonProperty("assetclearstate")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetclearstate;

    /**
     * 属性 [ASSETCLEARDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "assetcleardate" , format="yyyy-MM-dd")
    @JsonProperty("assetcleardate")
    private Timestamp assetcleardate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ASSETINNUM]
     *
     */
    @JSONField(name = "assetinnum")
    @JsonProperty("assetinnum")
    private Integer assetinnum;

    /**
     * 属性 [ASSETCHECKNUM]
     *
     */
    @JSONField(name = "assetchecknum")
    @JsonProperty("assetchecknum")
    private Integer assetchecknum;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMASSETCLEARID]
     *
     */
    @JSONField(name = "emassetclearid")
    @JsonProperty("emassetclearid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassetclearid;

    /**
     * 属性 [ASSETSORT]
     *
     */
    @JSONField(name = "assetsort")
    @JsonProperty("assetsort")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetsort;

    /**
     * 属性 [ASSETCODE]
     *
     */
    @JSONField(name = "assetcode")
    @JsonProperty("assetcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetcode;

    /**
     * 属性 [ASSETCLASSNAME]
     *
     */
    @JSONField(name = "assetclassname")
    @JsonProperty("assetclassname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclassname;

    /**
     * 属性 [EQMODELCODE]
     *
     */
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqmodelcode;

    /**
     * 属性 [PURCHDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "purchdate" , format="yyyy-MM-dd")
    @JsonProperty("purchdate")
    private Timestamp purchdate;

    /**
     * 属性 [ORIGINALCOST]
     *
     */
    @JSONField(name = "originalcost")
    @JsonProperty("originalcost")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String originalcost;

    /**
     * 属性 [ASSETCLASSID]
     *
     */
    @JSONField(name = "assetclassid")
    @JsonProperty("assetclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String assetclassid;

    /**
     * 属性 [EMASSETNAME]
     *
     */
    @JSONField(name = "emassetname")
    @JsonProperty("emassetname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassetname;

    /**
     * 属性 [EMASSETID]
     *
     */
    @JSONField(name = "emassetid")
    @JsonProperty("emassetid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassetid;


    /**
     * 设置 [ASSETINPRICE]
     */
    public void setAssetinprice(String  assetinprice){
        this.assetinprice = assetinprice ;
        this.modify("assetinprice",assetinprice);
    }

    /**
     * 设置 [ASSETCHECKPRICE]
     */
    public void setAssetcheckprice(String  assetcheckprice){
        this.assetcheckprice = assetcheckprice ;
        this.modify("assetcheckprice",assetcheckprice);
    }

    /**
     * 设置 [ISNEW]
     */
    public void setIsnew(Integer  isnew){
        this.isnew = isnew ;
        this.modify("isnew",isnew);
    }

    /**
     * 设置 [ASSETOUTPRICE]
     */
    public void setAssetoutprice(String  assetoutprice){
        this.assetoutprice = assetoutprice ;
        this.modify("assetoutprice",assetoutprice);
    }

    /**
     * 设置 [ASSETCLEARLCT]
     */
    public void setAssetclearlct(String  assetclearlct){
        this.assetclearlct = assetclearlct ;
        this.modify("assetclearlct",assetclearlct);
    }

    /**
     * 设置 [EMASSETCLEARNAME]
     */
    public void setEmassetclearname(String  emassetclearname){
        this.emassetclearname = emassetclearname ;
        this.modify("emassetclearname",emassetclearname);
    }

    /**
     * 设置 [ASSETOUTNUM]
     */
    public void setAssetoutnum(Integer  assetoutnum){
        this.assetoutnum = assetoutnum ;
        this.modify("assetoutnum",assetoutnum);
    }

    /**
     * 设置 [ASSETCLEARSTATE]
     */
    public void setAssetclearstate(String  assetclearstate){
        this.assetclearstate = assetclearstate ;
        this.modify("assetclearstate",assetclearstate);
    }

    /**
     * 设置 [ASSETCLEARDATE]
     */
    public void setAssetcleardate(Timestamp  assetcleardate){
        this.assetcleardate = assetcleardate ;
        this.modify("assetcleardate",assetcleardate);
    }

    /**
     * 设置 [ASSETINNUM]
     */
    public void setAssetinnum(Integer  assetinnum){
        this.assetinnum = assetinnum ;
        this.modify("assetinnum",assetinnum);
    }

    /**
     * 设置 [ASSETCHECKNUM]
     */
    public void setAssetchecknum(Integer  assetchecknum){
        this.assetchecknum = assetchecknum ;
        this.modify("assetchecknum",assetchecknum);
    }

    /**
     * 设置 [EMASSETID]
     */
    public void setEmassetid(String  emassetid){
        this.emassetid = emassetid ;
        this.modify("emassetid",emassetid);
    }


}


