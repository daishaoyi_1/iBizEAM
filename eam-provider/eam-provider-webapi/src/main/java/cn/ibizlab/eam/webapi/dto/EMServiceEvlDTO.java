package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMServiceEvlDTO]
 */
@Data
public class EMServiceEvlDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMSERVICEEVLID]
     *
     */
    @JSONField(name = "emserviceevlid")
    @JsonProperty("emserviceevlid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emserviceevlid;

    /**
     * 属性 [EMSERVICEEVLNAME]
     *
     */
    @JSONField(name = "emserviceevlname")
    @JsonProperty("emserviceevlname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emserviceevlname;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [EVLRESULT9]
     *
     */
    @JSONField(name = "evlresult9")
    @JsonProperty("evlresult9")
    private Integer evlresult9;

    /**
     * 属性 [EVLRESULT5]
     *
     */
    @JSONField(name = "evlresult5")
    @JsonProperty("evlresult5")
    private Integer evlresult5;

    /**
     * 属性 [EVLRESULT7]
     *
     */
    @JSONField(name = "evlresult7")
    @JsonProperty("evlresult7")
    private Integer evlresult7;

    /**
     * 属性 [EVLRESULT6]
     *
     */
    @JSONField(name = "evlresult6")
    @JsonProperty("evlresult6")
    private Integer evlresult6;

    /**
     * 属性 [EVLRESULT1]
     *
     */
    @JSONField(name = "evlresult1")
    @JsonProperty("evlresult1")
    private Integer evlresult1;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EVLMARK1]
     *
     */
    @JSONField(name = "evlmark1")
    @JsonProperty("evlmark1")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String evlmark1;

    /**
     * 属性 [EVLRESULT4]
     *
     */
    @JSONField(name = "evlresult4")
    @JsonProperty("evlresult4")
    private Integer evlresult4;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EVLRESULT8]
     *
     */
    @JSONField(name = "evlresult8")
    @JsonProperty("evlresult8")
    private Integer evlresult8;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empname;

    /**
     * 属性 [EVLDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "evldate" , format="yyyy-MM-dd")
    @JsonProperty("evldate")
    private Timestamp evldate;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [EVLMARK]
     *
     */
    @JSONField(name = "evlmark")
    @JsonProperty("evlmark")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String evlmark;

    /**
     * 属性 [EVLRESULT]
     *
     */
    @JSONField(name = "evlresult")
    @JsonProperty("evlresult")
    private Integer evlresult;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [EVLREGION]
     *
     */
    @JSONField(name = "evlregion")
    @JsonProperty("evlregion")
    @NotBlank(message = "[评估区间]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String evlregion;

    /**
     * 属性 [EVLRESULT3]
     *
     */
    @JSONField(name = "evlresult3")
    @JsonProperty("evlresult3")
    private Integer evlresult3;

    /**
     * 属性 [EVLRESULT2]
     *
     */
    @JSONField(name = "evlresult2")
    @JsonProperty("evlresult2")
    private Integer evlresult2;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [SERVICEEVLSTATE]
     *
     */
    @JSONField(name = "serviceevlstate")
    @JsonProperty("serviceevlstate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String serviceevlstate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [SERVICENAME]
     *
     */
    @JSONField(name = "servicename")
    @JsonProperty("servicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String servicename;

    /**
     * 属性 [SERVICEID]
     *
     */
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String serviceid;


    /**
     * 设置 [EMSERVICEEVLNAME]
     */
    public void setEmserviceevlname(String  emserviceevlname){
        this.emserviceevlname = emserviceevlname ;
        this.modify("emserviceevlname",emserviceevlname);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [EVLRESULT9]
     */
    public void setEvlresult9(Integer  evlresult9){
        this.evlresult9 = evlresult9 ;
        this.modify("evlresult9",evlresult9);
    }

    /**
     * 设置 [EVLRESULT5]
     */
    public void setEvlresult5(Integer  evlresult5){
        this.evlresult5 = evlresult5 ;
        this.modify("evlresult5",evlresult5);
    }

    /**
     * 设置 [EVLRESULT7]
     */
    public void setEvlresult7(Integer  evlresult7){
        this.evlresult7 = evlresult7 ;
        this.modify("evlresult7",evlresult7);
    }

    /**
     * 设置 [EVLRESULT6]
     */
    public void setEvlresult6(Integer  evlresult6){
        this.evlresult6 = evlresult6 ;
        this.modify("evlresult6",evlresult6);
    }

    /**
     * 设置 [EVLRESULT1]
     */
    public void setEvlresult1(Integer  evlresult1){
        this.evlresult1 = evlresult1 ;
        this.modify("evlresult1",evlresult1);
    }

    /**
     * 设置 [EVLMARK1]
     */
    public void setEvlmark1(String  evlmark1){
        this.evlmark1 = evlmark1 ;
        this.modify("evlmark1",evlmark1);
    }

    /**
     * 设置 [EVLRESULT4]
     */
    public void setEvlresult4(Integer  evlresult4){
        this.evlresult4 = evlresult4 ;
        this.modify("evlresult4",evlresult4);
    }

    /**
     * 设置 [EVLRESULT8]
     */
    public void setEvlresult8(Integer  evlresult8){
        this.evlresult8 = evlresult8 ;
        this.modify("evlresult8",evlresult8);
    }

    /**
     * 设置 [EMPNAME]
     */
    public void setEmpname(String  empname){
        this.empname = empname ;
        this.modify("empname",empname);
    }

    /**
     * 设置 [EVLDATE]
     */
    public void setEvldate(Timestamp  evldate){
        this.evldate = evldate ;
        this.modify("evldate",evldate);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [EVLMARK]
     */
    public void setEvlmark(String  evlmark){
        this.evlmark = evlmark ;
        this.modify("evlmark",evlmark);
    }

    /**
     * 设置 [EVLREGION]
     */
    public void setEvlregion(String  evlregion){
        this.evlregion = evlregion ;
        this.modify("evlregion",evlregion);
    }

    /**
     * 设置 [EVLRESULT3]
     */
    public void setEvlresult3(Integer  evlresult3){
        this.evlresult3 = evlresult3 ;
        this.modify("evlresult3",evlresult3);
    }

    /**
     * 设置 [EVLRESULT2]
     */
    public void setEvlresult2(Integer  evlresult2){
        this.evlresult2 = evlresult2 ;
        this.modify("evlresult2",evlresult2);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }

    /**
     * 设置 [SERVICEEVLSTATE]
     */
    public void setServiceevlstate(String  serviceevlstate){
        this.serviceevlstate = serviceevlstate ;
        this.modify("serviceevlstate",serviceevlstate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [SERVICEID]
     */
    public void setServiceid(String  serviceid){
        this.serviceid = serviceid ;
        this.modify("serviceid",serviceid);
    }


}


