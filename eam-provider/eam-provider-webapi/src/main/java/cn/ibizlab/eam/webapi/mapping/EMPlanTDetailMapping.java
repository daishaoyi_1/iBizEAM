package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTDetail;
import cn.ibizlab.eam.webapi.dto.EMPlanTDetailDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMPlanTDetailMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMPlanTDetailMapping extends MappingBase<EMPlanTDetailDTO, EMPlanTDetail> {


}

