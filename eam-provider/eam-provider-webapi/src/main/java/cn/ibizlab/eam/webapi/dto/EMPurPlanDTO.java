package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPurPlanDTO]
 */
@Data
public class EMPurPlanDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [YEARFROM]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "yearfrom" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("yearfrom")
    private Timestamp yearfrom;

    /**
     * 属性 [ASSESSREPORT]
     *
     */
    @JSONField(name = "assessreport")
    @JsonProperty("assessreport")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String assessreport;

    /**
     * 属性 [EMPURPLANNAME]
     *
     */
    @JSONField(name = "empurplanname")
    @JsonProperty("empurplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empurplanname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [YEARS]
     *
     */
    @JSONField(name = "years")
    @JsonProperty("years")
    @NotBlank(message = "[采购年度]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String years;

    /**
     * 属性 [MSITEMTYPE]
     *
     */
    @JSONField(name = "msitemtype")
    @JsonProperty("msitemtype")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String msitemtype;

    /**
     * 属性 [EMPURPLANID]
     *
     */
    @JSONField(name = "empurplanid")
    @JsonProperty("empurplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empurplanid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [PLANSTATE]
     *
     */
    @JSONField(name = "planstate")
    @JsonProperty("planstate")
    private Integer planstate;

    /**
     * 属性 [ACCEPTANCEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "acceptancedate" , format="yyyy-MM-dd")
    @JsonProperty("acceptancedate")
    private Timestamp acceptancedate;

    /**
     * 属性 [COCNT]
     *
     */
    @JSONField(name = "cocnt")
    @JsonProperty("cocnt")
    private Double cocnt;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [YEARTO]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "yearto" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("yearto")
    private Timestamp yearto;

    /**
     * 属性 [NOWAMOUNT]
     *
     */
    @JSONField(name = "nowamount")
    @JsonProperty("nowamount")
    private Double nowamount;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [SERVICECODE]
     *
     */
    @JSONField(name = "servicecode")
    @JsonProperty("servicecode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String servicecode;

    /**
     * 属性 [ACCEPTANCERESULT]
     *
     */
    @JSONField(name = "acceptanceresult")
    @JsonProperty("acceptanceresult")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String acceptanceresult;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [PURAMOUNT]
     *
     */
    @JSONField(name = "puramount")
    @JsonProperty("puramount")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String puramount;

    /**
     * 属性 [TRACKRULE]
     *
     */
    @JSONField(name = "trackrule")
    @JsonProperty("trackrule")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String trackrule;

    /**
     * 属性 [M3Q]
     *
     */
    @JSONField(name = "m3q")
    @JsonProperty("m3q")
    @NotNull(message = "[经理指定询价数]不允许为空!")
    private Integer m3q;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [NOWCNT]
     *
     */
    @JSONField(name = "nowcnt")
    @JsonProperty("nowcnt")
    private Double nowcnt;

    /**
     * 属性 [COAMOUNT]
     *
     */
    @JSONField(name = "coamount")
    @JsonProperty("coamount")
    private Double coamount;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempname;

    /**
     * 属性 [ISTRACKOK]
     *
     */
    @JSONField(name = "istrackok")
    @JsonProperty("istrackok")
    private Integer istrackok;

    /**
     * 属性 [CONTRACTSCAN]
     *
     */
    @JSONField(name = "contractscan")
    @JsonProperty("contractscan")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String contractscan;

    /**
     * 属性 [PURSUM]
     *
     */
    @JSONField(name = "pursum")
    @JsonProperty("pursum")
    private Double pursum;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [ITEMTYPENAME]
     *
     */
    @JSONField(name = "itemtypename")
    @JsonProperty("itemtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemtypename;

    /**
     * 属性 [EMBIDINQUIRYNAME]
     *
     */
    @JSONField(name = "embidinquiryname")
    @JsonProperty("embidinquiryname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String embidinquiryname;

    /**
     * 属性 [ITEMTYPEID]
     *
     */
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemtypeid;

    /**
     * 属性 [EMBIDINQUIRYID]
     *
     */
    @JSONField(name = "embidinquiryid")
    @JsonProperty("embidinquiryid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String embidinquiryid;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitid;


    /**
     * 设置 [ASSESSREPORT]
     */
    public void setAssessreport(String  assessreport){
        this.assessreport = assessreport ;
        this.modify("assessreport",assessreport);
    }

    /**
     * 设置 [EMPURPLANNAME]
     */
    public void setEmpurplanname(String  empurplanname){
        this.empurplanname = empurplanname ;
        this.modify("empurplanname",empurplanname);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [YEARS]
     */
    public void setYears(String  years){
        this.years = years ;
        this.modify("years",years);
    }

    /**
     * 设置 [MSITEMTYPE]
     */
    public void setMsitemtype(String  msitemtype){
        this.msitemtype = msitemtype ;
        this.modify("msitemtype",msitemtype);
    }

    /**
     * 设置 [PLANSTATE]
     */
    public void setPlanstate(Integer  planstate){
        this.planstate = planstate ;
        this.modify("planstate",planstate);
    }

    /**
     * 设置 [ACCEPTANCEDATE]
     */
    public void setAcceptancedate(Timestamp  acceptancedate){
        this.acceptancedate = acceptancedate ;
        this.modify("acceptancedate",acceptancedate);
    }

    /**
     * 设置 [SERVICECODE]
     */
    public void setServicecode(String  servicecode){
        this.servicecode = servicecode ;
        this.modify("servicecode",servicecode);
    }

    /**
     * 设置 [ACCEPTANCERESULT]
     */
    public void setAcceptanceresult(String  acceptanceresult){
        this.acceptanceresult = acceptanceresult ;
        this.modify("acceptanceresult",acceptanceresult);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [PURAMOUNT]
     */
    public void setPuramount(String  puramount){
        this.puramount = puramount ;
        this.modify("puramount",puramount);
    }

    /**
     * 设置 [TRACKRULE]
     */
    public void setTrackrule(String  trackrule){
        this.trackrule = trackrule ;
        this.modify("trackrule",trackrule);
    }

    /**
     * 设置 [M3Q]
     */
    public void setM3q(Integer  m3q){
        this.m3q = m3q ;
        this.modify("m3q",m3q);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [CONTRACTSCAN]
     */
    public void setContractscan(String  contractscan){
        this.contractscan = contractscan ;
        this.modify("contractscan",contractscan);
    }

    /**
     * 设置 [PURSUM]
     */
    public void setPursum(Double  pursum){
        this.pursum = pursum ;
        this.modify("pursum",pursum);
    }

    /**
     * 设置 [ITEMTYPEID]
     */
    public void setItemtypeid(String  itemtypeid){
        this.itemtypeid = itemtypeid ;
        this.modify("itemtypeid",itemtypeid);
    }

    /**
     * 设置 [EMBIDINQUIRYID]
     */
    public void setEmbidinquiryid(String  embidinquiryid){
        this.embidinquiryid = embidinquiryid ;
        this.modify("embidinquiryid",embidinquiryid);
    }

    /**
     * 设置 [UNITID]
     */
    public void setUnitid(String  unitid){
        this.unitid = unitid ;
        this.modify("unitid",unitid);
    }


}


