package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTTIRes;
import cn.ibizlab.eam.webapi.dto.EMEQLCTTIResDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMEQLCTTIResMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMEQLCTTIResMapping extends MappingBase<EMEQLCTTIResDTO, EMEQLCTTIRes> {


}

