package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPurchangeHisDTO]
 */
@Data
public class EMPurchangeHisDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMPURCHANGEHISNAME]
     *
     */
    @JSONField(name = "empurchangehisname")
    @JsonProperty("empurchangehisname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empurchangehisname;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [BACKIT]
     *
     */
    @JSONField(name = "backit")
    @JsonProperty("backit")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String backit;

    /**
     * 属性 [PFDEPTNAME]
     *
     */
    @JSONField(name = "pfdeptname")
    @JsonProperty("pfdeptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfdeptname;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [PFEMPNAME]
     *
     */
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfempname;

    /**
     * 属性 [PFDEPTID]
     *
     */
    @JSONField(name = "pfdeptid")
    @JsonProperty("pfdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfdeptid;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [HDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "hdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("hdate")
    @NotNull(message = "[换料时间]不允许为空!")
    private Timestamp hdate;

    /**
     * 属性 [CHANGENUM]
     *
     */
    @JSONField(name = "changenum")
    @JsonProperty("changenum")
    @NotNull(message = "[换料数量]不允许为空!")
    private Double changenum;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [RESSON]
     *
     */
    @JSONField(name = "resson")
    @JsonProperty("resson")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String resson;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [EMPURCHANGEHISID]
     *
     */
    @JSONField(name = "empurchangehisid")
    @JsonProperty("empurchangehisid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empurchangehisid;

    /**
     * 属性 [PFEMPID]
     *
     */
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    @NotBlank(message = "[换料人]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfempid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [EMITEMPUSENAME]
     *
     */
    @JSONField(name = "emitempusename")
    @JsonProperty("emitempusename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitempusename;

    /**
     * 属性 [PSUM]
     *
     */
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;

    /**
     * 属性 [EMITEMPUSEID]
     *
     */
    @JSONField(name = "emitempuseid")
    @JsonProperty("emitempuseid")
    @NotBlank(message = "[领料单]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitempuseid;


    /**
     * 设置 [EMPURCHANGEHISNAME]
     */
    public void setEmpurchangehisname(String  empurchangehisname){
        this.empurchangehisname = empurchangehisname ;
        this.modify("empurchangehisname",empurchangehisname);
    }

    /**
     * 设置 [BACKIT]
     */
    public void setBackit(String  backit){
        this.backit = backit ;
        this.modify("backit",backit);
    }

    /**
     * 设置 [PFDEPTNAME]
     */
    public void setPfdeptname(String  pfdeptname){
        this.pfdeptname = pfdeptname ;
        this.modify("pfdeptname",pfdeptname);
    }

    /**
     * 设置 [PFEMPNAME]
     */
    public void setPfempname(String  pfempname){
        this.pfempname = pfempname ;
        this.modify("pfempname",pfempname);
    }

    /**
     * 设置 [PFDEPTID]
     */
    public void setPfdeptid(String  pfdeptid){
        this.pfdeptid = pfdeptid ;
        this.modify("pfdeptid",pfdeptid);
    }

    /**
     * 设置 [HDATE]
     */
    public void setHdate(Timestamp  hdate){
        this.hdate = hdate ;
        this.modify("hdate",hdate);
    }

    /**
     * 设置 [CHANGENUM]
     */
    public void setChangenum(Double  changenum){
        this.changenum = changenum ;
        this.modify("changenum",changenum);
    }

    /**
     * 设置 [RESSON]
     */
    public void setResson(String  resson){
        this.resson = resson ;
        this.modify("resson",resson);
    }

    /**
     * 设置 [PFEMPID]
     */
    public void setPfempid(String  pfempid){
        this.pfempid = pfempid ;
        this.modify("pfempid",pfempid);
    }

    /**
     * 设置 [EMITEMPUSEID]
     */
    public void setEmitempuseid(String  emitempuseid){
        this.emitempuseid = emitempuseid ;
        this.modify("emitempuseid",emitempuseid);
    }


}


