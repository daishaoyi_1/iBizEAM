package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKPMap;
import cn.ibizlab.eam.core.eam_core.service.IEMEQKPMapService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备关键点关系" })
@RestController("WebApi-emeqkpmap")
@RequestMapping("")
public class EMEQKPMapResource {

    @Autowired
    public IEMEQKPMapService emeqkpmapService;

    @Autowired
    @Lazy
    public EMEQKPMapMapping emeqkpmapMapping;

    @PreAuthorize("hasPermission(this.emeqkpmapMapping.toDomain(#emeqkpmapdto),'eam-EMEQKPMap-Create')")
    @ApiOperation(value = "新建设备关键点关系", tags = {"设备关键点关系" },  notes = "新建设备关键点关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkpmaps")
    public ResponseEntity<EMEQKPMapDTO> create(@Validated @RequestBody EMEQKPMapDTO emeqkpmapdto) {
        EMEQKPMap domain = emeqkpmapMapping.toDomain(emeqkpmapdto);
		emeqkpmapService.create(domain);
        EMEQKPMapDTO dto = emeqkpmapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkpmapMapping.toDomain(#emeqkpmapdtos),'eam-EMEQKPMap-Create')")
    @ApiOperation(value = "批量新建设备关键点关系", tags = {"设备关键点关系" },  notes = "批量新建设备关键点关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkpmaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQKPMapDTO> emeqkpmapdtos) {
        emeqkpmapService.createBatch(emeqkpmapMapping.toDomain(emeqkpmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqkpmap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqkpmapService.get(#emeqkpmap_id),'eam-EMEQKPMap-Update')")
    @ApiOperation(value = "更新设备关键点关系", tags = {"设备关键点关系" },  notes = "更新设备关键点关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkpmaps/{emeqkpmap_id}")
    public ResponseEntity<EMEQKPMapDTO> update(@PathVariable("emeqkpmap_id") String emeqkpmap_id, @RequestBody EMEQKPMapDTO emeqkpmapdto) {
		EMEQKPMap domain  = emeqkpmapMapping.toDomain(emeqkpmapdto);
        domain .setEmeqkpmapid(emeqkpmap_id);
		emeqkpmapService.update(domain );
		EMEQKPMapDTO dto = emeqkpmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkpmapService.getEmeqkpmapByEntities(this.emeqkpmapMapping.toDomain(#emeqkpmapdtos)),'eam-EMEQKPMap-Update')")
    @ApiOperation(value = "批量更新设备关键点关系", tags = {"设备关键点关系" },  notes = "批量更新设备关键点关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkpmaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQKPMapDTO> emeqkpmapdtos) {
        emeqkpmapService.updateBatch(emeqkpmapMapping.toDomain(emeqkpmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqkpmapService.get(#emeqkpmap_id),'eam-EMEQKPMap-Remove')")
    @ApiOperation(value = "删除设备关键点关系", tags = {"设备关键点关系" },  notes = "删除设备关键点关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkpmaps/{emeqkpmap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqkpmap_id") String emeqkpmap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqkpmapService.remove(emeqkpmap_id));
    }

    @PreAuthorize("hasPermission(this.emeqkpmapService.getEmeqkpmapByIds(#ids),'eam-EMEQKPMap-Remove')")
    @ApiOperation(value = "批量删除设备关键点关系", tags = {"设备关键点关系" },  notes = "批量删除设备关键点关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkpmaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqkpmapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqkpmapMapping.toDomain(returnObject.body),'eam-EMEQKPMap-Get')")
    @ApiOperation(value = "获取设备关键点关系", tags = {"设备关键点关系" },  notes = "获取设备关键点关系")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkpmaps/{emeqkpmap_id}")
    public ResponseEntity<EMEQKPMapDTO> get(@PathVariable("emeqkpmap_id") String emeqkpmap_id) {
        EMEQKPMap domain = emeqkpmapService.get(emeqkpmap_id);
        EMEQKPMapDTO dto = emeqkpmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备关键点关系草稿", tags = {"设备关键点关系" },  notes = "获取设备关键点关系草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkpmaps/getdraft")
    public ResponseEntity<EMEQKPMapDTO> getDraft(EMEQKPMapDTO dto) {
        EMEQKPMap domain = emeqkpmapMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkpmapMapping.toDto(emeqkpmapService.getDraft(domain)));
    }

    @ApiOperation(value = "检查设备关键点关系", tags = {"设备关键点关系" },  notes = "检查设备关键点关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkpmaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQKPMapDTO emeqkpmapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqkpmapService.checkKey(emeqkpmapMapping.toDomain(emeqkpmapdto)));
    }

    @PreAuthorize("hasPermission(this.emeqkpmapMapping.toDomain(#emeqkpmapdto),'eam-EMEQKPMap-Save')")
    @ApiOperation(value = "保存设备关键点关系", tags = {"设备关键点关系" },  notes = "保存设备关键点关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkpmaps/save")
    public ResponseEntity<EMEQKPMapDTO> save(@RequestBody EMEQKPMapDTO emeqkpmapdto) {
        EMEQKPMap domain = emeqkpmapMapping.toDomain(emeqkpmapdto);
        emeqkpmapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkpmapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqkpmapMapping.toDomain(#emeqkpmapdtos),'eam-EMEQKPMap-Save')")
    @ApiOperation(value = "批量保存设备关键点关系", tags = {"设备关键点关系" },  notes = "批量保存设备关键点关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkpmaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQKPMapDTO> emeqkpmapdtos) {
        emeqkpmapService.saveBatch(emeqkpmapMapping.toDomain(emeqkpmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKPMap-searchDefault-all') and hasPermission(#context,'eam-EMEQKPMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备关键点关系" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqkpmaps/fetchdefault")
	public ResponseEntity<List<EMEQKPMapDTO>> fetchDefault(EMEQKPMapSearchContext context) {
        Page<EMEQKPMap> domains = emeqkpmapService.searchDefault(context) ;
        List<EMEQKPMapDTO> list = emeqkpmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKPMap-searchDefault-all') and hasPermission(#context,'eam-EMEQKPMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备关键点关系" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqkpmaps/searchdefault")
	public ResponseEntity<Page<EMEQKPMapDTO>> searchDefault(@RequestBody EMEQKPMapSearchContext context) {
        Page<EMEQKPMap> domains = emeqkpmapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkpmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

