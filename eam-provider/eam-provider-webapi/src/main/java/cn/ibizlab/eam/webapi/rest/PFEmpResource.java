package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmp;
import cn.ibizlab.eam.core.eam_pf.service.IPFEmpService;
import cn.ibizlab.eam.core.eam_pf.filter.PFEmpSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"职员" })
@RestController("WebApi-pfemp")
@RequestMapping("")
public class PFEmpResource {

    @Autowired
    public IPFEmpService pfempService;

    @Autowired
    @Lazy
    public PFEmpMapping pfempMapping;

    @PreAuthorize("hasPermission(this.pfempMapping.toDomain(#pfempdto),'eam-PFEmp-Create')")
    @ApiOperation(value = "新建职员", tags = {"职员" },  notes = "新建职员")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemps")
    public ResponseEntity<PFEmpDTO> create(@Validated @RequestBody PFEmpDTO pfempdto) {
        PFEmp domain = pfempMapping.toDomain(pfempdto);
		pfempService.create(domain);
        PFEmpDTO dto = pfempMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfempMapping.toDomain(#pfempdtos),'eam-PFEmp-Create')")
    @ApiOperation(value = "批量新建职员", tags = {"职员" },  notes = "批量新建职员")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFEmpDTO> pfempdtos) {
        pfempService.createBatch(pfempMapping.toDomain(pfempdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pfemp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pfempService.get(#pfemp_id),'eam-PFEmp-Update')")
    @ApiOperation(value = "更新职员", tags = {"职员" },  notes = "更新职员")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfemps/{pfemp_id}")
    public ResponseEntity<PFEmpDTO> update(@PathVariable("pfemp_id") String pfemp_id, @RequestBody PFEmpDTO pfempdto) {
		PFEmp domain  = pfempMapping.toDomain(pfempdto);
        domain .setPfempid(pfemp_id);
		pfempService.update(domain );
		PFEmpDTO dto = pfempMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfempService.getPfempByEntities(this.pfempMapping.toDomain(#pfempdtos)),'eam-PFEmp-Update')")
    @ApiOperation(value = "批量更新职员", tags = {"职员" },  notes = "批量更新职员")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfemps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFEmpDTO> pfempdtos) {
        pfempService.updateBatch(pfempMapping.toDomain(pfempdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pfempService.get(#pfemp_id),'eam-PFEmp-Remove')")
    @ApiOperation(value = "删除职员", tags = {"职员" },  notes = "删除职员")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfemps/{pfemp_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pfemp_id") String pfemp_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pfempService.remove(pfemp_id));
    }

    @PreAuthorize("hasPermission(this.pfempService.getPfempByIds(#ids),'eam-PFEmp-Remove')")
    @ApiOperation(value = "批量删除职员", tags = {"职员" },  notes = "批量删除职员")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfemps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pfempService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pfempMapping.toDomain(returnObject.body),'eam-PFEmp-Get')")
    @ApiOperation(value = "获取职员", tags = {"职员" },  notes = "获取职员")
	@RequestMapping(method = RequestMethod.GET, value = "/pfemps/{pfemp_id}")
    public ResponseEntity<PFEmpDTO> get(@PathVariable("pfemp_id") String pfemp_id) {
        PFEmp domain = pfempService.get(pfemp_id);
        PFEmpDTO dto = pfempMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取职员草稿", tags = {"职员" },  notes = "获取职员草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pfemps/getdraft")
    public ResponseEntity<PFEmpDTO> getDraft(PFEmpDTO dto) {
        PFEmp domain = pfempMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(pfempMapping.toDto(pfempService.getDraft(domain)));
    }

    @ApiOperation(value = "检查职员", tags = {"职员" },  notes = "检查职员")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFEmpDTO pfempdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pfempService.checkKey(pfempMapping.toDomain(pfempdto)));
    }

    @PreAuthorize("hasPermission(this.pfempMapping.toDomain(#pfempdto),'eam-PFEmp-Save')")
    @ApiOperation(value = "保存职员", tags = {"职员" },  notes = "保存职员")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemps/save")
    public ResponseEntity<PFEmpDTO> save(@RequestBody PFEmpDTO pfempdto) {
        PFEmp domain = pfempMapping.toDomain(pfempdto);
        pfempService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(pfempMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.pfempMapping.toDomain(#pfempdtos),'eam-PFEmp-Save')")
    @ApiOperation(value = "批量保存职员", tags = {"职员" },  notes = "批量保存职员")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFEmpDTO> pfempdtos) {
        pfempService.saveBatch(pfempMapping.toDomain(pfempdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFEmp-searchDefault-all') and hasPermission(#context,'eam-PFEmp-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"职员" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfemps/fetchdefault")
	public ResponseEntity<List<PFEmpDTO>> fetchDefault(PFEmpSearchContext context) {
        Page<PFEmp> domains = pfempService.searchDefault(context) ;
        List<PFEmpDTO> list = pfempMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFEmp-searchDefault-all') and hasPermission(#context,'eam-PFEmp-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"职员" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfemps/searchdefault")
	public ResponseEntity<Page<PFEmpDTO>> searchDefault(@RequestBody PFEmpSearchContext context) {
        Page<PFEmp> domains = pfempService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfempMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFEmp-searchDeptEmp-all') and hasPermission(#context,'eam-PFEmp-Get')")
	@ApiOperation(value = "获取部门下职员", tags = {"职员" } ,notes = "获取部门下职员")
    @RequestMapping(method= RequestMethod.GET , value="/pfemps/fetchdeptemp")
	public ResponseEntity<List<PFEmpDTO>> fetchDeptEmp(PFEmpSearchContext context) {
        Page<PFEmp> domains = pfempService.searchDeptEmp(context) ;
        List<PFEmpDTO> list = pfempMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFEmp-searchDeptEmp-all') and hasPermission(#context,'eam-PFEmp-Get')")
	@ApiOperation(value = "查询部门下职员", tags = {"职员" } ,notes = "查询部门下职员")
    @RequestMapping(method= RequestMethod.POST , value="/pfemps/searchdeptemp")
	public ResponseEntity<Page<PFEmpDTO>> searchDeptEmp(@RequestBody PFEmpSearchContext context) {
        Page<PFEmp> domains = pfempService.searchDeptEmp(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfempMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

