package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEMap;
import cn.ibizlab.eam.core.eam_core.service.IEMRFODEMapService;
import cn.ibizlab.eam.core.eam_core.filter.EMRFODEMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"现象引用" })
@RestController("WebApi-emrfodemap")
@RequestMapping("")
public class EMRFODEMapResource {

    @Autowired
    public IEMRFODEMapService emrfodemapService;

    @Autowired
    @Lazy
    public EMRFODEMapMapping emrfodemapMapping;

    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdto),'eam-EMRFODEMap-Create')")
    @ApiOperation(value = "新建现象引用", tags = {"现象引用" },  notes = "新建现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodemaps")
    public ResponseEntity<EMRFODEMapDTO> create(@Validated @RequestBody EMRFODEMapDTO emrfodemapdto) {
        EMRFODEMap domain = emrfodemapMapping.toDomain(emrfodemapdto);
		emrfodemapService.create(domain);
        EMRFODEMapDTO dto = emrfodemapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdtos),'eam-EMRFODEMap-Create')")
    @ApiOperation(value = "批量新建现象引用", tags = {"现象引用" },  notes = "批量新建现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodemaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRFODEMapDTO> emrfodemapdtos) {
        emrfodemapService.createBatch(emrfodemapMapping.toDomain(emrfodemapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfodemap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfodemapService.get(#emrfodemap_id),'eam-EMRFODEMap-Update')")
    @ApiOperation(value = "更新现象引用", tags = {"现象引用" },  notes = "更新现象引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodemaps/{emrfodemap_id}")
    public ResponseEntity<EMRFODEMapDTO> update(@PathVariable("emrfodemap_id") String emrfodemap_id, @RequestBody EMRFODEMapDTO emrfodemapdto) {
		EMRFODEMap domain  = emrfodemapMapping.toDomain(emrfodemapdto);
        domain .setEmrfodemapid(emrfodemap_id);
		emrfodemapService.update(domain );
		EMRFODEMapDTO dto = emrfodemapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodemapService.getEmrfodemapByEntities(this.emrfodemapMapping.toDomain(#emrfodemapdtos)),'eam-EMRFODEMap-Update')")
    @ApiOperation(value = "批量更新现象引用", tags = {"现象引用" },  notes = "批量更新现象引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodemaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRFODEMapDTO> emrfodemapdtos) {
        emrfodemapService.updateBatch(emrfodemapMapping.toDomain(emrfodemapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfodemapService.get(#emrfodemap_id),'eam-EMRFODEMap-Remove')")
    @ApiOperation(value = "删除现象引用", tags = {"现象引用" },  notes = "删除现象引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodemaps/{emrfodemap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrfodemap_id") String emrfodemap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrfodemapService.remove(emrfodemap_id));
    }

    @PreAuthorize("hasPermission(this.emrfodemapService.getEmrfodemapByIds(#ids),'eam-EMRFODEMap-Remove')")
    @ApiOperation(value = "批量删除现象引用", tags = {"现象引用" },  notes = "批量删除现象引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodemaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrfodemapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfodemapMapping.toDomain(returnObject.body),'eam-EMRFODEMap-Get')")
    @ApiOperation(value = "获取现象引用", tags = {"现象引用" },  notes = "获取现象引用")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodemaps/{emrfodemap_id}")
    public ResponseEntity<EMRFODEMapDTO> get(@PathVariable("emrfodemap_id") String emrfodemap_id) {
        EMRFODEMap domain = emrfodemapService.get(emrfodemap_id);
        EMRFODEMapDTO dto = emrfodemapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取现象引用草稿", tags = {"现象引用" },  notes = "获取现象引用草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodemaps/getdraft")
    public ResponseEntity<EMRFODEMapDTO> getDraft(EMRFODEMapDTO dto) {
        EMRFODEMap domain = emrfodemapMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emrfodemapMapping.toDto(emrfodemapService.getDraft(domain)));
    }

    @ApiOperation(value = "检查现象引用", tags = {"现象引用" },  notes = "检查现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodemaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRFODEMapDTO emrfodemapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfodemapService.checkKey(emrfodemapMapping.toDomain(emrfodemapdto)));
    }

    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdto),'eam-EMRFODEMap-Save')")
    @ApiOperation(value = "保存现象引用", tags = {"现象引用" },  notes = "保存现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodemaps/save")
    public ResponseEntity<EMRFODEMapDTO> save(@RequestBody EMRFODEMapDTO emrfodemapdto) {
        EMRFODEMap domain = emrfodemapMapping.toDomain(emrfodemapdto);
        emrfodemapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfodemapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdtos),'eam-EMRFODEMap-Save')")
    @ApiOperation(value = "批量保存现象引用", tags = {"现象引用" },  notes = "批量保存现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodemaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRFODEMapDTO> emrfodemapdtos) {
        emrfodemapService.saveBatch(emrfodemapMapping.toDomain(emrfodemapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODEMap-searchDefault-all') and hasPermission(#context,'eam-EMRFODEMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"现象引用" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodemaps/fetchdefault")
	public ResponseEntity<List<EMRFODEMapDTO>> fetchDefault(EMRFODEMapSearchContext context) {
        Page<EMRFODEMap> domains = emrfodemapService.searchDefault(context) ;
        List<EMRFODEMapDTO> list = emrfodemapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODEMap-searchDefault-all') and hasPermission(#context,'eam-EMRFODEMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"现象引用" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodemaps/searchdefault")
	public ResponseEntity<Page<EMRFODEMapDTO>> searchDefault(@RequestBody EMRFODEMapSearchContext context) {
        Page<EMRFODEMap> domains = emrfodemapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfodemapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdto),'eam-EMRFODEMap-Create')")
    @ApiOperation(value = "根据现象建立现象引用", tags = {"现象引用" },  notes = "根据现象建立现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfodemaps")
    public ResponseEntity<EMRFODEMapDTO> createByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFODEMapDTO emrfodemapdto) {
        EMRFODEMap domain = emrfodemapMapping.toDomain(emrfodemapdto);
        domain.setRfodeid(emrfode_id);
		emrfodemapService.create(domain);
        EMRFODEMapDTO dto = emrfodemapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdtos),'eam-EMRFODEMap-Create')")
    @ApiOperation(value = "根据现象批量建立现象引用", tags = {"现象引用" },  notes = "根据现象批量建立现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfodemaps/batch")
    public ResponseEntity<Boolean> createBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFODEMapDTO> emrfodemapdtos) {
        List<EMRFODEMap> domainlist=emrfodemapMapping.toDomain(emrfodemapdtos);
        for(EMRFODEMap domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfodemapService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfodemap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfodemapService.get(#emrfodemap_id),'eam-EMRFODEMap-Update')")
    @ApiOperation(value = "根据现象更新现象引用", tags = {"现象引用" },  notes = "根据现象更新现象引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfodemaps/{emrfodemap_id}")
    public ResponseEntity<EMRFODEMapDTO> updateByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfodemap_id") String emrfodemap_id, @RequestBody EMRFODEMapDTO emrfodemapdto) {
        EMRFODEMap domain = emrfodemapMapping.toDomain(emrfodemapdto);
        domain.setRfodeid(emrfode_id);
        domain.setEmrfodemapid(emrfodemap_id);
		emrfodemapService.update(domain);
        EMRFODEMapDTO dto = emrfodemapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodemapService.getEmrfodemapByEntities(this.emrfodemapMapping.toDomain(#emrfodemapdtos)),'eam-EMRFODEMap-Update')")
    @ApiOperation(value = "根据现象批量更新现象引用", tags = {"现象引用" },  notes = "根据现象批量更新现象引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfodemaps/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFODEMapDTO> emrfodemapdtos) {
        List<EMRFODEMap> domainlist=emrfodemapMapping.toDomain(emrfodemapdtos);
        for(EMRFODEMap domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfodemapService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfodemapService.get(#emrfodemap_id),'eam-EMRFODEMap-Remove')")
    @ApiOperation(value = "根据现象删除现象引用", tags = {"现象引用" },  notes = "根据现象删除现象引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfodemaps/{emrfodemap_id}")
    public ResponseEntity<Boolean> removeByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfodemap_id") String emrfodemap_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfodemapService.remove(emrfodemap_id));
    }

    @PreAuthorize("hasPermission(this.emrfodemapService.getEmrfodemapByIds(#ids),'eam-EMRFODEMap-Remove')")
    @ApiOperation(value = "根据现象批量删除现象引用", tags = {"现象引用" },  notes = "根据现象批量删除现象引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfodemaps/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFODE(@RequestBody List<String> ids) {
        emrfodemapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfodemapMapping.toDomain(returnObject.body),'eam-EMRFODEMap-Get')")
    @ApiOperation(value = "根据现象获取现象引用", tags = {"现象引用" },  notes = "根据现象获取现象引用")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfodemaps/{emrfodemap_id}")
    public ResponseEntity<EMRFODEMapDTO> getByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfodemap_id") String emrfodemap_id) {
        EMRFODEMap domain = emrfodemapService.get(emrfodemap_id);
        EMRFODEMapDTO dto = emrfodemapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据现象获取现象引用草稿", tags = {"现象引用" },  notes = "根据现象获取现象引用草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfodemaps/getdraft")
    public ResponseEntity<EMRFODEMapDTO> getDraftByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, EMRFODEMapDTO dto) {
        EMRFODEMap domain = emrfodemapMapping.toDomain(dto);
        domain.setRfodeid(emrfode_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfodemapMapping.toDto(emrfodemapService.getDraft(domain)));
    }

    @ApiOperation(value = "根据现象检查现象引用", tags = {"现象引用" },  notes = "根据现象检查现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfodemaps/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFODEMapDTO emrfodemapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfodemapService.checkKey(emrfodemapMapping.toDomain(emrfodemapdto)));
    }

    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdto),'eam-EMRFODEMap-Save')")
    @ApiOperation(value = "根据现象保存现象引用", tags = {"现象引用" },  notes = "根据现象保存现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfodemaps/save")
    public ResponseEntity<EMRFODEMapDTO> saveByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFODEMapDTO emrfodemapdto) {
        EMRFODEMap domain = emrfodemapMapping.toDomain(emrfodemapdto);
        domain.setRfodeid(emrfode_id);
        emrfodemapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfodemapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfodemapMapping.toDomain(#emrfodemapdtos),'eam-EMRFODEMap-Save')")
    @ApiOperation(value = "根据现象批量保存现象引用", tags = {"现象引用" },  notes = "根据现象批量保存现象引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfodemaps/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFODEMapDTO> emrfodemapdtos) {
        List<EMRFODEMap> domainlist=emrfodemapMapping.toDomain(emrfodemapdtos);
        for(EMRFODEMap domain:domainlist){
             domain.setRfodeid(emrfode_id);
        }
        emrfodemapService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODEMap-searchDefault-all') and hasPermission(#context,'eam-EMRFODEMap-Get')")
	@ApiOperation(value = "根据现象获取DEFAULT", tags = {"现象引用" } ,notes = "根据现象获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodes/{emrfode_id}/emrfodemaps/fetchdefault")
	public ResponseEntity<List<EMRFODEMapDTO>> fetchEMRFODEMapDefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id,EMRFODEMapSearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFODEMap> domains = emrfodemapService.searchDefault(context) ;
        List<EMRFODEMapDTO> list = emrfodemapMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODEMap-searchDefault-all') and hasPermission(#context,'eam-EMRFODEMap-Get')")
	@ApiOperation(value = "根据现象查询DEFAULT", tags = {"现象引用" } ,notes = "根据现象查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodes/{emrfode_id}/emrfodemaps/searchdefault")
	public ResponseEntity<Page<EMRFODEMapDTO>> searchEMRFODEMapDefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFODEMapSearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFODEMap> domains = emrfodemapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfodemapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

