package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMDRWG1DTO]
 */
@Data
public class EMDRWG1DTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMDRWG1NAME]
     *
     */
    @JSONField(name = "emdrwg1name")
    @JsonProperty("emdrwg1name")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emdrwg1name;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String content;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [DRWGCODE]
     *
     */
    @JSONField(name = "drwgcode")
    @JsonProperty("drwgcode")
    @NotBlank(message = "[文档代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String drwgcode;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMDRWG1ID]
     *
     */
    @JSONField(name = "emdrwg1id")
    @JsonProperty("emdrwg1id")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emdrwg1id;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [DRWGINFO]
     *
     */
    @JSONField(name = "drwginfo")
    @JsonProperty("drwginfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String drwginfo;


    /**
     * 设置 [EMDRWG1NAME]
     */
    public void setEmdrwg1name(String  emdrwg1name){
        this.emdrwg1name = emdrwg1name ;
        this.modify("emdrwg1name",emdrwg1name);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [DRWGCODE]
     */
    public void setDrwgcode(String  drwgcode){
        this.drwgcode = drwgcode ;
        this.modify("drwgcode",drwgcode);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }


}


