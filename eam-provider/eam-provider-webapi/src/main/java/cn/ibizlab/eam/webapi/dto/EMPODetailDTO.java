package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMPODetailDTO]
 */
@Data
public class EMPODetailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [CIVO]
     *
     */
    @JSONField(name = "civo")
    @JsonProperty("civo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String civo;

    /**
     * 属性 [RDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("rdate")
    private Timestamp rdate;

    /**
     * 属性 [YIJU]
     *
     */
    @JSONField(name = "yiju")
    @JsonProperty("yiju")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String yiju;

    /**
     * 属性 [ISRESTART]
     *
     */
    @JSONField(name = "isrestart")
    @JsonProperty("isrestart")
    private Integer isrestart;

    /**
     * 属性 [CIVOCOPY]
     *
     */
    @JSONField(name = "civocopy")
    @JsonProperty("civocopy")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String civocopy;

    /**
     * 属性 [SUMDIFF]
     *
     */
    @JSONField(name = "sumdiff")
    @JsonProperty("sumdiff")
    private Double sumdiff;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [PRICEDIFF]
     *
     */
    @JSONField(name = "pricediff")
    @JsonProperty("pricediff")
    private Double pricediff;

    /**
     * 属性 [TAXRATE]
     *
     */
    @JSONField(name = "taxrate")
    @JsonProperty("taxrate")
    private Double taxrate;

    /**
     * 属性 [AVGTAXFEE]
     *
     */
    @JSONField(name = "avgtaxfee")
    @JsonProperty("avgtaxfee")
    private Double avgtaxfee;

    /**
     * 属性 [LISTPRICE]
     *
     */
    @JSONField(name = "listprice")
    @JsonProperty("listprice")
    private Double listprice;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @NotNull(message = "[单价]不允许为空!")
    private Double price;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [UNITRATE]
     *
     */
    @JSONField(name = "unitrate")
    @JsonProperty("unitrate")
    private Double unitrate;

    /**
     * 属性 [EMPODETAILID]
     *
     */
    @JSONField(name = "empodetailid")
    @JsonProperty("empodetailid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empodetailid;

    /**
     * 属性 [ITEMDESC]
     *
     */
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String itemdesc;

    /**
     * 属性 [SHF]
     *
     */
    @JSONField(name = "shf")
    @JsonProperty("shf")
    private Double shf;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [TOTALPRICE]
     *
     */
    @JSONField(name = "totalprice")
    @JsonProperty("totalprice")
    private Double totalprice;

    /**
     * 属性 [PODETAILSTATE]
     *
     */
    @JSONField(name = "podetailstate")
    @JsonProperty("podetailstate")
    private Integer podetailstate;

    /**
     * 属性 [RSUM]
     *
     */
    @JSONField(name = "rsum")
    @JsonProperty("rsum")
    private Double rsum;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [PODETAILINFO]
     *
     */
    @JSONField(name = "podetailinfo")
    @JsonProperty("podetailinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String podetailinfo;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [SAPSL]
     *
     */
    @JSONField(name = "sapsl")
    @JsonProperty("sapsl")
    @NotBlank(message = "[sap税率]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sapsl;

    /**
     * 属性 [AVGTSFEE]
     *
     */
    @JSONField(name = "avgtsfee")
    @JsonProperty("avgtsfee")
    private Double avgtsfee;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wfinstanceid;

    /**
     * 属性 [SUMALL]
     *
     */
    @JSONField(name = "sumall")
    @JsonProperty("sumall")
    private Double sumall;

    /**
     * 属性 [DISCNT]
     *
     */
    @JSONField(name = "discnt")
    @JsonProperty("discnt")
    @NotNull(message = "[折扣(%)]不允许为空!")
    private Double discnt;

    /**
     * 属性 [ATTPRICE]
     *
     */
    @JSONField(name = "attprice")
    @JsonProperty("attprice")
    private Integer attprice;

    /**
     * 属性 [ORDERFLAG]
     *
     */
    @JSONField(name = "orderflag")
    @JsonProperty("orderflag")
    private Integer orderflag;

    /**
     * 属性 [RPRICE]
     *
     */
    @JSONField(name = "rprice")
    @JsonProperty("rprice")
    private Double rprice;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wfstep;

    /**
     * 属性 [PSUM]
     *
     */
    @JSONField(name = "psum")
    @JsonProperty("psum")
    @NotNull(message = "[订货数量]不允许为空!")
    private Double psum;

    /**
     * 属性 [EMPODETAILNAME]
     *
     */
    @JSONField(name = "empodetailname")
    @JsonProperty("empodetailname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empodetailname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [SUNITID]
     *
     */
    @JSONField(name = "sunitid")
    @JsonProperty("sunitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sunitid;

    /**
     * 属性 [USETO]
     *
     */
    @JSONField(name = "useto")
    @JsonProperty("useto")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String useto;

    /**
     * 属性 [POWFSTEP]
     *
     */
    @JSONField(name = "powfstep")
    @JsonProperty("powfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String powfstep;

    /**
     * 属性 [SUNITNAME]
     *
     */
    @JSONField(name = "sunitname")
    @JsonProperty("sunitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sunitname;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String objid;

    /**
     * 属性 [EQUIPS]
     *
     */
    @JSONField(name = "equips")
    @JsonProperty("equips")
    @Size(min = 0, max = 4000, message = "内容长度必须小于等于[4000]")
    private String equips;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String equipid;

    /**
     * 属性 [RUNITNAME]
     *
     */
    @JSONField(name = "runitname")
    @JsonProperty("runitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String runitname;

    /**
     * 属性 [AVGPRICE]
     *
     */
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    private Double avgprice;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itembtypeid;

    /**
     * 属性 [WPLISTNAME]
     *
     */
    @JSONField(name = "wplistname")
    @JsonProperty("wplistname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String wplistname;

    /**
     * 属性 [TEAMID]
     *
     */
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String teamid;

    /**
     * 属性 [POSTATE]
     *
     */
    @JSONField(name = "postate")
    @JsonProperty("postate")
    private Integer postate;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipname;

    /**
     * 属性 [PONAME]
     *
     */
    @JSONField(name = "poname")
    @JsonProperty("poname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String poname;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unitname;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String unitid;

    /**
     * 属性 [POID]
     *
     */
    @JSONField(name = "poid")
    @JsonProperty("poid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String poid;

    /**
     * 属性 [RUNITID]
     *
     */
    @JSONField(name = "runitid")
    @JsonProperty("runitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String runitid;

    /**
     * 属性 [WPLISTID]
     *
     */
    @JSONField(name = "wplistid")
    @JsonProperty("wplistid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String wplistid;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rempid;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rempname;

    /**
     * 属性 [POREMPNAME]
     *
     */
    @JSONField(name = "porempname")
    @JsonProperty("porempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String porempname;

    /**
     * 属性 [POREMPID]
     *
     */
    @JSONField(name = "porempid")
    @JsonProperty("porempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String porempid;

    /**
     * 属性 [EMPID]
     *
     */
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String empid;

    /**
     * 属性 [EMPNAME]
     *
     */
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String empname;


    /**
     * 设置 [CIVO]
     */
    public void setCivo(String  civo){
        this.civo = civo ;
        this.modify("civo",civo);
    }

    /**
     * 设置 [RDATE]
     */
    public void setRdate(Timestamp  rdate){
        this.rdate = rdate ;
        this.modify("rdate",rdate);
    }

    /**
     * 设置 [YIJU]
     */
    public void setYiju(String  yiju){
        this.yiju = yiju ;
        this.modify("yiju",yiju);
    }

    /**
     * 设置 [ISRESTART]
     */
    public void setIsrestart(Integer  isrestart){
        this.isrestart = isrestart ;
        this.modify("isrestart",isrestart);
    }

    /**
     * 设置 [CIVOCOPY]
     */
    public void setCivocopy(String  civocopy){
        this.civocopy = civocopy ;
        this.modify("civocopy",civocopy);
    }

    /**
     * 设置 [TAXRATE]
     */
    public void setTaxrate(Double  taxrate){
        this.taxrate = taxrate ;
        this.modify("taxrate",taxrate);
    }

    /**
     * 设置 [AVGTAXFEE]
     */
    public void setAvgtaxfee(Double  avgtaxfee){
        this.avgtaxfee = avgtaxfee ;
        this.modify("avgtaxfee",avgtaxfee);
    }

    /**
     * 设置 [LISTPRICE]
     */
    public void setListprice(Double  listprice){
        this.listprice = listprice ;
        this.modify("listprice",listprice);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [UNITRATE]
     */
    public void setUnitrate(Double  unitrate){
        this.unitrate = unitrate ;
        this.modify("unitrate",unitrate);
    }

    /**
     * 设置 [ITEMDESC]
     */
    public void setItemdesc(String  itemdesc){
        this.itemdesc = itemdesc ;
        this.modify("itemdesc",itemdesc);
    }

    /**
     * 设置 [SHF]
     */
    public void setShf(Double  shf){
        this.shf = shf ;
        this.modify("shf",shf);
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [PODETAILSTATE]
     */
    public void setPodetailstate(Integer  podetailstate){
        this.podetailstate = podetailstate ;
        this.modify("podetailstate",podetailstate);
    }

    /**
     * 设置 [RSUM]
     */
    public void setRsum(Double  rsum){
        this.rsum = rsum ;
        this.modify("rsum",rsum);
    }

    /**
     * 设置 [SAPSL]
     */
    public void setSapsl(String  sapsl){
        this.sapsl = sapsl ;
        this.modify("sapsl",sapsl);
    }

    /**
     * 设置 [AVGTSFEE]
     */
    public void setAvgtsfee(Double  avgtsfee){
        this.avgtsfee = avgtsfee ;
        this.modify("avgtsfee",avgtsfee);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [DISCNT]
     */
    public void setDiscnt(Double  discnt){
        this.discnt = discnt ;
        this.modify("discnt",discnt);
    }

    /**
     * 设置 [ORDERFLAG]
     */
    public void setOrderflag(Integer  orderflag){
        this.orderflag = orderflag ;
        this.modify("orderflag",orderflag);
    }

    /**
     * 设置 [RPRICE]
     */
    public void setRprice(Double  rprice){
        this.rprice = rprice ;
        this.modify("rprice",rprice);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [PSUM]
     */
    public void setPsum(Double  psum){
        this.psum = psum ;
        this.modify("psum",psum);
    }

    /**
     * 设置 [EMPODETAILNAME]
     */
    public void setEmpodetailname(String  empodetailname){
        this.empodetailname = empodetailname ;
        this.modify("empodetailname",empodetailname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [UNITID]
     */
    public void setUnitid(String  unitid){
        this.unitid = unitid ;
        this.modify("unitid",unitid);
    }

    /**
     * 设置 [POID]
     */
    public void setPoid(String  poid){
        this.poid = poid ;
        this.modify("poid",poid);
    }

    /**
     * 设置 [RUNITID]
     */
    public void setRunitid(String  runitid){
        this.runitid = runitid ;
        this.modify("runitid",runitid);
    }

    /**
     * 设置 [WPLISTID]
     */
    public void setWplistid(String  wplistid){
        this.wplistid = wplistid ;
        this.modify("wplistid",wplistid);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [EMPID]
     */
    public void setEmpid(String  empid){
        this.empid = empid ;
        this.modify("empid",empid);
    }


}


