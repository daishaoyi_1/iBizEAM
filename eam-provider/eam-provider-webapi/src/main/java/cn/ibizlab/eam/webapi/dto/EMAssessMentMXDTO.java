package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMAssessMentMXDTO]
 */
@Data
public class EMAssessMentMXDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [PERSON]
     *
     */
    @JSONField(name = "person")
    @JsonProperty("person")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    private String person;

    /**
     * 属性 [PFEMPID]
     *
     */
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfempid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [ASSESSMENT]
     *
     */
    @JSONField(name = "assessment")
    @JsonProperty("assessment")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assessment;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [EMASSESSMENTMXID]
     *
     */
    @JSONField(name = "emassessmentmxid")
    @JsonProperty("emassessmentmxid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassessmentmxid;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [XH]
     *
     */
    @JSONField(name = "xh")
    @JsonProperty("xh")
    private Integer xh;

    /**
     * 属性 [TIMESET]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "timeset" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("timeset")
    private Timestamp timeset;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [TIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "time" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("time")
    private Timestamp time;

    /**
     * 属性 [EMASSESSMENTMXNAME]
     *
     */
    @JSONField(name = "emassessmentmxname")
    @JsonProperty("emassessmentmxname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassessmentmxname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [PFEMPNAME]
     *
     */
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pfempname;

    /**
     * 属性 [FINISHDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "finishdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("finishdate")
    private Timestamp finishdate;

    /**
     * 属性 [EMASSESSMENTNAME]
     *
     */
    @JSONField(name = "emassessmentname")
    @JsonProperty("emassessmentname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emassessmentname;

    /**
     * 属性 [EMASSESSMENTID]
     *
     */
    @JSONField(name = "emassessmentid")
    @JsonProperty("emassessmentid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emassessmentid;


    /**
     * 设置 [PERSON]
     */
    public void setPerson(String  person){
        this.person = person ;
        this.modify("person",person);
    }

    /**
     * 设置 [PFEMPID]
     */
    public void setPfempid(String  pfempid){
        this.pfempid = pfempid ;
        this.modify("pfempid",pfempid);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ASSESSMENT]
     */
    public void setAssessment(String  assessment){
        this.assessment = assessment ;
        this.modify("assessment",assessment);
    }

    /**
     * 设置 [XH]
     */
    public void setXh(Integer  xh){
        this.xh = xh ;
        this.modify("xh",xh);
    }

    /**
     * 设置 [TIMESET]
     */
    public void setTimeset(Timestamp  timeset){
        this.timeset = timeset ;
        this.modify("timeset",timeset);
    }

    /**
     * 设置 [TIME]
     */
    public void setTime(Timestamp  time){
        this.time = time ;
        this.modify("time",time);
    }

    /**
     * 设置 [EMASSESSMENTMXNAME]
     */
    public void setEmassessmentmxname(String  emassessmentmxname){
        this.emassessmentmxname = emassessmentmxname ;
        this.modify("emassessmentmxname",emassessmentmxname);
    }

    /**
     * 设置 [PFEMPNAME]
     */
    public void setPfempname(String  pfempname){
        this.pfempname = pfempname ;
        this.modify("pfempname",pfempname);
    }

    /**
     * 设置 [FINISHDATE]
     */
    public void setFinishdate(Timestamp  finishdate){
        this.finishdate = finishdate ;
        this.modify("finishdate",finishdate);
    }

    /**
     * 设置 [EMASSESSMENTID]
     */
    public void setEmassessmentid(String  emassessmentid){
        this.emassessmentid = emassessmentid ;
        this.modify("emassessmentid",emassessmentid);
    }


}


