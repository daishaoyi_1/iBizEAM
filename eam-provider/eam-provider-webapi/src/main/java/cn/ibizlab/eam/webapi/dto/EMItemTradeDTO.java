package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMItemTradeDTO]
 */
@Data
public class EMItemTradeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [BATCODE]
     *
     */
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    @NotBlank(message = "[批次]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String batcode;

    /**
     * 属性 [EMITEMTRADENAME]
     *
     */
    @JSONField(name = "emitemtradename")
    @JsonProperty("emitemtradename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emitemtradename;

    /**
     * 属性 [CIVO]
     *
     */
    @JSONField(name = "civo")
    @JsonProperty("civo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String civo;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [SDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    private Timestamp sdate;

    /**
     * 属性 [TRADESTATE]
     *
     */
    @JSONField(name = "tradestate")
    @JsonProperty("tradestate")
    private Integer tradestate;

    /**
     * 属性 [PUSETYPE]
     *
     */
    @JSONField(name = "pusetype")
    @JsonProperty("pusetype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pusetype;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [PSUM]
     *
     */
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [INOUTFLAG]
     *
     */
    @JSONField(name = "inoutflag")
    @JsonProperty("inoutflag")
    @NotNull(message = "[出入标志]不允许为空!")
    private Integer inoutflag;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [SHF]
     *
     */
    @JSONField(name = "shf")
    @JsonProperty("shf")
    private Double shf;

    /**
     * 属性 [EMITEMTRADETYPE]
     *
     */
    @JSONField(name = "emitemtradetype")
    @JsonProperty("emitemtradetype")
    @NotBlank(message = "[交易分组]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitemtradetype;

    /**
     * 属性 [EMITEMTRADEID]
     *
     */
    @JSONField(name = "emitemtradeid")
    @JsonProperty("emitemtradeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emitemtradeid;

    /**
     * 属性 [ITEMTYPEGROUP]
     *
     */
    @JSONField(name = "itemtypegroup")
    @JsonProperty("itemtypegroup")
    private Integer itemtypegroup;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [ITEMMTYPEID]
     *
     */
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemmtypeid;

    /**
     * 属性 [ITEMBTYPENAME]
     *
     */
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itembtypename;

    /**
     * 属性 [STORENAME]
     *
     */
    @JSONField(name = "storename")
    @JsonProperty("storename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storename;

    /**
     * 属性 [SHFPRICE]
     *
     */
    @JSONField(name = "shfprice")
    @JsonProperty("shfprice")
    private Double shfprice;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itembtypeid;

    /**
     * 属性 [STOCKAMOUNT]
     *
     */
    @JSONField(name = "stockamount")
    @JsonProperty("stockamount")
    private Double stockamount;

    /**
     * 属性 [RNAME]
     *
     */
    @JSONField(name = "rname")
    @JsonProperty("rname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rname;

    /**
     * 属性 [ITEMCODE]
     *
     */
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemcode;

    /**
     * 属性 [STOREPARTNAME]
     *
     */
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String storepartname;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemname;

    /**
     * 属性 [TEAMNAME]
     *
     */
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamname;

    /**
     * 属性 [ITEMMTYPENAME]
     *
     */
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String itemmtypename;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String labservicename;

    /**
     * 属性 [ITEMTYPEID]
     *
     */
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemtypeid;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labserviceid;

    /**
     * 属性 [TEAMID]
     *
     */
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String teamid;

    /**
     * 属性 [RID]
     *
     */
    @JSONField(name = "rid")
    @JsonProperty("rid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rid;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String itemid;

    /**
     * 属性 [STOREID]
     *
     */
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storeid;

    /**
     * 属性 [STOREPARTID]
     *
     */
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storepartid;

    /**
     * 属性 [AEMPID]
     *
     */
    @JSONField(name = "aempid")
    @JsonProperty("aempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aempid;

    /**
     * 属性 [AEMPNAME]
     *
     */
    @JSONField(name = "aempname")
    @JsonProperty("aempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String aempname;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String deptid;

    /**
     * 属性 [DEPTNAME]
     *
     */
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String deptname;

    /**
     * 属性 [SEMPID]
     *
     */
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sempid;

    /**
     * 属性 [SEMPNAME]
     *
     */
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sempname;


    /**
     * 设置 [BATCODE]
     */
    public void setBatcode(String  batcode){
        this.batcode = batcode ;
        this.modify("batcode",batcode);
    }

    /**
     * 设置 [EMITEMTRADENAME]
     */
    public void setEmitemtradename(String  emitemtradename){
        this.emitemtradename = emitemtradename ;
        this.modify("emitemtradename",emitemtradename);
    }

    /**
     * 设置 [CIVO]
     */
    public void setCivo(String  civo){
        this.civo = civo ;
        this.modify("civo",civo);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [SDATE]
     */
    public void setSdate(Timestamp  sdate){
        this.sdate = sdate ;
        this.modify("sdate",sdate);
    }

    /**
     * 设置 [TRADESTATE]
     */
    public void setTradestate(Integer  tradestate){
        this.tradestate = tradestate ;
        this.modify("tradestate",tradestate);
    }

    /**
     * 设置 [PUSETYPE]
     */
    public void setPusetype(String  pusetype){
        this.pusetype = pusetype ;
        this.modify("pusetype",pusetype);
    }

    /**
     * 设置 [PSUM]
     */
    public void setPsum(Double  psum){
        this.psum = psum ;
        this.modify("psum",psum);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [INOUTFLAG]
     */
    public void setInoutflag(Integer  inoutflag){
        this.inoutflag = inoutflag ;
        this.modify("inoutflag",inoutflag);
    }

    /**
     * 设置 [SHF]
     */
    public void setShf(Double  shf){
        this.shf = shf ;
        this.modify("shf",shf);
    }

    /**
     * 设置 [EMITEMTRADETYPE]
     */
    public void setEmitemtradetype(String  emitemtradetype){
        this.emitemtradetype = emitemtradetype ;
        this.modify("emitemtradetype",emitemtradetype);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [TEAMID]
     */
    public void setTeamid(String  teamid){
        this.teamid = teamid ;
        this.modify("teamid",teamid);
    }

    /**
     * 设置 [RID]
     */
    public void setRid(String  rid){
        this.rid = rid ;
        this.modify("rid",rid);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [STOREID]
     */
    public void setStoreid(String  storeid){
        this.storeid = storeid ;
        this.modify("storeid",storeid);
    }

    /**
     * 设置 [STOREPARTID]
     */
    public void setStorepartid(String  storepartid){
        this.storepartid = storepartid ;
        this.modify("storepartid",storepartid);
    }

    /**
     * 设置 [AEMPID]
     */
    public void setAempid(String  aempid){
        this.aempid = aempid ;
        this.modify("aempid",aempid);
    }

    /**
     * 设置 [DEPTID]
     */
    public void setDeptid(String  deptid){
        this.deptid = deptid ;
        this.modify("deptid",deptid);
    }

    /**
     * 设置 [SEMPID]
     */
    public void setSempid(String  sempid){
        this.sempid = sempid ;
        this.modify("sempid",sempid);
    }


}


