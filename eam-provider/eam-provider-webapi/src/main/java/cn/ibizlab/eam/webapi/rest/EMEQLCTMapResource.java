package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTMap;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTMapService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"位置关系" })
@RestController("WebApi-emeqlctmap")
@RequestMapping("")
public class EMEQLCTMapResource {

    @Autowired
    public IEMEQLCTMapService emeqlctmapService;

    @Autowired
    @Lazy
    public EMEQLCTMapMapping emeqlctmapMapping;

    @PreAuthorize("hasPermission(this.emeqlctmapMapping.toDomain(#emeqlctmapdto),'eam-EMEQLCTMap-Create')")
    @ApiOperation(value = "新建位置关系", tags = {"位置关系" },  notes = "新建位置关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctmaps")
    public ResponseEntity<EMEQLCTMapDTO> create(@Validated @RequestBody EMEQLCTMapDTO emeqlctmapdto) {
        EMEQLCTMap domain = emeqlctmapMapping.toDomain(emeqlctmapdto);
		emeqlctmapService.create(domain);
        EMEQLCTMapDTO dto = emeqlctmapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctmapMapping.toDomain(#emeqlctmapdtos),'eam-EMEQLCTMap-Create')")
    @ApiOperation(value = "批量新建位置关系", tags = {"位置关系" },  notes = "批量新建位置关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctmaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQLCTMapDTO> emeqlctmapdtos) {
        emeqlctmapService.createBatch(emeqlctmapMapping.toDomain(emeqlctmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqlctmap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqlctmapService.get(#emeqlctmap_id),'eam-EMEQLCTMap-Update')")
    @ApiOperation(value = "更新位置关系", tags = {"位置关系" },  notes = "更新位置关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctmaps/{emeqlctmap_id}")
    public ResponseEntity<EMEQLCTMapDTO> update(@PathVariable("emeqlctmap_id") String emeqlctmap_id, @RequestBody EMEQLCTMapDTO emeqlctmapdto) {
		EMEQLCTMap domain  = emeqlctmapMapping.toDomain(emeqlctmapdto);
        domain .setEmeqlctmapid(emeqlctmap_id);
		emeqlctmapService.update(domain );
		EMEQLCTMapDTO dto = emeqlctmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctmapService.getEmeqlctmapByEntities(this.emeqlctmapMapping.toDomain(#emeqlctmapdtos)),'eam-EMEQLCTMap-Update')")
    @ApiOperation(value = "批量更新位置关系", tags = {"位置关系" },  notes = "批量更新位置关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctmaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQLCTMapDTO> emeqlctmapdtos) {
        emeqlctmapService.updateBatch(emeqlctmapMapping.toDomain(emeqlctmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqlctmapService.get(#emeqlctmap_id),'eam-EMEQLCTMap-Remove')")
    @ApiOperation(value = "删除位置关系", tags = {"位置关系" },  notes = "删除位置关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctmaps/{emeqlctmap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqlctmap_id") String emeqlctmap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqlctmapService.remove(emeqlctmap_id));
    }

    @PreAuthorize("hasPermission(this.emeqlctmapService.getEmeqlctmapByIds(#ids),'eam-EMEQLCTMap-Remove')")
    @ApiOperation(value = "批量删除位置关系", tags = {"位置关系" },  notes = "批量删除位置关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctmaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqlctmapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqlctmapMapping.toDomain(returnObject.body),'eam-EMEQLCTMap-Get')")
    @ApiOperation(value = "获取位置关系", tags = {"位置关系" },  notes = "获取位置关系")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctmaps/{emeqlctmap_id}")
    public ResponseEntity<EMEQLCTMapDTO> get(@PathVariable("emeqlctmap_id") String emeqlctmap_id) {
        EMEQLCTMap domain = emeqlctmapService.get(emeqlctmap_id);
        EMEQLCTMapDTO dto = emeqlctmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取位置关系草稿", tags = {"位置关系" },  notes = "获取位置关系草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctmaps/getdraft")
    public ResponseEntity<EMEQLCTMapDTO> getDraft(EMEQLCTMapDTO dto) {
        EMEQLCTMap domain = emeqlctmapMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctmapMapping.toDto(emeqlctmapService.getDraft(domain)));
    }

    @ApiOperation(value = "检查位置关系", tags = {"位置关系" },  notes = "检查位置关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctmaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQLCTMapDTO emeqlctmapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqlctmapService.checkKey(emeqlctmapMapping.toDomain(emeqlctmapdto)));
    }

    @PreAuthorize("hasPermission(this.emeqlctmapMapping.toDomain(#emeqlctmapdto),'eam-EMEQLCTMap-Save')")
    @ApiOperation(value = "保存位置关系", tags = {"位置关系" },  notes = "保存位置关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctmaps/save")
    public ResponseEntity<EMEQLCTMapDTO> save(@RequestBody EMEQLCTMapDTO emeqlctmapdto) {
        EMEQLCTMap domain = emeqlctmapMapping.toDomain(emeqlctmapdto);
        emeqlctmapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctmapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqlctmapMapping.toDomain(#emeqlctmapdtos),'eam-EMEQLCTMap-Save')")
    @ApiOperation(value = "批量保存位置关系", tags = {"位置关系" },  notes = "批量保存位置关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctmaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQLCTMapDTO> emeqlctmapdtos) {
        emeqlctmapService.saveBatch(emeqlctmapMapping.toDomain(emeqlctmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchByLocation-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "获取ByLocation", tags = {"位置关系" } ,notes = "获取ByLocation")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctmaps/fetchbylocation")
	public ResponseEntity<List<EMEQLCTMapDTO>> fetchByLocation(EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchByLocation(context) ;
        List<EMEQLCTMapDTO> list = emeqlctmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchByLocation-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "查询ByLocation", tags = {"位置关系" } ,notes = "查询ByLocation")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctmaps/searchbylocation")
	public ResponseEntity<Page<EMEQLCTMapDTO>> searchByLocation(@RequestBody EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchByLocation(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchChildLocation-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "获取ChildLocation", tags = {"位置关系" } ,notes = "获取ChildLocation")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctmaps/fetchchildlocation")
	public ResponseEntity<List<EMEQLCTMapDTO>> fetchChildLocation(EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchChildLocation(context) ;
        List<EMEQLCTMapDTO> list = emeqlctmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchChildLocation-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "查询ChildLocation", tags = {"位置关系" } ,notes = "查询ChildLocation")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctmaps/searchchildlocation")
	public ResponseEntity<Page<EMEQLCTMapDTO>> searchChildLocation(@RequestBody EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchChildLocation(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"位置关系" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctmaps/fetchdefault")
	public ResponseEntity<List<EMEQLCTMapDTO>> fetchDefault(EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchDefault(context) ;
        List<EMEQLCTMapDTO> list = emeqlctmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"位置关系" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctmaps/searchdefault")
	public ResponseEntity<Page<EMEQLCTMapDTO>> searchDefault(@RequestBody EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchRootLocation-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "获取RootLocation", tags = {"位置关系" } ,notes = "获取RootLocation")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctmaps/fetchrootlocation")
	public ResponseEntity<List<EMEQLCTMapDTO>> fetchRootLocation(EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchRootLocation(context) ;
        List<EMEQLCTMapDTO> list = emeqlctmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTMap-searchRootLocation-all') and hasPermission(#context,'eam-EMEQLCTMap-Get')")
	@ApiOperation(value = "查询RootLocation", tags = {"位置关系" } ,notes = "查询RootLocation")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctmaps/searchrootlocation")
	public ResponseEntity<Page<EMEQLCTMapDTO>> searchRootLocation(@RequestBody EMEQLCTMapSearchContext context) {
        Page<EMEQLCTMap> domains = emeqlctmapService.searchRootLocation(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

