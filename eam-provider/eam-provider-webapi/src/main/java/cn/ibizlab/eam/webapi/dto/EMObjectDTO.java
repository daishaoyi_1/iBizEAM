package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMObjectDTO]
 */
@Data
public class EMObjectDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [OBJECTINFO]
     *
     */
    @JSONField(name = "objectinfo")
    @JsonProperty("objectinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objectinfo;

    /**
     * 属性 [EMOBJECTTYPE]
     *
     */
    @JSONField(name = "emobjecttype")
    @JsonProperty("emobjecttype")
    @NotBlank(message = "[对象类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emobjecttype;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [EMOBJECTID]
     *
     */
    @JSONField(name = "emobjectid")
    @JsonProperty("emobjectid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emobjectid;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String orgid;

    /**
     * 属性 [OBJECTCODE]
     *
     */
    @JSONField(name = "objectcode")
    @JsonProperty("objectcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String objectcode;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMOBJECTNAME]
     *
     */
    @JSONField(name = "emobjectname")
    @JsonProperty("emobjectname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emobjectname;

    /**
     * 属性 [MAJOREQUIPNAME]
     *
     */
    @JSONField(name = "majorequipname")
    @JsonProperty("majorequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String majorequipname;

    /**
     * 属性 [MAJOREQUIPID]
     *
     */
    @JSONField(name = "majorequipid")
    @JsonProperty("majorequipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String majorequipid;


    /**
     * 设置 [EMOBJECTTYPE]
     */
    public void setEmobjecttype(String  emobjecttype){
        this.emobjecttype = emobjecttype ;
        this.modify("emobjecttype",emobjecttype);
    }

    /**
     * 设置 [OBJECTCODE]
     */
    public void setObjectcode(String  objectcode){
        this.objectcode = objectcode ;
        this.modify("objectcode",objectcode);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMOBJECTNAME]
     */
    public void setEmobjectname(String  emobjectname){
        this.emobjectname = emobjectname ;
        this.modify("emobjectname",emobjectname);
    }

    /**
     * 设置 [MAJOREQUIPID]
     */
    public void setMajorequipid(String  majorequipid){
        this.majorequipid = majorequipid ;
        this.modify("majorequipid",majorequipid);
    }


}


