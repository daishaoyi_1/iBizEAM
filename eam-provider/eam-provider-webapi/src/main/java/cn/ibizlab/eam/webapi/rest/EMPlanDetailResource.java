package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanDetail;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanDetailService;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanDetailSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划步骤" })
@RestController("WebApi-emplandetail")
@RequestMapping("")
public class EMPlanDetailResource {

    @Autowired
    public IEMPlanDetailService emplandetailService;

    @Autowired
    @Lazy
    public EMPlanDetailMapping emplandetailMapping;

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "新建计划步骤", tags = {"计划步骤" },  notes = "新建计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> create(@Validated @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "批量新建计划步骤", tags = {"计划步骤" },  notes = "批量新建计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplandetails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        emplandetailService.createBatch(emplandetailMapping.toDomain(emplandetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "更新计划步骤", tags = {"计划步骤" },  notes = "更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> update(@PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
		EMPlanDetail domain  = emplandetailMapping.toDomain(emplandetaildto);
        domain .setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain );
		EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "批量更新计划步骤", tags = {"计划步骤" },  notes = "批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        emplandetailService.updateBatch(emplandetailMapping.toDomain(emplandetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "删除计划步骤", tags = {"计划步骤" },  notes = "删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emplandetail_id") String emplandetail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "批量删除计划步骤", tags = {"计划步骤" },  notes = "批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "获取计划步骤", tags = {"计划步骤" },  notes = "获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> get(@PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划步骤草稿", tags = {"计划步骤" },  notes = "获取计划步骤草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraft(EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划步骤", tags = {"计划步骤" },  notes = "检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "保存计划步骤", tags = {"计划步骤" },  notes = "保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> save(@RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "批量保存计划步骤", tags = {"计划步骤" },  notes = "批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        emplandetailService.saveBatch(emplandetailMapping.toDomain(emplandetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划步骤" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchDefault(EMPlanDetailSearchContext context) {
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划步骤" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchDefault(@RequestBody EMPlanDetailSearchContext context) {
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据计划建立计划步骤", tags = {"计划步骤" },  notes = "根据计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据计划更新计划步骤", tags = {"计划步骤" },  notes = "根据计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据计划删除计划步骤", tags = {"计划步骤" },  notes = "根据计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据计划获取计划步骤", tags = {"计划步骤" },  notes = "根据计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByEMPlan(@PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划检查计划步骤", tags = {"计划步骤" },  notes = "根据计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据计划保存计划步骤", tags = {"计划步骤" },  notes = "根据计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据总帐科目计划建立计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据总帐科目计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据总帐科目计划更新计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据总帐科目计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据总帐科目计划删除计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据总帐科目计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据总帐科目计划获取计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据总帐科目计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划检查计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据总帐科目计划保存计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据总帐科目计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据总帐科目计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据总帐科目计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据总帐科目计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据总帐科目计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据设备档案计划建立计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据设备档案计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据设备档案计划更新计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据设备档案计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据设备档案计划删除计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据设备档案计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquipEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据设备档案计划获取计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据设备档案计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案计划检查计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据设备档案计划保存计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据设备档案计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据设备档案计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据设备档案计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据设备档案计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据设备档案计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据设备档案计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据计划模板计划建立计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据计划模板计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据计划模板计划更新计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据计划模板计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据计划模板计划删除计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据计划模板计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据计划模板计划获取计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划模板计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据计划模板计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划模板计划检查计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据计划模板计划保存计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据计划模板计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据计划模板计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据计划模板计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据计划模板计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据服务商计划建立计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据服务商计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据服务商计划更新计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据服务商计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据服务商计划删除计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据服务商计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据服务商计划获取计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据服务商计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划检查计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据服务商计划保存计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据服务商计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据服务商计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据服务商计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据服务商计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据服务商计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据班组计划建立计划步骤", tags = {"计划步骤" },  notes = "根据班组计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据班组计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据班组计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据班组计划更新计划步骤", tags = {"计划步骤" },  notes = "根据班组计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据班组计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据班组计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据班组计划删除计划步骤", tags = {"计划步骤" },  notes = "根据班组计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据班组计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据班组计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据班组计划获取计划步骤", tags = {"计划步骤" },  notes = "根据班组计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据班组计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划检查计划步骤", tags = {"计划步骤" },  notes = "根据班组计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据班组计划保存计划步骤", tags = {"计划步骤" },  notes = "根据班组计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据班组计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据班组计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据班组计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据班组计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据班组计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据班组计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据班组设备档案计划建立计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据班组设备档案计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据班组设备档案计划更新计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据班组设备档案计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据班组设备档案计划删除计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据班组设备档案计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquipEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据班组设备档案计划获取计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据班组设备档案计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案计划检查计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据班组设备档案计划保存计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据班组设备档案计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据班组设备档案计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据班组设备档案计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据班组设备档案计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据班组设备档案计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据班组设备档案计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划建立计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划更新计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划删除计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据总帐科目计划模板计划获取计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划模板计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划模板计划检查计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划保存计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据总帐科目计划模板计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据总帐科目计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据总帐科目计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据服务商计划模板计划建立计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据服务商计划模板计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据服务商计划模板计划更新计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据服务商计划模板计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划删除计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据服务商计划模板计划获取计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划模板计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据服务商计划模板计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划模板计划检查计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据服务商计划模板计划保存计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据服务商计划模板计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据服务商计划模板计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据服务商计划模板计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据服务商计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据服务商计划模板计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据服务商计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据班组计划模板计划建立计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails")
    public ResponseEntity<EMPlanDetailDTO> createByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
		emplandetailService.create(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Create')")
    @ApiOperation(value = "根据班组计划模板计划批量建立计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划批量建立计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplandetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据班组计划模板计划更新计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> updateByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        domain.setEmplandetailid(emplandetail_id);
		emplandetailService.update(domain);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByEntities(this.emplandetailMapping.toDomain(#emplandetaildtos)),'eam-EMPlanDetail-Update')")
    @ApiOperation(value = "根据班组计划模板计划批量更新计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划批量更新计划步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
            domain.setPlanid(emplan_id);
        }
        emplandetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplandetailService.get(#emplandetail_id),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据班组计划模板计划删除计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplandetailService.remove(emplandetail_id));
    }

    @PreAuthorize("hasPermission(this.emplandetailService.getEmplandetailByIds(#ids),'eam-EMPlanDetail-Remove')")
    @ApiOperation(value = "根据班组计划模板计划批量删除计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划批量删除计划步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplandetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplandetailMapping.toDomain(returnObject.body),'eam-EMPlanDetail-Get')")
    @ApiOperation(value = "根据班组计划模板计划获取计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划获取计划步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/{emplandetail_id}")
    public ResponseEntity<EMPlanDetailDTO> getByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplandetail_id") String emplandetail_id) {
        EMPlanDetail domain = emplandetailService.get(emplandetail_id);
        EMPlanDetailDTO dto = emplandetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划模板计划获取计划步骤草稿", tags = {"计划步骤" },  notes = "根据班组计划模板计划获取计划步骤草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/getdraft")
    public ResponseEntity<EMPlanDetailDTO> getDraftByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPlanDetailDTO dto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(dto);
        domain.setPlanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(emplandetailService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划模板计划检查计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划检查计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplandetailService.checkKey(emplandetailMapping.toDomain(emplandetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildto),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据班组计划模板计划保存计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/save")
    public ResponseEntity<EMPlanDetailDTO> saveByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailDTO emplandetaildto) {
        EMPlanDetail domain = emplandetailMapping.toDomain(emplandetaildto);
        domain.setPlanid(emplan_id);
        emplandetailService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplandetailMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplandetailMapping.toDomain(#emplandetaildtos),'eam-EMPlanDetail-Save')")
    @ApiOperation(value = "根据班组计划模板计划批量保存计划步骤", tags = {"计划步骤" },  notes = "根据班组计划模板计划批量保存计划步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPlanDetailDTO> emplandetaildtos) {
        List<EMPlanDetail> domainlist=emplandetailMapping.toDomain(emplandetaildtos);
        for(EMPlanDetail domain:domainlist){
             domain.setPlanid(emplan_id);
        }
        emplandetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据班组计划模板计划获取DEFAULT", tags = {"计划步骤" } ,notes = "根据班组计划模板计划获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/fetchdefault")
	public ResponseEntity<List<EMPlanDetailDTO>> fetchEMPlanDetailDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
        List<EMPlanDetailDTO> list = emplandetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanDetail-Get')")
	@ApiOperation(value = "根据班组计划模板计划查询DEFAULT", tags = {"计划步骤" } ,notes = "根据班组计划模板计划查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplandetails/searchdefault")
	public ResponseEntity<Page<EMPlanDetailDTO>> searchEMPlanDetailDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPlanDetailSearchContext context) {
        context.setN_planid_eq(emplan_id);
        Page<EMPlanDetail> domains = emplandetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplandetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

