package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[EMEQLCTMapDTO]
 */
@Data
public class EMEQLCTMapDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String orgid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String description;

    /**
     * 属性 [EMEQLCTMAPID]
     *
     */
    @JSONField(name = "emeqlctmapid")
    @JsonProperty("emeqlctmapid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emeqlctmapid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String updateman;

    /**
     * 属性 [EMEQLCTMAPNAME]
     *
     */
    @JSONField(name = "emeqlctmapname")
    @JsonProperty("emeqlctmapname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String emeqlctmapname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;

    /**
     * 属性 [MAJOREQUIPNAME]
     *
     */
    @JSONField(name = "majorequipname")
    @JsonProperty("majorequipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String majorequipname;

    /**
     * 属性 [EQLOCATIONCODE]
     *
     */
    @JSONField(name = "eqlocationcode")
    @JsonProperty("eqlocationcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationcode;

    /**
     * 属性 [EQLOCATIONPNAME]
     *
     */
    @JSONField(name = "eqlocationpname")
    @JsonProperty("eqlocationpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationpname;

    /**
     * 属性 [EQLOCATIONDESC]
     *
     */
    @JSONField(name = "eqlocationdesc")
    @JsonProperty("eqlocationdesc")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String eqlocationdesc;

    /**
     * 属性 [MAJOREQUIPID]
     *
     */
    @JSONField(name = "majorequipid")
    @JsonProperty("majorequipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String majorequipid;

    /**
     * 属性 [EQLOCATIONNAME]
     *
     */
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eqlocationname;

    /**
     * 属性 [EQLOCATIONID]
     *
     */
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationid;

    /**
     * 属性 [EQLOCATIONPID]
     *
     */
    @JSONField(name = "eqlocationpid")
    @JsonProperty("eqlocationpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String eqlocationpid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMEQLCTMAPNAME]
     */
    public void setEmeqlctmapname(String  emeqlctmapname){
        this.emeqlctmapname = emeqlctmapname ;
        this.modify("emeqlctmapname",emeqlctmapname);
    }

    /**
     * 设置 [EQLOCATIONID]
     */
    public void setEqlocationid(String  eqlocationid){
        this.eqlocationid = eqlocationid ;
        this.modify("eqlocationid",eqlocationid);
    }

    /**
     * 设置 [EQLOCATIONPID]
     */
    public void setEqlocationpid(String  eqlocationpid){
        this.eqlocationpid = eqlocationpid ;
        this.modify("eqlocationpid",eqlocationpid);
    }


}


