package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMProduct;
import cn.ibizlab.eam.core.eam_core.service.IEMProductService;
import cn.ibizlab.eam.core.eam_core.filter.EMProductSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"试用品" })
@RestController("WebApi-emproduct")
@RequestMapping("")
public class EMProductResource {

    @Autowired
    public IEMProductService emproductService;

    @Autowired
    @Lazy
    public EMProductMapping emproductMapping;

    @PreAuthorize("hasPermission(this.emproductMapping.toDomain(#emproductdto),'eam-EMProduct-Create')")
    @ApiOperation(value = "新建试用品", tags = {"试用品" },  notes = "新建试用品")
	@RequestMapping(method = RequestMethod.POST, value = "/emproducts")
    public ResponseEntity<EMProductDTO> create(@Validated @RequestBody EMProductDTO emproductdto) {
        EMProduct domain = emproductMapping.toDomain(emproductdto);
		emproductService.create(domain);
        EMProductDTO dto = emproductMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emproductMapping.toDomain(#emproductdtos),'eam-EMProduct-Create')")
    @ApiOperation(value = "批量新建试用品", tags = {"试用品" },  notes = "批量新建试用品")
	@RequestMapping(method = RequestMethod.POST, value = "/emproducts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMProductDTO> emproductdtos) {
        emproductService.createBatch(emproductMapping.toDomain(emproductdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emproduct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emproductService.get(#emproduct_id),'eam-EMProduct-Update')")
    @ApiOperation(value = "更新试用品", tags = {"试用品" },  notes = "更新试用品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emproducts/{emproduct_id}")
    public ResponseEntity<EMProductDTO> update(@PathVariable("emproduct_id") String emproduct_id, @RequestBody EMProductDTO emproductdto) {
		EMProduct domain  = emproductMapping.toDomain(emproductdto);
        domain .setEmproductid(emproduct_id);
		emproductService.update(domain );
		EMProductDTO dto = emproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emproductService.getEmproductByEntities(this.emproductMapping.toDomain(#emproductdtos)),'eam-EMProduct-Update')")
    @ApiOperation(value = "批量更新试用品", tags = {"试用品" },  notes = "批量更新试用品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emproducts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMProductDTO> emproductdtos) {
        emproductService.updateBatch(emproductMapping.toDomain(emproductdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emproductService.get(#emproduct_id),'eam-EMProduct-Remove')")
    @ApiOperation(value = "删除试用品", tags = {"试用品" },  notes = "删除试用品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emproducts/{emproduct_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emproduct_id") String emproduct_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emproductService.remove(emproduct_id));
    }

    @PreAuthorize("hasPermission(this.emproductService.getEmproductByIds(#ids),'eam-EMProduct-Remove')")
    @ApiOperation(value = "批量删除试用品", tags = {"试用品" },  notes = "批量删除试用品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emproducts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emproductService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emproductMapping.toDomain(returnObject.body),'eam-EMProduct-Get')")
    @ApiOperation(value = "获取试用品", tags = {"试用品" },  notes = "获取试用品")
	@RequestMapping(method = RequestMethod.GET, value = "/emproducts/{emproduct_id}")
    public ResponseEntity<EMProductDTO> get(@PathVariable("emproduct_id") String emproduct_id) {
        EMProduct domain = emproductService.get(emproduct_id);
        EMProductDTO dto = emproductMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取试用品草稿", tags = {"试用品" },  notes = "获取试用品草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emproducts/getdraft")
    public ResponseEntity<EMProductDTO> getDraft(EMProductDTO dto) {
        EMProduct domain = emproductMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emproductMapping.toDto(emproductService.getDraft(domain)));
    }

    @ApiOperation(value = "检查试用品", tags = {"试用品" },  notes = "检查试用品")
	@RequestMapping(method = RequestMethod.POST, value = "/emproducts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMProductDTO emproductdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emproductService.checkKey(emproductMapping.toDomain(emproductdto)));
    }

    @PreAuthorize("hasPermission(this.emproductMapping.toDomain(#emproductdto),'eam-EMProduct-Save')")
    @ApiOperation(value = "保存试用品", tags = {"试用品" },  notes = "保存试用品")
	@RequestMapping(method = RequestMethod.POST, value = "/emproducts/save")
    public ResponseEntity<EMProductDTO> save(@RequestBody EMProductDTO emproductdto) {
        EMProduct domain = emproductMapping.toDomain(emproductdto);
        emproductService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emproductMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emproductMapping.toDomain(#emproductdtos),'eam-EMProduct-Save')")
    @ApiOperation(value = "批量保存试用品", tags = {"试用品" },  notes = "批量保存试用品")
	@RequestMapping(method = RequestMethod.POST, value = "/emproducts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMProductDTO> emproductdtos) {
        emproductService.saveBatch(emproductMapping.toDomain(emproductdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMProduct-searchDefault-all') and hasPermission(#context,'eam-EMProduct-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"试用品" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emproducts/fetchdefault")
	public ResponseEntity<List<EMProductDTO>> fetchDefault(EMProductSearchContext context) {
        Page<EMProduct> domains = emproductService.searchDefault(context) ;
        List<EMProductDTO> list = emproductMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMProduct-searchDefault-all') and hasPermission(#context,'eam-EMProduct-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"试用品" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emproducts/searchdefault")
	public ResponseEntity<Page<EMProductDTO>> searchDefault(@RequestBody EMProductSearchContext context) {
        Page<EMProduct> domains = emproductService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emproductMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

