package cn.ibizlab.eam.util.client;

import cn.ibizlab.eam.util.domain.JobsInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "${ibiz.ref.service.task:ibztask-api}",contextId = "task",fallback = IBZTaskFallback.class)
public interface IBZTaskFeignClient
{

	/**
	 * 启动任务
	 * @param jobsinfodto
	 * @return
	 */
	@PostMapping("/jobsinfos/{jobsinfo_id}/start}")
	JobsInfoDTO startJob(@PathVariable("jobsinfo_id") String jobsinfo_id, @RequestBody JobsInfoDTO jobsinfodto);

	/**
	 * 保存任务信息
	 * @param jobsinfodto
	 * @return
	 */
	@PostMapping("/jobsinfos/save")
	Boolean saveJob(@RequestBody JobsInfoDTO jobsinfodto);



}
