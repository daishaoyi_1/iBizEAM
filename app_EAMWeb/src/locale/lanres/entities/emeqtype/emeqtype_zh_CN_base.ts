import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("设备类型", null),
		fields: {
			eqtypegroup: commonLogic.appcommonhandle("类型分组",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			eqstateinfo: commonLogic.appcommonhandle("设备状态情况",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			eqtypecode: commonLogic.appcommonhandle("类型代码",null),
			stype: commonLogic.appcommonhandle("统计归口分组",null),
			eqtypeinfo: commonLogic.appcommonhandle("设备类型信息",null),
			arg: commonLogic.appcommonhandle("辅助设施故障/维护",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			emeqtypeid: commonLogic.appcommonhandle("设备类型标识",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			sname: commonLogic.appcommonhandle("统计归口名称",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emeqtypename: commonLogic.appcommonhandle("设备类型名称",null),
			description: commonLogic.appcommonhandle("描述",null),
			eqtypepcode: commonLogic.appcommonhandle("上级设备类型代码",null),
			eqtypepname: commonLogic.appcommonhandle("上级设备类型",null),
			eqtypepid: commonLogic.appcommonhandle("上级设备类型",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型",null),
				},
				gridexpview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型表格导航视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型选择表格视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型数据选择视图",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型",null),
				},
				treeexpview2: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备类型",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备类型",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型",null),
				},
				pickuptreeview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型",null),
				},
				treepickupview: {
					caption: commonLogic.appcommonhandle("设备类型",null),
					title: commonLogic.appcommonhandle("设备类型数据选择视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("设备类型信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备类型标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备类型名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqtypecode: commonLogic.appcommonhandle("类型代码",null), 
					emeqtypename: commonLogic.appcommonhandle("设备类型名称",null), 
					eqtypepname: commonLogic.appcommonhandle("上级设备类型",null), 
					eqtypegroup: commonLogic.appcommonhandle("类型分组",null), 
					stype: commonLogic.appcommonhandle("统计归口分组",null), 
					sname: commonLogic.appcommonhandle("统计归口名称",null), 
					eqtypepid: commonLogic.appcommonhandle("上级设备类型",null), 
					emeqtypeid: commonLogic.appcommonhandle("设备类型标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					eqtypecode: commonLogic.appcommonhandle("类型代码",null),
					emeqtypename: commonLogic.appcommonhandle("设备类型名称",null),
					eqtypepname: commonLogic.appcommonhandle("上级设备类型",null),
					eqtypegroup: commonLogic.appcommonhandle("类型分组",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			nav_grid: {
				columns: {
					eqtypecode: commonLogic.appcommonhandle("类型代码",null),
					emeqtypename: commonLogic.appcommonhandle("设备类型名称",null),
					eqtypepname: commonLogic.appcommonhandle("上级设备类型",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_eqtypecode_like: commonLogic.appcommonhandle("类型代码(%)",null), 
					n_emeqtypename_like: commonLogic.appcommonhandle("设备类型名称(%)",null), 
					n_eqtypepid_eq: commonLogic.appcommonhandle("上级设备类型(=)",null), 
					n_eqtypegroup_eq: commonLogic.appcommonhandle("类型分组(=)",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			eqtypetree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("根节点",null),
					equip: commonLogic.appcommonhandle("全部设备",null),
				},
				uiactions: {
					emeqtype_addeqtype: commonLogic.appcommonhandle("添加设备类型",null),
					emeqtype_editeqtype: commonLogic.appcommonhandle("编辑",null),
					emeqtype_addequip: commonLogic.appcommonhandle("添加设备",null),
				},
			},
			eqtype_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("根节点",null),
					eqtype: commonLogic.appcommonhandle("全部设备类型",null),
				},
				uiactions: {
					emeqtype_addeqtype: commonLogic.appcommonhandle("添加设备类型",null),
					emeqtype_remove: commonLogic.appcommonhandle("删除",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;