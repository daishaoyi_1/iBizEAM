import EMItem_zh_CN_Base from './emitem_zh_CN_base';

function getLocaleResource(){
    const EMItem_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItem_zh_CN_Base(), EMItem_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;