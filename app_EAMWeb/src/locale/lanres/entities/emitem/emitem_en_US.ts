import EMItem_en_US_Base from './emitem_en_US_base';

function getLocaleResource(){
    const EMItem_en_US_OwnData = {};
    const targetData = Object.assign(EMItem_en_US_Base(), EMItem_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
