import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("设备仪表", null),
		fields: {
			updateman: commonLogic.appcommonhandle("更新人",null),
			mpcode: commonLogic.appcommonhandle("设备仪表代码",null),
			emeqmpname: commonLogic.appcommonhandle("设备仪表名称",null),
			mpdesc: commonLogic.appcommonhandle("仪表备注",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			mpinfo: commonLogic.appcommonhandle("设备仪表信息",null),
			emeqmpid: commonLogic.appcommonhandle("设备仪表标识",null),
			normalrefval: commonLogic.appcommonhandle("正常参考值",null),
			mptypeid: commonLogic.appcommonhandle("设备仪表类型",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			mpscope: commonLogic.appcommonhandle("仪表范围",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			objname: commonLogic.appcommonhandle("位置",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			objid: commonLogic.appcommonhandle("位置",null),
			equipid: commonLogic.appcommonhandle("设备",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("设备仪表",null),
					title: commonLogic.appcommonhandle("设备仪表数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("设备仪表",null),
					title: commonLogic.appcommonhandle("设备仪表选择表格视图",null),
				},
				mpeditview: {
					caption: commonLogic.appcommonhandle("设备仪表",null),
					title: commonLogic.appcommonhandle("设备仪表编辑视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("设备仪表",null),
					title: commonLogic.appcommonhandle("设备仪表表格视图",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("设备仪表信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备仪表标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备仪表名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					mpcode: commonLogic.appcommonhandle("设备仪表代码",null), 
					emeqmpname: commonLogic.appcommonhandle("设备仪表名称",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					mptypeid: commonLogic.appcommonhandle("设备仪表类型",null), 
					normalrefval: commonLogic.appcommonhandle("正常参考值",null), 
					mpscope: commonLogic.appcommonhandle("仪表范围",null), 
					mpdesc: commonLogic.appcommonhandle("仪表备注",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
					emeqmpid: commonLogic.appcommonhandle("设备仪表标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					emeqmpname: commonLogic.appcommonhandle("设备仪表名称",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					normalrefval: commonLogic.appcommonhandle("正常参考值",null),
					mpdesc: commonLogic.appcommonhandle("仪表备注",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					mpcode: commonLogic.appcommonhandle("设备仪表代码",null),
					emeqmpname: commonLogic.appcommonhandle("设备仪表名称",null),
					mptypeid: commonLogic.appcommonhandle("设备仪表类型",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					mpdesc: commonLogic.appcommonhandle("仪表备注",null),
					mpscope: commonLogic.appcommonhandle("仪表范围",null),
					normalrefval: commonLogic.appcommonhandle("正常参考值",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_equipname_like: commonLogic.appcommonhandle("设备(文本包含(%))",null), 
					n_objname_like: commonLogic.appcommonhandle("位置(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			mpeditviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;