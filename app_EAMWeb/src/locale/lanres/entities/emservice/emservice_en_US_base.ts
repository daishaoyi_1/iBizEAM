import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			createdate: commonLogic.appcommonhandle("建立时间",null),
			accode: commonLogic.appcommonhandle("帐号",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			lsareaid: commonLogic.appcommonhandle("所在地区",null),
			fax: commonLogic.appcommonhandle("传真",null),
			zzyw: commonLogic.appcommonhandle("主营业务",null),
			payway: commonLogic.appcommonhandle("付款方式",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			serviceinfo: commonLogic.appcommonhandle("服务商信息",null),
			pgrade: commonLogic.appcommonhandle("上季度评估得分",null),
			accodedesc: commonLogic.appcommonhandle("帐号备注",null),
			labservicelevelid: commonLogic.appcommonhandle("级别",null),
			sums: commonLogic.appcommonhandle("评估得分",null),
			labservicetypeid: commonLogic.appcommonhandle("服务商类型",null),
			tel: commonLogic.appcommonhandle("联系电话",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			prman: commonLogic.appcommonhandle("联系人",null),
			content: commonLogic.appcommonhandle("合同内容",null),
			emserviceid: commonLogic.appcommonhandle("服务商标识",null),
			qualitymana: commonLogic.appcommonhandle("质量管理体系附件",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			taxcode: commonLogic.appcommonhandle("税码",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			zip: commonLogic.appcommonhandle("邮编",null),
			range: commonLogic.appcommonhandle("服务商归属",null),
			enablebz: commonLogic.appcommonhandle("有效值备注",null),
			emservicename: commonLogic.appcommonhandle("服务商名称",null),
			servicecode: commonLogic.appcommonhandle("服务商代码",null),
			paywaydesc: commonLogic.appcommonhandle("付款方式备注",null),
			servicegroup: commonLogic.appcommonhandle("服务商分组",null),
			att: commonLogic.appcommonhandle("附件",null),
			website: commonLogic.appcommonhandle("网址",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			logo: commonLogic.appcommonhandle("图标",null),
			addr: commonLogic.appcommonhandle("联系地址",null),
			taxtypeid: commonLogic.appcommonhandle("税类型",null),
			taxdesc: commonLogic.appcommonhandle("税备注",null),
			shdate: commonLogic.appcommonhandle("审核时间",null),
			qualifications: commonLogic.appcommonhandle("资质附件",null),
			servicestate: commonLogic.appcommonhandle("服务商状态",null),
		},
			views: {
				infoview9: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商实体编辑视图（部件视图）<主信息>基本信息",null),
				},
				contactview9: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商实体编辑视图（部件视图）<主信息>联系信息",null),
				},
				fileview9: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商实体编辑视图（部件视图）<主信息>附件信息",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商选择表格视图",null),
				},
				draftgridview: {
					caption: commonLogic.appcommonhandle("服务商审批-草稿",null),
					title: commonLogic.appcommonhandle("服务商",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("PUR服务商信息",null),
					title: commonLogic.appcommonhandle("PUR服务商信息",null),
				},
				dashboardview: {
					caption: commonLogic.appcommonhandle("综合评估",null),
					title: commonLogic.appcommonhandle("综合评估",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商数据选择视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商表格视图",null),
				},
				financeview9: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商实体编辑视图（部件视图）<主信息>财务信息",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商编辑视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("PUR服务商信息",null),
					title: commonLogic.appcommonhandle("PUR服务商信息",null),
				},
				confirmedgridview: {
					caption: commonLogic.appcommonhandle("服务商审批-已审",null),
					title: commonLogic.appcommonhandle("服务商",null),
				},
				toconfirmgridview: {
					caption: commonLogic.appcommonhandle("服务商审批-待审",null),
					title: commonLogic.appcommonhandle("服务商",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("服务商",null),
					title: commonLogic.appcommonhandle("服务商分页导航视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("PUR服务商信息",null), 
					grouppanel13: commonLogic.appcommonhandle("联系信息",null), 
					grouppanel21: commonLogic.appcommonhandle("财务信息",null), 
					grouppanel29: commonLogic.appcommonhandle("服务商资质及合同",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					druipart1: commonLogic.appcommonhandle("服务商评估",null), 
					formpage33: commonLogic.appcommonhandle("评估记录",null), 
					grouppanel38: commonLogic.appcommonhandle("操作信息",null), 
					formpage37: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					servicecode: commonLogic.appcommonhandle("服务商代码",null), 
					emservicename: commonLogic.appcommonhandle("服务商名称",null), 
					servicegroup: commonLogic.appcommonhandle("服务商分组",null), 
					labservicelevelid: commonLogic.appcommonhandle("级别",null), 
					labservicetypeid: commonLogic.appcommonhandle("服务商类型",null), 
					sums: commonLogic.appcommonhandle("评估得分",null), 
					servicestate: commonLogic.appcommonhandle("服务商状态",null), 
					qualitymana: commonLogic.appcommonhandle("质量管理体系附件",null), 
					qualifications: commonLogic.appcommonhandle("资质附件",null), 
					prman: commonLogic.appcommonhandle("联系人",null), 
					tel: commonLogic.appcommonhandle("联系电话",null), 
					fax: commonLogic.appcommonhandle("传真",null), 
					zip: commonLogic.appcommonhandle("邮编",null), 
					website: commonLogic.appcommonhandle("网址",null), 
					lsareaid: commonLogic.appcommonhandle("所在地区",null), 
					addr: commonLogic.appcommonhandle("联系地址",null), 
					accode: commonLogic.appcommonhandle("帐号",null), 
					accodedesc: commonLogic.appcommonhandle("帐号备注",null), 
					payway: commonLogic.appcommonhandle("付款方式",null), 
					paywaydesc: commonLogic.appcommonhandle("付款方式备注",null), 
					taxcode: commonLogic.appcommonhandle("税码",null), 
					taxtypeid: commonLogic.appcommonhandle("税类型",null), 
					taxdesc: commonLogic.appcommonhandle("税备注",null), 
					shdate: commonLogic.appcommonhandle("审核时间",null), 
					att: commonLogic.appcommonhandle("附件",null), 
					content: commonLogic.appcommonhandle("合同内容",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emserviceid: commonLogic.appcommonhandle("服务商标识",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("PUR服务商信息",null), 
					grouppanel13: commonLogic.appcommonhandle("联系信息",null), 
					grouppanel21: commonLogic.appcommonhandle("财务信息",null), 
					grouppanel29: commonLogic.appcommonhandle("服务商资质及合同",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					druipart1: commonLogic.appcommonhandle("",null), 
					formpage33: commonLogic.appcommonhandle("评估记录",null), 
					grouppanel38: commonLogic.appcommonhandle("操作信息",null), 
					formpage37: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					servicecode: commonLogic.appcommonhandle("服务商代码",null), 
					emservicename: commonLogic.appcommonhandle("服务商名称",null), 
					servicegroup: commonLogic.appcommonhandle("服务商分组",null), 
					range: commonLogic.appcommonhandle("服务商归属",null), 
					labservicelevelid: commonLogic.appcommonhandle("级别",null), 
					labservicetypeid: commonLogic.appcommonhandle("服务商类型",null), 
					sums: commonLogic.appcommonhandle("评估得分",null), 
					servicestate: commonLogic.appcommonhandle("服务商状态",null), 
					qualitymana: commonLogic.appcommonhandle("质量管理体系附件",null), 
					qualifications: commonLogic.appcommonhandle("资质附件",null), 
					prman: commonLogic.appcommonhandle("联系人",null), 
					tel: commonLogic.appcommonhandle("联系电话",null), 
					fax: commonLogic.appcommonhandle("传真",null), 
					zip: commonLogic.appcommonhandle("邮编",null), 
					website: commonLogic.appcommonhandle("网址",null), 
					lsareaid: commonLogic.appcommonhandle("所在地区",null), 
					addr: commonLogic.appcommonhandle("联系地址",null), 
					accode: commonLogic.appcommonhandle("帐号",null), 
					accodedesc: commonLogic.appcommonhandle("帐号备注",null), 
					payway: commonLogic.appcommonhandle("付款方式",null), 
					paywaydesc: commonLogic.appcommonhandle("付款方式备注",null), 
					taxcode: commonLogic.appcommonhandle("税码",null), 
					taxtypeid: commonLogic.appcommonhandle("税类型",null), 
					taxdesc: commonLogic.appcommonhandle("税备注",null), 
					shdate: commonLogic.appcommonhandle("审核时间",null), 
					att: commonLogic.appcommonhandle("附件",null), 
					content: commonLogic.appcommonhandle("合同内容",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emserviceid: commonLogic.appcommonhandle("服务商标识",null), 
				},
				uiactions: {
				},
			},
			info_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("PUR服务商信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					servicecode: commonLogic.appcommonhandle("服务商代码",null), 
					emservicename: commonLogic.appcommonhandle("服务商名称",null), 
					range: commonLogic.appcommonhandle("服务商归属",null), 
					servicegroup: commonLogic.appcommonhandle("服务商分组",null), 
					labservicelevelid: commonLogic.appcommonhandle("级别",null), 
					labservicetypeid: commonLogic.appcommonhandle("服务商类型",null), 
					sums: commonLogic.appcommonhandle("评估得分",null), 
					servicestate: commonLogic.appcommonhandle("服务商状态",null), 
					emserviceid: commonLogic.appcommonhandle("服务商标识",null), 
				},
				uiactions: {
				},
			},
			contact_form: {
				details: {
					grouppanel13: commonLogic.appcommonhandle("联系信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					prman: commonLogic.appcommonhandle("联系人",null), 
					tel: commonLogic.appcommonhandle("联系电话",null), 
					fax: commonLogic.appcommonhandle("传真",null), 
					zip: commonLogic.appcommonhandle("邮编",null), 
					website: commonLogic.appcommonhandle("网址",null), 
					lsareaid: commonLogic.appcommonhandle("所在地区",null), 
					addr: commonLogic.appcommonhandle("联系地址",null), 
					emserviceid: commonLogic.appcommonhandle("服务商标识",null), 
				},
				uiactions: {
				},
			},
			file_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("附件信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					qualitymana: commonLogic.appcommonhandle("质量管理体系附件",null), 
					qualifications: commonLogic.appcommonhandle("资质附件",null), 
					att: commonLogic.appcommonhandle("合同附件",null), 
					emserviceid: commonLogic.appcommonhandle("服务商标识",null), 
				},
				uiactions: {
				},
			},
			finance_form: {
				details: {
					grouppanel21: commonLogic.appcommonhandle("财务信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					accode: commonLogic.appcommonhandle("帐号",null), 
					accodedesc: commonLogic.appcommonhandle("帐号备注",null), 
					payway: commonLogic.appcommonhandle("付款方式",null), 
					paywaydesc: commonLogic.appcommonhandle("付款方式备注",null), 
					taxcode: commonLogic.appcommonhandle("税码",null), 
					taxtypeid: commonLogic.appcommonhandle("税类型",null), 
					taxdesc: commonLogic.appcommonhandle("税备注",null), 
					emserviceid: commonLogic.appcommonhandle("服务商标识",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("服务商基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					serviceinfo: commonLogic.appcommonhandle("服务商信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emserviceid: commonLogic.appcommonhandle("服务商标识",null), 
				},
				uiactions: {
				},
			},
			main4_grid: {
				columns: {
					servicecode: commonLogic.appcommonhandle("服务商代码",null),
					emservicename: commonLogic.appcommonhandle("服务商名称",null),
					servicegroup: commonLogic.appcommonhandle("服务商分组",null),
					sums: commonLogic.appcommonhandle("评估得分",null),
					zzyw: commonLogic.appcommonhandle("主营业务",null),
					labservicelevelid: commonLogic.appcommonhandle("级别",null),
					servicestate: commonLogic.appcommonhandle("服务商状态",null),
					prman: commonLogic.appcommonhandle("联系人",null),
					tel: commonLogic.appcommonhandle("联系电话",null),
					fax: commonLogic.appcommonhandle("传真",null),
					zip: commonLogic.appcommonhandle("邮编",null),
					addr: commonLogic.appcommonhandle("联系地址",null),
					website: commonLogic.appcommonhandle("网址",null),
					accode: commonLogic.appcommonhandle("帐号",null),
					payway: commonLogic.appcommonhandle("付款方式",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					servicecode: commonLogic.appcommonhandle("服务商代码",null),
					emservicename: commonLogic.appcommonhandle("服务商名称",null),
					servicegroup: commonLogic.appcommonhandle("服务商分组",null),
					sums: commonLogic.appcommonhandle("评估得分",null),
					zzyw: commonLogic.appcommonhandle("主营业务",null),
					labservicelevelid: commonLogic.appcommonhandle("级别",null),
					servicestate: commonLogic.appcommonhandle("服务商状态",null),
					prman: commonLogic.appcommonhandle("联系人",null),
					tel: commonLogic.appcommonhandle("联系电话",null),
					fax: commonLogic.appcommonhandle("传真",null),
					zip: commonLogic.appcommonhandle("邮编",null),
					addr: commonLogic.appcommonhandle("联系地址",null),
					website: commonLogic.appcommonhandle("网址",null),
					accode: commonLogic.appcommonhandle("帐号",null),
					payway: commonLogic.appcommonhandle("付款方式",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emservicename_like: commonLogic.appcommonhandle("服务商名称(文本包含(%))",null), 
					n_labservicetypeid_eq: commonLogic.appcommonhandle("服务商类型(等于(=))",null), 
					n_labservicelevelid_eq: commonLogic.appcommonhandle("级别(等于(=))",null), 
					n_servicestate_eq: commonLogic.appcommonhandle("服务商状态(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				deuiaction2_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			draftgridviewtoolbar_toolbar: {
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("完成",null),
					tip: commonLogic.appcommonhandle("完成",null),
				},
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_unconfirmed: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem17_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
			},
			toconfirmgridviewtoolbar_toolbar: {
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("完成",null),
					tip: commonLogic.appcommonhandle("完成",null),
				},
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_unconfirmed: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("服务商信息",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("草稿",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("待审",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("已审",null),
					}
				},
				uiactions: {
				},
			},
			dashboardviewdashboard_container1_portlet: {
				uiactions: {
				},
			},
			dashboardviewdashboard_container2_portlet: {
				uiactions: {
				},
			},
			info_portlet: {
				info: {
					title: commonLogic.appcommonhandle("基本信息", null)
			  	},
				uiactions: {
				},
			},
			dashboardviewdashboard_container3_portlet: {
				uiactions: {
				},
			},
			contact_portlet: {
				contact: {
					title: commonLogic.appcommonhandle("联系信息", null)
			  	},
				uiactions: {
				},
			},
			finance_portlet: {
				finance: {
					title: commonLogic.appcommonhandle("财务信息", null)
			  	},
				uiactions: {
				},
			},
			file_portlet: {
				file: {
					title: commonLogic.appcommonhandle("附件信息", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;