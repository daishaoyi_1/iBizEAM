import EMEQKPMap_zh_CN_Base from './emeqkpmap_zh_CN_base';

function getLocaleResource(){
    const EMEQKPMap_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQKPMap_zh_CN_Base(), EMEQKPMap_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;