import PLANSCHEDULE_en_US_Base from './planschedule_en_US_base';

function getLocaleResource(){
    const PLANSCHEDULE_en_US_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_en_US_Base(), PLANSCHEDULE_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
