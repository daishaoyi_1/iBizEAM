import PLANSCHEDULE_zh_CN_Base from './planschedule_zh_CN_base';

function getLocaleResource(){
    const PLANSCHEDULE_zh_CN_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_zh_CN_Base(), PLANSCHEDULE_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;