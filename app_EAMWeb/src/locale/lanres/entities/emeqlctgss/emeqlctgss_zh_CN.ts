import EMEQLCTGSS_zh_CN_Base from './emeqlctgss_zh_CN_base';

function getLocaleResource(){
    const EMEQLCTGSS_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQLCTGSS_zh_CN_Base(), EMEQLCTGSS_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;