import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("位置关系", null),
		fields: {
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			emeqlctmapid: commonLogic.appcommonhandle("位置关系标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emeqlctmapname: commonLogic.appcommonhandle("位置关系",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			majorequipname: commonLogic.appcommonhandle("主设备",null),
			eqlocationcode: commonLogic.appcommonhandle("位置代码",null),
			eqlocationpname: commonLogic.appcommonhandle("上级位置",null),
			eqlocationdesc: commonLogic.appcommonhandle("位置描述",null),
			majorequipid: commonLogic.appcommonhandle("主设备",null),
			eqlocationname: commonLogic.appcommonhandle("位置",null),
			eqlocationid: commonLogic.appcommonhandle("位置",null),
			eqlocationpid: commonLogic.appcommonhandle("上级位置",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("位置关系",null),
					title: commonLogic.appcommonhandle("位置关系编辑视图",null),
				},
				gridview9: {
					caption: commonLogic.appcommonhandle("位置关系",null),
					title: commonLogic.appcommonhandle("位置关系表格视图",null),
				},
			},
			main_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("位置关系信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置关系标识",null), 
					srfmajortext: commonLogic.appcommonhandle("位置关系",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqlocationname: commonLogic.appcommonhandle("位置",null), 
					eqlocationpname: commonLogic.appcommonhandle("上级位置",null), 
					eqlocationid: commonLogic.appcommonhandle("位置",null), 
					emeqlctmapid: commonLogic.appcommonhandle("位置关系标识",null), 
					eqlocationpid: commonLogic.appcommonhandle("上级位置",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					eqlocationpname: commonLogic.appcommonhandle("上级位置",null),
					eqlocationname: commonLogic.appcommonhandle("位置",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			gridview9toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;