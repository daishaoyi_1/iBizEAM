import EMRFOCA_en_US_Base from './emrfoca_en_US_base';

function getLocaleResource(){
    const EMRFOCA_en_US_OwnData = {};
    const targetData = Object.assign(EMRFOCA_en_US_Base(), EMRFOCA_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
