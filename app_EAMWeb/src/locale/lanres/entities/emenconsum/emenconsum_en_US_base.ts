import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			price: commonLogic.appcommonhandle("单价",null),
			deptid: commonLogic.appcommonhandle("部门",null),
			emenconsumid: commonLogic.appcommonhandle("能耗标识",null),
			pusetype: commonLogic.appcommonhandle("领料分类",null),
			vrate: commonLogic.appcommonhandle("倍率",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			amount: commonLogic.appcommonhandle("总金额",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			nval: commonLogic.appcommonhandle("能耗值",null),
			lastval: commonLogic.appcommonhandle("上次记录值",null),
			deptname: commonLogic.appcommonhandle("部门",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			curval: commonLogic.appcommonhandle("本次记录值",null),
			emenconsumname: commonLogic.appcommonhandle("能耗名称",null),
			bdate: commonLogic.appcommonhandle("上次采集时间",null),
			edate: commonLogic.appcommonhandle("采集时间",null),
			enprice: commonLogic.appcommonhandle("能源单价",null),
			objname: commonLogic.appcommonhandle("位置",null),
			itemmtypename: commonLogic.appcommonhandle("物品二级类",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			woname: commonLogic.appcommonhandle("工单",null),
			unitname: commonLogic.appcommonhandle("单位",null),
			itemprice: commonLogic.appcommonhandle("能源库存单价",null),
			energytypeid: commonLogic.appcommonhandle("能源类型",null),
			equipcode: commonLogic.appcommonhandle("设备",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			enname: commonLogic.appcommonhandle("能源",null),
			sname: commonLogic.appcommonhandle("设备统计归类",null),
			itemtypeid: commonLogic.appcommonhandle("物品类型",null),
			itembtypeid: commonLogic.appcommonhandle("物品大类",null),
			itembtypename: commonLogic.appcommonhandle("物品大类",null),
			itemid: commonLogic.appcommonhandle("物品",null),
			itemmtypeid: commonLogic.appcommonhandle("物品二级类",null),
			objid: commonLogic.appcommonhandle("位置",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			woid: commonLogic.appcommonhandle("工单",null),
			enid: commonLogic.appcommonhandle("能源",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("能耗",null),
					title: commonLogic.appcommonhandle("能耗编辑视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("能耗信息",null),
					title: commonLogic.appcommonhandle("能耗信息",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("能耗",null),
					title: commonLogic.appcommonhandle("能耗数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("能耗",null),
					title: commonLogic.appcommonhandle("能耗选择表格视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("能耗信息",null),
					title: commonLogic.appcommonhandle("能耗信息",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("能耗信息",null),
					title: commonLogic.appcommonhandle("能耗信息",null),
				},
			},
			main3_form: {
				details: {
					rawitem1: commonLogic.appcommonhandle("",null), 
					grouppanel2: commonLogic.appcommonhandle("能耗信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能耗标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能耗名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					enname: commonLogic.appcommonhandle("能源",null), 
					woname: commonLogic.appcommonhandle("工单",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					edate: commonLogic.appcommonhandle("采集时间",null), 
					bdate: commonLogic.appcommonhandle("上次采集时间",null), 
					curval: commonLogic.appcommonhandle("本次记录值",null), 
					lastval: commonLogic.appcommonhandle("上次记录值",null), 
					vrate: commonLogic.appcommonhandle("倍率",null), 
					nval: commonLogic.appcommonhandle("能耗值",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					emenconsumid: commonLogic.appcommonhandle("能耗标识",null), 
					woid: commonLogic.appcommonhandle("工单",null), 
					enid: commonLogic.appcommonhandle("能源",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("能耗基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能耗标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能耗名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					sname: commonLogic.appcommonhandle("设备统计归类",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emenconsumid: commonLogic.appcommonhandle("能耗标识",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("能耗信息",null), 
					grouppanel15: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能耗标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能耗名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					enname: commonLogic.appcommonhandle("能源",null), 
					woname: commonLogic.appcommonhandle("工单",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					edate: commonLogic.appcommonhandle("采集时间",null), 
					bdate: commonLogic.appcommonhandle("上次采集时间",null), 
					curval: commonLogic.appcommonhandle("本次记录值",null), 
					lastval: commonLogic.appcommonhandle("上次记录值",null), 
					vrate: commonLogic.appcommonhandle("倍率",null), 
					nval: commonLogic.appcommonhandle("能耗值",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emenconsumid: commonLogic.appcommonhandle("能耗标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					enname: commonLogic.appcommonhandle("能源",null),
					nval: commonLogic.appcommonhandle("能耗值",null),
					vrate: commonLogic.appcommonhandle("倍率",null),
					curval: commonLogic.appcommonhandle("本次记录值",null),
					lastval: commonLogic.appcommonhandle("上次记录值",null),
					edate: commonLogic.appcommonhandle("采集时间",null),
					bdate: commonLogic.appcommonhandle("上次采集时间",null),
					woname: commonLogic.appcommonhandle("工单",null),
					price: commonLogic.appcommonhandle("单价",null),
					amount: commonLogic.appcommonhandle("总金额",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			eqenbyyearline_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_equipname_like: commonLogic.appcommonhandle("设备(文本包含(%))",null), 
					n_objname_like: commonLogic.appcommonhandle("位置(文本包含(%))",null), 
					n_enname_like: commonLogic.appcommonhandle("能源(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			eqenbyyearline_portlet: {
				eqenbyyearline: {
					title: commonLogic.appcommonhandle("年度能耗", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;