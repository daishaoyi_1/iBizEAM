import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			itemroutinfo: commonLogic.appcommonhandle("调整单信息",null),
			price: commonLogic.appcommonhandle("单价",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			sdate: commonLogic.appcommonhandle("调整日期",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			emitemcsname: commonLogic.appcommonhandle("调整单名称",null),
			emitemcsid: commonLogic.appcommonhandle("调整单号",null),
			batcode: commonLogic.appcommonhandle("批次",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			description: commonLogic.appcommonhandle("描述",null),
			amount: commonLogic.appcommonhandle("总金额",null),
			tradestate: commonLogic.appcommonhandle("调整状态",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			psum: commonLogic.appcommonhandle("数量",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			storepartname: commonLogic.appcommonhandle("库位",null),
			rname: commonLogic.appcommonhandle("入库单",null),
			storename: commonLogic.appcommonhandle("仓库",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			stockname: commonLogic.appcommonhandle("库存",null),
			storeid: commonLogic.appcommonhandle("仓库",null),
			rid: commonLogic.appcommonhandle("入库单",null),
			storepartid: commonLogic.appcommonhandle("库位",null),
			stockid: commonLogic.appcommonhandle("库存",null),
			itemid: commonLogic.appcommonhandle("物品",null),
			sempid: commonLogic.appcommonhandle("调整人",null),
			sempname: commonLogic.appcommonhandle("调整人",null),
		},
			views: {
				tabexpview: {
					caption: commonLogic.appcommonhandle("库间调整单",null),
					title: commonLogic.appcommonhandle("库间调整单分页导航视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("调整单",null),
					title: commonLogic.appcommonhandle("调整单",null),
				},
				toconfirmgridview: {
					caption: commonLogic.appcommonhandle("调整单-待确认",null),
					title: commonLogic.appcommonhandle("调整单",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("库间调整单",null),
					title: commonLogic.appcommonhandle("库间调整单编辑视图",null),
				},
				confirmedgridview: {
					caption: commonLogic.appcommonhandle("调整单-已确认",null),
					title: commonLogic.appcommonhandle("调整单",null),
				},
				draftgridview: {
					caption: commonLogic.appcommonhandle("调整单-草稿",null),
					title: commonLogic.appcommonhandle("调整单",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("调整单",null),
					title: commonLogic.appcommonhandle("调整单",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("调整单",null),
					title: commonLogic.appcommonhandle("调整单",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("调整单信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel15: commonLogic.appcommonhandle("操作信息",null), 
					formpage14: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("调整单号",null), 
					srfmajortext: commonLogic.appcommonhandle("调整单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emitemcsid: commonLogic.appcommonhandle("调整单号(自动)",null), 
					itemid: commonLogic.appcommonhandle("物品",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					sdate: commonLogic.appcommonhandle("调整日期",null), 
					sempid: commonLogic.appcommonhandle("调整人",null), 
					sempname: commonLogic.appcommonhandle("调整人",null), 
					stockid: commonLogic.appcommonhandle("库存",null), 
					stockname: commonLogic.appcommonhandle("库存",null), 
					psum: commonLogic.appcommonhandle("数量",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					batcode: commonLogic.appcommonhandle("批次",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					storepartname: commonLogic.appcommonhandle("库位",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					storepartid: commonLogic.appcommonhandle("库位",null), 
					storeid: commonLogic.appcommonhandle("仓库",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("调整单信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel15: commonLogic.appcommonhandle("操作信息",null), 
					formpage14: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("调整单号",null), 
					srfmajortext: commonLogic.appcommonhandle("调整单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emitemcsid: commonLogic.appcommonhandle("调整单号(自动)",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					sdate: commonLogic.appcommonhandle("调整日期",null), 
					sempid: commonLogic.appcommonhandle("调整人",null), 
					sempname: commonLogic.appcommonhandle("调整人",null), 
					stockname: commonLogic.appcommonhandle("库存",null), 
					psum: commonLogic.appcommonhandle("数量",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					batcode: commonLogic.appcommonhandle("批次",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					storepartname: commonLogic.appcommonhandle("库位",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("库间调整单基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("调整单号",null), 
					srfmajortext: commonLogic.appcommonhandle("调整单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemroutinfo: commonLogic.appcommonhandle("调整单信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emitemcsid: commonLogic.appcommonhandle("调整单号",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					emitemcsid: commonLogic.appcommonhandle("调整单号",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					sdate: commonLogic.appcommonhandle("调整日期",null),
					stockname: commonLogic.appcommonhandle("库存",null),
					storename: commonLogic.appcommonhandle("仓库",null),
					storepartname: commonLogic.appcommonhandle("库位",null),
					psum: commonLogic.appcommonhandle("数量",null),
					price: commonLogic.appcommonhandle("单价",null),
					amount: commonLogic.appcommonhandle("总金额",null),
					batcode: commonLogic.appcommonhandle("批次",null),
					tradestate: commonLogic.appcommonhandle("调整状态",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_itemname_like: commonLogic.appcommonhandle("物品(文本包含(%))",null), 
					n_storeid_eq: commonLogic.appcommonhandle("仓库(等于(=))",null), 
					n_storepartid_eq: commonLogic.appcommonhandle("库位(等于(=))",null), 
					n_tradestate_eq: commonLogic.appcommonhandle("调整状态(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				deuiaction2_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem17_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
			},
			draftgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("确认",null),
					tip: commonLogic.appcommonhandle("确认",null),
				},
				tbitem1_unconfirm: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			toconfirmgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("确认",null),
					tip: commonLogic.appcommonhandle("确认",null),
				},
				tbitem1_unconfirm: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("全部调整单",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("草稿",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("待确认",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("已确认",null),
					}
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;