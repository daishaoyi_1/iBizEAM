import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("序列号", null),
		fields: {
			sequencename: commonLogic.appcommonhandle("序列号名称",null),
			sequenceid: commonLogic.appcommonhandle("序列号标识",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			code: commonLogic.appcommonhandle("实体名",null),
			implementation: commonLogic.appcommonhandle("实现",null),
			prefix: commonLogic.appcommonhandle("前缀",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			suffix: commonLogic.appcommonhandle("后缀",null),
			numincrement: commonLogic.appcommonhandle("序号增长",null),
			padding: commonLogic.appcommonhandle("序号长度",null),
			numnext: commonLogic.appcommonhandle("下一号码",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("序列号",null),
					title: commonLogic.appcommonhandle("序列号表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("序列号",null),
					title: commonLogic.appcommonhandle("序列号编辑视图",null),
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("序列号基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("序列号标识",null), 
					srfmajortext: commonLogic.appcommonhandle("序列号名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					sequenceid: commonLogic.appcommonhandle("序列号标识",null), 
					sequencename: commonLogic.appcommonhandle("序列号名称",null), 
					code: commonLogic.appcommonhandle("实体名",null), 
					implementation: commonLogic.appcommonhandle("实现",null), 
					prefix: commonLogic.appcommonhandle("前缀",null), 
					suffix: commonLogic.appcommonhandle("后缀",null), 
					padding: commonLogic.appcommonhandle("序号长度",null), 
					numincrement: commonLogic.appcommonhandle("序号增长",null), 
					numnext: commonLogic.appcommonhandle("下一号码",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					enable: commonLogic.appcommonhandle("逻辑有效标志",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					sequencename: commonLogic.appcommonhandle("序列号名称",null),
					code: commonLogic.appcommonhandle("实体名",null),
					implementation: commonLogic.appcommonhandle("实现",null),
					prefix: commonLogic.appcommonhandle("前缀",null),
					suffix: commonLogic.appcommonhandle("后缀",null),
					padding: commonLogic.appcommonhandle("序号长度",null),
					numincrement: commonLogic.appcommonhandle("序号增长",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;