import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			orgid: commonLogic.appcommonhandle("组织",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			activedate: commonLogic.appcommonhandle("活动日期",null),
			activedesc: commonLogic.appcommonhandle("活动记录",null),
			emeqahname: commonLogic.appcommonhandle("活动历史名称",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			regionenddate: commonLogic.appcommonhandle("结束时间",null),
			eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
			mfee: commonLogic.appcommonhandle("材料费(￥)",null),
			activebdesc: commonLogic.appcommonhandle("活动前情况",null),
			activeadesc: commonLogic.appcommonhandle("活动后情况",null),
			rdeptname: commonLogic.appcommonhandle("责任部门",null),
			rdeptid: commonLogic.appcommonhandle("责任部门",null),
			activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			emeqahid: commonLogic.appcommonhandle("活动历史标识",null),
			prefee: commonLogic.appcommonhandle("预算(￥)",null),
			rempname: commonLogic.appcommonhandle("责任人",null),
			description: commonLogic.appcommonhandle("描述",null),
			regionbegindate: commonLogic.appcommonhandle("起始时间",null),
			sfee: commonLogic.appcommonhandle("服务费(￥)",null),
			content: commonLogic.appcommonhandle("详细内容",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			rempid: commonLogic.appcommonhandle("责任人",null),
			pfee: commonLogic.appcommonhandle("人工费(￥)",null),
			emeqahtype: commonLogic.appcommonhandle("分组类型",null),
			acclassname: commonLogic.appcommonhandle("总帐科目",null),
			rfoacname: commonLogic.appcommonhandle("方案",null),
			rfomoname: commonLogic.appcommonhandle("模式",null),
			woname: commonLogic.appcommonhandle("工单",null),
			rfodename: commonLogic.appcommonhandle("现象",null),
			rfocaname: commonLogic.appcommonhandle("原因",null),
			rservicename: commonLogic.appcommonhandle("服务商",null),
			rteamname: commonLogic.appcommonhandle("责任班组",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			objname: commonLogic.appcommonhandle("位置",null),
			rfodeid: commonLogic.appcommonhandle("现象",null),
			rfocaid: commonLogic.appcommonhandle("原因",null),
			rteamid: commonLogic.appcommonhandle("责任班组",null),
			acclassid: commonLogic.appcommonhandle("总帐科目",null),
			rfoacid: commonLogic.appcommonhandle("方案",null),
			rfomoid: commonLogic.appcommonhandle("模式",null),
			objid: commonLogic.appcommonhandle("位置",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			woid: commonLogic.appcommonhandle("工单",null),
			rserviceid: commonLogic.appcommonhandle("服务商",null),
		},
			views: {
				calendarexpview: {
					caption: commonLogic.appcommonhandle("活动历史",null),
					title: commonLogic.appcommonhandle("活动历史日历导航视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("活动历史",null),
					title: commonLogic.appcommonhandle("活动历史表格视图",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("活动历史",null),
					title: commonLogic.appcommonhandle("活动历史树导航视图",null),
				},
				eqcalendarexpview: {
					caption: commonLogic.appcommonhandle("活动历史",null),
					title: commonLogic.appcommonhandle("设备活动日历导航视图",null),
				},
			},
			main2_grid: {
				columns: {
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					emeqahtype: commonLogic.appcommonhandle("分组类型",null),
					woname: commonLogic.appcommonhandle("工单",null),
					activedate: commonLogic.appcommonhandle("活动日期",null),
					activedesc: commonLogic.appcommonhandle("活动记录",null),
					regionbegindate: commonLogic.appcommonhandle("起始时间",null),
					regionenddate: commonLogic.appcommonhandle("结束时间",null),
					activeadesc: commonLogic.appcommonhandle("活动后情况",null),
					rempname: commonLogic.appcommonhandle("责任人",null),
					rdeptname: commonLogic.appcommonhandle("责任部门",null),
					rteamname: commonLogic.appcommonhandle("责任班组",null),
					rservicename: commonLogic.appcommonhandle("服务商",null),
					rfodename: commonLogic.appcommonhandle("现象",null),
					rfomoname: commonLogic.appcommonhandle("模式",null),
					rfocaname: commonLogic.appcommonhandle("原因",null),
					rfoacname: commonLogic.appcommonhandle("方案",null),
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
					activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
					prefee: commonLogic.appcommonhandle("预算(￥)",null),
					mfee: commonLogic.appcommonhandle("材料费(￥)",null),
					pfee: commonLogic.appcommonhandle("人工费(￥)",null),
					sfee: commonLogic.appcommonhandle("服务费(￥)",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_rfocaname_like: commonLogic.appcommonhandle("原因(文本包含(%))",null), 
					n_emeqahname_like: commonLogic.appcommonhandle("活动历史名称(文本包含(%))",null), 
					n_rfodename_like: commonLogic.appcommonhandle("现象(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem16: {
					caption: commonLogic.appcommonhandle("其它",null),
					tip: commonLogic.appcommonhandle("其它",null),
				},
				tbitem21: {
					caption: commonLogic.appcommonhandle("Export Data Model",null),
					tip: commonLogic.appcommonhandle("导出数据模型",null),
				},
			},
			eqahtree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					eqsetup: commonLogic.appcommonhandle("安装记录",null),
					eqdebug: commonLogic.appcommonhandle("事故记录",null),
					eqkeep: commonLogic.appcommonhandle("保养记录",null),
					eqcheck: commonLogic.appcommonhandle("维修记录",null),
					eqah: commonLogic.appcommonhandle("活动历史",null),
					root: commonLogic.appcommonhandle("默认根节点",null),
					eqmaintance: commonLogic.appcommonhandle("抢修记录",null),
				},
				uiactions: {
				},
			},
			eqahcal_calendar: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			eqahcalendar_calendar: {
				nodata:commonLogic.appcommonhandle("暂无数据",null),
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;