import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("位置", null),
		fields: {
			orgid: commonLogic.appcommonhandle("组织",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			eqlocationinfo: commonLogic.appcommonhandle("位置信息",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emeqlocationname: commonLogic.appcommonhandle("位置名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			emeqlocationid: commonLogic.appcommonhandle("位置标识",null),
			eqlocationcode: commonLogic.appcommonhandle("位置代码",null),
			eqlocationtype: commonLogic.appcommonhandle("位置类型",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			majorequipname: commonLogic.appcommonhandle("主设备",null),
			majorequipid: commonLogic.appcommonhandle("主设备",null),
		},
			views: {
				alllocgridview: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置选择表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置",null),
				},
				maininfo: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置数据看板视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置数据选择视图",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("位置新建",null),
					title: commonLogic.appcommonhandle("位置新建",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("位置",null),
					title: commonLogic.appcommonhandle("位置树导航视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("位置信息",null), 
					druipart1: commonLogic.appcommonhandle("",null), 
					grouppanel1: commonLogic.appcommonhandle("位置关系",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("位置名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqlocationcode: commonLogic.appcommonhandle("位置代码",null), 
					emeqlocationname: commonLogic.appcommonhandle("位置名称",null), 
					eqlocationtype: commonLogic.appcommonhandle("位置类型",null), 
					emeqlocationid: commonLogic.appcommonhandle("位置标识",null), 
				},
				uiactions: {
				},
			},
			new_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("位置信息",null), 
					druipart1: commonLogic.appcommonhandle("",null), 
					grouppanel1: commonLogic.appcommonhandle("位置关系",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("位置名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqlocationcode: commonLogic.appcommonhandle("位置代码",null), 
					emeqlocationname: commonLogic.appcommonhandle("位置名称",null), 
					eqlocationtype: commonLogic.appcommonhandle("位置类型",null), 
					emeqlocationid: commonLogic.appcommonhandle("位置标识",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("位置信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("位置名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqlocationcode: commonLogic.appcommonhandle("位置代码",null), 
					emeqlocationname: commonLogic.appcommonhandle("位置名称",null), 
					eqlocationtype: commonLogic.appcommonhandle("位置类型",null), 
					emeqlocationid: commonLogic.appcommonhandle("位置标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					eqlocationinfo: commonLogic.appcommonhandle("位置信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					eqlocationcode: commonLogic.appcommonhandle("位置代码",null),
					emeqlocationname: commonLogic.appcommonhandle("位置名称",null),
					eqlocationtype: commonLogic.appcommonhandle("位置类型",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_eqlocationcode_like: commonLogic.appcommonhandle("位置代码(%)",null), 
					n_emeqlocationname_like: commonLogic.appcommonhandle("位置名称(%)",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			alllocgridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			main_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("默认根节点",null),
					location: commonLogic.appcommonhandle("全部位置",null),
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;