import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("测点记录", null),
		fields: {
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emdprcttype: commonLogic.appcommonhandle("测点记录类型",null),
			emdprctname: commonLogic.appcommonhandle("测点记录名称",null),
			bdate: commonLogic.appcommonhandle("区间起始",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			edate: commonLogic.appcommonhandle("区间截至",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			val: commonLogic.appcommonhandle("值",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			nval: commonLogic.appcommonhandle("数值",null),
			emdprctid: commonLogic.appcommonhandle("测点记录标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			woname: commonLogic.appcommonhandle("工单",null),
			dpname: commonLogic.appcommonhandle("测点",null),
			objname: commonLogic.appcommonhandle("位置",null),
			woid: commonLogic.appcommonhandle("工单",null),
			objid: commonLogic.appcommonhandle("位置",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			dpid: commonLogic.appcommonhandle("测点",null),
		},
		};
		return data;
}
export default getLocaleResourceBase;