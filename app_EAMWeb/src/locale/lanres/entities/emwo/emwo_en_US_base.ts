import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			wostate: commonLogic.appcommonhandle("工单状态",null),
			vrate: commonLogic.appcommonhandle("倍率",null),
			mdate: commonLogic.appcommonhandle("制定时间",null),
			wotype: commonLogic.appcommonhandle("工单类型",null),
			cplanflag: commonLogic.appcommonhandle("转计划标志",null),
			waitbuy: commonLogic.appcommonhandle("等待配件时(小时)",null),
			emwotype: commonLogic.appcommonhandle("工单种类",null),
			description: commonLogic.appcommonhandle("描述",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			prefee: commonLogic.appcommonhandle("预算(￥)",null),
			activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
			waitmodi: commonLogic.appcommonhandle("等待修理时(小时)",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emwoid: commonLogic.appcommonhandle("工单编号",null),
			mfee: commonLogic.appcommonhandle("约合材料费(￥)",null),
			regionenddate: commonLogic.appcommonhandle("结束时间",null),
			wogroup: commonLogic.appcommonhandle("工单分组",null),
			worklength: commonLogic.appcommonhandle("实际工时(分)",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			content: commonLogic.appcommonhandle("详细内容",null),
			woteam: commonLogic.appcommonhandle("工单组",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			expiredate: commonLogic.appcommonhandle("过期日期",null),
			yxcb: commonLogic.appcommonhandle("影响船舶",null),
			priority: commonLogic.appcommonhandle("优先级",null),
			woinfo: commonLogic.appcommonhandle("工单信息",null),
			nval: commonLogic.appcommonhandle("数值",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emwoname: commonLogic.appcommonhandle("工单名称",null),
			eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
			wodate: commonLogic.appcommonhandle("执行日期",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			wresult: commonLogic.appcommonhandle("执行结果",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			wodesc: commonLogic.appcommonhandle("工单内容",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			val: commonLogic.appcommonhandle("值",null),
			archive: commonLogic.appcommonhandle("归档",null),
			regionbegindate: commonLogic.appcommonhandle("起始时间",null),
			rservicename: commonLogic.appcommonhandle("服务商",null),
			wooriname: commonLogic.appcommonhandle("工单来源",null),
			equipcode: commonLogic.appcommonhandle("设备",null),
			rfoacname: commonLogic.appcommonhandle("方案",null),
			rfomoname: commonLogic.appcommonhandle("模式",null),
			acclassname: commonLogic.appcommonhandle("总帐科目",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			wopname: commonLogic.appcommonhandle("上级工单",null),
			rfodename: commonLogic.appcommonhandle("现象",null),
			rteamname: commonLogic.appcommonhandle("责任班组",null),
			stype: commonLogic.appcommonhandle("统计归口类型分组",null),
			dptype: commonLogic.appcommonhandle("测点类型",null),
			dpname: commonLogic.appcommonhandle("测点",null),
			wooritype: commonLogic.appcommonhandle("来源类型",null),
			sname: commonLogic.appcommonhandle("统计归口类型",null),
			objname: commonLogic.appcommonhandle("位置",null),
			rfocaname: commonLogic.appcommonhandle("原因",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			rserviceid: commonLogic.appcommonhandle("服务商",null),
			acclassid: commonLogic.appcommonhandle("总帐科目",null),
			wopid: commonLogic.appcommonhandle("上级工单",null),
			rfoacid: commonLogic.appcommonhandle("方案",null),
			rfodeid: commonLogic.appcommonhandle("现象",null),
			wooriid: commonLogic.appcommonhandle("工单来源",null),
			rfomoid: commonLogic.appcommonhandle("模式",null),
			rteamid: commonLogic.appcommonhandle("责任班组",null),
			dpid: commonLogic.appcommonhandle("测点",null),
			rfocaid: commonLogic.appcommonhandle("原因",null),
			objid: commonLogic.appcommonhandle("位置",null),
			rdeptid: commonLogic.appcommonhandle("责任部门",null),
			rdeptname: commonLogic.appcommonhandle("责任部门",null),
			wpersonid: commonLogic.appcommonhandle("执行人",null),
			wpersonname: commonLogic.appcommonhandle("执行人",null),
			recvpersonid: commonLogic.appcommonhandle("接收人",null),
			recvpersonname: commonLogic.appcommonhandle("接收人",null),
			rempid: commonLogic.appcommonhandle("责任人",null),
			rempname: commonLogic.appcommonhandle("责任人",null),
			mpersonid: commonLogic.appcommonhandle("制定人",null),
			mpersonname: commonLogic.appcommonhandle("制定人",null),
		},
			views: {
				indexpickupview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单选择表格视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单表格视图",null),
				},
				indexpickupdataview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单索引关系选择数据视图",null),
				},
				calendarview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单日历视图",null),
				},
				redirectview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单数据重定向视图",null),
				},
				calendarexpview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单日历导航视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单数据选择视图",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单树导航视图",null),
				},
				emwogridview: {
					caption: commonLogic.appcommonhandle("工单",null),
					title: commonLogic.appcommonhandle("工单表格视图",null),
				},
			},
			main6_grid: {
				columns: {
					emwoid: commonLogic.appcommonhandle("工单编号",null),
					emwoname: commonLogic.appcommonhandle("工单名称",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					wodate: commonLogic.appcommonhandle("执行日期",null),
					rteamname: commonLogic.appcommonhandle("责任班组",null),
					rservicename: commonLogic.appcommonhandle("服务商",null),
					dpname: commonLogic.appcommonhandle("测点",null),
					wopname: commonLogic.appcommonhandle("上级工单",null),
					wooriname: commonLogic.appcommonhandle("工单来源",null),
					regionbegindate: commonLogic.appcommonhandle("起始时间",null),
					regionenddate: commonLogic.appcommonhandle("结束时间",null),
					priority: commonLogic.appcommonhandle("优先级",null),
					expiredate: commonLogic.appcommonhandle("过期日期",null),
					emwotype: commonLogic.appcommonhandle("工单种类",null),
					wogroup: commonLogic.appcommonhandle("工单分组",null),
					wotype: commonLogic.appcommonhandle("工单类型",null),
					worklength: commonLogic.appcommonhandle("实际工时(分)",null),
					val: commonLogic.appcommonhandle("值",null),
					wresult: commonLogic.appcommonhandle("执行结果",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			indextype_dataview: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			eqwotrend_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			yearwonumbyplan_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			yearwotrend_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emwoname_like: commonLogic.appcommonhandle("工单名称(文本包含(%))",null), 
					n_equipname_like: commonLogic.appcommonhandle("设备(文本包含(%))",null), 
					n_objname_like: commonLogic.appcommonhandle("位置(文本包含(%))",null), 
					n_emwotype_eq: commonLogic.appcommonhandle("工单种类(等于(=))",null), 
					n_wooriname_like: commonLogic.appcommonhandle("工单来源(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem16: {
					caption: commonLogic.appcommonhandle("其它",null),
					tip: commonLogic.appcommonhandle("其它",null),
				},
				tbitem21: {
					caption: commonLogic.appcommonhandle("Export Data Model",null),
					tip: commonLogic.appcommonhandle("导出数据模型",null),
				},
			},
			emwogridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			wotree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					all: commonLogic.appcommonhandle("全部工单",null),
					en: commonLogic.appcommonhandle("能耗工单",null),
					dp: commonLogic.appcommonhandle("点检工单",null),
					inner: commonLogic.appcommonhandle("内部工单",null),
					osc: commonLogic.appcommonhandle("保养工单",null),
					root: commonLogic.appcommonhandle("默认根节点",null),
				},
				uiactions: {
				},
			},
			wocalendar_calendar: {
				nodata:commonLogic.appcommonhandle("暂无数据",null),
				uiactions: {
				},
			},
			wocalendarexp_calendar: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			eqwotrend_portlet: {
				eqwotrend: {
					title: commonLogic.appcommonhandle("年度工单数量", null)
			  	},
				uiactions: {
				},
			},
			yearwonumbyplan_portlet: {
				yearwonumbyplan: {
					title: commonLogic.appcommonhandle("生产工单统计", null)
			  	},
				uiactions: {
				},
			},
			yearwotrend_portlet: {
				yearwotrend: {
					title: commonLogic.appcommonhandle("年度工单数量", null)
			  	},
				uiactions: {
				},
			},
			calendarview_portlet: {
				calendarview: {
					title: commonLogic.appcommonhandle("工单日历视图", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;