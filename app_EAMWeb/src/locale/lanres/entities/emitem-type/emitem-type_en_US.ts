import EMItemType_en_US_Base from './emitem-type_en_US_base';

function getLocaleResource(){
    const EMItemType_en_US_OwnData = {};
    const targetData = Object.assign(EMItemType_en_US_Base(), EMItemType_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
