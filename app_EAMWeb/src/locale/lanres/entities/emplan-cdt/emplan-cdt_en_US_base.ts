import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			emplancdtid: commonLogic.appcommonhandle("计划条件标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			triggerval: commonLogic.appcommonhandle("临界值",null),
			dpvaltype: commonLogic.appcommonhandle("测点值类型",null),
			lastval: commonLogic.appcommonhandle("上次触发值",null),
			nowval: commonLogic.appcommonhandle("预警值",null),
			description: commonLogic.appcommonhandle("描述",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			triggerdp: commonLogic.appcommonhandle("触发操作",null),
			emplancdtname: commonLogic.appcommonhandle("计划条件名称",null),
			dptype: commonLogic.appcommonhandle("测点类型",null),
			objname: commonLogic.appcommonhandle("位置",null),
			dpname: commonLogic.appcommonhandle("测点",null),
			planname: commonLogic.appcommonhandle("计划",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			dpid: commonLogic.appcommonhandle("测点",null),
			planid: commonLogic.appcommonhandle("计划",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			objid: commonLogic.appcommonhandle("位置",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("计划条件",null),
					title: commonLogic.appcommonhandle("计划条件",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("计划条件",null),
					title: commonLogic.appcommonhandle("计划条件",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("计划条件信息",null), 
					grouppanel13: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划条件标识",null), 
					srfmajortext: commonLogic.appcommonhandle("计划条件名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					dpname: commonLogic.appcommonhandle("测点",null), 
					dptype: commonLogic.appcommonhandle("测点类型",null), 
					dpvaltype: commonLogic.appcommonhandle("测点值类型",null), 
					triggerdp: commonLogic.appcommonhandle("触发操作",null), 
					triggerval: commonLogic.appcommonhandle("临界值",null), 
					nowval: commonLogic.appcommonhandle("预警值",null), 
					lastval: commonLogic.appcommonhandle("上次触发值",null), 
					planname: commonLogic.appcommonhandle("计划",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					planid: commonLogic.appcommonhandle("计划",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					emplancdtid: commonLogic.appcommonhandle("计划条件标识",null), 
					dpid: commonLogic.appcommonhandle("测点",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					planname: commonLogic.appcommonhandle("计划",null),
					dpname: commonLogic.appcommonhandle("测点",null),
					dptype: commonLogic.appcommonhandle("测点类型",null),
					dpvaltype: commonLogic.appcommonhandle("测点值类型",null),
					triggerdp: commonLogic.appcommonhandle("触发操作",null),
					triggerval: commonLogic.appcommonhandle("临界值",null),
					nowval: commonLogic.appcommonhandle("预警值",null),
					lastval: commonLogic.appcommonhandle("上次触发值",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;