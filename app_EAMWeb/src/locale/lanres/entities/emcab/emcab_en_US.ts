import EMCab_en_US_Base from './emcab_en_US_base';

function getLocaleResource(){
    const EMCab_en_US_OwnData = {};
    const targetData = Object.assign(EMCab_en_US_Base(), EMCab_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
