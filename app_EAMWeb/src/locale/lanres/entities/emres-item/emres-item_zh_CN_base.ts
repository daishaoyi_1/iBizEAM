import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("物品资源", null),
		fields: {
			price: commonLogic.appcommonhandle("单价",null),
			amount: commonLogic.appcommonhandle("总金额",null),
			emresitemname: commonLogic.appcommonhandle("物品资源名称",null),
			description: commonLogic.appcommonhandle("描述",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			pnum: commonLogic.appcommonhandle("安排用量",null),
			bdate: commonLogic.appcommonhandle("开始时间",null),
			snum: commonLogic.appcommonhandle("实际用量",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			emresitemid: commonLogic.appcommonhandle("物品资源标识",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			edate: commonLogic.appcommonhandle("结束时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			datafrom: commonLogic.appcommonhandle("数据来源",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			resrefobjname: commonLogic.appcommonhandle("引用对象",null),
			itembtypename: commonLogic.appcommonhandle("物品大类",null),
			sname: commonLogic.appcommonhandle("设备统计",null),
			equipcode: commonLogic.appcommonhandle("设备",null),
			itembtypeid: commonLogic.appcommonhandle("物品大类",null),
			resname_show: commonLogic.appcommonhandle("物品",null),
			itemmtypename: commonLogic.appcommonhandle("物品二级类",null),
			unitname: commonLogic.appcommonhandle("单位",null),
			itemmtypeid: commonLogic.appcommonhandle("物品二级类",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			resname: commonLogic.appcommonhandle("物品",null),
			itemtypeid: commonLogic.appcommonhandle("物品类型",null),
			teamname: commonLogic.appcommonhandle("班组",null),
			eqenable: commonLogic.appcommonhandle("设备逻辑有效",null),
			resid: commonLogic.appcommonhandle("物品",null),
			resrefobjid: commonLogic.appcommonhandle("引用对象",null),
			equipid: commonLogic.appcommonhandle("设备",null),
		},
			usedbyitem_list: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			amountbytypebyeq_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			usedbyitem_portlet: {
				usedbyitem: {
					title: commonLogic.appcommonhandle("最近使用记录", null)
				},
				uiactions: {
				},
			},
			amountbytypebyeq_portlet: {
				amountbytypebyeq: {
					title: commonLogic.appcommonhandle("物品类型消耗金额", null)
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;