import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("工单来源", null),
		fields: {
			description: commonLogic.appcommonhandle("描述",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			emwooriname: commonLogic.appcommonhandle("工单来源名称",null),
			wooriinfo: commonLogic.appcommonhandle("工单来源信息",null),
			emwooriid: commonLogic.appcommonhandle("工单来源标识",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emwooritype: commonLogic.appcommonhandle("来源类型",null),
			createman: commonLogic.appcommonhandle("建立人",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("工单来源",null),
					title: commonLogic.appcommonhandle("工单来源数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("工单来源",null),
					title: commonLogic.appcommonhandle("工单来源选择表格视图",null),
				},
			},
			main_grid: {
				columns: {
					wooriinfo: commonLogic.appcommonhandle("工单来源信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;