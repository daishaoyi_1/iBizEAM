import EMWOORI_zh_CN_Base from './emwoori_zh_CN_base';

function getLocaleResource(){
    const EMWOORI_zh_CN_OwnData = {};
    const targetData = Object.assign(EMWOORI_zh_CN_Base(), EMWOORI_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;