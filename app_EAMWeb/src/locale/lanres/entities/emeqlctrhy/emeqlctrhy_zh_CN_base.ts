import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("润滑油位置", null),
		fields: {
			yl: commonLogic.appcommonhandle("油量",null),
			eqmodelcode: commonLogic.appcommonhandle("型号",null),
			rhytype: commonLogic.appcommonhandle("润滑油类型",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			eqlocationinfo: commonLogic.appcommonhandle("润滑油信息",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			emeqlocationid: commonLogic.appcommonhandle("位置标识",null),
			equipid: commonLogic.appcommonhandle("设备",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("润滑油位置",null),
					title: commonLogic.appcommonhandle("润滑油位置",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("润滑油位置",null),
					title: commonLogic.appcommonhandle("润滑油位置",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("钢丝绳位置信息",null), 
					grouppanel8: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("润滑油信息",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emeqlocationid: commonLogic.appcommonhandle("位置标识",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					rhytype: commonLogic.appcommonhandle("润滑油类型",null), 
					eqmodelcode: commonLogic.appcommonhandle("型号",null), 
					yl: commonLogic.appcommonhandle("油量",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					eqlocationinfo: commonLogic.appcommonhandle("润滑油信息",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					rhytype: commonLogic.appcommonhandle("润滑油类型",null),
					eqmodelcode: commonLogic.appcommonhandle("型号",null),
					yl: commonLogic.appcommonhandle("油量",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;