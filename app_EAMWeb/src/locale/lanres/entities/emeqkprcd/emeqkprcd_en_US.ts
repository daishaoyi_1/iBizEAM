import EMEQKPRCD_en_US_Base from './emeqkprcd_en_US_base';

function getLocaleResource(){
    const EMEQKPRCD_en_US_OwnData = {};
    const targetData = Object.assign(EMEQKPRCD_en_US_Base(), EMEQKPRCD_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
