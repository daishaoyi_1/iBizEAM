import EMAsset_en_US_Base from './emasset_en_US_base';

function getLocaleResource(){
    const EMAsset_en_US_OwnData = {};
    const targetData = Object.assign(EMAsset_en_US_Base(), EMAsset_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
