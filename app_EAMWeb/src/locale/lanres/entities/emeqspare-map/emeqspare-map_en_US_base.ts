import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			emeqsparemapid: commonLogic.appcommonhandle("备件包引用标识",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emeqsparemapname: commonLogic.appcommonhandle("备件包引用",null),
			refobjname: commonLogic.appcommonhandle("引用对象",null),
			eqsparename: commonLogic.appcommonhandle("备件包",null),
			refobjid: commonLogic.appcommonhandle("对象标识",null),
			eqspareid: commonLogic.appcommonhandle("备件包",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("备件包应用",null),
					title: commonLogic.appcommonhandle("备件包应用",null),
				},
				dataview9: {
					caption: commonLogic.appcommonhandle("备件包引用",null),
					title: commonLogic.appcommonhandle("备件包引用数据视图",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("备件包引用",null),
					title: commonLogic.appcommonhandle("备件包引用",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("备件包引用",null),
					title: commonLogic.appcommonhandle("备件包引用",null),
				},
				dashboardview9: {
					caption: commonLogic.appcommonhandle("备件包引用",null),
					title: commonLogic.appcommonhandle("备件包引用数据看板视图",null),
				},
				gridview9: {
					caption: commonLogic.appcommonhandle("备件包应用",null),
					title: commonLogic.appcommonhandle("备件包应用",null),
				},
				equipgridview9: {
					caption: commonLogic.appcommonhandle("备件包应用",null),
					title: commonLogic.appcommonhandle("备件包应用",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("备件包引用信息",null), 
					grouppanel5: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("备件包引用标识",null), 
					srfmajortext: commonLogic.appcommonhandle("备件包引用",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqsparename: commonLogic.appcommonhandle("备件包",null), 
					refobjname: commonLogic.appcommonhandle("引用对象",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emeqsparemapid: commonLogic.appcommonhandle("备件包引用标识",null), 
					refobjid: commonLogic.appcommonhandle("对象标识",null), 
					eqspareid: commonLogic.appcommonhandle("备件包",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("备件包引用信息",null), 
					grouppanel5: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("备件包引用标识",null), 
					srfmajortext: commonLogic.appcommonhandle("备件包引用",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqsparename: commonLogic.appcommonhandle("备件包",null), 
					refobjname: commonLogic.appcommonhandle("引用对象",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emeqsparemapid: commonLogic.appcommonhandle("备件包引用标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					refobjname: commonLogic.appcommonhandle("引用对象",null),
					eqsparename: commonLogic.appcommonhandle("备件包",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			byeq_list: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			card_dataview: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			dataview9toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
			},
			equipgridview9toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
			},
			gridview9toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
			},
			byeq_portlet: {
				byeq: {
					title: commonLogic.appcommonhandle("备件包信息", null)
			  	},
				uiactions: {
				},
			},
			gridview9_portlet: {
				gridview9: {
					title: commonLogic.appcommonhandle("备件包应用", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;