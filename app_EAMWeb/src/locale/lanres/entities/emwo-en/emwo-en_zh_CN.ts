import EMWO_EN_zh_CN_Base from './emwo-en_zh_CN_base';

function getLocaleResource(){
    const EMWO_EN_zh_CN_OwnData = {};
    const targetData = Object.assign(EMWO_EN_zh_CN_Base(), EMWO_EN_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;