import EMBerth_en_US_Base from './emberth_en_US_base';

function getLocaleResource(){
    const EMBerth_en_US_OwnData = {};
    const targetData = Object.assign(EMBerth_en_US_Base(), EMBerth_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
