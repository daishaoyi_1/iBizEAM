import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			assetinprice: commonLogic.appcommonhandle("盘盈金额",null),
			assetcheckprice: commonLogic.appcommonhandle("盘点清查金额",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			isnew: commonLogic.appcommonhandle("最新清盘",null),
			assetoutprice: commonLogic.appcommonhandle("盘亏金额",null),
			assetclearlct: commonLogic.appcommonhandle("使用部门",null),
			emassetclearname: commonLogic.appcommonhandle("资产清盘记录名称",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			assetoutnum: commonLogic.appcommonhandle("盘亏数量",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			assetclearstate: commonLogic.appcommonhandle("状况",null),
			assetcleardate: commonLogic.appcommonhandle("盘点日期",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			assetinnum: commonLogic.appcommonhandle("盘盈数量",null),
			assetchecknum: commonLogic.appcommonhandle("盘点清查数量",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			emassetclearid: commonLogic.appcommonhandle("资产清盘记录标识",null),
			assetsort: commonLogic.appcommonhandle("资产排序",null),
			assetcode: commonLogic.appcommonhandle("资产代码",null),
			assetclassname: commonLogic.appcommonhandle("资产类型",null),
			eqmodelcode: commonLogic.appcommonhandle("规格型号",null),
			purchdate: commonLogic.appcommonhandle("入帐时间",null),
			originalcost: commonLogic.appcommonhandle("资产原值",null),
			assetclassid: commonLogic.appcommonhandle("资产类型",null),
			emassetname: commonLogic.appcommonhandle("资产",null),
			emassetid: commonLogic.appcommonhandle("资产",null),
		},
			views: {
				editview_908: {
					caption: commonLogic.appcommonhandle("资产清盘记录",null),
					title: commonLogic.appcommonhandle("资产清盘记录",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("资产清盘记录",null),
					title: commonLogic.appcommonhandle("资产清盘记录",null),
				},
				gridview_5564: {
					caption: commonLogic.appcommonhandle("资产清盘记录",null),
					title: commonLogic.appcommonhandle("资产清盘记录",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("资产清盘记录",null),
					title: commonLogic.appcommonhandle("资产清盘记录",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("资产清盘记录信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("资产清盘记录标识",null), 
					srfmajortext: commonLogic.appcommonhandle("资产清盘记录名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emassetclearname: commonLogic.appcommonhandle("资产清盘记录名称",null), 
					emassetname: commonLogic.appcommonhandle("资产",null), 
					assetinnum: commonLogic.appcommonhandle("盘盈数量",null), 
					assetinprice: commonLogic.appcommonhandle("盘盈金额",null), 
					assetoutnum: commonLogic.appcommonhandle("盘亏数量",null), 
					assetoutprice: commonLogic.appcommonhandle("盘亏金额",null), 
					assetchecknum: commonLogic.appcommonhandle("盘点清查数量",null), 
					assetcheckprice: commonLogic.appcommonhandle("盘点清查金额",null), 
					assetclearstate: commonLogic.appcommonhandle("状况",null), 
					assetclearlct: commonLogic.appcommonhandle("使用部门",null), 
					emassetclearid: commonLogic.appcommonhandle("资产清盘记录标识",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("资产清盘记录信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("资产清盘记录标识",null), 
					srfmajortext: commonLogic.appcommonhandle("资产清盘记录名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emassetclearname: commonLogic.appcommonhandle("资产清盘记录名称",null), 
					emassetname: commonLogic.appcommonhandle("资产",null), 
					assetinnum: commonLogic.appcommonhandle("盘盈数量",null), 
					assetinprice: commonLogic.appcommonhandle("盘盈金额",null), 
					assetoutnum: commonLogic.appcommonhandle("盘亏数量",null), 
					assetoutprice: commonLogic.appcommonhandle("盘亏金额",null), 
					assetchecknum: commonLogic.appcommonhandle("盘点清查数量",null), 
					assetcheckprice: commonLogic.appcommonhandle("盘点清查金额",null), 
					assetclearstate: commonLogic.appcommonhandle("状况",null), 
					assetclearlct: commonLogic.appcommonhandle("使用部门",null), 
					emassetclearid: commonLogic.appcommonhandle("资产清盘记录标识",null), 
					emassetid: commonLogic.appcommonhandle("资产",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					emassetclearname: commonLogic.appcommonhandle("资产清盘记录名称",null),
					assetinprice: commonLogic.appcommonhandle("盘盈金额",null),
					assetoutprice: commonLogic.appcommonhandle("盘亏金额",null),
					assetinnum: commonLogic.appcommonhandle("盘盈数量",null),
					assetoutnum: commonLogic.appcommonhandle("盘亏数量",null),
					assetchecknum: commonLogic.appcommonhandle("盘点清查数量",null),
					assetcheckprice: commonLogic.appcommonhandle("盘点清查金额",null),
					assetclearstate: commonLogic.appcommonhandle("状况",null),
					assetclearlct: commonLogic.appcommonhandle("使用部门",null),
					assetcleardate: commonLogic.appcommonhandle("盘点日期",null),
					emassetname: commonLogic.appcommonhandle("资产",null),
					eqmodelcode: commonLogic.appcommonhandle("规格型号",null),
					purchdate: commonLogic.appcommonhandle("入帐时间",null),
					originalcost: commonLogic.appcommonhandle("资产原值",null),
					assetcode: commonLogic.appcommonhandle("资产代码",null),
					assetclassname: commonLogic.appcommonhandle("资产类型",null),
					assetsort: commonLogic.appcommonhandle("资产排序",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emassetclearname_like: commonLogic.appcommonhandle("资产清盘记录名称(文本包含(%))",null), 
					n_emassetname_like: commonLogic.appcommonhandle("资产(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			editview_908toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridview_5564toolbar_toolbar: {
				tbitem1_clear: {
					caption: commonLogic.appcommonhandle("资产盘点",null),
					tip: commonLogic.appcommonhandle("资产盘点",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;