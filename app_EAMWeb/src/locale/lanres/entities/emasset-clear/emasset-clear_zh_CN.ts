import EMAssetClear_zh_CN_Base from './emasset-clear_zh_CN_base';

function getLocaleResource(){
    const EMAssetClear_zh_CN_OwnData = {};
    const targetData = Object.assign(EMAssetClear_zh_CN_Base(), EMAssetClear_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;