import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emdrwgmapname: commonLogic.appcommonhandle("文档引用",null),
			description: commonLogic.appcommonhandle("描述",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			emdrwgmapid: commonLogic.appcommonhandle("文档引用标识",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			refobjname: commonLogic.appcommonhandle("引用对象",null),
			drwgname: commonLogic.appcommonhandle("文档",null),
			refobjid: commonLogic.appcommonhandle("引用对象",null),
			drwgid: commonLogic.appcommonhandle("文档",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("文档引用",null),
					title: commonLogic.appcommonhandle("文档引用编辑视图",null),
				},
				equipgridview9: {
					caption: commonLogic.appcommonhandle("文档引用",null),
					title: commonLogic.appcommonhandle("文档引用表格视图",null),
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("文档引用基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("文档引用标识",null), 
					srfmajortext: commonLogic.appcommonhandle("文档引用",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emdrwgmapname: commonLogic.appcommonhandle("文档引用",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emdrwgmapid: commonLogic.appcommonhandle("文档引用标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					refobjname: commonLogic.appcommonhandle("引用对象",null),
					drwgname: commonLogic.appcommonhandle("文档",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			byeq_list: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
			equipgridview9toolbar_toolbar: {
			},
			byeq_portlet: {
				byeq: {
					title: commonLogic.appcommonhandle("文档信息", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;