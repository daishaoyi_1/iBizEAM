import EMOutputAuthServiceBase from './emoutput-auth-service-base';


/**
 * 能力权限服务对象
 *
 * @export
 * @class EMOutputAuthService
 * @extends {EMOutputAuthServiceBase}
 */
export default class EMOutputAuthService extends EMOutputAuthServiceBase {

    /**
     * Creates an instance of  EMOutputAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMOutputAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}