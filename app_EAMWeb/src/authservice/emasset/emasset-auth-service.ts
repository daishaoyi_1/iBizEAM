import EMAssetAuthServiceBase from './emasset-auth-service-base';


/**
 * 资产权限服务对象
 *
 * @export
 * @class EMAssetAuthService
 * @extends {EMAssetAuthServiceBase}
 */
export default class EMAssetAuthService extends EMAssetAuthServiceBase {

    /**
     * Creates an instance of  EMAssetAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}