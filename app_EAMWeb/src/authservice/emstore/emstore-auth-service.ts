import EMStoreAuthServiceBase from './emstore-auth-service-base';


/**
 * 仓库权限服务对象
 *
 * @export
 * @class EMStoreAuthService
 * @extends {EMStoreAuthServiceBase}
 */
export default class EMStoreAuthService extends EMStoreAuthServiceBase {

    /**
     * Creates an instance of  EMStoreAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStoreAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}