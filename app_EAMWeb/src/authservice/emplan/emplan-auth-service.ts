import EMPlanAuthServiceBase from './emplan-auth-service-base';


/**
 * 计划权限服务对象
 *
 * @export
 * @class EMPlanAuthService
 * @extends {EMPlanAuthServiceBase}
 */
export default class EMPlanAuthService extends EMPlanAuthServiceBase {

    /**
     * Creates an instance of  EMPlanAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}