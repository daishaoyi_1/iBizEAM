import EMBrandAuthServiceBase from './embrand-auth-service-base';


/**
 * 品牌权限服务对象
 *
 * @export
 * @class EMBrandAuthService
 * @extends {EMBrandAuthServiceBase}
 */
export default class EMBrandAuthService extends EMBrandAuthServiceBase {

    /**
     * Creates an instance of  EMBrandAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMBrandAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}