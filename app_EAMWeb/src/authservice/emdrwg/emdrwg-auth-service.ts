import EMDRWGAuthServiceBase from './emdrwg-auth-service-base';


/**
 * 文档权限服务对象
 *
 * @export
 * @class EMDRWGAuthService
 * @extends {EMDRWGAuthServiceBase}
 */
export default class EMDRWGAuthService extends EMDRWGAuthServiceBase {

    /**
     * Creates an instance of  EMDRWGAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDRWGAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}