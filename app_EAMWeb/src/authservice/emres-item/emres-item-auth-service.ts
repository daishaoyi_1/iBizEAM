import EMResItemAuthServiceBase from './emres-item-auth-service-base';


/**
 * 物品资源权限服务对象
 *
 * @export
 * @class EMResItemAuthService
 * @extends {EMResItemAuthServiceBase}
 */
export default class EMResItemAuthService extends EMResItemAuthServiceBase {

    /**
     * Creates an instance of  EMResItemAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMResItemAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}