import SEQUENCEAuthServiceBase from './sequence-auth-service-base';


/**
 * 序列号权限服务对象
 *
 * @export
 * @class SEQUENCEAuthService
 * @extends {SEQUENCEAuthServiceBase}
 */
export default class SEQUENCEAuthService extends SEQUENCEAuthServiceBase {

    /**
     * Creates an instance of  SEQUENCEAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  SEQUENCEAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}