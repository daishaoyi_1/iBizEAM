import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取studio链接数据
mock.onGet('./assets/json/view-config.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
                "emitemtypegridexpview": {
            "title": "物品类型表格导航视图",
            "caption": "物品类型",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeGridExpView",
            "viewtag": "00299a7e72bc57af235585f219db3159"
        },
        "emitemtypegridview": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeGridView",
            "viewtag": "00751c15423366b8738c166e2afd4e86"
        },
        "emassetpickupgridview": {
            "title": "资产选择表格视图",
            "caption": "资产",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETPickupGridView",
            "viewtag": "0098781f1e1dd58fca268291027efbaf"
        },
        "emeqsparemapgridview": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREMAPGridView",
            "viewtag": "013bc8a5fb55d671563f83ac88cda7f7"
        },
        "emeqlctrhygridview": {
            "title": "润滑油位置",
            "caption": "润滑油位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTRHYGridView",
            "viewtag": "01433ecfad20e482c9228cabe2ee3de0"
        },
        "emstoreparteditview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREPARTEditView",
            "viewtag": "0145b2180bbcd5c5addc5d077105961a"
        },
        "emrfodetabexpview": {
            "title": "现象",
            "caption": "现象",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODETabExpView",
            "viewtag": "0254c140083d7f8a22e12149d7dbad60"
        },
        "pfunitgridview": {
            "title": "计量单位",
            "caption": "计量单位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFUNITGridView",
            "viewtag": "02c94bc50171ee375bd3d44f0b85fe14"
        },
        "emwo_innerconfirmedgridview": {
            "title": "内部工单表格视图",
            "caption": "内部工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERConfirmedGridView",
            "viewtag": "02ee7c90ad8565be2789d68d67785f08"
        },
        "empogridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPOGridView",
            "viewtag": "03675910b6c4640a13c0f4140201046e"
        },
        "emeqkeepeditview9": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQKEEPEditView9",
            "viewtag": "045d66a45dfc2ac249d6c5d4981145a6"
        },
        "pfemppickupgridview": {
            "title": "职员选择表格视图",
            "caption": "职员",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpPickupGridView",
            "viewtag": "04a9ace562bc2e5fa76c7931bd40f889"
        },
        "emitempusewaitissuegridview": {
            "title": "领料单",
            "caption": "领料单-待发料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseWaitIssueGridView",
            "viewtag": "055963ddb13a8c05003a68c34acc2ea7"
        },
        "emitemcstabexpview": {
            "title": "库间调整单分页导航视图",
            "caption": "库间调整单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSTabExpView",
            "viewtag": "0670719d2f5d7f7f1563195ae46c5dac"
        },
        "emplantempleditview": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLEditView",
            "viewtag": "06a385b661866fe39c0a912d1944a8cf"
        },
        "emwoindexpickupview": {
            "title": "工单数据选择视图",
            "caption": "工单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOIndexPickupView",
            "viewtag": "06d31b143f03d7914ee8bf51a5fc7cc9"
        },
        "emserviceinfoview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>基本信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceInfoView9",
            "viewtag": "0724053e34e6d11bee937727f26175f8"
        },
        "emeqwlwleditview": {
            "title": "设备运行日志编辑视图",
            "caption": "设备运行日志",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLWLEditView",
            "viewtag": "07860293c43ad6eab2440c789422efa2"
        },
        "empodetaileditview9_editmode": {
            "title": "采购订单条目",
            "caption": "采购订单条目",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILEditView9_EditMode",
            "viewtag": "07a90ae0be7cfe4493ec62bb6dd2bcd8"
        },
        "emeqlctgsstabexpview": {
            "title": "钢丝绳位置超期预警",
            "caption": "钢丝绳位置超期预警",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSTabExpView",
            "viewtag": "07bd75896dc15dcd49ba5d57e105c4e6"
        },
        "emservicecontactview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>联系信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceContactView9",
            "viewtag": "0956b530277fc595cdd8c54068c1fd5f"
        },
        "emeqsparedetaileditview": {
            "title": "备件包明细",
            "caption": "备件包明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILEditView",
            "viewtag": "095b356064013d1fe71771b75ca6dbfe"
        },
        "emwplistcosteditview": {
            "title": "询价单编辑视图",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTCOSTEditView",
            "viewtag": "09ad3a83b0e34aac0f60864d0730dda3"
        },
        "emproductgridview": {
            "title": "试用品",
            "caption": "试用品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPRODUCTGridView",
            "viewtag": "0acaddf8473367f0800e7c37205eb063"
        },
        "emeqsetuppickupview": {
            "title": "更换安装数据选择视图",
            "caption": "更换安装",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPPickupView",
            "viewtag": "0b88e2e073970a899ed7e2e34d2a84b8"
        },
        "emwplisttreeexpview": {
            "title": "采购申请树导航视图",
            "caption": "采购申请",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListTreeExpView",
            "viewtag": "0cf7f1c1a00db80827f7b36601a03fb9"
        },
        "emwo_innerwovieweditview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERWOViewEditView",
            "viewtag": "0d3c73215f1072d99ef057001eef1ffa"
        },
        "emitemcseditview9_editmode": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemCSEditView9_EditMode",
            "viewtag": "0e97baefe424809a7afd48fdefa312f9"
        },
        "emwplistdraftgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListDraftGridView",
            "viewtag": "0ee6021184e1f1dd1e11fdc77f788786"
        },
        "emwplistpodgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListPodGridView",
            "viewtag": "108abef0c27783c0a1c59d21eb291c5f"
        },
        "emeqsetupeditview9_editmode": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPEditView9_EditMode",
            "viewtag": "10a76f2f664ac4b9967df11ddc1d10a7"
        },
        "emitemtradegridview": {
            "title": "物品交易表格视图",
            "caption": "物品交易",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMTRADEGridView",
            "viewtag": "10d99b2c6e650a76eb8cd566376e000b"
        },
        "emeqlcttirespickupgridview": {
            "title": "轮胎位置选择表格视图",
            "caption": "轮胎位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTTIRESPickupGridView",
            "viewtag": "112c36fb37d93eeb5173212fe4f4f789"
        },
        "emwo_oscconfirmedgridview": {
            "title": "外委保养工单表格视图",
            "caption": "外委工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCConfirmedGridView",
            "viewtag": "11c2d2a6acfa74137f3e71d835e391d8"
        },
        "emwplistfillcosteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListFillCostEditView9",
            "viewtag": "141d426336433bb8d1d8a01eb2801b5d"
        },
        "emeqcheckeditview9": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQCHECKEditView9",
            "viewtag": "1450ec1c67898c375f9a6990466a5581"
        },
        "emrfodemapdataview": {
            "title": "现象引用数据视图",
            "caption": "现象引用",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEMapDataView",
            "viewtag": "1558c07b942fab195d426a36561e4b55"
        },
        "emeqsparequickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareQuickView",
            "viewtag": "162ad08b077f613b82a2aa3540125683"
        },
        "emwo_dpeditview_editmode": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPEditView_EditMode",
            "viewtag": "165b334c10d4980ba8539260005c32be"
        },
        "emobjmaplocationtreeview": {
            "title": "位置信息",
            "caption": "位置信息",
            "viewtype": "DETREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMObjMapLocationTreeView",
            "viewtag": "1667d8e2ed75111df39d548ca05ec206"
        },
        "emequipeditview_editmode": {
            "title": "基本信息",
            "caption": "基本信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEquipEditView_EditMode",
            "viewtag": "171898e6f4c7a247d735068d4d3cb1b8"
        },
        "sequencegridview": {
            "title": "序列号表格视图",
            "caption": "序列号",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "SEQUENCEGridView",
            "viewtag": "17739bf89647b18e0c16198b964cb30b"
        },
        "emeqkppickupgridview": {
            "title": "设备关键点选择表格视图",
            "caption": "设备关键点",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPPickupGridView",
            "viewtag": "1899736ab44809eb0dfa5452b8893a2f"
        },
        "emdrwgeditview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGEditView",
            "viewtag": "18bedf17550dca06530d8d1470b788e9"
        },
        "emrfodegridview": {
            "title": "现象",
            "caption": "现象",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEGridView",
            "viewtag": "193cf07f08f6448c6166184c8081c2aa"
        },
        "emitemrineditview": {
            "title": "入库单编辑视图",
            "caption": "入库单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMRINEditView",
            "viewtag": "1a4bf5fbe641c5ae04aabc16b4950ebb"
        },
        "emassetgridview_bf": {
            "title": "报废资产",
            "caption": "报废资产",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETGridView_BF",
            "viewtag": "1b788edc55f8d8c2d1c4b298adc182df"
        },
        "emeqsetupeditview9": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSETUPEditView9",
            "viewtag": "1b9e62f46991d036786cdceec5288d6e"
        },
        "emwplisteditview": {
            "title": "采购申请编辑视图",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTEditView",
            "viewtag": "1bb337d8433c77da88f187b1eb0db265"
        },
        "emoutputeditview9": {
            "title": "能力信息",
            "caption": "能力信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTEditView9",
            "viewtag": "1c1a2a1eb45370f331f8d5355ca7f695"
        },
        "emservicefileview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>附件信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceFileView9",
            "viewtag": "1c23ef3bb8ce4871ddfa7623920dc8b5"
        },
        "emitemprtneditview9_editmode": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPRtnEditView9_EditMode",
            "viewtag": "1c6a59a68c904d83c9b313a3ae66edb8"
        },
        "emeqkeepgridview": {
            "title": "保养记录",
            "caption": "保养记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQKeepGridView",
            "viewtag": "1d0eac0c4dfb6dd669b494b444701806"
        },
        "emrfomopickupgridview": {
            "title": "模式选择表格视图",
            "caption": "模式",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOMOPickupGridView",
            "viewtag": "1d107adacfe3f22a02aff712b12c1116"
        },
        "emservicepickupgridview": {
            "title": "服务商选择表格视图",
            "caption": "服务商",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSERVICEPickupGridView",
            "viewtag": "1d1e70a5b9543304e8ed1e67cd4359fc"
        },
        "empurplanpickupview": {
            "title": "计划修理数据选择视图",
            "caption": "计划修理",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPURPLANPickupView",
            "viewtag": "1d3dffff56f0f8225c55bd8652c9768f"
        },
        "emplantempleditview_editmode": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanTemplEditView_EditMode",
            "viewtag": "1dccd45494894dd35174db1f415a7f06"
        },
        "embrandpickupview": {
            "title": "品牌数据选择视图",
            "caption": "品牌",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBRANDPickupView",
            "viewtag": "1e26c5698500be5b232f96ceb6750271"
        },
        "emwplistgridview_ydh": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTGridView_Ydh",
            "viewtag": "1e6ec592f18c98018c7195aafdd886c8"
        },
        "planschedule_oeditview": {
            "title": "自定义间隔天数编辑视图",
            "caption": "自定义间隔天数",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PLANSCHEDULE_OEditView",
            "viewtag": "1fa0b104e0f682b7c231b42fbad5074e"
        },
        "emitemeditview_editmode": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMEditView_EditMode",
            "viewtag": "1fdb3aec31b15dc994c87c5647b7c97c"
        },
        "emwplistcostpickupview": {
            "title": "询价单数据选择视图",
            "caption": "询价单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTCOSTPickupView",
            "viewtag": "1fde83a42aba69496ab0574941a04a08"
        },
        "pfunitpickupgridview": {
            "title": "计量单位选择表格视图",
            "caption": "计量单位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFUNITPickupGridView",
            "viewtag": "2004a8e359ab90d68d2cc18dbbaea5e4"
        },
        "emwplisteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListEditView9",
            "viewtag": "206ec4a61e19c76ccdf230cb045cfa84"
        },
        "emeqsparepickupgridview": {
            "title": "备件包选择表格视图",
            "caption": "备件包",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREPickupGridView",
            "viewtag": "21669f5aa6d4c956282494807ef5ab59"
        },
        "emitempusedrafteditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseDraftEditView9",
            "viewtag": "2288eae1baef62b00803a5edd8d5f1be"
        },
        "emrfoacpickupview": {
            "title": "方案数据选择视图",
            "caption": "方案",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOACPickupView",
            "viewtag": "22c4268fcb5bd9a5d544f48c6f4141aa"
        },
        "emwo_innereditview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNEREditView",
            "viewtag": "2361099643586c627b770119262f2724"
        },
        "emitemtypeinfotreeexpview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeInfoTreeExpView",
            "viewtag": "23da2a9b093893566176cc8ec08f4cdd"
        },
        "emwo_osctabexpview": {
            "title": "外委保养工单分页导航视图",
            "caption": "外委保养工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCTabExpView",
            "viewtag": "24136af6534fd57600b3a9faf620f528"
        },
        "emeqkppickupview": {
            "title": "设备关键点数据选择视图",
            "caption": "设备关键点",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPPickupView",
            "viewtag": "25427d06dbcb7c87ca6df31b39eafc8b"
        },
        "emitemeditview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMEditView",
            "viewtag": "25964c045ead74bbe0c1e0a58ff7fb07"
        },
        "pfempeditview_editmode": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEmpEditView_EditMode",
            "viewtag": "25a1dd82a2e45a928d12f9760c0b68cd"
        },
        "emwopickupgridview": {
            "title": "工单选择表格视图",
            "caption": "工单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOPickupGridView",
            "viewtag": "25a2d0b0d334a527ed021b59143f10b2"
        },
        "emitempickupview": {
            "title": "物品数据选择视图",
            "caption": "物品",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPickupView",
            "viewtag": "25d5bba2ea6fd13e0b7b3135c298236a"
        },
        "planschedulegridview": {
            "title": "计划时刻设置表格视图",
            "caption": "计划时刻设置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PLANSCHEDULEGridView",
            "viewtag": "262138498ce3eb9459995bee4d764e53"
        },
        "emplandashboardview": {
            "title": "计划数据看板视图",
            "caption": "计划",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanDashboardView",
            "viewtag": "2683e297bf434b81564528df53d16b20"
        },
        "emeqsetupeditview": {
            "title": "更换安装编辑视图",
            "caption": "更换安装",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPEditView",
            "viewtag": "2977252e22a822a52c615bdffe091afd"
        },
        "emengridview": {
            "title": "能源",
            "caption": "能源",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENGridView",
            "viewtag": "29eea99beaa446ca9e276435ff403187"
        },
        "appportalview": {
            "title": "应用门户视图",
            "caption": "",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "r7rt_dyna",
            "viewname": "AppPortalView",
            "viewtag": "2aa99fe33c29dc9f244dd4f64b35f4d1"
        },
        "emrfodetypegridview": {
            "title": "现象分类",
            "caption": "现象分类",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODETYPEGridView",
            "viewtag": "2ae00e2f5556f24e9fdfc5c4388f7281"
        },
        "emeneditview": {
            "title": "能源编辑视图",
            "caption": "能源",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENEditView",
            "viewtag": "2bc1dbad21e69dd8bcabf478693434b6"
        },
        "emeqtypeeditview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTYPEEditView",
            "viewtag": "2c43c508e829600fb3cc64c7d960527b"
        },
        "emwplistpickupgridview": {
            "title": "采购申请选择表格视图",
            "caption": "采购申请",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTPickupGridView",
            "viewtag": "2d61d361e2270f72c10ad2bb12d142b9"
        },
        "emwplistpoeditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListPOEditView9",
            "viewtag": "2fb6bbd7063415a2620e8f5887a0b48f"
        },
        "pfempdeptempgridview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpDeptEmpGridView",
            "viewtag": "30d859afc6a71ef3c2927fecc7eac35b"
        },
        "emwplistconfimcostgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListConfimCostGridView",
            "viewtag": "30dc1fe0033ff81520071e30fd8d8895"
        },
        "emitempltoconfirmgridview": {
            "title": "损溢单",
            "caption": "损溢单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLToConfirmGridView",
            "viewtag": "3158f677cba93d2472bf52007605f2de"
        },
        "emeqkeepeditview": {
            "title": "维护保养编辑视图",
            "caption": "维护保养",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQKEEPEditView",
            "viewtag": "31687d0a70ff5d370520e621fbfb7754"
        },
        "emeqlctgsspickupgridview": {
            "title": "钢丝绳位置选择表格视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSPickupGridView",
            "viewtag": "31d177948b54964c5d9399ef3e108d68"
        },
        "emrfodequickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEQuickView",
            "viewtag": "321bdf12b76f2a8eaa19c3da4d004d97"
        },
        "emitemrineditview9_editmode": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMRINEditView9_EditMode",
            "viewtag": "322dea3adbbf873408e0e6e8a6b3882c"
        },
        "emeqlcttirespickupview": {
            "title": "轮胎位置数据选择视图",
            "caption": "轮胎位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTTIRESPickupView",
            "viewtag": "3332c5d0d6faef5d1782d61ac41eff0a"
        },
        "emeqmonitorgridview": {
            "title": "设备状态监控表格视图",
            "caption": "设备状态监控",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMonitorGridView",
            "viewtag": "34085fc7d06fab234e85c3f85822cc5a"
        },
        "emwogridview": {
            "title": "工单表格视图",
            "caption": "工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOGridView",
            "viewtag": "341da6e362f62ba71efd268b27c65ad0"
        },
        "emwo_eneditview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENEditView",
            "viewtag": "345ff519f8aa1ddb061490c2488fbf17"
        },
        "emequipdashboardview9": {
            "title": "设备主信息图表数据看板视图",
            "caption": "设备档案",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipDashboardView9",
            "viewtag": "34c8cbdf7eb525aa361d29c15e77141e"
        },
        "emeqahcalendarexpview": {
            "title": "活动历史日历导航视图",
            "caption": "活动历史",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHCalendarExpView",
            "viewtag": "350f46fc8a9c42892dd8dd19d143750b"
        },
        "embrandpickupgridview": {
            "title": "品牌选择表格视图",
            "caption": "品牌",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBRANDPickupGridView",
            "viewtag": "365fba0fd1ce4d099fa4822479166f73"
        },
        "empopickupview": {
            "title": "订单数据选择视图",
            "caption": "订单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPickupView",
            "viewtag": "3747513fcd029dfa942647e803f81d02"
        },
        "emrfodepickupgridview": {
            "title": "现象选择表格视图",
            "caption": "现象",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEPickupGridView",
            "viewtag": "37a73a57abb4994b6fcc0454cec89f2a"
        },
        "emrfoacgridview": {
            "title": "方案",
            "caption": "方案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOACGridView",
            "viewtag": "37c0112937fdbe024269ecaf9f61a0dc"
        },
        "emeqlocationalllocgridview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationAllLocGridView",
            "viewtag": "393fdf0ddca39fdce6e61d31bbbcfffa"
        },
        "emplancardview": {
            "title": "计划数据视图",
            "caption": "计划",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanCardView",
            "viewtag": "394057d0ca633ba2e9cc849439c8c932"
        },
        "emoutputpickupview": {
            "title": "能力数据选择视图",
            "caption": "能力",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTPickupView",
            "viewtag": "3949f37a80cf105b50e28c948edcc0e1"
        },
        "empoeditview9_editmode": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPOEditView9_EditMode",
            "viewtag": "39b5a686996f3c97f4b2ee163ec3aa10"
        },
        "planscheduleeditview": {
            "title": "计划时刻设置编辑视图",
            "caption": "计划时刻设置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PLANSCHEDULEEditView",
            "viewtag": "3a0a145b3a5d2c49f225e37f8f431cab"
        },
        "emeqlctgssgridview": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTGSSGridView",
            "viewtag": "3aa5fb5800f3876034da0f420ef39255"
        },
        "emassetcleareditview_908": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEAREditView_908",
            "viewtag": "3ab67d1643e912ed34f002875498e09e"
        },
        "emeqcheckeditview": {
            "title": "维修记录编辑视图",
            "caption": "维修记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQCHECKEditView",
            "viewtag": "3bcba83274f4438a2b7caadc5e06b55a"
        },
        "emassetclasspickupgridview": {
            "title": "资产类别选择表格视图",
            "caption": "资产类别",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSPickupGridView",
            "viewtag": "3c335913b1a0ab75b99986012dfcfe78"
        },
        "emeqmppickupview": {
            "title": "设备仪表数据选择视图",
            "caption": "设备仪表",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPPickupView",
            "viewtag": "3cab5e35dc9a82502376baaccc0e5c10"
        },
        "emrfomoeditview": {
            "title": "模式信息",
            "caption": "模式信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOMOEditView",
            "viewtag": "3cca012f4fa60e57fa3cc611f5aee8a5"
        },
        "emeqkprcdgridview": {
            "title": "关键点记录表格视图",
            "caption": "关键点记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPRCDGridView",
            "viewtag": "3dbcfe8c1dcd6f132c8e3785e9122d05"
        },
        "emwooripickupview": {
            "title": "工单来源数据选择视图",
            "caption": "工单来源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOORIPickupView",
            "viewtag": "3ea6d46516d8218294d34a59d7371c0b"
        },
        "emserviceevltoconfirmgridview": {
            "title": "服务商评估",
            "caption": "服务商评估-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlToConfirmGridView",
            "viewtag": "3eeb190a46a22ff09df91893548e2014"
        },
        "emstorepickupgridview": {
            "title": "仓库选择表格视图",
            "caption": "仓库",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPickupGridView",
            "viewtag": "3f7478a3c5e87b5fba682e59acecc9c7"
        },
        "eamindexview": {
            "title": "设备资产管理首页视图",
            "caption": "设备资产管理",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "eam_core",
            "viewname": "EAMIndexView",
            "viewtag": "40d34eca6c3e8e15f0f57a7e3976eb04"
        },
        "emitemtypetreeexpview": {
            "title": "物品类型树导航视图",
            "caption": "物品类型",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeTreeExpView",
            "viewtag": "414713a2409444f96084360ecbb6c1ab"
        },
        "emeqmaintanceeditview": {
            "title": "抢修记录编辑视图",
            "caption": "抢修记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQMAINTANCEEditView",
            "viewtag": "414e6517d6331940d4e46365b25e3e98"
        },
        "emwplistwaitpogridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWaitPoGridView",
            "viewtag": "42604619a64735bdc3e67dab55e0be4b"
        },
        "emitemgridview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemGridView",
            "viewtag": "428e823cc84d522fb5493aefa31781f2"
        },
        "emwo_endraftgridview": {
            "title": "能耗登记工单表格视图",
            "caption": "能耗工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENDraftGridView",
            "viewtag": "42b870c711df2839018ab1d07f6f0ee4"
        },
        "emwplistcostfillcostview": {
            "title": "询价单填报",
            "caption": "询价单填报",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostFillCostView",
            "viewtag": "43345ee88a76f58137020fb0828fa0cc"
        },
        "emrfodemapeditview": {
            "title": "现象引用编辑视图",
            "caption": "现象引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEMapEditView",
            "viewtag": "44248a93f9593eb9016d7460287f6529"
        },
        "emitemprtneditview9_new": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnEditView9_New",
            "viewtag": "44f5bb9d327c0749d2aa75e2004d8d88"
        },
        "emeqsetupgridview": {
            "title": "更换安装",
            "caption": "更换安装",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSetupGridView",
            "viewtag": "464e420f9d987f4866ca077037ee01d8"
        },
        "emeneditview9_editmode": {
            "title": "能源信息",
            "caption": "能源信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_en",
            "viewname": "EMENEditView9_EditMode",
            "viewtag": "46616d674cb8df81b2126c71f513aaaa"
        },
        "emwplistgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTGridView",
            "viewtag": "4750578f9f9466595f803ecac0acd619"
        },
        "emitempusepickupview": {
            "title": "领料单数据选择视图",
            "caption": "领料单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEPickupView",
            "viewtag": "47782801a8e838445601e03e13f887f5"
        },
        "emmachmodelpickupgridview": {
            "title": "机型选择表格视图",
            "caption": "机型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHMODELPickupGridView",
            "viewtag": "478b6337d8db293aefe322422df85c08"
        },
        "emobjectpickupgridview": {
            "title": "对象选择表格视图",
            "caption": "对象",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOBJECTPickupGridView",
            "viewtag": "486a2b0216ea92dbfe1fe1b78baa73fe"
        },
        "emrfoaceditview": {
            "title": "方案信息",
            "caption": "方案信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOACEditView",
            "viewtag": "48f7dee358bd7941a353327768d5b680"
        },
        "emeqsparemaindashboardview9": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMainDashboardView9",
            "viewtag": "4903396d56ac585de8cc7ef989d2f177"
        },
        "empoeditview": {
            "title": "订单编辑视图",
            "caption": "订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPOEditView",
            "viewtag": "49125cda017314ebbf2d8bea4226a17d"
        },
        "emasseteditview9": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMASSETEditView9",
            "viewtag": "49422a5e0d8d3c06e3b8b9a15b5807db"
        },
        "emwplistconfirmcosteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListConfirmCostEditView9",
            "viewtag": "49a2dbe99b43a1e0c81618e64a04f84d"
        },
        "emrfocapickupview": {
            "title": "原因数据选择视图",
            "caption": "原因",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOCAPickupView",
            "viewtag": "49af69275d04283fa27d351646a8fa7e"
        },
        "emeqcheckcaleditview9": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQCheckCalEditView9",
            "viewtag": "49dd63cdf5018e086ab14d6cb5c1e346"
        },
        "emwo_engridview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENGridView",
            "viewtag": "4a0d09aa106d3dba14d1e3ceaf06165a"
        },
        "emitemtypeeditview_itemtype_editmode": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_itemtype_EditMode",
            "viewtag": "4a0f773543a61a3a5533704e368df3eb"
        },
        "emapplytabexpview": {
            "title": "外委申请分页导航视图",
            "caption": "外委申请",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyTabExpView",
            "viewtag": "4a11513dea899f213a55cd728648157b"
        },
        "emeqsparedetaileditview_editmode": {
            "title": "备件包明细",
            "caption": "备件包明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILEditView_EditMode",
            "viewtag": "4a8121c6fa1b47a736d87f0d41ac4085"
        },
        "pfteamgridview": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFTEAMGridView",
            "viewtag": "4b08ade1e92a4d06e8f7c67323298a01"
        },
        "emservicedraftgridview": {
            "title": "服务商",
            "caption": "服务商审批-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceDraftGridView",
            "viewtag": "4c6a2f47751b7345a5fe3611317b4292"
        },
        "emserviceevlconfirmedgridview": {
            "title": "服务商评估",
            "caption": "服务商评估-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlConfirmedGridView",
            "viewtag": "4cb06d484ab3a15ecb8c5b6f978ac1ba"
        },
        "empodetailpickupgridview": {
            "title": "订单条目选择表格视图",
            "caption": "订单条目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILPickupGridView",
            "viewtag": "4cc4fce3fd52172ae7f756d1939e6c34"
        },
        "emeqsparemapdataview9": {
            "title": "备件包引用数据视图",
            "caption": "备件包引用",
            "viewtype": "DEDATAVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapDataView9",
            "viewtag": "4cf5b4957e19fddb725de2a27113558b"
        },
        "pfteameditview_editmode": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFTEAMEditView_EditMode",
            "viewtag": "4d91aca5ccf47ba93c881709705b34ac"
        },
        "emeqtypegridexpview": {
            "title": "设备类型表格导航视图",
            "caption": "设备类型",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeGridExpView",
            "viewtag": "4d9ca7330ee025a5a209f7d05253540e"
        },
        "emequipeditview9": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipEditView9",
            "viewtag": "4dc57f05398da372f51802c947c534b3"
        },
        "emassettabexpview": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETTabExpView",
            "viewtag": "4eb56a63c382258c0c1a2ccabae9389c"
        },
        "emrfomogridview": {
            "title": "模式",
            "caption": "模式",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOMOGridView",
            "viewtag": "4facc0cb6c8830328080ed421bb4b2eb"
        },
        "emapplytoconfirmgridview": {
            "title": "外委申请表格视图",
            "caption": "外委申请-待完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyToConfirmGridView",
            "viewtag": "503944d61e52b3228aa065450cd74cb8"
        },
        "emeqtypepickupgridview": {
            "title": "设备类型选择表格视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTYPEPickupGridView",
            "viewtag": "503b5eacba22c457c93a5d7f52160d26"
        },
        "emstorepartpickupview": {
            "title": "仓库库位数据选择视图",
            "caption": "仓库库位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPARTPickupView",
            "viewtag": "50ac93347b4b197c99df4f6bc0c58123"
        },
        "emeqdebugeditview9": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQDEBUGEditView9",
            "viewtag": "51394fba2ae5c4f01f85eeaa0f74ec1f"
        },
        "emwo_innertabexpview": {
            "title": "内部工单分页导航视图",
            "caption": "内部工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERTabExpView",
            "viewtag": "514070d29889796f8f4152025968b6ac"
        },
        "emrfodeeditview": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEEditView",
            "viewtag": "51d8f08df268550d0721dc5c1c3cb723"
        },
        "emwo_oscgridview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCGridView",
            "viewtag": "53a8f9336325b3cb34cf4d6e94b9ae82"
        },
        "emitemprtnconfirmedgridview": {
            "title": "还料单",
            "caption": "还料单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnConfirmedGridView",
            "viewtag": "53acdc5bbce56c8be01e1e17d9b89478"
        },
        "emitemprtneditview": {
            "title": "还料单编辑视图",
            "caption": "还料单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPRTNEditView",
            "viewtag": "54117096c2109748d2a8c151f12a5149"
        },
        "emwo_entoconfirmgridview": {
            "title": "能耗登记工单表格视图",
            "caption": "能耗工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENToConfirmGridView",
            "viewtag": "552e66724d4b34fb9e927fcd52ab7e2c"
        },
        "emitemtypepickuptreeview": {
            "title": "物品类型选择树视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPTREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypePickupTreeView",
            "viewtag": "56c0834dfc8fb65997271c0df1a40afe"
        },
        "emeqmaintancegridview": {
            "title": "抢修记录",
            "caption": "抢修记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQMaintanceGridView",
            "viewtag": "56f9100d6367b6a9e7e565e0c4f6939c"
        },
        "emstoreparteditview_9836": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREPARTEditView_9836",
            "viewtag": "571dd277c558698c2ca05150b9eb7841"
        },
        "emeqlocationpickupgridview": {
            "title": "位置选择表格视图",
            "caption": "位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONPickupGridView",
            "viewtag": "571e487e337b60033d4001f09d2f1878"
        },
        "emeqwleqgridview": {
            "title": "设备运行日志表格视图",
            "caption": "设备运行日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLEqGridView",
            "viewtag": "57aade39bd02a1bc8916b3bbd881a127"
        },
        "appindex": {
            "title": "iBizEAM",
            "caption": "iBizEAM",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "eam_core",
            "viewname": "appIndex",
            "viewtag": "57fcc385785639236bcadbd12fae9c58"
        },
        "emwo_eneditview_editmode": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENEditView_EditMode",
            "viewtag": "587737198e4f46fa023e46417d9d5217"
        },
        "pfcontracteditview9": {
            "title": "合同信息",
            "caption": "合同信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTEditView9",
            "viewtag": "58bf277c2cbee2a52a74106b41d778ae"
        },
        "emserviceeditview9": {
            "title": "PUR服务商信息",
            "caption": "PUR服务商信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEditView9",
            "viewtag": "58c4f88b60a44f034297a49d8f864c97"
        },
        "emitempleditview": {
            "title": "损溢单编辑视图",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPLEditView",
            "viewtag": "5982fb15294b4762ff8e00d11f12a279"
        },
        "pfdeptpickupgridview": {
            "title": "部门选择表格视图",
            "caption": "部门",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFDeptPickupGridView",
            "viewtag": "59a14eae6cf6369807fc76dec155879a"
        },
        "emeqdebugcaleditview9": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQDebugCalEditView9",
            "viewtag": "59d1b39b224d5846b20a7914318e4486"
        },
        "emstorepartpickupgridview": {
            "title": "仓库库位选择表格视图",
            "caption": "仓库库位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPARTPickupGridView",
            "viewtag": "5a54185fd49f62506492f33fc27827ea"
        },
        "emwo_innergridview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNERGridView",
            "viewtag": "5a97d6251bdce275669771c24fb5b6e5"
        },
        "emequippickupgridview": {
            "title": "设备档案选择表格视图",
            "caption": "设备档案",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipPickupGridView",
            "viewtag": "5c0326aa9b310f037de5e6eb270857be"
        },
        "emplanrecordgridview": {
            "title": "触发记录表格视图",
            "caption": "触发记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANRECORDGridView",
            "viewtag": "5cc84204e6e5d7e96ab954db45b157c3"
        },
        "emitemtypepickupgridview": {
            "title": "物品类型选择表格视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypePickupGridView",
            "viewtag": "5e894d9e36080ddc4d7b4e5448bd7d24"
        },
        "emeqcheckgridview": {
            "title": "维修记录",
            "caption": "维修记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQCheckGridView",
            "viewtag": "5f19751cbdf4c149af0b96af3043e377"
        },
        "emdrwgmapeditview": {
            "title": "文档引用编辑视图",
            "caption": "文档引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGMapEditView",
            "viewtag": "5fc1bdb2af5ed457da1115bd91b98e19"
        },
        "emenconsumeditview": {
            "title": "能耗编辑视图",
            "caption": "能耗",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMEditView",
            "viewtag": "609f585350400bdf554915dca6837136"
        },
        "emeqsparemapeditview_editmode": {
            "title": "备件包引用",
            "caption": "备件包引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareMapEditView_EditMode",
            "viewtag": "60eeff9bc94af0f2429aa2206058b5d5"
        },
        "emstockpickupview": {
            "title": "库存数据选择视图",
            "caption": "库存",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOCKPickupView",
            "viewtag": "60f1d13bd2af148643eda5a6feb02fbb"
        },
        "emwo_dpeditview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPEditView",
            "viewtag": "611985ee732078e174c62e53aa9f9c18"
        },
        "emwo_dpwovieweditview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPWOViewEditView",
            "viewtag": "6139394fb9d85cf4c096654567d9d799"
        },
        "emitemplgridview": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPLGridView",
            "viewtag": "62330dd54219668920b9e397e8053178"
        },
        "emoutputpickupgridview": {
            "title": "能力选择表格视图",
            "caption": "能力",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTPickupGridView",
            "viewtag": "62950881fdc22843e8ff34af2d557e98"
        },
        "emwplistcosteditview9": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostEditView9",
            "viewtag": "63c76d9d23f4953d0af77d6de9a80155"
        },
        "emitembaseinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemBaseInfoView",
            "viewtag": "63db6a201f9e11bccd3f07780dad526a"
        },
        "emwplistcostpickupgridview": {
            "title": "询价单选择表格视图",
            "caption": "询价单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTCOSTPickupGridView",
            "viewtag": "64bed4452adbdc48b9f432a2561b0998"
        },
        "emstockgridview": {
            "title": "库存物品",
            "caption": "库存物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStockGridView",
            "viewtag": "65b3d0c6e10a4f46a804e62d969a7a1d"
        },
        "emitemtypepickupview": {
            "title": "物品类型数据选择视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMTYPEPickupView",
            "viewtag": "67949bd6416cdb683c73643a5c6ad819"
        },
        "emeqmaintanceeditview9_editmode": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQMAINTANCEEditView9_EditMode",
            "viewtag": "67b3cfddcd792f6106aba52dc832bc10"
        },
        "emwo_enconfirmedgridview": {
            "title": "能耗登记工单表格视图",
            "caption": "能耗工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENConfirmedGridView",
            "viewtag": "681574f7632bd4093bd5d83d0a72b7d1"
        },
        "emeqkpkpeditview": {
            "title": "设备关键点编辑视图",
            "caption": "设备关键点",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPKPEditView",
            "viewtag": "686b8511c78fc6f8b4e3e059f7af79cf"
        },
        "emwo_innertoconfirmgridview": {
            "title": "内部工单表格视图",
            "caption": "内部工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERToConfirmGridView",
            "viewtag": "6888486f430db9a687c4f2f080965a40"
        },
        "emitempusetabexpview": {
            "title": "领料单分页导航视图",
            "caption": "领料单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseTabExpView",
            "viewtag": "688d7f1dbae083ef4bc4412406b3ec86"
        },
        "emplandetailgridview": {
            "title": "计划步骤",
            "caption": "计划步骤",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANDETAILGridView",
            "viewtag": "6896e173bceeaed74a0ce72bac4f904d"
        },
        "emitemroutdraftgridview": {
            "title": "退货单",
            "caption": "退货单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutDraftGridView",
            "viewtag": "6922c319b546610019e55323253f041b"
        },
        "emassetgridview": {
            "title": "固定资产台账",
            "caption": "固定资产台账",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETGridView",
            "viewtag": "6a10b8f5e87a12e8a24bdc67b437b47a"
        },
        "emeqlctrhyeditview": {
            "title": "润滑油位置",
            "caption": "润滑油位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTRHYEditView",
            "viewtag": "6a2a4551096d3828c346829603762b0b"
        },
        "emwoindexpickupdataview": {
            "title": "工单索引关系选择数据视图",
            "caption": "工单",
            "viewtype": "DEINDEXPICKUPDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOIndexPickupDataView",
            "viewtag": "6a39140f8397b86d916154a9adafc7ae"
        },
        "emapplygridview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyGridView",
            "viewtag": "6ac8d5af7eb6ce3329e63fb26aa92235"
        },
        "emstockbystorepartgridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByStorePartGridView",
            "viewtag": "6b31663e99ae7db0b815f5086179c629"
        },
        "emwocalendarview": {
            "title": "工单日历视图",
            "caption": "工单",
            "viewtype": "DECALENDARVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOCalendarView",
            "viewtag": "6b6710a4a2c4e3f7d8970215f5bbaa25"
        },
        "emeqahgridview": {
            "title": "活动历史表格视图",
            "caption": "活动历史",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHGridView",
            "viewtag": "6b8d56d2a85071b7841d50dde9a4e378"
        },
        "emwo_entabexpview": {
            "title": "能耗登记工单分页导航视图",
            "caption": "能耗登记工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENTabExpView",
            "viewtag": "6b9aae29e05af29560ad6b2a11b8be41"
        },
        "pfunitpickupview": {
            "title": "计量单位数据选择视图",
            "caption": "计量单位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFUNITPickupView",
            "viewtag": "6c210a328b465189e1ce8213e64f29d8"
        },
        "emdrwgmapequipgridview9": {
            "title": "文档引用表格视图",
            "caption": "文档引用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGMapEquipGridView9",
            "viewtag": "6c23a5825d86b417480d89e075d6fc4b"
        },
        "emstoreeditview": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREEditView",
            "viewtag": "6cb619376f72dccb8c09b0f68daaa909"
        },
        "emrfodeeditview9_edit": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEEditView9_Edit",
            "viewtag": "6cba6d5bce9ca50f6390d650bb909eed"
        },
        "empotabexpview": {
            "title": "订单",
            "caption": "订单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOTabExpView",
            "viewtag": "6dbfcb414a6ef9afdad952637a07efa3"
        },
        "emrfodedashboardview9": {
            "title": "现象数据看板视图",
            "caption": "现象",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEDashboardView9",
            "viewtag": "6e90cd45fbe08d067185bfdb1499e55d"
        },
        "pfteampickupgridview": {
            "title": "班组选择表格视图",
            "caption": "班组",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFTeamPickupGridView",
            "viewtag": "6e9902f95a9cbb189e5da96753da45da"
        },
        "emberthpickupview": {
            "title": "泊位数据选择视图",
            "caption": "泊位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBERTHPickupView",
            "viewtag": "6eb4f765c149eee0c495a2edaa2983cf"
        },
        "emwo_enwovieweditview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENWOViewEditView",
            "viewtag": "6f5e3747bc551b4bbfe3adaf13dc7039"
        },
        "emeqmppickupgridview": {
            "title": "设备仪表选择表格视图",
            "caption": "设备仪表",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPPickupGridView",
            "viewtag": "6f7d72913ed0c1d4ed171df2f23abfec"
        },
        "emitempusedraftgridview": {
            "title": "领料单",
            "caption": "领料单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseDraftGridView",
            "viewtag": "6fa000420be6442b1310bc20d400fda7"
        },
        "emservicedashboardview": {
            "title": "综合评估",
            "caption": "综合评估",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceDashboardView",
            "viewtag": "704169bee36db179560fd8d3e9a0c9cf"
        },
        "emenconsumeditview9_editmode": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMEditView9_EditMode",
            "viewtag": "70422ddbe4aa1bb51c8426db89b3be7e"
        },
        "pfemptreeexpview": {
            "title": "职员树导航视图",
            "caption": "职员",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpTreeExpView",
            "viewtag": "70b7c15547a211e55b0af9a2d43e6d25"
        },
        "emserviceevleditview9_editmode": {
            "title": "服务商评估信息",
            "caption": "服务商评估信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlEditView9_EditMode",
            "viewtag": "70f71409257325745ff2e86c93876df7"
        },
        "emitemcstoconfirmgridview": {
            "title": "调整单",
            "caption": "调整单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSToConfirmGridView",
            "viewtag": "7116e1ff497ab114f81fd13d22c7854f"
        },
        "emeneditview9": {
            "title": "能源信息",
            "caption": "能源信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMENEditView9",
            "viewtag": "71b6cc819239143a110e13eb520d3626"
        },
        "pfuniteditview": {
            "title": "计量单位",
            "caption": "计量单位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFUNITEditView",
            "viewtag": "728d8cd0b4fabe71b80be85958e281d8"
        },
        "emstockbystoregridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByStoreGridView",
            "viewtag": "734bb76bdd8c43d4fc6670734724d855"
        },
        "emeqlocationeditview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLocationEditView",
            "viewtag": "73ef3caef86a2fd0cfd7454e4dd4d8cf"
        },
        "emwo_innereditview_editmode": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNEREditView_EditMode",
            "viewtag": "7499f8fe484f4182cff42890c9cd0c58"
        },
        "planscheduleindexpickupview": {
            "title": "计划时刻设置数据选择视图",
            "caption": "计划时刻设置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "PLANSCHEDULEIndexPickupView",
            "viewtag": "75a56333f5772389e508a3babe9166b9"
        },
        "emitemrouttabexpview": {
            "title": "退货单分页导航视图",
            "caption": "退货单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutTabExpView",
            "viewtag": "7606936e6f4b9940c035f8f665512aaf"
        },
        "emwplisteditview9_editmode": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListEditView9_EditMode",
            "viewtag": "762134bd0c3144814da5afa9ace061b3"
        },
        "pfcontractpickupview": {
            "title": "合同数据选择视图",
            "caption": "合同",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTPickupView",
            "viewtag": "774381e4b650bcdfdce939ad318f0fed"
        },
        "emservicepickupview": {
            "title": "服务商数据选择视图",
            "caption": "服务商",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSERVICEPickupView",
            "viewtag": "7768e48f7cc440f8ef552f938853d026"
        },
        "emequipoptionview": {
            "title": "设备档案选项操作视图",
            "caption": "设备档案",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipOptionView",
            "viewtag": "77c55c12b5d23974a9f44f901244c805"
        },
        "emstockbycabgridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByCabGridView",
            "viewtag": "77ce36fd5f5b9afeb6335280cf93b025"
        },
        "emeqtypegridview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeGridView",
            "viewtag": "784c685c2487042fd1b7fa55bffb73bb"
        },
        "emassetclassgridview": {
            "title": "资产类别",
            "caption": "资产类别",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSGridView",
            "viewtag": "7854d9a7509d47aaf971c8b6dd2769c3"
        },
        "emserviceevleditview9": {
            "title": "服务商评估信息",
            "caption": "服务商评估信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlEditView9",
            "viewtag": "796bf3a44a5f4d9d27995d2ebb5f482a"
        },
        "planscheduleindexpickupdataview": {
            "title": "计划时刻设置索引关系选择数据视图",
            "caption": "计划时刻设置",
            "viewtype": "DEINDEXPICKUPDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "PLANSCHEDULEIndexPickupDataView",
            "viewtag": "797682a7f879312b3c1171981d68bdfe"
        },
        "planschedule_deditview": {
            "title": "计划_按天编辑视图",
            "caption": "计划_按天",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "PLANSCHEDULE_DEditView",
            "viewtag": "79a4d940509b65bcca133b7db99dcbd8"
        },
        "emeqlctgsseditview": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTGSSEditView",
            "viewtag": "7a94d7f0edaeee15127dc6e681a6de4f"
        },
        "emitempuseeditview": {
            "title": "领料单编辑视图",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPUSEEditView",
            "viewtag": "7a9bcdffd947fe8b8b9b88d63191291b"
        },
        "emrfocapickupgridview": {
            "title": "原因选择表格视图",
            "caption": "原因",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOCAPickupGridView",
            "viewtag": "7b1b112bf9e107a231ab748f634a2a23"
        },
        "emitempusewaitissueeditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseWaitIssueEditView9",
            "viewtag": "7b484d970b09deea053cdda96e234500"
        },
        "emrfocaeditview": {
            "title": "原因信息",
            "caption": "原因信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOCAEditView",
            "viewtag": "7d70944c35d05b73a64d96692d9b7a11"
        },
        "emplantabexpview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanTabExpView",
            "viewtag": "7f058dd8ad37ce1cb57ea0c71b3c6fcf"
        },
        "emrfoacpickupgridview": {
            "title": "方案选择表格视图",
            "caption": "方案",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOACPickupGridView",
            "viewtag": "7f8f5353cccd960a2fee86637c57e140"
        },
        "pfempeditview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEMPEditView",
            "viewtag": "7f9f1447aa8a509446c1214c3bd5a6a8"
        },
        "emeqspareeditview_editmode": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareEditView_EditMode",
            "viewtag": "7fbab3035ca47489c70f5aaa0321b7ca"
        },
        "emwo_dpconfirmedgridview": {
            "title": "点检工单表格视图",
            "caption": "点检工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPConfirmedGridView",
            "viewtag": "7fc00ff89edba42225b1e6f333b0fe04"
        },
        "emenconsumpickupview": {
            "title": "能耗数据选择视图",
            "caption": "能耗",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMPickupView",
            "viewtag": "80a7a43341181c0103abee898a46ed53"
        },
        "emapplyconfirmedgridview": {
            "title": "外委申请表格视图",
            "caption": "外委申请-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyConfirmedGridView",
            "viewtag": "811c144f0c1727cfd628d7c676b09ff7"
        },
        "empodetailpickupview": {
            "title": "订单条目数据选择视图",
            "caption": "订单条目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILPickupView",
            "viewtag": "820753f3aeea3c78cb214230a40e7aa8"
        },
        "emeqsparedetailgridview9": {
            "title": "备件物品",
            "caption": "备件物品",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareDetailGridView9",
            "viewtag": "84924f7fb797afe555106c38d97de991"
        },
        "emplancdtgridview": {
            "title": "计划条件",
            "caption": "计划条件",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANCDTGridView",
            "viewtag": "84add22fa08dcb5195f8009b07dfdecc"
        },
        "emrfocagridview": {
            "title": "原因",
            "caption": "原因",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOCAGridView",
            "viewtag": "84c04af308d4fa82ac8465d31b236c8e"
        },
        "emeqlctfdjeditview": {
            "title": "发动机位置",
            "caption": "发动机位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTFDJEditView",
            "viewtag": "84d680d78fa5968f5a1aabd0570c554f"
        },
        "emstorepartoptionview": {
            "title": "仓库库位选项操作视图",
            "caption": "仓库库位",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStorePartOptionView",
            "viewtag": "8508295fc95181cd16b5d021b3d36e55"
        },
        "emapplyeqgridview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyEqGridView",
            "viewtag": "850d7e62a5d28a15564f3d2b4e382196"
        },
        "emapplyeditview_editmode": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyEditView_EditMode",
            "viewtag": "855aa48e7c35c5070e82166fd59bf9e5"
        },
        "emeqtypepickupview": {
            "title": "设备类型数据选择视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTYPEPickupView",
            "viewtag": "8699750ab7e52363ec30b0e681f58ed0"
        },
        "emwplistwaitcostgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWaitCostGridView",
            "viewtag": "8847df9d2122cfe85cee5918fb0ef262"
        },
        "emoutputrcteditview": {
            "title": "产能信息",
            "caption": "产能信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTRCTEditView",
            "viewtag": "885c76f79072e981b9e9611d023e93c3"
        },
        "emeqtypeoptionview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeOptionView",
            "viewtag": "88cc1e6805809822fffbea4083edfc9e"
        },
        "emitemprtntoconfirmgridview": {
            "title": "还料单",
            "caption": "还料单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnToConfirmGridView",
            "viewtag": "88e8ef8b355dbcc3a9c8f705216435e5"
        },
        "emequipgridview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEquipGridView",
            "viewtag": "89830082f3410da731c0e787fbf4648a"
        },
        "emserviceevldraftgridview": {
            "title": "服务商评估",
            "caption": "服务商评估-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlDraftGridView",
            "viewtag": "898ca747d145378ce64f774ddf46bd6b"
        },
        "emeqmaintancecaleditview9": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQMaintanceCalEditView9",
            "viewtag": "89b981714f5148355b795554ae011609"
        },
        "emserviceevltabexpview": {
            "title": "服务商评估分页导航视图",
            "caption": "服务商评估",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlTabExpView",
            "viewtag": "89ec2c929b3fbef555dcc5d828f38cdd"
        },
        "empodetaileditview": {
            "title": "订单条目编辑视图",
            "caption": "订单条目",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILEditView",
            "viewtag": "8a2a4d020f9bd33e9e1cea010aaff41d"
        },
        "emoutputrcteditview9": {
            "title": "产能信息",
            "caption": "产能信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTRCTEditView9",
            "viewtag": "8a3d1375852125c144f8ab45598d9884"
        },
        "emwooripickupgridview": {
            "title": "工单来源选择表格视图",
            "caption": "工单来源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOORIPickupGridView",
            "viewtag": "8ab3aace431552e8ad15f24c6cf0c198"
        },
        "emitempldraftgridview": {
            "title": "损溢单",
            "caption": "损溢单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLDraftGridView",
            "viewtag": "8b4d2c7b3ec2c10e515c40768d6f939b"
        },
        "emwplistpickupview": {
            "title": "采购申请数据选择视图",
            "caption": "采购申请",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTPickupView",
            "viewtag": "8b71081af178f5d3d04e4400acf700b5"
        },
        "emitemcseditview": {
            "title": "库间调整单编辑视图",
            "caption": "库间调整单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMCSEditView",
            "viewtag": "8bebbeff9bcc047392dd84a201ec8805"
        },
        "emitemtypeeditview": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMTYPEEditView",
            "viewtag": "8c4fa22482db266069dbfbf1aece48eb"
        },
        "emwplistcancelgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCancelGridView",
            "viewtag": "8c8fc4a487423fb908308cbbd7b220e4"
        },
        "empurplanpickupgridview": {
            "title": "计划修理选择表格视图",
            "caption": "计划修理",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPURPLANPickupGridView",
            "viewtag": "8cc8bb0e1da7824a272b13bad913dadd"
        },
        "emplantemplpickupview": {
            "title": "计划模板数据选择视图",
            "caption": "计划模板",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLPickupView",
            "viewtag": "8e8a9b547590ca9d94a65e7373bef39a"
        },
        "emitemtypeitemtreeexpview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeItemTreeExpView",
            "viewtag": "8f76e5a86d3c0f946b27ac79f2cc4153"
        },
        "emplanequipgridview9": {
            "title": "计划表格视图",
            "caption": "计划",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPlanEquipGridView9",
            "viewtag": "8fc5fb61719a9eafbf7d64f92cb65d26"
        },
        "emitempusepickupgridview": {
            "title": "领料单选择表格视图",
            "caption": "领料单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEPickupGridView",
            "viewtag": "90071f0679e8fdd324cea581331ceeb9"
        },
        "emitemrouttoconfirmgridview": {
            "title": "退货单",
            "caption": "退货单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutToConfirmGridView",
            "viewtag": "90d927400de6cba1e1dff1386adf8f6c"
        },
        "emwo_osceditview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCEditView",
            "viewtag": "9157061505c8c6f5cbe2cdee2cc4726b"
        },
        "emoutputeditview": {
            "title": "能力信息",
            "caption": "能力信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTEditView",
            "viewtag": "918d65d66e024998c81bbdb6a0a61913"
        },
        "emworedirectview": {
            "title": "工单数据重定向视图",
            "caption": "工单",
            "viewtype": "DEREDIRECTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWORedirectView",
            "viewtag": "932c814dcb2d66244513cc5ceba69021"
        },
        "emeqlctgsspickupview": {
            "title": "钢丝绳位置数据选择视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSPickupView",
            "viewtag": "9341ba20f39986f10fe4715ca70fbfcd"
        },
        "empodetailcloseddetailgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailClosedDetailGridView",
            "viewtag": "9355b5be05b432e04a4e087aa6f6b426"
        },
        "emitemtypeeditview_itemtype": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_itemtype",
            "viewtag": "93ee67e91be4f661f92860285dfbbece"
        },
        "emeqahtreeexpview": {
            "title": "活动历史树导航视图",
            "caption": "活动历史",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHTreeExpView",
            "viewtag": "93f4c5800790b67affe864db5e0d6e6e"
        },
        "sequenceeditview": {
            "title": "序列号编辑视图",
            "caption": "序列号",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "SEQUENCEEditView",
            "viewtag": "94a1f65ab75bcfb300e74e7a4b568c42"
        },
        "emservicegridview": {
            "title": "服务商表格视图",
            "caption": "服务商",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceGridView",
            "viewtag": "95e5d8e9491ac1c704391edba3df8dac"
        },
        "emeqkpgridview": {
            "title": "设备关键点表格视图",
            "caption": "设备关键点",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPGridView",
            "viewtag": "95f6cc645fb95b3a7d407fd47a481df5"
        },
        "emdrwggridview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGGridView",
            "viewtag": "95fa93ae2764709bf48ca56295795929"
        },
        "emwocalendarexpview": {
            "title": "工单日历导航视图",
            "caption": "工单",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOCalendarExpView",
            "viewtag": "964d77f0f1182905a2cd5eb622cfe18a"
        },
        "emeqcheckeditview9_editmode": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQCHECKEditView9_EditMode",
            "viewtag": "9753ceb117067ddcdbb28b5028c45368"
        },
        "emwplistcosteditview9_editmode": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListCostEditView9_EditMode",
            "viewtag": "97764057e5506bfc1e5774cdce25ffea"
        },
        "emeqkprcdkprcdeditview": {
            "title": "关键点记录编辑视图",
            "caption": "关键点记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPRCDKPRcdEditView",
            "viewtag": "97ad09e39eb9982850befaafcd6f978d"
        },
        "pfcontractpickupgridview": {
            "title": "合同选择表格视图",
            "caption": "合同",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTPickupGridView",
            "viewtag": "98fc6acb2c4c6be29d2ce47dd0fccc5a"
        },
        "emenpickupview": {
            "title": "能源数据选择视图",
            "caption": "能源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENPickupView",
            "viewtag": "98fd7570fa05a5721e90f8c2bc734e8b"
        },
        "emwplistcostgridview": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListCostGridView",
            "viewtag": "997ecabc7fe92a5ea975f669e5d2a0c2"
        },
        "emwo_innerdraftgridview": {
            "title": "内部工单表格视图",
            "caption": "内部工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERDraftGridView",
            "viewtag": "99909c01d1ba483c18800d12451ceeb6"
        },
        "emenconsumpickupgridview": {
            "title": "能耗选择表格视图",
            "caption": "能耗",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMPickupGridView",
            "viewtag": "9b213ebe80d8a53a6756f9d306b8c596"
        },
        "emitemprtntabexpview": {
            "title": "还料单分页导航视图",
            "caption": "还料单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnTabExpView",
            "viewtag": "9c2dea5842bc2d89286597a1aec45cb5"
        },
        "emwo_oscdraftgridview": {
            "title": "外委保养工单表格视图",
            "caption": "外委工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCDraftGridView",
            "viewtag": "9c31598ff68272284b386bf11bbabf00"
        },
        "emacclasspickupgridview": {
            "title": "总帐科目选择表格视图",
            "caption": "总帐科目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMACCLASSPickupGridView",
            "viewtag": "9caa8a81f904195f8c89d52564766774"
        },
        "emeqtypetreeexpview2": {
            "title": "设备类型",
            "caption": "设备档案",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeTreeExpView2",
            "viewtag": "9d0dcd73a045726254641ee42544e53d"
        },
        "emequipeditview2": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW2",
            "viewmodule": "eam_core",
            "viewname": "EMEquipEditView2",
            "viewtag": "9d546ecd716089e12f2c2602f5de0123"
        },
        "emitemcsconfirmedgridview": {
            "title": "调整单",
            "caption": "调整单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSConfirmedGridView",
            "viewtag": "9e1a8351284e31776940c41cc556334e"
        },
        "emitemtypeeditview_editmode": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_EditMode",
            "viewtag": "9e4ec3e0ede3f5634c7540f2c21673a6"
        },
        "emequippickupview": {
            "title": "设备档案数据选择视图",
            "caption": "设备档案",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQUIPPickupView",
            "viewtag": "9f1808ad0af0680daf887d4050376b41"
        },
        "emeqmpmtrgridview": {
            "title": "设备仪表读数表格视图",
            "caption": "设备仪表读数",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMTRGridView",
            "viewtag": "9f559f8a15fa5cac3d9ce75e64664b26"
        },
        "emwo_dpgridview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPGridView",
            "viewtag": "9f9732def4c66d602c1c3e7d4576385a"
        },
        "emitemcsdraftgridview": {
            "title": "调整单",
            "caption": "调整单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSDraftGridView",
            "viewtag": "a00e9fdfb01be26ee42dd11724030da6"
        },
        "emdrwgeditview_editmode": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGEditView_EditMode",
            "viewtag": "a043d07fdf452d7a89efe7e3b1b86795"
        },
        "emeqtypetreeexpview": {
            "title": "设备类型",
            "caption": "设备档案",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeTreeExpView",
            "viewtag": "a05c3a903b6c4c1657e1a2b01f5cb97b"
        },
        "emplanrecordeditview": {
            "title": "触发记录编辑视图",
            "caption": "触发记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANRECORDEditView",
            "viewtag": "a2a5c5e2656e1e30d5ec261f0c5ee32a"
        },
        "emstockpickupgridview": {
            "title": "库存选择表格视图",
            "caption": "库存",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockPickupGridView",
            "viewtag": "a2d3b73797351b53b3c6c18c5521e588"
        },
        "emrfomopickupview": {
            "title": "模式数据选择视图",
            "caption": "模式",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOMOPickupView",
            "viewtag": "a45ddcd2be606b6ee6ba88b2f0e40374"
        },
        "emitemcsgridview": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemCSGridView",
            "viewtag": "a4b03b50ee6214ced931a9a1139674f7"
        },
        "emeqkeepcaleditview9": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQKeepCalEditView9",
            "viewtag": "a4b3d64c7172ef4fb22a903076faa87d"
        },
        "emeqlcttireseditview": {
            "title": "轮胎位置",
            "caption": "轮胎位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTTIRESEditView",
            "viewtag": "a59db44f90b2dc4168f2d6a15158dbb4"
        },
        "pfdepteditview": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFDEPTEditView",
            "viewtag": "a5d46f833cb95fbfaa23212d89364925"
        },
        "emitemoptionview": {
            "title": "物品选项操作视图",
            "caption": "物品",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemOptionView",
            "viewtag": "a6c1475b17aa27870e7814ec31e1c9d3"
        },
        "emitemrinpickupgridview": {
            "title": "入库单选择表格视图",
            "caption": "入库单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINPickupGridView",
            "viewtag": "a797e59cfe10b1bb05a5e71d7ce45aca"
        },
        "emstoreparteditview_7215": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStorePartEditView_7215",
            "viewtag": "a7b26679d68f7d3407173c0934ea84bc"
        },
        "emitempuseissuedgridview": {
            "title": "领料单",
            "caption": "领料单-已发料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseIssuedGridView",
            "viewtag": "a7f37f3404780842d87c9256f89cd06b"
        },
        "emservicefinanceview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>财务信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceFinanceView9",
            "viewtag": "a836e0ca7157cf025430ffc6b9d17072"
        },
        "empodetaileditview9": {
            "title": "采购订单条目",
            "caption": "采购订单条目",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILEditView9",
            "viewtag": "a92e856807e7904251f5e6e500ae588c"
        },
        "emstorepickupview": {
            "title": "仓库数据选择视图",
            "caption": "仓库",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPickupView",
            "viewtag": "a93af11767f301deffbd9e48c085bf1b"
        },
        "emitempleditview9_new": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLEditView9_New",
            "viewtag": "a97e151984eed94e5a7ae659322b7900"
        },
        "emeqlcttiresgridview": {
            "title": "轮胎位置",
            "caption": "轮胎位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTTIRESGridView",
            "viewtag": "aa418c0dcfb1d823e87a66da6b08285e"
        },
        "emmachinecategorypickupview": {
            "title": "机种编号数据选择视图",
            "caption": "机种编号",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHINECATEGORYPickupView",
            "viewtag": "aaaed92d2daf01a3dde53b4d0ac8784c"
        },
        "emeqtypeeditview_editmode": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeEditView_EditMode",
            "viewtag": "aaf736274867f83f509b019c53bdfca9"
        },
        "pfemppickupview": {
            "title": "职员数据选择视图",
            "caption": "职员",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEMPPickupView",
            "viewtag": "ab4f475b20564461a2834dc778f7db5e"
        },
        "emequipruninfo": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipRunInfo",
            "viewtag": "ab92b57ce717b9202dd399bc3e96888d"
        },
        "empodetailwaitcheckgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailWaitCheckGridView",
            "viewtag": "abcb2710be0edd6afe8fcf0667aaf6b8"
        },
        "emeqsparedetailgridview": {
            "title": "备件物品",
            "caption": "备件物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILGridView",
            "viewtag": "ac2ec6e6d320e1912ce92d764ca1306d"
        },
        "emitemrinwaitingridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemRInWaitInGridView",
            "viewtag": "ada1cce37215f3fcdf335f5a72192564"
        },
        "planschedule_weditview": {
            "title": "计划_按周编辑视图",
            "caption": "计划_按周",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "PLANSCHEDULE_WEditView",
            "viewtag": "adb2843c7feb6cd25089334acffd61c9"
        },
        "emeqlctmapeditview": {
            "title": "位置关系编辑视图",
            "caption": "位置关系",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTMapEditView",
            "viewtag": "adc4bd961edb468b6ab70b433f9ae735"
        },
        "emeqtypepickuptreeview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEPICKUPTREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypePickupTreeView",
            "viewtag": "ae0cbfb0fc5567784f5d61e5819b5553"
        },
        "emitemrouteditview9_editmode": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemROutEditView9_EditMode",
            "viewtag": "ae364d55428ad46f97db9670dc07d88b"
        },
        "emrfodepickupview": {
            "title": "现象数据选择视图",
            "caption": "现象",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEPickupView",
            "viewtag": "ae4d57ca65ee6e0dc185ae92383936c0"
        },
        "emitempuseeditview9_new": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseEditView9_New",
            "viewtag": "ae82f2b8ce2e7f577ca7007d7cc5e436"
        },
        "emitemrouteditview9_new": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutEditView9_New",
            "viewtag": "af2947480712ae723009a44b196d8ef7"
        },
        "emassetclasseditview": {
            "title": "ASSET资产类别信息",
            "caption": "ASSET资产类别信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSEditView",
            "viewtag": "af7d2de4c57a8912a459d4574fa48d9b"
        },
        "emproducteditview9": {
            "title": "试用品信息",
            "caption": "试用品信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPRODUCTEditView9",
            "viewtag": "afaeddca7e510be3e3d487eedd81aa4d"
        },
        "emitemstoreinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemStoreInfoView",
            "viewtag": "b01cd4a6b77db5f9f9a6435b4c1f8299"
        },
        "emplangridview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanGridView",
            "viewtag": "b0cdcd21454667bc48196e43dcea0042"
        },
        "emitemrinpickupview": {
            "title": "入库单数据选择视图",
            "caption": "入库单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINPickupView",
            "viewtag": "b0ff136b90c2901235ac196d3a053549"
        },
        "emenconsumgridview": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENConsumGridView",
            "viewtag": "b19c6d4c64f60d21f2087da573276c64"
        },
        "emdrwgtreeexpview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGTreeExpView",
            "viewtag": "b1fb9893ca839830dd6e3023769081d8"
        },
        "emitemdashboardview": {
            "title": "物品数据看板视图",
            "caption": "物品",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemDashboardView",
            "viewtag": "b2b7c2a794daf3e698ce13dfda1c48f1"
        },
        "emapplydraftgridview": {
            "title": "外委申请表格视图",
            "caption": "外委申请-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyDraftGridView",
            "viewtag": "b2e361cc3e03b89953d54e20d9f868db"
        },
        "emitemprtngridview": {
            "title": "还料单",
            "caption": "还料单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPRtnGridView",
            "viewtag": "b2ea4f0fcb22c7fee66340f6c991d946"
        },
        "emplancdteditview": {
            "title": "计划条件",
            "caption": "计划条件",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANCDTEditView",
            "viewtag": "b39fc5c6e7ece40a427f0eb097a4d985"
        },
        "emitempltabexpview": {
            "title": "损溢单分页导航视图",
            "caption": "损溢单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLTabExpView",
            "viewtag": "b44baae58651b7f17025dae86d29e8da"
        },
        "emwopickupview": {
            "title": "工单数据选择视图",
            "caption": "工单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOPickupView",
            "viewtag": "b48f506479fd66761ee52b3f531e1f68"
        },
        "pfcontractgridview": {
            "title": "合同",
            "caption": "合同",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "PFCONTRACTGridView",
            "viewtag": "b4908382917ec2721c640f7052e0c674"
        },
        "empoeditview9": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPOEditView9",
            "viewtag": "b55c3214a958e201031d16d01f043900"
        },
        "emeqtypetreepickupview": {
            "title": "设备类型数据选择视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeTreePickupView",
            "viewtag": "b58c6ec080801f0f79cc734dd405c6bc"
        },
        "emeqdebugeditview": {
            "title": "事故记录编辑视图",
            "caption": "事故记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQDEBUGEditView",
            "viewtag": "b64fde81d6050c1c6970f6f45abf5e4c"
        },
        "empoonordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOOnOrderGridView",
            "viewtag": "b800db560cccbcb6ffa47dbe080f303b"
        },
        "emeqsparemapeditview": {
            "title": "备件包引用",
            "caption": "备件包引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREMAPEditView",
            "viewtag": "b872a2756b8f76de6e3952d936540661"
        },
        "emeqdebuggridview": {
            "title": "事故记录",
            "caption": "事故记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQDebugGridView",
            "viewtag": "b93057b3a295c324b6fe6863261ddaa9"
        },
        "emitemsupplyinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemSupplyInfoView",
            "viewtag": "b9ecf74cbda46c14b32c44275e551088"
        },
        "emserviceevleditview": {
            "title": "服务商评估编辑视图",
            "caption": "服务商评估",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlEditView",
            "viewtag": "ba03c7160de1b430812272ff2aa94ea5"
        },
        "emberthpickupgridview": {
            "title": "泊位选择表格视图",
            "caption": "泊位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBERTHPickupGridView",
            "viewtag": "ba16ccec3253b3118e6bcdd92b6a5f51"
        },
        "emitemallgridview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemAllGridView",
            "viewtag": "bac65300a250b57297d0ad4fad36f35b"
        },
        "emenconsumeditview9": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMENCONSUMEditView9",
            "viewtag": "bb3bc0bd2796c3a6ae4d4f9a366a8249"
        },
        "emitemtradetreeexpview": {
            "title": "物品交易树导航视图",
            "caption": "物品交易",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTradeTreeExpView",
            "viewtag": "bca931f90bf97b982d3935d161d4deb9"
        },
        "emwo_osceditview_editmode": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCEditView_EditMode",
            "viewtag": "bcd52da6e9a6f6a8926bc580ab14de07"
        },
        "empoclosedordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOClosedOrderGridView",
            "viewtag": "bcd5fc9a8d72728b8e2622dd103ad913"
        },
        "emitemprtndraftgridview": {
            "title": "还料单",
            "caption": "还料单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnDraftGridView",
            "viewtag": "bce45ed958634061726cab545bddb0ce"
        },
        "equipportalview": {
            "title": "设备模块看板视图",
            "caption": "",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EquipPortalView",
            "viewtag": "bd14f861d017110ea6898bf4e44b46cb"
        },
        "emproducteditview9_editmode": {
            "title": "试用品信息",
            "caption": "试用品信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPRODUCTEditView9_EditMode",
            "viewtag": "bd6e771add09e4cb9e4cd706831a6975"
        },
        "pfteampickupview": {
            "title": "班组数据选择视图",
            "caption": "班组",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFTEAMPickupView",
            "viewtag": "bef3ee446a9a19347eb0cb1e57878741"
        },
        "emitemtypeoptionview": {
            "title": "物品类型选项操作视图",
            "caption": "物品类型",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeOptionView",
            "viewtag": "befef0f65ec69377d56e0d2bb11e1cb5"
        },
        "emitempleditview9": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLEditView9",
            "viewtag": "bf23ad24bfa1863e039772a1f1c00bb5"
        },
        "emwplistcostconfirmgridview9": {
            "title": "询价单表格视图",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListCostConfirmGridView9",
            "viewtag": "bf74f139a973966228381a353b8238f0"
        },
        "empopickupgridview": {
            "title": "订单选择表格视图",
            "caption": "订单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPickupGridView",
            "viewtag": "bfef160d5a90d65b6d6b1cf7b5c04dfa"
        },
        "emassetcleareditview": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEAREditView",
            "viewtag": "c04fd8c9c27dd0b85e796601359fae58"
        },
        "emasseteditview9_editmode": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_as",
            "viewname": "EMASSETEditView9_EditMode",
            "viewtag": "c0ea0882b97aeba72dd56b5314a4c191"
        },
        "emserviceeditview": {
            "title": "服务商编辑视图",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMSERVICEEditView",
            "viewtag": "c125be1b61d36b5d49e1263a5992117b"
        },
        "emplaneditview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanEditView",
            "viewtag": "c17d90d625c98847c2bb51cfed7fb546"
        },
        "emstoreeditview_editmode": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreEditView_EditMode",
            "viewtag": "c27bfe990d149afe2ec27543630b7990"
        },
        "pfdeptgridview": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFDeptGridView",
            "viewtag": "c3dc47de07e89c6ce14ee04951c5d91e"
        },
        "emitemrineditview9": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINEditView9",
            "viewtag": "c4534d77bfe66462a22661acb1783456"
        },
        "emeqlctgssgridview_cqyj": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_yj",
            "viewname": "EMEQLCTGSSGridView_CQYJ",
            "viewtag": "c505271bd9640daa2ea25255b24ca8b6"
        },
        "emassetclasseditview_4778": {
            "title": "ASSET资产类别信息",
            "caption": "ASSET资产类别信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSEditView_4778",
            "viewtag": "c5225b0c008cfb05c29468662954e875"
        },
        "emeqlocationeditview9": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONEditView9",
            "viewtag": "c6098f1792e5751f61e4eddade272fe9"
        },
        "emplanpersoninfo": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanPersonInfo",
            "viewtag": "c61d794032d7d0cfedb229837fb09b16"
        },
        "emeqmpmtrmpmtreditview": {
            "title": "设备仪表读数编辑视图",
            "caption": "设备仪表读数",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMTRMpMtrEditView",
            "viewtag": "c644e089610d802b6fd6043e9c3e1d15"
        },
        "emeqmpmpeditview": {
            "title": "设备仪表编辑视图",
            "caption": "设备仪表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMPEditView",
            "viewtag": "c68659be1beaf69a172492df81d46340"
        },
        "emserviceeditview9_editmode": {
            "title": "PUR服务商信息",
            "caption": "PUR服务商信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEditView9_EditMode",
            "viewtag": "c758a28b4e386b736da420a5f66af404"
        },
        "emeqmaintanceeditview9": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQMAINTANCEEditView9",
            "viewtag": "c82c51213ba9d242dcf21890a74f5bcf"
        },
        "emitempickupgridview": {
            "title": "物品选择表格视图",
            "caption": "物品",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPickupGridView",
            "viewtag": "c85bed1ffbfeeccbdf2d115ca32aafe0"
        },
        "empoplaceordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPlaceOrderGridView",
            "viewtag": "c8ac5a253a281a643de4db468b5cac85"
        },
        "emitemringridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemRInGridView",
            "viewtag": "c8bffb1278eddb48efc2625865b307f9"
        },
        "emwo_dptoconfirmgridview": {
            "title": "点检工单表格视图",
            "caption": "点检工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPToConfirmGridView",
            "viewtag": "c916a65310937452bd38d3cfb4f0bb8e"
        },
        "emplanpickupgridview": {
            "title": "计划选择表格视图",
            "caption": "计划",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANPickupGridView",
            "viewtag": "c98f0a1b66adbd986643d7d4d78b0232"
        },
        "emeqsetuppickupgridview": {
            "title": "更换安装选择表格视图",
            "caption": "更换安装",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPPickupGridView",
            "viewtag": "cabff129e300b0aacabf8563886a6f95"
        },
        "emitempuseeditview9_editmode": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseEditView9_EditMode",
            "viewtag": "caeb019319cbd24bdee9a439f1876835"
        },
        "emeitirespickupgridview": {
            "title": "轮胎清单选择表格视图",
            "caption": "轮胎清单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEITIRESPickupGridView",
            "viewtag": "cb5018876b531cb8fe5414a428ae07d7"
        },
        "emwotreeexpview": {
            "title": "工单树导航视图",
            "caption": "工单",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOTreeExpView",
            "viewtag": "cb5cf460ddd14ba04b8844c6ac0301ca"
        },
        "emeqsparelistexpview": {
            "title": "备件包列表导航视图",
            "caption": "备件包",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareListExpView",
            "viewtag": "cb7f7f19232a2b0abf09d222c94261c9"
        },
        "empodetailgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILGridView",
            "viewtag": "cbb15afbdfeb9c6267dbdd2ee4fcb0a8"
        },
        "emserviceconfirmedgridview": {
            "title": "服务商",
            "caption": "服务商审批-已审",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceConfirmedGridView",
            "viewtag": "cbc894e730509e8d7b7110eb62d9ee7a"
        },
        "emplaneditview_editmode": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanEditView_EditMode",
            "viewtag": "cc45a14a2a571f2d5eb916c0595962ed"
        },
        "emassetcleargridview_5564": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMAssetClearGridView_5564",
            "viewtag": "ccc5721160cee3833b89319b9359c426"
        },
        "emeqlocationmaininfo": {
            "title": "位置数据看板视图",
            "caption": "位置",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationMainInfo",
            "viewtag": "cd7a063dbc44490e456d164be357887b"
        },
        "emplanoptionview": {
            "title": "计划选项操作视图",
            "caption": "计划",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANOptionView",
            "viewtag": "cda54777e21b6912299786271dc8b75f"
        },
        "emeqdebugeditview9_editmode": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQDEBUGEditView9_EditMode",
            "viewtag": "cdfd4f75cceaedb5f905374547581eae"
        },
        "pfdepteditview_editmode": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFDeptEditView_EditMode",
            "viewtag": "cf0ed1d76148abb0435c7fa5fd9556b1"
        },
        "emwoemwogridview": {
            "title": "工单表格视图",
            "caption": "工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOEMWOGridView",
            "viewtag": "cfc4a2b8a26eee16318c30843db96764"
        },
        "emenpickupgridview": {
            "title": "能源选择表格视图",
            "caption": "能源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENPickupGridView",
            "viewtag": "cfcb33f4e84ddd4f2e21297d45a0e66c"
        },
        "emeqsparepickupview": {
            "title": "备件包数据选择视图",
            "caption": "备件包",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREPickupView",
            "viewtag": "d0eb65021d65496d58473308e744be7b"
        },
        "emmachinecategorypickupgridview": {
            "title": "机种编号选择表格视图",
            "caption": "机种编号",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHINECATEGORYPickupGridView",
            "viewtag": "d16ca3055fdcbeb1dd03272f872898bf"
        },
        "emeqlocationgridview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLocationGridView",
            "viewtag": "d2da14cc2c2f2f9594a808f65c8609b0"
        },
        "emeqsparedetailtabexpview": {
            "title": "备件包明细分页导航视图",
            "caption": "备件包明细",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareDetailTabExpView",
            "viewtag": "d2e8ecc5e7246bbf89d9ee333483408a"
        },
        "emitemtypetreepickupview": {
            "title": "物品类型数据选择视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeTreePickupView",
            "viewtag": "d3d496ddfdb1d8e5886b1e2629dffc88"
        },
        "emplantemplpickupgridview": {
            "title": "计划模板选择表格视图",
            "caption": "计划模板",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLPickupGridView",
            "viewtag": "d69ba26d56a9ea91392719849724261e"
        },
        "emeqspareeditview": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareEditView",
            "viewtag": "d6c472d7d44a4bd791fd33e86100cb18"
        },
        "emeqsparemapdashboardview9": {
            "title": "备件包引用数据看板视图",
            "caption": "备件包引用",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapDashboardView9",
            "viewtag": "d7034519e878e71c2d13e436006f1af8"
        },
        "empodetailgridview9": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailGridView9",
            "viewtag": "d83f82996b3c3fe88caaa755c51425a0"
        },
        "emeqlctfdjgridview": {
            "title": "发动机位置",
            "caption": "发动机位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTFDJGridView",
            "viewtag": "d84963d959f370f98cfc86031dff753e"
        },
        "emeqsparegridview": {
            "title": "备件包",
            "caption": "备件包",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREGridView",
            "viewtag": "d8d0b0b0463c04d887f65581fea3079f"
        },
        "emeitirespickupview": {
            "title": "轮胎清单数据选择视图",
            "caption": "轮胎清单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEITIRESPickupView",
            "viewtag": "da1ba460f87b2b7aeceb64c67aa370fd"
        },
        "emitemrouteditview9": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutEditView9",
            "viewtag": "daafb3465e23223525f4c4d4da08fae3"
        },
        "emitemtabexpview": {
            "title": "物品分页导航视图",
            "caption": "物品",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTabExpView",
            "viewtag": "db76369286d8c4592646b38515118b7a"
        },
        "emoutputgridview": {
            "title": "能力",
            "caption": "能力",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTGridView",
            "viewtag": "dba2d5c1ab40d93af771607d88bf02d7"
        },
        "planschedule_meditview": {
            "title": "计划_按月编辑视图",
            "caption": "计划_按月",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "PLANSCHEDULE_MEditView",
            "viewtag": "dbf96a21db0ae2ad59f7053cc978d537"
        },
        "emserviceevlgridview9": {
            "title": "服务商评估",
            "caption": "服务商评估",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlGridView9",
            "viewtag": "dc76f83e9d767ba736fd253848e01730"
        },
        "emeqlctgsstreeexpview": {
            "title": "钢丝绳位置树导航",
            "caption": "钢丝绳位置树导航",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSTreeExpView",
            "viewtag": "dcde22194e0b645b20bb462200eab5a8"
        },
        "emeqaheqcalendarexpview": {
            "title": "设备活动日历导航视图",
            "caption": "活动历史",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHEqCalendarExpView",
            "viewtag": "dcfeacf4a24cc28b33edce91dad64d07"
        },
        "emitempleditview9_editmode": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPLEditView9_EditMode",
            "viewtag": "dd6fd047f64a53d578abfd414add9c22"
        },
        "emapplyredirectview": {
            "title": "外委申请数据重定向视图",
            "caption": "外委申请",
            "viewtype": "DEREDIRECTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyRedirectView",
            "viewtag": "de2c33edc1d5094b8668bbfb9858e266"
        },
        "emservicetoconfirmgridview": {
            "title": "服务商",
            "caption": "服务商审批-待审",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceToConfirmGridView",
            "viewtag": "df632c1fa0cab98cc252fe9a9ed6e8e7"
        },
        "emeqkeepeditview9_editmode": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQKEEPEditView9_EditMode",
            "viewtag": "dfa9ac688227ba6d2564d9c344d17c32"
        },
        "emoutputrctgridview": {
            "title": "产能",
            "caption": "产能",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOutputRctGridView",
            "viewtag": "e0c4f65469feff3713fa23d7bf6f0bd5"
        },
        "emeqlocationpickupview": {
            "title": "位置数据选择视图",
            "caption": "位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONPickupView",
            "viewtag": "e2909898adfb9623415ed5e4b6017115"
        },
        "emdrwgoptionview": {
            "title": "文档选项操作视图",
            "caption": "文档",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGOptionView",
            "viewtag": "e3be6a58d9cabd1926cb73883a8bf482"
        },
        "emitempusegridview": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseGridView",
            "viewtag": "e3ea9d4287f91ef15f3491f205f476ac"
        },
        "emequipallgridview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipAllGridView",
            "viewtag": "e42135babd14ea6c3087ca9de2d79f90"
        },
        "emitemroutconfirmedgridview": {
            "title": "退货单",
            "caption": "退货单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutConfirmedGridView",
            "viewtag": "e42dbff9a720506104d420cd3cd0d67f"
        },
        "emeqlctmapgridview9": {
            "title": "位置关系表格视图",
            "caption": "位置关系",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTMapGridView9",
            "viewtag": "e57dcc14e709bf888de92196ccc8a11b"
        },
        "emwo_oscwovieweditview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCWOViewEditView",
            "viewtag": "e58b5c1d5fa915b41c01c1961b69e476"
        },
        "emrfodeeditview9": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEEditView9",
            "viewtag": "e5d5066723553d17862ae4faabf9b991"
        },
        "emwplistwpprocesstreeexpview": {
            "title": "采购申请树导航视图",
            "caption": "采购申请",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWpProcessTreeExpView",
            "viewtag": "e615a3257c6dc7b230cd373fb9039657"
        },
        "emeqsparemapgridview9": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSPAREMAPGridView9",
            "viewtag": "e654b593fddf72a64b374ff937f17d63"
        },
        "pfempgridview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEmpGridView",
            "viewtag": "e8c58b31c30d9279e27709bcb278902b"
        },
        "emeqsparegridexpview": {
            "title": "备件包表格导航视图",
            "caption": "备件包",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareGridExpView",
            "viewtag": "e9ca59b6093c7e0bb596025b1dafb8ab"
        },
        "emstoreeditview_9924": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreEditView_9924",
            "viewtag": "ea862e1cf91c975a230ee463122cf0bb"
        },
        "emeqlocationoptionview": {
            "title": "位置新建",
            "caption": "位置新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationOptionView",
            "viewtag": "eae455de131638d43bb3fa4e3d3e9cf9"
        },
        "emwo_dpdraftgridview": {
            "title": "点检工单表格视图",
            "caption": "点检工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPDraftGridView",
            "viewtag": "eb7035b4eb1b1ffdf8b2592f62bf5d84"
        },
        "emitemrouteditview": {
            "title": "退货单编辑视图",
            "caption": "退货单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMROUTEditView",
            "viewtag": "ebbca2434d7f93a265abe76223f0be66"
        },
        "emobjectpickupview": {
            "title": "对象数据选择视图",
            "caption": "对象",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOBJECTPickupView",
            "viewtag": "ec1dc7662e938aa1087ab591c9e91c0d"
        },
        "emplandetaileditview": {
            "title": "计划步骤",
            "caption": "计划步骤",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANDETAILEditView",
            "viewtag": "ec3414365aa6d8dbf2307cded48232db"
        },
        "pfteameditview": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFTEAMEditView",
            "viewtag": "ecd3905f3ae08142169d7861c44f82ef"
        },
        "emplantemplgridview": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanTemplGridView",
            "viewtag": "ed69711ada89a236e083dbf6e670592a"
        },
        "emstoretreeexpview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStoreTreeExpView",
            "viewtag": "ed77b0d2dfcda3182684e13be3bec9ee"
        },
        "pfcontracteditview9_editmode": {
            "title": "合同信息",
            "caption": "合同信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "PFCONTRACTEditView9_EditMode",
            "viewtag": "ed8fe6b716f26b909943e18d050cd957"
        },
        "emitemplconfirmedgridview": {
            "title": "损溢单",
            "caption": "损溢单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLConfirmedGridView",
            "viewtag": "edf2724c5768435a0a77c6e9ea18f01c"
        },
        "emservicetabexpview": {
            "title": "服务商分页导航视图",
            "caption": "服务商",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceTabExpView",
            "viewtag": "ee32a00c8c3c0f0703294b298c8b273f"
        },
        "emeqsparemapequipgridview9": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapEquipGridView9",
            "viewtag": "eec5e28b764f42664ec510535ec07770"
        },
        "emequiptabexpview": {
            "title": "设备档案分页导航视图",
            "caption": "设备档案",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipTabExpView",
            "viewtag": "ef1522e1c2cbe45896bd18c4f2a6774e"
        },
        "emitemrinputingridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMItemRInPutInGridView",
            "viewtag": "ef25f02ff6b31ae67d9ea36dfb55d632"
        },
        "emitemprtneditview9": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnEditView9",
            "viewtag": "f05aed62de0d686c8c52a8b8bfc5b96a"
        },
        "emeqmpgridview": {
            "title": "设备仪表表格视图",
            "caption": "设备仪表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPGridView",
            "viewtag": "f19dbbe510c90444c2ec99780efcf24e"
        },
        "emwplistingridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListInGridView",
            "viewtag": "f1b97b56a3d9ebecd460a34183655331"
        },
        "emwo_osctoconfirmgridview": {
            "title": "外委保养工单表格视图",
            "caption": "外委工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCToConfirmGridView",
            "viewtag": "f3a4900716e7b35e249eecdf11a64b2a"
        },
        "emstorepartgridview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStorePartGridView",
            "viewtag": "f493994398e4ecbc5974e4e026634da7"
        },
        "pfdeptpickupview": {
            "title": "部门数据选择视图",
            "caption": "部门",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFDEPTPickupView",
            "viewtag": "f4dd610f8a72c9304f48d878d969547b"
        },
        "emitemrineditview9_new": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemRInEditView9_New",
            "viewtag": "f517a47c58bdb4e768eb9020d149e4d3"
        },
        "emitempuseeditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseEditView9",
            "viewtag": "f51b89d6e74d1d393deb1e9b7985082e"
        },
        "emeqwlgridview": {
            "title": "设备运行日志表格视图",
            "caption": "设备运行日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLGridView",
            "viewtag": "f6b13c85a105b9aff4df73d0c81e28de"
        },
        "emapplyeditview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyEditView",
            "viewtag": "f6be77a4355df423a03ecd98de66eec0"
        },
        "emrfodegridexpview": {
            "title": "现象表格导航视图",
            "caption": "现象",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEGridExpView",
            "viewtag": "f7dc92337280d6d9706d117d196eb2be"
        },
        "emequipeditview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQUIPEditView",
            "viewtag": "f7e24c4a7de19c8d74c1cc18bbebe5b4"
        },
        "emeqlocationtreeexpview": {
            "title": "位置树导航视图",
            "caption": "位置",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationTreeExpView",
            "viewtag": "f893e6ceb44d267810e27c0f553eaeb1"
        },
        "emeqlctgsseditview9_editmode": {
            "title": "钢丝绳位置编辑视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_yj",
            "viewname": "EMEQLCTGSSEditView9_EditMode",
            "viewtag": "f9ce353eef595aa88a3abb7017017723"
        },
        "emassetclasspickupview": {
            "title": "资产类别数据选择视图",
            "caption": "资产类别",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSPickupView",
            "viewtag": "f9cf5c209e6d1447bf92d42bf4af4bba"
        },
        "emstoreoptionview": {
            "title": "仓库选项操作视图",
            "caption": "仓库",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStoreOptionView",
            "viewtag": "f9fe7601e306151c2dee19f08e3b878e"
        },
        "emwo_dptabexpview": {
            "title": "点检工单分页导航视图",
            "caption": "点检工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPTabExpView",
            "viewtag": "fa0bfaab32d32b3d1f047f2928561e39"
        },
        "emplanpickupview": {
            "title": "计划数据选择视图",
            "caption": "计划",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANPickupView",
            "viewtag": "faa92111474981684349c243e9823fc7"
        },
        "emstoregridview_7848": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreGridView_7848",
            "viewtag": "fac546b9086999cdcc36f5204393007a"
        },
        "emwplistcostgridview9": {
            "title": "询价单表格视图",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostGridView9",
            "viewtag": "faedcc480066e9aa28183f9b60396982"
        },
        "emrfodetypeeditview": {
            "title": "现象分类",
            "caption": "现象分类",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODETYPEEditView",
            "viewtag": "fb58b86e0fa4171fd062a6cb2ff34bb9"
        },
        "emstockeditview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStockEditView",
            "viewtag": "fbfe650e1269b71dab6462ffe67e57ee"
        },
        "emitemroutgridview": {
            "title": "退货单",
            "caption": "退货单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemROutGridView",
            "viewtag": "fc153775f37ee7a931d74c12bd96845e"
        },
        "emrfodeeditview9_editmode": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEEditView9_EditMode",
            "viewtag": "fc39fd346a1f9ddc6ec297200c57f93f"
        },
        "emeqsetupcaleditview9": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSetupCalEditView9",
            "viewtag": "fc741d32f080c4af11803d2ff3dcc11d"
        },
        "emmachmodelpickupview": {
            "title": "机型数据选择视图",
            "caption": "机型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHMODELPickupView",
            "viewtag": "fd2134118dc7f9c1db7086af8eb794bb"
        },
        "emplaneqinfo": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanEQInfo",
            "viewtag": "fd4d5c9de9db67c48aa65cb3ea5e3ca2"
        },
        "emassetcleargridview": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEARGridView",
            "viewtag": "fd55e5e3b51b3c4b6f06063896c0b5db"
        },
        "emserviceevlgridview": {
            "title": "服务商评估",
            "caption": "服务商评估",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlGridView",
            "viewtag": "fdd707e083da5df48c8a4df45b885d44"
        },
        "emstoregridview": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreGridView",
            "viewtag": "fdf497f9dbb3079b914b7e5999a06d8e"
        },
        "emeqmonitormonitoreditview": {
            "title": "设备状态监控编辑视图",
            "caption": "设备状态监控",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMonitorMonitorEditView",
            "viewtag": "fe6e3234c3d2fa811a35117b31d88a29"
        },
        "planschedule_teditview": {
            "title": "定时编辑视图",
            "caption": "计划_定时",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "PLANSCHEDULE_TEditView",
            "viewtag": "fe84244ce26148ec320ff67eaabebd68"
        },
        "empodetailwaitbookgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailWaitBookGridView",
            "viewtag": "ff028f7b7012ad4d1563e0cfc2b64fd2"
        },
        "emitemcseditview9": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSEditView9",
            "viewtag": "ff3339e0459fb79b19ddfa628b8b9dd0"
        },
        "emacclasspickupview": {
            "title": "总帐科目数据选择视图",
            "caption": "总帐科目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMACCLASSPickupView",
            "viewtag": "ff79be163370f7ee32ebd0e6536689c4"
        },
        "emassetpickupview": {
            "title": "资产数据选择视图",
            "caption": "资产",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETPickupView",
            "viewtag": "ff7b91fcf517cd05e71d65349cbf042e"
        }
    }];
});

// 获取视图消息分组信息
mock.onGet('./assets/json/view-message-group.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
    }];
});