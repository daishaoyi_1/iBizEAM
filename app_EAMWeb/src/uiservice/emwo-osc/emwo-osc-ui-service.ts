import EMWO_OSCUIServiceBase from './emwo-osc-ui-service-base';

/**
 * 外委保养工单UI服务对象
 *
 * @export
 * @class EMWO_OSCUIService
 */
export default class EMWO_OSCUIService extends EMWO_OSCUIServiceBase {

    /**
     * Creates an instance of  EMWO_OSCUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_OSCUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}