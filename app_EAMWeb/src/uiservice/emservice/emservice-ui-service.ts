import EMServiceUIServiceBase from './emservice-ui-service-base';

/**
 * 服务商UI服务对象
 *
 * @export
 * @class EMServiceUIService
 */
export default class EMServiceUIService extends EMServiceUIServiceBase {

    /**
     * Creates an instance of  EMServiceUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMServiceUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}