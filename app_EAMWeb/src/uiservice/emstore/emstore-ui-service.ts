import EMStoreUIServiceBase from './emstore-ui-service-base';

/**
 * 仓库UI服务对象
 *
 * @export
 * @class EMStoreUIService
 */
export default class EMStoreUIService extends EMStoreUIServiceBase {

    /**
     * Creates an instance of  EMStoreUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStoreUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}