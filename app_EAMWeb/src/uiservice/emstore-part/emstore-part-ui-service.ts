import EMStorePartUIServiceBase from './emstore-part-ui-service-base';

/**
 * 仓库库位UI服务对象
 *
 * @export
 * @class EMStorePartUIService
 */
export default class EMStorePartUIService extends EMStorePartUIServiceBase {

    /**
     * Creates an instance of  EMStorePartUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStorePartUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}