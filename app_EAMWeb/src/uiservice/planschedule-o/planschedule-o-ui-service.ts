import PLANSCHEDULE_OUIServiceBase from './planschedule-o-ui-service-base';

/**
 * 自定义间隔天数UI服务对象
 *
 * @export
 * @class PLANSCHEDULE_OUIService
 */
export default class PLANSCHEDULE_OUIService extends PLANSCHEDULE_OUIServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_OUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_OUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}