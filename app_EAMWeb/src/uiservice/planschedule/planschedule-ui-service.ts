import PLANSCHEDULEUIServiceBase from './planschedule-ui-service-base';

/**
 * 计划时刻设置UI服务对象
 *
 * @export
 * @class PLANSCHEDULEUIService
 */
export default class PLANSCHEDULEUIService extends PLANSCHEDULEUIServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULEUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULEUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}