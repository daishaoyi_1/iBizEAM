import EMDRWGMapUIServiceBase from './emdrwgmap-ui-service-base';

/**
 * 文档引用UI服务对象
 *
 * @export
 * @class EMDRWGMapUIService
 */
export default class EMDRWGMapUIService extends EMDRWGMapUIServiceBase {

    /**
     * Creates an instance of  EMDRWGMapUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDRWGMapUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}