import PLANSCHEDULE_TUIServiceBase from './planschedule-t-ui-service-base';

/**
 * 计划_定时UI服务对象
 *
 * @export
 * @class PLANSCHEDULE_TUIService
 */
export default class PLANSCHEDULE_TUIService extends PLANSCHEDULE_TUIServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_TUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_TUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}