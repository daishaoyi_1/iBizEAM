import EMRFOMOUIServiceBase from './emrfomo-ui-service-base';

/**
 * 模式UI服务对象
 *
 * @export
 * @class EMRFOMOUIService
 */
export default class EMRFOMOUIService extends EMRFOMOUIServiceBase {

    /**
     * Creates an instance of  EMRFOMOUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOMOUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}