import EMEQLCTFDJUIServiceBase from './emeqlctfdj-ui-service-base';

/**
 * 发动机位置UI服务对象
 *
 * @export
 * @class EMEQLCTFDJUIService
 */
export default class EMEQLCTFDJUIService extends EMEQLCTFDJUIServiceBase {

    /**
     * Creates an instance of  EMEQLCTFDJUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTFDJUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}