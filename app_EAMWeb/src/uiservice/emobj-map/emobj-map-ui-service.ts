import EMObjMapUIServiceBase from './emobj-map-ui-service-base';

/**
 * 对象关系UI服务对象
 *
 * @export
 * @class EMObjMapUIService
 */
export default class EMObjMapUIService extends EMObjMapUIServiceBase {

    /**
     * Creates an instance of  EMObjMapUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMObjMapUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}