/**
 * 方案
 *
 * @export
 * @interface EMRFOAC
 */
export interface EMRFOAC {

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    enable?: any;

    /**
     * 对象编号
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    objid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    description?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    createman?: any;

    /**
     * 方案名称
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    emrfoacname?: any;

    /**
     * 方案代码
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    rfoaccode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    orgid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    updateman?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    rfoacinfo?: any;

    /**
     * 方案标识
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    emrfoacid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    rfodenane?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    rfomoname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    rfodeid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMRFOAC
     */
    rfomoid?: any;
}