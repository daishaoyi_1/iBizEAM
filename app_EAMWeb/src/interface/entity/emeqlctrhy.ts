/**
 * 润滑油位置
 *
 * @export
 * @interface EMEQLCTRHY
 */
export interface EMEQLCTRHY {

    /**
     * 油量
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    yl?: any;

    /**
     * 型号
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    eqmodelcode?: any;

    /**
     * 润滑油类型
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    rhytype?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    enable?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    updateman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    orgid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    description?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    createman?: any;

    /**
     * 润滑油信息
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    eqlocationinfo?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    equipname?: any;

    /**
     * 位置标识
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    emeqlocationid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTRHY
     */
    equipid?: any;
}