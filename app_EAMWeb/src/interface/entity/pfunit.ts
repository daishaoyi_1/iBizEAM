/**
 * 计量单位
 *
 * @export
 * @interface PFUnit
 */
export interface PFUnit {

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PFUnit
     */
    description?: any;

    /**
     * 计量单位名称
     *
     * @returns {*}
     * @memberof PFUnit
     */
    pfunitname?: any;

    /**
     * 计量单位代码
     *
     * @returns {*}
     * @memberof PFUnit
     */
    unitcode?: any;

    /**
     * 计量单位信息
     *
     * @returns {*}
     * @memberof PFUnit
     */
    unitinfo?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof PFUnit
     */
    orgid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PFUnit
     */
    createman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PFUnit
     */
    createdate?: any;

    /**
     * 计量单位标识
     *
     * @returns {*}
     * @memberof PFUnit
     */
    pfunitid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PFUnit
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PFUnit
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof PFUnit
     */
    enable?: any;
}