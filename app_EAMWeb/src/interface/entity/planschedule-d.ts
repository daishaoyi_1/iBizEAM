/**
 * 计划_按天
 *
 * @export
 * @interface PLANSCHEDULE_D
 */
export interface PLANSCHEDULE_D {

    /**
     * 计划_按天名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    planschedule_dname?: any;

    /**
     * 计划_按天标识
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    planschedule_did?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    updatedate?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    emplanid?: any;

    /**
     * 间隔时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    intervalminute?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    scheduleparam?: any;

    /**
     * 循环开始时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    cyclestarttime?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    emplanname?: any;

    /**
     * 循环结束时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    cycleendtime?: any;

    /**
     * 时刻类型
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    scheduletype?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    lastminute?: any;

    /**
     * 时刻设置状态
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    schedulestate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    scheduleparam2?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    scheduleparam4?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    description?: any;

    /**
     * 运行日期
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    rundate?: any;

    /**
     * 执行时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    runtime?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    scheduleparam3?: any;

    /**
     * 定时任务
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_D
     */
    taskid?: any;
}