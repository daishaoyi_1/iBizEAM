/**
 * 设备状态监控
 *
 * @export
 * @interface EMEQMonitor
 */
export interface EMEQMonitor {

    /**
     * 状态区间截至
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    edate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    createman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    orgid?: any;

    /**
     * 运行状态
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    val?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    enable?: any;

    /**
     * 设备状态监控标识
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    emeqmonitorid?: any;

    /**
     * 设备状态监控名称
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    emeqmonitorname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    createdate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    description?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    updateman?: any;

    /**
     * 状态区间起始
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    bdate?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    woname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    equipname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    woid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMonitor
     */
    equipid?: any;
}