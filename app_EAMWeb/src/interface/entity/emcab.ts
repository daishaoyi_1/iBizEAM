/**
 * 货架
 *
 * @export
 * @interface EMCab
 */
export interface EMCab {

    /**
     * ARG2
     *
     * @returns {*}
     * @memberof EMCab
     */
    arg2?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMCab
     */
    updatedate?: any;

    /**
     * 货架管理
     *
     * @returns {*}
     * @memberof EMCab
     */
    storepartgl?: any;

    /**
     * ARG1
     *
     * @returns {*}
     * @memberof EMCab
     */
    arg1?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMCab
     */
    enable?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMCab
     */
    updateman?: any;

    /**
     * ARG0
     *
     * @returns {*}
     * @memberof EMCab
     */
    arg0?: any;

    /**
     * 货架标识
     *
     * @returns {*}
     * @memberof EMCab
     */
    emcabid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMCab
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMCab
     */
    orgid?: any;

    /**
     * 货架名称
     *
     * @returns {*}
     * @memberof EMCab
     */
    emcabname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMCab
     */
    createman?: any;

    /**
     * GRAPHPARAM
     *
     * @returns {*}
     * @memberof EMCab
     */
    graphparam?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMCab
     */
    emstorepartname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMCab
     */
    emstorename?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMCab
     */
    emstorepartid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMCab
     */
    emstoreid?: any;
}