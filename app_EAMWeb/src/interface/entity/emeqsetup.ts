/**
 * 更换安装
 *
 * @export
 * @interface EMEQSetup
 */
export interface EMEQSetup {

    /**
     * 更换安装理由
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    activeadesc?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    enable?: any;

    /**
     * 安装记录名称
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    emeqsetupname?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    regionenddate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    orgid?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    prefee?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    updatedate?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    content?: any;

    /**
     * 更换安装记录
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    activedesc?: any;

    /**
     * 更换安装日期
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    activedate?: any;

    /**
     * 材料费(￥)
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    mfee?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    regionbegindate?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    pic5?: any;

    /**
     * 服务费(￥)
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    sfee?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    createman?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    eqstoplength?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    createdate?: any;

    /**
     * 更换安装结果
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    activebdesc?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    activelengths?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rempname?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rempid?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    pic2?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    pic?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    pic3?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    description?: any;

    /**
     * 安装记录标识
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    emeqsetupid?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rdeptname?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    pic1?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rdeptid?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    pic4?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    updateman?: any;

    /**
     * 人工费(￥)
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    pfee?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rteamname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfodename?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfocaname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    acclassname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    woname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rservicename?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfoacname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    equipname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    objname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfomoname?: any;

    /**
     * 出场编号
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    eitiresname?: any;

    /**
     * 出场编号
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    eitiresid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfodeid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    objid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    equipid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfocaid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    acclassid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfomoid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rserviceid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rteamid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    woid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQSetup
     */
    rfoacid?: any;
}