/**
 * 设备档案
 *
 * @export
 * @interface EMEquip
 */
export interface EMEquip {

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic9?: any;

    /**
     * 强检周期(年)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efcheck?: any;

    /**
     * 下次检测日期
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efcheckndate?: any;

    /**
     * 更换价格
     *
     * @returns {*}
     * @memberof EMEquip
     */
    replacecost?: any;

    /**
     * 设备优先级
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqpriority?: any;

    /**
     * 利用率(当年)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efficiency_y?: any;

    /**
     * 完好率(当年)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    intactrate_y?: any;

    /**
     * 完好率(当季度)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    intactrate_q?: any;

    /**
     * 利用率(当月)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efficiency_m?: any;

    /**
     * 设备名称
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emequipname?: any;

    /**
     * 设备标识
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emequipid?: any;

    /**
     * 强检备注
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efcheckdesc?: any;

    /**
     * 故障率(当月)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    failurerate_m?: any;

    /**
     * 专责部门
     *
     * @returns {*}
     * @memberof EMEquip
     */
    deptid?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic6?: any;

    /**
     * 材料成本
     *
     * @returns {*}
     * @memberof EMEquip
     */
    materialcost?: any;

    /**
     * 停机类型
     *
     * @returns {*}
     * @memberof EMEquip
     */
    haltstate?: any;

    /**
     * 完好率(当月)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    intactrate_m?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic8?: any;

    /**
     * 购买日期
     *
     * @returns {*}
     * @memberof EMEquip
     */
    purchdate?: any;

    /**
     * 停机原因
     *
     * @returns {*}
     * @memberof EMEquip
     */
    haltcause?: any;

    /**
     * 生产属性
     *
     * @returns {*}
     * @memberof EMEquip
     */
    productparam?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic4?: any;

    /**
     * 集团设备编号
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equip_bh?: any;

    /**
     * 维护成本累计
     *
     * @returns {*}
     * @memberof EMEquip
     */
    maintenancecost?: any;

    /**
     * 设备状态
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqstate?: any;

    /**
     * 寿命
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqlife?: any;

    /**
     * 初始价格
     *
     * @returns {*}
     * @memberof EMEquip
     */
    originalcost?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEquip
     */
    createdate?: any;

    /**
     * 基本参数
     *
     * @returns {*}
     * @memberof EMEquip
     */
    params?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEquip
     */
    updateman?: any;

    /**
     * 箱量操作量(当季度)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    outputrct_dj?: any;

    /**
     * 产品系列号
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqserialcode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEquip
     */
    orgid?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic2?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic3?: any;

    /**
     * 设备代码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equipcode?: any;

    /**
     * 箱量操作量(当月)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    outputrct_dy?: any;

    /**
     * 成本中心
     *
     * @returns {*}
     * @memberof EMEquip
     */
    costcenterid?: any;

    /**
     * 投运日期
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqstartdate?: any;

    /**
     * 保修终止日期
     *
     * @returns {*}
     * @memberof EMEquip
     */
    warrantydate?: any;

    /**
     * 统计大型设备
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqisservice1?: any;

    /**
     * 故障率(当季度)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    failurerate_q?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic7?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic?: any;

    /**
     * 所属系统备注
     *
     * @returns {*}
     * @memberof EMEquip
     */
    blsystemdesc?: any;

    /**
     * 利用率(当季度)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efficiency_q?: any;

    /**
     * 产品型号
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqmodelcode?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEquip
     */
    pic5?: any;

    /**
     * 设备备注
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equipdesc?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEquip
     */
    description?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEquip
     */
    enable?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEquip
     */
    createman?: any;

    /**
     * 人工成本
     *
     * @returns {*}
     * @memberof EMEquip
     */
    innerlaborcost?: any;

    /**
     * 关键属性参数
     *
     * @returns {*}
     * @memberof EMEquip
     */
    keyattparam?: any;

    /**
     * 专责人
     *
     * @returns {*}
     * @memberof EMEquip
     */
    empid?: any;

    /**
     * 工艺代码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    techcode?: any;

    /**
     * 箱量操作量(当年)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    outputrct_dn?: any;

    /**
     * 是否在工作
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqisservice?: any;

    /**
     * 故障率(当年)
     *
     * @returns {*}
     * @memberof EMEquip
     */
    failurerate_y?: any;

    /**
     * 本次检测日期
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efcheckdate?: any;

    /**
     * 服务成本
     *
     * @returns {*}
     * @memberof EMEquip
     */
    foreignlaborcost?: any;

    /**
     * 设备分组
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equipgroup?: any;

    /**
     * 专责部门
     *
     * @returns {*}
     * @memberof EMEquip
     */
    deptname?: any;

    /**
     * 设备状态情况
     *
     * @returns {*}
     * @memberof EMEquip
     */
    haltstateinfo?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEquip
     */
    updatedate?: any;

    /**
     * 本次发证日期
     *
     * @returns {*}
     * @memberof EMEquip
     */
    efcheckcdate?: any;

    /**
     * 设备信息
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equipinfo?: any;

    /**
     * 总停机时间
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqsumstoptime?: any;

    /**
     * 专责人
     *
     * @returns {*}
     * @memberof EMEquip
     */
    empname?: any;

    /**
     * 泊位编码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emberthcode?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEquip
     */
    rteamname?: any;

    /**
     * 机种编号1
     *
     * @returns {*}
     * @memberof EMEquip
     */
    jzbh1?: any;

    /**
     * 机种
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emmachinecategoryname?: any;

    /**
     * 统计归口类型分组
     *
     * @returns {*}
     * @memberof EMEquip
     */
    stype?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMEquip
     */
    mservicename?: any;

    /**
     * 设备类型代码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqtypecode?: any;

    /**
     * 资产科目代码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    assetclasscode?: any;

    /**
     * 上级设备代码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equippcode?: any;

    /**
     * 设备类型
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqtypename?: any;

    /**
     * 机型
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emmachmodelname?: any;

    /**
     * 品牌编码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    embrandcode?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEquip
     */
    acclassname?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMEquip
     */
    labservicename?: any;

    /**
     * 资产
     *
     * @returns {*}
     * @memberof EMEquip
     */
    assetname?: any;

    /**
     * 资产科目
     *
     * @returns {*}
     * @memberof EMEquip
     */
    assetclassname?: any;

    /**
     * 品牌
     *
     * @returns {*}
     * @memberof EMEquip
     */
    embrandname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqlocationname?: any;

    /**
     * 上级设备
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equippname?: any;

    /**
     * 资产科目
     *
     * @returns {*}
     * @memberof EMEquip
     */
    assetclassid?: any;

    /**
     * 合同
     *
     * @returns {*}
     * @memberof EMEquip
     */
    contractname?: any;

    /**
     * 泊位
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emberthname?: any;

    /**
     * 资产代码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    assetcode?: any;

    /**
     * 服务提供商
     *
     * @returns {*}
     * @memberof EMEquip
     */
    rservicename?: any;

    /**
     * 位置代码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqlocationcode?: any;

    /**
     * 机种编码
     *
     * @returns {*}
     * @memberof EMEquip
     */
    machtypecode?: any;

    /**
     * 上级设备类型
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqtypepid?: any;

    /**
     * 统计归口类型
     *
     * @returns {*}
     * @memberof EMEquip
     */
    sname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqlocationid?: any;

    /**
     * 机种
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emmachinecategoryid?: any;

    /**
     * 泊位
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emberthid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEquip
     */
    rteamid?: any;

    /**
     * 品牌
     *
     * @returns {*}
     * @memberof EMEquip
     */
    embrandid?: any;

    /**
     * 上级设备
     *
     * @returns {*}
     * @memberof EMEquip
     */
    equippid?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMEquip
     */
    labserviceid?: any;

    /**
     * 服务提供商
     *
     * @returns {*}
     * @memberof EMEquip
     */
    rserviceid?: any;

    /**
     * 机型
     *
     * @returns {*}
     * @memberof EMEquip
     */
    emmachmodelid?: any;

    /**
     * 合同
     *
     * @returns {*}
     * @memberof EMEquip
     */
    contractid?: any;

    /**
     * 设备类型
     *
     * @returns {*}
     * @memberof EMEquip
     */
    eqtypeid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEquip
     */
    acclassid?: any;

    /**
     * 资产
     *
     * @returns {*}
     * @memberof EMEquip
     */
    assetid?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMEquip
     */
    mserviceid?: any;
}