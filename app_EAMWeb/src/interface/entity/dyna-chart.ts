/**
 * 动态图表
 *
 * @export
 * @interface DynaChart
 */
export interface DynaChart {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof DynaChart
     */
    createdate?: any;

    /**
     * 动态图表名称
     *
     * @returns {*}
     * @memberof DynaChart
     */
    dynachartname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof DynaChart
     */
    updatedate?: any;

    /**
     * 模型标识
     *
     * @returns {*}
     * @memberof DynaChart
     */
    modelid?: any;

    /**
     * 应用标识
     *
     * @returns {*}
     * @memberof DynaChart
     */
    appid?: any;

    /**
     * 模型
     *
     * @returns {*}
     * @memberof DynaChart
     */
    model?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof DynaChart
     */
    updateman?: any;

    /**
     * 用户标识
     *
     * @returns {*}
     * @memberof DynaChart
     */
    userid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof DynaChart
     */
    createman?: any;

    /**
     * 动态图表标识
     *
     * @returns {*}
     * @memberof DynaChart
     */
    dynachartid?: any;
}