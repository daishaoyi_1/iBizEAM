/**
 * 泊位
 *
 * @export
 * @interface EMBerth
 */
export interface EMBerth {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMBerth
     */
    createman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMBerth
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMBerth
     */
    enable?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMBerth
     */
    updateman?: any;

    /**
     * 泊位编码
     *
     * @returns {*}
     * @memberof EMBerth
     */
    berthcode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMBerth
     */
    createdate?: any;

    /**
     * 泊位名称
     *
     * @returns {*}
     * @memberof EMBerth
     */
    emberthname?: any;

    /**
     * 流水号
     *
     * @returns {*}
     * @memberof EMBerth
     */
    emberthid?: any;
}