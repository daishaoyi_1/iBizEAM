import { Verify } from '@/utils/verify/verify';


/**
 * 生成订单编号
 *
 * @export
 * @class GenIdLogicBase
 */
export default class GenIdLogicBase {

    /**
     * 名称
     * 
     * @memberof  GenIdLogicBase
     */
    private name:string ="GenId";

    /**
     * 唯一标识
     * 
     * @memberof  GenIdLogicBase
     */
    private id:string = "6e892c7c9a8937f434959e92e307fcdb";

    /**
     * 默认参数名称
     * 
     * @memberof  GenIdLogicBase
     */
    private defaultParamName:string = "Default";

    /**
     * 参数集合
     * 
     * @memberof  GenIdLogicBase
     */
    private paramsMap:Map<string,any> = new Map();

    /**
     * Creates an instance of  GenIdLogicBase.
     * 
     * @param {*} [opts={}]
     * @memberof  GenIdLogicBase
     */
    constructor(opts: any = {}) {
        this.initParams(opts);
    }

    /**
     * 初始化参数集合
     * 
     * @param {*} [opts={}]
     * @memberof  GenIdLogicBase
     */
    public initParams(opts:any){
        this.paramsMap.set('IBZSEQ',{});
        this.paramsMap.set('Default',opts);
    }


    /**
     * 计算0节点结果
     * 
     * @param params 传入参数
     */
    public compute0Cond(params:any):boolean{
        return true;
    }

    /**
     * 计算1节点结果
     * 
     * @param params 传入参数
     */
    public compute1Cond(params:any):boolean{
        return true;
    }

    /**
     * 计算2节点结果
     * 
     * @param params 传入参数
     */
    public compute2Cond(params:any):boolean{
        return true;
    }

    /**
     * 执行逻辑
     * 
     * @param context 应用上下文
     * @param params 传入参数
     */
    public onExecute(context:any,params:any,isloading:boolean){
        return this.executeBegin(context,params,isloading);
    }


    /**
    * 生成订单编号
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executeDeaction1(context:any,params:any,isloading:boolean){
        // 行为处理节点
    }

    /**
    * 开始
    * 
    * @param params 传入参数
    */
    private async executeBegin(context:any,params:any,isloading:boolean){
        //开始节点
        if(this.compute1Cond(params)){
            return this.executePrepareparam1(context,params,isloading);   
        }
    }

    /**
    * 准备参数-回填
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executePrepareparam2(context:any,params:any,isloading:boolean){
        // 准备参数节点
    let tempDstParam0Context:any = this.paramsMap.get('Default').context?this.paramsMap.get('Default').context:{};
    let tempDstParam0Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    let tempSrcParam0Data:any = this.paramsMap.get('IBZSEQ').data?this.paramsMap.get('IBZSEQ').data:{};
    Object.assign(tempDstParam0Context,{empo:tempSrcParam0Data['curid']});
    Object.assign(tempDstParam0Data,{empoid:tempSrcParam0Data['curid']});
    this.paramsMap.set('Default',{data:tempDstParam0Data,context:tempDstParam0Context});
        return this.paramsMap.get(this.defaultParamName).data;
    }

    /**
    * 准备参数
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executePrepareparam1(context:any,params:any,isloading:boolean){
        // 准备参数节点
    let tempDstParam0Context:any = this.paramsMap.get('IBZSEQ').context?this.paramsMap.get('IBZSEQ').context:{};
    let tempDstParam0Data:any = this.paramsMap.get('IBZSEQ').data?this.paramsMap.get('IBZSEQ').data:{};
    Object.assign(tempDstParam0Data,{prefix:"B"});
    this.paramsMap.set('IBZSEQ',{data:tempDstParam0Data,context:tempDstParam0Context});
    let tempDstParam1Context:any = this.paramsMap.get('IBZSEQ').context?this.paramsMap.get('IBZSEQ').context:{};
    let tempDstParam1Data:any = this.paramsMap.get('IBZSEQ').data?this.paramsMap.get('IBZSEQ').data:{};
    Object.assign(tempDstParam1Data,{entity:"EMPO"});
    this.paramsMap.set('IBZSEQ',{data:tempDstParam1Data,context:tempDstParam1Context});
    let tempDstParam2Context:any = this.paramsMap.get('IBZSEQ').context?this.paramsMap.get('IBZSEQ').context:{};
    let tempDstParam2Data:any = this.paramsMap.get('IBZSEQ').data?this.paramsMap.get('IBZSEQ').data:{};
    Object.assign(tempDstParam2Data,{seqtype:"type1"});
    this.paramsMap.set('IBZSEQ',{data:tempDstParam2Data,context:tempDstParam2Context});
        if(this.compute0Cond(params)){
            return this.executeDeaction1(context,params,isloading);   
        }
    }


}