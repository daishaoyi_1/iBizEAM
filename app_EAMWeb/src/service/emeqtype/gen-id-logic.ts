import { Http } from '@/utils';
import { Util } from '@/utils';
import GenIdLogicBase from './gen-id-logic-base';

/**
 * GenId
 *
 * @export
 * @class GenIdLogic
 */
export default class GenIdLogic extends GenIdLogicBase{

    /**
     * Creates an instance of  GenIdLogic
     * 
     * @param {*} [opts={}]
     * @memberof  GenIdLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}