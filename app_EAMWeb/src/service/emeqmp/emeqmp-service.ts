import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQMPServiceBase from './emeqmp-service-base';


/**
 * 设备仪表服务对象
 *
 * @export
 * @class EMEQMPService
 * @extends {EMEQMPServiceBase}
 */
export default class EMEQMPService extends EMEQMPServiceBase {

    /**
     * Creates an instance of  EMEQMPService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMPService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}