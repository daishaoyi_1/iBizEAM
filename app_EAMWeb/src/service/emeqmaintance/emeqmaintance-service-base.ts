import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 抢修记录服务对象基类
 *
 * @export
 * @class EMEQMaintanceServiceBase
 * @extends {EntityServie}
 */
export default class EMEQMaintanceServiceBase extends EntityService {

    /**
     * Creates an instance of  EMEQMaintanceServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMaintanceServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMEQMaintanceServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emeqmaintance';
        this.APPDEKEY = 'emeqmaintanceid';
        this.APPDENAME = 'emeqmaintances';
        this.APPDETEXT = 'emeqmaintancename';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqmaintance){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqmaintance){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emeqmaintances/${context.emeqmaintance}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances`,data,isloading);
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emeqmaintances`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emeqmaintances`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqmaintance){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqmaintance){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emeqmaintances/${context.emeqmaintance}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqmaintance){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}`,isloading);
            return res;
        }
        if(context.emequip && context.emeqmaintance){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emeqmaintances/${context.emeqmaintance}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqmaintance){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqmaintance){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emeqmaintances/${context.emeqmaintance}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqmaintance) delete tempData.emeqmaintance;
            if(tempData.emeqmaintanceid) delete tempData.emeqmaintanceid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/getdraft`,tempData,isloading);
            res.data.emeqmaintance = data.emeqmaintance;
            
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqmaintance) delete tempData.emeqmaintance;
            if(tempData.emeqmaintanceid) delete tempData.emeqmaintanceid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emeqmaintances/getdraft`,tempData,isloading);
            res.data.emeqmaintance = data.emeqmaintance;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emeqmaintance) delete tempData.emeqmaintance;
        if(tempData.emeqmaintanceid) delete tempData.emeqmaintanceid;
        let res:any = await  Http.getInstance().get(`/emeqmaintances/getdraft`,tempData,isloading);
        res.data.emeqmaintance = data.emeqmaintance;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqmaintance){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqmaintance){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emeqmaintances/${context.emeqmaintance}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqmaintance){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqmaintance){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emeqmaintances/${context.emeqmaintance}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emeqmaintances/${context.emeqmaintance}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchCalendar接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async FetchCalendar(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/fetchcalendar`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emeqmaintances/fetchcalendar`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emeqmaintances/fetchcalendar`,tempData,isloading);
        return res;
    }

    /**
     * searchCalendar接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async searchCalendar(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/searchcalendar`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emeqmaintances/searchcalendar`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emeqmaintances/searchcalendar`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emeqmaintances/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emeqmaintances/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQMaintanceServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqmaintances/searchdefault`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emeqmaintances/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emeqmaintances/searchdefault`,tempData,isloading);
    }
}