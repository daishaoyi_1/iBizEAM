import { Http } from '@/utils';
import { Util } from '@/utils';
import EMObjMapServiceBase from './emobj-map-service-base';


/**
 * 对象关系服务对象
 *
 * @export
 * @class EMObjMapService
 * @extends {EMObjMapServiceBase}
 */
export default class EMObjMapService extends EMObjMapServiceBase {

    /**
     * Creates an instance of  EMObjMapService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMObjMapService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}