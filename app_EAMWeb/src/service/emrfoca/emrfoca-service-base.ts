import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 原因服务对象基类
 *
 * @export
 * @class EMRFOCAServiceBase
 * @extends {EntityServie}
 */
export default class EMRFOCAServiceBase extends EntityService {

    /**
     * Creates an instance of  EMRFOCAServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOCAServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMRFOCAServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emrfoca';
        this.APPDEKEY = 'emrfocaid';
        this.APPDENAME = 'emrfocas';
        this.APPDETEXT = 'emrfocaname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoca){
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}/select`,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoca){
            let res:any = Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}/select`,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoca){
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfocas/${context.emrfoca}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emrfocas/${context.emrfoca}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfocas`,data,isloading);
            
            return res;
        }
        if(context.emrfode && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfocas`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emrfocas`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}`,data,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfodes/${context.emrfode}/emrfocas/${context.emrfoca}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emrfocas/${context.emrfoca}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoca){
            let res:any = Http.getInstance().delete(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}`,isloading);
            return res;
        }
        if(context.emrfomo && context.emrfoca){
            let res:any = Http.getInstance().delete(`/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}`,isloading);
            return res;
        }
        if(context.emrfode && context.emrfoca){
            let res:any = Http.getInstance().delete(`/emrfodes/${context.emrfode}/emrfocas/${context.emrfoca}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emrfocas/${context.emrfoca}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoca){
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}`,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoca){
            let res:any = await Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}`,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoca){
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfocas/${context.emrfoca}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emrfocas/${context.emrfoca}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfoca) delete tempData.emrfoca;
            if(tempData.emrfocaid) delete tempData.emrfocaid;
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/getdraft`,tempData,isloading);
            res.data.emrfoca = data.emrfoca;
            
            return res;
        }
        if(context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfoca) delete tempData.emrfoca;
            if(tempData.emrfocaid) delete tempData.emrfocaid;
            let res:any = await Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfocas/getdraft`,tempData,isloading);
            res.data.emrfoca = data.emrfoca;
            
            return res;
        }
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfoca) delete tempData.emrfoca;
            if(tempData.emrfocaid) delete tempData.emrfocaid;
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfocas/getdraft`,tempData,isloading);
            res.data.emrfoca = data.emrfoca;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emrfoca) delete tempData.emrfoca;
        if(tempData.emrfocaid) delete tempData.emrfocaid;
        let res:any = await  Http.getInstance().get(`/emrfocas/getdraft`,tempData,isloading);
        res.data.emrfoca = data.emrfoca;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfocas/${context.emrfoca}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emrfocas/${context.emrfoca}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}/save`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfocas/${context.emrfoca}/save`,data,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoca){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfocas/${context.emrfoca}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emrfocas/${context.emrfoca}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfocas/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfocas/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emrfocas/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOCAServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfocas/searchdefault`,tempData,isloading);
        }
        if(context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfocas/searchdefault`,tempData,isloading);
        }
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfocas/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emrfocas/searchdefault`,tempData,isloading);
    }
}