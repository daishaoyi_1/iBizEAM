import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 计划模板服务对象基类
 *
 * @export
 * @class EMPlanTemplServiceBase
 * @extends {EntityServie}
 */
export default class EMPlanTemplServiceBase extends EntityService {

    /**
     * Creates an instance of  EMPlanTemplServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanTemplServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMPlanTemplServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emplantempl';
        this.APPDEKEY = 'emplantemplid';
        this.APPDENAME = 'emplantempls';
        this.APPDETEXT = 'emplantemplname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl){
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emplantempls/${context.emplantempl}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplans',JSON.stringify(res.data.emplans?res.data.emplans:[]));
            
            return res;
        }
        if(context.emservice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplans',JSON.stringify(res.data.emplans?res.data.emplans:[]));
            
            return res;
        }
        if(context.emacclass && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplans',JSON.stringify(res.data.emplans?res.data.emplans:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emplantempls`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emplans',JSON.stringify(res.data.emplans?res.data.emplans:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emplantempls/${context.emplantempl}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}`,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl){
            let res:any = Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emplantempls/${context.emplantempl}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplantempl) delete tempData.emplantempl;
            if(tempData.emplantemplid) delete tempData.emplantemplid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/getdraft`,tempData,isloading);
            res.data.emplantempl = data.emplantempl;
            
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplantempl) delete tempData.emplantempl;
            if(tempData.emplantemplid) delete tempData.emplantemplid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/getdraft`,tempData,isloading);
            res.data.emplantempl = data.emplantempl;
            
            return res;
        }
        if(context.emacclass && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplantempl) delete tempData.emplantempl;
            if(tempData.emplantemplid) delete tempData.emplantemplid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/getdraft`,tempData,isloading);
            res.data.emplantempl = data.emplantempl;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emplantempl) delete tempData.emplantempl;
        if(tempData.emplantemplid) delete tempData.emplantemplid;
        let res:any = await  Http.getInstance().get(`/emplantempls/getdraft`,tempData,isloading);
        res.data.emplantempl = data.emplantempl;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emplantempls/${context.emplantempl}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emplantempls/${context.emplantempl}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emplantempls/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanTemplServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/searchdefault`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emplantempls/searchdefault`,tempData,isloading);
    }
}