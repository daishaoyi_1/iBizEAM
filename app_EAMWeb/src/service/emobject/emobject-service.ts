import { Http } from '@/utils';
import { Util } from '@/utils';
import EMObjectServiceBase from './emobject-service-base';


/**
 * 对象服务对象
 *
 * @export
 * @class EMObjectService
 * @extends {EMObjectServiceBase}
 */
export default class EMObjectService extends EMObjectServiceBase {

    /**
     * Creates an instance of  EMObjectService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMObjectService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}