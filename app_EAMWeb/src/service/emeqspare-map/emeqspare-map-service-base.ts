import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 备件包引用服务对象基类
 *
 * @export
 * @class EMEQSpareMapServiceBase
 * @extends {EntityServie}
 */
export default class EMEQSpareMapServiceBase extends EntityService {

    /**
     * Creates an instance of  EMEQSpareMapServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareMapServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMEQSpareMapServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emeqsparemap';
        this.APPDEKEY = 'emeqsparemapid';
        this.APPDENAME = 'emeqsparemaps';
        this.APPDETEXT = 'emeqsparemapname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && context.emeqsparemap){
            let res:any = Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparemaps/${context.emeqsparemap}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emeqsparemaps/${context.emeqsparemap}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparemaps`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emeqsparemaps`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && context.emeqsparemap){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emeqspares/${context.emeqspare}/emeqsparemaps/${context.emeqsparemap}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emeqsparemaps/${context.emeqsparemap}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && context.emeqsparemap){
            let res:any = Http.getInstance().delete(`/emeqspares/${context.emeqspare}/emeqsparemaps/${context.emeqsparemap}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emeqsparemaps/${context.emeqsparemap}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && context.emeqsparemap){
            let res:any = await Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparemaps/${context.emeqsparemap}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emeqsparemaps/${context.emeqsparemap}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqsparemap) delete tempData.emeqsparemap;
            if(tempData.emeqsparemapid) delete tempData.emeqsparemapid;
            let res:any = await Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparemaps/getdraft`,tempData,isloading);
            res.data.emeqsparemap = data.emeqsparemap;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emeqsparemap) delete tempData.emeqsparemap;
        if(tempData.emeqsparemapid) delete tempData.emeqsparemapid;
        let res:any = await  Http.getInstance().get(`/emeqsparemaps/getdraft`,tempData,isloading);
        res.data.emeqsparemap = data.emeqsparemap;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && context.emeqsparemap){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparemaps/${context.emeqsparemap}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emeqsparemaps/${context.emeqsparemap}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && context.emeqsparemap){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparemaps/${context.emeqsparemap}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emeqsparemaps/${context.emeqsparemap}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchByEQ接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async FetchByEQ(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparemaps/fetchbyeq`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emeqsparemaps/fetchbyeq`,tempData,isloading);
        return res;
    }

    /**
     * searchByEQ接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async searchByEQ(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparemaps/searchbyeq`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emeqsparemaps/searchbyeq`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparemaps/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emeqsparemaps/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareMapServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparemaps/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emeqsparemaps/searchdefault`,tempData,isloading);
    }
}