import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';
import FillItemLogic from '@/service/emwplist-cost/fill-item-logic';



/**
 * 询价单服务对象基类
 *
 * @export
 * @class EMWPListCostServiceBase
 * @extends {EntityServie}
 */
export default class EMWPListCostServiceBase extends EntityService {

    /**
     * Creates an instance of  EMWPListCostServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWPListCostServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMWPListCostServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emwplistcost';
        this.APPDEKEY = 'emwplistcostid';
        this.APPDENAME = 'emwplistcosts';
        this.APPDETEXT = 'emwplistcostname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emwplistcosts/${context.emwplistcost}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emservice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emwplistcosts`,data,isloading);
            
            return res;
        }
        if(context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplistcosts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emwplistcosts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emwplistcosts/${context.emwplistcost}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emwplist && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emservice && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
        if(context.emitem && context.emwplistcost){
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emwplistcosts/${context.emwplistcost}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emwplist && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplistcost){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emwplistcosts/${context.emwplistcost}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emwplists/${context.emwplist}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwplistcost) delete tempData.emwplistcost;
            if(tempData.emwplistcostid) delete tempData.emwplistcostid;
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emwplistcosts/getdraft`,tempData,isloading);
            res.data.emwplistcost = data.emwplistcost;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emwplistcost) delete tempData.emwplistcost;
        if(tempData.emwplistcostid) delete tempData.emwplistcostid;
        let res:any = await  Http.getInstance().get(`/emwplistcosts/getdraft`,tempData,isloading);
        res.data.emwplistcost = data.emwplistcost;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwplistcosts/${context.emwplistcost}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Confirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async Confirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwplistcosts/${context.emwplistcost}/confirm`,data,isloading);
            return res;
    }

    /**
     * FillItem接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async FillItem(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let appLogic:FillItemLogic = new FillItemLogic({context:JSON.parse(JSON.stringify(context)),data:JSON.parse(JSON.stringify(data))});
        const res = await appLogic.onExecute(context,data,isloading?true:false);
        return {status:200,data:res};
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplistcost){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emwplistcosts/${context.emwplistcost}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchCostByItem接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async FetchCostByItem(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplistcosts/fetchcostbyitem`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwplistcosts/fetchcostbyitem`,tempData,isloading);
        return res;
    }

    /**
     * searchCostByItem接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async searchCostByItem(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emwplists/${context.emwplist}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emwplistcosts/searchcostbyitem`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emwplistcosts/searchcostbyitem`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplistcosts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwplistcosts/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWPListCostServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emwplists/${context.emwplist}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emwplistcosts/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emwplistcosts/searchdefault`,tempData,isloading);
    }
}