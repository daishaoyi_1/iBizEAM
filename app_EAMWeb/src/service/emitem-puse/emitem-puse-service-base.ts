import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 领料单服务对象基类
 *
 * @export
 * @class EMItemPUseServiceBase
 * @extends {EntityServie}
 */
export default class EMItemPUseServiceBase extends EntityService {

    /**
     * Creates an instance of  EMItemPUseServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPUseServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMItemPUseServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emitempuse';
        this.APPDEKEY = 'emitempuseid';
        this.APPDENAME = 'emitempuses';
        this.APPDETEXT = 'itempuseinfo';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let res:any = Http.getInstance().get(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emitempuses/${context.emitempuse}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.pfteam && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emstore && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emservice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.empurplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses`,data,isloading);
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emitempuses`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emitempuses/${context.emitempuse}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emstore && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emservice && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let res:any = Http.getInstance().delete(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emitem && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
        if(context.emequip && context.emitempuse){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emitempuses/${context.emitempuse}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let res:any = await Http.getInstance().get(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emitempuses/${context.emitempuse}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/empurplans/${context.empurplan}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitempuse) delete tempData.emitempuse;
            if(tempData.emitempuseid) delete tempData.emitempuseid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emitempuses/getdraft`,tempData,isloading);
            res.data.emitempuse = data.emitempuse;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emitempuse) delete tempData.emitempuse;
        if(tempData.emitempuseid) delete tempData.emitempuseid;
        let res:any = await  Http.getInstance().get(`/emitempuses/getdraft`,tempData,isloading);
        res.data.emitempuse = data.emitempuse;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitempuses/${context.emitempuse}/checkkey`,data,isloading);
            return res;
    }

    /**
     * FormUpdateByITEMID接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async FormUpdateByITEMID(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/emitempuses/${context.emitempuse}/formupdatebyitemid`,data,isloading);
            return res;
    }

    /**
     * Issue接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Issue(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}/issue`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitempuses/${context.emitempuse}/issue`,data,isloading);
            return res;
    }

    /**
     * Rejected接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Rejected(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitempuses/${context.emitempuse}/rejected`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emitempuses/${context.emitempuse}/save`,data,isloading);
            
            return res;
    }

    /**
     * Submit接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async Submit(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.empurplan && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emitempuse){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/${context.emitempuse}/submit`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitempuses/${context.emitempuse}/submit`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empurplans/${context.empurplan}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emitempuses/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitempuses/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/searchdefault`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitempuses/searchdefault`,tempData,isloading);
    }

    /**
     * FetchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async FetchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empurplans/${context.empurplan}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emitempuses/fetchdraft`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitempuses/fetchdraft`,tempData,isloading);
        return res;
    }

    /**
     * searchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async searchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/searchdraft`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/searchdraft`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitempuses/searchdraft`,tempData,isloading);
    }

    /**
     * FetchIssued接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async FetchIssued(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empurplans/${context.empurplan}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emitempuses/fetchissued`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitempuses/fetchissued`,tempData,isloading);
        return res;
    }

    /**
     * searchIssued接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async searchIssued(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/searchissued`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/searchissued`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitempuses/searchissued`,tempData,isloading);
    }

    /**
     * FetchWaitIssue接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async FetchWaitIssue(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empurplans/${context.empurplan}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emitempuses/fetchwaitissue`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitempuses/fetchwaitissue`,tempData,isloading);
        return res;
    }

    /**
     * searchWaitIssue接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemPUseServiceBase
     */
    public async searchWaitIssue(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.empurplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/empurplans/${context.empurplan}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emitempuses/searchwaitissue`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitempuses/searchwaitissue`,tempData,isloading);
    }
}