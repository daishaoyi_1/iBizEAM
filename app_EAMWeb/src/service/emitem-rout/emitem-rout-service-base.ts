import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 退货单服务对象基类
 *
 * @export
 * @class EMItemROutServiceBase
 * @extends {EntityServie}
 */
export default class EMItemROutServiceBase extends EntityService {

    /**
     * Creates an instance of  EMItemROutServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemROutServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMItemROutServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emitemrout';
        this.APPDEKEY = 'emitemroutid';
        this.APPDENAME = 'emitemrouts';
        this.APPDETEXT = 'emitemroutname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emitemrouts/${context.emitemrout}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts`,data,isloading);
            
            return res;
        }
        if(context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emitemrouts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emitemrouts/${context.emitemrout}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            return res;
        }
        if(context.emitem && context.emitemrout){
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emitemrouts/${context.emitemrout}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emitemrouts/${context.emitemrout}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitemrout) delete tempData.emitemrout;
            if(tempData.emitemroutid) delete tempData.emitemroutid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/getdraft`,tempData,isloading);
            res.data.emitemrout = data.emitemrout;
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitemrout) delete tempData.emitemrout;
            if(tempData.emitemroutid) delete tempData.emitemroutid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/getdraft`,tempData,isloading);
            res.data.emitemrout = data.emitemrout;
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitemrout) delete tempData.emitemrout;
            if(tempData.emitemroutid) delete tempData.emitemroutid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/getdraft`,tempData,isloading);
            res.data.emitemrout = data.emitemrout;
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitemrout) delete tempData.emitemrout;
            if(tempData.emitemroutid) delete tempData.emitemroutid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/getdraft`,tempData,isloading);
            res.data.emitemrout = data.emitemrout;
            
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitemrout) delete tempData.emitemrout;
            if(tempData.emitemroutid) delete tempData.emitemroutid;
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emitemrouts/getdraft`,tempData,isloading);
            res.data.emitemrout = data.emitemrout;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emitemrout) delete tempData.emitemrout;
        if(tempData.emitemroutid) delete tempData.emitemroutid;
        let res:any = await  Http.getInstance().get(`/emitemrouts/getdraft`,tempData,isloading);
        res.data.emitemrout = data.emitemrout;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitemrouts/${context.emitemrout}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Confirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Confirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/confirm`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitemrouts/${context.emitemrout}/confirm`,data,isloading);
            return res;
    }

    /**
     * FormUpdateByRID接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async FormUpdateByRID(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/formupdatebyrid`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/formupdatebyrid`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/formupdatebyrid`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/formupdatebyrid`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/formupdatebyrid`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/emitemrouts/${context.emitemrout}/formupdatebyrid`,data,isloading);
            return res;
    }

    /**
     * Rejected接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Rejected(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/rejected`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitemrouts/${context.emitemrout}/rejected`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emitemrouts/${context.emitemrout}/save`,data,isloading);
            
            return res;
    }

    /**
     * Submit接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async Submit(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/submit`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/submit`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/submit`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/submit`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emitemrout){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/${context.emitemrout}/submit`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emitemrouts/${context.emitemrout}/submit`,data,isloading);
            return res;
    }

    /**
     * FetchConfirmed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async FetchConfirmed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchconfirmed`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/fetchconfirmed`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchconfirmed`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/fetchconfirmed`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitemrouts/fetchconfirmed`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitemrouts/fetchconfirmed`,tempData,isloading);
        return res;
    }

    /**
     * searchConfirmed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async searchConfirmed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchconfirmed`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/searchconfirmed`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchconfirmed`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/searchconfirmed`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/searchconfirmed`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitemrouts/searchconfirmed`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitemrouts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitemrouts/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/searchdefault`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitemrouts/searchdefault`,tempData,isloading);
    }

    /**
     * FetchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async FetchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitemrouts/fetchdraft`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitemrouts/fetchdraft`,tempData,isloading);
        return res;
    }

    /**
     * searchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async searchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchdraft`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/searchdraft`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchdraft`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/searchdraft`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/searchdraft`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitemrouts/searchdraft`,tempData,isloading);
    }

    /**
     * FetchToConfirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async FetchToConfirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emitemrouts/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitemrouts/fetchtoconfirm`,tempData,isloading);
        return res;
    }

    /**
     * searchToConfirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemROutServiceBase
     */
    public async searchToConfirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchtoconfirm`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emitemrouts/searchtoconfirm`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emitemrouts/searchtoconfirm`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emitemrouts/searchtoconfirm`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emitemrouts/searchtoconfirm`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitemrouts/searchtoconfirm`,tempData,isloading);
    }
}