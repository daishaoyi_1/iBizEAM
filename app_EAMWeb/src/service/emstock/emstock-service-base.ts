import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 库存服务对象基类
 *
 * @export
 * @class EMStockServiceBase
 * @extends {EntityServie}
 */
export default class EMStockServiceBase extends EntityService {

    /**
     * Creates an instance of  EMStockServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStockServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMStockServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emstock';
        this.APPDEKEY = 'emstockid';
        this.APPDENAME = 'emstocks';
        this.APPDETEXT = 'stockinfo';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emstock){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emstock){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emstock){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emstock){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emstock){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstock){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emstock){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emstock){
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emstocks/${context.emstock}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emstocks/${context.emstock}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks`,data,isloading);
            
            return res;
        }
        if(context.emstore && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstocks`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emstocks`,data,isloading);
            
            return res;
        }
        if(context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emstocks`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emstocks`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emstocks/${context.emstock}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emstocks/${context.emstock}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emstock){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emstock){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emstock){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emstock){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emstock){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            return res;
        }
        if(context.emstore && context.emstock){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstocks/${context.emstock}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emstock){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}`,isloading);
            return res;
        }
        if(context.emitem && context.emstock){
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emstocks/${context.emstock}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emstock){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emstock){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emstock){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emstock){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emstock){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstock){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emstock){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emstock){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emstocks/${context.emstock}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emstocks/${context.emstock}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emstock) delete tempData.emstock;
            if(tempData.emstockid) delete tempData.emstockid;
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emstocks/getdraft`,tempData,isloading);
            res.data.emstock = data.emstock;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emstock) delete tempData.emstock;
        if(tempData.emstockid) delete tempData.emstockid;
        let res:any = await  Http.getInstance().get(`/emstocks/getdraft`,tempData,isloading);
        res.data.emstock = data.emstock;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emstocks/${context.emstock}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emstocks/${context.emstock}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emstock){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emstocks/${context.emstock}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emstocks/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emstocks/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/searchdefault`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstocks/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emstocks/searchdefault`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emstocks/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emstocks/searchdefault`,tempData,isloading);
    }

    /**
     * FetchTypeStock接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async FetchTypeStock(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emstocks/fetchtypestock`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emstocks/fetchtypestock`,tempData,isloading);
        return res;
    }

    /**
     * searchTypeStock接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMStockServiceBase
     */
    public async searchTypeStock(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/searchtypestock`,tempData,isloading);
        }
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emstocks/searchtypestock`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emstocks/searchtypestock`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emstocks/searchtypestock`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emstocks/searchtypestock`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstocks/searchtypestock`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emstocks/searchtypestock`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emstocks/searchtypestock`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emstocks/searchtypestock`,tempData,isloading);
    }
}