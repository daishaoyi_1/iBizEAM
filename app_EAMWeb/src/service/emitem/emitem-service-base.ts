import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 物品服务对象基类
 *
 * @export
 * @class EMItemServiceBase
 * @extends {EntityServie}
 */
export default class EMItemServiceBase extends EntityService {

    /**
     * Creates an instance of  EMItemServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMItemServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emitem';
        this.APPDEKEY = 'emitemid';
        this.APPDENAME = 'emitems';
        this.APPDETEXT = 'emitemname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && true){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emen',JSON.stringify(res.data.emen?res.data.emen:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqsparedetails',JSON.stringify(res.data.emeqsparedetails?res.data.emeqsparedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitempuses',JSON.stringify(res.data.emitempuses?res.data.emitempuses:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrins',JSON.stringify(res.data.emitemrins?res.data.emitemrins:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrouts',JSON.stringify(res.data.emitemrouts?res.data.emitemrouts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemtrades',JSON.stringify(res.data.emitemtrades?res.data.emitemtrades:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_empodetails',JSON.stringify(res.data.empodetails?res.data.empodetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emstocks',JSON.stringify(res.data.emstocks?res.data.emstocks:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplistcosts',JSON.stringify(res.data.emwplistcosts?res.data.emwplistcosts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplists',JSON.stringify(res.data.emwplists?res.data.emwplists:[]));
            
            return res;
        }
        if(context.emstore && true){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emen',JSON.stringify(res.data.emen?res.data.emen:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqsparedetails',JSON.stringify(res.data.emeqsparedetails?res.data.emeqsparedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitempuses',JSON.stringify(res.data.emitempuses?res.data.emitempuses:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrins',JSON.stringify(res.data.emitemrins?res.data.emitemrins:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrouts',JSON.stringify(res.data.emitemrouts?res.data.emitemrouts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemtrades',JSON.stringify(res.data.emitemtrades?res.data.emitemtrades:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_empodetails',JSON.stringify(res.data.empodetails?res.data.empodetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emstocks',JSON.stringify(res.data.emstocks?res.data.emstocks:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplistcosts',JSON.stringify(res.data.emwplistcosts?res.data.emwplistcosts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplists',JSON.stringify(res.data.emwplists?res.data.emwplists:[]));
            
            return res;
        }
        if(context.emstorepart && true){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emen',JSON.stringify(res.data.emen?res.data.emen:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqsparedetails',JSON.stringify(res.data.emeqsparedetails?res.data.emeqsparedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitempuses',JSON.stringify(res.data.emitempuses?res.data.emitempuses:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrins',JSON.stringify(res.data.emitemrins?res.data.emitemrins:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrouts',JSON.stringify(res.data.emitemrouts?res.data.emitemrouts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemtrades',JSON.stringify(res.data.emitemtrades?res.data.emitemtrades:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_empodetails',JSON.stringify(res.data.empodetails?res.data.empodetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emstocks',JSON.stringify(res.data.emstocks?res.data.emstocks:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplistcosts',JSON.stringify(res.data.emwplistcosts?res.data.emwplistcosts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplists',JSON.stringify(res.data.emwplists?res.data.emwplists:[]));
            
            return res;
        }
        if(context.emservice && true){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emen',JSON.stringify(res.data.emen?res.data.emen:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqsparedetails',JSON.stringify(res.data.emeqsparedetails?res.data.emeqsparedetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitempuses',JSON.stringify(res.data.emitempuses?res.data.emitempuses:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrins',JSON.stringify(res.data.emitemrins?res.data.emitemrins:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrouts',JSON.stringify(res.data.emitemrouts?res.data.emitemrouts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemtrades',JSON.stringify(res.data.emitemtrades?res.data.emitemtrades:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_empodetails',JSON.stringify(res.data.empodetails?res.data.empodetails:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emstocks',JSON.stringify(res.data.emstocks?res.data.emstocks:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplistcosts',JSON.stringify(res.data.emwplistcosts?res.data.emwplistcosts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplists',JSON.stringify(res.data.emwplists?res.data.emwplists:[]));
            
            return res;
        }
        let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emitems`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emen',JSON.stringify(res.data.emen?res.data.emen:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqsparedetails',JSON.stringify(res.data.emeqsparedetails?res.data.emeqsparedetails:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emitempuses',JSON.stringify(res.data.emitempuses?res.data.emitempuses:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrins',JSON.stringify(res.data.emitemrins?res.data.emitemrins:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemrouts',JSON.stringify(res.data.emitemrouts?res.data.emitemrouts:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emitemtrades',JSON.stringify(res.data.emitemtrades?res.data.emitemtrades:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_empodetails',JSON.stringify(res.data.empodetails?res.data.empodetails:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emstocks',JSON.stringify(res.data.emstocks?res.data.emstocks:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplistcosts',JSON.stringify(res.data.emwplistcosts?res.data.emwplistcosts:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emwplists',JSON.stringify(res.data.emwplists?res.data.emwplists:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstore && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstorepart && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emservice && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emitems/${context.emitem}`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}`,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstore && context.emitem){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}`,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstorepart && context.emitem){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}`,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emservice && context.emitem){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}`,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}`,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitem) delete tempData.emitem;
            if(tempData.emitemid) delete tempData.emitemid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/getdraft`,tempData,isloading);
            res.data.emitem = data.emitem;
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitem) delete tempData.emitem;
            if(tempData.emitemid) delete tempData.emitemid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/getdraft`,tempData,isloading);
            res.data.emitem = data.emitem;
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitem) delete tempData.emitem;
            if(tempData.emitemid) delete tempData.emitemid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/getdraft`,tempData,isloading);
            res.data.emitem = data.emitem;
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emitem) delete tempData.emitem;
            if(tempData.emitemid) delete tempData.emitemid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/getdraft`,tempData,isloading);
            res.data.emitem = data.emitem;
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emitem) delete tempData.emitem;
        if(tempData.emitemid) delete tempData.emitemid;
        let res:any = await  Http.getInstance().get(`/emitems/getdraft`,tempData,isloading);
        res.data.emitem = data.emitem;
                    this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/checkkey`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstore && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/checkkey`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstorepart && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/checkkey`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emservice && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/checkkey`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
            let res:any = Http.getInstance().post(`/emitems/${context.emitem}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/save`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstore && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/save`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emstorepart && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/save`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        if(context.emservice && context.emitem){
            let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/save`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
        }
        let masterData:any = {};
        let emitemprtnsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns'),'undefined')){
            emitemprtnsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_emitemprtns') as any);
            if(emitemprtnsData && emitemprtnsData.length && emitemprtnsData.length > 0){
                emitemprtnsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.emitemprtnid = null;
                            if(item.hasOwnProperty('emitemid') && item.emitemid) item.emitemid = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.emitemprtns = emitemprtnsData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emitems/${context.emitem}/save`,data,isloading);
                        this.tempStorage.setItem(context.srfsessionkey+'_emitemprtns',JSON.stringify(res.data.emitemprtns?res.data.emitemprtns:[]));

            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitems/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/searchdefault`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/searchdefault`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitems/searchdefault`,tempData,isloading);
    }

    /**
     * FetchItemTypeTree接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async FetchItemTypeTree(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/fetchitemtypetree`,tempData,isloading);
            return res;
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/fetchitemtypetree`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/fetchitemtypetree`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/fetchitemtypetree`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emitems/fetchitemtypetree`,tempData,isloading);
        return res;
    }

    /**
     * searchItemTypeTree接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMItemServiceBase
     */
    public async searchItemTypeTree(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/searchitemtypetree`,tempData,isloading);
        }
        if(context.emstore && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/searchitemtypetree`,tempData,isloading);
        }
        if(context.emstorepart && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/searchitemtypetree`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/searchitemtypetree`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emitems/searchitemtypetree`,tempData,isloading);
    }
}