import EMItemService from '@/service/emitem/emitem-service';
import { Verify } from '@/utils/verify/verify';


/**
 * 根据物品获取采购员
 *
 * @export
 * @class GetREMPLogicBase
 */
export default class GetREMPLogicBase {

    /**
     * 名称
     * 
     * @memberof  GetREMPLogicBase
     */
    private name:string ="GetREMP";

    /**
     * 唯一标识
     * 
     * @memberof  GetREMPLogicBase
     */
    private id:string = "5fb2529b96756f8b8f92743fc263b70f";

    /**
     * 默认参数名称
     * 
     * @memberof  GetREMPLogicBase
     */
    private defaultParamName:string = "Default";

    /**
     * 参数集合
     * 
     * @memberof  GetREMPLogicBase
     */
    private paramsMap:Map<string,any> = new Map();

    /**
     * Creates an instance of  GetREMPLogicBase.
     * 
     * @param {*} [opts={}]
     * @memberof  GetREMPLogicBase
     */
    constructor(opts: any = {}) {
        this.initParams(opts);
    }

    /**
     * 初始化参数集合
     * 
     * @param {*} [opts={}]
     * @memberof  GetREMPLogicBase
     */
    public initParams(opts:any){
        this.paramsMap.set('EMITEM',{});
        this.paramsMap.set('Default',opts);
    }


    /**
     * 计算0节点结果
     * 
     * @param params 传入参数
     */
    public compute0Cond(params:any):boolean{
        return true;
    }

    /**
     * 计算1节点结果
     * 
     * @param params 传入参数
     */
    public compute1Cond(params:any):boolean{
        return true;
    }

    /**
     * 计算2节点结果
     * 
     * @param params 传入参数
     */
    public compute2Cond(params:any):boolean{
        return true;
    }

    /**
     * 执行逻辑
     * 
     * @param context 应用上下文
     * @param params 传入参数
     */
    public onExecute(context:any,params:any,isloading:boolean){
        return this.executeBegin(context,params,isloading);
    }


    /**
    * 准备物品ID
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executePrepareparam1(context:any,params:any,isloading:boolean){
        // 准备参数节点
    let tempDstParam0Context:any = this.paramsMap.get('EMITEM').context?this.paramsMap.get('EMITEM').context:{};
    let tempDstParam0Data:any = this.paramsMap.get('EMITEM').data?this.paramsMap.get('EMITEM').data:{};
    let tempSrcParam0Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    Object.assign(tempDstParam0Context,{emitem:tempSrcParam0Data['itemid']});
    Object.assign(tempDstParam0Data,{emitemid:tempSrcParam0Data['itemid']});
    this.paramsMap.set('EMITEM',{data:tempDstParam0Data,context:tempDstParam0Context});
        if(this.compute2Cond(params)){
            return this.executeDeaction1(context,params,isloading);   
        }
    }

    /**
    * 开始
    * 
    * @param params 传入参数
    */
    private async executeBegin(context:any,params:any,isloading:boolean){
        //开始节点
        if(this.compute1Cond(params)){
            return this.executePrepareparam1(context,params,isloading);   
        }
    }

    /**
    * 获取物品信息
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executeDeaction1(context:any,params:any,isloading:boolean){
        // 行为处理节点
        let result: any;
        let actionParam:any = this.paramsMap.get('EMITEM');
        const targetService:EMItemService = new EMItemService();
        if (targetService['Get'] && targetService['Get'] instanceof Function) {
            result = await targetService['Get'](actionParam.context,actionParam.data, false);
        }
        if(result && result.status == 200){
            Object.assign(actionParam.data,result.data);
        if(this.compute0Cond(params)){
            return this.executePrepareparam2(context,params,isloading);   
        }
        }
    }

    /**
    * 回填采购员
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executePrepareparam2(context:any,params:any,isloading:boolean){
        // 准备参数节点
    let tempDstParam0Context:any = this.paramsMap.get('Default').context?this.paramsMap.get('Default').context:{};
    let tempDstParam0Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    let tempSrcParam0Data:any = this.paramsMap.get('EMITEM').data?this.paramsMap.get('EMITEM').data:{};
    Object.assign(tempDstParam0Data,{rempid:tempSrcParam0Data['empid']});
    this.paramsMap.set('Default',{data:tempDstParam0Data,context:tempDstParam0Context});
    let tempDstParam1Context:any = this.paramsMap.get('Default').context?this.paramsMap.get('Default').context:{};
    let tempDstParam1Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    let tempSrcParam1Data:any = this.paramsMap.get('EMITEM').data?this.paramsMap.get('EMITEM').data:{};
    Object.assign(tempDstParam1Data,{rempname:tempSrcParam1Data['empname']});
    this.paramsMap.set('Default',{data:tempDstParam1Data,context:tempDstParam1Context});
        return this.paramsMap.get(this.defaultParamName).data;
    }


}