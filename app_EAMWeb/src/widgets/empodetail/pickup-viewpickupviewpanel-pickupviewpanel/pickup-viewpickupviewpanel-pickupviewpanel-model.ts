/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'civo',
      },
      {
        name: 'rdate',
      },
      {
        name: 'yiju',
      },
      {
        name: 'isrestart',
      },
      {
        name: 'civocopy',
      },
      {
        name: 'sumdiff',
      },
      {
        name: 'createdate',
      },
      {
        name: 'pricediff',
      },
      {
        name: 'taxrate',
      },
      {
        name: 'avgtaxfee',
      },
      {
        name: 'listprice',
      },
      {
        name: 'amount',
      },
      {
        name: 'price',
      },
      {
        name: 'updateman',
      },
      {
        name: 'unitrate',
      },
      {
        name: 'empodetail',
        prop: 'empodetailid',
      },
      {
        name: 'itemdesc',
      },
      {
        name: 'shf',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'enable',
      },
      {
        name: 'totalprice',
      },
      {
        name: 'podetailstate',
      },
      {
        name: 'rsum',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'podetailinfo',
      },
      {
        name: 'orgid',
      },
      {
        name: 'sapsl',
      },
      {
        name: 'avgtsfee',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'sumall',
      },
      {
        name: 'discnt',
      },
      {
        name: 'attprice',
      },
      {
        name: 'orderflag',
      },
      {
        name: 'rprice',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'psum',
      },
      {
        name: 'empodetailname',
      },
      {
        name: 'description',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'sunitid',
      },
      {
        name: 'useto',
      },
      {
        name: 'powfstep',
      },
      {
        name: 'sunitname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'objid',
      },
      {
        name: 'equips',
      },
      {
        name: 'equipid',
      },
      {
        name: 'runitname',
      },
      {
        name: 'avgprice',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'wplistname',
      },
      {
        name: 'teamid',
      },
      {
        name: 'postate',
      },
      {
        name: 'objname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'poname',
      },
      {
        name: 'unitname',
      },
      {
        name: 'itemid',
      },
      {
        name: 'unitid',
      },
      {
        name: 'poid',
      },
      {
        name: 'runitid',
      },
      {
        name: 'wplistid',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'porempname',
      },
      {
        name: 'porempid',
      },
      {
        name: 'empid',
      },
      {
        name: 'empname',
      },
    ]
  }


}