/**
 * TreeExpViewtreeexpbar 部件模型
 *
 * @export
 * @class TreeExpViewtreeexpbarModel
 */
export default class TreeExpViewtreeexpbarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TreeExpViewtreeexpbarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'description',
      },
      {
        name: 'storecode',
      },
      {
        name: 'createdate',
      },
      {
        name: 'storeinfo',
      },
      {
        name: 'standpriceflag',
      },
      {
        name: 'poweravgflag',
      },
      {
        name: 'createman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'emstore',
        prop: 'emstoreid',
      },
      {
        name: 'newstoretypeid',
      },
      {
        name: 'costcenterid',
      },
      {
        name: 'mgrpersonid',
      },
      {
        name: 'orgid',
      },
      {
        name: 'ioalgo',
      },
      {
        name: 'storetypeid',
      },
      {
        name: 'storeaddr',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emstorename',
      },
      {
        name: 'enable',
      },
      {
        name: 'storetel',
      },
      {
        name: 'storefax',
      },
      {
        name: 'empid',
      },
      {
        name: 'empname',
      },
    ]
  }


}