/**
 * EQAhCalendar 部件模型
 *
 * @export
 * @class EQAhCalendarModel
 */
export default class EQAhCalendarModel {

	/**
	 * 日历项类型
	 *
	 * @returns {any[]}
	 * @memberof EQAhCalendarCalendarexpbar_calendarMode
	 */
	public itemType: string = "";

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof EQAhCalendarCalendarexpbar_calendarMode
	 */
	public getDataItems(): any[] {
     let dataItems: any = [
          // 前端新增修改标识，新增为"0",修改为"1"或未设值
          {
            name: 'srffrontuf',
            prop: 'srffrontuf',
            dataType: 'TEXT',
          },
          {
            name: 'color',
          },
          {
            name: 'textColor',
          },
          {
            name: 'itemType',
          },
          {
            name: 'query',
            prop: 'query',
          },
      ];
      switch(this.itemType){
          case "EMEQDEBUG":
              dataItems = [...dataItems,
                  {
                    name: 'emeqdebug',
                    prop: 'emeqdebugid'
                  },
                  {
                    name: 'title',
                    prop: 'emeqdebugname'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
          case "EMEQKEEP":
              dataItems = [...dataItems,
                  {
                    name: 'emeqkeep',
                    prop: 'emeqkeepid'
                  },
                  {
                    name: 'title',
                    prop: 'emeqkeepname'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
          case "EMEQMAINTANCE":
              dataItems = [...dataItems,
                  {
                    name: 'emeqmaintance',
                    prop: 'emeqmaintanceid'
                  },
                  {
                    name: 'title',
                    prop: 'emeqmaintancename'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
          case "EMEQSETUP":
              dataItems = [...dataItems,
                  {
                    name: 'emeqsetup',
                    prop: 'emeqsetupid'
                  },
                  {
                    name: 'title',
                    prop: 'emeqsetupname'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
          case "EMEQCHECK":
              dataItems = [...dataItems,
                  {
                    name: 'emeqcheck',
                    prop: 'emeqcheckid'
                  },
                  {
                    name: 'title',
                    prop: 'emeqcheckname'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
      }
      return dataItems;
	}

}