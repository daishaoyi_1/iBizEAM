/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'rfodecode',
      },
      {
        name: 'objid',
      },
      {
        name: 'rfodeinfo',
      },
      {
        name: 'emrfode',
        prop: 'emrfodeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emrfodename',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'updateman',
      },
    ]
  }


}