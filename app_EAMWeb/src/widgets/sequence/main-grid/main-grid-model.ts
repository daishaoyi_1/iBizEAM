/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'sequencename',
          prop: 'sequencename',
          dataType: 'TEXT',
        },
        {
          name: 'code',
          prop: 'code',
          dataType: 'TEXT',
        },
        {
          name: 'implementation',
          prop: 'implementation',
          dataType: 'TEXT',
        },
        {
          name: 'prefix',
          prop: 'prefix',
          dataType: 'TEXT',
        },
        {
          name: 'suffix',
          prop: 'suffix',
          dataType: 'TEXT',
        },
        {
          name: 'padding',
          prop: 'padding',
          dataType: 'INT',
        },
        {
          name: 'numincrement',
          prop: 'numincrement',
          dataType: 'INT',
        },
        {
          name: 'srfmajortext',
          prop: 'sequencename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'sequenceid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'sequenceid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'sequence',
          prop: 'sequenceid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}