import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * AmountByTypeByEQ 部件服务对象
 *
 * @export
 * @class AmountByTypeByEQService
 */
export default class AmountByTypeByEQService extends ControlService {
}
