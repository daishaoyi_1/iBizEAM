import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import EMResItemService from '@/service/emres-item/emres-item-service';
import UsedByItemService from './used-by-item-panel-service';
import EMResItemUIService from '@/uiservice/emres-item/emres-item-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import UsedByItemModel from './used-by-item-panel-model';
import CodeListService from "@service/app/codelist-service";

/**
 * dashboard_sysportlet4_list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {UsedByItemPanelBase}
 */
export class UsedByItemPanelBase extends PanelControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof UsedByItemPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {UsedByItemService}
     * @memberof UsedByItemPanelBase
     */
    public service: UsedByItemService = new UsedByItemService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMResItemService}
     * @memberof UsedByItemPanelBase
     */
    public appEntityService: EMResItemService = new EMResItemService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof UsedByItemPanelBase
     */
    protected appDeName: string = 'emresitem';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof UsedByItemPanelBase
     */
    protected appDeLogicName: string = '物品资源';

    /**
     * 界面UI服务对象
     *
     * @type {EMResItemUIService}
     * @memberof UsedByItemBase
     */  
    public appUIService: EMResItemUIService = new EMResItemUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof UsedByItem
     */
    public detailsModel: any = {
        bdate: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'bdate', panel: this })
,
        equipname: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'equipname', panel: this })
,
        rawitem1: new PanelRawitemModel({ caption: '使用说明', itemType: 'RAWITEM',visible: true, disabled: false, name: 'rawitem1', panel: this })
,
        amount: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'amount', panel: this })
,
        snum: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'snum', panel: this })
,
        container1: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container1', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof UsedByItem
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                






    }

    /**
     * 数据模型对象
     *
     * @type {UsedByItemModel}
     * @memberof UsedByItem
     */
    public dataModel: UsedByItemModel = new UsedByItemModel();

    /**
     * 界面行为标识数组
     *
     * @type {Array<any>}
     * @memberof UsedByItem
     */
    public actionList:Array<any> = [];

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof UsedByItem
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}