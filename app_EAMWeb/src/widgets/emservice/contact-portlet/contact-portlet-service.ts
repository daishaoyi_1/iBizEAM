import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Contact 部件服务对象
 *
 * @export
 * @class ContactService
 */
export default class ContactService extends ControlService {
}
