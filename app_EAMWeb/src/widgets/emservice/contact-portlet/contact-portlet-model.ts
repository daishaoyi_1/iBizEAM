/**
 * Contact 部件模型
 *
 * @export
 * @class ContactModel
 */
export default class ContactModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ContactModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createdate',
      },
      {
        name: 'accode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'enable',
      },
      {
        name: 'lsareaid',
      },
      {
        name: 'fax',
      },
      {
        name: 'zzyw',
      },
      {
        name: 'payway',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'serviceinfo',
      },
      {
        name: 'pgrade',
      },
      {
        name: 'accodedesc',
      },
      {
        name: 'labservicelevelid',
      },
      {
        name: 'sums',
      },
      {
        name: 'labservicetypeid',
      },
      {
        name: 'tel',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'prman',
      },
      {
        name: 'content',
      },
      {
        name: 'emservice',
        prop: 'emserviceid',
      },
      {
        name: 'qualitymana',
      },
      {
        name: 'createman',
      },
      {
        name: 'taxcode',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'zip',
      },
      {
        name: 'range',
      },
      {
        name: 'enablebz',
      },
      {
        name: 'emservicename',
      },
      {
        name: 'servicecode',
      },
      {
        name: 'paywaydesc',
      },
      {
        name: 'servicegroup',
      },
      {
        name: 'att',
      },
      {
        name: 'website',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'logo',
      },
      {
        name: 'addr',
      },
      {
        name: 'taxtypeid',
      },
      {
        name: 'taxdesc',
      },
      {
        name: 'shdate',
      },
      {
        name: 'qualifications',
      },
      {
        name: 'servicestate',
      },
    ]
  }


}
