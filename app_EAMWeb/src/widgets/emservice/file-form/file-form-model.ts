/**
 * File 部件模型
 *
 * @export
 * @class FileModel
 */
export default class FileModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof FileModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emservicename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'qualitymana',
        prop: 'qualitymana',
        dataType: 'LONGTEXT',
      },
      {
        name: 'qualifications',
        prop: 'qualifications',
        dataType: 'LONGTEXT',
      },
      {
        name: 'att',
        prop: 'att',
        dataType: 'TEXT',
      },
      {
        name: 'emserviceid',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'emservice',
        prop: 'emserviceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}