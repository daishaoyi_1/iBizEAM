import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMENConsumService from '@/service/emenconsum/emenconsum-service';
import DefaultService from './default-searchform-service';
import EMENConsumUIService from '@/uiservice/emenconsum/emenconsum-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMENConsumService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMENConsumService = new EMENConsumService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emenconsum';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '能耗';

    /**
     * 界面UI服务对象
     *
     * @type {EMENConsumUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMENConsumUIService = new EMENConsumUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_equipname_like: null,
        n_objname_like: null,
        n_enname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_equipname_like: new FormItemModel({ caption: '设备(文本包含(%))', detailType: 'FORMITEM', name: 'n_equipname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_objname_like: new FormItemModel({ caption: '位置(文本包含(%))', detailType: 'FORMITEM', name: 'n_objname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_enname_like: new FormItemModel({ caption: '能源(文本包含(%))', detailType: 'FORMITEM', name: 'n_enname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}