import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMPlanCDTService from '@/service/emplan-cdt/emplan-cdt-service';
import Main2Service from './main2-form-service';
import EMPlanCDTUIService from '@/uiservice/emplan-cdt/emplan-cdt-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main2EditFormBase}
 */
export class Main2EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main2Service}
     * @memberof Main2EditFormBase
     */
    public service: Main2Service = new Main2Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMPlanCDTService}
     * @memberof Main2EditFormBase
     */
    public appEntityService: EMPlanCDTService = new EMPlanCDTService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeName: string = 'emplancdt';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeLogicName: string = '计划条件';

    /**
     * 界面UI服务对象
     *
     * @type {EMPlanCDTUIService}
     * @memberof Main2Base
     */  
    public appUIService: EMPlanCDTUIService = new EMPlanCDTUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        equipname: null,
        objname: null,
        dpname: null,
        dptype: null,
        dpvaltype: null,
        triggerdp: null,
        triggerval: null,
        nowval: null,
        lastval: null,
        planname: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        planid: null,
        objid: null,
        emplancdtid: null,
        dpid: null,
        equipid: null,
        emplancdt: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public rules(): any{
        return {
            dpvaltype: [
                {
                    required: this.detailsModel.dpvaltype.required,
                    type: 'string',
                    message: `${this.$t('entities.emplancdt.main2_form.details.dpvaltype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.dpvaltype.required,
                    type: 'string',
                    message: `${this.$t('entities.emplancdt.main2_form.details.dpvaltype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '计划条件信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emplancdt.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel13: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel13', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emplancdt.main2_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '计划条件标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '计划条件名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dpname: new FormItemModel({
    caption: '测点', detailType: 'FORMITEM', name: 'dpname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dptype: new FormItemModel({
    caption: '测点类型', detailType: 'FORMITEM', name: 'dptype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dpvaltype: new FormItemModel({
    caption: '测点值类型', detailType: 'FORMITEM', name: 'dpvaltype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        triggerdp: new FormItemModel({
    caption: '触发操作', detailType: 'FORMITEM', name: 'triggerdp', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        triggerval: new FormItemModel({
    caption: '临界值', detailType: 'FORMITEM', name: 'triggerval', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        nowval: new FormItemModel({
    caption: '预警值', detailType: 'FORMITEM', name: 'nowval', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lastval: new FormItemModel({
    caption: '上次触发值', detailType: 'FORMITEM', name: 'lastval', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        planname: new FormItemModel({
    caption: '计划', detailType: 'FORMITEM', name: 'planname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        planid: new FormItemModel({
    caption: '计划', detailType: 'FORMITEM', name: 'planid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emplancdtid: new FormItemModel({
    caption: '计划条件标识', detailType: 'FORMITEM', name: 'emplancdtid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        dpid: new FormItemModel({
    caption: '测点', detailType: 'FORMITEM', name: 'dpid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main2Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}