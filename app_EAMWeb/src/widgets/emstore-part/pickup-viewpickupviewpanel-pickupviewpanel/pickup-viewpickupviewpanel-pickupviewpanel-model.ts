/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emstorepart',
        prop: 'emstorepartid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'emstorepartname',
      },
      {
        name: 'enable',
      },
      {
        name: 'storepartinfo',
      },
      {
        name: 'storepartcode',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createman',
      },
      {
        name: 'description',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'storename',
      },
      {
        name: 'storeid',
      },
    ]
  }


}