/**
 * TabExpViewtabviewpanel2 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel2Model
 */
export default class TabExpViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'itemroutinfo',
      },
      {
        name: 'price',
      },
      {
        name: 'createdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'sdate',
      },
      {
        name: 'enable',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'emitemcsname',
      },
      {
        name: 'emitemcs',
        prop: 'emitemcsid',
      },
      {
        name: 'batcode',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'description',
      },
      {
        name: 'amount',
      },
      {
        name: 'tradestate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'psum',
      },
      {
        name: 'orgid',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'rname',
      },
      {
        name: 'storename',
      },
      {
        name: 'itemname',
      },
      {
        name: 'stockname',
      },
      {
        name: 'storeid',
      },
      {
        name: 'rid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'stockid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'sempid',
      },
      {
        name: 'sempname',
      },
    ]
  }


}