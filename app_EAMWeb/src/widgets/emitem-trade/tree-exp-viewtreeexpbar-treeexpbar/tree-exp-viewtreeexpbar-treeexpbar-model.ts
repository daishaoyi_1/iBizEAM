/**
 * TreeExpViewtreeexpbar 部件模型
 *
 * @export
 * @class TreeExpViewtreeexpbarModel
 */
export default class TreeExpViewtreeexpbarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TreeExpViewtreeexpbarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'batcode',
      },
      {
        name: 'emitemtradename',
      },
      {
        name: 'civo',
      },
      {
        name: 'price',
      },
      {
        name: 'createdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'sdate',
      },
      {
        name: 'tradestate',
      },
      {
        name: 'pusetype',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'psum',
      },
      {
        name: 'amount',
      },
      {
        name: 'inoutflag',
      },
      {
        name: 'updateman',
      },
      {
        name: 'shf',
      },
      {
        name: 'emitemtradetype',
      },
      {
        name: 'emitemtrade',
        prop: 'emitemtradeid',
      },
      {
        name: 'itemtypegroup',
      },
      {
        name: 'enable',
      },
      {
        name: 'description',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'storename',
      },
      {
        name: 'shfprice',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'stockamount',
      },
      {
        name: 'rname',
      },
      {
        name: 'itemcode',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'teamname',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'teamid',
      },
      {
        name: 'rid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'storeid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'aempid',
      },
      {
        name: 'aempname',
      },
      {
        name: 'deptid',
      },
      {
        name: 'deptname',
      },
      {
        name: 'sempid',
      },
      {
        name: 'sempname',
      },
    ]
  }


}