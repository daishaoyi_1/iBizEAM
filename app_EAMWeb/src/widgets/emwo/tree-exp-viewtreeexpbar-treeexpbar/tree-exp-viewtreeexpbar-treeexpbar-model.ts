/**
 * TreeExpViewtreeexpbar 部件模型
 *
 * @export
 * @class TreeExpViewtreeexpbarModel
 */
export default class TreeExpViewtreeexpbarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TreeExpViewtreeexpbarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'wostate',
      },
      {
        name: 'vrate',
      },
      {
        name: 'mdate',
      },
      {
        name: 'wotype',
      },
      {
        name: 'cplanflag',
      },
      {
        name: 'waitbuy',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'description',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'prefee',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'waitmodi',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emwo',
        prop: 'emwoid',
      },
      {
        name: 'mfee',
      },
      {
        name: 'regionenddate',
      },
      {
        name: 'wogroup',
      },
      {
        name: 'worklength',
      },
      {
        name: 'enable',
      },
      {
        name: 'content',
      },
      {
        name: 'woteam',
      },
      {
        name: 'createman',
      },
      {
        name: 'expiredate',
      },
      {
        name: 'yxcb',
      },
      {
        name: 'priority',
      },
      {
        name: 'woinfo',
      },
      {
        name: 'nval',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emwoname',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'wodate',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'wresult',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'wodesc',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'val',
      },
      {
        name: 'archive',
      },
      {
        name: 'regionbegindate',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'wooriname',
      },
      {
        name: 'equipcode',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'wopname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'stype',
      },
      {
        name: 'dptype',
      },
      {
        name: 'dpname',
      },
      {
        name: 'wooritype',
      },
      {
        name: 'sname',
      },
      {
        name: 'objname',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'equipid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'wopid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'wooriid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'dpid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'objid',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'wpersonid',
      },
      {
        name: 'wpersonname',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
    ]
  }


}