/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwoid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwoname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emwoid',
        prop: 'emwoid',
        dataType: 'GUID',
      },
      {
        name: 'emwoname',
        prop: 'emwoname',
        dataType: 'TEXT',
      },
      {
        name: 'wooriname',
        prop: 'wooriname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wooritype',
        prop: 'wooritype',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'emwotype',
        prop: 'emwotype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'wogroup',
        prop: 'wogroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'wotype',
        prop: 'wotype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'wopname',
        prop: 'wopname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wodate',
        prop: 'wodate',
        dataType: 'DATETIME',
      },
      {
        name: 'expiredate',
        prop: 'expiredate',
        dataType: 'DATETIME',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'priority',
        prop: 'priority',
        dataType: 'SSCODELIST',
      },
      {
        name: 'mdate',
        prop: 'mdate',
        dataType: 'DATETIME',
      },
      {
        name: 'prefee',
        prop: 'prefee',
        dataType: 'FLOAT',
      },
      {
        name: 'wodesc',
        prop: 'wodesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'dpname',
        prop: 'dpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfodename',
        prop: 'rfodename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfomoname',
        prop: 'rfomoname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfocaname',
        prop: 'rfocaname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfoacname',
        prop: 'rfoacname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionenddate',
        prop: 'regionenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'waitbuy',
        prop: 'waitbuy',
        dataType: 'FLOAT',
      },
      {
        name: 'waitmodi',
        prop: 'waitmodi',
        dataType: 'FLOAT',
      },
      {
        name: 'worklength',
        prop: 'worklength',
        dataType: 'FLOAT',
      },
      {
        name: 'wpersonid',
        prop: 'wpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'wpersonname',
        prop: 'wpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'val',
        prop: 'val',
        dataType: 'TEXT',
      },
      {
        name: 'vrate',
        prop: 'vrate',
        dataType: 'FLOAT',
      },
      {
        name: 'wresult',
        prop: 'wresult',
        dataType: 'TEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'archive',
        prop: 'archive',
        dataType: 'SMCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'content',
        prop: 'content',
        dataType: 'HTMLTEXT',
      },
      {
        name: 'rfodeid',
        prop: 'rfodeid',
        dataType: 'PICKUP',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'wooriid',
        prop: 'wooriid',
        dataType: 'PICKUP',
      },
      {
        name: 'rfoacid',
        prop: 'rfoacid',
        dataType: 'PICKUP',
      },
      {
        name: 'rserviceid',
        prop: 'rserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'rfomoid',
        prop: 'rfomoid',
        dataType: 'PICKUP',
      },
      {
        name: 'dpid',
        prop: 'dpid',
        dataType: 'PICKUP',
      },
      {
        name: 'rfocaid',
        prop: 'rfocaid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'wopid',
        prop: 'wopid',
        dataType: 'PICKUP',
      },
      {
        name: 'rteamid',
        prop: 'rteamid',
        dataType: 'PICKUP',
      },
      {
        name: 'emwo',
        prop: 'emwoid',
        dataType: 'FONTKEY',
      },
    ]
  }

}