/**
 * Default 部件模型
 *
 * @export
 * @class DefaultModel
 */
export default class DefaultModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DefaultModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'n_emwoname_like',
        prop: 'emwoname',
        dataType: 'TEXT',
      },
      {
        name: 'n_equipname_like',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_emwotype_eq',
        prop: 'emwotype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_wooriname_like',
        prop: 'wooriname',
        dataType: 'PICKUPTEXT',
      },
    ]
  }

}