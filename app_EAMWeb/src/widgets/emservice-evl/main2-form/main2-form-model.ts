/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emserviceevlid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emserviceevlname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'servicename',
        prop: 'servicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'TEXT',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'TEXT',
      },
      {
        name: 'evlregion',
        prop: 'evlregion',
        dataType: 'SSCODELIST',
      },
      {
        name: 'evldate',
        prop: 'evldate',
        dataType: 'DATE',
      },
      {
        name: 'evlresult3',
        prop: 'evlresult3',
        dataType: 'INT',
      },
      {
        name: 'evlresult7',
        prop: 'evlresult7',
        dataType: 'INT',
      },
      {
        name: 'evlresult2',
        prop: 'evlresult2',
        dataType: 'INT',
      },
      {
        name: 'evlresult8',
        prop: 'evlresult8',
        dataType: 'INT',
      },
      {
        name: 'evlresult5',
        prop: 'evlresult5',
        dataType: 'INT',
      },
      {
        name: 'evlresult6',
        prop: 'evlresult6',
        dataType: 'INT',
      },
      {
        name: 'evlresult9',
        prop: 'evlresult9',
        dataType: 'INT',
      },
      {
        name: 'evlresult4',
        prop: 'evlresult4',
        dataType: 'INT',
      },
      {
        name: 'evlresult1',
        prop: 'evlresult1',
        dataType: 'INT',
      },
      {
        name: 'evlresult',
        prop: 'evlresult',
        dataType: 'INT',
      },
      {
        name: 'serviceevlstate',
        prop: 'serviceevlstate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'evlmark',
        prop: 'evlmark',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emserviceevlid',
        prop: 'emserviceevlid',
        dataType: 'GUID',
      },
      {
        name: 'emserviceevl',
        prop: 'emserviceevlid',
        dataType: 'FONTKEY',
      },
    ]
  }

}