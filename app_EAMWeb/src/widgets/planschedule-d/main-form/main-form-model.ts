/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'planschedule_did',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'planschedule_dname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emplanname',
        prop: 'emplanname',
        dataType: 'INHERIT',
      },
      {
        name: 'cyclestarttime',
        prop: 'cyclestarttime',
        dataType: 'INHERIT',
      },
      {
        name: 'cycleendtime',
        prop: 'cycleendtime',
        dataType: 'INHERIT',
      },
      {
        name: 'runtime',
        prop: 'runtime',
        dataType: 'INHERIT',
      },
      {
        name: 'schedulestate',
        prop: 'schedulestate',
        dataType: 'INHERIT',
      },
      {
        name: 'emplanid',
        prop: 'emplanid',
        dataType: 'INHERIT',
      },
      {
        name: 'planschedule_did',
        prop: 'planschedule_did',
        dataType: 'GUID',
      },
      {
        name: 'planschedule_d',
        prop: 'planschedule_did',
        dataType: 'FONTKEY',
      },
    ]
  }

}