/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'empoid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emponame',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'empoid',
        prop: 'empoid',
        dataType: 'GUID',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'PICKUP',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'pdate',
        prop: 'pdate',
        dataType: 'DATETIME',
      },
      {
        name: 'eadate',
        prop: 'eadate',
        dataType: 'DATETIME',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'labservicedesc',
        prop: 'labservicedesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'civo',
        prop: 'civo',
        dataType: 'TEXT',
      },
      {
        name: 'payway',
        prop: 'payway',
        dataType: 'SSCODELIST',
      },
      {
        name: 'taxivo',
        prop: 'taxivo',
        dataType: 'TEXT',
      },
      {
        name: 'taxfee',
        prop: 'taxfee',
        dataType: 'FLOAT',
      },
      {
        name: 'tsivo',
        prop: 'tsivo',
        dataType: 'TEXT',
      },
      {
        name: 'tsfee',
        prop: 'tsfee',
        dataType: 'FLOAT',
      },
      {
        name: 'poamount',
        prop: 'poamount',
        dataType: 'FLOAT',
      },
      {
        name: 'postate',
        prop: 'postate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'apprempid',
        prop: 'apprempid',
        dataType: 'PICKUP',
      },
      {
        name: 'apprempname',
        prop: 'apprempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'apprdate',
        prop: 'apprdate',
        dataType: 'DATETIME',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'content',
        prop: 'content',
        dataType: 'LONGTEXT',
      },
      {
        name: 'att',
        prop: 'att',
        dataType: 'TEXT',
      },
      {
        name: 'empo',
        prop: 'empoid',
        dataType: 'FONTKEY',
      },
    ]
  }

}