/**
 * Main4 部件模型
 *
 * @export
 * @class Main4Model
 */
export default class Main4Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main4GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main4GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emwo_enid',
          prop: 'emwo_enid',
          dataType: 'GUID',
        },
        {
          name: 'emwo_enname',
          prop: 'emwo_enname',
          dataType: 'TEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'dpname',
          prop: 'dpname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'regionbegindate',
          prop: 'regionbegindate',
          dataType: 'DATETIME',
        },
        {
          name: 'lastval',
          prop: 'lastval',
          dataType: 'FLOAT',
        },
        {
          name: 'curval',
          prop: 'curval',
          dataType: 'FLOAT',
        },
        {
          name: 'nval',
          prop: 'nval',
          dataType: 'FLOAT',
        },
        {
          name: 'bdate',
          prop: 'bdate',
          dataType: 'DATETIME',
        },
        {
          name: 'vrate',
          prop: 'vrate',
          dataType: 'FLOAT',
        },
        {
          name: 'woteam',
          prop: 'woteam',
          dataType: 'TEXT',
        },
        {
          name: 'wostate',
          prop: 'wostate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmstag',
        },
        {
          name: 'wopid',
          prop: 'wopid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emwo_enname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emwo_enid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwo_enid',
          dataType: 'GUID',
        },
        {
          name: 'mpersonid',
          prop: 'mpersonid',
          dataType: 'PICKUP',
        },
        {
          name: 'wooriid',
          prop: 'wooriid',
          dataType: 'PICKUP',
        },
        {
          name: 'rempid',
          prop: 'rempid',
          dataType: 'PICKUP',
        },
        {
          name: 'dpid',
          prop: 'dpid',
          dataType: 'PICKUP',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'rdeptid',
          prop: 'rdeptid',
          dataType: 'PICKUP',
        },
        {
          name: 'wpersonid',
          prop: 'wpersonid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'recvpersonid',
          prop: 'recvpersonid',
          dataType: 'PICKUP',
        },
        {
          name: 'emwo_en',
          prop: 'emwo_enid',
        },
      {
        name: 'n_emwo_enname_like',
        prop: 'n_emwo_enname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_equipname_like',
        prop: 'n_equipname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'n_objname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wogroup_eq',
        prop: 'n_wogroup_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_wostate_eq',
        prop: 'n_wostate_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_wooriname_like',
        prop: 'n_wooriname_like',
        dataType: 'PICKUPTEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}