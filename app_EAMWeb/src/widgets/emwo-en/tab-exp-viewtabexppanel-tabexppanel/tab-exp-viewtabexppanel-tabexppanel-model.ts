/**
 * TabExpViewtabexppanel 部件模型
 *
 * @export
 * @class TabExpViewtabexppanelModel
 */
export default class TabExpViewtabexppanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabexppanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'enable',
      },
      {
        name: 'woteam',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'regionbegindate',
      },
      {
        name: 'expiredate',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'prefee',
      },
      {
        name: 'updateman',
      },
      {
        name: 'content',
      },
      {
        name: 'priority',
      },
      {
        name: 'mdate',
      },
      {
        name: 'emwo_enname',
      },
      {
        name: 'val',
      },
      {
        name: 'regionenddate',
      },
      {
        name: 'wodate',
      },
      {
        name: 'createman',
      },
      {
        name: 'description',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'vrate',
      },
      {
        name: 'curval',
      },
      {
        name: 'wostate',
      },
      {
        name: 'worklength',
      },
      {
        name: 'wogroup',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'emwo_en',
        prop: 'emwo_enid',
      },
      {
        name: 'lastval',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'woteam_show',
      },
      {
        name: 'bdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'wotype',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'wresult',
      },
      {
        name: 'nval',
      },
      {
        name: 'archive',
      },
      {
        name: 'wodesc',
      },
      {
        name: 'orgid',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'dptype',
      },
      {
        name: 'objname',
      },
      {
        name: 'wopname_show',
      },
      {
        name: 'wopname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'dpname',
      },
      {
        name: 'wooriname',
      },
      {
        name: 'wooritype',
      },
      {
        name: 'equipname',
      },
      {
        name: 'dpid',
      },
      {
        name: 'objid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'wooriid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'wopid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'wpersonid',
      },
      {
        name: 'wpersonname',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'recvpersonname',
      },
    ]
  }


}