/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'itemcode',
        prop: 'itemcode',
        dataType: 'TEXT',
      },
      {
        name: 'emitemname',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'itemgroup',
        prop: 'itemgroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'itemtypename',
        prop: 'itemtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'acclassname',
        prop: 'acclassname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemmodelcode',
        prop: 'itemmodelcode',
        dataType: 'TEXT',
      },
      {
        name: 'itemserialcode',
        prop: 'itemserialcode',
        dataType: 'TEXT',
      },
      {
        name: 'isassetflag',
        prop: 'isassetflag',
        dataType: 'YESNO',
      },
      {
        name: 'checkmethod',
        prop: 'checkmethod',
        dataType: 'TEXT',
      },
      {
        name: 'itemdesc',
        prop: 'itemdesc',
        dataType: 'TEXT',
      },
      {
        name: 'dens',
        prop: 'dens',
        dataType: 'FLOAT',
      },
      {
        name: 'isnew',
        prop: 'isnew',
        dataType: 'SSCODELIST',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'stocksum',
        prop: 'stocksum',
        dataType: 'FLOAT',
      },
      {
        name: 'unitname',
        prop: 'unitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'lastsum',
        prop: 'lastsum',
        dataType: 'FLOAT',
      },
      {
        name: 'highsum',
        prop: 'highsum',
        dataType: 'FLOAT',
      },
      {
        name: 'repsum',
        prop: 'repsum',
        dataType: 'FLOAT',
      },
      {
        name: 'abctype',
        prop: 'abctype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'lastprice',
        prop: 'lastprice',
        dataType: 'FLOAT',
      },
      {
        name: 'stockinl',
        prop: 'stockinl',
        dataType: 'FLOAT',
      },
      {
        name: 'lastindate',
        prop: 'lastindate',
        dataType: 'DATETIME',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'PICKUP',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'lastaempid',
        prop: 'lastaempid',
        dataType: 'PICKUP',
      },
      {
        name: 'lastaempname',
        prop: 'lastaempname',
        dataType: 'TEXT',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'PICKUP',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mservicename',
        prop: 'mservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'warrantyday',
        prop: 'warrantyday',
        dataType: 'FLOAT',
      },
      {
        name: 'no3q',
        prop: 'no3q',
        dataType: 'YESNO',
      },
      {
        name: 'life',
        prop: 'life',
        dataType: 'INT',
      },
      {
        name: 'acclassid',
        prop: 'acclassid',
        dataType: 'PICKUP',
      },
      {
        name: 'storepartid',
        prop: 'storepartid',
        dataType: 'PICKUP',
      },
      {
        name: 'mserviceid',
        prop: 'mserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'storeid',
        prop: 'storeid',
        dataType: 'PICKUP',
      },
      {
        name: 'unitid',
        prop: 'unitid',
        dataType: 'PICKUP',
      },
      {
        name: 'labserviceid',
        prop: 'labserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemtypeid',
        prop: 'itemtypeid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitemid',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
        dataType: 'FONTKEY',
      },
    ]
  }

}