/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'lastprice',
      },
      {
        name: 'objid',
      },
      {
        name: 'iteminfo',
      },
      {
        name: 'isassetflag',
      },
      {
        name: 'highsum',
      },
      {
        name: 'createdate',
      },
      {
        name: 'amount',
      },
      {
        name: 'isbatchflag',
      },
      {
        name: 'checkmethod',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
      },
      {
        name: 'stockdesum',
      },
      {
        name: 'abctype',
      },
      {
        name: 'shfprice',
      },
      {
        name: 'repsum',
      },
      {
        name: 'updateman',
      },
      {
        name: 'enable',
      },
      {
        name: 'stockextime',
      },
      {
        name: 'isnew',
      },
      {
        name: 'itemnid',
      },
      {
        name: 'itemserialcode',
      },
      {
        name: 'registerdat',
      },
      {
        name: 'itemmodelcode',
      },
      {
        name: 'itemncode',
      },
      {
        name: 'itemcode',
      },
      {
        name: 'no3q',
      },
      {
        name: 'emitemname',
      },
      {
        name: 'dens',
      },
      {
        name: 'sapcontrol',
      },
      {
        name: 'batchtype',
      },
      {
        name: 'itemgroup',
      },
      {
        name: 'lastsum',
      },
      {
        name: 'stocksum',
      },
      {
        name: 'createman',
      },
      {
        name: 'lastindate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'sapcontrolcode',
      },
      {
        name: 'life',
      },
      {
        name: 'price',
      },
      {
        name: 'costcenterid',
      },
      {
        name: 'stockinl',
      },
      {
        name: 'itemdesc',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'warrantyday',
      },
      {
        name: 'storename',
      },
      {
        name: 'unitname',
      },
      {
        name: 'emcabname',
      },
      {
        name: 'storecode',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'mservicename',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'itemtypecode',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'itemtypename',
      },
      {
        name: 'emcabid',
      },
      {
        name: 'unitid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'mserviceid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'storeid',
      },
      {
        name: 'empid',
      },
      {
        name: 'empname',
      },
      {
        name: 'sempid',
      },
      {
        name: 'sempname',
      },
      {
        name: 'lastaempid',
      },
      {
        name: 'lastaempname',
      },
    ]
  }


}