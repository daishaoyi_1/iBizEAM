import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMItemService from '@/service/emitem/emitem-service';
import DefaultService from './default-searchform-service';
import EMItemUIService from '@/uiservice/emitem/emitem-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMItemService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMItemService = new EMItemService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emitem';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '物品';

    /**
     * 界面UI服务对象
     *
     * @type {EMItemUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMItemUIService = new EMItemUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_emitemname_like: null,
        n_itemtypename_like: null,
        n_labservicename_like: null,
        n_mservicename_like: null,
        n_storeid_eq: null,
        n_storepartid_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_emitemname_like: new FormItemModel({ caption: '物品名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_emitemname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_itemtypename_like: new FormItemModel({ caption: '物品类型(文本包含(%))', detailType: 'FORMITEM', name: 'n_itemtypename_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_labservicename_like: new FormItemModel({ caption: '建议供应商(文本包含(%))', detailType: 'FORMITEM', name: 'n_labservicename_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_mservicename_like: new FormItemModel({ caption: '制造商(文本包含(%))', detailType: 'FORMITEM', name: 'n_mservicename_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_storeid_eq: new FormItemModel({ caption: '最新存储仓库(等于(=))', detailType: 'FORMITEM', name: 'n_storeid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_storepartid_eq: new FormItemModel({ caption: '最新存储库位(等于(=))', detailType: 'FORMITEM', name: 'n_storepartid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}