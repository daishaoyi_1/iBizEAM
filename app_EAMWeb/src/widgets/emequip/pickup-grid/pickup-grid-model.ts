/**
 * Pickup 部件模型
 *
 * @export
 * @class PickupModel
 */
export default class PickupModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof PickupGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof PickupGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'equipcode',
          prop: 'equipcode',
          dataType: 'TEXT',
        },
        {
          name: 'emequipname',
          prop: 'emequipname',
          dataType: 'TEXT',
        },
        {
          name: 'eqtypename',
          prop: 'eqtypename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqlocationname',
          prop: 'eqlocationname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqstate',
          prop: 'eqstate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'deptname',
          prop: 'deptname',
          dataType: 'TEXT',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'embrandid',
          prop: 'embrandid',
          dataType: 'PICKUP',
        },
        {
          name: 'contractid',
          prop: 'contractid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'emmachinecategoryid',
          prop: 'emmachinecategoryid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emequipname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emequipid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emequipid',
          dataType: 'GUID',
        },
        {
          name: 'emberthid',
          prop: 'emberthid',
          dataType: 'PICKUP',
        },
        {
          name: 'mserviceid',
          prop: 'mserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqlocationid',
          prop: 'eqlocationid',
          dataType: 'PICKUP',
        },
        {
          name: 'equippid',
          prop: 'equippid',
          dataType: 'PICKUP',
        },
        {
          name: 'assetid',
          prop: 'assetid',
          dataType: 'PICKUP',
        },
        {
          name: 'emmachmodelid',
          prop: 'emmachmodelid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqtypeid',
          prop: 'eqtypeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'emequip',
          prop: 'emequipid',
        },
      {
        name: 'n_equipcode_like',
        prop: 'n_equipcode_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_emequipname_like',
        prop: 'n_emequipname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_eqlocationname_eq',
        prop: 'n_eqlocationname_eq',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_equipgroup_eq',
        prop: 'n_equipgroup_eq',
        dataType: 'NMCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}