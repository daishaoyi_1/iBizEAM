/**
 * ItemType 部件模型
 *
 * @export
 * @class ItemTypeModel
 */
export default class ItemTypeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ItemTypeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emitemtype',
        prop: 'emitemtypeid',
      },
      {
        name: 'description',
      },
      {
        name: 'itemtypecode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'createman',
      },
      {
        name: 'emitemtypename',
      },
      {
        name: 'updateman',
      },
      {
        name: 'itemtypeinfo',
      },
      {
        name: 'createdate',
      },
      {
        name: 'itemtypepcode',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'itemtypepname',
      },
      {
        name: 'itemtypepid',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'itembtypeid',
      },
    ]
  }


}