import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * InfoTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class InfoTreeExpViewtreeexpbarService
 */
export default class InfoTreeExpViewtreeexpbarService extends ControlService {
}