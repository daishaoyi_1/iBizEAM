/**
 * GridView9 部件模型
 *
 * @export
 * @class GridView9Model
 */
export default class GridView9Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof GridView9Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emeqsparemap',
        prop: 'emeqsparemapid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'enable',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emeqsparemapname',
      },
      {
        name: 'refobjname',
      },
      {
        name: 'eqsparename',
      },
      {
        name: 'refobjid',
      },
      {
        name: 'eqspareid',
      },
    ]
  }


}
