/**
 * Main4 部件模型
 *
 * @export
 * @class Main4Model
 */
export default class Main4Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main4Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwo_dpid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwo_dpname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emwo_dpid',
        prop: 'emwo_dpid',
        dataType: 'GUID',
      },
      {
        name: 'emwo_dpname',
        prop: 'emwo_dpname',
        dataType: 'TEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wodate',
        prop: 'wodate',
        dataType: 'DATETIME',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'val',
        prop: 'val',
        dataType: 'TEXT',
      },
      {
        name: 'wpersonid',
        prop: 'wpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'wpersonname',
        prop: 'wpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'dpname',
        prop: 'dpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'PICKUP',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rdeptid',
        prop: 'rdeptid',
        dataType: 'PICKUP',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emwo_dp',
        prop: 'emwo_dpid',
        dataType: 'FONTKEY',
      },
    ]
  }

}