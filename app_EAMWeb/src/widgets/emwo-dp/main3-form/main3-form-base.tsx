import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMWO_DPService from '@/service/emwo-dp/emwo-dp-service';
import Main3Service from './main3-form-service';
import EMWO_DPUIService from '@/uiservice/emwo-dp/emwo-dp-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main3EditFormBase}
 */
export class Main3EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main3Service}
     * @memberof Main3EditFormBase
     */
    public service: Main3Service = new Main3Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWO_DPService}
     * @memberof Main3EditFormBase
     */
    public appEntityService: EMWO_DPService = new EMWO_DPService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeName: string = 'emwo_dp';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeLogicName: string = '点检工单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWO_DPUIService}
     * @memberof Main3Base
     */  
    public appUIService: EMWO_DPUIService = new EMWO_DPUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main3EditFormBase
     */
    protected formKeyItemName: string = 'emwo_dpid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        emwo_dpid: null,
        emwo_dpname: null,
        equipname: null,
        objid: null,
        objname: null,
        wodate: null,
        activelengths: null,
        dpname: null,
        wpersonid: null,
        wpersonname: null,
        regionbegindate: null,
        val: null,
        rempid: null,
        rempname: null,
        rdeptid: null,
        rdeptname: null,
        mpersonid: null,
        mpersonname: null,
        recvpersonid: null,
        recvpersonname: null,
        wogroup: null,
        wopname: null,
        wooriname: null,
        wooritype: null,
        orgid: null,
        archive: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        wooriid: null,
        dpid: null,
        equipid: null,
        wopid: null,
        emwo_dp: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public majorMessageField: string = 'emwo_dpname';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public rules(): any{
        return {
            emwo_dpname: [
                {
                    required: this.detailsModel.emwo_dpname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_dp.main3_form.details.emwo_dpname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.emwo_dpname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_dp.main3_form.details.emwo_dpname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            wodate: [
                {
                    required: this.detailsModel.wodate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_dp.main3_form.details.wodate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wodate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_dp.main3_form.details.wodate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            wogroup: [
                {
                    required: this.detailsModel.wogroup.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_dp.main3_form.details.wogroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wogroup.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_dp.main3_form.details.wogroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '工单信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_dp.main3_form', extractMode: 'ITEM', details: [] } }),

        grouppanel9: new FormGroupPanelModel({ caption: '执行信息', detailType: 'GROUPPANEL', name: 'grouppanel9', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_dp.main3_form', extractMode: 'ITEM', details: [] } }),

        grouppanel14: new FormGroupPanelModel({ caption: '责任信息', detailType: 'GROUPPANEL', name: 'grouppanel14', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_dp.main3_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel20: new FormGroupPanelModel({ caption: '高级信息', detailType: 'GROUPPANEL', name: 'grouppanel20', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_dp.main3_form', extractMode: 'ITEM', details: [] } }),

        grouppanel25: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel25', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_dp.main3_form', extractMode: 'ITEM', details: [] } }),

        formpage19: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage19', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '工单编号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emwo_dpid: new FormItemModel({
    caption: '工单编号(自动)', detailType: 'FORMITEM', name: 'emwo_dpid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        emwo_dpname: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'emwo_dpname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wodate: new FormItemModel({
    caption: '安排日期', detailType: 'FORMITEM', name: 'wodate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        activelengths: new FormItemModel({
    caption: '持续时间(H)', detailType: 'FORMITEM', name: 'activelengths', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dpname: new FormItemModel({
    caption: '测点', detailType: 'FORMITEM', name: 'dpname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonid: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonname: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionbegindate: new FormItemModel({
    caption: '实际点检时间', detailType: 'FORMITEM', name: 'regionbegindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        val: new FormItemModel({
    caption: '点检值', detailType: 'FORMITEM', name: 'val', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptid: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptname: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonid: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonname: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonid: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonname: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wogroup: new FormItemModel({
    caption: '工单分组', detailType: 'FORMITEM', name: 'wogroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 1,
}),

        wopname: new FormItemModel({
    caption: '上级工单', detailType: 'FORMITEM', name: 'wopname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooriname: new FormItemModel({
    caption: '工单来源', detailType: 'FORMITEM', name: 'wooriname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooritype: new FormItemModel({
    caption: '来源类型', detailType: 'FORMITEM', name: 'wooritype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        archive: new FormItemModel({
    caption: '归档', detailType: 'FORMITEM', name: 'archive', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        wooriid: new FormItemModel({
    caption: '工单来源', detailType: 'FORMITEM', name: 'wooriid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dpid: new FormItemModel({
    caption: '测点', detailType: 'FORMITEM', name: 'dpid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wopid: new FormItemModel({
    caption: '上级工单', detailType: 'FORMITEM', name: 'wopid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage19',
                    index: 1,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof Main3EditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string; newVal: any; oldVal: any }): Promise<void> {
                


















































        if (Object.is(name, 'equipname')) {
            const details: string[] = ['rdeptid', 'objname', 'objid', 'rdeptname'];
            this.updateFormItems('FormUpdateByEmquipId', this.data, details, true);
        }
    }

    /**
     * 新建默认值
     * @memberof Main3EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('activelengths')) {
            this.data['activelengths'] = 8;
        }
        if (this.data.hasOwnProperty('wogroup')) {
            this.data['wogroup'] = 'TASK';
        }
        if (this.data.hasOwnProperty('orgid')) {
            this.data['orgid'] = 'TOP';
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main3Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}