import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMWO_INNERService from '@/service/emwo-inner/emwo-inner-service';
import Main3Service from './main3-form-service';
import EMWO_INNERUIService from '@/uiservice/emwo-inner/emwo-inner-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main3EditFormBase}
 */
export class Main3EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main3Service}
     * @memberof Main3EditFormBase
     */
    public service: Main3Service = new Main3Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWO_INNERService}
     * @memberof Main3EditFormBase
     */
    public appEntityService: EMWO_INNERService = new EMWO_INNERService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeName: string = 'emwo_inner';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeLogicName: string = '内部工单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWO_INNERUIService}
     * @memberof Main3Base
     */  
    public appUIService: EMWO_INNERUIService = new EMWO_INNERUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main3EditFormBase
     */
    protected formKeyItemName: string = 'emwo_innerid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        equipid: null,
        equipname: null,
        objid: null,
        objname: null,
        emwo_innername: null,
        emwo_innerid: null,
        wogroup: null,
        activelengths: null,
        wotype: null,
        emeqlcttiresname: null,
        closeflag: null,
        emeqlctgssname: null,
        emeitiresname: null,
        priority: null,
        yxcb: null,
        archive: null,
        wodesc: null,
        wodate: null,
        regionbegindate: null,
        regionenddate: null,
        eqstoplength: null,
        waitmodi: null,
        waitbuy: null,
        worklength: null,
        wresult: null,
        mfee: null,
        cplanflag: null,
        rempid: null,
        rempname: null,
        recvpersonid: null,
        recvpersonname: null,
        mpersonid: null,
        mpersonname: null,
        wpersonid: null,
        wpersonname: null,
        rdeptid: null,
        rdeptname: null,
        rteamname: null,
        rteamid: null,
        rfodename: null,
        rfomoname: null,
        rfocaname: null,
        rfoacname: null,
        wooriname: null,
        wooritype: null,
        wopname: null,
        prefee: null,
        mdate: null,
        expiredate: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        emeqlctgssid: null,
        rfodeid: null,
        wooriid: null,
        emeitiresid: null,
        rfoacid: null,
        rfomoid: null,
        emeqlcttiresid: null,
        rfocaid: null,
        wopid: null,
        emwo_inner: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public majorMessageField: string = 'emwo_innername';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public rules(): any{
        return {
            emwo_innername: [
                {
                    required: this.detailsModel.emwo_innername.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.emwo_innername')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.emwo_innername.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.emwo_innername')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            wogroup: [
                {
                    required: this.detailsModel.wogroup.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.wogroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wogroup.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.wogroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            wodate: [
                {
                    required: this.detailsModel.wodate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.wodate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wodate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.wodate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            rdeptname: [
                {
                    required: this.detailsModel.rdeptname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.rdeptname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.rdeptname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo_inner.main3_form.details.rdeptname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '工单信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_inner.main3_form', extractMode: 'ITEM', details: [] } }),

        grouppanel18: new FormGroupPanelModel({ caption: '执行信息', detailType: 'GROUPPANEL', name: 'grouppanel18', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_inner.main3_form', extractMode: 'ITEM', details: [] } }),

        grouppanel30: new FormGroupPanelModel({ caption: '责任信息', detailType: 'GROUPPANEL', name: 'grouppanel30', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_inner.main3_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel37: new FormGroupPanelModel({ caption: '故障信息', detailType: 'GROUPPANEL', name: 'grouppanel37', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_inner.main3_form', extractMode: 'ITEM', details: [] } }),

        grouppanel42: new FormGroupPanelModel({ caption: '高级信息', detailType: 'GROUPPANEL', name: 'grouppanel42', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_inner.main3_form', extractMode: 'ITEM', details: [] } }),

        grouppanel49: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel49', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_inner.main3_form', extractMode: 'ITEM', details: [] } }),

        formpage36: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage36', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '工单编号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emwo_innername: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'emwo_innername', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        emwo_innerid: new FormItemModel({
    caption: '工单编号(自动)', detailType: 'FORMITEM', name: 'emwo_innerid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        wogroup: new FormItemModel({
    caption: '工单分组', detailType: 'FORMITEM', name: 'wogroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 1,
}),

        activelengths: new FormItemModel({
    caption: '安排工时(H)', detailType: 'FORMITEM', name: 'activelengths', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wotype: new FormItemModel({
    caption: '工单类型', detailType: 'FORMITEM', name: 'wotype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeqlcttiresname: new FormItemModel({
    caption: '轮胎位置', detailType: 'FORMITEM', name: 'emeqlcttiresname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        closeflag: new FormItemModel({
    caption: '直接完成', detailType: 'FORMITEM', name: 'closeflag', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeqlctgssname: new FormItemModel({
    caption: '钢丝绳位置', detailType: 'FORMITEM', name: 'emeqlctgssname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeitiresname: new FormItemModel({
    caption: '轮胎清单', detailType: 'FORMITEM', name: 'emeitiresname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        priority: new FormItemModel({
    caption: '优先级', detailType: 'FORMITEM', name: 'priority', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        yxcb: new FormItemModel({
    caption: '影响船舶', detailType: 'FORMITEM', name: 'yxcb', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        archive: new FormItemModel({
    caption: '归档', detailType: 'FORMITEM', name: 'archive', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wodesc: new FormItemModel({
    caption: '工单内容', detailType: 'FORMITEM', name: 'wodesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wodate: new FormItemModel({
    caption: '安排执行日期', detailType: 'FORMITEM', name: 'wodate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        regionbegindate: new FormItemModel({
    caption: '起始时间', detailType: 'FORMITEM', name: 'regionbegindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionenddate: new FormItemModel({
    caption: '结束时间', detailType: 'FORMITEM', name: 'regionenddate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqstoplength: new FormItemModel({
    caption: '停运时间(分)', detailType: 'FORMITEM', name: 'eqstoplength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        waitmodi: new FormItemModel({
    caption: '等待修理时(分)', detailType: 'FORMITEM', name: 'waitmodi', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        waitbuy: new FormItemModel({
    caption: '等待配件时(分)', detailType: 'FORMITEM', name: 'waitbuy', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        worklength: new FormItemModel({
    caption: '实际工时(分)', detailType: 'FORMITEM', name: 'worklength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wresult: new FormItemModel({
    caption: '执行结果', detailType: 'FORMITEM', name: 'wresult', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mfee: new FormItemModel({
    caption: '约合材料费(￥)', detailType: 'FORMITEM', name: 'mfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        cplanflag: new FormItemModel({
    caption: '转计划标志', detailType: 'FORMITEM', name: 'cplanflag', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonid: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonname: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonid: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonname: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonid: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonname: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptid: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptname: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        rteamname: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rteamid: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfodename: new FormItemModel({
    caption: '现象', detailType: 'FORMITEM', name: 'rfodename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfomoname: new FormItemModel({
    caption: '模式', detailType: 'FORMITEM', name: 'rfomoname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfocaname: new FormItemModel({
    caption: '原因', detailType: 'FORMITEM', name: 'rfocaname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfoacname: new FormItemModel({
    caption: '方案', detailType: 'FORMITEM', name: 'rfoacname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooriname: new FormItemModel({
    caption: '工单来源', detailType: 'FORMITEM', name: 'wooriname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooritype: new FormItemModel({
    caption: '来源类型', detailType: 'FORMITEM', name: 'wooritype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wopname: new FormItemModel({
    caption: '上级工单', detailType: 'FORMITEM', name: 'wopname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        prefee: new FormItemModel({
    caption: '预算(￥)', detailType: 'FORMITEM', name: 'prefee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mdate: new FormItemModel({
    caption: '制定时间', detailType: 'FORMITEM', name: 'mdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        expiredate: new FormItemModel({
    caption: '过期日期', detailType: 'FORMITEM', name: 'expiredate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        emeqlctgssid: new FormItemModel({
    caption: '钢丝绳位置', detailType: 'FORMITEM', name: 'emeqlctgssid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfodeid: new FormItemModel({
    caption: '现象', detailType: 'FORMITEM', name: 'rfodeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooriid: new FormItemModel({
    caption: '工单来源', detailType: 'FORMITEM', name: 'wooriid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeitiresid: new FormItemModel({
    caption: '轮胎清单', detailType: 'FORMITEM', name: 'emeitiresid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfoacid: new FormItemModel({
    caption: '方案', detailType: 'FORMITEM', name: 'rfoacid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfomoid: new FormItemModel({
    caption: '模式', detailType: 'FORMITEM', name: 'rfomoid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeqlcttiresid: new FormItemModel({
    caption: '轮胎位置', detailType: 'FORMITEM', name: 'emeqlcttiresid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfocaid: new FormItemModel({
    caption: '原因', detailType: 'FORMITEM', name: 'rfocaid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wopid: new FormItemModel({
    caption: '上级工单', detailType: 'FORMITEM', name: 'wopid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage36',
                    index: 1,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof Main3EditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string; newVal: any; oldVal: any }): Promise<void> {
                
















































































        if (Object.is(name, 'equipname')) {
            const details: string[] = ['rempid', 'equipid', 'rdeptname', 'equipname', 'rdeptid', 'objname', 'objid', 'rteamname'];
            this.updateFormItems('FormUpdateByEmequipid', this.data, details, true);
        }
    }

    /**
     * 新建默认值
     * @memberof Main3EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('wogroup')) {
            this.data['wogroup'] = 'TASK';
        }
        if (this.data.hasOwnProperty('activelengths')) {
            this.data['activelengths'] = 8;
        }
        if (this.data.hasOwnProperty('priority')) {
            this.data['priority'] = '2';
        }
        if (this.data.hasOwnProperty('wodate')) {
            this.data['wodate'] = this.$util.dateFormat(new Date());
        }
        if (this.data.hasOwnProperty('worklength')) {
            this.data['worklength'] = 480;
        }
        if (this.data.hasOwnProperty('mpersonname')) {
            this.data['mpersonname'] = this.context['srfusername'];
        }
        if (this.data.hasOwnProperty('orgid')) {
            this.data['orgid'] = 'TOP';
        }
    }

    /**
     * 更新默认值
     * @memberof Main3EditFormBase
     */
    public updateDefault() {                    
        if (this.data.hasOwnProperty('mpersonname') && !this.data.mpersonname) {
            this.data['mpersonname'] = this.context['srfusername'];
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main3Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}