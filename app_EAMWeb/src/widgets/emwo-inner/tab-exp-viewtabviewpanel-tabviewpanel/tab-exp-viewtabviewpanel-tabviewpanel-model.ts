/**
 * TabExpViewtabviewpanel 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanelModel
 */
export default class TabExpViewtabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'content',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'emwo_inner',
        prop: 'emwo_innerid',
      },
      {
        name: 'worklength',
      },
      {
        name: 'wogroup',
      },
      {
        name: 'wotype',
      },
      {
        name: 'waitbuy',
      },
      {
        name: 'wostate',
      },
      {
        name: 'val',
      },
      {
        name: 'waitmodi',
      },
      {
        name: 'expiredate',
      },
      {
        name: 'createman',
      },
      {
        name: 'closeflag',
      },
      {
        name: 'mdate',
      },
      {
        name: 'wodesc',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'priority',
      },
      {
        name: 'yxcb',
      },
      {
        name: 'nval',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'emwo_innername',
      },
      {
        name: 'wodate',
      },
      {
        name: 'archive',
      },
      {
        name: 'regionenddate',
      },
      {
        name: 'cplanflag',
      },
      {
        name: 'woteam',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'enable',
      },
      {
        name: 'description',
      },
      {
        name: 'prefee',
      },
      {
        name: 'regionbegindate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'vrate',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'wresult',
      },
      {
        name: 'mfee',
      },
      {
        name: 'updateman',
      },
      {
        name: 'dpname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'emeqlctgssname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'wopname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'emeitiresname',
      },
      {
        name: 'objname',
      },
      {
        name: 'wooriname',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'dptype',
      },
      {
        name: 'emeqlcttiresname',
      },
      {
        name: 'wooritype',
      },
      {
        name: 'objid',
      },
      {
        name: 'wooriid',
      },
      {
        name: 'emeqlctgssid',
      },
      {
        name: 'emeitiresid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'emeqlcttiresid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'dpid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'wopid',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'wpersonid',
      },
      {
        name: 'wpersonname',
      },
    ]
  }


}