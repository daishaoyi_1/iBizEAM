/**
 * CostByItem 部件模型
 *
 * @export
 * @class CostByItemModel
 */
export default class CostByItemModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof CostByItemModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'adate',
        prop: 'adate'
      },
      {
        name: 'labservicename',
        prop: 'labservicename'
      },
      {
        name: 'price',
        prop: 'price'
      }
    ]
  }
}