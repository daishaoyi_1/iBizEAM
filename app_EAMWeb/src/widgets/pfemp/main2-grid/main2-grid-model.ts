/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'empcode',
          prop: 'empcode',
          dataType: 'TEXT',
        },
        {
          name: 'pfempname',
          prop: 'pfempname',
          dataType: 'TEXT',
        },
        {
          name: 'majorteamname',
          prop: 'majorteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'certcode',
          prop: 'certcode',
          dataType: 'TEXT',
        },
        {
          name: 'empsex',
          prop: 'empsex',
          dataType: 'SSCODELIST',
        },
        {
          name: 'birthday',
          prop: 'birthday',
          dataType: 'DATE',
        },
        {
          name: 'tel',
          prop: 'tel',
          dataType: 'TEXT',
        },
        {
          name: 'cell',
          prop: 'cell',
          dataType: 'TEXT',
        },
        {
          name: 'addr',
          prop: 'addr',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'e_mail',
          prop: 'e_mail',
          dataType: 'TEXT',
        },
        {
          name: 'orgid',
          prop: 'orgid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'workdate',
          prop: 'workdate',
          dataType: 'DATE',
        },
        {
          name: 'raisedate',
          prop: 'raisedate',
          dataType: 'DATE',
        },
        {
          name: 'hometel',
          prop: 'hometel',
          dataType: 'TEXT',
        },
        {
          name: 'homeaddr',
          prop: 'homeaddr',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'majordeptid',
          prop: 'majordeptid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'pfempname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'pfempid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'pfempid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'majorteamid',
          prop: 'majorteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'pfemp',
          prop: 'pfempid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}