/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'pfempid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'pfempname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'pfempid',
        prop: 'pfempid',
        dataType: 'GUID',
      },
      {
        name: 'empcode',
        prop: 'empcode',
        dataType: 'TEXT',
      },
      {
        name: 'pfempname',
        prop: 'pfempname',
        dataType: 'TEXT',
      },
      {
        name: 'psw',
        prop: 'psw',
        dataType: 'TEXT',
      },
      {
        name: 'empsex',
        prop: 'empsex',
        dataType: 'SSCODELIST',
      },
      {
        name: 'certcode',
        prop: 'certcode',
        dataType: 'TEXT',
      },
      {
        name: 'birthday',
        prop: 'birthday',
        dataType: 'DATE',
      },
      {
        name: 'workdate',
        prop: 'workdate',
        dataType: 'DATE',
      },
      {
        name: 'raisedate',
        prop: 'raisedate',
        dataType: 'DATE',
      },
      {
        name: 'hometel',
        prop: 'hometel',
        dataType: 'TEXT',
      },
      {
        name: 'tel',
        prop: 'tel',
        dataType: 'TEXT',
      },
      {
        name: 'cell',
        prop: 'cell',
        dataType: 'TEXT',
      },
      {
        name: 'majorteamid',
        prop: 'majorteamid',
        dataType: 'PICKUP',
      },
      {
        name: 'majorteamname',
        prop: 'majorteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'majordeptid',
        prop: 'majordeptid',
        dataType: 'PICKUP',
      },
      {
        name: 'majordeptname',
        prop: 'majordeptname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'e_mail',
        prop: 'e_mail',
        dataType: 'TEXT',
      },
      {
        name: 'addr',
        prop: 'addr',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'homeaddr',
        prop: 'homeaddr',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'pfemp',
        prop: 'pfempid',
        dataType: 'FONTKEY',
      },
    ]
  }

}