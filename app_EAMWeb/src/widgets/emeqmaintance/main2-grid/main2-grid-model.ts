/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'woname',
          prop: 'woname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'activedate',
          prop: 'activedate',
          dataType: 'DATE',
        },
        {
          name: 'activedesc',
          prop: 'activedesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'regionbegindate',
          prop: 'regionbegindate',
          dataType: 'DATETIME',
        },
        {
          name: 'regionenddate',
          prop: 'regionenddate',
          dataType: 'DATETIME',
        },
        {
          name: 'activeadesc',
          prop: 'activeadesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'rdeptname',
          prop: 'rdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'rteamname',
          prop: 'rteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfodename',
          prop: 'rfodename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfomoname',
          prop: 'rfomoname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfocaname',
          prop: 'rfocaname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfoacname',
          prop: 'rfoacname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqstoplength',
          prop: 'eqstoplength',
          dataType: 'FLOAT',
        },
        {
          name: 'activelengths',
          prop: 'activelengths',
          dataType: 'FLOAT',
        },
        {
          name: 'prefee',
          prop: 'prefee',
          dataType: 'FLOAT',
        },
        {
          name: 'mfee',
          prop: 'mfee',
          dataType: 'FLOAT',
        },
        {
          name: 'pfee',
          prop: 'pfee',
          dataType: 'FLOAT',
        },
        {
          name: 'sfee',
          prop: 'sfee',
          dataType: 'FLOAT',
        },
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqmaintancename',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emeqmaintanceid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqmaintanceid',
          dataType: 'GUID',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'woid',
          prop: 'woid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'emeqmaintance',
          prop: 'emeqmaintanceid',
        },
      {
        name: 'n_activedesc_like',
        prop: 'n_activedesc_like',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'n_equipname_like',
        prop: 'n_equipname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'n_objname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_woname_like',
        prop: 'n_woname_like',
        dataType: 'PICKUPTEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}