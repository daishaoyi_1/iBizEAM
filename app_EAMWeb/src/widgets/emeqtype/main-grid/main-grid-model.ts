/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'eqtypecode',
          prop: 'eqtypecode',
          dataType: 'TEXT',
        },
        {
          name: 'emeqtypename',
          prop: 'emeqtypename',
          dataType: 'TEXT',
        },
        {
          name: 'eqtypepname',
          prop: 'eqtypepname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqtypegroup',
          prop: 'eqtypegroup',
          dataType: 'SSCODELIST',
        },
        {
          name: 'eqtypepid',
          prop: 'eqtypepid',
          dataType: 'PICKUP',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqtypename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqtypeid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emeqtypeid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emeqtype',
          prop: 'emeqtypeid',
        },
      {
        name: 'n_eqtypecode_like',
        prop: 'n_eqtypecode_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_emeqtypename_like',
        prop: 'n_emeqtypename_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_eqtypepid_eq',
        prop: 'n_eqtypepid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_eqtypegroup_eq',
        prop: 'n_eqtypegroup_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}