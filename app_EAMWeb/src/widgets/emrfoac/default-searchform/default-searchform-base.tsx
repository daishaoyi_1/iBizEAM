import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMRFOACService from '@/service/emrfoac/emrfoac-service';
import DefaultService from './default-searchform-service';
import EMRFOACUIService from '@/uiservice/emrfoac/emrfoac-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMRFOACService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMRFOACService = new EMRFOACService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emrfoac';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '方案';

    /**
     * 界面UI服务对象
     *
     * @type {EMRFOACUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMRFOACUIService = new EMRFOACUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_emrfoacname_like: null,
        n_rfodenane_like: null,
        n_rfomoname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_emrfoacname_like: new FormItemModel({ caption: '方案名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_emrfoacname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_rfodenane_like: new FormItemModel({ caption: '现象(文本包含(%))', detailType: 'FORMITEM', name: 'n_rfodenane_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_rfomoname_like: new FormItemModel({ caption: '模式(文本包含(%))', detailType: 'FORMITEM', name: 'n_rfomoname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}