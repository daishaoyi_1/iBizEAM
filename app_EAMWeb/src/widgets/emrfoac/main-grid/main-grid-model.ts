/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'rfoacinfo',
          prop: 'rfoacinfo',
          dataType: 'TEXT',
        },
        {
          name: 'updateman',
          prop: 'updateman',
          dataType: 'TEXT',
        },
        {
          name: 'updatedate',
          prop: 'updatedate',
          dataType: 'DATETIME',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emrfoacname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emrfoacid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emrfoacid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emrfoac',
          prop: 'emrfoacid',
        },
      {
        name: 'n_emrfoacname_like',
        prop: 'n_emrfoacname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_rfodenane_like',
        prop: 'n_rfodenane_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_rfomoname_like',
        prop: 'n_rfomoname_like',
        dataType: 'PICKUPTEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}