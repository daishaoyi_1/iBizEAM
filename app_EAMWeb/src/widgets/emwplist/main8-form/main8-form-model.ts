/**
 * Main8 部件模型
 *
 * @export
 * @class Main8Model
 */
export default class Main8Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main8Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwplistid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwplistname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emwplistid',
        prop: 'emwplistid',
        dataType: 'GUID',
      },
      {
        name: 'wplisttype',
        prop: 'wplisttype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'adate',
        prop: 'adate',
        dataType: 'DATETIME',
      },
      {
        name: 'useto',
        prop: 'useto',
        dataType: 'SSCODELIST',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equips',
        prop: 'equips',
        dataType: 'TEXT',
      },
      {
        name: 'teamname',
        prop: 'teamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'hdate',
        prop: 'hdate',
        dataType: 'DATETIME',
      },
      {
        name: 'wpliststate',
        prop: 'wpliststate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'need3q',
        prop: 'need3q',
        dataType: 'YESNO',
      },
      {
        name: 'lastdate',
        prop: 'lastdate',
        dataType: 'DATETIME',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'PICKUP',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'asum',
        prop: 'asum',
        dataType: 'FLOAT',
      },
      {
        name: 'itemdesc',
        prop: 'itemdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'apprempid',
        prop: 'apprempid',
        dataType: 'PICKUP',
      },
      {
        name: 'apprempname',
        prop: 'apprempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'apprdate',
        prop: 'apprdate',
        dataType: 'DATETIME',
      },
      {
        name: 'wplistcostname',
        prop: 'wplistcostname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'emwplist',
        prop: 'emwplistid',
        dataType: 'FONTKEY',
      },
    ]
  }

}