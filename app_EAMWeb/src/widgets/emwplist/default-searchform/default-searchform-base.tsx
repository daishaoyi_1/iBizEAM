import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMWPListService from '@/service/emwplist/emwplist-service';
import DefaultService from './default-searchform-service';
import EMWPListUIService from '@/uiservice/emwplist/emwplist-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWPListService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMWPListService = new EMWPListService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emwplist';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '采购申请';

    /**
     * 界面UI服务对象
     *
     * @type {EMWPListUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMWPListUIService = new EMWPListUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_itemname_like: null,
        n_adate_gtandeq: null,
        n_adate_ltandeq: null,
        n_emservicename_eq: null,
        n_wpliststate_eq: null,
        n_emserviceid_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_itemname_like: new FormItemModel({ caption: '物品(文本包含(%))', detailType: 'FORMITEM', name: 'n_itemname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_adate_gtandeq: new FormItemModel({ caption: '申请日期(>=)', detailType: 'FORMITEM', name: 'n_adate_gtandeq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_adate_ltandeq: new FormItemModel({ caption: '申请日期(<=)', detailType: 'FORMITEM', name: 'n_adate_ltandeq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_emservicename_eq: new FormItemModel({ caption: '服务商(等于(=))', detailType: 'FORMITEM', name: 'n_emservicename_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_wpliststate_eq: new FormItemModel({ caption: '请购状态(等于(=))', detailType: 'FORMITEM', name: 'n_wpliststate_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_emserviceid_eq: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'n_emserviceid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}