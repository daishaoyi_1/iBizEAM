import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * WpAmountFunnel 部件服务对象
 *
 * @export
 * @class WpAmountFunnelService
 */
export default class WpAmountFunnelService extends ControlService {
}
