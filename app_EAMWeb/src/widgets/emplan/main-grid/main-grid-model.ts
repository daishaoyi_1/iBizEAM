/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emplanid',
          prop: 'emplanid',
          dataType: 'GUID',
        },
        {
          name: 'emplanname',
          prop: 'emplanname',
          dataType: 'TEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'plantype',
          prop: 'plantype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'mtflag',
          prop: 'mtflag',
          dataType: 'YESNO',
        },
        {
          name: 'mdate',
          prop: 'mdate',
          dataType: 'DATETIME',
        },
        {
          name: 'planstate',
          prop: 'planstate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'rdeptid',
          prop: 'rdeptid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emplanname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emplanid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emplanid',
          dataType: 'GUID',
        },
        {
          name: 'mpersonid',
          prop: 'mpersonid',
          dataType: 'PICKUP',
        },
        {
          name: 'rempid',
          prop: 'rempid',
          dataType: 'PICKUP',
        },
        {
          name: 'plantemplid',
          prop: 'plantemplid',
          dataType: 'PICKUP',
        },
        {
          name: 'dpid',
          prop: 'dpid',
          dataType: 'PICKUP',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'emplan',
          prop: 'emplanid',
        },
      {
        name: 'n_emplanname_like',
        prop: 'n_emplanname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_plantype_eq',
        prop: 'n_plantype_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_planstate_eq',
        prop: 'n_planstate_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}