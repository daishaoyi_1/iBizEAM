import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * LocalByEQ 部件服务对象
 *
 * @export
 * @class LocalByEQService
 */
export default class LocalByEQService extends ControlService {
}
