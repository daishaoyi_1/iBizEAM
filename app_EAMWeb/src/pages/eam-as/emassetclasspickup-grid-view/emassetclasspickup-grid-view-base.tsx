import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMAssetClassService from '@/service/emasset-class/emasset-class-service';
import EMAssetClassAuthService from '@/authservice/emasset-class/emasset-class-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMAssetClassUIService from '@/uiservice/emasset-class/emasset-class-ui-service';

/**
 * 资产类别选择表格视图视图基类
 *
 * @export
 * @class EMASSETCLASSPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMASSETCLASSPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    protected appDeName: string = 'emassetclass';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    protected appDeKey: string = 'emassetclassid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    protected appDeMajor: string = 'emassetclassname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMAssetClassService}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    protected appEntityService: EMAssetClassService = new EMAssetClassService;

    /**
     * 实体权限服务对象
     *
     * @type EMAssetClassUIService
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public appUIService: EMAssetClassUIService = new EMAssetClassUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emassetclass.views.pickupgridview.caption',
        srfTitle: 'entities.emassetclass.views.pickupgridview.title',
        srfSubTitle: 'entities.emassetclass.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
	protected viewtag: string = '3c335913b1a0ab75b99986012dfcfe78';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupGridViewBase
     */ 
    protected viewName: string = 'EMASSETCLASSPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMASSETCLASSPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emassetclass',
            majorPSDEField: 'emassetclassname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMASSETCLASSPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}