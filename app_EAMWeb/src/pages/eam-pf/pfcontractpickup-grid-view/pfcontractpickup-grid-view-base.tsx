import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import PFContractService from '@/service/pfcontract/pfcontract-service';
import PFContractAuthService from '@/authservice/pfcontract/pfcontract-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import PFContractUIService from '@/uiservice/pfcontract/pfcontract-ui-service';

/**
 * 合同选择表格视图视图基类
 *
 * @export
 * @class PFCONTRACTPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class PFCONTRACTPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    protected appDeName: string = 'pfcontract';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    protected appDeKey: string = 'pfcontractid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    protected appDeMajor: string = 'pfcontractname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {PFContractService}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    protected appEntityService: PFContractService = new PFContractService;

    /**
     * 实体权限服务对象
     *
     * @type PFContractUIService
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public appUIService: PFContractUIService = new PFContractUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfcontract.views.pickupgridview.caption',
        srfTitle: 'entities.pfcontract.views.pickupgridview.title',
        srfSubTitle: 'entities.pfcontract.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupGridViewBase
     */
	protected viewtag: string = '98fc6acb2c4c6be29d2ce47dd0fccc5a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupGridViewBase
     */ 
    protected viewName: string = 'PFCONTRACTPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFCONTRACTPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'pfcontract',
            majorPSDEField: 'pfcontractname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof PFCONTRACTPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}