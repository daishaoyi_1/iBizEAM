import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMEQSpareDetailService from '@/service/emeqspare-detail/emeqspare-detail-service';
import EMEQSpareDetailAuthService from '@/authservice/emeqspare-detail/emeqspare-detail-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMEQSpareDetailUIService from '@/uiservice/emeqspare-detail/emeqspare-detail-ui-service';

/**
 * 备件包明细视图基类
 *
 * @export
 * @class EMEQSPAREDETAILEditViewBase
 * @extends {EditViewBase}
 */
export class EMEQSPAREDETAILEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    protected appDeName: string = 'emeqsparedetail';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    protected appDeKey: string = 'emeqsparedetailid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    protected appDeMajor: string = 'emeqsparedetailname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareDetailService}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    protected appEntityService: EMEQSpareDetailService = new EMEQSpareDetailService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareDetailUIService
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    public appUIService: EMEQSpareDetailUIService = new EMEQSpareDetailUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqsparedetail.views.editview.caption',
        srfTitle: 'entities.emeqsparedetail.views.editview.title',
        srfSubTitle: 'entities.emeqsparedetail.views.editview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMEQSPAREDETAILEditView
     */
    public toolBarModels: any = {
        deuiaction1: { name: 'deuiaction1', caption: 'entities.emeqsparedetail.editviewtoolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emeqsparedetail.editviewtoolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:1,dataaccaction: 'EDIT', uiaction: { tag: 'OpenEditMode', target: 'SINGLEKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
	protected viewtag: string = '095b356064013d1fe71771b75ca6dbfe';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditViewBase
     */ 
    protected viewName: string = 'EMEQSPAREDETAILEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSPAREDETAILEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqsparedetail',
            majorPSDEField: 'emeqsparedetailname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.OpenEditMode(datas, contextJO,paramJO,  $event, xData,this,"EMEQSpareDetail");
    }

    /**
     * 编辑
     *
     * @param {any[]} args 当前数据
     * @param {any} context 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @param {*} [srfParentDeName] 父实体名称
     * @returns {Promise<any>}
     */
    public async OpenEditMode(args: any[], context:any = {} ,params: any={}, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
    
        xData = $event;
        $event = params;
        params = context;
        let context2: any = {};
        let data: any = {};
 			let parentContext:any = {};
        let parentViewParam:any = {};
        const _args: any[] = this.$util.deepCopy(args);
        const _this: any = this;
        const actionTarget: string | null = 'SINGLEKEY';
        Object.assign(context2, { res_partner: '%id%' });
        Object.assign(params, { id: '%id%' });
        Object.assign(params, { name: '%name%' })
 			if(actionContext.context){
            parentContext = actionContext.context;
        }
        if(actionContext.viewparams){
            parentViewParam = actionContext.viewparams;
        }
        context = UIActionTool.handleContextParam(actionTarget,_args,parentContext,parentViewParam,context);
        data = UIActionTool.handleActionParam(actionTarget,_args,parentContext,parentViewParam,params);
        Object.assign(context,this.context,context);
        if(context && context.srfsessionid){
          context.srfsessionkey = context.srfsessionid;
            delete context.srfsessionid;
        }
        const parameters: any[] = [
            { pathName: 'res_partners', parameterName: 'res_partner' },
        ];
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, context,data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                const _this: any = this;
                if (xData && xData.refresh && xData.refresh instanceof Function) {
                    xData.refresh(args);
                }
                return result.datas;
            });
        }
        const view: any = {
            viewname: 'emeqsparedetailedit-view-edit-mode', 
            height: 0, 
            width: 0,  
            title: '备件包明细', 
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);

    }


}