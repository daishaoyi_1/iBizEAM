import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMEQLocationService from '@/service/emeqlocation/emeqlocation-service';
import EMEQLocationAuthService from '@/authservice/emeqlocation/emeqlocation-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMEQLocationUIService from '@/uiservice/emeqlocation/emeqlocation-ui-service';

/**
 * 位置选择表格视图视图基类
 *
 * @export
 * @class EMEQLOCATIONPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMEQLOCATIONPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    protected appDeName: string = 'emeqlocation';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    protected appDeMajor: string = 'emeqlocationname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMEQLocationService}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    protected appEntityService: EMEQLocationService = new EMEQLocationService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLocationUIService
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public appUIService: EMEQLocationUIService = new EMEQLocationUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlocation.views.pickupgridview.caption',
        srfTitle: 'entities.emeqlocation.views.pickupgridview.title',
        srfSubTitle: 'entities.emeqlocation.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
	protected viewtag: string = '571e487e337b60033d4001f09d2f1878';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */ 
    protected viewName: string = 'EMEQLOCATIONPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emeqlocation',
            majorPSDEField: 'emeqlocationname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMEQLOCATIONPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}