import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { CalendarExpViewBase } from '@/studio-core';
import EMWOService from '@/service/emwo/emwo-service';
import EMWOAuthService from '@/authservice/emwo/emwo-auth-service';
import CalendarExpViewEngine from '@engine/view/calendar-exp-view-engine';
import EMWOUIService from '@/uiservice/emwo/emwo-ui-service';

/**
 * 工单日历导航视图视图基类
 *
 * @export
 * @class EMWOCalendarExpViewBase
 * @extends {CalendarExpViewBase}
 */
export class EMWOCalendarExpViewBase extends CalendarExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarExpViewBase
     */
    protected appDeName: string = 'emwo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarExpViewBase
     */
    protected appDeKey: string = 'emwoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarExpViewBase
     */
    protected appDeMajor: string = 'emwoname';

    /**
     * 实体服务对象
     *
     * @type {EMWOService}
     * @memberof EMWOCalendarExpViewBase
     */
    protected appEntityService: EMWOService = new EMWOService;

    /**
     * 实体权限服务对象
     *
     * @type EMWOUIService
     * @memberof EMWOCalendarExpViewBase
     */
    public appUIService: EMWOUIService = new EMWOUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWOCalendarExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWOCalendarExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo.views.calendarexpview.caption',
        srfTitle: 'entities.emwo.views.calendarexpview.title',
        srfSubTitle: 'entities.emwo.views.calendarexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWOCalendarExpViewBase
     */
    protected containerModel: any = {
        view_calendarexpbar: {
            name: 'calendarexpbar',
            type: 'CALENDAREXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarExpViewBase
     */
	protected viewtag: string = '964d77f0f1182905a2cd5eb622cfe18a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarExpViewBase
     */ 
    protected viewName: string = 'EMWOCalendarExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWOCalendarExpViewBase
     */
    public engine: CalendarExpViewEngine = new CalendarExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWOCalendarExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWOCalendarExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            calendarexpbar: this.$refs.calendarexpbar,
            keyPSDEField: 'emwo',
            majorPSDEField: 'emwoname',
            isLoadDefault: true,
        });
    }

    /**
     * calendarexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOCalendarExpViewBase
     */
    public calendarexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'selectionchange', $event);
    }

    /**
     * calendarexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOCalendarExpViewBase
     */
    public calendarexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'activated', $event);
    }

    /**
     * calendarexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOCalendarExpViewBase
     */
    public calendarexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWOCalendarExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        this.$Notice.warning({ title: '错误', desc: '请添加新建数据向导视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWOCalendarExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMWOCalendarExpView
     */
    public viewUID: string = 'eam-core-emwocalendar-exp-view';


}