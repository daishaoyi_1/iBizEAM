import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMServiceService from '@/service/emservice/emservice-service';
import EMServiceAuthService from '@/authservice/emservice/emservice-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMServiceUIService from '@/uiservice/emservice/emservice-ui-service';

/**
 * 服务商实体编辑视图（部件视图）<主信息>财务信息视图基类
 *
 * @export
 * @class EMServiceFinanceView9Base
 * @extends {EditView9Base}
 */
export class EMServiceFinanceView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceFinanceView9Base
     */
    protected appDeName: string = 'emservice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMServiceFinanceView9Base
     */
    protected appDeKey: string = 'emserviceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMServiceFinanceView9Base
     */
    protected appDeMajor: string = 'emservicename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceFinanceView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMServiceService}
     * @memberof EMServiceFinanceView9Base
     */
    protected appEntityService: EMServiceService = new EMServiceService;

    /**
     * 实体权限服务对象
     *
     * @type EMServiceUIService
     * @memberof EMServiceFinanceView9Base
     */
    public appUIService: EMServiceUIService = new EMServiceUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMServiceFinanceView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emservice.views.financeview9.caption',
        srfTitle: 'entities.emservice.views.financeview9.title',
        srfSubTitle: 'entities.emservice.views.financeview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMServiceFinanceView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMServiceFinanceView9Base
     */
	protected viewtag: string = 'a836e0ca7157cf025430ffc6b9d17072';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceFinanceView9Base
     */ 
    protected viewName: string = 'EMServiceFinanceView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMServiceFinanceView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMServiceFinanceView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMServiceFinanceView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emservice',
            majorPSDEField: 'emservicename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMServiceFinanceView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMServiceFinanceView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMServiceFinanceView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMServiceFinanceView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}