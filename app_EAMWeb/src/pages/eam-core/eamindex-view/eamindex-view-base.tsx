import { Vue } from 'vue-property-decorator';
import { FooterItemsService } from '@/studio-core/service/FooterItemsService';
import { AppService } from '@/studio-core/service/app-service/AppService';
import AppMenusModel from '@/widgets/app/eamindex-view-appmenu/eamindex-view-appmenu-model';
import AuthService from '@/authservice/auth-service';

/**
 * 应用首页基类
 */
export class EAMIndexViewBase extends Vue {

  /**
   * 计数器服务对象集合
   *
   * @type {any[]}
   * @memberof EAMIndexViewBase
   */
  protected counterServiceArray: any[] = [];

  /**
   * 建构权限服务对象
   *
   * @type {AuthService}
   * @memberof EAMIndexViewBase
   */
  public authService:AuthService = new AuthService({ $store: this.$store });

  /**
   * 应用服务
   *
   * @protected
   * @type {AppService}
   * @memberof EAMIndexViewBase
   */
  protected appService: AppService = new AppService();

  /**
   * 应用菜单集合
   *
   * @type {AppMenusModel}
   * @memberof EAMIndexViewBase
   */
  protected appMenuModel: AppMenusModel = new AppMenusModel();

  /**
   * 左侧导航菜单
   *
   * @type {*}
   * @memberof EAMIndexViewBase
   */
  protected left_exp: any = this.appMenuModel.getMenuGroup('left_exp') || {};

  /**
   * 底部导航菜单
   *
   * @type {*}
   * @memberof EAMIndexViewBase
   */
  protected bottom_exp: any = this.appMenuModel.getMenuGroup('bottom_exp') || {};
 
  /**
   * 标题栏菜单
   *
   * @type {*}
   * @memberof EAMIndexViewBase
   */
  protected top_menus: any = this.appMenuModel.getMenuGroup('top_menus') || {};
 
  /**
   * 用户菜单
   *
   * @type {*}
   * @memberof EAMIndexViewBase
   */
  protected user_menus: any = this.appMenuModel.getMenuGroup('user_menus') || {};

  /**
   * 底部项绘制服务
   *
   * @type {FooterItemsService}
   * @memberof EAMIndexViewBase
   */
  protected footerItemsService: FooterItemsService = new FooterItemsService();

  /**
   * 视图标识
   *
   * @type {string}
   * @memberof EAMIndexViewBase
   */
  protected viewtag: string = '40d34eca6c3e8e15f0f57a7e3976eb04';

  /**
   * 视图模型数据
   *
   * @type {*}
   * @memberof EAMIndexViewBase
   */
  protected model: any = {
      srfCaption: 'app.views.eamindexview.caption',
      srfTitle: 'app.views.eamindexview.title',
      srfSubTitle: 'app.views.eamindexview.subtitle',
      dataInfo: ''
  }

  /**
   * 应用上下文
   *
   * @type {*}
   * @memberof EAMIndexViewBase
   */
  protected context: any = {};

  /**
   * 视图参数
   *
   * @type {*}
   * @memberof EAMIndexViewBase
   */
  protected viewparams: any = {};

  /**
   * 是否支持应用切换
   *
   * @type {boolean}
   * @memberof EAMIndexViewBase
   */
  public isEnableAppSwitch: boolean = false;

  /**
   * 抽屉状态
   *
   * @type {boolean}
   * @memberof EAMIndexViewBase
   */
  public contextMenuDragVisiable: boolean = false;

  /**
   * 底部绘制
   *
   * @private
   * @memberof AppCenterBase
   */
  private footerRenders: { remove: () => boolean }[] = [];

  /**
   * 注册底部项
   *
   * @memberof EAMIndexViewBase
   */
  protected registerFooterItems(): void {
    const leftItems: any = this.appMenuModel.getMenuGroup('footer_left');
    const centerItems: any = this.appMenuModel.getMenuGroup('footer_center');
    const rightItems: any = this.appMenuModel.getMenuGroup('footer_right');
    if (leftItems && leftItems.items) {
      leftItems.items.forEach((item: any) => {
        this.footerRenders.push(this.footerItemsService.registerLeftItem((h: any) => {
          return <div class='action-item' title={item.tooltip} on-click={() => this.click(item)}>
            <menu-icon item={item}/>
            {item.text}
          </div>;
        }));
      });
    }
    if (centerItems && centerItems.items) {
      centerItems.items.forEach((item: any) => {
        this.footerRenders.push(this.footerItemsService.registerCenterItem((h: any) => {
          return <div class='action-item' title={item.tooltip} on-click={() => this.click(item)}>
            <menu-icon item={item}/>
            {item.text}
          </div>;
        }));
      });
    }
    if (rightItems && rightItems.items) {
      rightItems.items.forEach((item: any) => {
        this.footerRenders.push(this.footerItemsService.registerRightItem((h: any) => {
          return <div class='action-item' title={item.tooltip} on-click={() => this.click(item)}>
            <menu-icon item={item}/>
            {item.text}
          </div>;
        }));
      });
    }
  }

  /**
   * 项点击触发界面行为
   *
   * @protected
   * @param {*} item
   * @memberof EAMIndexViewBase
   */
  protected click(item: any): void {
    const appMenu: any = this.$refs.appmenu;
    if (appMenu) {
      appMenu.click(item);
    }
  }

  /**
   * 组件创建完毕
   *
   * @memberof EAMIndexViewBase
   */
  protected created() {
    this.$openViewService.init(this);
    this.left_exp = this.handleMenusResource(this.left_exp);
    this.bottom_exp = this.handleMenusResource(this.bottom_exp);
    this.top_menus = this.handleMenusResource(this.top_menus);
    this.user_menus = this.handleMenusResource(this.user_menus);
    const secondtag = this.$util.createUUID();
    this.$store.commit("viewaction/createdView", {
      viewtag: this.viewtag,
      secondtag: secondtag
    });
    this.viewtag = secondtag;
    this.parseViewParam();
    this.$uiState.changeLayoutState({
      styleMode: 'STYLE2'
    });
    this.registerFooterItems();
    //  加载语言资源文件
    this.$i18n.mergeLocaleMessage('zh-CN', require('@/locale/lang/zh-CN').default());
  }

  /**
   * 销毁之前
   *
   * @memberof EAMIndexViewBase
   */
  protected beforeDestroy() {
    this.$store.commit("viewaction/removeView", this.viewtag);
    this.footerRenders.forEach(item => item.remove());
  }

  /**
   * Vue声明周期(组件初始化完毕)
   *
   * @memberof EAMIndexViewBase
   */
  protected mounted() {
    this.$viewTool.setIndexParameters([
      { pathName: 'eamindexview', parameterName: 'eamindexview' }
    ]);
    this.$viewTool.setIndexViewParam(this.context);
    setTimeout(() => {
      const el = document.getElementById('app-loading-x');
      if (el) {
        el.style.display = 'none';
      }
    }, 300);
  }

  /**
   * 解析视图参数
   *
   * @private
   * @memberof EAMIndexViewBase
   */
  private parseViewParam(): void {
    const params = this.$route.params;
    if (params?.eamindexview) {
      this.context.eamindexview = params.eamindexview;
    }
    const context = this.$appService.contextStore.appContext;
    if (context) {
      Object.assign(this.context, context);
      context.clearAll();
      Object.assign(context, this.context);
    }
  }

  /**
   * 通过统一资源标识计算菜单
   *
   * @param {*} data
   * @memberof EAMIndexViewBase
   */
  public handleMenusResource(inputMenus: any) {
    if (inputMenus && inputMenus.items) {
      this.computedEffectiveMenus(inputMenus.items);
      this.computeParentMenus(inputMenus.items);
    }
    return inputMenus;
  }

  /**
   * 计算父项菜单项是否隐藏
   *
   * @param {*} inputMenus
   * @memberof EAMIndexViewBase
   */
  public computeParentMenus(inputMenus: Array<any>) {
    if (inputMenus && inputMenus.length > 0) {
      inputMenus.forEach((item: any) => {
        if (item.hidden && item.items && item.items.length > 0) {
          item.items.map((singleItem: any) => {
            if (!singleItem.hidden) {
              item.hidden = false;
            }
            if (singleItem.items && singleItem.items.length > 0) {
              this.computeParentMenus(singleItem.items);
            }
          })
        }
      })
    }
  }

  /**
   * 计算有效菜单项
   *
   * @param {*} inputMenus
   * @memberof EAMIndexViewBase
   */
  public computedEffectiveMenus(inputMenus: Array<any>) {
    inputMenus.forEach((_item: any) => {
      if (!this.authService.getMenusPermission(_item)) {
        _item.hidden = true;
      }
      if (_item.items && _item.items.length > 0) {
          this.computedEffectiveMenus(_item.items);
      }
    })
  }


  /**
   * 绘制内容
   */
  public render(h: any): any {
    const styleMode = this.$uiState.layoutState.styleMode;
    const isStyle2 = styleMode === 'STYLE2';
    let leftContent: any;
    switch (styleMode) {
      case 'DEFAULT':
        leftContent = <app-content-left-exp ref="leftExp" ctrlName="eamindexview" menus={this.left_exp.items} />;
        break;
      case 'STYLE2':
        leftContent = <app-content-left-nav-menu ref="leftNavMenu" ctrlName="eamindexview" menus={this.left_exp.items} on-menu-click={(item: any) => this.click(item)}/>;
    }
    return (
      <app-layout ref="appLayout">
        <template slot="header">
          <app-header>
            <template slot="header_left">
              <div class="title">
                { this.isEnableAppSwitch ? <span class="menuicon" on-click={() => this.contextMenuDragVisiable = !this.contextMenuDragVisiable}><icon type="md-menu" />&nbsp;</span> : null}
                设备资产管理
              </div>
            </template>
            <template slot="header_right">
              <app-header-menus ref="headerMenus" ctrlName="eamindexview" menus={this.top_menus.items} on-menu-click={(item: any) => this.click(item)}/>
              {this.$topRenderService.rightItemsRenders.map((fun: any) => fun(h))}
              <user-info ref="userInfo" ctrlName="eamindexview" menus={this.user_menus.items} on-menu-click={(item: any) => this.click(item)}/>
            </template>
          </app-header>
          <view_appmenu ref='appmenu'/>
          {this.isEnableAppSwitch ? <context-menu-drag contextMenuDragVisiable={this.contextMenuDragVisiable}></context-menu-drag> : null}
        </template>
        <app-content>
          {this.left_exp.items ? <template slot="content_left">
            {leftContent}
          </template> : null}
          {styleMode === 'DEFAULT' ? <tab-page-exp ref="tabExp"></tab-page-exp> : null}
          <div class="view-warp" on-click={() => this.contextMenuDragVisiable = false}>
            <app-keep-alive routerList={this.appService.navHistory.historyList}>
              <router-view key={this.$route.fullPath}></router-view>
            </app-keep-alive>
          </div>
          {this.bottom_exp.items ? <template slot="content_bottom">
            <app-content-bottom-exp ref="bootomExp" ctrlName="eamindexview" menus={this.bottom_exp.items} />
          </template> : null}
        </app-content>
        <template slot="footer">
          <app-footer ref="footer"/>
        </template>
      </app-layout>
    );
  }
}