import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import EMPlanService from '@/service/emplan/emplan-service';
import EMPlanAuthService from '@/authservice/emplan/emplan-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import EMPlanUIService from '@/uiservice/emplan/emplan-ui-service';

/**
 * 计划数据看板视图视图基类
 *
 * @export
 * @class EMPlanDashboardViewBase
 * @extends {DashboardViewBase}
 */
export class EMPlanDashboardViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanDashboardViewBase
     */
    protected appDeName: string = 'emplan';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPlanDashboardViewBase
     */
    protected appDeKey: string = 'emplanid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPlanDashboardViewBase
     */
    protected appDeMajor: string = 'emplanname';

    /**
     * 实体服务对象
     *
     * @type {EMPlanService}
     * @memberof EMPlanDashboardViewBase
     */
    protected appEntityService: EMPlanService = new EMPlanService;

    /**
     * 实体权限服务对象
     *
     * @type EMPlanUIService
     * @memberof EMPlanDashboardViewBase
     */
    public appUIService: EMPlanUIService = new EMPlanUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMPlanDashboardViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPlanDashboardViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emplan.views.dashboardview.caption',
        srfTitle: 'entities.emplan.views.dashboardview.title',
        srfSubTitle: 'entities.emplan.views.dashboardview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPlanDashboardViewBase
     */
    protected containerModel: any = {
        view_dashboard: {
            name: 'dashboard',
            type: 'DASHBOARD',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPlanDashboardViewBase
     */
	protected viewtag: string = '2683e297bf434b81564528df53d16b20';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanDashboardViewBase
     */ 
    protected viewName: string = 'EMPlanDashboardView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPlanDashboardViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPlanDashboardViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPlanDashboardViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'emplan',
            majorPSDEField: 'emplanname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanDashboardViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }

    /** 
     * 数据看板部件刷新状态
     * 
     * @type {boolean}
     * @memberof EMPlanDashboardViewBase
     */
    public state: boolean = true;

    /** 
     * 刷新
     * 
     * @memberof EMPlanDashboardViewBase
     */
    public refresh(args: any){
        this.state = false;
        setTimeout(() => {
            this.state = true;
            this.loadModel();
        }, 0);
    }

}