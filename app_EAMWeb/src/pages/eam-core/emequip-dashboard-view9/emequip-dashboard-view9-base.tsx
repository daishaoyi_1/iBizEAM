import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { DashboardView9Base } from '@/studio-core';
import EMEquipService from '@/service/emequip/emequip-service';
import EMEquipAuthService from '@/authservice/emequip/emequip-auth-service';
import PortalView9Engine from '@engine/view/portal-view9-engine';
import EMEquipUIService from '@/uiservice/emequip/emequip-ui-service';

/**
 * 设备主信息图表数据看板视图视图基类
 *
 * @export
 * @class EMEquipDashboardView9Base
 * @extends {DashboardView9Base}
 */
export class EMEquipDashboardView9Base extends DashboardView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEquipDashboardView9Base
     */
    protected appDeName: string = 'emequip';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEquipDashboardView9Base
     */
    protected appDeKey: string = 'emequipid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEquipDashboardView9Base
     */
    protected appDeMajor: string = 'emequipname';

    /**
     * 实体服务对象
     *
     * @type {EMEquipService}
     * @memberof EMEquipDashboardView9Base
     */
    protected appEntityService: EMEquipService = new EMEquipService;

    /**
     * 实体权限服务对象
     *
     * @type EMEquipUIService
     * @memberof EMEquipDashboardView9Base
     */
    public appUIService: EMEquipUIService = new EMEquipUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEquipDashboardView9Base
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEquipDashboardView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emequip.views.dashboardview9.caption',
        srfTitle: 'entities.emequip.views.dashboardview9.title',
        srfSubTitle: 'entities.emequip.views.dashboardview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEquipDashboardView9Base
     */
    protected containerModel: any = {
        view_dashboard: {
            name: 'dashboard',
            type: 'DASHBOARD',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEquipDashboardView9Base
     */
	protected viewtag: string = '34c8cbdf7eb525aa361d29c15e77141e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEquipDashboardView9Base
     */ 
    protected viewName: string = 'EMEquipDashboardView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEquipDashboardView9Base
     */
    public engine: PortalView9Engine = new PortalView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEquipDashboardView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEquipDashboardView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'emequip',
            majorPSDEField: 'emequipname',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEquipDashboardView9Base
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }

    /** 
     * 数据看板部件刷新状态
     * 
     * @type {boolean}
     * @memberof EMEquipDashboardView9Base
     */
    public state: boolean = true;

    /** 
     * 刷新
     * 
     * @memberof EMEquipDashboardView9Base
     */
    public refresh(args: any){
        this.state = false;
        setTimeout(() => {
            this.state = true;
            this.loadModel();
        }, 0);
    }

}