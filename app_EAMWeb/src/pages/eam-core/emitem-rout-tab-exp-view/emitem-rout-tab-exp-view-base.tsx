import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMItemROutService from '@/service/emitem-rout/emitem-rout-service';
import EMItemROutAuthService from '@/authservice/emitem-rout/emitem-rout-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMItemROutUIService from '@/uiservice/emitem-rout/emitem-rout-ui-service';

/**
 * 退货单分页导航视图视图基类
 *
 * @export
 * @class EMItemROutTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMItemROutTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutTabExpViewBase
     */
    protected appDeName: string = 'emitemrout';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutTabExpViewBase
     */
    protected appDeKey: string = 'emitemroutid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutTabExpViewBase
     */
    protected appDeMajor: string = 'emitemroutname';

    /**
     * 实体服务对象
     *
     * @type {EMItemROutService}
     * @memberof EMItemROutTabExpViewBase
     */
    protected appEntityService: EMItemROutService = new EMItemROutService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemROutUIService
     * @memberof EMItemROutTabExpViewBase
     */
    public appUIService: EMItemROutUIService = new EMItemROutUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemROutTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemROutTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemrout.views.tabexpview.caption',
        srfTitle: 'entities.emitemrout.views.tabexpview.title',
        srfSubTitle: 'entities.emitemrout.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemROutTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutTabExpViewBase
     */
	protected viewtag: string = '7606936e6f4b9940c035f8f665512aaf';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutTabExpViewBase
     */ 
    protected viewName: string = 'EMItemROutTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemROutTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemROutTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemROutTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emitemrout',
            majorPSDEField: 'emitemroutname',
            isLoadDefault: true,
        });
    }


}