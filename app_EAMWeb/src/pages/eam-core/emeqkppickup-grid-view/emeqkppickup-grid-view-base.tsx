import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMEQKPService from '@/service/emeqkp/emeqkp-service';
import EMEQKPAuthService from '@/authservice/emeqkp/emeqkp-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMEQKPUIService from '@/uiservice/emeqkp/emeqkp-ui-service';

/**
 * 设备关键点选择表格视图视图基类
 *
 * @export
 * @class EMEQKPPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMEQKPPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupGridViewBase
     */
    protected appDeName: string = 'emeqkp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupGridViewBase
     */
    protected appDeKey: string = 'emeqkpid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupGridViewBase
     */
    protected appDeMajor: string = 'emeqkpname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMEQKPService}
     * @memberof EMEQKPPickupGridViewBase
     */
    protected appEntityService: EMEQKPService = new EMEQKPService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQKPUIService
     * @memberof EMEQKPPickupGridViewBase
     */
    public appUIService: EMEQKPUIService = new EMEQKPUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQKPPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqkp.views.pickupgridview.caption',
        srfTitle: 'entities.emeqkp.views.pickupgridview.title',
        srfSubTitle: 'entities.emeqkp.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQKPPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupGridViewBase
     */
	protected viewtag: string = '1899736ab44809eb0dfa5452b8893a2f';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupGridViewBase
     */ 
    protected viewName: string = 'EMEQKPPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQKPPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQKPPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQKPPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emeqkp',
            majorPSDEField: 'emeqkpname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMEQKPPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}