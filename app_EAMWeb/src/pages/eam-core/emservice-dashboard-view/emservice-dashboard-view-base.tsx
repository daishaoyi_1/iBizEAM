import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import EMServiceService from '@/service/emservice/emservice-service';
import EMServiceAuthService from '@/authservice/emservice/emservice-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import EMServiceUIService from '@/uiservice/emservice/emservice-ui-service';

/**
 * 综合评估视图基类
 *
 * @export
 * @class EMServiceDashboardViewBase
 * @extends {DashboardViewBase}
 */
export class EMServiceDashboardViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceDashboardViewBase
     */
    protected appDeName: string = 'emservice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMServiceDashboardViewBase
     */
    protected appDeKey: string = 'emserviceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMServiceDashboardViewBase
     */
    protected appDeMajor: string = 'emservicename';

    /**
     * 实体服务对象
     *
     * @type {EMServiceService}
     * @memberof EMServiceDashboardViewBase
     */
    protected appEntityService: EMServiceService = new EMServiceService;

    /**
     * 实体权限服务对象
     *
     * @type EMServiceUIService
     * @memberof EMServiceDashboardViewBase
     */
    public appUIService: EMServiceUIService = new EMServiceUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMServiceDashboardViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMServiceDashboardViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emservice.views.dashboardview.caption',
        srfTitle: 'entities.emservice.views.dashboardview.title',
        srfSubTitle: 'entities.emservice.views.dashboardview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMServiceDashboardViewBase
     */
    protected containerModel: any = {
        view_dashboard: {
            name: 'dashboard',
            type: 'DASHBOARD',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMServiceDashboardViewBase
     */
	protected viewtag: string = '2e4a39364b4cef24bff0c8d221d38576';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceDashboardViewBase
     */ 
    protected viewName: string = 'EMServiceDashboardView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMServiceDashboardViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMServiceDashboardViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMServiceDashboardViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'emservice',
            majorPSDEField: 'emservicename',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMServiceDashboardViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }

    /** 
     * 数据看板部件刷新状态
     * 
     * @type {boolean}
     * @memberof EMServiceDashboardViewBase
     */
    public state: boolean = true;

    /** 
     * 刷新
     * 
     * @memberof EMServiceDashboardViewBase
     */
    public refresh(args: any){
        this.state = false;
        setTimeout(() => {
            this.state = true;
            this.loadModel();
        }, 0);
    }

}