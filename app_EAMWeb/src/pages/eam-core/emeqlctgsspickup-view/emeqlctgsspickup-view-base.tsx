import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMEQLCTGSSService from '@/service/emeqlctgss/emeqlctgss-service';
import EMEQLCTGSSAuthService from '@/authservice/emeqlctgss/emeqlctgss-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMEQLCTGSSUIService from '@/uiservice/emeqlctgss/emeqlctgss-ui-service';

/**
 * 钢丝绳位置数据选择视图视图基类
 *
 * @export
 * @class EMEQLCTGSSPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMEQLCTGSSPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSPickupViewBase
     */
    protected appDeName: string = 'emeqlctgss';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSPickupViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSPickupViewBase
     */
    protected appDeMajor: string = 'eqlocationinfo';

    /**
     * 实体服务对象
     *
     * @type {EMEQLCTGSSService}
     * @memberof EMEQLCTGSSPickupViewBase
     */
    protected appEntityService: EMEQLCTGSSService = new EMEQLCTGSSService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLCTGSSUIService
     * @memberof EMEQLCTGSSPickupViewBase
     */
    public appUIService: EMEQLCTGSSUIService = new EMEQLCTGSSUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTGSSPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlctgss.views.pickupview.caption',
        srfTitle: 'entities.emeqlctgss.views.pickupview.title',
        srfSubTitle: 'entities.emeqlctgss.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTGSSPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSPickupViewBase
     */
	protected viewtag: string = '9341ba20f39986f10fe4715ca70fbfcd';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSPickupViewBase
     */ 
    protected viewName: string = 'EMEQLCTGSSPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLCTGSSPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLCTGSSPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLCTGSSPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emeqlctgss',
            majorPSDEField: 'eqlocationinfo',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTGSSPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTGSSPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTGSSPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}