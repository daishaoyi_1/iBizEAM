import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMEQSetupService from '@/service/emeqsetup/emeqsetup-service';
import EMEQSetupAuthService from '@/authservice/emeqsetup/emeqsetup-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMEQSetupUIService from '@/uiservice/emeqsetup/emeqsetup-ui-service';

/**
 * 安装记录信息视图基类
 *
 * @export
 * @class EMEQSetupCalEditView9Base
 * @extends {EditView9Base}
 */
export class EMEQSetupCalEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSetupCalEditView9Base
     */
    protected appDeName: string = 'emeqsetup';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSetupCalEditView9Base
     */
    protected appDeKey: string = 'emeqsetupid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSetupCalEditView9Base
     */
    protected appDeMajor: string = 'emeqsetupname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSetupCalEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQSetupService}
     * @memberof EMEQSetupCalEditView9Base
     */
    protected appEntityService: EMEQSetupService = new EMEQSetupService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSetupUIService
     * @memberof EMEQSetupCalEditView9Base
     */
    public appUIService: EMEQSetupUIService = new EMEQSetupUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSetupCalEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emeqsetup.views.caleditview9.caption',
        srfTitle: 'entities.emeqsetup.views.caleditview9.title',
        srfSubTitle: 'entities.emeqsetup.views.caleditview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSetupCalEditView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSetupCalEditView9Base
     */
	protected viewtag: string = 'fc741d32f080c4af11803d2ff3dcc11d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSetupCalEditView9Base
     */ 
    protected viewName: string = 'EMEQSetupCalEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSetupCalEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSetupCalEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSetupCalEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqsetup',
            majorPSDEField: 'emeqsetupname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSetupCalEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSetupCalEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSetupCalEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMEQSetupCalEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}