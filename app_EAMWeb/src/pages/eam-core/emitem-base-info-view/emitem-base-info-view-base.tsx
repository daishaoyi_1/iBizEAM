import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMItemService from '@/service/emitem/emitem-service';
import EMItemAuthService from '@/authservice/emitem/emitem-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMItemUIService from '@/uiservice/emitem/emitem-ui-service';

/**
 * 物品视图基类
 *
 * @export
 * @class EMItemBaseInfoViewBase
 * @extends {EditViewBase}
 */
export class EMItemBaseInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemBaseInfoViewBase
     */
    protected appDeName: string = 'emitem';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemBaseInfoViewBase
     */
    protected appDeKey: string = 'emitemid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemBaseInfoViewBase
     */
    protected appDeMajor: string = 'emitemname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemBaseInfoViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMItemService}
     * @memberof EMItemBaseInfoViewBase
     */
    protected appEntityService: EMItemService = new EMItemService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemUIService
     * @memberof EMItemBaseInfoViewBase
     */
    public appUIService: EMItemUIService = new EMItemUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemBaseInfoViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemBaseInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitem.views.baseinfoview.caption',
        srfTitle: 'entities.emitem.views.baseinfoview.title',
        srfSubTitle: 'entities.emitem.views.baseinfoview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemBaseInfoViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemBaseInfoViewBase
     */
	protected viewtag: string = '63db6a201f9e11bccd3f07780dad526a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemBaseInfoViewBase
     */ 
    protected viewName: string = 'EMItemBaseInfoView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemBaseInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemBaseInfoViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemBaseInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emitem',
            majorPSDEField: 'emitemname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemBaseInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemBaseInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemBaseInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}