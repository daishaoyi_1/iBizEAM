import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMPlanService from '@/service/emplan/emplan-service';
import EMPlanAuthService from '@/authservice/emplan/emplan-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMPlanUIService from '@/uiservice/emplan/emplan-ui-service';

/**
 * 计划视图基类
 *
 * @export
 * @class EMPlanPersonInfoBase
 * @extends {EditViewBase}
 */
export class EMPlanPersonInfoBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanPersonInfoBase
     */
    protected appDeName: string = 'emplan';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPlanPersonInfoBase
     */
    protected appDeKey: string = 'emplanid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPlanPersonInfoBase
     */
    protected appDeMajor: string = 'emplanname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanPersonInfoBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMPlanService}
     * @memberof EMPlanPersonInfoBase
     */
    protected appEntityService: EMPlanService = new EMPlanService;

    /**
     * 实体权限服务对象
     *
     * @type EMPlanUIService
     * @memberof EMPlanPersonInfoBase
     */
    public appUIService: EMPlanUIService = new EMPlanUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMPlanPersonInfoBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPlanPersonInfoBase
     */
    protected model: any = {
        srfCaption: 'entities.emplan.views.personinfo.caption',
        srfTitle: 'entities.emplan.views.personinfo.title',
        srfSubTitle: 'entities.emplan.views.personinfo.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPlanPersonInfoBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPlanPersonInfoBase
     */
	protected viewtag: string = 'c61d794032d7d0cfedb229837fb09b16';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanPersonInfoBase
     */ 
    protected viewName: string = 'EMPlanPersonInfo';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPlanPersonInfoBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPlanPersonInfoBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPlanPersonInfoBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emplan',
            majorPSDEField: 'emplanname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanPersonInfoBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanPersonInfoBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanPersonInfoBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}