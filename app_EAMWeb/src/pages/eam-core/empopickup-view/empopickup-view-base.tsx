import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMPOService from '@/service/empo/empo-service';
import EMPOAuthService from '@/authservice/empo/empo-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMPOUIService from '@/uiservice/empo/empo-ui-service';

/**
 * 订单数据选择视图视图基类
 *
 * @export
 * @class EMPOPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMPOPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupViewBase
     */
    protected appDeName: string = 'empo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupViewBase
     */
    protected appDeKey: string = 'empoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupViewBase
     */
    protected appDeMajor: string = 'emponame';

    /**
     * 实体服务对象
     *
     * @type {EMPOService}
     * @memberof EMPOPickupViewBase
     */
    protected appEntityService: EMPOService = new EMPOService;

    /**
     * 实体权限服务对象
     *
     * @type EMPOUIService
     * @memberof EMPOPickupViewBase
     */
    public appUIService: EMPOUIService = new EMPOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPOPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.empo.views.pickupview.caption',
        srfTitle: 'entities.empo.views.pickupview.title',
        srfSubTitle: 'entities.empo.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPOPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupViewBase
     */
	protected viewtag: string = '3747513fcd029dfa942647e803f81d02';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupViewBase
     */ 
    protected viewName: string = 'EMPOPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPOPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPOPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPOPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'empo',
            majorPSDEField: 'emponame',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}