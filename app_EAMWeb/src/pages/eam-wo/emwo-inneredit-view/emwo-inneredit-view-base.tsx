import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMWO_INNERService from '@/service/emwo-inner/emwo-inner-service';
import EMWO_INNERAuthService from '@/authservice/emwo-inner/emwo-inner-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMWO_INNERUIService from '@/uiservice/emwo-inner/emwo-inner-ui-service';

/**
 * 内部工单视图基类
 *
 * @export
 * @class EMWO_INNEREditViewBase
 * @extends {EditViewBase}
 */
export class EMWO_INNEREditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditViewBase
     */
    protected appDeName: string = 'emwo_inner';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditViewBase
     */
    protected appDeKey: string = 'emwo_innerid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditViewBase
     */
    protected appDeMajor: string = 'emwo_innername';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWO_INNERService}
     * @memberof EMWO_INNEREditViewBase
     */
    protected appEntityService: EMWO_INNERService = new EMWO_INNERService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_INNERUIService
     * @memberof EMWO_INNEREditViewBase
     */
    public appUIService: EMWO_INNERUIService = new EMWO_INNERUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_INNEREditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNEREditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_inner.views.editview.caption',
        srfTitle: 'entities.emwo_inner.views.editview.title',
        srfSubTitle: 'entities.emwo_inner.views.editview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNEREditViewBase
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMWO_INNEREditView
     */
    public toolBarModels: any = {
        deuiaction1: { name: 'deuiaction1', caption: 'entities.emwo_inner.editviewtoolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emwo_inner.editviewtoolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:1,dataaccaction: 'EDIT', uiaction: { tag: 'OpenEditMode', target: 'SINGLEKEY', class: '' } },

        tbitem17_submit: { name: 'tbitem17_submit', caption: 'entities.emwo_inner.editviewtoolbar_toolbar.tbitem17_submit.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emwo_inner.editviewtoolbar_toolbar.tbitem17_submit.tip', iconcls: 'fa fa-check', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:1,dataaccaction: 'SUBMIT', uiaction: { tag: 'Submit', target: 'SINGLEKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditViewBase
     */
	protected viewtag: string = '2361099643586c627b770119262f2724';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditViewBase
     */ 
    protected viewName: string = 'EMWO_INNEREditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_INNEREditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_INNEREditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_INNEREditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwo_inner',
            majorPSDEField: 'emwo_innername',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem17_submit')) {
            this.toolbar_tbitem17_submit_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.OpenEditMode(datas, contextJO,paramJO,  $event, xData,this,"EMWO_INNER");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem17_submit_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:EMWO_INNERUIService  = new EMWO_INNERUIService();
        curUIService.EMWO_INNER_Submit(datas,contextJO, paramJO,  $event, xData,this,"EMWO_INNER");
    }

    /**
     * 编辑
     *
     * @param {any[]} args 当前数据
     * @param {any} context 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @param {*} [srfParentDeName] 父实体名称
     * @returns {Promise<any>}
     */
    public async OpenEditMode(args: any[], context:any = {} ,params: any={}, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
    
        xData = $event;
        $event = params;
        params = context;
        let context2: any = {};
        let data: any = {};
 			let parentContext:any = {};
        let parentViewParam:any = {};
        const _args: any[] = this.$util.deepCopy(args);
        const _this: any = this;
        const actionTarget: string | null = 'SINGLEKEY';
        Object.assign(context2, { res_partner: '%id%' });
        Object.assign(params, { id: '%id%' });
        Object.assign(params, { name: '%name%' })
 			if(actionContext.context){
            parentContext = actionContext.context;
        }
        if(actionContext.viewparams){
            parentViewParam = actionContext.viewparams;
        }
        context = UIActionTool.handleContextParam(actionTarget,_args,parentContext,parentViewParam,context);
        data = UIActionTool.handleActionParam(actionTarget,_args,parentContext,parentViewParam,params);
        Object.assign(context,this.context,context);
        if(context && context.srfsessionid){
          context.srfsessionkey = context.srfsessionid;
            delete context.srfsessionid;
        }
        const parameters: any[] = [
            { pathName: 'res_partners', parameterName: 'res_partner' },
        ];
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, context,data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                const _this: any = this;
                if (xData && xData.refresh && xData.refresh instanceof Function) {
                    xData.refresh(args);
                }
                return result.datas;
            });
        }
        const view: any = {
            viewname: 'emwo-inneredit-view-edit-mode', 
            height: 0, 
            width: 0,  
            title: '内部工单', 
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);

    }


}