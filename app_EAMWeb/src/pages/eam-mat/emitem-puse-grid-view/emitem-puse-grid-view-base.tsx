
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { GridViewBase } from '@/studio-core';
import EMItemPUseService from '@/service/emitem-puse/emitem-puse-service';
import EMItemPUseAuthService from '@/authservice/emitem-puse/emitem-puse-auth-service';
import GridViewEngine from '@engine/view/grid-view-engine';
import EMItemPUseUIService from '@/uiservice/emitem-puse/emitem-puse-ui-service';
import CodeListService from '@service/app/codelist-service';


/**
 * 领料单视图基类
 *
 * @export
 * @class EMItemPUseGridViewBase
 * @extends {GridViewBase}
 */
export class EMItemPUseGridViewBase extends GridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseGridViewBase
     */
    protected appDeName: string = 'emitempuse';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseGridViewBase
     */
    protected appDeKey: string = 'emitempuseid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseGridViewBase
     */
    protected appDeMajor: string = 'itempuseinfo';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMItemPUseService}
     * @memberof EMItemPUseGridViewBase
     */
    protected appEntityService: EMItemPUseService = new EMItemPUseService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemPUseUIService
     * @memberof EMItemPUseGridViewBase
     */
    public appUIService: EMItemPUseUIService = new EMItemPUseUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemPUseGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitempuse.views.gridview.caption',
        srfTitle: 'entities.emitempuse.views.gridview.title',
        srfSubTitle: 'entities.emitempuse.views.gridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemPUseGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseGridViewBase
     */
	protected viewtag: string = 'e3ea9d4287f91ef15f3491f205f476ac';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseGridViewBase
     */ 
    protected viewName: string = 'EMItemPUseGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemPUseGridViewBase
     */
    public engine: GridViewEngine = new GridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemPUseGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemPUseGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.opendata(args, fullargs, params, $event, xData);
            },
            newdata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.newdata(args, fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emitempuse',
            majorPSDEField: 'itempuseinfo',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemPUseGridView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.emitempuse;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emequip && true){
            deResParameters = [
            { pathName: 'emequips', parameterName: 'emequip' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emitempuses', parameterName: 'emitempuse' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emitem-puse-edit-view9-new', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emitempuse.views.editview9_new.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemPUseGridView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emequip && true){
            deResParameters = [
            { pathName: 'emequips', parameterName: 'emequip' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emitempuses', parameterName: 'emitempuse' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emitem-puse-edit-view9', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emitempuse.views.editview9.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


}