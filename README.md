iBizEAM是iBiz企业级管理系统群的重要组成部分，iBiz正在逐步把EHR、CRM、EAM、ERP等企业级管理系统群以全新理念、最新技术向大家开源，iBizEAM是开源的重要一步。

iBiz企业级管理系统群全面采取中台模式、SpringBoot+VUE前后台分离架构、MDD/MDA全方位建模技术，满足上万级用户的高性能需求，致力于提供高可用度、全业务覆盖的重度开源项目，iBizEAM既提供传统设备资产管理系统的两大静态主线和一根动态主线，又可以面向IOT提供设备信息采集监控的全面接口体系。

iBiz致力于提升中国软件建设和应用的价值，从业务到技术，从用户到开发者，我们希望iBiz能帮助尽可能多的人：
* 如果你是企业CTO，不仅可以获取运行系统和完整源码，更可获取完整的业务模型，获得系统的实施建设能力和对软件资产的全面管控；
* 如果你是业务或者技术专家，你不仅可以获取全部的业务模型和完整源码，更可获取更强大的自动化实施能力，延伸强者的手臂；
* 如果你是一个初学者，我想从没有一个开源项目可以把最先进的业务和技术全面开放，完整的呈现在你的面前，任你所取。


# 项目总述
* 今天，我们正在快步进入万物互联的世界，IOT、5G、工业互联网、智慧城市，新技术不断演进，将世界前所未有的连接在一起，呈现在我们面前。乱花渐欲迷人眼，我们更加需要一根线，来连接万物，将物与人、设备和系统有机的融合在一起。iBizEAM提供了设备与资产从部署-监控-维护-运行的全生命周期管理，为万物互联提供了强大的管理总线。
* iBizEAM提供了完整资产维护能力。她可以负责存储和维护企业设备，资产和库存的相关数据；可以辅助安排维护工作，跟踪设备状态管理库存和资源以及分析成本；可以帮助公司在降低运营成本的同时提高增收资产的可用性和效能而且不会增加安全问题。 
* iBizEAM提供了完整的设备维护管理能力。她以企业设备台帐为基础，以工作单执行为主线，体现以预防预测性维修为主、强调安全处处存在、强化成本核算的管理思想。实现降低设备故障停机时间、提高设备的可靠性、延长设备的使用寿命、降低维护成本的最终目标。
* iBizEAM提供了强大的设备信息采集对接能力。她面向万物互联，从智慧城市、工业互联网建设的需求着眼，提供了强大的模型定义、接口封装、事务处理能力，我们会逐步开放相应功能，不仅让项目可用于传统EAM应用，更让她真正成为物与人、设备与系统的连接。
* iBizEAM依托iBiz生产体系，不仅提供源码开放，更可提供EAM全面的业务模型，包括每一个数据实体、每一个服务设计、每一个页面UI、每一个流程模型，源码和业务模型完全对应。有兴趣请帮点一下 **Star** 哦！
* **[iBiz开源社区](https://www.ibizlab.cn)**
* **[iBizEAM在线演示](http://eam.ibizlab.cn)**
* **[iBizEAM解决方案](http://demo.ibizlab.cn/ibizeam)**
* **[iBizEAM训练营](http://demo.ibizlab.cn/ibizeam_practice)**
* **[如何在演示系统中建立Issue](https://gitee.com/ibizlab/iBizEAM/wikis/%E5%A6%82%E4%BD%95%E5%9C%A8%E6%BC%94%E7%A4%BA%E7%B3%BB%E7%BB%9F%E4%B8%AD%E6%89%93%E5%BC%80Issue?sort_id=2251512)**
* **[iBizEAM配置平台](http://mos.ibizlab.cn/mos/#/common_mosindex/srfkeys=433D8BEB-6024-4E94-953C-3807DF994584)**
* **[更多开源项目](https://gitee.com/ibizlab)**
* **[系统更新日志](https://gitee.com/ibizlab/iBizEAM/wikis/6%E6%9C%881%E6%97%A5%E6%9B%B4%E6%96%B0?sort_id=2280013)**

**建模平台内测申请：[iBiz建模平台内测申请通道](https://gitee.com/ibizlab/iBizEHR/wikis/%E5%BB%BA%E6%A8%A1%E5%B9%B3%E5%8F%B0%E5%86%85%E6%B5%8B%E9%A1%BB%E7%9F%A5?sort_id=2992220)**

**新版本：[iBizAssetManagement资产管理](https://gitee.com/ibizlab/iBizAssetManagement)**

iBizAssetManagement区别于iBizEAM，他是iBiz商业套件的一个组成部分，是基于iBiz商业套件针对资产维护作业管理一个应用APP。

* **欢迎加入iBizEAM交流QQ群：1056401976**

# 新前端模板
 
**iBizEAM前端切换新模板了** 。

新模板的界面更加精细化，界面组织更为灵活，界面层次感更丰富，菜单的组织从原有的单侧菜单转成更多元化的组织方式——顶部、左侧、底部都可配置菜单，支持切换多种主题颜色，用户体验感也更好。

以下是新模板系统效果图:

![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/161227_d1ca2133_7582525.png "新首页工作台.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/160841_e7957dae_7582525.png "新设备档案列表.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/161356_e4cc3327_7582525.png "新设备档案信息界面.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/161521_fea03b21_7582525.png "新备件包导航界面.png")

喜欢的小伙伴请帮忙点一下 **Star** 哦！


# 业务描述
iBizEAM划分为八大模块:
* 设备管理
* 计划管理
* 工单管理
* 活动历史
* 故障管理
* 资产管理
* 材料管理
* 采购管理

![输入图片说明](https://images.gitee.com/uploads/images/2020/0520/153930_441dc742_1181347.png "iBizEAM业务蓝图.png")


# 技术框架
**后台技术模板[iBiz4j Spring R7](http://demo.ibizlab.cn/ibizr7sfstdtempl/ibiz4jr7)**
* 核心框架：Spring Boot
* 持久层框架: Mybatis-plus
* 服务发现：Nacos
* 日志管理：Logback
* 项目管理框架: Maven

**前端技术模板[iBiz-Vue-R7-Plus](http://demo.ibizlab.cn/ibizr7pfstdtempl/ibizvuer7plus)**
* 前端MVVM框架：vue.js 2.6.10
* 路由：vue-router 3.1.3
* 状态管理：vue-router 3.1.3
* 国际化：vue-i18n 8.15.3
* 数据交互：axios 0.19.1
* UI框架：element-ui 2.13.0, view-design 4.1.0
* 工具库：qs, path-to-regexp, rxjs
* 图标库：font-awesome 4.7.0
* 引入组件： tinymce 4.8.5
* 代码风格检测：eslint

**iBizRuntime微服务运行时  [开源地址](https://gitee.com/ibizlab/ibizlab-runtime)**
- [x]  ibz-rt:基于vue的时尚现代前端UI+统一网关
- [x]  ibz-uaa:统一认证授权微服务（支持第三方认证）  
- [x]  ibz-ou:组织人事管理微服务  
- [x]  ibz-wf:工作流代理微服务（集成flowable） 
- [ ]  ibz-disk:分布式存储微服务(集成kkfileview在线预览+collabora-code在线编辑)  
- [x]  ibz-task:任务调度微服务（集成baomidou-jobs）  
- [x]  ibz-dict:数据字典管理微服务  
- [ ]  ~~ibz-pay:支付管理微服务~~  
- [ ]  ~~ibz-notify:通知微服务~~  
- [ ]  ~~ibz-dst:分布式统计分析微服务（基于Cassandra+drools）~~ 


# 开发环境
* JDK
* Maven
* Node.js
* Yarn
* Vue Cli

# 本地运行
```
# 获取代码
git clone https://gitee.com/ibizlab/iBizEAM.git

# 运行后台

数据库配置

修改eam-util/src/main/resources/application-sys.yml中的数据库配置

  datasource:
    username: 用户名
    password: '密码'
    url: jdbc:mysql://IP地址:3306/库名?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&useOldAliasMetadataBehavior=true
    isSyncDBSchema: true
    defaultSchema: 库名


关闭nacos注册

spring:
  cloud:
    nacos:
      discovery:
        register-enabled: false


不采用统一认证

ibiz:
  enablePermissionValid: false
  auth:
    service: SimpleUserService
    token:
      util: SimpleTokenUtil

eam-boot\src\main\resources\application-dev.yml中注释以下配置

#      loginv7:
#        path: /v7/login
#        serviceId: ibzuaa-api
#        stripPrefix: false
#      oucore:
#        path: /ibzorganizations/**
#        serviceId: ibzou-api
#        stripPrefix: false


>cd iBizEAM
>mvn install
>cd iBizEAM/eam-boot
>mvn spring-boot:run

# 运行前端

修改代理配置,app_EAMWeb/vue.config.js中
// proxy: "http://127.0.0.1:8080/EAMWeb",
-->
proxy: "http://127.0.0.1:8080",

>cd iBizEAM/app_EAMWeb
>yarn install
>yarn serve

# 访问系统
浏览器访问 localhost:8111
```


# 开源说明
* 本系统100%开源，遵守MIT协议


# 模型设计
* ER图设计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0520/153950_a9d7c32d_1181347.png "ER图设计.png")
* 故事板
![输入图片说明](https://images.gitee.com/uploads/images/2020/0520/154005_55b3e5a2_1181347.png "故事板.png")
* 表单设计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0520/181440_3a4e42a1_7582525.png "表单设计.png")
* 图表设计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0520/154031_19f28dc0_1181347.png "图表设计.png")
* 外部接口设计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0520/154105_77e29900_1181347.png "外部接口设计.png")


# 系统美图

* 服务接口
![输入图片说明](https://images.gitee.com/uploads/images/2020/0531/183330_3038f33f_7582525.png "服务接口1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0531/183358_43a54721_7582525.png "服务接口2.png")



# 捐赠
开源不易，坚持更难！如果您觉得iBizEAM不错，可以捐赠请作者喝杯咖啡~，在此表示感谢^_^。

点击以下链接，将页面拉到最下方点击“捐赠”即可。

[前往捐赠](https://gitee.com/ibizlab/iBizEAM)