import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("备件包", null),
		fields: {
			updateman: commonLogic.appcommonhandle("更新人",null),
			emeqspareid: commonLogic.appcommonhandle("备件包标识",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emeqsparename: commonLogic.appcommonhandle("备件包名称",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			eqsparecode: commonLogic.appcommonhandle("备件包代码",null),
			eqspareinfo: commonLogic.appcommonhandle("备件包信息",null),
			description: commonLogic.appcommonhandle("描述",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("备件包",null),
					title: commonLogic.appcommonhandle("备件包选择表格视图",null),
				},
				gridexpview: {
					caption: commonLogic.appcommonhandle("备件包",null),
					title: commonLogic.appcommonhandle("备件包表格导航视图",null),
				},
				listexpview: {
					caption: commonLogic.appcommonhandle("备件包",null),
					title: commonLogic.appcommonhandle("备件包列表导航视图",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("备件包信息",null),
					title: commonLogic.appcommonhandle("备件包信息",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("备件包信息",null),
					title: commonLogic.appcommonhandle("备件包信息",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("备件包",null),
					title: commonLogic.appcommonhandle("备件包",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("备件包",null),
					title: commonLogic.appcommonhandle("备件包数据选择视图",null),
				},
				quickview: {
					caption: commonLogic.appcommonhandle("快速新建",null),
					title: commonLogic.appcommonhandle("快速新建视图",null),
				},
				maindashboardview9: {
					caption: commonLogic.appcommonhandle("备件包信息",null),
					title: commonLogic.appcommonhandle("备件包信息",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("备件包信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("备件包标识",null), 
					srfmajortext: commonLogic.appcommonhandle("备件包名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqsparecode: commonLogic.appcommonhandle("备件包代码",null), 
					emeqsparename: commonLogic.appcommonhandle("备件包名称",null), 
					emeqspareid: commonLogic.appcommonhandle("备件包标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("备件包信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("备件包标识",null), 
					srfmajortext: commonLogic.appcommonhandle("备件包名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqsparecode: commonLogic.appcommonhandle("备件包代码",null), 
					emeqsparename: commonLogic.appcommonhandle("备件包名称",null), 
					emeqspareid: commonLogic.appcommonhandle("备件包标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					eqspareinfo: commonLogic.appcommonhandle("备件包信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main3_grid: {
				columns: {
					eqsparecode: commonLogic.appcommonhandle("备件包代码",null),
					emeqsparename: commonLogic.appcommonhandle("备件包名称",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					eqsparecode: commonLogic.appcommonhandle("备件包代码",null),
					emeqsparename: commonLogic.appcommonhandle("备件包名称",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			list_list: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_eqsparecode_like: commonLogic.appcommonhandle("备件包代码(%)",null), 
					n_emeqsparename_like: commonLogic.appcommonhandle("备件包名称(%)",null), 
				},
				uiactions: {
				},
			},
			gridexpviewgridexpbar_toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem26: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			maindashboardview9dashboard_container1_portlet: {
				uiactions: {
				},
			},
			info_portlet: {
				info: {
					title: commonLogic.appcommonhandle("备件包主信息", null)
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;