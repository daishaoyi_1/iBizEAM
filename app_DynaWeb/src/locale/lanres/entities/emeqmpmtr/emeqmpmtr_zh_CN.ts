import EMEQMPMTR_zh_CN_Base from './emeqmpmtr_zh_CN_base';

function getLocaleResource(){
    const EMEQMPMTR_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQMPMTR_zh_CN_Base(), EMEQMPMTR_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;