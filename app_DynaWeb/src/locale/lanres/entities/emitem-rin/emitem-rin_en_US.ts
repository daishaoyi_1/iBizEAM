import EMItemRIn_en_US_Base from './emitem-rin_en_US_base';

function getLocaleResource(){
    const EMItemRIn_en_US_OwnData = {};
    const targetData = Object.assign(EMItemRIn_en_US_Base(), EMItemRIn_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
