import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("物品交易", null),
		fields: {
			sempname: commonLogic.appcommonhandle("库管员",null),
			batcode: commonLogic.appcommonhandle("批次",null),
			emitemtradename: commonLogic.appcommonhandle("物品交易名称",null),
			civo: commonLogic.appcommonhandle("发票号",null),
			price: commonLogic.appcommonhandle("单价",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			aempname: commonLogic.appcommonhandle("申请人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			sdate: commonLogic.appcommonhandle("出入日期",null),
			tradestate: commonLogic.appcommonhandle("交易状态",null),
			pusetype: commonLogic.appcommonhandle("领料分类",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			aempid: commonLogic.appcommonhandle("申请人",null),
			psum: commonLogic.appcommonhandle("数量",null),
			amount: commonLogic.appcommonhandle("总金额",null),
			inoutflag: commonLogic.appcommonhandle("出入标志",null),
			deptname: commonLogic.appcommonhandle("部门",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			shf: commonLogic.appcommonhandle("税费",null),
			deptid: commonLogic.appcommonhandle("部门",null),
			emitemtradetype: commonLogic.appcommonhandle("交易分组",null),
			sempid: commonLogic.appcommonhandle("库管员",null),
			emitemtradeid: commonLogic.appcommonhandle("物品交易标识",null),
			itemtypegroup: commonLogic.appcommonhandle("物品类型分组",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			description: commonLogic.appcommonhandle("描述",null),
			itemmtypeid: commonLogic.appcommonhandle("物品2级类",null),
			itembtypename: commonLogic.appcommonhandle("物品大类",null),
			storename: commonLogic.appcommonhandle("仓库",null),
			shfprice: commonLogic.appcommonhandle("平均税费",null),
			itembtypeid: commonLogic.appcommonhandle("物品大类",null),
			stockamount: commonLogic.appcommonhandle("物品库存金额",null),
			rname: commonLogic.appcommonhandle("关联单",null),
			itemcode: commonLogic.appcommonhandle("物品代码",null),
			storepartname: commonLogic.appcommonhandle("库位",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			teamname: commonLogic.appcommonhandle("班组",null),
			itemmtypename: commonLogic.appcommonhandle("物品二级类",null),
			labservicename: commonLogic.appcommonhandle("供应商",null),
			itemtypeid: commonLogic.appcommonhandle("物品类型",null),
			labserviceid: commonLogic.appcommonhandle("供应商",null),
			teamid: commonLogic.appcommonhandle("班组",null),
			rid: commonLogic.appcommonhandle("关联单",null),
			itemid: commonLogic.appcommonhandle("物品",null),
			storeid: commonLogic.appcommonhandle("仓库",null),
			storepartid: commonLogic.appcommonhandle("库位",null),
		},
			views: {
				treeexpview: {
					caption: commonLogic.appcommonhandle("物品交易",null),
					title: commonLogic.appcommonhandle("物品交易树导航视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("物品交易",null),
					title: commonLogic.appcommonhandle("物品交易表格视图",null),
				},
			},
			main2_grid: {
				columns: {
					emitemtradeid: commonLogic.appcommonhandle("物品交易标识",null),
					emitemtradetype: commonLogic.appcommonhandle("交易分组",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					sdate: commonLogic.appcommonhandle("出入日期",null),
					inoutflag: commonLogic.appcommonhandle("出入标志",null),
					storename: commonLogic.appcommonhandle("仓库",null),
					storepartname: commonLogic.appcommonhandle("库位",null),
					psum: commonLogic.appcommonhandle("数量",null),
					price: commonLogic.appcommonhandle("单价",null),
					amount: commonLogic.appcommonhandle("总金额",null),
					batcode: commonLogic.appcommonhandle("批次",null),
					sempname: commonLogic.appcommonhandle("库管员",null),
					rname: commonLogic.appcommonhandle("关联单",null),
					tradestate: commonLogic.appcommonhandle("交易状态",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			yearnumbyitem_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem16: {
					caption: commonLogic.appcommonhandle("其它",null),
					tip: commonLogic.appcommonhandle("其它",null),
				},
				tbitem21: {
					caption: commonLogic.appcommonhandle("导出数据模型",null),
					tip: commonLogic.appcommonhandle("导出数据模型",null),
				},
			},
			tradetree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					puse: commonLogic.appcommonhandle("领料单",null),
					rout: commonLogic.appcommonhandle("退货单",null),
					prtn: commonLogic.appcommonhandle("还料单",null),
					rin: commonLogic.appcommonhandle("入库单",null),
					trade: commonLogic.appcommonhandle("全部记录",null),
					root: commonLogic.appcommonhandle("默认根节点",null),
				},
				uiactions: {
				},
			},
			yearnumbyitem_portlet: {
				yearnumbyitem: {
					title: commonLogic.appcommonhandle("物品出入库统计", null)
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;