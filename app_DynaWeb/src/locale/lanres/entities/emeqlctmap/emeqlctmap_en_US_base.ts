import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			emeqlctmapid: commonLogic.appcommonhandle("位置关系标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emeqlctmapname: commonLogic.appcommonhandle("位置关系",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			majorequipname: commonLogic.appcommonhandle("主设备",null),
			eqlocationcode: commonLogic.appcommonhandle("位置代码",null),
			eqlocationpname: commonLogic.appcommonhandle("上级位置",null),
			eqlocationdesc: commonLogic.appcommonhandle("位置描述",null),
			majorequipid: commonLogic.appcommonhandle("主设备",null),
			eqlocationname: commonLogic.appcommonhandle("位置",null),
			eqlocationid: commonLogic.appcommonhandle("位置",null),
			eqlocationpid: commonLogic.appcommonhandle("上级位置",null),
		},
			views: {
				gridview9: {
					caption: commonLogic.appcommonhandle("位置关系",null),
					title: commonLogic.appcommonhandle("位置关系表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("位置关系",null),
					title: commonLogic.appcommonhandle("位置关系编辑视图",null),
				},
			},
			main_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("位置关系信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置关系标识",null), 
					srfmajortext: commonLogic.appcommonhandle("位置关系",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqlocationname: commonLogic.appcommonhandle("位置",null), 
					eqlocationpname: commonLogic.appcommonhandle("上级位置",null), 
					eqlocationid: commonLogic.appcommonhandle("位置",null), 
					emeqlctmapid: commonLogic.appcommonhandle("位置关系标识",null), 
					eqlocationpid: commonLogic.appcommonhandle("上级位置",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					eqlocationpname: commonLogic.appcommonhandle("上级位置",null),
					eqlocationname: commonLogic.appcommonhandle("位置",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			gridview9toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;