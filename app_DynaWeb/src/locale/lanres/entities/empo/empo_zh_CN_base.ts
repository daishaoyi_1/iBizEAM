import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("订单", null),
		fields: {
			content: commonLogic.appcommonhandle("合同内容",null),
			poamount: commonLogic.appcommonhandle("物品金额",null),
			eadate: commonLogic.appcommonhandle("预计到货日期",null),
			empoid: commonLogic.appcommonhandle("订单号",null),
			postate: commonLogic.appcommonhandle("订单状态",null),
			fgempname: commonLogic.appcommonhandle("采购分管副总",null),
			pdate: commonLogic.appcommonhandle("订购日期",null),
			civo: commonLogic.appcommonhandle("货物发票",null),
			labservicedesc: commonLogic.appcommonhandle("供应商备注",null),
			description: commonLogic.appcommonhandle("描述",null),
			emponame: commonLogic.appcommonhandle("订单名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			tsfee: commonLogic.appcommonhandle("运杂费",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			rempid: commonLogic.appcommonhandle("采购员",null),
			taxfee: commonLogic.appcommonhandle("关税",null),
			htjy: commonLogic.appcommonhandle("合同校验",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			apprdate: commonLogic.appcommonhandle("批准日期",null),
			apprempid: commonLogic.appcommonhandle("批准人",null),
			taxivo: commonLogic.appcommonhandle("关税发票",null),
			att: commonLogic.appcommonhandle("附件",null),
			zjlempid: commonLogic.appcommonhandle("总经理",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			fgempid: commonLogic.appcommonhandle("采购分管副总",null),
			maxprice: commonLogic.appcommonhandle("最高单价",null),
			apprempname: commonLogic.appcommonhandle("批准人",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			rempname: commonLogic.appcommonhandle("采购员",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			payway: commonLogic.appcommonhandle("付款方式",null),
			poinfo: commonLogic.appcommonhandle("订单信息",null),
			tsivo: commonLogic.appcommonhandle("运杂费发票",null),
			zjlempname: commonLogic.appcommonhandle("总经理",null),
			labservicetypeid: commonLogic.appcommonhandle("产品供应商",null),
			labservicename: commonLogic.appcommonhandle("产品供应商",null),
			labserviceid: commonLogic.appcommonhandle("产品供应商",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("订单",null),
					title: commonLogic.appcommonhandle("订单数据选择视图",null),
				},
				onordergridview: {
					caption: commonLogic.appcommonhandle("采购订单",null),
					title: commonLogic.appcommonhandle("采购订单",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("采购订单",null),
					title: commonLogic.appcommonhandle("采购订单",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("订单",null),
					title: commonLogic.appcommonhandle("订单",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("订单",null),
					title: commonLogic.appcommonhandle("订单选择表格视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("采购订单",null),
					title: commonLogic.appcommonhandle("采购订单",null),
				},
				placeordergridview: {
					caption: commonLogic.appcommonhandle("采购订单",null),
					title: commonLogic.appcommonhandle("采购订单",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("订单",null),
					title: commonLogic.appcommonhandle("订单编辑视图",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("采购订单",null),
					title: commonLogic.appcommonhandle("采购订单",null),
				},
				closedordergridview: {
					caption: commonLogic.appcommonhandle("采购订单",null),
					title: commonLogic.appcommonhandle("采购订单",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("订单信息",null), 
					grouppanel1: commonLogic.appcommonhandle("其它",null), 
					druipart1: commonLogic.appcommonhandle("订单条目",null), 
					grouppanel3: commonLogic.appcommonhandle("订单条目",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("订单号",null), 
					srfmajortext: commonLogic.appcommonhandle("订单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					empoid: commonLogic.appcommonhandle("订单号(自动)",null), 
					rempid: commonLogic.appcommonhandle("采购员",null), 
					rempname: commonLogic.appcommonhandle("采购员",null), 
					pdate: commonLogic.appcommonhandle("订购日期",null), 
					eadate: commonLogic.appcommonhandle("预计到货日期",null), 
					labservicename: commonLogic.appcommonhandle("产品供应商",null), 
					labservicedesc: commonLogic.appcommonhandle("供应商备注",null), 
					civo: commonLogic.appcommonhandle("货物发票",null), 
					payway: commonLogic.appcommonhandle("付款方式",null), 
					taxivo: commonLogic.appcommonhandle("关税发票",null), 
					taxfee: commonLogic.appcommonhandle("关税",null), 
					tsivo: commonLogic.appcommonhandle("运杂费发票",null), 
					tsfee: commonLogic.appcommonhandle("运杂费",null), 
					poamount: commonLogic.appcommonhandle("物品金额",null), 
					postate: commonLogic.appcommonhandle("订单状态",null), 
					apprempid: commonLogic.appcommonhandle("批准人",null), 
					apprempname: commonLogic.appcommonhandle("批准人",null), 
					apprdate: commonLogic.appcommonhandle("批准日期",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					content: commonLogic.appcommonhandle("合同内容",null), 
					att: commonLogic.appcommonhandle("附件",null), 
				},
				uiactions: {
				},
			},
			main5_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("订单信息",null), 
					grouppanel1: commonLogic.appcommonhandle("其它",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("订单号",null), 
					srfmajortext: commonLogic.appcommonhandle("订单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					empoid: commonLogic.appcommonhandle("订单号(自动)",null), 
					rempid: commonLogic.appcommonhandle("采购员",null), 
					rempname: commonLogic.appcommonhandle("采购员",null), 
					pdate: commonLogic.appcommonhandle("订购日期",null), 
					eadate: commonLogic.appcommonhandle("预计到货日期",null), 
					labservicename: commonLogic.appcommonhandle("产品供应商",null), 
					labservicedesc: commonLogic.appcommonhandle("供应商备注",null), 
					civo: commonLogic.appcommonhandle("货物发票",null), 
					payway: commonLogic.appcommonhandle("付款方式",null), 
					taxivo: commonLogic.appcommonhandle("关税发票",null), 
					taxfee: commonLogic.appcommonhandle("关税",null), 
					tsivo: commonLogic.appcommonhandle("运杂费发票",null), 
					tsfee: commonLogic.appcommonhandle("运杂费",null), 
					poamount: commonLogic.appcommonhandle("物品金额",null), 
					postate: commonLogic.appcommonhandle("订单状态",null), 
					wfstep: commonLogic.appcommonhandle("流程步骤",null), 
					apprempid: commonLogic.appcommonhandle("批准人",null), 
					apprempname: commonLogic.appcommonhandle("批准人",null), 
					apprdate: commonLogic.appcommonhandle("批准日期",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					content: commonLogic.appcommonhandle("合同内容",null), 
					att: commonLogic.appcommonhandle("附件",null), 
					labserviceid: commonLogic.appcommonhandle("产品供应商",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("订单基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("订单号",null), 
					srfmajortext: commonLogic.appcommonhandle("订单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					poinfo: commonLogic.appcommonhandle("订单信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					empoid: commonLogic.appcommonhandle("订单号",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					poinfo: commonLogic.appcommonhandle("订单信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					empoid: commonLogic.appcommonhandle("订单号",null),
					pdate: commonLogic.appcommonhandle("订购日期",null),
					labservicename: commonLogic.appcommonhandle("产品供应商",null),
					rempname: commonLogic.appcommonhandle("采购员",null),
					eadate: commonLogic.appcommonhandle("预计到货日期",null),
					postate: commonLogic.appcommonhandle("订单状态",null),
					wfstep: commonLogic.appcommonhandle("流程步骤",null),
					tsfee: commonLogic.appcommonhandle("运杂费",null),
					taxfee: commonLogic.appcommonhandle("关税",null),
					poamount: commonLogic.appcommonhandle("物品金额",null),
					payway: commonLogic.appcommonhandle("付款方式",null),
					civo: commonLogic.appcommonhandle("货物发票",null),
					tsivo: commonLogic.appcommonhandle("运杂费发票",null),
					taxivo: commonLogic.appcommonhandle("关税发票",null),
					apprempname: commonLogic.appcommonhandle("批准人",null),
					apprdate: commonLogic.appcommonhandle("批准日期",null),
					labservicedesc: commonLogic.appcommonhandle("供应商备注",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_empoid_eq: commonLogic.appcommonhandle("订单号（=）",null), 
					n_labservicename_like: commonLogic.appcommonhandle("产品供应商(文本包含(%))",null), 
					n_postate_eq: commonLogic.appcommonhandle("订单状态（=）",null), 
				},
				uiactions: {
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			onordergridviewtoolbar_toolbar: {
				tbitem1_arrival: {
					caption: commonLogic.appcommonhandle("到货",null),
					tip: commonLogic.appcommonhandle("到货",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				deuiaction2: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
			},
			placeordergridviewtoolbar_toolbar: {
				tbitem1_placeorder: {
					caption: commonLogic.appcommonhandle("下订单",null),
					tip: commonLogic.appcommonhandle("下订单",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				deuiaction2: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			closedordergridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("订单信息",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("订单条目",null),
					}
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;