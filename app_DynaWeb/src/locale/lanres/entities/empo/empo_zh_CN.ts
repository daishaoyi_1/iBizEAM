import EMPO_zh_CN_Base from './empo_zh_CN_base';

function getLocaleResource(){
    const EMPO_zh_CN_OwnData = {};
    const targetData = Object.assign(EMPO_zh_CN_Base(), EMPO_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;