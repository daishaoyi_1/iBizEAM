import EMRFODEType_zh_CN_Base from './emrfodetype_zh_CN_base';

function getLocaleResource(){
    const EMRFODEType_zh_CN_OwnData = {};
    const targetData = Object.assign(EMRFODEType_zh_CN_Base(), EMRFODEType_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;