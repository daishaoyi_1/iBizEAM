import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			objectinfo: commonLogic.appcommonhandle("对象信息",null),
			emobjecttype: commonLogic.appcommonhandle("对象类型",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emobjectid: commonLogic.appcommonhandle("对象标识",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			objectcode: commonLogic.appcommonhandle("对象代码",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			emobjectname: commonLogic.appcommonhandle("对象名称",null),
			majorequipname: commonLogic.appcommonhandle("主设备",null),
			majorequipid: commonLogic.appcommonhandle("主设备",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("对象",null),
					title: commonLogic.appcommonhandle("对象选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("对象",null),
					title: commonLogic.appcommonhandle("对象数据选择视图",null),
				},
			},
			main2_grid: {
				columns: {
					emobjectname: commonLogic.appcommonhandle("对象名称",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;