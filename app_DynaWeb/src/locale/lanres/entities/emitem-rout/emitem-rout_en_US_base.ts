import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			sapreason1: commonLogic.appcommonhandle("sap传输异常文本",null),
			emitemroutname: commonLogic.appcommonhandle("退货单名称",null),
			batcode: commonLogic.appcommonhandle("批次",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			price: commonLogic.appcommonhandle("单价",null),
			emitemroutid: commonLogic.appcommonhandle("退货单号",null),
			description: commonLogic.appcommonhandle("描述",null),
			amount: commonLogic.appcommonhandle("总金额",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			sap: commonLogic.appcommonhandle("sap传输状态",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			sdate: commonLogic.appcommonhandle("出库日期",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			itemroutinfo: commonLogic.appcommonhandle("退货单信息",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			tradestate: commonLogic.appcommonhandle("退货状态",null),
			shf: commonLogic.appcommonhandle("税费",null),
			sapcontrol: commonLogic.appcommonhandle("sap控制",null),
			sapreason: commonLogic.appcommonhandle("sap传输失败原因",null),
			psum: commonLogic.appcommonhandle("数量",null),
			teamid: commonLogic.appcommonhandle("班组",null),
			labserviceid: commonLogic.appcommonhandle("供应商",null),
			civo: commonLogic.appcommonhandle("发票号",null),
			storepartname: commonLogic.appcommonhandle("库位",null),
			storename: commonLogic.appcommonhandle("仓库",null),
			rname: commonLogic.appcommonhandle("入库单",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			storeid: commonLogic.appcommonhandle("仓库",null),
			rid: commonLogic.appcommonhandle("入库单",null),
			storepartid: commonLogic.appcommonhandle("库位",null),
			itemid: commonLogic.appcommonhandle("物品",null),
			sempid: commonLogic.appcommonhandle("退货人",null),
			sempname: commonLogic.appcommonhandle("退货人",null),
		},
			views: {
				draftgridview: {
					caption: commonLogic.appcommonhandle("退货单-草稿",null),
					title: commonLogic.appcommonhandle("退货单",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("退货单信息",null),
					title: commonLogic.appcommonhandle("退货单信息",null),
				},
				toconfirmgridview: {
					caption: commonLogic.appcommonhandle("退货单-待确认",null),
					title: commonLogic.appcommonhandle("退货单",null),
				},
				confirmedgridview: {
					caption: commonLogic.appcommonhandle("退货单-已确认",null),
					title: commonLogic.appcommonhandle("退货单",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("退货单信息",null),
					title: commonLogic.appcommonhandle("退货单信息",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("退货单",null),
					title: commonLogic.appcommonhandle("退货单",null),
				},
				editview9_new: {
					caption: commonLogic.appcommonhandle("退货单信息",null),
					title: commonLogic.appcommonhandle("退货单信息",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("退货单",null),
					title: commonLogic.appcommonhandle("退货单编辑视图",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("退货单",null),
					title: commonLogic.appcommonhandle("退货单分页导航视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("退货单信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel16: commonLogic.appcommonhandle("操作信息",null), 
					formpage15: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("退货单号",null), 
					srfmajortext: commonLogic.appcommonhandle("退货单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emitemroutid: commonLogic.appcommonhandle("退货单号(自动)",null), 
					rid: commonLogic.appcommonhandle("入库单",null), 
					rname: commonLogic.appcommonhandle("入库单",null), 
					itemid: commonLogic.appcommonhandle("物品",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					storeid: commonLogic.appcommonhandle("仓库",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					storepartid: commonLogic.appcommonhandle("库位",null), 
					storepartname: commonLogic.appcommonhandle("库位",null), 
					psum: commonLogic.appcommonhandle("数量",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					shf: commonLogic.appcommonhandle("税费",null), 
					batcode: commonLogic.appcommonhandle("批次",null), 
					sempname: commonLogic.appcommonhandle("退货人",null), 
					sempid: commonLogic.appcommonhandle("退货人",null), 
					emitemroutname: commonLogic.appcommonhandle("退货单名称",null), 
					sdate: commonLogic.appcommonhandle("出库日期",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("退货单信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel16: commonLogic.appcommonhandle("操作信息",null), 
					formpage15: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("退货单号",null), 
					srfmajortext: commonLogic.appcommonhandle("退货单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emitemroutid: commonLogic.appcommonhandle("退货单号(自动)",null), 
					rname: commonLogic.appcommonhandle("入库单",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					sdate: commonLogic.appcommonhandle("出库日期",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					storepartname: commonLogic.appcommonhandle("库位",null), 
					psum: commonLogic.appcommonhandle("数量",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					shf: commonLogic.appcommonhandle("税费",null), 
					batcode: commonLogic.appcommonhandle("批次",null), 
					sempid: commonLogic.appcommonhandle("退货人",null), 
					sempname: commonLogic.appcommonhandle("退货人",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("退货单基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("退货单号",null), 
					srfmajortext: commonLogic.appcommonhandle("退货单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemroutinfo: commonLogic.appcommonhandle("退货单信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emitemroutid: commonLogic.appcommonhandle("退货单号",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					emitemroutid: commonLogic.appcommonhandle("退货单号",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					sdate: commonLogic.appcommonhandle("出库日期",null),
					storename: commonLogic.appcommonhandle("仓库",null),
					storepartname: commonLogic.appcommonhandle("库位",null),
					psum: commonLogic.appcommonhandle("数量",null),
					price: commonLogic.appcommonhandle("单价",null),
					amount: commonLogic.appcommonhandle("总金额",null),
					batcode: commonLogic.appcommonhandle("批次",null),
					rname: commonLogic.appcommonhandle("入库单",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_itemname_like: commonLogic.appcommonhandle("物品(文本包含(%))",null), 
					n_storeid_eq: commonLogic.appcommonhandle("仓库(等于(=))",null), 
					n_storepartid_eq: commonLogic.appcommonhandle("库位(等于(=))",null), 
					n_tradestate_eq: commonLogic.appcommonhandle("退货状态(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview9_newtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				deuiaction2_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem17_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
			},
			draftgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			toconfirmgridviewtoolbar_toolbar: {
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("确认",null),
					tip: commonLogic.appcommonhandle("确认",null),
				},
				tbitem1_rejected: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				deuiaction2: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				deuiaction2_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("全部退货单",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("草稿",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("待确认",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("已确认",null),
					}
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;