import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			createman: commonLogic.appcommonhandle("建立人",null),
			machtypecode: commonLogic.appcommonhandle("机种编码",null),
			emmachmodelid: commonLogic.appcommonhandle("流水号",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			remarks: commonLogic.appcommonhandle("备注",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			emmachmodelname: commonLogic.appcommonhandle("机型名称",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("机型",null),
					title: commonLogic.appcommonhandle("机型选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("机型",null),
					title: commonLogic.appcommonhandle("机型数据选择视图",null),
				},
			},
			main_grid: {
				columns: {
					emmachmodelname: commonLogic.appcommonhandle("机型名称",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;