import EMMachModel_en_US_Base from './emmach-model_en_US_base';

function getLocaleResource(){
    const EMMachModel_en_US_OwnData = {};
    const targetData = Object.assign(EMMachModel_en_US_Base(), EMMachModel_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
