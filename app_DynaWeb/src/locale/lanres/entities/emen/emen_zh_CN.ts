import EMEN_zh_CN_Base from './emen_zh_CN_base';

function getLocaleResource(){
    const EMEN_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEN_zh_CN_Base(), EMEN_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;