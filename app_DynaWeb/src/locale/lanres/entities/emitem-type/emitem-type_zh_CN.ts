import EMItemType_zh_CN_Base from './emitem-type_zh_CN_base';

function getLocaleResource(){
    const EMItemType_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemType_zh_CN_Base(), EMItemType_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;