import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			description: commonLogic.appcommonhandle("描述",null),
			gssxm: commonLogic.appcommonhandle("项目",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			eqmodelcode: commonLogic.appcommonhandle("型号",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			valve: commonLogic.appcommonhandle("预警期限(天)",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			len: commonLogic.appcommonhandle("长度",null),
			zj: commonLogic.appcommonhandle("直径",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			eqlocationinfo: commonLogic.appcommonhandle("钢丝绳信息",null),
			emeqlocationid: commonLogic.appcommonhandle("位置标识",null),
			equipid: commonLogic.appcommonhandle("设备",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("钢丝绳位置",null),
					title: commonLogic.appcommonhandle("钢丝绳位置",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("钢丝绳位置",null),
					title: commonLogic.appcommonhandle("钢丝绳位置",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("钢丝绳位置",null),
					title: commonLogic.appcommonhandle("钢丝绳位置数据选择视图",null),
				},
				gridview_cqyj: {
					caption: commonLogic.appcommonhandle("钢丝绳位置",null),
					title: commonLogic.appcommonhandle("钢丝绳位置",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("钢丝绳位置超期预警",null),
					title: commonLogic.appcommonhandle("钢丝绳位置超期预警",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("钢丝绳位置",null),
					title: commonLogic.appcommonhandle("钢丝绳位置选择表格视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("钢丝绳位置",null),
					title: commonLogic.appcommonhandle("钢丝绳位置编辑视图",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("钢丝绳位置树导航",null),
					title: commonLogic.appcommonhandle("钢丝绳位置树导航",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("位置信息",null), 
					grouppanel9: commonLogic.appcommonhandle("预警信息",null), 
					grouppanel11: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("钢丝绳信息",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emeqlocationid: commonLogic.appcommonhandle("位置标识",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					gssxm: commonLogic.appcommonhandle("项目",null), 
					eqmodelcode: commonLogic.appcommonhandle("型号",null), 
					zj: commonLogic.appcommonhandle("直径",null), 
					len: commonLogic.appcommonhandle("长度",null), 
					valve: commonLogic.appcommonhandle("预警期限(天)",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("位置信息",null), 
					grouppanel9: commonLogic.appcommonhandle("预警信息",null), 
					grouppanel11: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("钢丝绳信息",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emeqlocationid: commonLogic.appcommonhandle("位置标识",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					gssxm: commonLogic.appcommonhandle("项目",null), 
					eqmodelcode: commonLogic.appcommonhandle("型号",null), 
					zj: commonLogic.appcommonhandle("直径",null), 
					len: commonLogic.appcommonhandle("长度",null), 
					valve: commonLogic.appcommonhandle("预警期限(天)",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					eqlocationinfo: commonLogic.appcommonhandle("钢丝绳信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main3_grid: {
				columns: {
					eqlocationinfo: commonLogic.appcommonhandle("钢丝绳信息",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					eqmodelcode: commonLogic.appcommonhandle("型号",null),
					len: commonLogic.appcommonhandle("长度",null),
					zj: commonLogic.appcommonhandle("直径",null),
					valve: commonLogic.appcommonhandle("预警期限(天)",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridview_cqyjtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			yjtree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					yjnode: commonLogic.appcommonhandle("钢丝绳位置",null),
					root: commonLogic.appcommonhandle("默认根节点",null),
				},
				uiactions: {
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("近两个月超期预警",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("全部超期预警",null),
					}
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;