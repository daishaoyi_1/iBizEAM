import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("加班工单", null),
		fields: {
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			jbgznr: commonLogic.appcommonhandle("加班工作内容",null),
			workdate: commonLogic.appcommonhandle("加班日期",null),
			overtime: commonLogic.appcommonhandle("加班时间(分)",null),
			empname: commonLogic.appcommonhandle("员工",null),
			workstate: commonLogic.appcommonhandle("加班区分",null),
			amendtime: commonLogic.appcommonhandle("上午结束时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			empid: commonLogic.appcommonhandle("员工",null),
			ambegintime: commonLogic.appcommonhandle("开始时间",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emworkstate: commonLogic.appcommonhandle("加班工单状态",null),
			emworkname: commonLogic.appcommonhandle("加班工单名称",null),
			pmendtime: commonLogic.appcommonhandle("结束时间",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			emworkid: commonLogic.appcommonhandle("加班工单标识",null),
			post: commonLogic.appcommonhandle("岗位",null),
			workplace: commonLogic.appcommonhandle("工作地点",null),
			pmbegintime: commonLogic.appcommonhandle("下午开始时间",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
		},
		};
		return data;
}
export default getLocaleResourceBase;