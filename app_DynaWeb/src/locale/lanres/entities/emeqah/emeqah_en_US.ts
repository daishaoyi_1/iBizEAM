import EMEQAH_en_US_Base from './emeqah_en_US_base';

function getLocaleResource(){
    const EMEQAH_en_US_OwnData = {};
    const targetData = Object.assign(EMEQAH_en_US_Base(), EMEQAH_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
