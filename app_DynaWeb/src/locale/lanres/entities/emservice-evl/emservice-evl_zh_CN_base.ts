import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("服务商评估", null),
		fields: {
			emserviceevlid: commonLogic.appcommonhandle("服务商评估标识",null),
			emserviceevlname: commonLogic.appcommonhandle("服务商评估名称",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			evlresult9: commonLogic.appcommonhandle("售后(100分,10%)",null),
			evlresult5: commonLogic.appcommonhandle("供货能力(100分,10%)",null),
			evlresult7: commonLogic.appcommonhandle("距离(100分,5%)",null),
			evlresult6: commonLogic.appcommonhandle("及时性(100分,10%)",null),
			evlresult1: commonLogic.appcommonhandle("资质(100分,5%)",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			evlmark1: commonLogic.appcommonhandle("评价",null),
			evlresult4: commonLogic.appcommonhandle("安全性能(100分,10%)",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			evlresult8: commonLogic.appcommonhandle("质量管理体系(100分,5%)",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			empname: commonLogic.appcommonhandle("评估人",null),
			evldate: commonLogic.appcommonhandle("评估时间",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			evlmark: commonLogic.appcommonhandle("评价",null),
			evlresult: commonLogic.appcommonhandle("总分数",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			evlregion: commonLogic.appcommonhandle("评估区间",null),
			evlresult3: commonLogic.appcommonhandle("价格(100分,20%)",null),
			evlresult2: commonLogic.appcommonhandle("质量(100分,25%)",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			empid: commonLogic.appcommonhandle("评估人",null),
			serviceevlstate: commonLogic.appcommonhandle("评估状态",null),
			description: commonLogic.appcommonhandle("描述",null),
			servicename: commonLogic.appcommonhandle("服务商",null),
			serviceid: commonLogic.appcommonhandle("服务商",null),
		},
			views: {
				toconfirmgridview: {
					caption: commonLogic.appcommonhandle("服务商评估-待确认",null),
					title: commonLogic.appcommonhandle("服务商评估",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("服务商评估信息",null),
					title: commonLogic.appcommonhandle("服务商评估信息",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("服务商评估",null),
					title: commonLogic.appcommonhandle("服务商评估",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("服务商评估",null),
					title: commonLogic.appcommonhandle("服务商评估编辑视图",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("服务商评估",null),
					title: commonLogic.appcommonhandle("服务商评估分页导航视图",null),
				},
				gridview9: {
					caption: commonLogic.appcommonhandle("服务商评估",null),
					title: commonLogic.appcommonhandle("服务商评估",null),
				},
				confirmedgridview: {
					caption: commonLogic.appcommonhandle("服务商评估-已完成",null),
					title: commonLogic.appcommonhandle("服务商评估",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("服务商评估信息",null),
					title: commonLogic.appcommonhandle("服务商评估信息",null),
				},
				draftgridview: {
					caption: commonLogic.appcommonhandle("服务商评估-草稿",null),
					title: commonLogic.appcommonhandle("服务商评估",null),
				},
			},
			main4_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("服务商评估信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商评估标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商评估名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					servicename: commonLogic.appcommonhandle("服务商",null), 
					empname: commonLogic.appcommonhandle("评估人",null), 
					empid: commonLogic.appcommonhandle("评估人",null), 
					evlregion: commonLogic.appcommonhandle("评估区间",null), 
					evldate: commonLogic.appcommonhandle("评估时间",null), 
					evlresult3: commonLogic.appcommonhandle("价格(100分,20%)",null), 
					evlresult7: commonLogic.appcommonhandle("距离(100分,5%)",null), 
					evlresult2: commonLogic.appcommonhandle("质量(100分,25%)",null), 
					evlresult8: commonLogic.appcommonhandle("质量管理体系(100分,5%)",null), 
					evlresult5: commonLogic.appcommonhandle("供货能力(100分,10%)",null), 
					evlresult6: commonLogic.appcommonhandle("及时性(100分,10%)",null), 
					evlresult9: commonLogic.appcommonhandle("售后(100分,10%)",null), 
					evlresult4: commonLogic.appcommonhandle("安全性能(100分,10%)",null), 
					evlresult1: commonLogic.appcommonhandle("资质(100分,5%)",null), 
					evlresult: commonLogic.appcommonhandle("总分数",null), 
					serviceevlstate: commonLogic.appcommonhandle("评估状态",null), 
					evlmark: commonLogic.appcommonhandle("评价",null), 
					emserviceevlid: commonLogic.appcommonhandle("服务商评估标识",null), 
					serviceid: commonLogic.appcommonhandle("服务商",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("服务商评估信息",null), 
					grouppanel19: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商评估标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商评估名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					servicename: commonLogic.appcommonhandle("服务商",null), 
					empid: commonLogic.appcommonhandle("评估人",null), 
					empname: commonLogic.appcommonhandle("评估人",null), 
					evlregion: commonLogic.appcommonhandle("评估区间",null), 
					evldate: commonLogic.appcommonhandle("评估时间",null), 
					evlresult3: commonLogic.appcommonhandle("价格(100分,20%)",null), 
					evlresult7: commonLogic.appcommonhandle("距离(100分,5%)",null), 
					evlresult2: commonLogic.appcommonhandle("质量(100分,25%)",null), 
					evlresult8: commonLogic.appcommonhandle("质量管理体系(100分,5%)",null), 
					evlresult5: commonLogic.appcommonhandle("供货能力(100分,10%)",null), 
					evlresult6: commonLogic.appcommonhandle("及时性(100分,10%)",null), 
					evlresult9: commonLogic.appcommonhandle("售后(100分,10%)",null), 
					evlresult4: commonLogic.appcommonhandle("安全性能(100分,10%)",null), 
					evlresult1: commonLogic.appcommonhandle("资质(100分,5%)",null), 
					evlresult: commonLogic.appcommonhandle("总分数",null), 
					serviceevlstate: commonLogic.appcommonhandle("评估状态",null), 
					evlmark: commonLogic.appcommonhandle("评价",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emserviceevlid: commonLogic.appcommonhandle("服务商评估标识",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("服务商评估信息",null), 
					grouppanel19: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("服务商评估标识",null), 
					srfmajortext: commonLogic.appcommonhandle("服务商评估名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					servicename: commonLogic.appcommonhandle("服务商",null), 
					empid: commonLogic.appcommonhandle("评估人",null), 
					empname: commonLogic.appcommonhandle("评估人",null), 
					evlregion: commonLogic.appcommonhandle("评估区间",null), 
					evldate: commonLogic.appcommonhandle("评估时间",null), 
					evlresult3: commonLogic.appcommonhandle("价格(100分,20%)",null), 
					evlresult7: commonLogic.appcommonhandle("距离(100分,5%)",null), 
					evlresult2: commonLogic.appcommonhandle("质量(100分,25%)",null), 
					evlresult8: commonLogic.appcommonhandle("质量管理体系(100分,5%)",null), 
					evlresult5: commonLogic.appcommonhandle("供货能力(100分,10%)",null), 
					evlresult6: commonLogic.appcommonhandle("及时性(100分,10%)",null), 
					evlresult9: commonLogic.appcommonhandle("售后(100分,10%)",null), 
					evlresult4: commonLogic.appcommonhandle("安全性能(100分,10%)",null), 
					evlresult1: commonLogic.appcommonhandle("资质(100分,5%)",null), 
					evlresult: commonLogic.appcommonhandle("总分数",null), 
					serviceevlstate: commonLogic.appcommonhandle("评估状态",null), 
					evlmark: commonLogic.appcommonhandle("评价",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emserviceevlid: commonLogic.appcommonhandle("服务商评估标识",null), 
					serviceid: commonLogic.appcommonhandle("服务商",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					servicename: commonLogic.appcommonhandle("服务商",null),
					empname: commonLogic.appcommonhandle("评估人",null),
					evldate: commonLogic.appcommonhandle("评估时间",null),
					evlresult6: commonLogic.appcommonhandle("及时性(100分,10%)",null),
					evlresult4: commonLogic.appcommonhandle("安全性能(100分,10%)",null),
					evlresult1: commonLogic.appcommonhandle("资质(100分,5%)",null),
					evlresult2: commonLogic.appcommonhandle("质量(100分,25%)",null),
					evlresult5: commonLogic.appcommonhandle("供货能力(100分,10%)",null),
					evlresult8: commonLogic.appcommonhandle("质量管理体系(100分,5%)",null),
					evlresult7: commonLogic.appcommonhandle("距离(100分,5%)",null),
					evlresult3: commonLogic.appcommonhandle("价格(100分,20%)",null),
					evlresult9: commonLogic.appcommonhandle("售后(100分,10%)",null),
					evlresult: commonLogic.appcommonhandle("总分数",null),
					evlmark: commonLogic.appcommonhandle("评价",null),
					serviceevlstate: commonLogic.appcommonhandle("评估状态",null),
					evlregion: commonLogic.appcommonhandle("评估区间",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					servicename: commonLogic.appcommonhandle("服务商",null),
					empname: commonLogic.appcommonhandle("评估人",null),
					evldate: commonLogic.appcommonhandle("评估时间",null),
					evlresult6: commonLogic.appcommonhandle("及时性(100分,10%)",null),
					evlresult4: commonLogic.appcommonhandle("安全性能(100分,10%)",null),
					evlresult1: commonLogic.appcommonhandle("资质(100分,5%)",null),
					evlresult2: commonLogic.appcommonhandle("质量(100分,25%)",null),
					evlresult5: commonLogic.appcommonhandle("供货能力(100分,10%)",null),
					evlresult8: commonLogic.appcommonhandle("质量管理体系(100分,5%)",null),
					evlresult7: commonLogic.appcommonhandle("距离(100分,5%)",null),
					evlresult3: commonLogic.appcommonhandle("价格(100分,20%)",null),
					evlresult9: commonLogic.appcommonhandle("售后(100分,10%)",null),
					evlresult: commonLogic.appcommonhandle("总分数",null),
					evlmark: commonLogic.appcommonhandle("评价",null),
					serviceevlstate: commonLogic.appcommonhandle("评估状态",null),
					evlregion: commonLogic.appcommonhandle("评估区间",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			overallevl_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			evaluatetop5_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_serviceid_eq: commonLogic.appcommonhandle("服务商(等于(=))",null), 
					n_evlregion_eq: commonLogic.appcommonhandle("评估区间(等于(=))",null), 
					n_serviceevlstate_eq: commonLogic.appcommonhandle("评估状态(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				deuiaction2_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem17_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
			},
			toconfirmgridviewtoolbar_toolbar: {
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("确认",null),
					tip: commonLogic.appcommonhandle("确认",null),
				},
				tbitem1_rejected: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				deuiaction2: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			draftgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("全部服务商评估",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("草稿",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("待确认",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("已完成",null),
					}
				},
				uiactions: {
				},
			},
			overallevl_portlet: {
				overallevl: {
					title: commonLogic.appcommonhandle("综合评估", null)
				},
				uiactions: {
				},
			},
			evaluatetop5_portlet: {
				evaluatetop5: {
					title: commonLogic.appcommonhandle("评估前五", null)
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;