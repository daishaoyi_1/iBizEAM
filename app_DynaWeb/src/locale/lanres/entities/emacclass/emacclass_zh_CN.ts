import EMACClass_zh_CN_Base from './emacclass_zh_CN_base';

function getLocaleResource(){
    const EMACClass_zh_CN_OwnData = {};
    const targetData = Object.assign(EMACClass_zh_CN_Base(), EMACClass_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;