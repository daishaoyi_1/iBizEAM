import EMEQKPRCD_zh_CN_Base from './emeqkprcd_zh_CN_base';

function getLocaleResource(){
    const EMEQKPRCD_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQKPRCD_zh_CN_Base(), EMEQKPRCD_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;