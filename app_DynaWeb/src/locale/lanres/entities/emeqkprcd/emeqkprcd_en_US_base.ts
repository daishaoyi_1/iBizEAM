import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			edate: commonLogic.appcommonhandle("采集时间",null),
			bdate: commonLogic.appcommonhandle("上次采集时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			emeqkprcdname: commonLogic.appcommonhandle("关键点记录名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			nval: commonLogic.appcommonhandle("数值",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			val: commonLogic.appcommonhandle("值",null),
			description: commonLogic.appcommonhandle("描述",null),
			emeqkprcdid: commonLogic.appcommonhandle("关键点记录标识",null),
			objname: commonLogic.appcommonhandle("位置",null),
			kpname: commonLogic.appcommonhandle("关键点",null),
			woname: commonLogic.appcommonhandle("工单",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			objid: commonLogic.appcommonhandle("位置",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			woid: commonLogic.appcommonhandle("工单",null),
			kpid: commonLogic.appcommonhandle("关键点",null),
		},
			views: {
				kprcdeditview: {
					caption: commonLogic.appcommonhandle("关键点记录",null),
					title: commonLogic.appcommonhandle("关键点记录编辑视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("关键点记录",null),
					title: commonLogic.appcommonhandle("关键点记录表格视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("关键点记录信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("关键点记录标识",null), 
					srfmajortext: commonLogic.appcommonhandle("关键点记录名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					kpname: commonLogic.appcommonhandle("关键点",null), 
					woname: commonLogic.appcommonhandle("工单",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					edate: commonLogic.appcommonhandle("采集时间",null), 
					val: commonLogic.appcommonhandle("值",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					woid: commonLogic.appcommonhandle("工单",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
					emeqkprcdid: commonLogic.appcommonhandle("关键点记录标识",null), 
					kpid: commonLogic.appcommonhandle("关键点",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					kpname: commonLogic.appcommonhandle("关键点",null),
					woname: commonLogic.appcommonhandle("工单",null),
					edate: commonLogic.appcommonhandle("采集时间",null),
					val: commonLogic.appcommonhandle("值",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_equipname_like: commonLogic.appcommonhandle("设备(文本包含(%))",null), 
					n_objname_like: commonLogic.appcommonhandle("位置(文本包含(%))",null), 
					n_kpname_like: commonLogic.appcommonhandle("关键点(文本包含(%))",null), 
					n_woname_like: commonLogic.appcommonhandle("工单(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			kprcdeditviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;