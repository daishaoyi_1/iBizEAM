import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			description: commonLogic.appcommonhandle("描述",null),
			emplanid: commonLogic.appcommonhandle("计划编号",null),
			mdate: commonLogic.appcommonhandle("制定时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			prefee: commonLogic.appcommonhandle("预算(￥)",null),
			plandesc: commonLogic.appcommonhandle("计划内容",null),
			mtflag: commonLogic.appcommonhandle("多任务?",null),
			rempname: commonLogic.appcommonhandle("责任人",null),
			plancvl: commonLogic.appcommonhandle("计划周期(天)",null),
			mpersonname: commonLogic.appcommonhandle("制定人",null),
			emplanname: commonLogic.appcommonhandle("计划名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			mpersonid: commonLogic.appcommonhandle("制定人",null),
			emwotype: commonLogic.appcommonhandle("生成工单种类",null),
			rempid: commonLogic.appcommonhandle("责任人",null),
			plantype: commonLogic.appcommonhandle("计划类型",null),
			content: commonLogic.appcommonhandle("详细内容",null),
			rdeptname: commonLogic.appcommonhandle("责任部门",null),
			planinfo: commonLogic.appcommonhandle("计划信息",null),
			rdeptid: commonLogic.appcommonhandle("责任部门",null),
			eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
			recvpersonid: commonLogic.appcommonhandle("接收人",null),
			archive: commonLogic.appcommonhandle("归档",null),
			activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
			planstate: commonLogic.appcommonhandle("计划状态",null),
			recvpersonname: commonLogic.appcommonhandle("接收人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			dpname: commonLogic.appcommonhandle("测点",null),
			acclassname: commonLogic.appcommonhandle("总帐科目",null),
			dptype: commonLogic.appcommonhandle("测点类型",null),
			plantemplname: commonLogic.appcommonhandle("计划模板",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			objname: commonLogic.appcommonhandle("位置",null),
			rteamname: commonLogic.appcommonhandle("责任班组",null),
			rservicename: commonLogic.appcommonhandle("服务商",null),
			rserviceid: commonLogic.appcommonhandle("服务商",null),
			objid: commonLogic.appcommonhandle("位置",null),
			dpid: commonLogic.appcommonhandle("测点",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			acclassid: commonLogic.appcommonhandle("总帐科目",null),
			plantemplid: commonLogic.appcommonhandle("计划模板",null),
			rteamid: commonLogic.appcommonhandle("责任班组",null),
		},
			views: {
				editview_editmode: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划数据选择视图",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划选项操作视图",null),
				},
				cardview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划数据视图",null),
				},
				eqinfo: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划",null),
				},
				personinfo: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划",null),
				},
				dashboardview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划数据看板视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划选择表格视图",null),
				},
				equipgridview9: {
					caption: commonLogic.appcommonhandle("计划",null),
					title: commonLogic.appcommonhandle("计划表格视图",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("计划信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划编号",null), 
					srfmajortext: commonLogic.appcommonhandle("计划名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emplanid: commonLogic.appcommonhandle("计划编号(自动)",null), 
					emplanname: commonLogic.appcommonhandle("计划名称",null), 
					plantemplname: commonLogic.appcommonhandle("计划模板",null), 
					planstate: commonLogic.appcommonhandle("计划状态",null), 
					plantype: commonLogic.appcommonhandle("计划类型",null), 
					emwotype: commonLogic.appcommonhandle("生成工单种类",null), 
					mtflag: commonLogic.appcommonhandle("多任务?",null), 
					mdate: commonLogic.appcommonhandle("制定时间",null), 
					plancvl: commonLogic.appcommonhandle("计划周期(天)",null), 
					prefee: commonLogic.appcommonhandle("预算(￥)",null), 
					activelengths: commonLogic.appcommonhandle("持续时间(H)",null), 
					archive: commonLogic.appcommonhandle("归档",null), 
					plandesc: commonLogic.appcommonhandle("计划内容",null), 
				},
				uiactions: {
				},
			},
			eqinfo_form: {
				details: {
					grouppanel15: commonLogic.appcommonhandle("对象信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划编号",null), 
					srfmajortext: commonLogic.appcommonhandle("计划名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null), 
					dpname: commonLogic.appcommonhandle("测点",null), 
					emplanid: commonLogic.appcommonhandle("计划编号",null), 
				},
				uiactions: {
				},
			},
			personinfo_form: {
				details: {
					grouppanel20: commonLogic.appcommonhandle("责任信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划编号",null), 
					srfmajortext: commonLogic.appcommonhandle("计划名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					mpersonname: commonLogic.appcommonhandle("制定人",null), 
					mpersonid: commonLogic.appcommonhandle("制定人",null), 
					rempname: commonLogic.appcommonhandle("责任人",null), 
					rempid: commonLogic.appcommonhandle("责任人",null), 
					rdeptname: commonLogic.appcommonhandle("责任部门",null), 
					rteamname: commonLogic.appcommonhandle("责任班组",null), 
					rservicename: commonLogic.appcommonhandle("服务商",null), 
					recvpersonid: commonLogic.appcommonhandle("接收人",null), 
					recvpersonname: commonLogic.appcommonhandle("接收人",null), 
					emplanid: commonLogic.appcommonhandle("计划编号",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("计划信息",null), 
					grouppanel15: commonLogic.appcommonhandle("对象信息",null), 
					grouppanel20: commonLogic.appcommonhandle("责任信息",null), 
					grouppanel27: commonLogic.appcommonhandle("归档信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划编号",null), 
					srfmajortext: commonLogic.appcommonhandle("计划名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emplanid: commonLogic.appcommonhandle("计划编号(自动)",null), 
					plantemplname: commonLogic.appcommonhandle("计划模板",null), 
					emplanname: commonLogic.appcommonhandle("计划名称",null), 
					plantype: commonLogic.appcommonhandle("计划类型",null), 
					planstate: commonLogic.appcommonhandle("计划状态",null), 
					mdate: commonLogic.appcommonhandle("制定时间",null), 
					prefee: commonLogic.appcommonhandle("预算(￥)",null), 
					activelengths: commonLogic.appcommonhandle("持续时间(H)",null), 
					mtflag: commonLogic.appcommonhandle("多任务?",null), 
					emwotype: commonLogic.appcommonhandle("生成工单种类",null), 
					plancvl: commonLogic.appcommonhandle("计划周期(天)",null), 
					plandesc: commonLogic.appcommonhandle("计划内容",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null), 
					dpname: commonLogic.appcommonhandle("测点",null), 
					mpersonid: commonLogic.appcommonhandle("制定人",null), 
					mpersonname: commonLogic.appcommonhandle("制定人",null), 
					rempid: commonLogic.appcommonhandle("责任人",null), 
					rempname: commonLogic.appcommonhandle("责任人",null), 
					rdeptname: commonLogic.appcommonhandle("责任部门",null), 
					rteamname: commonLogic.appcommonhandle("责任班组",null), 
					rservicename: commonLogic.appcommonhandle("服务商",null), 
					recvpersonid: commonLogic.appcommonhandle("接收人",null), 
					recvpersonname: commonLogic.appcommonhandle("接收人",null), 
					archive: commonLogic.appcommonhandle("归档",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					rserviceid: commonLogic.appcommonhandle("服务商",null), 
					plantemplid: commonLogic.appcommonhandle("计划模板",null), 
					dpid: commonLogic.appcommonhandle("测点",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
					rteamid: commonLogic.appcommonhandle("责任班组",null), 
				},
				uiactions: {
				},
			},
			quickcreate_form: {
				details: {
					group1: commonLogic.appcommonhandle("计划基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划编号",null), 
					srfmajortext: commonLogic.appcommonhandle("计划名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emplanname: commonLogic.appcommonhandle("计划名称",null), 
					plantype: commonLogic.appcommonhandle("计划类型",null), 
					planstate: commonLogic.appcommonhandle("计划状态",null), 
					rempid: commonLogic.appcommonhandle("责任人",null), 
					rempname: commonLogic.appcommonhandle("责任人",null), 
					rdeptname: commonLogic.appcommonhandle("责任部门",null), 
					emplanid: commonLogic.appcommonhandle("计划编号",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					emplanid: commonLogic.appcommonhandle("计划编号",null),
					emplanname: commonLogic.appcommonhandle("计划名称",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					plantype: commonLogic.appcommonhandle("计划类型",null),
					mtflag: commonLogic.appcommonhandle("多任务?",null),
					mpersonname: commonLogic.appcommonhandle("制定人",null),
					mdate: commonLogic.appcommonhandle("制定时间",null),
					planstate: commonLogic.appcommonhandle("计划状态",null),
					uagridcolumn1: commonLogic.appcommonhandle("操作",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				emplan_openeditview: commonLogic.appcommonhandle("编辑",null),
				emplan_remove: commonLogic.appcommonhandle("删除",null),
				},
			},
			main3_grid: {
				columns: {
					emplanid: commonLogic.appcommonhandle("计划编号",null),
					emplanname: commonLogic.appcommonhandle("计划名称",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					plantype: commonLogic.appcommonhandle("计划类型",null),
					planstate: commonLogic.appcommonhandle("计划状态",null),
					rempname: commonLogic.appcommonhandle("责任人",null),
					rdeptname: commonLogic.appcommonhandle("责任部门",null),
					rteamname: commonLogic.appcommonhandle("责任班组",null),
					rservicename: commonLogic.appcommonhandle("服务商",null),
					recvpersonname: commonLogic.appcommonhandle("接收人",null),
					mpersonname: commonLogic.appcommonhandle("制定人",null),
					mdate: commonLogic.appcommonhandle("制定时间",null),
					activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
					dpname: commonLogic.appcommonhandle("测点",null),
					dptype: commonLogic.appcommonhandle("测点类型",null),
					plandesc: commonLogic.appcommonhandle("计划内容",null),
					mtflag: commonLogic.appcommonhandle("多任务?",null),
					prefee: commonLogic.appcommonhandle("预算(￥)",null),
					archive: commonLogic.appcommonhandle("归档",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			card_dataview: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emplanname_like: commonLogic.appcommonhandle("计划名称(%)",null), 
					n_plantype_eq: commonLogic.appcommonhandle("计划类型(=)",null), 
					n_planstate_eq: commonLogic.appcommonhandle("计划状态(=)",null), 
				},
				uiactions: {
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			equipgridview9toolbar_toolbar: {
			},
			gridviewtoolbar_toolbar: {
				tbitem1_openquickcreate: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			cardviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("计划信息",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("计划条件",null),
					}
				},
				uiactions: {
				},
			},
			dashboardviewdashboard_container1_portlet: {
				uiactions: {
				},
			},
			dashboardviewdashboard_container2_portlet: {
				uiactions: {
				},
			},
			info_portlet: {
				info: {
					title: commonLogic.appcommonhandle("计划信息", null)
			  	},
				uiactions: {
				},
			},
			eqinfo_portlet: {
				eqinfo: {
					title: commonLogic.appcommonhandle("设备信息", null)
			  	},
				uiactions: {
				},
			},
			personinfo_portlet: {
				personinfo: {
					title: commonLogic.appcommonhandle("责任信息", null)
			  	},
				uiactions: {
				},
			},
			dashboardviewdashboard_container3_portlet: {
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;