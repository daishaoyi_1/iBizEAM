import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			emoutputid: commonLogic.appcommonhandle("能力标识",null),
			outputcode: commonLogic.appcommonhandle("能力代码",null),
			description: commonLogic.appcommonhandle("描述",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			emoutputname: commonLogic.appcommonhandle("能力名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			outputinfo: commonLogic.appcommonhandle("能力信息",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("能力",null),
					title: commonLogic.appcommonhandle("能力选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("能力",null),
					title: commonLogic.appcommonhandle("能力数据选择视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("能力信息",null),
					title: commonLogic.appcommonhandle("能力信息",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("能力",null),
					title: commonLogic.appcommonhandle("能力",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("能力信息",null),
					title: commonLogic.appcommonhandle("能力信息",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("能力信息",null), 
					grouppanel5: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能力标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能力名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					outputcode: commonLogic.appcommonhandle("能力代码",null), 
					emoutputname: commonLogic.appcommonhandle("能力名称",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emoutputid: commonLogic.appcommonhandle("能力标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("能力信息",null), 
					grouppanel5: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能力标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能力名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					outputcode: commonLogic.appcommonhandle("能力代码",null), 
					emoutputname: commonLogic.appcommonhandle("能力名称",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emoutputid: commonLogic.appcommonhandle("能力标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					outputinfo: commonLogic.appcommonhandle("能力信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					outputcode: commonLogic.appcommonhandle("能力代码",null),
					emoutputname: commonLogic.appcommonhandle("能力名称",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;