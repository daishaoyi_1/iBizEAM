import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updateman: commonLogic.appcommonhandle("更新人",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emrfodemapid: commonLogic.appcommonhandle("现象引用标识",null),
			description: commonLogic.appcommonhandle("描述",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			emrfodemapname: commonLogic.appcommonhandle("现象引用名称",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			rfodename: commonLogic.appcommonhandle("现象",null),
			refobjname: commonLogic.appcommonhandle("现象引用",null),
			rfodeid: commonLogic.appcommonhandle("现象",null),
			refobjid: commonLogic.appcommonhandle("现象引用",null),
		},
			views: {
				dataview: {
					caption: commonLogic.appcommonhandle("现象引用",null),
					title: commonLogic.appcommonhandle("现象引用数据视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("现象引用",null),
					title: commonLogic.appcommonhandle("现象引用编辑视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("现象引用信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("现象引用标识",null), 
					srfmajortext: commonLogic.appcommonhandle("现象引用名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					rfodename: commonLogic.appcommonhandle("现象",null), 
					refobjname: commonLogic.appcommonhandle("现象引用",null), 
					rfodeid: commonLogic.appcommonhandle("现象",null), 
					refobjid: commonLogic.appcommonhandle("现象引用",null), 
					emrfodemapid: commonLogic.appcommonhandle("现象引用标识",null), 
				},
				uiactions: {
				},
			},
			card_dataview: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			dataviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;