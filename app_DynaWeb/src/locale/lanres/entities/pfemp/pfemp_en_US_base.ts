import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updateman: commonLogic.appcommonhandle("更新人",null),
			description: commonLogic.appcommonhandle("描述",null),
			tel: commonLogic.appcommonhandle("联系电话",null),
			majordeptid: commonLogic.appcommonhandle("主部门",null),
			workdate: commonLogic.appcommonhandle("工作日期",null),
			pfempname: commonLogic.appcommonhandle("职员名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			raisedate: commonLogic.appcommonhandle("入本企业日期",null),
			hometel: commonLogic.appcommonhandle("家庭电话",null),
			birthday: commonLogic.appcommonhandle("出生日期",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			pfempid: commonLogic.appcommonhandle("职员标识",null),
			homeaddr: commonLogic.appcommonhandle("家庭地址",null),
			empinfo: commonLogic.appcommonhandle("职员信息",null),
			majordeptname: commonLogic.appcommonhandle("主部门",null),
			deptid: commonLogic.appcommonhandle("部门",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			empsex: commonLogic.appcommonhandle("性别",null),
			certcode: commonLogic.appcommonhandle("证件号码",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			empcode: commonLogic.appcommonhandle("职员代码",null),
			psw: commonLogic.appcommonhandle("口令",null),
			teamid: commonLogic.appcommonhandle("班组",null),
			maindeptcode: commonLogic.appcommonhandle("主部门代码",null),
			e_mail: commonLogic.appcommonhandle("电子邮件",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			cell: commonLogic.appcommonhandle("移动电话",null),
			addr: commonLogic.appcommonhandle("联系地址",null),
			majorteamname: commonLogic.appcommonhandle("主班组",null),
			majorteamid: commonLogic.appcommonhandle("主班组",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("职员",null),
					title: commonLogic.appcommonhandle("职员选择表格视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("职员",null),
					title: commonLogic.appcommonhandle("职员",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("职员",null),
					title: commonLogic.appcommonhandle("职员",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("职员",null),
					title: commonLogic.appcommonhandle("职员数据选择视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("职员",null),
					title: commonLogic.appcommonhandle("职员",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("职员",null),
					title: commonLogic.appcommonhandle("职员树导航视图",null),
				},
				deptempgridview: {
					caption: commonLogic.appcommonhandle("职员",null),
					title: commonLogic.appcommonhandle("职员",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("PF职员信息",null), 
					grouppanel17: commonLogic.appcommonhandle("附属信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("职员标识",null), 
					srfmajortext: commonLogic.appcommonhandle("职员名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					pfempid: commonLogic.appcommonhandle("职员标识",null), 
					empcode: commonLogic.appcommonhandle("职员代码",null), 
					pfempname: commonLogic.appcommonhandle("职员名称",null), 
					psw: commonLogic.appcommonhandle("口令",null), 
					empsex: commonLogic.appcommonhandle("性别",null), 
					certcode: commonLogic.appcommonhandle("证件号码",null), 
					birthday: commonLogic.appcommonhandle("出生日期",null), 
					workdate: commonLogic.appcommonhandle("工作日期",null), 
					raisedate: commonLogic.appcommonhandle("入本企业日期",null), 
					hometel: commonLogic.appcommonhandle("家庭电话",null), 
					tel: commonLogic.appcommonhandle("联系电话",null), 
					cell: commonLogic.appcommonhandle("移动电话",null), 
					majorteamname: commonLogic.appcommonhandle("主班组",null), 
					majordeptname: commonLogic.appcommonhandle("主部门",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					e_mail: commonLogic.appcommonhandle("电子邮件",null), 
					addr: commonLogic.appcommonhandle("联系地址",null), 
					homeaddr: commonLogic.appcommonhandle("家庭地址",null), 
					description: commonLogic.appcommonhandle("描述",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("PF职员信息",null), 
					grouppanel17: commonLogic.appcommonhandle("附属信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("职员标识",null), 
					srfmajortext: commonLogic.appcommonhandle("职员名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					pfempid: commonLogic.appcommonhandle("职员标识",null), 
					empcode: commonLogic.appcommonhandle("职员代码",null), 
					pfempname: commonLogic.appcommonhandle("职员名称",null), 
					psw: commonLogic.appcommonhandle("口令",null), 
					empsex: commonLogic.appcommonhandle("性别",null), 
					certcode: commonLogic.appcommonhandle("证件号码",null), 
					birthday: commonLogic.appcommonhandle("出生日期",null), 
					workdate: commonLogic.appcommonhandle("工作日期",null), 
					raisedate: commonLogic.appcommonhandle("入本企业日期",null), 
					hometel: commonLogic.appcommonhandle("家庭电话",null), 
					tel: commonLogic.appcommonhandle("联系电话",null), 
					cell: commonLogic.appcommonhandle("移动电话",null), 
					majorteamname: commonLogic.appcommonhandle("主班组",null), 
					majordeptname: commonLogic.appcommonhandle("主部门",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					e_mail: commonLogic.appcommonhandle("电子邮件",null), 
					addr: commonLogic.appcommonhandle("联系地址",null), 
					homeaddr: commonLogic.appcommonhandle("家庭地址",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					majorteamid: commonLogic.appcommonhandle("主班组",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					empcode: commonLogic.appcommonhandle("职员代码",null),
					pfempname: commonLogic.appcommonhandle("职员名称",null),
					majordeptname: commonLogic.appcommonhandle("主部门",null),
					majorteamname: commonLogic.appcommonhandle("主班组",null),
					certcode: commonLogic.appcommonhandle("证件号码",null),
					empsex: commonLogic.appcommonhandle("性别",null),
					birthday: commonLogic.appcommonhandle("出生日期",null),
					tel: commonLogic.appcommonhandle("联系电话",null),
					cell: commonLogic.appcommonhandle("移动电话",null),
					addr: commonLogic.appcommonhandle("联系地址",null),
					e_mail: commonLogic.appcommonhandle("电子邮件",null),
					orgid: commonLogic.appcommonhandle("组织",null),
					workdate: commonLogic.appcommonhandle("工作日期",null),
					raisedate: commonLogic.appcommonhandle("入本企业日期",null),
					hometel: commonLogic.appcommonhandle("家庭电话",null),
					homeaddr: commonLogic.appcommonhandle("家庭地址",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			deptempgridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;