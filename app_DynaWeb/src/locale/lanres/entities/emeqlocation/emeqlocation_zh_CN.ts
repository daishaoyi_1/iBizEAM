import EMEQLocation_zh_CN_Base from './emeqlocation_zh_CN_base';

function getLocaleResource(){
    const EMEQLocation_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQLocation_zh_CN_Base(), EMEQLocation_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;