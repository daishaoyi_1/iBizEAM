import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updateman: commonLogic.appcommonhandle("更新人",null),
			bdate: commonLogic.appcommonhandle("运行区间起始",null),
			nval: commonLogic.appcommonhandle("运行时间(H)",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			edate: commonLogic.appcommonhandle("运行区间截至",null),
			emeqwlname: commonLogic.appcommonhandle("设备运行日志名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			emeqwlid: commonLogic.appcommonhandle("设备运行日志标识",null),
			woname: commonLogic.appcommonhandle("工单",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			objname: commonLogic.appcommonhandle("位置",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			woid: commonLogic.appcommonhandle("工单",null),
			objid: commonLogic.appcommonhandle("位置",null),
		},
			views: {
				eqgridview: {
					caption: commonLogic.appcommonhandle("设备运行日志",null),
					title: commonLogic.appcommonhandle("设备运行日志表格视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("设备运行日志",null),
					title: commonLogic.appcommonhandle("设备运行日志表格视图",null),
				},
				wleditview: {
					caption: commonLogic.appcommonhandle("设备运行日志",null),
					title: commonLogic.appcommonhandle("设备运行日志编辑视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("设备运行日志信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备运行日志标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备运行日志名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					bdate: commonLogic.appcommonhandle("运行区间起始",null), 
					edate: commonLogic.appcommonhandle("运行区间截至",null), 
					nval: commonLogic.appcommonhandle("运行时间(H)",null), 
					woname: commonLogic.appcommonhandle("工单",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					emeqwlid: commonLogic.appcommonhandle("设备运行日志标识",null), 
					woid: commonLogic.appcommonhandle("工单",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					nval: commonLogic.appcommonhandle("运行时间(H)",null),
					bdate: commonLogic.appcommonhandle("运行区间起始",null),
					edate: commonLogic.appcommonhandle("运行区间截至",null),
					woname: commonLogic.appcommonhandle("工单",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			nearest30daybyeq_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_equipname_like: commonLogic.appcommonhandle("设备(文本包含(%))",null), 
					n_objname_like: commonLogic.appcommonhandle("位置(文本包含(%))",null), 
					n_woname_like: commonLogic.appcommonhandle("工单(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			wleditviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			eqgridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			nearest30daybyeq_portlet: {
				nearest30daybyeq: {
					title: commonLogic.appcommonhandle("最近30天运行", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;