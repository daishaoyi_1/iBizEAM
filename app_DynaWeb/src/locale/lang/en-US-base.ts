import enUSUser from '../user/en-US.user';
import { Util } from '@/utils/util/util';
import emobjmap_en_US from '@locale/lanres/entities/emobj-map/emobj-map_en_US';
import emeqwl_en_US from '@locale/lanres/entities/emeqwl/emeqwl_en_US';
import emresitem_en_US from '@locale/lanres/entities/emres-item/emres-item_en_US';
import emwork_en_US from '@locale/lanres/entities/emwork/emwork_en_US';
import emeqmpmtr_en_US from '@locale/lanres/entities/emeqmpmtr/emeqmpmtr_en_US';
import emeqlctmap_en_US from '@locale/lanres/entities/emeqlctmap/emeqlctmap_en_US';
import emeqmp_en_US from '@locale/lanres/entities/emeqmp/emeqmp_en_US';
import emeqkprcd_en_US from '@locale/lanres/entities/emeqkprcd/emeqkprcd_en_US';
import emdprct_en_US from '@locale/lanres/entities/emdprct/emdprct_en_US';
import emeqkpmap_en_US from '@locale/lanres/entities/emeqkpmap/emeqkpmap_en_US';
import emplan_en_US from '@locale/lanres/entities/emplan/emplan_en_US';
import emeqtype_en_US from '@locale/lanres/entities/emeqtype/emeqtype_en_US';
import emitem_en_US from '@locale/lanres/entities/emitem/emitem_en_US';
import emequip_en_US from '@locale/lanres/entities/emequip/emequip_en_US';
import emdrwgmap_en_US from '@locale/lanres/entities/emdrwgmap/emdrwgmap_en_US';
import emeqmonitor_en_US from '@locale/lanres/entities/emeqmonitor/emeqmonitor_en_US';
import emcab_en_US from '@locale/lanres/entities/emcab/emcab_en_US';
import emrfodemap_en_US from '@locale/lanres/entities/emrfodemap/emrfodemap_en_US';
import emeqlocation_en_US from '@locale/lanres/entities/emeqlocation/emeqlocation_en_US';
import emservice_en_US from '@locale/lanres/entities/emservice/emservice_en_US';
import emitemtype_en_US from '@locale/lanres/entities/emitem-type/emitem-type_en_US';
import emeqkp_en_US from '@locale/lanres/entities/emeqkp/emeqkp_en_US';
import emstorepart_en_US from '@locale/lanres/entities/emstore-part/emstore-part_en_US';
import empodetail_en_US from '@locale/lanres/entities/empodetail/empodetail_en_US';
import emobject_en_US from '@locale/lanres/entities/emobject/emobject_en_US';
import emapply_en_US from '@locale/lanres/entities/emapply/emapply_en_US';
import emeqlctgss_en_US from '@locale/lanres/entities/emeqlctgss/emeqlctgss_en_US';
import emwo_osc_en_US from '@locale/lanres/entities/emwo-osc/emwo-osc_en_US';
import emmachinecategory_en_US from '@locale/lanres/entities/emmachine-category/emmachine-category_en_US';
import emdrwg_en_US from '@locale/lanres/entities/emdrwg/emdrwg_en_US';
import emrfode_en_US from '@locale/lanres/entities/emrfode/emrfode_en_US';
import emstore_en_US from '@locale/lanres/entities/emstore/emstore_en_US';
import emeqdebug_en_US from '@locale/lanres/entities/emeqdebug/emeqdebug_en_US';
import emeqlctfdj_en_US from '@locale/lanres/entities/emeqlctfdj/emeqlctfdj_en_US';
import emwplistcost_en_US from '@locale/lanres/entities/emwplist-cost/emwplist-cost_en_US';
import pfteam_en_US from '@locale/lanres/entities/pfteam/pfteam_en_US';
import pfemppostmap_en_US from '@locale/lanres/entities/pfemp-post-map/pfemp-post-map_en_US';
import emacclass_en_US from '@locale/lanres/entities/emacclass/emacclass_en_US';
import pfcontract_en_US from '@locale/lanres/entities/pfcontract/pfcontract_en_US';
import emeitires_en_US from '@locale/lanres/entities/emeitires/emeitires_en_US';
import emoutput_en_US from '@locale/lanres/entities/emoutput/emoutput_en_US';
import emrfodetype_en_US from '@locale/lanres/entities/emrfodetype/emrfodetype_en_US';
import dynachart_en_US from '@locale/lanres/entities/dyna-chart/dyna-chart_en_US';
import empurplan_en_US from '@locale/lanres/entities/empur-plan/empur-plan_en_US';
import emproduct_en_US from '@locale/lanres/entities/emproduct/emproduct_en_US';
import embrand_en_US from '@locale/lanres/entities/embrand/embrand_en_US';
import pfdept_en_US from '@locale/lanres/entities/pfdept/pfdept_en_US';
import emeqsparedetail_en_US from '@locale/lanres/entities/emeqspare-detail/emeqspare-detail_en_US';
import pfemp_en_US from '@locale/lanres/entities/pfemp/pfemp_en_US';
import emstock_en_US from '@locale/lanres/entities/emstock/emstock_en_US';
import dynadashboard_en_US from '@locale/lanres/entities/dyna-dashboard/dyna-dashboard_en_US';
import emwoori_en_US from '@locale/lanres/entities/emwoori/emwoori_en_US';
import emplandetail_en_US from '@locale/lanres/entities/emplan-detail/emplan-detail_en_US';
import emplantempl_en_US from '@locale/lanres/entities/emplan-templ/emplan-templ_en_US';
import emwo_dp_en_US from '@locale/lanres/entities/emwo-dp/emwo-dp_en_US';
import emen_en_US from '@locale/lanres/entities/emen/emen_en_US';
import empo_en_US from '@locale/lanres/entities/empo/empo_en_US';
import pfunit_en_US from '@locale/lanres/entities/pfunit/pfunit_en_US';
import emitemprtn_en_US from '@locale/lanres/entities/emitem-prtn/emitem-prtn_en_US';
import emberth_en_US from '@locale/lanres/entities/emberth/emberth_en_US';
import emeqlcttires_en_US from '@locale/lanres/entities/emeqlcttires/emeqlcttires_en_US';
import emitemrout_en_US from '@locale/lanres/entities/emitem-rout/emitem-rout_en_US';
import emeqkeep_en_US from '@locale/lanres/entities/emeqkeep/emeqkeep_en_US';
import emitemrin_en_US from '@locale/lanres/entities/emitem-rin/emitem-rin_en_US';
import emeqmaintance_en_US from '@locale/lanres/entities/emeqmaintance/emeqmaintance_en_US';
import emitempuse_en_US from '@locale/lanres/entities/emitem-puse/emitem-puse_en_US';
import emeqsetup_en_US from '@locale/lanres/entities/emeqsetup/emeqsetup_en_US';
import emitemtrade_en_US from '@locale/lanres/entities/emitem-trade/emitem-trade_en_US';
import emwo_en_en_US from '@locale/lanres/entities/emwo-en/emwo-en_en_US';
import emrfoac_en_US from '@locale/lanres/entities/emrfoac/emrfoac_en_US';
import emassetclear_en_US from '@locale/lanres/entities/emasset-clear/emasset-clear_en_US';
import emmachmodel_en_US from '@locale/lanres/entities/emmach-model/emmach-model_en_US';
import emassetclass_en_US from '@locale/lanres/entities/emasset-class/emasset-class_en_US';
import emeqspare_en_US from '@locale/lanres/entities/emeqspare/emeqspare_en_US';
import emitempl_en_US from '@locale/lanres/entities/emitem-pl/emitem-pl_en_US';
import emeqah_en_US from '@locale/lanres/entities/emeqah/emeqah_en_US';
import emwo_en_US from '@locale/lanres/entities/emwo/emwo_en_US';
import emeqlctrhy_en_US from '@locale/lanres/entities/emeqlctrhy/emeqlctrhy_en_US';
import emitemcs_en_US from '@locale/lanres/entities/emitem-cs/emitem-cs_en_US';
import emenconsum_en_US from '@locale/lanres/entities/emenconsum/emenconsum_en_US';
import emserviceevl_en_US from '@locale/lanres/entities/emservice-evl/emservice-evl_en_US';
import emplancdt_en_US from '@locale/lanres/entities/emplan-cdt/emplan-cdt_en_US';
import emeqsparemap_en_US from '@locale/lanres/entities/emeqspare-map/emeqspare-map_en_US';
import emoutputrct_en_US from '@locale/lanres/entities/emoutput-rct/emoutput-rct_en_US';
import emrfoca_en_US from '@locale/lanres/entities/emrfoca/emrfoca_en_US';
import emwo_inner_en_US from '@locale/lanres/entities/emwo-inner/emwo-inner_en_US';
import emwplist_en_US from '@locale/lanres/entities/emwplist/emwplist_en_US';
import emrfomo_en_US from '@locale/lanres/entities/emrfomo/emrfomo_en_US';
import emasset_en_US from '@locale/lanres/entities/emasset/emasset_en_US';
import emeqcheck_en_US from '@locale/lanres/entities/emeqcheck/emeqcheck_en_US';
import components_en_US from '@locale/lanres/components/components_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';
import commonLogic from '@/locale/logic/common/common-logic';

function getAppLocale(){
    const data: any = {
        app: {
            directoryTree:{
                loading: "Data loading",
                placeholder: "Filename",
                noData: "No Data"
            },
            fullTextSearch:{
                placeholder: "The full text retrieval",
                pathError: "Configure the full-text retrieval route",
                noticeTitle: "The full text retrieval",
                noticeDesc: "The keyword cannot be less than 1",
                multiFormDEField: "Multiple form properties are not configured",
                redirectService: "The redirect service does not exist!",
                findRedirectView: "The redirect view was not found",
                redirectConfiguration: "The redirect view is not configured",
            },
            commonWords:{
                error: "Error",
                success: "Success",
                ok: "OK",
                cancel: "Cancel",
                save: "Save",
                codeNotExist: 'Code list does not exist',
                reqException: "Request exception",
                sysException: "System abnormality",
                warning: "Warning",
                wrong: "Error",
                rulesException: "Abnormal value check rule",
                saveSuccess: "Saved successfully",
                saveFailed: "Save failed",
                deleteSuccess: "Successfully deleted!",
                deleteError: "Failed to delete",
                delDataFail: "Failed to delete data",
                noData: "No data",
                startsuccess:"Start successful",
                createFailed: 'Unable to create',
                isExist: 'existed',
                valueNotEmpty: 'value is not empty',
                required: 'is Required',
                valueMustBe: 'value must be',
                number: 'Number',
                string: 'String',
                type: 'Type'
            },
            local:{
                new: "New",
                add: "Add",
            },
            gridpage: {
                choicecolumns: "Choice columns",
                refresh: "refresh",
                show: "Show",
                records: "records",
                totle: "totle",
                noData: "No data",
                valueVail: "Value cannot be empty",
                notConfig: {
                    fetchAction: "The view table fetchaction parameter is not configured",
                    removeAction: "The view table removeaction parameter is not configured",
                    createAction: "The view table createaction parameter is not configured",
                    updateAction: "The view table updateaction parameter is not configured",
                    loaddraftAction: "The view table loadtrafaction parameter is not configured",
                },
                data: "Data",
                delDataFail: "Failed to delete data",
                delSuccess: "Delete successfully!",
                confirmDel: "Are you sure you want to delete",
                notRecoverable: "delete will not be recoverable?",
                notBatch: "Batch addition not implemented",
                grid: "Grid",
                exportFail: "Data export failed",
                sum: "Total",
                formitemFailed: "Form item update failed",
            },
            list: {
                notConfig: {
                    fetchAction: "View list fetchAction parameter is not configured",
                    removeAction: "View table removeAction parameter is not configured",
                    createAction: "View list createAction parameter is not configured",
                    updateAction: "View list updateAction parameter is not configured",
                },
                confirmDel: "Are you sure you want to delete",
                notRecoverable: "delete will not be recoverable?",
            },
            listExpBar: {
                title: "List navigation bar",
            },
            wfExpBar: {
                title: "Process navigation bar",
            },
            calendarExpBar:{
                title: "Calendar navigation bar",
            },
            treeExpBar: {
                title: "Tree view navigation bar",
            },
            portlet: {
                noExtensions: "No extensions",
            },
            tabpage: {
                sureclosetip: {
                    title: "Close warning",
                    content: "Form data Changed, are sure close?",
                },
                closeall: "Close all",
                closeother: "Close other",
            },
            fileUpload: {
                caption: "Upload",
                uploading: "Uploading...",
            },
            searchButton: {
                search: "Search",
                reset: "Reset",
            },
            calendar:{
            today: "today",
            month: "month",
            week: "week",
            day: "day",
            list: "list",
            dateSelectModalTitle: "select the time you wanted",
            gotoDate: "goto",
            from: "From",
            to: "To",
            },
            // 非实体视图
            views: {
                appindex: {
                    caption: commonLogic.appcommonhandle("iBizEAM",null),
                    title: commonLogic.appcommonhandle("iBizEAM",null),
                },
                appportalview: {
                    caption: commonLogic.appcommonhandle("",null),
                    title: commonLogic.appcommonhandle("应用门户视图",null),
                },
                eamindexview: {
                    caption: commonLogic.appcommonhandle("设备资产管理",null),
                    title: commonLogic.appcommonhandle("设备资产管理首页视图",null),
                },
            },
            utilview:{
                importview:"Import Data",
                warning:"warning",
                info:"Please configure the data import item"    
            },
            menus: {
                indexview: {
                    menuitem61: commonLogic.appcommonhandle("工作台",null),
                    menuitem3: commonLogic.appcommonhandle("设备",null),
                    menuitem1: commonLogic.appcommonhandle("设备类型",null),
                    menuitem8: commonLogic.appcommonhandle("设备档案",null),
                    menuitem5: commonLogic.appcommonhandle("位置",null),
                    menuitem6: commonLogic.appcommonhandle("文档",null),
                    menuitem7: commonLogic.appcommonhandle("备件包",null),
                    menuitem72: commonLogic.appcommonhandle("运行",null),
                    menuitem73: commonLogic.appcommonhandle("运行日志",null),
                    menuitem74: commonLogic.appcommonhandle("运行监控",null),
                    menuitem77: commonLogic.appcommonhandle("仪表",null),
                    menuitem78: commonLogic.appcommonhandle("仪表读数",null),
                    menuitem75: commonLogic.appcommonhandle("关键点",null),
                    menuitem76: commonLogic.appcommonhandle("关键点记录",null),
                    menuitem16: commonLogic.appcommonhandle("计划",null),
                    menuitem17: commonLogic.appcommonhandle("计划",null),
                    menuitem18: commonLogic.appcommonhandle("计划模板",null),
                    menuitem33: commonLogic.appcommonhandle("能耗",null),
                    menuitem50: commonLogic.appcommonhandle("能源",null),
                    menuitem55: commonLogic.appcommonhandle("能耗",null),
                    menuitem9: commonLogic.appcommonhandle("工单",null),
                    menuitem69: commonLogic.appcommonhandle("工单日历",null),
                    menuitem11: commonLogic.appcommonhandle("内部工单",null),
                    menuitem12: commonLogic.appcommonhandle("外委工单",null),
                    menuitem13: commonLogic.appcommonhandle("能耗工单",null),
                    menuitem14: commonLogic.appcommonhandle("点检工单",null),
                    menuitem15: commonLogic.appcommonhandle("外委申请",null),
                    menuitem32: commonLogic.appcommonhandle("活动",null),
                    menuitem70: commonLogic.appcommonhandle("活动日历",null),
                    menuitem56: commonLogic.appcommonhandle("更换安装",null),
                    menuitem57: commonLogic.appcommonhandle("事故记录",null),
                    menuitem58: commonLogic.appcommonhandle("维修记录",null),
                    menuitem59: commonLogic.appcommonhandle("抢修记录",null),
                    menuitem60: commonLogic.appcommonhandle("保养记录",null),
                    menuitem31: commonLogic.appcommonhandle("故障",null),
                    menuitem79: commonLogic.appcommonhandle("故障知识库",null),
                    menuitem48: commonLogic.appcommonhandle("现象",null),
                    menuitem47: commonLogic.appcommonhandle("现象分类",null),
                    menuitem49: commonLogic.appcommonhandle("模式",null),
                    menuitem51: commonLogic.appcommonhandle("原因",null),
                    menuitem52: commonLogic.appcommonhandle("方案",null),
                    menuitem30: commonLogic.appcommonhandle("资产",null),
                    menuitem43: commonLogic.appcommonhandle("资产科目",null),
                    menuitem44: commonLogic.appcommonhandle("固定资产台账",null),
                    menuitem45: commonLogic.appcommonhandle("报废资产",null),
                    menuitem46: commonLogic.appcommonhandle("资产盘点记录",null),
                    menuitem4: commonLogic.appcommonhandle("材料",null),
                    menuitem2: commonLogic.appcommonhandle("物品类型",null),
                    menuitem19: commonLogic.appcommonhandle("物品",null),
                    menuitem21: commonLogic.appcommonhandle("库存管理",null),
                    menuitem24: commonLogic.appcommonhandle("损溢单",null),
                    menuitem25: commonLogic.appcommonhandle("调整单",null),
                    menuitem27: commonLogic.appcommonhandle("出库单",null),
                    menuitem23: commonLogic.appcommonhandle("领料单",null),
                    menuitem28: commonLogic.appcommonhandle("还料单",null),
                    menuitem29: commonLogic.appcommonhandle("采购",null),
                    menuitem64: commonLogic.appcommonhandle("采购流程",null),
                    menuitem38: commonLogic.appcommonhandle("服务商",null),
                    menuitem39: commonLogic.appcommonhandle("服务商评估",null),
                    menuitem62: commonLogic.appcommonhandle("预警",null),
                    menuitem63: commonLogic.appcommonhandle("钢丝绳位置超期预警",null),
                },
                eamindexview: {
                    user_menus: commonLogic.appcommonhandle("用户菜单",null),
                    top_menus: commonLogic.appcommonhandle("顶部菜单",null),
                    menuitem60: commonLogic.appcommonhandle("iBiz开放平台",null),
                    menuitem62: commonLogic.appcommonhandle("项目文件",null),
                    menuitem61: commonLogic.appcommonhandle("模型设计工具",null),
                    menuitem63: commonLogic.appcommonhandle("iBiz论坛",null),
                    left_exp: commonLogic.appcommonhandle("左侧菜单",null),
                    menuitem1: commonLogic.appcommonhandle("工作台",null),
                    menuitem2: commonLogic.appcommonhandle("设备",null),
                    menuitem4: commonLogic.appcommonhandle("设备档案",null),
                    menuitem3: commonLogic.appcommonhandle("设备类型",null),
                    menuitem5: commonLogic.appcommonhandle("位置",null),
                    menuitem6: commonLogic.appcommonhandle("文档",null),
                    menuitem7: commonLogic.appcommonhandle("备件包",null),
                    menuitem8: commonLogic.appcommonhandle("运行",null),
                    menuitem9: commonLogic.appcommonhandle("运行日志",null),
                    menuitem10: commonLogic.appcommonhandle("运行监控",null),
                    menuitem11: commonLogic.appcommonhandle("仪表",null),
                    menuitem12: commonLogic.appcommonhandle("仪表读数",null),
                    menuitem13: commonLogic.appcommonhandle("关键点",null),
                    menuitem14: commonLogic.appcommonhandle("关键点记录",null),
                    menuitem15: commonLogic.appcommonhandle("计划",null),
                    menuitem16: commonLogic.appcommonhandle("计划",null),
                    menuitem17: commonLogic.appcommonhandle("计划模板",null),
                    menuitem18: commonLogic.appcommonhandle("能耗",null),
                    menuitem19: commonLogic.appcommonhandle("能源",null),
                    menuitem20: commonLogic.appcommonhandle("能耗",null),
                    menuitem21: commonLogic.appcommonhandle("工单",null),
                    menuitem23: commonLogic.appcommonhandle("内部工单",null),
                    menuitem24: commonLogic.appcommonhandle("外委工单",null),
                    menuitem25: commonLogic.appcommonhandle("能耗工单",null),
                    menuitem26: commonLogic.appcommonhandle("点检工单",null),
                    menuitem28: commonLogic.appcommonhandle("工单日历",null),
                    menuitem27: commonLogic.appcommonhandle("外委申请",null),
                    menuitem36: commonLogic.appcommonhandle("活动",null),
                    menuitem37: commonLogic.appcommonhandle("更换安装",null),
                    menuitem38: commonLogic.appcommonhandle("事故记录",null),
                    menuitem40: commonLogic.appcommonhandle("维修记录",null),
                    menuitem39: commonLogic.appcommonhandle("抢修记录",null),
                    menuitem41: commonLogic.appcommonhandle("保养记录",null),
                    menuitem64: commonLogic.appcommonhandle("活动日历",null),
                    menuitem29: commonLogic.appcommonhandle("故障",null),
                    menuitem30: commonLogic.appcommonhandle("故障知识库",null),
                    menuitem31: commonLogic.appcommonhandle("现象",null),
                    menuitem32: commonLogic.appcommonhandle("现象分类",null),
                    menuitem33: commonLogic.appcommonhandle("模式",null),
                    menuitem34: commonLogic.appcommonhandle("原因",null),
                    menuitem35: commonLogic.appcommonhandle("解决方案",null),
                    menuitem42: commonLogic.appcommonhandle("资产",null),
                    menuitem43: commonLogic.appcommonhandle("资产科目",null),
                    menuitem44: commonLogic.appcommonhandle("固定资产台账",null),
                    menuitem45: commonLogic.appcommonhandle("报废资产",null),
                    menuitem46: commonLogic.appcommonhandle("资产盘点记录",null),
                    menuitem47: commonLogic.appcommonhandle("材料",null),
                    menuitem49: commonLogic.appcommonhandle("物品",null),
                    menuitem48: commonLogic.appcommonhandle("物品类型",null),
                    menuitem65: commonLogic.appcommonhandle("仓库管理",null),
                    menuitem50: commonLogic.appcommonhandle("库存管理",null),
                    menuitem51: commonLogic.appcommonhandle("损溢单",null),
                    menuitem52: commonLogic.appcommonhandle("库位调整单",null),
                    menuitem53: commonLogic.appcommonhandle("出库单",null),
                    menuitem54: commonLogic.appcommonhandle("领料单",null),
                    menuitem55: commonLogic.appcommonhandle("还料单",null),
                    menuitem56: commonLogic.appcommonhandle("采购",null),
                    menuitem57: commonLogic.appcommonhandle("采购流程",null),
                    menuitem58: commonLogic.appcommonhandle("服务商",null),
                    menuitem59: commonLogic.appcommonhandle("服务商评估",null),
                    bottom_exp: commonLogic.appcommonhandle("底部内容",null),
                    footer_left: commonLogic.appcommonhandle("底部左侧",null),
                    footer_center: commonLogic.appcommonhandle("底部中间",null),
                    footer_right: commonLogic.appcommonhandle("底部右侧",null),
                },
                emeqtypetreemenu: {
                    menuitem1: commonLogic.appcommonhandle("添加设备类型",null),
                    seperator1: commonLogic.appcommonhandle("菜单项",null),
                    menuitem2: commonLogic.appcommonhandle("添加设备",null),
                },
            },
            formpage:{
                error: "Error",
                desc1: "Operation failed, failed to find current form item",
                desc2: "Can't continue",
                notconfig: {
                    loadaction: "View form loadAction parameter is not configured",
                    loaddraftaction: "View form loaddraftAction parameter is not configured",
                    actionname: "View form actionName parameter is not configured",
                    removeaction: "View form removeAction parameter is not configured",
                },
                saveerror: "Error saving data",
                savecontent: "The data is inconsistent. The background data may have been modified. Do you want to reload the data?",
                valuecheckex: "Value rule check exception",
                savesuccess: "Saved successfully!",
                deletesuccess: "Successfully deleted!",  
                workflow: {
                    starterror: "Workflow started successfully",
                    startsuccess: "Workflow failed to start",
                    submiterror: "Workflow submission failed",
                    submitsuccess: "Workflow submitted successfully",
                },
                updateerror: "Form item update failed",       
            },
            gridBar: {
                title: "Table navigation bar",
            },
            multiEditView: {
                notConfig: {
                    fetchAction: "View multi-edit view panel fetchAction parameter is not configured",
                    loaddraftAction: "View multi-edit view panel loaddraftAction parameter is not configured",
                },
            },
            dataViewExpBar: {
                title: "Card view navigation bar",
            },
            kanban: {
                notConfig: {
                    fetchAction: "View list fetchAction parameter is not configured",
                    removeAction: "View table removeAction parameter is not configured",
                },
                delete1: "Confirm to delete ",
                delete2: "the delete operation will be unrecoverable!",
                fold: "fold",
                unfold: "upfold",
            },
            dashBoard: {
                handleClick: {
                    title: "Panel design",
                },
            },
            dataView: {
                sum: "total",
                data: "data",
            },
            chart: {
                undefined: "Undefined",
                quarter: "Quarter",   
                year: "Year",
            },
            searchForm: {
                notConfig: {
                    loadAction: "View search form loadAction parameter is not configured",
                    loaddraftAction: "View search form loaddraftAction parameter is not configured",
                },
                custom: "Store custom queries",
                title: "Name",
            },
            wizardPanel: {
                back: "Back",
                next: "Next",
                complete: "Complete",
                preactionmessage:"The calculation of the previous behavior is not configured"
            },
            viewLayoutPanel: {
                appLogoutView: {
                    prompt1: "Dear customer, you have successfully exited the system, after",
                    prompt2: "seconds, we will jump to the",
                    logingPage: "login page",
                },
                appWfstepTraceView: {
                    title: "Application process processing record view",
                },
                appWfstepDataView: {
                    title: "Application process tracking view",
                },
                appLoginView: {
                    username: "Username",
                    password: "Password",
                    login: "Login",
                },
            },
        },
        form: {
            group: {
                show_more: "Show More",
                hidden_more: "Hide More"
            }
        },
        entities: {
            emobjmap: emobjmap_en_US(),
            emeqwl: emeqwl_en_US(),
            emresitem: emresitem_en_US(),
            emwork: emwork_en_US(),
            emeqmpmtr: emeqmpmtr_en_US(),
            emeqlctmap: emeqlctmap_en_US(),
            emeqmp: emeqmp_en_US(),
            emeqkprcd: emeqkprcd_en_US(),
            emdprct: emdprct_en_US(),
            emeqkpmap: emeqkpmap_en_US(),
            emplan: emplan_en_US(),
            emeqtype: emeqtype_en_US(),
            emitem: emitem_en_US(),
            emequip: emequip_en_US(),
            emdrwgmap: emdrwgmap_en_US(),
            emeqmonitor: emeqmonitor_en_US(),
            emcab: emcab_en_US(),
            emrfodemap: emrfodemap_en_US(),
            emeqlocation: emeqlocation_en_US(),
            emservice: emservice_en_US(),
            emitemtype: emitemtype_en_US(),
            emeqkp: emeqkp_en_US(),
            emstorepart: emstorepart_en_US(),
            empodetail: empodetail_en_US(),
            emobject: emobject_en_US(),
            emapply: emapply_en_US(),
            emeqlctgss: emeqlctgss_en_US(),
            emwo_osc: emwo_osc_en_US(),
            emmachinecategory: emmachinecategory_en_US(),
            emdrwg: emdrwg_en_US(),
            emrfode: emrfode_en_US(),
            emstore: emstore_en_US(),
            emeqdebug: emeqdebug_en_US(),
            emeqlctfdj: emeqlctfdj_en_US(),
            emwplistcost: emwplistcost_en_US(),
            pfteam: pfteam_en_US(),
            pfemppostmap: pfemppostmap_en_US(),
            emacclass: emacclass_en_US(),
            pfcontract: pfcontract_en_US(),
            emeitires: emeitires_en_US(),
            emoutput: emoutput_en_US(),
            emrfodetype: emrfodetype_en_US(),
            dynachart: dynachart_en_US(),
            empurplan: empurplan_en_US(),
            emproduct: emproduct_en_US(),
            embrand: embrand_en_US(),
            pfdept: pfdept_en_US(),
            emeqsparedetail: emeqsparedetail_en_US(),
            pfemp: pfemp_en_US(),
            emstock: emstock_en_US(),
            dynadashboard: dynadashboard_en_US(),
            emwoori: emwoori_en_US(),
            emplandetail: emplandetail_en_US(),
            emplantempl: emplantempl_en_US(),
            emwo_dp: emwo_dp_en_US(),
            emen: emen_en_US(),
            empo: empo_en_US(),
            pfunit: pfunit_en_US(),
            emitemprtn: emitemprtn_en_US(),
            emberth: emberth_en_US(),
            emeqlcttires: emeqlcttires_en_US(),
            emitemrout: emitemrout_en_US(),
            emeqkeep: emeqkeep_en_US(),
            emitemrin: emitemrin_en_US(),
            emeqmaintance: emeqmaintance_en_US(),
            emitempuse: emitempuse_en_US(),
            emeqsetup: emeqsetup_en_US(),
            emitemtrade: emitemtrade_en_US(),
            emwo_en: emwo_en_en_US(),
            emrfoac: emrfoac_en_US(),
            emassetclear: emassetclear_en_US(),
            emmachmodel: emmachmodel_en_US(),
            emassetclass: emassetclass_en_US(),
            emeqspare: emeqspare_en_US(),
            emitempl: emitempl_en_US(),
            emeqah: emeqah_en_US(),
            emwo: emwo_en_US(),
            emeqlctrhy: emeqlctrhy_en_US(),
            emitemcs: emitemcs_en_US(),
            emenconsum: emenconsum_en_US(),
            emserviceevl: emserviceevl_en_US(),
            emplancdt: emplancdt_en_US(),
            emeqsparemap: emeqsparemap_en_US(),
            emoutputrct: emoutputrct_en_US(),
            emrfoca: emrfoca_en_US(),
            emwo_inner: emwo_inner_en_US(),
            emwplist: emwplist_en_US(),
            emrfomo: emrfomo_en_US(),
            emasset: emasset_en_US(),
            emeqcheck: emeqcheck_en_US(),
        },
        components: components_en_US,
        codelist: codelist_en_US(),
        userCustom: userCustom_en_US(),
    };
    // 合并用户自定义多语言
    Util.mergeDeepObject(data, enUSUser); 
    return data;   
}

// 默认导出
export default getAppLocale;