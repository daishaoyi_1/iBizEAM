/**
 * 合同
 *
 * @export
 * @interface PFContract
 */
export interface PFContract {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PFContract
     */
    updatedate?: any;

    /**
     * 需说明事项
     *
     * @returns {*}
     * @memberof PFContract
     */
    attention?: any;

    /**
     * 合同对方分组
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractobjgroup?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof PFContract
     */
    orgid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof PFContract
     */
    enable?: any;

    /**
     * 已付金额(万元)
     *
     * @returns {*}
     * @memberof PFContract
     */
    payamount?: any;

    /**
     * 资质证书
     *
     * @returns {*}
     * @memberof PFContract
     */
    certdesc?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof PFContract
     */
    att?: any;

    /**
     * 合同名称
     *
     * @returns {*}
     * @memberof PFContract
     */
    pfcontractname?: any;

    /**
     * 合同履约记录
     *
     * @returns {*}
     * @memberof PFContract
     */
    performrecord?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractdesc?: any;

    /**
     * 验收标准
     *
     * @returns {*}
     * @memberof PFContract
     */
    checkstandard?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PFContract
     */
    updateman?: any;

    /**
     * 审批状态
     *
     * @returns {*}
     * @memberof PFContract
     */
    apprstate?: any;

    /**
     * 每次付款数
     *
     * @returns {*}
     * @memberof PFContract
     */
    peramount?: any;

    /**
     * 合同标识
     *
     * @returns {*}
     * @memberof PFContract
     */
    pfcontractid?: any;

    /**
     * 合同付款情况
     *
     * @returns {*}
     * @memberof PFContract
     */
    paydesc?: any;

    /**
     * 付款期数
     *
     * @returns {*}
     * @memberof PFContract
     */
    paycnt?: any;

    /**
     * 履行期限
     *
     * @returns {*}
     * @memberof PFContract
     */
    performline?: any;

    /**
     * 合同数量
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractnum?: any;

    /**
     * 附件名称
     *
     * @returns {*}
     * @memberof PFContract
     */
    attdesc?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PFContract
     */
    description?: any;

    /**
     * 合同原件
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractdoc?: any;

    /**
     * 调整说明
     *
     * @returns {*}
     * @memberof PFContract
     */
    changedesc?: any;

    /**
     * 合同序列号
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractno?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PFContract
     */
    createman?: any;

    /**
     * 盖章单位
     *
     * @returns {*}
     * @memberof PFContract
     */
    rorgid?: any;

    /**
     * 履约情况
     *
     * @returns {*}
     * @memberof PFContract
     */
    performstate?: any;

    /**
     * 其它事项说明
     *
     * @returns {*}
     * @memberof PFContract
     */
    declaration?: any;

    /**
     * 合同内容
     *
     * @returns {*}
     * @memberof PFContract
     */
    content?: any;

    /**
     * 付款理由
     *
     * @returns {*}
     * @memberof PFContract
     */
    payreason?: any;

    /**
     * 许可证
     *
     * @returns {*}
     * @memberof PFContract
     */
    licdesc?: any;

    /**
     * 上报时间
     *
     * @returns {*}
     * @memberof PFContract
     */
    sdate?: any;

    /**
     * 审批备注
     *
     * @returns {*}
     * @memberof PFContract
     */
    apprdesc?: any;

    /**
     * 合同单价(￥)
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractprice?: any;

    /**
     * 合同类型
     *
     * @returns {*}
     * @memberof PFContract
     */
    contracttypeid?: any;

    /**
     * 价款(万元)
     *
     * @returns {*}
     * @memberof PFContract
     */
    amount?: any;

    /**
     * 乙方
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractgroup?: any;

    /**
     * 其他资料
     *
     * @returns {*}
     * @memberof PFContract
     */
    docdesc?: any;

    /**
     * 合同质量
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractqa?: any;

    /**
     * 订立日期
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractdate?: any;

    /**
     * 收到资料名称
     *
     * @returns {*}
     * @memberof PFContract
     */
    recvdoc?: any;

    /**
     * 审批时间
     *
     * @returns {*}
     * @memberof PFContract
     */
    apprdate?: any;

    /**
     * 履行地点
     *
     * @returns {*}
     * @memberof PFContract
     */
    performplace?: any;

    /**
     * 合同信息
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractinfo?: any;

    /**
     * 合同对方
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractobjid?: any;

    /**
     * 履行方式
     *
     * @returns {*}
     * @memberof PFContract
     */
    performway?: any;

    /**
     * 变更内容
     *
     * @returns {*}
     * @memberof PFContract
     */
    changecontent?: any;

    /**
     * 合同代码
     *
     * @returns {*}
     * @memberof PFContract
     */
    contractcode?: any;

    /**
     * 合同标准
     *
     * @returns {*}
     * @memberof PFContract
     */
    contracttender?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PFContract
     */
    createdate?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof PFContract
     */
    emservicename?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof PFContract
     */
    emserviceid?: any;
}