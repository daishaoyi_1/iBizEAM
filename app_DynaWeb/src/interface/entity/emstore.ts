/**
 * 仓库
 *
 * @export
 * @interface EMStore
 */
export interface EMStore {

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMStore
     */
    description?: any;

    /**
     * 仓库代码
     *
     * @returns {*}
     * @memberof EMStore
     */
    storecode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMStore
     */
    createdate?: any;

    /**
     * 仓库信息
     *
     * @returns {*}
     * @memberof EMStore
     */
    storeinfo?: any;

    /**
     * 标准价标志
     *
     * @returns {*}
     * @memberof EMStore
     */
    standpriceflag?: any;

    /**
     * 加权平均标志
     *
     * @returns {*}
     * @memberof EMStore
     */
    poweravgflag?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMStore
     */
    createman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMStore
     */
    updatedate?: any;

    /**
     * 仓库标识
     *
     * @returns {*}
     * @memberof EMStore
     */
    emstoreid?: any;

    /**
     * NEW仓库类型
     *
     * @returns {*}
     * @memberof EMStore
     */
    newstoretypeid?: any;

    /**
     * 成本中心
     *
     * @returns {*}
     * @memberof EMStore
     */
    costcenterid?: any;

    /**
     * 主管经理
     *
     * @returns {*}
     * @memberof EMStore
     */
    mgrpersonid?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMStore
     */
    orgid?: any;

    /**
     * 出入算法
     *
     * @returns {*}
     * @memberof EMStore
     */
    ioalgo?: any;

    /**
     * 库管员
     *
     * @returns {*}
     * @memberof EMStore
     */
    empid?: any;

    /**
     * 仓库类型
     *
     * @returns {*}
     * @memberof EMStore
     */
    storetypeid?: any;

    /**
     * 地址
     *
     * @returns {*}
     * @memberof EMStore
     */
    storeaddr?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMStore
     */
    updateman?: any;

    /**
     * 库管员
     *
     * @returns {*}
     * @memberof EMStore
     */
    empname?: any;

    /**
     * 仓库名称
     *
     * @returns {*}
     * @memberof EMStore
     */
    emstorename?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMStore
     */
    enable?: any;

    /**
     * 联系电话
     *
     * @returns {*}
     * @memberof EMStore
     */
    storetel?: any;

    /**
     * 传真
     *
     * @returns {*}
     * @memberof EMStore
     */
    storefax?: any;
}