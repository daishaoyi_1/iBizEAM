/**
 * 订单条目
 *
 * @export
 * @interface EMPODetail
 */
export interface EMPODetail {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    createman?: any;

    /**
     * 发票号
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    civo?: any;

    /**
     * 收货日期
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    rdate?: any;

    /**
     * 验收凭据
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    yiju?: any;

    /**
     * 是否为重启单
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    isrestart?: any;

    /**
     * 发票存根
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    civocopy?: any;

    /**
     * 数量差
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    sumdiff?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    createdate?: any;

    /**
     * 收货人
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    rempname?: any;

    /**
     * 收货价差
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    pricediff?: any;

    /**
     * 税率
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    taxrate?: any;

    /**
     * 均摊关税
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    avgtaxfee?: any;

    /**
     * 标价
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    listprice?: any;

    /**
     * 物品金额
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    amount?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    price?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    updateman?: any;

    /**
     * 单位转换率
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    unitrate?: any;

    /**
     * 订单条目号
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    empodetailid?: any;

    /**
     * 物品备注
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    itemdesc?: any;

    /**
     * 税费
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    shf?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    wfstate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    enable?: any;

    /**
     * 记账人
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    empid?: any;

    /**
     * 总价
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    totalprice?: any;

    /**
     * 条目状态
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    podetailstate?: any;

    /**
     * 收货数量
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    rsum?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    updatedate?: any;

    /**
     * 订单条目信息
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    podetailinfo?: any;

    /**
     * 记账人
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    empname?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    orgid?: any;

    /**
     * sap税率
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    sapsl?: any;

    /**
     * 均摊运杂费
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    avgtsfee?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    wfinstanceid?: any;

    /**
     * 含税总金额
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    sumall?: any;

    /**
     * 折扣(%)
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    discnt?: any;

    /**
     * 价格波动提醒
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    attprice?: any;

    /**
     * 顺序号
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    orderflag?: any;

    /**
     * 收货单价
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    rprice?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    wfstep?: any;

    /**
     * 订货数量
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    psum?: any;

    /**
     * 订单条目名称
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    empodetailname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    description?: any;

    /**
     * 收货人
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    rempid?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    labservicename?: any;

    /**
     * 标准单位
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    sunitid?: any;

    /**
     * 用途
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    useto?: any;

    /**
     * 订单采购员
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    porempname?: any;

    /**
     * 订单流程步骤
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    powfstep?: any;

    /**
     * 订单采购员
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    porempid?: any;

    /**
     * 标准单位
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    sunitname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    itemname?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    labserviceid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    objid?: any;

    /**
     * 设备集合
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    equips?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    equipid?: any;

    /**
     * 收货单位
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    runitname?: any;

    /**
     * 物品均价
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    avgprice?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    itembtypeid?: any;

    /**
     * 采购申请
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    wplistname?: any;

    /**
     * 申请班组
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    teamid?: any;

    /**
     * 订单状态
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    postate?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    objname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    equipname?: any;

    /**
     * 订单
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    poname?: any;

    /**
     * 订货单位
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    unitname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    itemid?: any;

    /**
     * 订货单位
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    unitid?: any;

    /**
     * 订单
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    poid?: any;

    /**
     * 收货单位
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    runitid?: any;

    /**
     * 采购申请
     *
     * @returns {*}
     * @memberof EMPODetail
     */
    wplistid?: any;
}