/**
 * 工单
 *
 * @export
 * @interface EMWO
 */
export interface EMWO {

    /**
     * 工单状态
     *
     * @returns {*}
     * @memberof EMWO
     */
    wostate?: any;

    /**
     * 倍率
     *
     * @returns {*}
     * @memberof EMWO
     */
    vrate?: any;

    /**
     * 制定时间
     *
     * @returns {*}
     * @memberof EMWO
     */
    mdate?: any;

    /**
     * 工单类型
     *
     * @returns {*}
     * @memberof EMWO
     */
    wotype?: any;

    /**
     * 转计划标志
     *
     * @returns {*}
     * @memberof EMWO
     */
    cplanflag?: any;

    /**
     * 等待配件时(小时)
     *
     * @returns {*}
     * @memberof EMWO
     */
    waitbuy?: any;

    /**
     * 工单种类
     *
     * @returns {*}
     * @memberof EMWO
     */
    emwotype?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWO
     */
    description?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO
     */
    rdeptname?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMWO
     */
    wfstep?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMWO
     */
    prefee?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMWO
     */
    activelengths?: any;

    /**
     * 等待修理时(小时)
     *
     * @returns {*}
     * @memberof EMWO
     */
    waitmodi?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWO
     */
    createdate?: any;

    /**
     * 工单编号
     *
     * @returns {*}
     * @memberof EMWO
     */
    emwoid?: any;

    /**
     * 约合材料费(￥)
     *
     * @returns {*}
     * @memberof EMWO
     */
    mfee?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMWO
     */
    regionenddate?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO
     */
    wogroup?: any;

    /**
     * 实际工时(分)
     *
     * @returns {*}
     * @memberof EMWO
     */
    worklength?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO
     */
    rdeptid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWO
     */
    enable?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMWO
     */
    content?: any;

    /**
     * 工单组
     *
     * @returns {*}
     * @memberof EMWO
     */
    woteam?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWO
     */
    createman?: any;

    /**
     * 过期日期
     *
     * @returns {*}
     * @memberof EMWO
     */
    expiredate?: any;

    /**
     * 影响船舶
     *
     * @returns {*}
     * @memberof EMWO
     */
    yxcb?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO
     */
    recvpersonname?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof EMWO
     */
    priority?: any;

    /**
     * 工单信息
     *
     * @returns {*}
     * @memberof EMWO
     */
    woinfo?: any;

    /**
     * 数值
     *
     * @returns {*}
     * @memberof EMWO
     */
    nval?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWO
     */
    updateman?: any;

    /**
     * 工单名称
     *
     * @returns {*}
     * @memberof EMWO
     */
    emwoname?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMWO
     */
    eqstoplength?: any;

    /**
     * 执行日期
     *
     * @returns {*}
     * @memberof EMWO
     */
    wodate?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMWO
     */
    wfinstanceid?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO
     */
    recvpersonid?: any;

    /**
     * 执行结果
     *
     * @returns {*}
     * @memberof EMWO
     */
    wresult?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO
     */
    wpersonid?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO
     */
    rempname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWO
     */
    updatedate?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO
     */
    wpersonname?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWO
     */
    orgid?: any;

    /**
     * 工单内容
     *
     * @returns {*}
     * @memberof EMWO
     */
    wodesc?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMWO
     */
    wfstate?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO
     */
    rempid?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMWO
     */
    val?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMWO
     */
    archive?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMWO
     */
    regionbegindate?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO
     */
    rservicename?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO
     */
    wooriname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO
     */
    equipcode?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfoacname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfomoname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO
     */
    acclassname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO
     */
    equipname?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO
     */
    wopname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfodename?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO
     */
    rteamname?: any;

    /**
     * 统计归口类型分组
     *
     * @returns {*}
     * @memberof EMWO
     */
    stype?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMWO
     */
    dptype?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO
     */
    dpname?: any;

    /**
     * 来源类型
     *
     * @returns {*}
     * @memberof EMWO
     */
    wooritype?: any;

    /**
     * 统计归口类型
     *
     * @returns {*}
     * @memberof EMWO
     */
    sname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO
     */
    objname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfocaname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO
     */
    equipid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO
     */
    rserviceid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO
     */
    acclassid?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO
     */
    wopid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfoacid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfodeid?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO
     */
    wooriid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfomoid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO
     */
    rteamid?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO
     */
    dpid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO
     */
    rfocaid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO
     */
    objid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO
     */
    mpersonid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO
     */
    mpersonname?: any;
}