/**
 * 订单
 *
 * @export
 * @interface EMPO
 */
export interface EMPO {

    /**
     * 合同内容
     *
     * @returns {*}
     * @memberof EMPO
     */
    content?: any;

    /**
     * 物品金额
     *
     * @returns {*}
     * @memberof EMPO
     */
    poamount?: any;

    /**
     * 预计到货日期
     *
     * @returns {*}
     * @memberof EMPO
     */
    eadate?: any;

    /**
     * 订单号
     *
     * @returns {*}
     * @memberof EMPO
     */
    empoid?: any;

    /**
     * 订单状态
     *
     * @returns {*}
     * @memberof EMPO
     */
    postate?: any;

    /**
     * 采购分管副总
     *
     * @returns {*}
     * @memberof EMPO
     */
    fgempname?: any;

    /**
     * 订购日期
     *
     * @returns {*}
     * @memberof EMPO
     */
    pdate?: any;

    /**
     * 货物发票
     *
     * @returns {*}
     * @memberof EMPO
     */
    civo?: any;

    /**
     * 供应商备注
     *
     * @returns {*}
     * @memberof EMPO
     */
    labservicedesc?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMPO
     */
    description?: any;

    /**
     * 订单名称
     *
     * @returns {*}
     * @memberof EMPO
     */
    emponame?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPO
     */
    createdate?: any;

    /**
     * 运杂费
     *
     * @returns {*}
     * @memberof EMPO
     */
    tsfee?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMPO
     */
    orgid?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof EMPO
     */
    rempid?: any;

    /**
     * 关税
     *
     * @returns {*}
     * @memberof EMPO
     */
    taxfee?: any;

    /**
     * 合同校验
     *
     * @returns {*}
     * @memberof EMPO
     */
    htjy?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPO
     */
    updatedate?: any;

    /**
     * 批准日期
     *
     * @returns {*}
     * @memberof EMPO
     */
    apprdate?: any;

    /**
     * 批准人
     *
     * @returns {*}
     * @memberof EMPO
     */
    apprempid?: any;

    /**
     * 关税发票
     *
     * @returns {*}
     * @memberof EMPO
     */
    taxivo?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof EMPO
     */
    att?: any;

    /**
     * 总经理
     *
     * @returns {*}
     * @memberof EMPO
     */
    zjlempid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPO
     */
    createman?: any;

    /**
     * 采购分管副总
     *
     * @returns {*}
     * @memberof EMPO
     */
    fgempid?: any;

    /**
     * 最高单价
     *
     * @returns {*}
     * @memberof EMPO
     */
    maxprice?: any;

    /**
     * 批准人
     *
     * @returns {*}
     * @memberof EMPO
     */
    apprempname?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMPO
     */
    wfinstanceid?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMPO
     */
    wfstep?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMPO
     */
    enable?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof EMPO
     */
    rempname?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMPO
     */
    wfstate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPO
     */
    updateman?: any;

    /**
     * 付款方式
     *
     * @returns {*}
     * @memberof EMPO
     */
    payway?: any;

    /**
     * 订单信息
     *
     * @returns {*}
     * @memberof EMPO
     */
    poinfo?: any;

    /**
     * 运杂费发票
     *
     * @returns {*}
     * @memberof EMPO
     */
    tsivo?: any;

    /**
     * 总经理
     *
     * @returns {*}
     * @memberof EMPO
     */
    zjlempname?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMPO
     */
    labservicetypeid?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMPO
     */
    labservicename?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMPO
     */
    labserviceid?: any;
}