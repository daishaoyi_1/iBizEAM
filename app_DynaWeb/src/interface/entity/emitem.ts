/**
 * 物品
 *
 * @export
 * @interface EMItem
 */
export interface EMItem {

    /**
     * 最新价格
     *
     * @returns {*}
     * @memberof EMItem
     */
    lastprice?: any;

    /**
     * 对象编号
     *
     * @returns {*}
     * @memberof EMItem
     */
    objid?: any;

    /**
     * 物品信息
     *
     * @returns {*}
     * @memberof EMItem
     */
    iteminfo?: any;

    /**
     * 按资产
     *
     * @returns {*}
     * @memberof EMItem
     */
    isassetflag?: any;

    /**
     * 最高库存
     *
     * @returns {*}
     * @memberof EMItem
     */
    highsum?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItem
     */
    createdate?: any;

    /**
     * 库存金额
     *
     * @returns {*}
     * @memberof EMItem
     */
    amount?: any;

    /**
     * 按批次
     *
     * @returns {*}
     * @memberof EMItem
     */
    isbatchflag?: any;

    /**
     * 验收方法
     *
     * @returns {*}
     * @memberof EMItem
     */
    checkmethod?: any;

    /**
     * 物品标识
     *
     * @returns {*}
     * @memberof EMItem
     */
    emitemid?: any;

    /**
     * 库存定额余量
     *
     * @returns {*}
     * @memberof EMItem
     */
    stockdesum?: any;

    /**
     * ABC分类
     *
     * @returns {*}
     * @memberof EMItem
     */
    abctype?: any;

    /**
     * 平均税费
     *
     * @returns {*}
     * @memberof EMItem
     */
    shfprice?: any;

    /**
     * 重订量
     *
     * @returns {*}
     * @memberof EMItem
     */
    repsum?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItem
     */
    updateman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItem
     */
    enable?: any;

    /**
     * 库存超期
     *
     * @returns {*}
     * @memberof EMItem
     */
    stockextime?: any;

    /**
     * 物品新旧标识
     *
     * @returns {*}
     * @memberof EMItem
     */
    isnew?: any;

    /**
     * 物品编码(新)
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemnid?: any;

    /**
     * 产品系列号
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemserialcode?: any;

    /**
     * 最新请购人
     *
     * @returns {*}
     * @memberof EMItem
     */
    lastaempname?: any;

    /**
     * 最新采购员
     *
     * @returns {*}
     * @memberof EMItem
     */
    empid?: any;

    /**
     * 登记日期
     *
     * @returns {*}
     * @memberof EMItem
     */
    registerdat?: any;

    /**
     * 产品型号
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemmodelcode?: any;

    /**
     * 物品代码(新)
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemncode?: any;

    /**
     * 物品代码
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemcode?: any;

    /**
     * 库管员
     *
     * @returns {*}
     * @memberof EMItem
     */
    sempid?: any;

    /**
     * 不足3家供应商
     *
     * @returns {*}
     * @memberof EMItem
     */
    no3q?: any;

    /**
     * 物品名称
     *
     * @returns {*}
     * @memberof EMItem
     */
    emitemname?: any;

    /**
     * 密度
     *
     * @returns {*}
     * @memberof EMItem
     */
    dens?: any;

    /**
     * sap控制
     *
     * @returns {*}
     * @memberof EMItem
     */
    sapcontrol?: any;

    /**
     * 批次类型
     *
     * @returns {*}
     * @memberof EMItem
     */
    batchtype?: any;

    /**
     * 物品分组
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemgroup?: any;

    /**
     * 最新请购人
     *
     * @returns {*}
     * @memberof EMItem
     */
    lastaempid?: any;

    /**
     * 最低库存
     *
     * @returns {*}
     * @memberof EMItem
     */
    lastsum?: any;

    /**
     * 最新采购员
     *
     * @returns {*}
     * @memberof EMItem
     */
    empname?: any;

    /**
     * 库存量
     *
     * @returns {*}
     * @memberof EMItem
     */
    stocksum?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItem
     */
    createman?: any;

    /**
     * 最后入料时间
     *
     * @returns {*}
     * @memberof EMItem
     */
    lastindate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItem
     */
    orgid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItem
     */
    description?: any;

    /**
     * sap控制代码
     *
     * @returns {*}
     * @memberof EMItem
     */
    sapcontrolcode?: any;

    /**
     * 寿命周期(天)
     *
     * @returns {*}
     * @memberof EMItem
     */
    life?: any;

    /**
     * 平均价
     *
     * @returns {*}
     * @memberof EMItem
     */
    price?: any;

    /**
     * 成本中心
     *
     * @returns {*}
     * @memberof EMItem
     */
    costcenterid?: any;

    /**
     * 库存周期(天)
     *
     * @returns {*}
     * @memberof EMItem
     */
    stockinl?: any;

    /**
     * 物品备注
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemdesc?: any;

    /**
     * 库管员
     *
     * @returns {*}
     * @memberof EMItem
     */
    sempname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItem
     */
    updatedate?: any;

    /**
     * 保修天数
     *
     * @returns {*}
     * @memberof EMItem
     */
    warrantyday?: any;

    /**
     * 最新存储仓库
     *
     * @returns {*}
     * @memberof EMItem
     */
    storename?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItem
     */
    unitname?: any;

    /**
     * 货架
     *
     * @returns {*}
     * @memberof EMItem
     */
    emcabname?: any;

    /**
     * 最新存储仓库
     *
     * @returns {*}
     * @memberof EMItem
     */
    storecode?: any;

    /**
     * 最新存储库位
     *
     * @returns {*}
     * @memberof EMItem
     */
    storepartname?: any;

    /**
     * 建议供应商
     *
     * @returns {*}
     * @memberof EMItem
     */
    labservicename?: any;

    /**
     * 物品二级类型
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemmtypeid?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMItem
     */
    mservicename?: any;

    /**
     * 物品一级类型
     *
     * @returns {*}
     * @memberof EMItem
     */
    itembtypename?: any;

    /**
     * 物品一级类型
     *
     * @returns {*}
     * @memberof EMItem
     */
    itembtypeid?: any;

    /**
     * 物品类型代码
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemtypecode?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMItem
     */
    acclassname?: any;

    /**
     * 物品二级类型
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemmtypename?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemtypename?: any;

    /**
     * 货架
     *
     * @returns {*}
     * @memberof EMItem
     */
    emcabid?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItem
     */
    unitid?: any;

    /**
     * 最新存储库位
     *
     * @returns {*}
     * @memberof EMItem
     */
    storepartid?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMItem
     */
    itemtypeid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMItem
     */
    acclassid?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMItem
     */
    mserviceid?: any;

    /**
     * 建议供应商
     *
     * @returns {*}
     * @memberof EMItem
     */
    labserviceid?: any;

    /**
     * 最新存储仓库
     *
     * @returns {*}
     * @memberof EMItem
     */
    storeid?: any;
}