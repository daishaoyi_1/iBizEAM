/**
 * 入库单
 *
 * @export
 * @interface EMItemRIn
 */
export interface EMItemRIn {

    /**
     * 入库单名称
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    emitemrinname?: any;

    /**
     * sap传输异常文本
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sapreason1?: any;

    /**
     * 收料人
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sempname?: any;

    /**
     * 实收数
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    psum?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    updateman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    description?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    createman?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    wfstate?: any;

    /**
     * 入库单信息
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    itemrininfo?: any;

    /**
     * 物品金额
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    amount?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    enable?: any;

    /**
     * 实收数1
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    rin_psum?: any;

    /**
     * 制单人
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    empname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    updatedate?: any;

    /**
     * 入库单号
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    emitemrinid?: any;

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    batcode?: any;

    /**
     * 入库状态
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    rinstate?: any;

    /**
     * sap传输失败原因
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sapreason?: any;

    /**
     * 含税总金额
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sumall?: any;

    /**
     * sap传输状态
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sap?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    wfinstanceid?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    orgid?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    wfstep?: any;

    /**
     * 制单人
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    empid?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    price?: any;

    /**
     * sap税码
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sapsm?: any;

    /**
     * 收料人
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sempid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    createdate?: any;

    /**
     * 入库日期
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sdate?: any;

    /**
     * sap控制
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    sapcontrol?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    storepartname?: any;

    /**
     * 物品代码
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    itemcode?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    teamid?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    unitid?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    labservicename?: any;

    /**
     * 发票号
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    civo?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    unitname?: any;

    /**
     * 订单编号
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    poid?: any;

    /**
     * 运杂费
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    avgtsfee?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    itemname?: any;

    /**
     * 采购申请
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    wplistid?: any;

    /**
     * 订单条目
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    podetailname?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    itemtypename?: any;

    /**
     * 物品均价
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    avgprice?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    emservicename?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    porempname?: any;

    /**
     * 税费
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    shf?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    storename?: any;

    /**
     * 供应商编号
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    labserviceid?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    itembtypeid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    emserviceid?: any;

    /**
     * 订单条目
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    podetailid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    storeid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    itemid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemRIn
     */
    storepartid?: any;
}