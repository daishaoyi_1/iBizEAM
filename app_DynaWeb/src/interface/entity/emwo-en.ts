/**
 * 能耗登记工单
 *
 * @export
 * @interface EMWO_EN
 */
export interface EMWO_EN {

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    enable?: any;

    /**
     * 工单组
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    woteam?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wfstate?: any;

    /**
     * 实际抄表时间
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    regionbegindate?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rempname?: any;

    /**
     * 过期日期
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    expiredate?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wfstep?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    prefee?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    updateman?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    content?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rempid?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    priority?: any;

    /**
     * 制定时间
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    mdate?: any;

    /**
     * 工单名称
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    emwo_enname?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    val?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    regionenddate?: any;

    /**
     * 执行日期
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wodate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    createman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    description?: any;

    /**
     * 指派抄表人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    recvpersonname?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    activelengths?: any;

    /**
     * 倍率
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    vrate?: any;

    /**
     * 本次记录值
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    curval?: any;

    /**
     * 工单状态
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wostate?: any;

    /**
     * 实际工时(分)
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    worklength?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wogroup?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    emwotype?: any;

    /**
     * 抄表人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wpersonname?: any;

    /**
     * 工单编号
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    emwo_enid?: any;

    /**
     * 上次记录值
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    lastval?: any;

    /**
     * 指派抄表人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    recvpersonid?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wfinstanceid?: any;

    /**
     * 工单组
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    woteam_show?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rdeptid?: any;

    /**
     * 上次采集时间
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    bdate?: any;

    /**
     * 抄表人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wpersonid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    updatedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    createdate?: any;

    /**
     * 工单类型
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wotype?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    eqstoplength?: any;

    /**
     * 执行结果
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wresult?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rdeptname?: any;

    /**
     * 能耗值
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    nval?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    archive?: any;

    /**
     * 工单内容
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wodesc?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    orgid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfomoname?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfoacname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rteamname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfocaname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rservicename?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    dptype?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    objname?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wopname_show?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wopname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    acclassname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfodename?: any;

    /**
     * 能源
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    dpname?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wooriname?: any;

    /**
     * 来源类型
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wooritype?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    equipname?: any;

    /**
     * 能源
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    dpid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    objid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rserviceid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    equipid?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wooriid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    acclassid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfomoid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rteamid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfocaid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfodeid?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    wopid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    rfoacid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    mpersonid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_EN
     */
    mpersonname?: any;
}