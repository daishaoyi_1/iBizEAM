/**
 * 服务商评估
 *
 * @export
 * @interface EMServiceEvl
 */
export interface EMServiceEvl {

    /**
     * 服务商评估标识
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    emserviceevlid?: any;

    /**
     * 服务商评估名称
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    emserviceevlname?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    wfinstanceid?: any;

    /**
     * 售后(100分,10%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult9?: any;

    /**
     * 供货能力(100分,10%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult5?: any;

    /**
     * 距离(100分,5%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult7?: any;

    /**
     * 及时性(100分,10%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult6?: any;

    /**
     * 资质(100分,5%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult1?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    createdate?: any;

    /**
     * 评价
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlmark1?: any;

    /**
     * 安全性能(100分,10%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult4?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    orgid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    updateman?: any;

    /**
     * 质量管理体系(100分,5%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult8?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    createman?: any;

    /**
     * 评估人
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    empname?: any;

    /**
     * 评估时间
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evldate?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    wfstate?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    wfstep?: any;

    /**
     * 评价
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlmark?: any;

    /**
     * 总分数
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    updatedate?: any;

    /**
     * 评估区间
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlregion?: any;

    /**
     * 价格(100分,20%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult3?: any;

    /**
     * 质量(100分,25%)
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    evlresult2?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    enable?: any;

    /**
     * 评估人
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    empid?: any;

    /**
     * 评估状态
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    serviceevlstate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    description?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    servicename?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMServiceEvl
     */
    serviceid?: any;
}