/**
 * 物品交易
 *
 * @export
 * @interface EMItemTrade
 */
export interface EMItemTrade {

    /**
     * 库管员
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    sempname?: any;

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    batcode?: any;

    /**
     * 物品交易名称
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    emitemtradename?: any;

    /**
     * 发票号
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    civo?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    price?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    createman?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    aempname?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    orgid?: any;

    /**
     * 出入日期
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    sdate?: any;

    /**
     * 交易状态
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    tradestate?: any;

    /**
     * 领料分类
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    pusetype?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    updatedate?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    aempid?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    psum?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    amount?: any;

    /**
     * 出入标志
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    inoutflag?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    deptname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    updateman?: any;

    /**
     * 税费
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    shf?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    deptid?: any;

    /**
     * 交易分组
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    emitemtradetype?: any;

    /**
     * 库管员
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    sempid?: any;

    /**
     * 物品交易标识
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    emitemtradeid?: any;

    /**
     * 物品类型分组
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itemtypegroup?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    enable?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    description?: any;

    /**
     * 物品2级类
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itemmtypeid?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itembtypename?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    storename?: any;

    /**
     * 平均税费
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    shfprice?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itembtypeid?: any;

    /**
     * 物品库存金额
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    stockamount?: any;

    /**
     * 关联单
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    rname?: any;

    /**
     * 物品代码
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itemcode?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    storepartname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itemname?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    teamname?: any;

    /**
     * 物品二级类
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itemmtypename?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    labservicename?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itemtypeid?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    labserviceid?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    teamid?: any;

    /**
     * 关联单
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    rid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    itemid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    storeid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemTrade
     */
    storepartid?: any;
}