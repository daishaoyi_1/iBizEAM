/**
 * 加班工单
 *
 * @export
 * @interface EMWork
 */
export interface EMWork {

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMWork
     */
    wfstate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWork
     */
    updateman?: any;

    /**
     * 加班工作内容
     *
     * @returns {*}
     * @memberof EMWork
     */
    jbgznr?: any;

    /**
     * 加班日期
     *
     * @returns {*}
     * @memberof EMWork
     */
    workdate?: any;

    /**
     * 加班时间(分)
     *
     * @returns {*}
     * @memberof EMWork
     */
    overtime?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof EMWork
     */
    empname?: any;

    /**
     * 加班区分
     *
     * @returns {*}
     * @memberof EMWork
     */
    workstate?: any;

    /**
     * 上午结束时间
     *
     * @returns {*}
     * @memberof EMWork
     */
    amendtime?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWork
     */
    updatedate?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof EMWork
     */
    empid?: any;

    /**
     * 开始时间
     *
     * @returns {*}
     * @memberof EMWork
     */
    ambegintime?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWork
     */
    createdate?: any;

    /**
     * 加班工单状态
     *
     * @returns {*}
     * @memberof EMWork
     */
    emworkstate?: any;

    /**
     * 加班工单名称
     *
     * @returns {*}
     * @memberof EMWork
     */
    emworkname?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMWork
     */
    pmendtime?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMWork
     */
    wfstep?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWork
     */
    enable?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWork
     */
    createman?: any;

    /**
     * 加班工单标识
     *
     * @returns {*}
     * @memberof EMWork
     */
    emworkid?: any;

    /**
     * 岗位
     *
     * @returns {*}
     * @memberof EMWork
     */
    post?: any;

    /**
     * 工作地点
     *
     * @returns {*}
     * @memberof EMWork
     */
    workplace?: any;

    /**
     * 下午开始时间
     *
     * @returns {*}
     * @memberof EMWork
     */
    pmbegintime?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMWork
     */
    wfinstanceid?: any;
}