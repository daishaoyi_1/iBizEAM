/**
 * 轮胎位置
 *
 * @export
 * @interface EMEQLCTTIRes
 */
export interface EMEQLCTTIRes {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    createman?: any;

    /**
     * 轮胎状态
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    tiresstate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    description?: any;

    /**
     * 使用气压
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    par?: any;

    /**
     * 价格
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    amount?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    enable?: any;

    /**
     * 图形8*8=11-88
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    picparams?: any;

    /**
     * 预警期限(天)
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    valve?: any;

    /**
     * 更换时间
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    replacedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    createdate?: any;

    /**
     * 型号
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    eqmodelcode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    orgid?: any;

    /**
     * 新旧标志
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    newoldflag?: any;

    /**
     * 厂牌
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    changp?: any;

    /**
     * 材质层数
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    systemparam?: any;

    /**
     * 更换原因
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    replacereason?: any;

    /**
     * 轮胎备注
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    lctdesc?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    updatedate?: any;

    /**
     * 轮胎车型
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    tirestype?: any;

    /**
     * 轮胎信息
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    lcttiresinfo?: any;

    /**
     * 有内胎
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    haveinner?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    updateman?: any;

    /**
     * 出厂编号
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    mccode?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    labservicename?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    mservicename?: any;

    /**
     * 位置信息
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    eqlocationinfo?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    equipname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    equipid?: any;

    /**
     * 位置标识
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    emeqlocationid?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    labserviceid?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMEQLCTTIRes
     */
    mserviceid?: any;
}