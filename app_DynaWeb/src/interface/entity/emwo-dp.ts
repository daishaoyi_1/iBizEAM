/**
 * 点检工单
 *
 * @export
 * @interface EMWO_DP
 */
export interface EMWO_DP {

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    eqstoplength?: any;

    /**
     * 工单状态
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wostate?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rdeptname?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wfstep?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    val?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wpersonid?: any;

    /**
     * 工单编号
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    emwo_dpid?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    priority?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    archive?: any;

    /**
     * 数值
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    nval?: any;

    /**
     * 执行日期
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wodate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    description?: any;

    /**
     * 实际执行时间
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    regionbegindate?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    activelengths?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wogroup?: any;

    /**
     * 工单组
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    woteam?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    recvpersonname?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    recvpersonid?: any;

    /**
     * 执行结果
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wresult?: any;

    /**
     * 过期日期
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    expiredate?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wfstate?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    content?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wpersonname?: any;

    /**
     * 实际工时(分)
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    worklength?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    enable?: any;

    /**
     * 工单内容
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wodesc?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    updateman?: any;

    /**
     * 倍率
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    vrate?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wfinstanceid?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    emwotype?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rempid?: any;

    /**
     * 制定时间
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    mdate?: any;

    /**
     * 工单名称
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    emwo_dpname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    createman?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    prefee?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rdeptid?: any;

    /**
     * 工单类型
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wotype?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    orgid?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rempname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    updatedate?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    regionenddate?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rteamname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfomoname?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    dpname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfodename?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wopname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    objname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfocaname?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    dptype?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfoacname?: any;

    /**
     * 来源类型
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wooritype?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    acclassname?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wooriname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rservicename?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    equipname?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    dpid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    objid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    acclassid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfoacid?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wopid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfocaid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfodeid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rserviceid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rfomoid?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    wooriid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    equipid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    rteamid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    mpersonid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_DP
     */
    mpersonname?: any;
}