/**
 * 资产清盘记录
 *
 * @export
 * @interface EMAssetClear
 */
export interface EMAssetClear {

    /**
     * 盘盈金额
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetinprice?: any;

    /**
     * 盘点清查金额
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetcheckprice?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    orgid?: any;

    /**
     * 最新清盘
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    isnew?: any;

    /**
     * 盘亏金额
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetoutprice?: any;

    /**
     * 使用部门
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetclearlct?: any;

    /**
     * 资产清盘记录名称
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    emassetclearname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    updatedate?: any;

    /**
     * 盘亏数量
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetoutnum?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    updateman?: any;

    /**
     * 状况
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetclearstate?: any;

    /**
     * 盘点日期
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetcleardate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    enable?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    createdate?: any;

    /**
     * 盘盈数量
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetinnum?: any;

    /**
     * 盘点清查数量
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetchecknum?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    createman?: any;

    /**
     * 资产清盘记录标识
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    emassetclearid?: any;

    /**
     * 资产排序
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetsort?: any;

    /**
     * 资产代码
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetcode?: any;

    /**
     * 资产类型
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetclassname?: any;

    /**
     * 规格型号
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    eqmodelcode?: any;

    /**
     * 入帐时间
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    purchdate?: any;

    /**
     * 资产原值
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    originalcost?: any;

    /**
     * 资产类型
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    assetclassid?: any;

    /**
     * 资产
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    emassetname?: any;

    /**
     * 资产
     *
     * @returns {*}
     * @memberof EMAssetClear
     */
    emassetid?: any;
}