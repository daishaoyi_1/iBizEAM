/**
 * 位置关系
 *
 * @export
 * @interface EMEQLCTMap
 */
export interface EMEQLCTMap {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    updatedate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    orgid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    description?: any;

    /**
     * 位置关系标识
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    emeqlctmapid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    updateman?: any;

    /**
     * 位置关系
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    emeqlctmapname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    createdate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    enable?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    majorequipname?: any;

    /**
     * 位置代码
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    eqlocationcode?: any;

    /**
     * 上级位置
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    eqlocationpname?: any;

    /**
     * 位置描述
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    eqlocationdesc?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    majorequipid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    eqlocationname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    eqlocationid?: any;

    /**
     * 上级位置
     *
     * @returns {*}
     * @memberof EMEQLCTMap
     */
    eqlocationpid?: any;
}