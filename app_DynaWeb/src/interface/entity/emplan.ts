/**
 * 计划
 *
 * @export
 * @interface EMPlan
 */
export interface EMPlan {

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMPlan
     */
    description?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof EMPlan
     */
    emplanid?: any;

    /**
     * 制定时间
     *
     * @returns {*}
     * @memberof EMPlan
     */
    mdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    updateman?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMPlan
     */
    prefee?: any;

    /**
     * 计划内容
     *
     * @returns {*}
     * @memberof EMPlan
     */
    plandesc?: any;

    /**
     * 多任务?
     *
     * @returns {*}
     * @memberof EMPlan
     */
    mtflag?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rempname?: any;

    /**
     * 计划周期(天)
     *
     * @returns {*}
     * @memberof EMPlan
     */
    plancvl?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    mpersonname?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof EMPlan
     */
    emplanname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPlan
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMPlan
     */
    orgid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    mpersonid?: any;

    /**
     * 生成工单种类
     *
     * @returns {*}
     * @memberof EMPlan
     */
    emwotype?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rempid?: any;

    /**
     * 计划类型
     *
     * @returns {*}
     * @memberof EMPlan
     */
    plantype?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMPlan
     */
    content?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rdeptname?: any;

    /**
     * 计划信息
     *
     * @returns {*}
     * @memberof EMPlan
     */
    planinfo?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rdeptid?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMPlan
     */
    eqstoplength?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    recvpersonid?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMPlan
     */
    archive?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMPlan
     */
    activelengths?: any;

    /**
     * 计划状态
     *
     * @returns {*}
     * @memberof EMPlan
     */
    planstate?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMPlan
     */
    recvpersonname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPlan
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMPlan
     */
    enable?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMPlan
     */
    dpname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMPlan
     */
    acclassname?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMPlan
     */
    dptype?: any;

    /**
     * 计划模板
     *
     * @returns {*}
     * @memberof EMPlan
     */
    plantemplname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPlan
     */
    equipname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPlan
     */
    objname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rteamname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rservicename?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rserviceid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPlan
     */
    objid?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMPlan
     */
    dpid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPlan
     */
    equipid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMPlan
     */
    acclassid?: any;

    /**
     * 计划模板
     *
     * @returns {*}
     * @memberof EMPlan
     */
    plantemplid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMPlan
     */
    rteamid?: any;
}