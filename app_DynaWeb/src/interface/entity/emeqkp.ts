/**
 * 设备关键点
 *
 * @export
 * @interface EMEQKP
 */
export interface EMEQKP {

    /**
     * 设备关键点标识
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    emeqkpid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    orgid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    enable?: any;

    /**
     * 关键点范围
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    kpscope?: any;

    /**
     * 关键点备注
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    kpdesc?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    createman?: any;

    /**
     * 正常参考值
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    normalrefval?: any;

    /**
     * 关键点信息
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    kpinfo?: any;

    /**
     * 关键点类型
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    kptypeid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    updateman?: any;

    /**
     * 关键点代码
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    kpcode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    updatedate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    description?: any;

    /**
     * 关键点名称
     *
     * @returns {*}
     * @memberof EMEQKP
     */
    emeqkpname?: any;
}