/**
 * 品牌
 *
 * @export
 * @interface EMBrand
 */
export interface EMBrand {

    /**
     * 流水号
     *
     * @returns {*}
     * @memberof EMBrand
     */
    embrandid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMBrand
     */
    createman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMBrand
     */
    enable?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMBrand
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMBrand
     */
    updatedate?: any;

    /**
     * 品牌名称
     *
     * @returns {*}
     * @memberof EMBrand
     */
    embrandname?: any;

    /**
     * 品牌编码
     *
     * @returns {*}
     * @memberof EMBrand
     */
    embrandcode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMBrand
     */
    createdate?: any;

    /**
     * 机种编码
     *
     * @returns {*}
     * @memberof EMBrand
     */
    machtypecode?: any;
}