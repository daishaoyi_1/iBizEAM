/**
 * 职员
 *
 * @export
 * @interface PFEmp
 */
export interface PFEmp {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PFEmp
     */
    updateman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PFEmp
     */
    description?: any;

    /**
     * 联系电话
     *
     * @returns {*}
     * @memberof PFEmp
     */
    tel?: any;

    /**
     * 主部门
     *
     * @returns {*}
     * @memberof PFEmp
     */
    majordeptid?: any;

    /**
     * 工作日期
     *
     * @returns {*}
     * @memberof PFEmp
     */
    workdate?: any;

    /**
     * 职员名称
     *
     * @returns {*}
     * @memberof PFEmp
     */
    pfempname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PFEmp
     */
    createdate?: any;

    /**
     * 入本企业日期
     *
     * @returns {*}
     * @memberof PFEmp
     */
    raisedate?: any;

    /**
     * 家庭电话
     *
     * @returns {*}
     * @memberof PFEmp
     */
    hometel?: any;

    /**
     * 出生日期
     *
     * @returns {*}
     * @memberof PFEmp
     */
    birthday?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PFEmp
     */
    updatedate?: any;

    /**
     * 职员标识
     *
     * @returns {*}
     * @memberof PFEmp
     */
    pfempid?: any;

    /**
     * 家庭地址
     *
     * @returns {*}
     * @memberof PFEmp
     */
    homeaddr?: any;

    /**
     * 职员信息
     *
     * @returns {*}
     * @memberof PFEmp
     */
    empinfo?: any;

    /**
     * 主部门
     *
     * @returns {*}
     * @memberof PFEmp
     */
    majordeptname?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof PFEmp
     */
    deptid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PFEmp
     */
    createman?: any;

    /**
     * 性别
     *
     * @returns {*}
     * @memberof PFEmp
     */
    empsex?: any;

    /**
     * 证件号码
     *
     * @returns {*}
     * @memberof PFEmp
     */
    certcode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof PFEmp
     */
    orgid?: any;

    /**
     * 职员代码
     *
     * @returns {*}
     * @memberof PFEmp
     */
    empcode?: any;

    /**
     * 口令
     *
     * @returns {*}
     * @memberof PFEmp
     */
    psw?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof PFEmp
     */
    teamid?: any;

    /**
     * 主部门代码
     *
     * @returns {*}
     * @memberof PFEmp
     */
    maindeptcode?: any;

    /**
     * 电子邮件
     *
     * @returns {*}
     * @memberof PFEmp
     */
    e_mail?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof PFEmp
     */
    enable?: any;

    /**
     * 移动电话
     *
     * @returns {*}
     * @memberof PFEmp
     */
    cell?: any;

    /**
     * 联系地址
     *
     * @returns {*}
     * @memberof PFEmp
     */
    addr?: any;

    /**
     * 主班组
     *
     * @returns {*}
     * @memberof PFEmp
     */
    majorteamname?: any;

    /**
     * 主班组
     *
     * @returns {*}
     * @memberof PFEmp
     */
    majorteamid?: any;
}