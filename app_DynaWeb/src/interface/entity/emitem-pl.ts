/**
 * 损溢单
 *
 * @export
 * @interface EMItemPL
 */
export interface EMItemPL {

    /**
     * 损溢日期
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    sdate?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    price?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    wfstep?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    orgid?: any;

    /**
     * 损溢人
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    sempid?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    amount?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    updateman?: any;

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    batcode?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    wfstate?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    psum?: any;

    /**
     * 损溢状态
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    tradestate?: any;

    /**
     * sap传输状态
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    sap?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    wfinstanceid?: any;

    /**
     * 损溢单号
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    emitemplid?: any;

    /**
     * 损溢单信息
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    itemroutinfo?: any;

    /**
     * 损溢单名称
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    emitemplname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    createdate?: any;

    /**
     * sap传输失败原因
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    sapreason1?: any;

    /**
     * 损益出入标志
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    inoutflag?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    description?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    updatedate?: any;

    /**
     * 损溢人
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    sempname?: any;

    /**
     * sap控制
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    sapcontrol?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    createman?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    storepartname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    itemname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    storename?: any;

    /**
     * 入库单
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    rname?: any;

    /**
     * 入库单
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    rid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    storeid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    itemid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemPL
     */
    storepartid?: any;
}