/**
 * 外委保养工单
 *
 * @export
 * @interface EMWO_OSC
 */
export interface EMWO_OSC {

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    activelengths?: any;

    /**
     * 工单名称
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    emwo_oscname?: any;

    /**
     * 钢丝绳润滑油
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    arg4?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    description?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    emwotype?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    val?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    updatedate?: any;

    /**
     * 执行结果
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wresult?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    regionbegindate?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rdeptname?: any;

    /**
     * 柴油
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    arg3?: any;

    /**
     * 实际工时(分)
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    worklength?: any;

    /**
     * 工单编号
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    emwo_oscid?: any;

    /**
     * 制定时间
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    mdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    updateman?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    regionenddate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    orgid?: any;

    /**
     * 数值
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    nval?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wfstep?: any;

    /**
     * 约合材料费(￥)
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    mfee?: any;

    /**
     * 过期日期
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    expiredate?: any;

    /**
     * 黄油
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    arg1?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wfinstanceid?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    content?: any;

    /**
     * 棉纱
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    arg2?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    archive?: any;

    /**
     * 工单分组
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wogroup?: any;

    /**
     * 工单组
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    woteam?: any;

    /**
     * 工单类型
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wotype?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    priority?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    enable?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    prefee?: any;

    /**
     * 工单状态
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wostate?: any;

    /**
     * 执行日期
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wodate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    createdate?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rdeptid?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wfstate?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    eqstoplength?: any;

    /**
     * 转计划标志
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    cplanflag?: any;

    /**
     * 倍率
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    vrate?: any;

    /**
     * 金属去污粉
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    arg5?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    createman?: any;

    /**
     * 工单内容
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wodesc?: any;

    /**
     * 来源类型
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wooritype?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfoacname?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wopname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfodename?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfocaname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    acclassname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rteamname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rservicename?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    dptype?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    equipcode?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    objname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfomoname?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wooriname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    equipname?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    dpname?: any;

    /**
     * 上级工单
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wopid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rteamid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfocaid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfomoid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    objid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    equipid?: any;

    /**
     * 工单来源
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wooriid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    acclassid?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    dpid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rserviceid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfoacid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rfodeid?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rempid?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    rempname?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    mpersonid?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    mpersonname?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wpersonid?: any;

    /**
     * 执行人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    wpersonname?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    recvpersonid?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMWO_OSC
     */
    recvpersonname?: any;
}