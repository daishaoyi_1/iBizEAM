import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import EMEQTypeService from '@/service/emeqtype/emeqtype-service';
import EMEQTypeAuthService from '@/authservice/emeqtype/emeqtype-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import EMEQTypeUIService from '@/uiservice/emeqtype/emeqtype-ui-service';

/**
 * 设备类型视图基类
 *
 * @export
 * @class EMEQTypeTreeExpView2Base
 * @extends {TreeExpViewBase}
 */
export class EMEQTypeTreeExpView2Base extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeTreeExpView2Base
     */
    protected appDeName: string = 'emeqtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeTreeExpView2Base
     */
    protected appDeKey: string = 'emeqtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeTreeExpView2Base
     */
    protected appDeMajor: string = 'emeqtypename';

    /**
     * 实体服务对象
     *
     * @type {EMEQTypeService}
     * @memberof EMEQTypeTreeExpView2Base
     */
    protected appEntityService: EMEQTypeService = new EMEQTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQTypeUIService
     * @memberof EMEQTypeTreeExpView2Base
     */
    public appUIService: EMEQTypeUIService = new EMEQTypeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQTypeTreeExpView2Base
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQTypeTreeExpView2Base
     */
    protected model: any = {
        srfCaption: 'entities.emeqtype.views.treeexpview2.caption',
        srfTitle: 'entities.emeqtype.views.treeexpview2.title',
        srfSubTitle: 'entities.emeqtype.views.treeexpview2.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQTypeTreeExpView2Base
     */
    protected containerModel: any = {
        view_treeexpbar: {
            name: 'treeexpbar',
            type: 'TREEEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeTreeExpView2Base
     */
	protected viewtag: string = 'd7f2a4b86b6ae7aa14fcc934df27a4d7';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeTreeExpView2Base
     */ 
    protected viewName: string = 'EMEQTypeTreeExpView2';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQTypeTreeExpView2Base
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQTypeTreeExpView2Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQTypeTreeExpView2Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'emeqtype',
            majorPSDEField: 'emeqtypename',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypeTreeExpView2Base
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypeTreeExpView2Base
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypeTreeExpView2Base
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQTypeTreeExpView2
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQTypeTreeExpView2
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMEQTypeTreeExpView2
     */
    public viewUID: string = 'eam-eq-emeqtype-tree-exp-view2';


}