import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMEQSpareDetailService from '@/service/emeqspare-detail/emeqspare-detail-service';
import EMEQSpareDetailAuthService from '@/authservice/emeqspare-detail/emeqspare-detail-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMEQSpareDetailUIService from '@/uiservice/emeqspare-detail/emeqspare-detail-ui-service';

/**
 * 备件包明细视图基类
 *
 * @export
 * @class EMEQSPAREDETAILEditView_EditModeBase
 * @extends {EditViewBase}
 */
export class EMEQSPAREDETAILEditView_EditModeBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    protected appDeName: string = 'emeqsparedetail';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    protected appDeKey: string = 'emeqsparedetailid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    protected appDeMajor: string = 'emeqsparedetailname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareDetailService}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    protected appEntityService: EMEQSpareDetailService = new EMEQSpareDetailService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareDetailUIService
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public appUIService: EMEQSpareDetailUIService = new EMEQSpareDetailUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqsparedetail.views.editview_editmode.caption',
        srfTitle: 'entities.emeqsparedetail.views.editview_editmode.title',
        srfSubTitle: 'entities.emeqsparedetail.views.editview_editmode.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMEQSPAREDETAILEditView_EditMode
     */
    public toolBarModels: any = {
        tbitem1: { name: 'tbitem1', caption: 'entities.emeqsparedetail.editview_editmodetoolbar_toolbar.tbitem1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emeqsparedetail.editview_editmodetoolbar_toolbar.tbitem1.tip', iconcls: 'fa fa-save', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'SaveAndExit', target: '', class: '' } },

        tbitem2: { name: 'tbitem2', caption: 'entities.emeqsparedetail.editview_editmodetoolbar_toolbar.tbitem2.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emeqsparedetail.editview_editmodetoolbar_toolbar.tbitem2.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
	protected viewtag: string = 'a3005005ff60e18b70b799f1dc550b73';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */ 
    protected viewName: string = 'EMEQSPAREDETAILEditView_EditMode';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqsparedetail',
            majorPSDEField: 'emeqsparedetailname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1')) {
            this.toolbar_tbitem1_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem2')) {
            this.toolbar_tbitem2_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.SaveAndExit(datas, contextJO,paramJO,  $event, xData,this,"EMEQSpareDetail");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem2_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMEQSpareDetail");
    }

    /**
     * 保存并关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public SaveAndExit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (xData && xData.saveAndExit instanceof Function) {
            xData.saveAndExit().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                if(window.parent){
                    window.parent.postMessage([{ ...response.data }],'*');
                }
            });
        } else if (_this.saveAndExit && _this.saveAndExit instanceof Function) {
            _this.saveAndExit().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                if(window.parent){
                    window.parent.postMessage([{ ...response.data }],'*');
                }
            });
        }
    }
    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMEQSPAREDETAILEditView_EditModeBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}