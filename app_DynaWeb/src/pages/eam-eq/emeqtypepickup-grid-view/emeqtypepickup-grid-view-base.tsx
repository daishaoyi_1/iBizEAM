import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMEQTypeService from '@/service/emeqtype/emeqtype-service';
import EMEQTypeAuthService from '@/authservice/emeqtype/emeqtype-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMEQTypeUIService from '@/uiservice/emeqtype/emeqtype-ui-service';

/**
 * 设备类型选择表格视图视图基类
 *
 * @export
 * @class EMEQTYPEPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMEQTYPEPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    protected appDeName: string = 'emeqtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    protected appDeKey: string = 'emeqtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    protected appDeMajor: string = 'emeqtypename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMEQTypeService}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    protected appEntityService: EMEQTypeService = new EMEQTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQTypeUIService
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public appUIService: EMEQTypeUIService = new EMEQTypeUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqtype.views.pickupgridview.caption',
        srfTitle: 'entities.emeqtype.views.pickupgridview.title',
        srfSubTitle: 'entities.emeqtype.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupGridViewBase
     */
	protected viewtag: string = 'd538a88bd80b36139966529b8dbf583a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupGridViewBase
     */ 
    protected viewName: string = 'EMEQTYPEPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQTYPEPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emeqtype',
            majorPSDEField: 'emeqtypename',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMEQTYPEPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}