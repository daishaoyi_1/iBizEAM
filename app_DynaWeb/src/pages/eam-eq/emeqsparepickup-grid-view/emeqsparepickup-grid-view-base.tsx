import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMEQSpareService from '@/service/emeqspare/emeqspare-service';
import EMEQSpareAuthService from '@/authservice/emeqspare/emeqspare-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMEQSpareUIService from '@/uiservice/emeqspare/emeqspare-ui-service';

/**
 * 备件包选择表格视图视图基类
 *
 * @export
 * @class EMEQSPAREPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMEQSPAREPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    protected appDeName: string = 'emeqspare';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    protected appDeKey: string = 'emeqspareid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    protected appDeMajor: string = 'emeqsparename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareService}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    protected appEntityService: EMEQSpareService = new EMEQSpareService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareUIService
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public appUIService: EMEQSpareUIService = new EMEQSpareUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqspare.views.pickupgridview.caption',
        srfTitle: 'entities.emeqspare.views.pickupgridview.title',
        srfSubTitle: 'entities.emeqspare.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupGridViewBase
     */
	protected viewtag: string = '33ca6b67909689f3d04473d97bbd4c4f';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupGridViewBase
     */ 
    protected viewName: string = 'EMEQSPAREPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSPAREPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emeqspare',
            majorPSDEField: 'emeqsparename',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMEQSPAREPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}