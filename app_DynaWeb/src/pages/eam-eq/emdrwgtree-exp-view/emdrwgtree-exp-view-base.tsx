import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import EMDRWGService from '@/service/emdrwg/emdrwg-service';
import EMDRWGAuthService from '@/authservice/emdrwg/emdrwg-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import EMDRWGUIService from '@/uiservice/emdrwg/emdrwg-ui-service';

/**
 * 文档视图基类
 *
 * @export
 * @class EMDRWGTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class EMDRWGTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGTreeExpViewBase
     */
    protected appDeName: string = 'emdrwg';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGTreeExpViewBase
     */
    protected appDeKey: string = 'emdrwgid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGTreeExpViewBase
     */
    protected appDeMajor: string = 'emdrwgname';

    /**
     * 实体服务对象
     *
     * @type {EMDRWGService}
     * @memberof EMDRWGTreeExpViewBase
     */
    protected appEntityService: EMDRWGService = new EMDRWGService;

    /**
     * 实体权限服务对象
     *
     * @type EMDRWGUIService
     * @memberof EMDRWGTreeExpViewBase
     */
    public appUIService: EMDRWGUIService = new EMDRWGUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMDRWGTreeExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMDRWGTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emdrwg.views.treeexpview.caption',
        srfTitle: 'entities.emdrwg.views.treeexpview.title',
        srfSubTitle: 'entities.emdrwg.views.treeexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMDRWGTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: {
            name: 'treeexpbar',
            type: 'TREEEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGTreeExpViewBase
     */
	protected viewtag: string = '2ad8d02d422d99d808a84e9f957dc18d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGTreeExpViewBase
     */ 
    protected viewName: string = 'EMDRWGTreeExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMDRWGTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMDRWGTreeExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMDRWGTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'emdrwg',
            majorPSDEField: 'emdrwgname',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMDRWGTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMDRWGTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMDRWGTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMDRWGTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMDRWGTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMDRWGTreeExpView
     */
    public viewUID: string = 'eam-eq-emdrwgtree-exp-view';


}