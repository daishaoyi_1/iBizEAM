import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMEQSpareService from '@/service/emeqspare/emeqspare-service';
import EMEQSpareAuthService from '@/authservice/emeqspare/emeqspare-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMEQSpareUIService from '@/uiservice/emeqspare/emeqspare-ui-service';

/**
 * 备件包数据选择视图视图基类
 *
 * @export
 * @class EMEQSPAREPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMEQSPAREPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupViewBase
     */
    protected appDeName: string = 'emeqspare';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupViewBase
     */
    protected appDeKey: string = 'emeqspareid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupViewBase
     */
    protected appDeMajor: string = 'emeqsparename';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareService}
     * @memberof EMEQSPAREPickupViewBase
     */
    protected appEntityService: EMEQSpareService = new EMEQSpareService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareUIService
     * @memberof EMEQSPAREPickupViewBase
     */
    public appUIService: EMEQSpareUIService = new EMEQSpareUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqspare.views.pickupview.caption',
        srfTitle: 'entities.emeqspare.views.pickupview.title',
        srfSubTitle: 'entities.emeqspare.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSPAREPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupViewBase
     */
	protected viewtag: string = 'c34866637c2982d192c601dec0848b8b';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSPAREPickupViewBase
     */ 
    protected viewName: string = 'EMEQSPAREPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSPAREPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSPAREPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSPAREPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emeqspare',
            majorPSDEField: 'emeqsparename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSPAREPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}