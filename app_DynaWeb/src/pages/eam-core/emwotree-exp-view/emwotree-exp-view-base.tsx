import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import EMWOService from '@/service/emwo/emwo-service';
import EMWOAuthService from '@/authservice/emwo/emwo-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import EMWOUIService from '@/uiservice/emwo/emwo-ui-service';

/**
 * 工单树导航视图视图基类
 *
 * @export
 * @class EMWOTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class EMWOTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOTreeExpViewBase
     */
    protected appDeName: string = 'emwo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWOTreeExpViewBase
     */
    protected appDeKey: string = 'emwoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWOTreeExpViewBase
     */
    protected appDeMajor: string = 'emwoname';

    /**
     * 实体服务对象
     *
     * @type {EMWOService}
     * @memberof EMWOTreeExpViewBase
     */
    protected appEntityService: EMWOService = new EMWOService;

    /**
     * 实体权限服务对象
     *
     * @type EMWOUIService
     * @memberof EMWOTreeExpViewBase
     */
    public appUIService: EMWOUIService = new EMWOUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWOTreeExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWOTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo.views.treeexpview.caption',
        srfTitle: 'entities.emwo.views.treeexpview.title',
        srfSubTitle: 'entities.emwo.views.treeexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWOTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: {
            name: 'treeexpbar',
            type: 'TREEEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWOTreeExpViewBase
     */
	protected viewtag: string = '9049845cbefcb1d6090c3dd11c79f3f3';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOTreeExpViewBase
     */ 
    protected viewName: string = 'EMWOTreeExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWOTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWOTreeExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWOTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'emwo',
            majorPSDEField: 'emwoname',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWOTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        this.$Notice.warning({ title: '错误', desc: '请添加新建数据向导视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWOTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMWOTreeExpView
     */
    public viewUID: string = 'eam-core-emwotree-exp-view';


}