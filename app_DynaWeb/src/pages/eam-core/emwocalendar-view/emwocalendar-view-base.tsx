import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { CalendarViewBase } from '@/studio-core';
import EMWOService from '@/service/emwo/emwo-service';
import EMWOAuthService from '@/authservice/emwo/emwo-auth-service';
import EMWOUIService from '@/uiservice/emwo/emwo-ui-service';
import CodeListService from "@service/app/codelist-service";


/**
 * 工单日历视图视图基类
 *
 * @export
 * @class EMWOCalendarViewBase
 * @extends {CalendarViewBase}
 */
export class EMWOCalendarViewBase extends CalendarViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarViewBase
     */
    protected appDeName: string = 'emwo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarViewBase
     */
    protected appDeKey: string = 'emwoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarViewBase
     */
    protected appDeMajor: string = 'emwoname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarViewBase
     */ 
    protected dataControl: string = 'calendar';

    /**
     * 实体服务对象
     *
     * @type {EMWOService}
     * @memberof EMWOCalendarViewBase
     */
    protected appEntityService: EMWOService = new EMWOService;

    /**
     * 实体权限服务对象
     *
     * @type EMWOUIService
     * @memberof EMWOCalendarViewBase
     */
    public appUIService: EMWOUIService = new EMWOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWOCalendarViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo.views.calendarview.caption',
        srfTitle: 'entities.emwo.views.calendarview.title',
        srfSubTitle: 'entities.emwo.views.calendarview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWOCalendarViewBase
     */
    protected containerModel: any = {
        view_calendar: {
            name: 'calendar',
            type: 'CALENDAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarViewBase
     */
	protected viewtag: string = '53d75eb263d54d235c0da11105639f16';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOCalendarViewBase
     */ 
    protected viewName: string = 'EMWOCalendarView';



    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWOCalendarViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWOCalendarViewBase
     */
    public engineInit(): void {
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWOCalendarView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        this.$Notice.warning({ title: '错误', desc: '请添加新建数据向导视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWOCalendarView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }





}