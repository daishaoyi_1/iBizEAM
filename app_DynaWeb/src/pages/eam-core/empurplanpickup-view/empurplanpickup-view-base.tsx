import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMPurPlanService from '@/service/empur-plan/empur-plan-service';
import EMPurPlanAuthService from '@/authservice/empur-plan/empur-plan-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMPurPlanUIService from '@/uiservice/empur-plan/empur-plan-ui-service';

/**
 * 计划修理数据选择视图视图基类
 *
 * @export
 * @class EMPURPLANPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMPURPLANPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPURPLANPickupViewBase
     */
    protected appDeName: string = 'empurplan';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPURPLANPickupViewBase
     */
    protected appDeKey: string = 'empurplanid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPURPLANPickupViewBase
     */
    protected appDeMajor: string = 'empurplanname';

    /**
     * 实体服务对象
     *
     * @type {EMPurPlanService}
     * @memberof EMPURPLANPickupViewBase
     */
    protected appEntityService: EMPurPlanService = new EMPurPlanService;

    /**
     * 实体权限服务对象
     *
     * @type EMPurPlanUIService
     * @memberof EMPURPLANPickupViewBase
     */
    public appUIService: EMPurPlanUIService = new EMPurPlanUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPURPLANPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.empurplan.views.pickupview.caption',
        srfTitle: 'entities.empurplan.views.pickupview.title',
        srfSubTitle: 'entities.empurplan.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPURPLANPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPURPLANPickupViewBase
     */
	protected viewtag: string = '33b626dfdee0554e09a307eefdea4896';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPURPLANPickupViewBase
     */ 
    protected viewName: string = 'EMPURPLANPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPURPLANPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPURPLANPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPURPLANPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'empurplan',
            majorPSDEField: 'empurplanname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPURPLANPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPURPLANPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPURPLANPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}