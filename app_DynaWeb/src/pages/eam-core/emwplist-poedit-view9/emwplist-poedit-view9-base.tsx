import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMWPListService from '@/service/emwplist/emwplist-service';
import EMWPListAuthService from '@/authservice/emwplist/emwplist-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMWPListUIService from '@/uiservice/emwplist/emwplist-ui-service';

/**
 * 采购申请视图基类
 *
 * @export
 * @class EMWPListPOEditView9Base
 * @extends {EditView9Base}
 */
export class EMWPListPOEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListPOEditView9Base
     */
    protected appDeName: string = 'emwplist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWPListPOEditView9Base
     */
    protected appDeKey: string = 'emwplistid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWPListPOEditView9Base
     */
    protected appDeMajor: string = 'emwplistname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListPOEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWPListService}
     * @memberof EMWPListPOEditView9Base
     */
    protected appEntityService: EMWPListService = new EMWPListService;

    /**
     * 实体权限服务对象
     *
     * @type EMWPListUIService
     * @memberof EMWPListPOEditView9Base
     */
    public appUIService: EMWPListUIService = new EMWPListUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWPListPOEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emwplist.views.poeditview9.caption',
        srfTitle: 'entities.emwplist.views.poeditview9.title',
        srfSubTitle: 'entities.emwplist.views.poeditview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWPListPOEditView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWPListPOEditView9Base
     */
	protected viewtag: string = 'b89a04d208a822c7beac6eb000ce5b0c';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListPOEditView9Base
     */ 
    protected viewName: string = 'EMWPListPOEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWPListPOEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWPListPOEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWPListPOEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwplist',
            majorPSDEField: 'emwplistname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListPOEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListPOEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListPOEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMWPListPOEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}