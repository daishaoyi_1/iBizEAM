import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMItemPUseService from '@/service/emitem-puse/emitem-puse-service';
import EMItemPUseAuthService from '@/authservice/emitem-puse/emitem-puse-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMItemPUseUIService from '@/uiservice/emitem-puse/emitem-puse-ui-service';

/**
 * 领料单选择表格视图视图基类
 *
 * @export
 * @class EMITEMPUSEPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMITEMPUSEPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    protected appDeName: string = 'emitempuse';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    protected appDeKey: string = 'emitempuseid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    protected appDeMajor: string = 'itempuseinfo';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPUSEPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMItemPUseService}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    protected appEntityService: EMItemPUseService = new EMItemPUseService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemPUseUIService
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public appUIService: EMItemPUseUIService = new EMItemPUseUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitempuse.views.pickupgridview.caption',
        srfTitle: 'entities.emitempuse.views.pickupgridview.title',
        srfSubTitle: 'entities.emitempuse.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
	protected viewtag: string = 'acf29e56426bf396fba28fe99fdb866e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPUSEPickupGridViewBase
     */ 
    protected viewName: string = 'EMITEMPUSEPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMITEMPUSEPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emitempuse',
            majorPSDEField: 'itempuseinfo',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMITEMPUSEPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}