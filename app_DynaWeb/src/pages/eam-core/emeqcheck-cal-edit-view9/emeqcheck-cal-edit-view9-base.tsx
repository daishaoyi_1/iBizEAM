import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMEQCheckService from '@/service/emeqcheck/emeqcheck-service';
import EMEQCheckAuthService from '@/authservice/emeqcheck/emeqcheck-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMEQCheckUIService from '@/uiservice/emeqcheck/emeqcheck-ui-service';

/**
 * 检定记录信息视图基类
 *
 * @export
 * @class EMEQCheckCalEditView9Base
 * @extends {EditView9Base}
 */
export class EMEQCheckCalEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQCheckCalEditView9Base
     */
    protected appDeName: string = 'emeqcheck';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQCheckCalEditView9Base
     */
    protected appDeKey: string = 'emeqcheckid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQCheckCalEditView9Base
     */
    protected appDeMajor: string = 'emeqcheckname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQCheckCalEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQCheckService}
     * @memberof EMEQCheckCalEditView9Base
     */
    protected appEntityService: EMEQCheckService = new EMEQCheckService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQCheckUIService
     * @memberof EMEQCheckCalEditView9Base
     */
    public appUIService: EMEQCheckUIService = new EMEQCheckUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQCheckCalEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emeqcheck.views.caleditview9.caption',
        srfTitle: 'entities.emeqcheck.views.caleditview9.title',
        srfSubTitle: 'entities.emeqcheck.views.caleditview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQCheckCalEditView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQCheckCalEditView9Base
     */
	protected viewtag: string = '6857fec47fc1a673fac07954629f47ac';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQCheckCalEditView9Base
     */ 
    protected viewName: string = 'EMEQCheckCalEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQCheckCalEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQCheckCalEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQCheckCalEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqcheck',
            majorPSDEField: 'emeqcheckname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQCheckCalEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQCheckCalEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQCheckCalEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMEQCheckCalEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}