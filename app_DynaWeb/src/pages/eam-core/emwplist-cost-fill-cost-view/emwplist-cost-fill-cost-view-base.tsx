import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import EMWPListCostService from '@/service/emwplist-cost/emwplist-cost-service';
import EMWPListCostAuthService from '@/authservice/emwplist-cost/emwplist-cost-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import EMWPListCostUIService from '@/uiservice/emwplist-cost/emwplist-cost-ui-service';

/**
 * 询价单填报视图基类
 *
 * @export
 * @class EMWPListCostFillCostViewBase
 * @extends {OptionViewBase}
 */
export class EMWPListCostFillCostViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostFillCostViewBase
     */
    protected appDeName: string = 'emwplistcost';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostFillCostViewBase
     */
    protected appDeKey: string = 'emwplistcostid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostFillCostViewBase
     */
    protected appDeMajor: string = 'emwplistcostname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostFillCostViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWPListCostService}
     * @memberof EMWPListCostFillCostViewBase
     */
    protected appEntityService: EMWPListCostService = new EMWPListCostService;

    /**
     * 实体权限服务对象
     *
     * @type EMWPListCostUIService
     * @memberof EMWPListCostFillCostViewBase
     */
    public appUIService: EMWPListCostUIService = new EMWPListCostUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWPListCostFillCostViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWPListCostFillCostViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwplistcost.views.fillcostview.caption',
        srfTitle: 'entities.emwplistcost.views.fillcostview.title',
        srfSubTitle: 'entities.emwplistcost.views.fillcostview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWPListCostFillCostViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostFillCostViewBase
     */
	protected viewtag: string = '2b834cfcd8d9f9cee463041259277a6a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostFillCostViewBase
     */ 
    protected viewName: string = 'EMWPListCostFillCostView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWPListCostFillCostViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWPListCostFillCostViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWPListCostFillCostViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwplistcost',
            majorPSDEField: 'emwplistcostname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostFillCostViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostFillCostViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostFillCostViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}