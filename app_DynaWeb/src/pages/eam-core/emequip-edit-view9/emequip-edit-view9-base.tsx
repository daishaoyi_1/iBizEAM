import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMEquipService from '@/service/emequip/emequip-service';
import EMEquipAuthService from '@/authservice/emequip/emequip-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMEquipUIService from '@/uiservice/emequip/emequip-ui-service';

/**
 * 设备档案编辑视图视图基类
 *
 * @export
 * @class EMEquipEditView9Base
 * @extends {EditView9Base}
 */
export class EMEquipEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEquipEditView9Base
     */
    protected appDeName: string = 'emequip';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEquipEditView9Base
     */
    protected appDeKey: string = 'emequipid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEquipEditView9Base
     */
    protected appDeMajor: string = 'emequipname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEquipEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEquipService}
     * @memberof EMEquipEditView9Base
     */
    protected appEntityService: EMEquipService = new EMEquipService;

    /**
     * 实体权限服务对象
     *
     * @type EMEquipUIService
     * @memberof EMEquipEditView9Base
     */
    public appUIService: EMEquipUIService = new EMEquipUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEquipEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emequip.views.editview9.caption',
        srfTitle: 'entities.emequip.views.editview9.title',
        srfSubTitle: 'entities.emequip.views.editview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEquipEditView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEquipEditView9Base
     */
	protected viewtag: string = '0f00641d113dca697da05070ac4accb9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEquipEditView9Base
     */ 
    protected viewName: string = 'EMEquipEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEquipEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEquipEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEquipEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emequip',
            majorPSDEField: 'emequipname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEquipEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEquipEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEquipEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMEquipEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}