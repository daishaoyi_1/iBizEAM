import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMAssetService from '@/service/emasset/emasset-service';
import EMAssetAuthService from '@/authservice/emasset/emasset-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMAssetUIService from '@/uiservice/emasset/emasset-ui-service';

/**
 * 资产信息视图基类
 *
 * @export
 * @class EMASSETTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMASSETTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETTabExpViewBase
     */
    protected appDeName: string = 'emasset';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMASSETTabExpViewBase
     */
    protected appDeKey: string = 'emassetid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMASSETTabExpViewBase
     */
    protected appDeMajor: string = 'emassetname';

    /**
     * 实体服务对象
     *
     * @type {EMAssetService}
     * @memberof EMASSETTabExpViewBase
     */
    protected appEntityService: EMAssetService = new EMAssetService;

    /**
     * 实体权限服务对象
     *
     * @type EMAssetUIService
     * @memberof EMASSETTabExpViewBase
     */
    public appUIService: EMAssetUIService = new EMAssetUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMASSETTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMASSETTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emasset.views.tabexpview.caption',
        srfTitle: 'entities.emasset.views.tabexpview.title',
        srfSubTitle: 'entities.emasset.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMASSETTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMASSETTabExpViewBase
     */
	protected viewtag: string = 'e9ae358b11db60121bb5e47e8dcb53e6';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETTabExpViewBase
     */ 
    protected viewName: string = 'EMASSETTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMASSETTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMASSETTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMASSETTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emasset',
            majorPSDEField: 'emassetname',
            isLoadDefault: true,
        });
    }


}