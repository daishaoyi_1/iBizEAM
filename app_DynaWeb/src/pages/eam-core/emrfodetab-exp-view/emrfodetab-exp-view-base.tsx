import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMRFODEService from '@/service/emrfode/emrfode-service';
import EMRFODEAuthService from '@/authservice/emrfode/emrfode-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMRFODEUIService from '@/uiservice/emrfode/emrfode-ui-service';

/**
 * 现象视图基类
 *
 * @export
 * @class EMRFODETabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMRFODETabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODETabExpViewBase
     */
    protected appDeName: string = 'emrfode';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFODETabExpViewBase
     */
    protected appDeKey: string = 'emrfodeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFODETabExpViewBase
     */
    protected appDeMajor: string = 'emrfodename';

    /**
     * 实体服务对象
     *
     * @type {EMRFODEService}
     * @memberof EMRFODETabExpViewBase
     */
    protected appEntityService: EMRFODEService = new EMRFODEService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFODEUIService
     * @memberof EMRFODETabExpViewBase
     */
    public appUIService: EMRFODEUIService = new EMRFODEUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMRFODETabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFODETabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfode.views.tabexpview.caption',
        srfTitle: 'entities.emrfode.views.tabexpview.title',
        srfSubTitle: 'entities.emrfode.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFODETabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFODETabExpViewBase
     */
	protected viewtag: string = '1a4fa502229e9c405dd08b07692825b0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODETabExpViewBase
     */ 
    protected viewName: string = 'EMRFODETabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFODETabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFODETabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFODETabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emrfode',
            majorPSDEField: 'emrfodename',
            isLoadDefault: true,
        });
    }


}