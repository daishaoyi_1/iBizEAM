import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMOutputRctService from '@/service/emoutput-rct/emoutput-rct-service';
import EMOutputRctAuthService from '@/authservice/emoutput-rct/emoutput-rct-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMOutputRctUIService from '@/uiservice/emoutput-rct/emoutput-rct-ui-service';

/**
 * 产能信息视图基类
 *
 * @export
 * @class EMOUTPUTRCTEditView9Base
 * @extends {EditView9Base}
 */
export class EMOUTPUTRCTEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMOUTPUTRCTEditView9Base
     */
    protected appDeName: string = 'emoutputrct';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMOUTPUTRCTEditView9Base
     */
    protected appDeKey: string = 'emoutputrctid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMOUTPUTRCTEditView9Base
     */
    protected appDeMajor: string = 'emoutputrctname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMOUTPUTRCTEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMOutputRctService}
     * @memberof EMOUTPUTRCTEditView9Base
     */
    protected appEntityService: EMOutputRctService = new EMOutputRctService;

    /**
     * 实体权限服务对象
     *
     * @type EMOutputRctUIService
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public appUIService: EMOutputRctUIService = new EMOutputRctUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMOUTPUTRCTEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emoutputrct.views.editview9.caption',
        srfTitle: 'entities.emoutputrct.views.editview9.title',
        srfSubTitle: 'entities.emoutputrct.views.editview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMOUTPUTRCTEditView9Base
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMOUTPUTRCTEditView9
     */
    public toolBarModels: any = {
        deuiaction1: { name: 'deuiaction1', caption: 'entities.emoutputrct.editview9toolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emoutputrct.editview9toolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMOUTPUTRCTEditView9Base
     */
	protected viewtag: string = '6d01bf1a43648232f9fb53382d238b4f';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMOUTPUTRCTEditView9Base
     */ 
    protected viewName: string = 'EMOUTPUTRCTEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMOUTPUTRCTEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emoutputrct',
            majorPSDEField: 'emoutputrctname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMOutputRct");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMOUTPUTRCTEditView9Base
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMOUTPUTRCTEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}