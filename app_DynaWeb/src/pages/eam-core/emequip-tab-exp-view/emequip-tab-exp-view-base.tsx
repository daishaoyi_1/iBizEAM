import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMEquipService from '@/service/emequip/emequip-service';
import EMEquipAuthService from '@/authservice/emequip/emequip-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMEquipUIService from '@/uiservice/emequip/emequip-ui-service';

/**
 * 设备档案分页导航视图视图基类
 *
 * @export
 * @class EMEquipTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMEquipTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEquipTabExpViewBase
     */
    protected appDeName: string = 'emequip';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEquipTabExpViewBase
     */
    protected appDeKey: string = 'emequipid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEquipTabExpViewBase
     */
    protected appDeMajor: string = 'emequipname';

    /**
     * 实体服务对象
     *
     * @type {EMEquipService}
     * @memberof EMEquipTabExpViewBase
     */
    protected appEntityService: EMEquipService = new EMEquipService;

    /**
     * 实体权限服务对象
     *
     * @type EMEquipUIService
     * @memberof EMEquipTabExpViewBase
     */
    public appUIService: EMEquipUIService = new EMEquipUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEquipTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEquipTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emequip.views.tabexpview.caption',
        srfTitle: 'entities.emequip.views.tabexpview.title',
        srfSubTitle: 'entities.emequip.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEquipTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEquipTabExpViewBase
     */
	protected viewtag: string = 'bc356258e1901571397cf49dc6544990';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEquipTabExpViewBase
     */ 
    protected viewName: string = 'EMEquipTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEquipTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEquipTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEquipTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emequip',
            majorPSDEField: 'emequipname',
            isLoadDefault: true,
        });
    }


}