import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMAssetService from '@/service/emasset/emasset-service';
import EMAssetAuthService from '@/authservice/emasset/emasset-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMAssetUIService from '@/uiservice/emasset/emasset-ui-service';

/**
 * 资产数据选择视图视图基类
 *
 * @export
 * @class EMASSETPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMASSETPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupViewBase
     */
    protected appDeName: string = 'emasset';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupViewBase
     */
    protected appDeKey: string = 'emassetid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupViewBase
     */
    protected appDeMajor: string = 'emassetname';

    /**
     * 实体服务对象
     *
     * @type {EMAssetService}
     * @memberof EMASSETPickupViewBase
     */
    protected appEntityService: EMAssetService = new EMAssetService;

    /**
     * 实体权限服务对象
     *
     * @type EMAssetUIService
     * @memberof EMASSETPickupViewBase
     */
    public appUIService: EMAssetUIService = new EMAssetUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMASSETPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emasset.views.pickupview.caption',
        srfTitle: 'entities.emasset.views.pickupview.title',
        srfSubTitle: 'entities.emasset.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMASSETPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupViewBase
     */
	protected viewtag: string = '0289b9f5c79a8693cf827b118d901d10';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupViewBase
     */ 
    protected viewName: string = 'EMASSETPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMASSETPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMASSETPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMASSETPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emasset',
            majorPSDEField: 'emassetname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}