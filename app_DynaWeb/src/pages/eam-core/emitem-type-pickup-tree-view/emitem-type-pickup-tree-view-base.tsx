import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupTreeViewBase } from '@/studio-core';
import EMItemTypeService from '@/service/emitem-type/emitem-type-service';
import EMItemTypeAuthService from '@/authservice/emitem-type/emitem-type-auth-service';
import PickupTreeViewEngine from '@engine/view/pickup-tree-view-engine';
import EMItemTypeUIService from '@/uiservice/emitem-type/emitem-type-ui-service';

/**
 * 物品类型选择树视图视图基类
 *
 * @export
 * @class EMItemTypePickupTreeViewBase
 * @extends {PickupTreeViewBase}
 */
export class EMItemTypePickupTreeViewBase extends PickupTreeViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypePickupTreeViewBase
     */
    protected appDeName: string = 'emitemtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypePickupTreeViewBase
     */
    protected appDeKey: string = 'emitemtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypePickupTreeViewBase
     */
    protected appDeMajor: string = 'emitemtypename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypePickupTreeViewBase
     */ 
    protected dataControl: string = 'tree';

    /**
     * 实体服务对象
     *
     * @type {EMItemTypeService}
     * @memberof EMItemTypePickupTreeViewBase
     */
    protected appEntityService: EMItemTypeService = new EMItemTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemTypeUIService
     * @memberof EMItemTypePickupTreeViewBase
     */
    public appUIService: EMItemTypeUIService = new EMItemTypeUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypePickupTreeViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemtype.views.pickuptreeview.caption',
        srfTitle: 'entities.emitemtype.views.pickuptreeview.title',
        srfSubTitle: 'entities.emitemtype.views.pickuptreeview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypePickupTreeViewBase
     */
    protected containerModel: any = {
        view_tree: {
            name: 'tree',
            type: 'TREEVIEW',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypePickupTreeViewBase
     */
	protected viewtag: string = '56ecad33ce61fb67a9daecb447004f2d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypePickupTreeViewBase
     */ 
    protected viewName: string = 'EMItemTypePickupTreeView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemTypePickupTreeViewBase
     */
    public engine: PickupTreeViewEngine = new PickupTreeViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemTypePickupTreeViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemTypePickupTreeViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            tree: this.$refs.tree,
            keyPSDEField: 'emitemtype',
            majorPSDEField: 'emitemtypename',
            isLoadDefault: true,
        });
    }

    /**
     * tree 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypePickupTreeViewBase
     */
    public tree_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('tree', 'selectionchange', $event);
    }

    /**
     * tree 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypePickupTreeViewBase
     */
    public tree_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('tree', 'load', $event);
    }


}