import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMEITIResService from '@/service/emeitires/emeitires-service';
import EMEITIResAuthService from '@/authservice/emeitires/emeitires-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMEITIResUIService from '@/uiservice/emeitires/emeitires-ui-service';

/**
 * 轮胎清单数据选择视图视图基类
 *
 * @export
 * @class EMEITIRESPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMEITIRESPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupViewBase
     */
    protected appDeName: string = 'emeitires';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupViewBase
     */
    protected appDeKey: string = 'emeitiresid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupViewBase
     */
    protected appDeMajor: string = 'emeitiresname';

    /**
     * 实体服务对象
     *
     * @type {EMEITIResService}
     * @memberof EMEITIRESPickupViewBase
     */
    protected appEntityService: EMEITIResService = new EMEITIResService;

    /**
     * 实体权限服务对象
     *
     * @type EMEITIResUIService
     * @memberof EMEITIRESPickupViewBase
     */
    public appUIService: EMEITIResUIService = new EMEITIResUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEITIRESPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeitires.views.pickupview.caption',
        srfTitle: 'entities.emeitires.views.pickupview.title',
        srfSubTitle: 'entities.emeitires.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEITIRESPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupViewBase
     */
	protected viewtag: string = 'eacd73a421bbceea58188203dc9405fc';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupViewBase
     */ 
    protected viewName: string = 'EMEITIRESPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEITIRESPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEITIRESPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEITIRESPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emeitires',
            majorPSDEField: 'emeitiresname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}