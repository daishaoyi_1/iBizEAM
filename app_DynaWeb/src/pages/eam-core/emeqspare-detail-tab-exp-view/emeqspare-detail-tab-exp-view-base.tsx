import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMEQSpareDetailService from '@/service/emeqspare-detail/emeqspare-detail-service';
import EMEQSpareDetailAuthService from '@/authservice/emeqspare-detail/emeqspare-detail-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMEQSpareDetailUIService from '@/uiservice/emeqspare-detail/emeqspare-detail-ui-service';

/**
 * 备件包明细分页导航视图视图基类
 *
 * @export
 * @class EMEQSpareDetailTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMEQSpareDetailTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    protected appDeName: string = 'emeqsparedetail';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    protected appDeKey: string = 'emeqsparedetailid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    protected appDeMajor: string = 'emeqsparedetailname';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareDetailService}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    protected appEntityService: EMEQSpareDetailService = new EMEQSpareDetailService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareDetailUIService
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    public appUIService: EMEQSpareDetailUIService = new EMEQSpareDetailUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqsparedetail.views.tabexpview.caption',
        srfTitle: 'entities.emeqsparedetail.views.tabexpview.title',
        srfSubTitle: 'entities.emeqsparedetail.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
	protected viewtag: string = '935b5026e8ed2eaf101678127b4c886d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareDetailTabExpViewBase
     */ 
    protected viewName: string = 'EMEQSpareDetailTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSpareDetailTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSpareDetailTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emeqsparedetail',
            majorPSDEField: 'emeqsparedetailname',
            isLoadDefault: true,
        });
    }


}