import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMWO_DPService from '@/service/emwo-dp/emwo-dp-service';
import EMWO_DPAuthService from '@/authservice/emwo-dp/emwo-dp-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMWO_DPUIService from '@/uiservice/emwo-dp/emwo-dp-ui-service';

/**
 * 点检工单分页导航视图视图基类
 *
 * @export
 * @class EMWO_DPTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMWO_DPTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPTabExpViewBase
     */
    protected appDeName: string = 'emwo_dp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPTabExpViewBase
     */
    protected appDeKey: string = 'emwo_dpid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPTabExpViewBase
     */
    protected appDeMajor: string = 'emwo_dpname';

    /**
     * 实体服务对象
     *
     * @type {EMWO_DPService}
     * @memberof EMWO_DPTabExpViewBase
     */
    protected appEntityService: EMWO_DPService = new EMWO_DPService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_DPUIService
     * @memberof EMWO_DPTabExpViewBase
     */
    public appUIService: EMWO_DPUIService = new EMWO_DPUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_DPTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_DPTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_dp.views.tabexpview.caption',
        srfTitle: 'entities.emwo_dp.views.tabexpview.title',
        srfSubTitle: 'entities.emwo_dp.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_DPTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPTabExpViewBase
     */
	protected viewtag: string = '122781dc9376a420a1f304b8dbed1d73';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPTabExpViewBase
     */ 
    protected viewName: string = 'EMWO_DPTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_DPTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_DPTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_DPTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emwo_dp',
            majorPSDEField: 'emwo_dpname',
            isLoadDefault: true,
        });
    }


}