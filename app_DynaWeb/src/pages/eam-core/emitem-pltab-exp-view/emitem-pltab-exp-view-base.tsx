import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMItemPLService from '@/service/emitem-pl/emitem-pl-service';
import EMItemPLAuthService from '@/authservice/emitem-pl/emitem-pl-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMItemPLUIService from '@/uiservice/emitem-pl/emitem-pl-ui-service';

/**
 * 损溢单分页导航视图视图基类
 *
 * @export
 * @class EMItemPLTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMItemPLTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPLTabExpViewBase
     */
    protected appDeName: string = 'emitempl';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemPLTabExpViewBase
     */
    protected appDeKey: string = 'emitemplid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemPLTabExpViewBase
     */
    protected appDeMajor: string = 'emitemplname';

    /**
     * 实体服务对象
     *
     * @type {EMItemPLService}
     * @memberof EMItemPLTabExpViewBase
     */
    protected appEntityService: EMItemPLService = new EMItemPLService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemPLUIService
     * @memberof EMItemPLTabExpViewBase
     */
    public appUIService: EMItemPLUIService = new EMItemPLUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemPLTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemPLTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitempl.views.tabexpview.caption',
        srfTitle: 'entities.emitempl.views.tabexpview.title',
        srfSubTitle: 'entities.emitempl.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemPLTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemPLTabExpViewBase
     */
	protected viewtag: string = '6710ded1610085281d4901168c056032';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPLTabExpViewBase
     */ 
    protected viewName: string = 'EMItemPLTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemPLTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemPLTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemPLTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emitempl',
            majorPSDEField: 'emitemplname',
            isLoadDefault: true,
        });
    }


}