import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMEQMPService from '@/service/emeqmp/emeqmp-service';
import EMEQMPAuthService from '@/authservice/emeqmp/emeqmp-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMEQMPUIService from '@/uiservice/emeqmp/emeqmp-ui-service';

/**
 * 设备仪表选择表格视图视图基类
 *
 * @export
 * @class EMEQMPPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMEQMPPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupGridViewBase
     */
    protected appDeName: string = 'emeqmp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupGridViewBase
     */
    protected appDeKey: string = 'emeqmpid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupGridViewBase
     */
    protected appDeMajor: string = 'emeqmpname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMEQMPService}
     * @memberof EMEQMPPickupGridViewBase
     */
    protected appEntityService: EMEQMPService = new EMEQMPService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQMPUIService
     * @memberof EMEQMPPickupGridViewBase
     */
    public appUIService: EMEQMPUIService = new EMEQMPUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQMPPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqmp.views.pickupgridview.caption',
        srfTitle: 'entities.emeqmp.views.pickupgridview.title',
        srfSubTitle: 'entities.emeqmp.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQMPPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupGridViewBase
     */
	protected viewtag: string = '5a322b7e71d91c113657a576853dd13a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupGridViewBase
     */ 
    protected viewName: string = 'EMEQMPPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQMPPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQMPPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQMPPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emeqmp',
            majorPSDEField: 'emeqmpname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMEQMPPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}