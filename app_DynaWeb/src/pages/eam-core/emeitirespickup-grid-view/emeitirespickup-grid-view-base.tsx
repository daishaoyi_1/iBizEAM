import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMEITIResService from '@/service/emeitires/emeitires-service';
import EMEITIResAuthService from '@/authservice/emeitires/emeitires-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMEITIResUIService from '@/uiservice/emeitires/emeitires-ui-service';

/**
 * 轮胎清单选择表格视图视图基类
 *
 * @export
 * @class EMEITIRESPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMEITIRESPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupGridViewBase
     */
    protected appDeName: string = 'emeitires';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupGridViewBase
     */
    protected appDeKey: string = 'emeitiresid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupGridViewBase
     */
    protected appDeMajor: string = 'emeitiresname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMEITIResService}
     * @memberof EMEITIRESPickupGridViewBase
     */
    protected appEntityService: EMEITIResService = new EMEITIResService;

    /**
     * 实体权限服务对象
     *
     * @type EMEITIResUIService
     * @memberof EMEITIRESPickupGridViewBase
     */
    public appUIService: EMEITIResUIService = new EMEITIResUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEITIRESPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeitires.views.pickupgridview.caption',
        srfTitle: 'entities.emeitires.views.pickupgridview.title',
        srfSubTitle: 'entities.emeitires.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEITIRESPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupGridViewBase
     */
	protected viewtag: string = 'd99dd7cfd98eac4ea5385c1b6da2201e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEITIRESPickupGridViewBase
     */ 
    protected viewName: string = 'EMEITIRESPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEITIRESPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEITIRESPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEITIRESPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emeitires',
            majorPSDEField: 'emeitiresname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEITIRESPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMEITIRESPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}