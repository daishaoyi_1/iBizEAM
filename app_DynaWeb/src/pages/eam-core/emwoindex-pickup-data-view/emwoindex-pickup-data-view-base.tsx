import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { IndexPickupDataViewBase } from '@/studio-core';
import EMWOService from '@/service/emwo/emwo-service';
import EMWOAuthService from '@/authservice/emwo/emwo-auth-service';
import EMWOUIService from '@/uiservice/emwo/emwo-ui-service';

/**
 * 工单索引关系选择数据视图视图基类
 *
 * @export
 * @class EMWOIndexPickupDataViewBase
 * @extends {IndexPickupDataViewBase}
 */
export class EMWOIndexPickupDataViewBase extends IndexPickupDataViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOIndexPickupDataViewBase
     */
    protected appDeName: string = 'emwo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWOIndexPickupDataViewBase
     */
    protected appDeKey: string = 'emwoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWOIndexPickupDataViewBase
     */
    protected appDeMajor: string = 'emwoname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOIndexPickupDataViewBase
     */ 
    protected dataControl: string = 'dataview';

    /**
     * 实体服务对象
     *
     * @type {EMWOService}
     * @memberof EMWOIndexPickupDataViewBase
     */
    protected appEntityService: EMWOService = new EMWOService;

    /**
     * 实体权限服务对象
     *
     * @type EMWOUIService
     * @memberof EMWOIndexPickupDataViewBase
     */
    public appUIService: EMWOUIService = new EMWOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWOIndexPickupDataViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo.views.indexpickupdataview.caption',
        srfTitle: 'entities.emwo.views.indexpickupdataview.title',
        srfSubTitle: 'entities.emwo.views.indexpickupdataview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWOIndexPickupDataViewBase
     */
    protected containerModel: any = {
        view_dataview: {
            name: 'dataview',
            type: 'DATAVIEW',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWOIndexPickupDataViewBase
     */
	protected viewtag: string = '7bb9f753ee9f57cf1393c6ba98e5c853';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOIndexPickupDataViewBase
     */ 
    protected viewName: string = 'EMWOIndexPickupDataView';



    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWOIndexPickupDataViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWOIndexPickupDataViewBase
     */
    public engineInit(): void {
    }


}