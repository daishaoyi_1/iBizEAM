import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMRFOACService from '@/service/emrfoac/emrfoac-service';
import EMRFOACAuthService from '@/authservice/emrfoac/emrfoac-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMRFOACUIService from '@/uiservice/emrfoac/emrfoac-ui-service';

/**
 * 方案选择表格视图视图基类
 *
 * @export
 * @class EMRFOACPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMRFOACPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupGridViewBase
     */
    protected appDeName: string = 'emrfoac';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupGridViewBase
     */
    protected appDeKey: string = 'emrfoacid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupGridViewBase
     */
    protected appDeMajor: string = 'emrfoacname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMRFOACService}
     * @memberof EMRFOACPickupGridViewBase
     */
    protected appEntityService: EMRFOACService = new EMRFOACService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFOACUIService
     * @memberof EMRFOACPickupGridViewBase
     */
    public appUIService: EMRFOACUIService = new EMRFOACUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFOACPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfoac.views.pickupgridview.caption',
        srfTitle: 'entities.emrfoac.views.pickupgridview.title',
        srfSubTitle: 'entities.emrfoac.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFOACPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupGridViewBase
     */
	protected viewtag: string = '4471a75e676439c54695de7c5ad1b885';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupGridViewBase
     */ 
    protected viewName: string = 'EMRFOACPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFOACPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFOACPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFOACPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emrfoac',
            majorPSDEField: 'emrfoacname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMRFOACPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}