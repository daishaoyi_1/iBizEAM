import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMStorePartService from '@/service/emstore-part/emstore-part-service';
import EMStorePartAuthService from '@/authservice/emstore-part/emstore-part-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMStorePartUIService from '@/uiservice/emstore-part/emstore-part-ui-service';

/**
 * 仓库库位选择表格视图视图基类
 *
 * @export
 * @class EMSTOREPARTPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMSTOREPARTPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    protected appDeName: string = 'emstorepart';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    protected appDeKey: string = 'emstorepartid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    protected appDeMajor: string = 'emstorepartname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMStorePartService}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    protected appEntityService: EMStorePartService = new EMStorePartService;

    /**
     * 实体权限服务对象
     *
     * @type EMStorePartUIService
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public appUIService: EMStorePartUIService = new EMStorePartUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emstorepart.views.pickupgridview.caption',
        srfTitle: 'entities.emstorepart.views.pickupgridview.title',
        srfSubTitle: 'entities.emstorepart.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
	protected viewtag: string = '38468e08ff871576829e622e36629deb';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupGridViewBase
     */ 
    protected viewName: string = 'EMSTOREPARTPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMSTOREPARTPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emstorepart',
            majorPSDEField: 'emstorepartname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMSTOREPARTPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}