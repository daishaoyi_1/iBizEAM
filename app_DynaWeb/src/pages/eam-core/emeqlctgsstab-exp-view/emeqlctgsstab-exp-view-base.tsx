import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMEQLCTGSSService from '@/service/emeqlctgss/emeqlctgss-service';
import EMEQLCTGSSAuthService from '@/authservice/emeqlctgss/emeqlctgss-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMEQLCTGSSUIService from '@/uiservice/emeqlctgss/emeqlctgss-ui-service';

/**
 * 钢丝绳位置超期预警视图基类
 *
 * @export
 * @class EMEQLCTGSSTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMEQLCTGSSTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    protected appDeName: string = 'emeqlctgss';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    protected appDeMajor: string = 'eqlocationinfo';

    /**
     * 实体服务对象
     *
     * @type {EMEQLCTGSSService}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    protected appEntityService: EMEQLCTGSSService = new EMEQLCTGSSService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLCTGSSUIService
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    public appUIService: EMEQLCTGSSUIService = new EMEQLCTGSSUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlctgss.views.tabexpview.caption',
        srfTitle: 'entities.emeqlctgss.views.tabexpview.title',
        srfSubTitle: 'entities.emeqlctgss.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
	protected viewtag: string = '9e62cb01fbbe5363b01a110b91022c17';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTabExpViewBase
     */ 
    protected viewName: string = 'EMEQLCTGSSTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLCTGSSTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLCTGSSTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emeqlctgss',
            majorPSDEField: 'eqlocationinfo',
            isLoadDefault: true,
        });
    }


}