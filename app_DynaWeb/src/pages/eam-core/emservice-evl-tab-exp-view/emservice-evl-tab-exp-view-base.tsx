import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMServiceEvlService from '@/service/emservice-evl/emservice-evl-service';
import EMServiceEvlAuthService from '@/authservice/emservice-evl/emservice-evl-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMServiceEvlUIService from '@/uiservice/emservice-evl/emservice-evl-ui-service';

/**
 * 服务商评估分页导航视图视图基类
 *
 * @export
 * @class EMServiceEvlTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMServiceEvlTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceEvlTabExpViewBase
     */
    protected appDeName: string = 'emserviceevl';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMServiceEvlTabExpViewBase
     */
    protected appDeKey: string = 'emserviceevlid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMServiceEvlTabExpViewBase
     */
    protected appDeMajor: string = 'emserviceevlname';

    /**
     * 实体服务对象
     *
     * @type {EMServiceEvlService}
     * @memberof EMServiceEvlTabExpViewBase
     */
    protected appEntityService: EMServiceEvlService = new EMServiceEvlService;

    /**
     * 实体权限服务对象
     *
     * @type EMServiceEvlUIService
     * @memberof EMServiceEvlTabExpViewBase
     */
    public appUIService: EMServiceEvlUIService = new EMServiceEvlUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMServiceEvlTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMServiceEvlTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emserviceevl.views.tabexpview.caption',
        srfTitle: 'entities.emserviceevl.views.tabexpview.title',
        srfSubTitle: 'entities.emserviceevl.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMServiceEvlTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMServiceEvlTabExpViewBase
     */
	protected viewtag: string = 'a2783bd8977552b60dec3e905852e094';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceEvlTabExpViewBase
     */ 
    protected viewName: string = 'EMServiceEvlTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMServiceEvlTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMServiceEvlTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMServiceEvlTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emserviceevl',
            majorPSDEField: 'emserviceevlname',
            isLoadDefault: true,
        });
    }


}