import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMWOORIService from '@/service/emwoori/emwoori-service';
import EMWOORIAuthService from '@/authservice/emwoori/emwoori-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMWOORIUIService from '@/uiservice/emwoori/emwoori-ui-service';

/**
 * 工单来源选择表格视图视图基类
 *
 * @export
 * @class EMWOORIPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMWOORIPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupGridViewBase
     */
    protected appDeName: string = 'emwoori';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupGridViewBase
     */
    protected appDeKey: string = 'emwooriid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupGridViewBase
     */
    protected appDeMajor: string = 'emwooriname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMWOORIService}
     * @memberof EMWOORIPickupGridViewBase
     */
    protected appEntityService: EMWOORIService = new EMWOORIService;

    /**
     * 实体权限服务对象
     *
     * @type EMWOORIUIService
     * @memberof EMWOORIPickupGridViewBase
     */
    public appUIService: EMWOORIUIService = new EMWOORIUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWOORIPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwoori.views.pickupgridview.caption',
        srfTitle: 'entities.emwoori.views.pickupgridview.title',
        srfSubTitle: 'entities.emwoori.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWOORIPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupGridViewBase
     */
	protected viewtag: string = '8a1837155cd47ac9f3f0642a57fca79e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupGridViewBase
     */ 
    protected viewName: string = 'EMWOORIPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWOORIPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWOORIPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWOORIPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emwoori',
            majorPSDEField: 'emwooriname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMWOORIPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}