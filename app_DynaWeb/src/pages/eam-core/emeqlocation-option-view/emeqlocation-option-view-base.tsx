import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import EMEQLocationService from '@/service/emeqlocation/emeqlocation-service';
import EMEQLocationAuthService from '@/authservice/emeqlocation/emeqlocation-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import EMEQLocationUIService from '@/uiservice/emeqlocation/emeqlocation-ui-service';

/**
 * 位置新建视图基类
 *
 * @export
 * @class EMEQLocationOptionViewBase
 * @extends {OptionViewBase}
 */
export class EMEQLocationOptionViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationOptionViewBase
     */
    protected appDeName: string = 'emeqlocation';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationOptionViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationOptionViewBase
     */
    protected appDeMajor: string = 'emeqlocationname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationOptionViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQLocationService}
     * @memberof EMEQLocationOptionViewBase
     */
    protected appEntityService: EMEQLocationService = new EMEQLocationService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLocationUIService
     * @memberof EMEQLocationOptionViewBase
     */
    public appUIService: EMEQLocationUIService = new EMEQLocationUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQLocationOptionViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLocationOptionViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlocation.views.optionview.caption',
        srfTitle: 'entities.emeqlocation.views.optionview.title',
        srfSubTitle: 'entities.emeqlocation.views.optionview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLocationOptionViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationOptionViewBase
     */
	protected viewtag: string = '9edfff48da0f53879ff0dd78f482ef93';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationOptionViewBase
     */ 
    protected viewName: string = 'EMEQLocationOptionView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLocationOptionViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLocationOptionViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLocationOptionViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqlocation',
            majorPSDEField: 'emeqlocationname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLocationOptionViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLocationOptionViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLocationOptionViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}