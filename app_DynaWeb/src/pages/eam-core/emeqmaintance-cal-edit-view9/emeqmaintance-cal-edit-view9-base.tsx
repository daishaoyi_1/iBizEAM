import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMEQMaintanceService from '@/service/emeqmaintance/emeqmaintance-service';
import EMEQMaintanceAuthService from '@/authservice/emeqmaintance/emeqmaintance-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMEQMaintanceUIService from '@/uiservice/emeqmaintance/emeqmaintance-ui-service';

/**
 * 维修记录信息视图基类
 *
 * @export
 * @class EMEQMaintanceCalEditView9Base
 * @extends {EditView9Base}
 */
export class EMEQMaintanceCalEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMaintanceCalEditView9Base
     */
    protected appDeName: string = 'emeqmaintance';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQMaintanceCalEditView9Base
     */
    protected appDeKey: string = 'emeqmaintanceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQMaintanceCalEditView9Base
     */
    protected appDeMajor: string = 'emeqmaintancename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMaintanceCalEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQMaintanceService}
     * @memberof EMEQMaintanceCalEditView9Base
     */
    protected appEntityService: EMEQMaintanceService = new EMEQMaintanceService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQMaintanceUIService
     * @memberof EMEQMaintanceCalEditView9Base
     */
    public appUIService: EMEQMaintanceUIService = new EMEQMaintanceUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQMaintanceCalEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emeqmaintance.views.caleditview9.caption',
        srfTitle: 'entities.emeqmaintance.views.caleditview9.title',
        srfSubTitle: 'entities.emeqmaintance.views.caleditview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQMaintanceCalEditView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQMaintanceCalEditView9Base
     */
	protected viewtag: string = '92c6d8367243f17c036c555ccd0685f5';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMaintanceCalEditView9Base
     */ 
    protected viewName: string = 'EMEQMaintanceCalEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQMaintanceCalEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQMaintanceCalEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQMaintanceCalEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqmaintance',
            majorPSDEField: 'emeqmaintancename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMaintanceCalEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMaintanceCalEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMaintanceCalEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMEQMaintanceCalEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}