import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMEQKPService from '@/service/emeqkp/emeqkp-service';
import EMEQKPAuthService from '@/authservice/emeqkp/emeqkp-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMEQKPUIService from '@/uiservice/emeqkp/emeqkp-ui-service';

/**
 * 设备关键点数据选择视图视图基类
 *
 * @export
 * @class EMEQKPPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMEQKPPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupViewBase
     */
    protected appDeName: string = 'emeqkp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupViewBase
     */
    protected appDeKey: string = 'emeqkpid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupViewBase
     */
    protected appDeMajor: string = 'emeqkpname';

    /**
     * 实体服务对象
     *
     * @type {EMEQKPService}
     * @memberof EMEQKPPickupViewBase
     */
    protected appEntityService: EMEQKPService = new EMEQKPService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQKPUIService
     * @memberof EMEQKPPickupViewBase
     */
    public appUIService: EMEQKPUIService = new EMEQKPUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQKPPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqkp.views.pickupview.caption',
        srfTitle: 'entities.emeqkp.views.pickupview.title',
        srfSubTitle: 'entities.emeqkp.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQKPPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupViewBase
     */
	protected viewtag: string = 'be10622b09c9bb6768d4826fb707c9d5';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKPPickupViewBase
     */ 
    protected viewName: string = 'EMEQKPPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQKPPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQKPPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQKPPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emeqkp',
            majorPSDEField: 'emeqkpname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKPPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}