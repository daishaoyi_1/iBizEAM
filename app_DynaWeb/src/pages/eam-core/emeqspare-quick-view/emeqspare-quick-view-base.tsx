import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import EMEQSpareService from '@/service/emeqspare/emeqspare-service';
import EMEQSpareAuthService from '@/authservice/emeqspare/emeqspare-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import EMEQSpareUIService from '@/uiservice/emeqspare/emeqspare-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class EMEQSpareQuickViewBase
 * @extends {OptionViewBase}
 */
export class EMEQSpareQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareQuickViewBase
     */
    protected appDeName: string = 'emeqspare';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareQuickViewBase
     */
    protected appDeKey: string = 'emeqspareid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareQuickViewBase
     */
    protected appDeMajor: string = 'emeqsparename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareQuickViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareService}
     * @memberof EMEQSpareQuickViewBase
     */
    protected appEntityService: EMEQSpareService = new EMEQSpareService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareUIService
     * @memberof EMEQSpareQuickViewBase
     */
    public appUIService: EMEQSpareUIService = new EMEQSpareUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQSpareQuickViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqspare.views.quickview.caption',
        srfTitle: 'entities.emeqspare.views.quickview.title',
        srfSubTitle: 'entities.emeqspare.views.quickview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareQuickViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareQuickViewBase
     */
	protected viewtag: string = 'e7364391d0c70ff537c5be5d3f10dfa0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareQuickViewBase
     */ 
    protected viewName: string = 'EMEQSpareQuickView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSpareQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSpareQuickViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSpareQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqspare',
            majorPSDEField: 'emeqsparename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSpareQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSpareQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSpareQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}