import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupTreeViewBase } from '@/studio-core';
import EMEQTypeService from '@/service/emeqtype/emeqtype-service';
import EMEQTypeAuthService from '@/authservice/emeqtype/emeqtype-auth-service';
import PickupTreeViewEngine from '@engine/view/pickup-tree-view-engine';
import EMEQTypeUIService from '@/uiservice/emeqtype/emeqtype-ui-service';

/**
 * 设备类型视图基类
 *
 * @export
 * @class EMEQTypePickupTreeViewBase
 * @extends {PickupTreeViewBase}
 */
export class EMEQTypePickupTreeViewBase extends PickupTreeViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypePickupTreeViewBase
     */
    protected appDeName: string = 'emeqtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypePickupTreeViewBase
     */
    protected appDeKey: string = 'emeqtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypePickupTreeViewBase
     */
    protected appDeMajor: string = 'emeqtypename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypePickupTreeViewBase
     */ 
    protected dataControl: string = 'tree';

    /**
     * 实体服务对象
     *
     * @type {EMEQTypeService}
     * @memberof EMEQTypePickupTreeViewBase
     */
    protected appEntityService: EMEQTypeService = new EMEQTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQTypeUIService
     * @memberof EMEQTypePickupTreeViewBase
     */
    public appUIService: EMEQTypeUIService = new EMEQTypeUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQTypePickupTreeViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqtype.views.pickuptreeview.caption',
        srfTitle: 'entities.emeqtype.views.pickuptreeview.title',
        srfSubTitle: 'entities.emeqtype.views.pickuptreeview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQTypePickupTreeViewBase
     */
    protected containerModel: any = {
        view_tree: {
            name: 'tree',
            type: 'TREEVIEW',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypePickupTreeViewBase
     */
	protected viewtag: string = '914a020cb21ac8bd1c4907426f54d425';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypePickupTreeViewBase
     */ 
    protected viewName: string = 'EMEQTypePickupTreeView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQTypePickupTreeViewBase
     */
    public engine: PickupTreeViewEngine = new PickupTreeViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQTypePickupTreeViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQTypePickupTreeViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            tree: this.$refs.tree,
            keyPSDEField: 'emeqtype',
            majorPSDEField: 'emeqtypename',
            isLoadDefault: true,
        });
    }

    /**
     * tree 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypePickupTreeViewBase
     */
    public tree_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('tree', 'selectionchange', $event);
    }

    /**
     * tree 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypePickupTreeViewBase
     */
    public tree_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('tree', 'load', $event);
    }


}