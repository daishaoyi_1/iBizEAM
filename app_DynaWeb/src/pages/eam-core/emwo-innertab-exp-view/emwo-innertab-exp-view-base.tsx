import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMWO_INNERService from '@/service/emwo-inner/emwo-inner-service';
import EMWO_INNERAuthService from '@/authservice/emwo-inner/emwo-inner-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMWO_INNERUIService from '@/uiservice/emwo-inner/emwo-inner-ui-service';

/**
 * 内部工单分页导航视图视图基类
 *
 * @export
 * @class EMWO_INNERTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMWO_INNERTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERTabExpViewBase
     */
    protected appDeName: string = 'emwo_inner';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERTabExpViewBase
     */
    protected appDeKey: string = 'emwo_innerid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERTabExpViewBase
     */
    protected appDeMajor: string = 'emwo_innername';

    /**
     * 实体服务对象
     *
     * @type {EMWO_INNERService}
     * @memberof EMWO_INNERTabExpViewBase
     */
    protected appEntityService: EMWO_INNERService = new EMWO_INNERService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_INNERUIService
     * @memberof EMWO_INNERTabExpViewBase
     */
    public appUIService: EMWO_INNERUIService = new EMWO_INNERUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_INNERTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNERTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_inner.views.tabexpview.caption',
        srfTitle: 'entities.emwo_inner.views.tabexpview.title',
        srfSubTitle: 'entities.emwo_inner.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNERTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERTabExpViewBase
     */
	protected viewtag: string = '9420cc9679f9c817cd6443c3174f9c25';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERTabExpViewBase
     */ 
    protected viewName: string = 'EMWO_INNERTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_INNERTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_INNERTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_INNERTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emwo_inner',
            majorPSDEField: 'emwo_innername',
            isLoadDefault: true,
        });
    }


}