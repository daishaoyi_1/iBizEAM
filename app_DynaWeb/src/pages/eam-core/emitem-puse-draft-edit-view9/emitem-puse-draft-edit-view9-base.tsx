import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMItemPUseService from '@/service/emitem-puse/emitem-puse-service';
import EMItemPUseAuthService from '@/authservice/emitem-puse/emitem-puse-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMItemPUseUIService from '@/uiservice/emitem-puse/emitem-puse-ui-service';

/**
 * 领料单视图基类
 *
 * @export
 * @class EMItemPUseDraftEditView9Base
 * @extends {EditView9Base}
 */
export class EMItemPUseDraftEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseDraftEditView9Base
     */
    protected appDeName: string = 'emitempuse';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseDraftEditView9Base
     */
    protected appDeKey: string = 'emitempuseid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseDraftEditView9Base
     */
    protected appDeMajor: string = 'itempuseinfo';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseDraftEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMItemPUseService}
     * @memberof EMItemPUseDraftEditView9Base
     */
    protected appEntityService: EMItemPUseService = new EMItemPUseService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemPUseUIService
     * @memberof EMItemPUseDraftEditView9Base
     */
    public appUIService: EMItemPUseUIService = new EMItemPUseUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemPUseDraftEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emitempuse.views.drafteditview9.caption',
        srfTitle: 'entities.emitempuse.views.drafteditview9.title',
        srfSubTitle: 'entities.emitempuse.views.drafteditview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemPUseDraftEditView9Base
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMItemPUseDraftEditView9
     */
    public toolBarModels: any = {
        tbitem1: { name: 'tbitem1', caption: 'entities.emitempuse.drafteditview9toolbar_toolbar.tbitem1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emitempuse.drafteditview9toolbar_toolbar.tbitem1.tip', iconcls: 'fa fa-save', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Save', target: '', class: '' } },

        deuiaction2_submit: { name: 'deuiaction2_submit', caption: 'entities.emitempuse.drafteditview9toolbar_toolbar.deuiaction2_submit.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emitempuse.drafteditview9toolbar_toolbar.deuiaction2_submit.tip', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Submit', target: 'SINGLEKEY', class: '' } },

        deuiaction1: { name: 'deuiaction1', caption: 'entities.emitempuse.drafteditview9toolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emitempuse.drafteditview9toolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseDraftEditView9Base
     */
	protected viewtag: string = '502f6483cbc49cf1faf4b7c3df519aa9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseDraftEditView9Base
     */ 
    protected viewName: string = 'EMItemPUseDraftEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemPUseDraftEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemPUseDraftEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemPUseDraftEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emitempuse',
            majorPSDEField: 'itempuseinfo',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseDraftEditView9Base
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1')) {
            this.toolbar_tbitem1_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'deuiaction2_submit')) {
            this.toolbar_deuiaction2_submit_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseDraftEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseDraftEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemPUseDraftEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Save(datas, contextJO,paramJO,  $event, xData,this,"EMItemPUse");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction2_submit_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:EMItemPUseUIService  = new EMItemPUseUIService();
        curUIService.EMItemPUse_Submit(datas,contextJO, paramJO,  $event, xData,this,"EMItemPUse");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMItemPUse");
    }

    /**
     * 保存
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMItemPUseDraftEditView9Base
     */
    public Save(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        // 界面行为容器对象 _this
        const _this: any = this;
        if (xData && xData.save instanceof Function) {
            xData.save().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                _this.$emit('viewdataschange', [{ ...response.data }]);
            });
        } else if (_this.save && _this.save instanceof Function) {
            _this.save();
        }
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMItemPUseDraftEditView9Base
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMItemPUseDraftEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}