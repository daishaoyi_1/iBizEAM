import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMRFOMOService from '@/service/emrfomo/emrfomo-service';
import EMRFOMOAuthService from '@/authservice/emrfomo/emrfomo-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMRFOMOUIService from '@/uiservice/emrfomo/emrfomo-ui-service';

/**
 * 模式数据选择视图视图基类
 *
 * @export
 * @class EMRFOMOPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMRFOMOPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupViewBase
     */
    protected appDeName: string = 'emrfomo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupViewBase
     */
    protected appDeKey: string = 'emrfomoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupViewBase
     */
    protected appDeMajor: string = 'emrfomoname';

    /**
     * 实体服务对象
     *
     * @type {EMRFOMOService}
     * @memberof EMRFOMOPickupViewBase
     */
    protected appEntityService: EMRFOMOService = new EMRFOMOService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFOMOUIService
     * @memberof EMRFOMOPickupViewBase
     */
    public appUIService: EMRFOMOUIService = new EMRFOMOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFOMOPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfomo.views.pickupview.caption',
        srfTitle: 'entities.emrfomo.views.pickupview.title',
        srfSubTitle: 'entities.emrfomo.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFOMOPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupViewBase
     */
	protected viewtag: string = '2b7b1e11e03a9b07c03faec090fd16bf';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupViewBase
     */ 
    protected viewName: string = 'EMRFOMOPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFOMOPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFOMOPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFOMOPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emrfomo',
            majorPSDEField: 'emrfomoname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}