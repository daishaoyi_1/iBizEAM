import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMPODetailService from '@/service/empodetail/empodetail-service';
import EMPODetailAuthService from '@/authservice/empodetail/empodetail-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMPODetailUIService from '@/uiservice/empodetail/empodetail-ui-service';

/**
 * 订单条目选择表格视图视图基类
 *
 * @export
 * @class EMPODETAILPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMPODETAILPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupGridViewBase
     */
    protected appDeName: string = 'empodetail';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupGridViewBase
     */
    protected appDeKey: string = 'empodetailid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupGridViewBase
     */
    protected appDeMajor: string = 'empodetailname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMPODetailService}
     * @memberof EMPODETAILPickupGridViewBase
     */
    protected appEntityService: EMPODetailService = new EMPODetailService;

    /**
     * 实体权限服务对象
     *
     * @type EMPODetailUIService
     * @memberof EMPODETAILPickupGridViewBase
     */
    public appUIService: EMPODetailUIService = new EMPODetailUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPODETAILPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.empodetail.views.pickupgridview.caption',
        srfTitle: 'entities.empodetail.views.pickupgridview.title',
        srfSubTitle: 'entities.empodetail.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPODETAILPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupGridViewBase
     */
	protected viewtag: string = 'b5856be28a055635fa82ccc842d1baa4';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupGridViewBase
     */ 
    protected viewName: string = 'EMPODETAILPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPODETAILPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPODETAILPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPODETAILPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'empodetail',
            majorPSDEField: 'empodetailname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMPODETAILPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}