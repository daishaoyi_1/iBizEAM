import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMPlanService from '@/service/emplan/emplan-service';
import EMPlanAuthService from '@/authservice/emplan/emplan-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMPlanUIService from '@/uiservice/emplan/emplan-ui-service';

/**
 * 计划数据选择视图视图基类
 *
 * @export
 * @class EMPLANPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMPLANPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANPickupViewBase
     */
    protected appDeName: string = 'emplan';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPLANPickupViewBase
     */
    protected appDeKey: string = 'emplanid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPLANPickupViewBase
     */
    protected appDeMajor: string = 'emplanname';

    /**
     * 实体服务对象
     *
     * @type {EMPlanService}
     * @memberof EMPLANPickupViewBase
     */
    protected appEntityService: EMPlanService = new EMPlanService;

    /**
     * 实体权限服务对象
     *
     * @type EMPlanUIService
     * @memberof EMPLANPickupViewBase
     */
    public appUIService: EMPlanUIService = new EMPlanUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPLANPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emplan.views.pickupview.caption',
        srfTitle: 'entities.emplan.views.pickupview.title',
        srfSubTitle: 'entities.emplan.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPLANPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPLANPickupViewBase
     */
	protected viewtag: string = '1def25c7e51403af4ba84bae7e112131';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANPickupViewBase
     */ 
    protected viewName: string = 'EMPLANPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPLANPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPLANPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPLANPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emplan',
            majorPSDEField: 'emplanname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}