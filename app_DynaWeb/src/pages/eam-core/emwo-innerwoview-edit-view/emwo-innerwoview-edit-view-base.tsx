import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMWO_INNERService from '@/service/emwo-inner/emwo-inner-service';
import EMWO_INNERAuthService from '@/authservice/emwo-inner/emwo-inner-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMWO_INNERUIService from '@/uiservice/emwo-inner/emwo-inner-ui-service';

/**
 * 内部工单视图基类
 *
 * @export
 * @class EMWO_INNERWOViewEditViewBase
 * @extends {EditViewBase}
 */
export class EMWO_INNERWOViewEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    protected appDeName: string = 'emwo_inner';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    protected appDeKey: string = 'emwo_innerid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    protected appDeMajor: string = 'emwo_innername';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERWOViewEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWO_INNERService}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    protected appEntityService: EMWO_INNERService = new EMWO_INNERService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_INNERUIService
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    public appUIService: EMWO_INNERUIService = new EMWO_INNERUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_inner.views.wovieweditview.caption',
        srfTitle: 'entities.emwo_inner.views.wovieweditview.title',
        srfSubTitle: 'entities.emwo_inner.views.wovieweditview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
	protected viewtag: string = '657b8397c50938a3f3d636353a0d332c';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNERWOViewEditViewBase
     */ 
    protected viewName: string = 'EMWO_INNERWOViewEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_INNERWOViewEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwo_inner',
            majorPSDEField: 'emwo_innername',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNERWOViewEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}