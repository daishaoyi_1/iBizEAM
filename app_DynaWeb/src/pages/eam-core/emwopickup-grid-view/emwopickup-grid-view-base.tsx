import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMWOService from '@/service/emwo/emwo-service';
import EMWOAuthService from '@/authservice/emwo/emwo-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMWOUIService from '@/uiservice/emwo/emwo-ui-service';

/**
 * 工单选择表格视图视图基类
 *
 * @export
 * @class EMWOPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMWOPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOPickupGridViewBase
     */
    protected appDeName: string = 'emwo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWOPickupGridViewBase
     */
    protected appDeKey: string = 'emwoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWOPickupGridViewBase
     */
    protected appDeMajor: string = 'emwoname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMWOService}
     * @memberof EMWOPickupGridViewBase
     */
    protected appEntityService: EMWOService = new EMWOService;

    /**
     * 实体权限服务对象
     *
     * @type EMWOUIService
     * @memberof EMWOPickupGridViewBase
     */
    public appUIService: EMWOUIService = new EMWOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWOPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo.views.pickupgridview.caption',
        srfTitle: 'entities.emwo.views.pickupgridview.title',
        srfSubTitle: 'entities.emwo.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWOPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWOPickupGridViewBase
     */
	protected viewtag: string = '4fb41066362194e1d35664a060f64799';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOPickupGridViewBase
     */ 
    protected viewName: string = 'EMWOPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWOPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWOPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWOPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emwo',
            majorPSDEField: 'emwoname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMWOPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}