import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMItemCSService from '@/service/emitem-cs/emitem-cs-service';
import EMItemCSAuthService from '@/authservice/emitem-cs/emitem-cs-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMItemCSUIService from '@/uiservice/emitem-cs/emitem-cs-ui-service';

/**
 * 库间调整单分页导航视图视图基类
 *
 * @export
 * @class EMItemCSTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMItemCSTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemCSTabExpViewBase
     */
    protected appDeName: string = 'emitemcs';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemCSTabExpViewBase
     */
    protected appDeKey: string = 'emitemcsid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemCSTabExpViewBase
     */
    protected appDeMajor: string = 'emitemcsname';

    /**
     * 实体服务对象
     *
     * @type {EMItemCSService}
     * @memberof EMItemCSTabExpViewBase
     */
    protected appEntityService: EMItemCSService = new EMItemCSService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemCSUIService
     * @memberof EMItemCSTabExpViewBase
     */
    public appUIService: EMItemCSUIService = new EMItemCSUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemCSTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemCSTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemcs.views.tabexpview.caption',
        srfTitle: 'entities.emitemcs.views.tabexpview.title',
        srfSubTitle: 'entities.emitemcs.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemCSTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemCSTabExpViewBase
     */
	protected viewtag: string = 'fdff4fa9c40fbd3e00f3d72eccaa2b1e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemCSTabExpViewBase
     */ 
    protected viewName: string = 'EMItemCSTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemCSTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemCSTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemCSTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emitemcs',
            majorPSDEField: 'emitemcsname',
            isLoadDefault: true,
        });
    }


}