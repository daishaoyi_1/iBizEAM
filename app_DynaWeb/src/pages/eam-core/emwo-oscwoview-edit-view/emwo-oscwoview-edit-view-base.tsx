import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMWO_OSCService from '@/service/emwo-osc/emwo-osc-service';
import EMWO_OSCAuthService from '@/authservice/emwo-osc/emwo-osc-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMWO_OSCUIService from '@/uiservice/emwo-osc/emwo-osc-ui-service';

/**
 * 外委工单视图基类
 *
 * @export
 * @class EMWO_OSCWOViewEditViewBase
 * @extends {EditViewBase}
 */
export class EMWO_OSCWOViewEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    protected appDeName: string = 'emwo_osc';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    protected appDeKey: string = 'emwo_oscid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    protected appDeMajor: string = 'emwo_oscname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCWOViewEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWO_OSCService}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    protected appEntityService: EMWO_OSCService = new EMWO_OSCService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_OSCUIService
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    public appUIService: EMWO_OSCUIService = new EMWO_OSCUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_osc.views.wovieweditview.caption',
        srfTitle: 'entities.emwo_osc.views.wovieweditview.title',
        srfSubTitle: 'entities.emwo_osc.views.wovieweditview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
	protected viewtag: string = '1dbea6147c18d41d86921cf624bef2c2';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCWOViewEditViewBase
     */ 
    protected viewName: string = 'EMWO_OSCWOViewEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_OSCWOViewEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwo_osc',
            majorPSDEField: 'emwo_oscname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_OSCWOViewEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}