import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMItemRInService from '@/service/emitem-rin/emitem-rin-service';
import EMItemRInAuthService from '@/authservice/emitem-rin/emitem-rin-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMItemRInUIService from '@/uiservice/emitem-rin/emitem-rin-ui-service';

/**
 * 入库单数据选择视图视图基类
 *
 * @export
 * @class EMITEMRINPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMITEMRINPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupViewBase
     */
    protected appDeName: string = 'emitemrin';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupViewBase
     */
    protected appDeKey: string = 'emitemrinid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupViewBase
     */
    protected appDeMajor: string = 'emitemrinname';

    /**
     * 实体服务对象
     *
     * @type {EMItemRInService}
     * @memberof EMITEMRINPickupViewBase
     */
    protected appEntityService: EMItemRInService = new EMItemRInService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemRInUIService
     * @memberof EMITEMRINPickupViewBase
     */
    public appUIService: EMItemRInUIService = new EMItemRInUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMITEMRINPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemrin.views.pickupview.caption',
        srfTitle: 'entities.emitemrin.views.pickupview.title',
        srfSubTitle: 'entities.emitemrin.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMITEMRINPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupViewBase
     */
	protected viewtag: string = 'c87da32eb30d04c1d169fd5a8e0b8785';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupViewBase
     */ 
    protected viewName: string = 'EMITEMRINPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMITEMRINPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMITEMRINPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMITEMRINPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emitemrin',
            majorPSDEField: 'emitemrinname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}