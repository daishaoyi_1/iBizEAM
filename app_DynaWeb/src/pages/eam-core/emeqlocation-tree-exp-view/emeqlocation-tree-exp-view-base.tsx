import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import EMEQLocationService from '@/service/emeqlocation/emeqlocation-service';
import EMEQLocationAuthService from '@/authservice/emeqlocation/emeqlocation-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import EMEQLocationUIService from '@/uiservice/emeqlocation/emeqlocation-ui-service';

/**
 * 位置树导航视图视图基类
 *
 * @export
 * @class EMEQLocationTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class EMEQLocationTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationTreeExpViewBase
     */
    protected appDeName: string = 'emeqlocation';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationTreeExpViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationTreeExpViewBase
     */
    protected appDeMajor: string = 'emeqlocationname';

    /**
     * 实体服务对象
     *
     * @type {EMEQLocationService}
     * @memberof EMEQLocationTreeExpViewBase
     */
    protected appEntityService: EMEQLocationService = new EMEQLocationService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLocationUIService
     * @memberof EMEQLocationTreeExpViewBase
     */
    public appUIService: EMEQLocationUIService = new EMEQLocationUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQLocationTreeExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLocationTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlocation.views.treeexpview.caption',
        srfTitle: 'entities.emeqlocation.views.treeexpview.title',
        srfSubTitle: 'entities.emeqlocation.views.treeexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLocationTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: {
            name: 'treeexpbar',
            type: 'TREEEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationTreeExpViewBase
     */
	protected viewtag: string = 'd352394004ff4ccbd04efa771ac4de31';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLocationTreeExpViewBase
     */ 
    protected viewName: string = 'EMEQLocationTreeExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLocationTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLocationTreeExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLocationTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'emeqlocation',
            majorPSDEField: 'emeqlocationname',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLocationTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLocationTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLocationTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQLocationTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQLocationTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMEQLocationTreeExpView
     */
    public viewUID: string = 'eam-core-emeqlocation-tree-exp-view';


}