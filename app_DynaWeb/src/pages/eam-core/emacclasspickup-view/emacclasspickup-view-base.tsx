import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMACClassService from '@/service/emacclass/emacclass-service';
import EMACClassAuthService from '@/authservice/emacclass/emacclass-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMACClassUIService from '@/uiservice/emacclass/emacclass-ui-service';

/**
 * 总帐科目数据选择视图视图基类
 *
 * @export
 * @class EMACCLASSPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMACCLASSPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupViewBase
     */
    protected appDeName: string = 'emacclass';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupViewBase
     */
    protected appDeKey: string = 'emacclassid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupViewBase
     */
    protected appDeMajor: string = 'emacclassname';

    /**
     * 实体服务对象
     *
     * @type {EMACClassService}
     * @memberof EMACCLASSPickupViewBase
     */
    protected appEntityService: EMACClassService = new EMACClassService;

    /**
     * 实体权限服务对象
     *
     * @type EMACClassUIService
     * @memberof EMACCLASSPickupViewBase
     */
    public appUIService: EMACClassUIService = new EMACClassUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMACCLASSPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emacclass.views.pickupview.caption',
        srfTitle: 'entities.emacclass.views.pickupview.title',
        srfSubTitle: 'entities.emacclass.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMACCLASSPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupViewBase
     */
	protected viewtag: string = '7016b925f12b320d5c647f897e38fa5b';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupViewBase
     */ 
    protected viewName: string = 'EMACCLASSPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMACCLASSPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMACCLASSPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMACCLASSPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emacclass',
            majorPSDEField: 'emacclassname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}