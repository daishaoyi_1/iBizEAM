import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMItemPUseService from '@/service/emitem-puse/emitem-puse-service';
import EMItemPUseAuthService from '@/authservice/emitem-puse/emitem-puse-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMItemPUseUIService from '@/uiservice/emitem-puse/emitem-puse-ui-service';

/**
 * 领料单分页导航视图视图基类
 *
 * @export
 * @class EMItemPUseTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMItemPUseTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseTabExpViewBase
     */
    protected appDeName: string = 'emitempuse';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseTabExpViewBase
     */
    protected appDeKey: string = 'emitempuseid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseTabExpViewBase
     */
    protected appDeMajor: string = 'itempuseinfo';

    /**
     * 实体服务对象
     *
     * @type {EMItemPUseService}
     * @memberof EMItemPUseTabExpViewBase
     */
    protected appEntityService: EMItemPUseService = new EMItemPUseService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemPUseUIService
     * @memberof EMItemPUseTabExpViewBase
     */
    public appUIService: EMItemPUseUIService = new EMItemPUseUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemPUseTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemPUseTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitempuse.views.tabexpview.caption',
        srfTitle: 'entities.emitempuse.views.tabexpview.title',
        srfSubTitle: 'entities.emitempuse.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemPUseTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseTabExpViewBase
     */
	protected viewtag: string = 'adda1a8a1008b45ec3ec3f18e6b3d783';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPUseTabExpViewBase
     */ 
    protected viewName: string = 'EMItemPUseTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemPUseTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemPUseTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemPUseTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emitempuse',
            majorPSDEField: 'itempuseinfo',
            isLoadDefault: true,
        });
    }


}