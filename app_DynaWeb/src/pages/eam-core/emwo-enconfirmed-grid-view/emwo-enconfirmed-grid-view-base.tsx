
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { GridViewBase } from '@/studio-core';
import EMWO_ENService from '@/service/emwo-en/emwo-en-service';
import EMWO_ENAuthService from '@/authservice/emwo-en/emwo-en-auth-service';
import GridViewEngine from '@engine/view/grid-view-engine';
import EMWO_ENUIService from '@/uiservice/emwo-en/emwo-en-ui-service';
import CodeListService from '@service/app/codelist-service';


/**
 * 能耗登记工单表格视图视图基类
 *
 * @export
 * @class EMWO_ENConfirmedGridViewBase
 * @extends {GridViewBase}
 */
export class EMWO_ENConfirmedGridViewBase extends GridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    protected appDeName: string = 'emwo_en';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    protected appDeKey: string = 'emwo_enid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    protected appDeMajor: string = 'emwo_enname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENConfirmedGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMWO_ENService}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    protected appEntityService: EMWO_ENService = new EMWO_ENService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_ENUIService
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public appUIService: EMWO_ENUIService = new EMWO_ENUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_en.views.confirmedgridview.caption',
        srfTitle: 'entities.emwo_en.views.confirmedgridview.title',
        srfSubTitle: 'entities.emwo_en.views.confirmedgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
	protected viewtag: string = '0ac7f785d30a6ec3312287fd463ec80d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENConfirmedGridViewBase
     */ 
    protected viewName: string = 'EMWO_ENConfirmedGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public engine: GridViewEngine = new GridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_ENConfirmedGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.opendata(args, fullargs, params, $event, xData);
            },
            newdata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.newdata(args, fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emwo_en',
            majorPSDEField: 'emwo_enname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENConfirmedGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWO_ENConfirmedGridView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.emwo_en;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emequip && true){
            deResParameters = [
            { pathName: 'emequips', parameterName: 'emequip' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emwo_ens', parameterName: 'emwo_en' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emwo-enedit-view-edit-mode', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emwo_en.views.editview_editmode.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWO_ENConfirmedGridView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emequip && true){
            deResParameters = [
            { pathName: 'emequips', parameterName: 'emequip' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emwo_ens', parameterName: 'emwo_en' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emwo-enedit-view-edit-mode', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emwo_en.views.editview_editmode.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


}