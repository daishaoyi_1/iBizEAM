import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMObjectService from '@/service/emobject/emobject-service';
import EMObjectAuthService from '@/authservice/emobject/emobject-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMObjectUIService from '@/uiservice/emobject/emobject-ui-service';

/**
 * 对象数据选择视图视图基类
 *
 * @export
 * @class EMOBJECTPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMOBJECTPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupViewBase
     */
    protected appDeName: string = 'emobject';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupViewBase
     */
    protected appDeKey: string = 'emobjectid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupViewBase
     */
    protected appDeMajor: string = 'emobjectname';

    /**
     * 实体服务对象
     *
     * @type {EMObjectService}
     * @memberof EMOBJECTPickupViewBase
     */
    protected appEntityService: EMObjectService = new EMObjectService;

    /**
     * 实体权限服务对象
     *
     * @type EMObjectUIService
     * @memberof EMOBJECTPickupViewBase
     */
    public appUIService: EMObjectUIService = new EMObjectUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMOBJECTPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emobject.views.pickupview.caption',
        srfTitle: 'entities.emobject.views.pickupview.title',
        srfSubTitle: 'entities.emobject.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMOBJECTPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupViewBase
     */
	protected viewtag: string = 'f5364bc2c7121b5c69e1c1078834cf27';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupViewBase
     */ 
    protected viewName: string = 'EMOBJECTPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMOBJECTPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMOBJECTPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMOBJECTPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emobject',
            majorPSDEField: 'emobjectname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}