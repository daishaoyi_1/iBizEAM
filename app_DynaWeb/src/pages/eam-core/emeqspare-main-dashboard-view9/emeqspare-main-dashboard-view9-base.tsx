import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DashboardView9Base } from '@/studio-core';
import EMEQSpareService from '@/service/emeqspare/emeqspare-service';
import EMEQSpareAuthService from '@/authservice/emeqspare/emeqspare-auth-service';
import PortalView9Engine from '@engine/view/portal-view9-engine';
import EMEQSpareUIService from '@/uiservice/emeqspare/emeqspare-ui-service';

/**
 * 备件包信息视图基类
 *
 * @export
 * @class EMEQSpareMainDashboardView9Base
 * @extends {DashboardView9Base}
 */
export class EMEQSpareMainDashboardView9Base extends DashboardView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    protected appDeName: string = 'emeqspare';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    protected appDeKey: string = 'emeqspareid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    protected appDeMajor: string = 'emeqsparename';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareService}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    protected appEntityService: EMEQSpareService = new EMEQSpareService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareUIService
     * @memberof EMEQSpareMainDashboardView9Base
     */
    public appUIService: EMEQSpareUIService = new EMEQSpareUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQSpareMainDashboardView9Base
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emeqspare.views.maindashboardview9.caption',
        srfTitle: 'entities.emeqspare.views.maindashboardview9.title',
        srfSubTitle: 'entities.emeqspare.views.maindashboardview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    protected containerModel: any = {
        view_dashboard: {
            name: 'dashboard',
            type: 'DASHBOARD',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareMainDashboardView9Base
     */
	protected viewtag: string = 'ee0078ab458bfa82654013369f231ff7';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareMainDashboardView9Base
     */ 
    protected viewName: string = 'EMEQSpareMainDashboardView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    public engine: PortalView9Engine = new PortalView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSpareMainDashboardView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSpareMainDashboardView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'emeqspare',
            majorPSDEField: 'emeqsparename',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSpareMainDashboardView9Base
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }

    /** 
     * 数据看板部件刷新状态
     * 
     * @type {boolean}
     * @memberof EMEQSpareMainDashboardView9Base
     */
    public state: boolean = true;

    /** 
     * 刷新
     * 
     * @memberof EMEQSpareMainDashboardView9Base
     */
    public refresh(args: any){
        this.state = false;
        setTimeout(() => {
            this.state = true;
            this.loadModel();
        }, 0);
    }

}