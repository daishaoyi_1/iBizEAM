import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMItemService from '@/service/emitem/emitem-service';
import EMItemAuthService from '@/authservice/emitem/emitem-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMItemUIService from '@/uiservice/emitem/emitem-ui-service';

/**
 * 物品分页导航视图视图基类
 *
 * @export
 * @class EMItemTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMItemTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTabExpViewBase
     */
    protected appDeName: string = 'emitem';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemTabExpViewBase
     */
    protected appDeKey: string = 'emitemid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemTabExpViewBase
     */
    protected appDeMajor: string = 'emitemname';

    /**
     * 实体服务对象
     *
     * @type {EMItemService}
     * @memberof EMItemTabExpViewBase
     */
    protected appEntityService: EMItemService = new EMItemService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemUIService
     * @memberof EMItemTabExpViewBase
     */
    public appUIService: EMItemUIService = new EMItemUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitem.views.tabexpview.caption',
        srfTitle: 'entities.emitem.views.tabexpview.title',
        srfSubTitle: 'entities.emitem.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemTabExpViewBase
     */
	protected viewtag: string = '60330d8050c9f91a156a6703579fb40b';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTabExpViewBase
     */ 
    protected viewName: string = 'EMItemTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emitem',
            majorPSDEField: 'emitemname',
            isLoadDefault: true,
        });
    }


}