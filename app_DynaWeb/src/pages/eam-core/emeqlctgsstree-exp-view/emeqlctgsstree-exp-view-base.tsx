import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import EMEQLCTGSSService from '@/service/emeqlctgss/emeqlctgss-service';
import EMEQLCTGSSAuthService from '@/authservice/emeqlctgss/emeqlctgss-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import EMEQLCTGSSUIService from '@/uiservice/emeqlctgss/emeqlctgss-ui-service';

/**
 * 钢丝绳位置树导航视图基类
 *
 * @export
 * @class EMEQLCTGSSTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class EMEQLCTGSSTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    protected appDeName: string = 'emeqlctgss';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    protected appDeMajor: string = 'eqlocationinfo';

    /**
     * 实体服务对象
     *
     * @type {EMEQLCTGSSService}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    protected appEntityService: EMEQLCTGSSService = new EMEQLCTGSSService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLCTGSSUIService
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    public appUIService: EMEQLCTGSSUIService = new EMEQLCTGSSUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlctgss.views.treeexpview.caption',
        srfTitle: 'entities.emeqlctgss.views.treeexpview.title',
        srfSubTitle: 'entities.emeqlctgss.views.treeexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: {
            name: 'treeexpbar',
            type: 'TREEEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
	protected viewtag: string = 'c3e594cee0ee5850e34a7e2947f310dc';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */ 
    protected viewName: string = 'EMEQLCTGSSTreeExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLCTGSSTreeExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'emeqlctgss',
            majorPSDEField: 'eqlocationinfo',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTGSSTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQLCTGSSTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQLCTGSSTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMEQLCTGSSTreeExpView
     */
    public viewUID: string = 'eam-core-emeqlctgsstree-exp-view';


}