import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMWPListService from '@/service/emwplist/emwplist-service';
import EMWPListAuthService from '@/authservice/emwplist/emwplist-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMWPListUIService from '@/uiservice/emwplist/emwplist-ui-service';

/**
 * 采购申请数据选择视图视图基类
 *
 * @export
 * @class EMWPLISTPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMWPLISTPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupViewBase
     */
    protected appDeName: string = 'emwplist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupViewBase
     */
    protected appDeKey: string = 'emwplistid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupViewBase
     */
    protected appDeMajor: string = 'emwplistname';

    /**
     * 实体服务对象
     *
     * @type {EMWPListService}
     * @memberof EMWPLISTPickupViewBase
     */
    protected appEntityService: EMWPListService = new EMWPListService;

    /**
     * 实体权限服务对象
     *
     * @type EMWPListUIService
     * @memberof EMWPLISTPickupViewBase
     */
    public appUIService: EMWPListUIService = new EMWPListUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWPLISTPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwplist.views.pickupview.caption',
        srfTitle: 'entities.emwplist.views.pickupview.title',
        srfSubTitle: 'entities.emwplist.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWPLISTPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupViewBase
     */
	protected viewtag: string = '826b9bc8847e18dcee5e1e3eacf560a3';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupViewBase
     */ 
    protected viewName: string = 'EMWPLISTPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWPLISTPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWPLISTPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWPLISTPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emwplist',
            majorPSDEField: 'emwplistname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}