import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMRFOACService from '@/service/emrfoac/emrfoac-service';
import EMRFOACAuthService from '@/authservice/emrfoac/emrfoac-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMRFOACUIService from '@/uiservice/emrfoac/emrfoac-ui-service';

/**
 * 方案数据选择视图视图基类
 *
 * @export
 * @class EMRFOACPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMRFOACPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupViewBase
     */
    protected appDeName: string = 'emrfoac';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupViewBase
     */
    protected appDeKey: string = 'emrfoacid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupViewBase
     */
    protected appDeMajor: string = 'emrfoacname';

    /**
     * 实体服务对象
     *
     * @type {EMRFOACService}
     * @memberof EMRFOACPickupViewBase
     */
    protected appEntityService: EMRFOACService = new EMRFOACService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFOACUIService
     * @memberof EMRFOACPickupViewBase
     */
    public appUIService: EMRFOACUIService = new EMRFOACUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFOACPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfoac.views.pickupview.caption',
        srfTitle: 'entities.emrfoac.views.pickupview.title',
        srfSubTitle: 'entities.emrfoac.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFOACPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupViewBase
     */
	protected viewtag: string = 'e65d9e21d03473e58b782d7d671bc276';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOACPickupViewBase
     */ 
    protected viewName: string = 'EMRFOACPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFOACPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFOACPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFOACPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emrfoac',
            majorPSDEField: 'emrfoacname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOACPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}