import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMAssetService from '@/service/emasset/emasset-service';
import EMAssetAuthService from '@/authservice/emasset/emasset-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMAssetUIService from '@/uiservice/emasset/emasset-ui-service';

/**
 * 资产选择表格视图视图基类
 *
 * @export
 * @class EMASSETPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMASSETPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupGridViewBase
     */
    protected appDeName: string = 'emasset';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupGridViewBase
     */
    protected appDeKey: string = 'emassetid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupGridViewBase
     */
    protected appDeMajor: string = 'emassetname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMAssetService}
     * @memberof EMASSETPickupGridViewBase
     */
    protected appEntityService: EMAssetService = new EMAssetService;

    /**
     * 实体权限服务对象
     *
     * @type EMAssetUIService
     * @memberof EMASSETPickupGridViewBase
     */
    public appUIService: EMAssetUIService = new EMAssetUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMASSETPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emasset.views.pickupgridview.caption',
        srfTitle: 'entities.emasset.views.pickupgridview.title',
        srfSubTitle: 'entities.emasset.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMASSETPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupGridViewBase
     */
	protected viewtag: string = 'c37cc465f06ab2e97e128b8e52883bcf';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETPickupGridViewBase
     */ 
    protected viewName: string = 'EMASSETPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMASSETPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMASSETPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMASSETPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emasset',
            majorPSDEField: 'emassetname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMASSETPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}