import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { CalendarExpViewBase } from '@/studio-core';
import EMEQAHService from '@/service/emeqah/emeqah-service';
import EMEQAHAuthService from '@/authservice/emeqah/emeqah-auth-service';
import CalendarExpViewEngine from '@engine/view/calendar-exp-view-engine';
import EMEQAHUIService from '@/uiservice/emeqah/emeqah-ui-service';

/**
 * 设备活动日历导航视图视图基类
 *
 * @export
 * @class EMEQAHEqCalendarExpViewBase
 * @extends {CalendarExpViewBase}
 */
export class EMEQAHEqCalendarExpViewBase extends CalendarExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    protected appDeName: string = 'emeqah';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    protected appDeKey: string = 'emeqahid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    protected appDeMajor: string = 'emeqahname';

    /**
     * 实体服务对象
     *
     * @type {EMEQAHService}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    protected appEntityService: EMEQAHService = new EMEQAHService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQAHUIService
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    public appUIService: EMEQAHUIService = new EMEQAHUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqah.views.eqcalendarexpview.caption',
        srfTitle: 'entities.emeqah.views.eqcalendarexpview.title',
        srfSubTitle: 'entities.emeqah.views.eqcalendarexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    protected containerModel: any = {
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
        view_calendarexpbar: {
            name: 'calendarexpbar',
            type: 'CALENDAREXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
	protected viewtag: string = '66ce65d2786eb2f62372b4e73ff0a728';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHEqCalendarExpViewBase
     */ 
    protected viewName: string = 'EMEQAHEqCalendarExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    public engine: CalendarExpViewEngine = new CalendarExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQAHEqCalendarExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            calendarexpbar: this.$refs.calendarexpbar,
            keyPSDEField: 'emeqah',
            majorPSDEField: 'emeqahname',
            isLoadDefault: true,
        });
    }

    /**
     * calendarexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    public calendarexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'selectionchange', $event);
    }

    /**
     * calendarexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    public calendarexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'activated', $event);
    }

    /**
     * calendarexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQAHEqCalendarExpViewBase
     */
    public calendarexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQAHEqCalendarExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        this.$Notice.warning({ title: '错误', desc: '请添加新建数据向导视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQAHEqCalendarExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMEQAHEqCalendarExpView
     */
    public viewUID: string = 'eam-core-emeqaheq-calendar-exp-view';


}