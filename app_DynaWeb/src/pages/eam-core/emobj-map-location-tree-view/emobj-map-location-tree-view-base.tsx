import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeViewBase } from '@/studio-core';
import EMObjMapService from '@/service/emobj-map/emobj-map-service';
import EMObjMapAuthService from '@/authservice/emobj-map/emobj-map-auth-service';
import TreeViewEngine from '@engine/view/tree-view-engine';
import EMObjMapUIService from '@/uiservice/emobj-map/emobj-map-ui-service';

/**
 * 位置信息视图基类
 *
 * @export
 * @class EMObjMapLocationTreeViewBase
 * @extends {TreeViewBase}
 */
export class EMObjMapLocationTreeViewBase extends TreeViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMObjMapLocationTreeViewBase
     */
    protected appDeName: string = 'emobjmap';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMObjMapLocationTreeViewBase
     */
    protected appDeKey: string = 'emobjmapid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMObjMapLocationTreeViewBase
     */
    protected appDeMajor: string = 'emobjmapname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMObjMapLocationTreeViewBase
     */ 
    protected dataControl: string = 'tree';

    /**
     * 实体服务对象
     *
     * @type {EMObjMapService}
     * @memberof EMObjMapLocationTreeViewBase
     */
    protected appEntityService: EMObjMapService = new EMObjMapService;

    /**
     * 实体权限服务对象
     *
     * @type EMObjMapUIService
     * @memberof EMObjMapLocationTreeViewBase
     */
    public appUIService: EMObjMapUIService = new EMObjMapUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMObjMapLocationTreeViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emobjmap.views.locationtreeview.caption',
        srfTitle: 'entities.emobjmap.views.locationtreeview.title',
        srfSubTitle: 'entities.emobjmap.views.locationtreeview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMObjMapLocationTreeViewBase
     */
    protected containerModel: any = {
        view_tree: {
            name: 'tree',
            type: 'TREEVIEW',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMObjMapLocationTreeViewBase
     */
	protected viewtag: string = 'a47832b9d879fe6dd889f3d00ccfe8bc';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMObjMapLocationTreeViewBase
     */ 
    protected viewName: string = 'EMObjMapLocationTreeView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMObjMapLocationTreeViewBase
     */
    public engine: TreeViewEngine = new TreeViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMObjMapLocationTreeViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMObjMapLocationTreeViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            tree: this.$refs.tree,
            keyPSDEField: 'emobjmap',
            majorPSDEField: 'emobjmapname',
            isLoadDefault: true,
        });
    }

    /**
     * tree 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMObjMapLocationTreeViewBase
     */
    public tree_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('tree', 'selectionchange', $event);
    }

    /**
     * tree 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMObjMapLocationTreeViewBase
     */
    public tree_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('tree', 'load', $event);
    }


}