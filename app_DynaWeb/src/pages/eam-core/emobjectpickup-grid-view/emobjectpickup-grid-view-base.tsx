import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMObjectService from '@/service/emobject/emobject-service';
import EMObjectAuthService from '@/authservice/emobject/emobject-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMObjectUIService from '@/uiservice/emobject/emobject-ui-service';

/**
 * 对象选择表格视图视图基类
 *
 * @export
 * @class EMOBJECTPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMOBJECTPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupGridViewBase
     */
    protected appDeName: string = 'emobject';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupGridViewBase
     */
    protected appDeKey: string = 'emobjectid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupGridViewBase
     */
    protected appDeMajor: string = 'emobjectname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMObjectService}
     * @memberof EMOBJECTPickupGridViewBase
     */
    protected appEntityService: EMObjectService = new EMObjectService;

    /**
     * 实体权限服务对象
     *
     * @type EMObjectUIService
     * @memberof EMOBJECTPickupGridViewBase
     */
    public appUIService: EMObjectUIService = new EMObjectUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMOBJECTPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emobject.views.pickupgridview.caption',
        srfTitle: 'entities.emobject.views.pickupgridview.title',
        srfSubTitle: 'entities.emobject.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMOBJECTPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupGridViewBase
     */
	protected viewtag: string = 'e695986e78ad685904b96a6505b1ddca';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMOBJECTPickupGridViewBase
     */ 
    protected viewName: string = 'EMOBJECTPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMOBJECTPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMOBJECTPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMOBJECTPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emobject',
            majorPSDEField: 'emobjectname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMOBJECTPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMOBJECTPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}