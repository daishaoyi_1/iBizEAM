import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import EMDRWGService from '@/service/emdrwg/emdrwg-service';
import EMDRWGAuthService from '@/authservice/emdrwg/emdrwg-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import EMDRWGUIService from '@/uiservice/emdrwg/emdrwg-ui-service';

/**
 * 文档选项操作视图视图基类
 *
 * @export
 * @class EMDRWGOptionViewBase
 * @extends {OptionViewBase}
 */
export class EMDRWGOptionViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGOptionViewBase
     */
    protected appDeName: string = 'emdrwg';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGOptionViewBase
     */
    protected appDeKey: string = 'emdrwgid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGOptionViewBase
     */
    protected appDeMajor: string = 'emdrwgname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGOptionViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMDRWGService}
     * @memberof EMDRWGOptionViewBase
     */
    protected appEntityService: EMDRWGService = new EMDRWGService;

    /**
     * 实体权限服务对象
     *
     * @type EMDRWGUIService
     * @memberof EMDRWGOptionViewBase
     */
    public appUIService: EMDRWGUIService = new EMDRWGUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMDRWGOptionViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMDRWGOptionViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emdrwg.views.optionview.caption',
        srfTitle: 'entities.emdrwg.views.optionview.title',
        srfSubTitle: 'entities.emdrwg.views.optionview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMDRWGOptionViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGOptionViewBase
     */
	protected viewtag: string = 'd86ec0fbc00d232370aee5ee83457a76';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMDRWGOptionViewBase
     */ 
    protected viewName: string = 'EMDRWGOptionView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMDRWGOptionViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMDRWGOptionViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMDRWGOptionViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emdrwg',
            majorPSDEField: 'emdrwgname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMDRWGOptionViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMDRWGOptionViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMDRWGOptionViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}