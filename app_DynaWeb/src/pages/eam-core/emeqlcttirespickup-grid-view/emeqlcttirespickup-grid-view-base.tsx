import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMEQLCTTIResService from '@/service/emeqlcttires/emeqlcttires-service';
import EMEQLCTTIResAuthService from '@/authservice/emeqlcttires/emeqlcttires-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMEQLCTTIResUIService from '@/uiservice/emeqlcttires/emeqlcttires-ui-service';

/**
 * 轮胎位置选择表格视图视图基类
 *
 * @export
 * @class EMEQLCTTIRESPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMEQLCTTIRESPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    protected appDeName: string = 'emeqlcttires';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    protected appDeMajor: string = 'eqlocationinfo';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMEQLCTTIResService}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    protected appEntityService: EMEQLCTTIResService = new EMEQLCTTIResService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLCTTIResUIService
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public appUIService: EMEQLCTTIResUIService = new EMEQLCTTIResUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlcttires.views.pickupgridview.caption',
        srfTitle: 'entities.emeqlcttires.views.pickupgridview.title',
        srfSubTitle: 'entities.emeqlcttires.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
	protected viewtag: string = '7b869dc8094e17d8288b824d30df7bce';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */ 
    protected viewName: string = 'EMEQLCTTIRESPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emeqlcttires',
            majorPSDEField: 'eqlocationinfo',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMEQLCTTIRESPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}