import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { GridExpViewBase } from '@/studio-core';
import EMEQSpareService from '@/service/emeqspare/emeqspare-service';
import EMEQSpareAuthService from '@/authservice/emeqspare/emeqspare-auth-service';
import GridExpViewEngine from '@engine/view/grid-exp-view-engine';
import EMEQSpareUIService from '@/uiservice/emeqspare/emeqspare-ui-service';

/**
 * 备件包表格导航视图视图基类
 *
 * @export
 * @class EMEQSpareGridExpViewBase
 * @extends {GridExpViewBase}
 */
export class EMEQSpareGridExpViewBase extends GridExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareGridExpViewBase
     */
    protected appDeName: string = 'emeqspare';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareGridExpViewBase
     */
    protected appDeKey: string = 'emeqspareid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareGridExpViewBase
     */
    protected appDeMajor: string = 'emeqsparename';

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareService}
     * @memberof EMEQSpareGridExpViewBase
     */
    protected appEntityService: EMEQSpareService = new EMEQSpareService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQSpareUIService
     * @memberof EMEQSpareGridExpViewBase
     */
    public appUIService: EMEQSpareUIService = new EMEQSpareUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQSpareGridExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareGridExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqspare.views.gridexpview.caption',
        srfTitle: 'entities.emeqspare.views.gridexpview.title',
        srfSubTitle: 'entities.emeqspare.views.gridexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQSpareGridExpViewBase
     */
    protected containerModel: any = {
        view_gridexpbar: {
            name: 'gridexpbar',
            type: 'GRIDEXPBAR',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMEQSpareGridExpView
     */
    public gridexpviewgridexpbar_toolbarModels: any = {
        tbitem3: { name: 'tbitem3', caption: 'entities.emeqspare.gridexpviewgridexpbar_toolbar_toolbar.tbitem3.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emeqspare.gridexpviewgridexpbar_toolbar_toolbar.tbitem3.tip', iconcls: 'fa fa-file-text-o', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'New', target: '', class: '' } },

        tbitem7: {  name: 'tbitem7', type: 'SEPERATOR', visible: true, dataaccaction: '', uiaction: { } },
        tbitem4: { name: 'tbitem4', caption: 'entities.emeqspare.gridexpviewgridexpbar_toolbar_toolbar.tbitem4.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emeqspare.gridexpviewgridexpbar_toolbar_toolbar.tbitem4.tip', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Edit', target: 'SINGLEKEY', class: '' } },

        tbitem26: {  name: 'tbitem26', type: 'SEPERATOR', visible: true, dataaccaction: '', uiaction: { } },
        tbitem8: { name: 'tbitem8', caption: 'entities.emeqspare.gridexpviewgridexpbar_toolbar_toolbar.tbitem8.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emeqspare.gridexpviewgridexpbar_toolbar_toolbar.tbitem8.tip', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'MULTIKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareGridExpViewBase
     */
	protected viewtag: string = '43c620e663f2e9764a9aa525a38ec369';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQSpareGridExpViewBase
     */ 
    protected viewName: string = 'EMEQSpareGridExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQSpareGridExpViewBase
     */
    public engine: GridExpViewEngine = new GridExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQSpareGridExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQSpareGridExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            gridexpbar: this.$refs.gridexpbar,
            keyPSDEField: 'emeqspare',
            majorPSDEField: 'emeqsparename',
            isLoadDefault: true,
        });
    }

    /**
     * gridexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSpareGridExpViewBase
     */
    public gridexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'selectionchange', $event);
    }

    /**
     * gridexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSpareGridExpViewBase
     */
    public gridexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'activated', $event);
    }

    /**
     * gridexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQSpareGridExpViewBase
     */
    public gridexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQSpareGridExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.emeqspare;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqspares', parameterName: 'emeqspare' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emeqspare-quick-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emeqspare.views.quickview.title'),
            placement: 'DRAWER_RIGHT',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQSpareGridExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqspares', parameterName: 'emeqspare' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emeqspare-edit-view-edit-mode', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emeqspare.views.editview_editmode.title'),
            placement: 'DRAWER_RIGHT',
        };
        openDrawer(view, data);
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMEQSpareGridExpView
     */
    public viewUID: string = 'eam-core-emeqspare-grid-exp-view';


}