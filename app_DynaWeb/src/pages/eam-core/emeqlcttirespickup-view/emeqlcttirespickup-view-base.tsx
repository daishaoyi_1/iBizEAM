import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMEQLCTTIResService from '@/service/emeqlcttires/emeqlcttires-service';
import EMEQLCTTIResAuthService from '@/authservice/emeqlcttires/emeqlcttires-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMEQLCTTIResUIService from '@/uiservice/emeqlcttires/emeqlcttires-ui-service';

/**
 * 轮胎位置数据选择视图视图基类
 *
 * @export
 * @class EMEQLCTTIRESPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMEQLCTTIRESPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    protected appDeName: string = 'emeqlcttires';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    protected appDeKey: string = 'emeqlocationid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    protected appDeMajor: string = 'eqlocationinfo';

    /**
     * 实体服务对象
     *
     * @type {EMEQLCTTIResService}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    protected appEntityService: EMEQLCTTIResService = new EMEQLCTTIResService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQLCTTIResUIService
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    public appUIService: EMEQLCTTIResUIService = new EMEQLCTTIResUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqlcttires.views.pickupview.caption',
        srfTitle: 'entities.emeqlcttires.views.pickupview.title',
        srfSubTitle: 'entities.emeqlcttires.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
	protected viewtag: string = '90bdc3d4449bdcbe11978314fff1ba85';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQLCTTIRESPickupViewBase
     */ 
    protected viewName: string = 'EMEQLCTTIRESPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQLCTTIRESPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emeqlcttires',
            majorPSDEField: 'eqlocationinfo',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQLCTTIRESPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}