import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMStorePartService from '@/service/emstore-part/emstore-part-service';
import EMStorePartAuthService from '@/authservice/emstore-part/emstore-part-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMStorePartUIService from '@/uiservice/emstore-part/emstore-part-ui-service';

/**
 * 仓库库位数据选择视图视图基类
 *
 * @export
 * @class EMSTOREPARTPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMSTOREPARTPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupViewBase
     */
    protected appDeName: string = 'emstorepart';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupViewBase
     */
    protected appDeKey: string = 'emstorepartid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupViewBase
     */
    protected appDeMajor: string = 'emstorepartname';

    /**
     * 实体服务对象
     *
     * @type {EMStorePartService}
     * @memberof EMSTOREPARTPickupViewBase
     */
    protected appEntityService: EMStorePartService = new EMStorePartService;

    /**
     * 实体权限服务对象
     *
     * @type EMStorePartUIService
     * @memberof EMSTOREPARTPickupViewBase
     */
    public appUIService: EMStorePartUIService = new EMStorePartUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPARTPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emstorepart.views.pickupview.caption',
        srfTitle: 'entities.emstorepart.views.pickupview.title',
        srfSubTitle: 'entities.emstorepart.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPARTPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupViewBase
     */
	protected viewtag: string = '01453141ea85827b19e188b8c52e16af';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTPickupViewBase
     */ 
    protected viewName: string = 'EMSTOREPARTPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMSTOREPARTPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMSTOREPARTPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMSTOREPARTPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emstorepart',
            majorPSDEField: 'emstorepartname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}