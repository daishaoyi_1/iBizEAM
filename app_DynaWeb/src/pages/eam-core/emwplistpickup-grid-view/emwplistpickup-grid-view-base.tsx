import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMWPListService from '@/service/emwplist/emwplist-service';
import EMWPListAuthService from '@/authservice/emwplist/emwplist-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMWPListUIService from '@/uiservice/emwplist/emwplist-ui-service';

/**
 * 采购申请选择表格视图视图基类
 *
 * @export
 * @class EMWPLISTPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMWPLISTPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupGridViewBase
     */
    protected appDeName: string = 'emwplist';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupGridViewBase
     */
    protected appDeKey: string = 'emwplistid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupGridViewBase
     */
    protected appDeMajor: string = 'emwplistname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMWPListService}
     * @memberof EMWPLISTPickupGridViewBase
     */
    protected appEntityService: EMWPListService = new EMWPListService;

    /**
     * 实体权限服务对象
     *
     * @type EMWPListUIService
     * @memberof EMWPLISTPickupGridViewBase
     */
    public appUIService: EMWPListUIService = new EMWPListUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWPLISTPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwplist.views.pickupgridview.caption',
        srfTitle: 'entities.emwplist.views.pickupgridview.title',
        srfSubTitle: 'entities.emwplist.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWPLISTPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupGridViewBase
     */
	protected viewtag: string = '6830863d50cf63d8314dd2deb129cd89';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPLISTPickupGridViewBase
     */ 
    protected viewName: string = 'EMWPLISTPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWPLISTPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWPLISTPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWPLISTPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emwplist',
            majorPSDEField: 'emwplistname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPLISTPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMWPLISTPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}