import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMWOORIService from '@/service/emwoori/emwoori-service';
import EMWOORIAuthService from '@/authservice/emwoori/emwoori-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMWOORIUIService from '@/uiservice/emwoori/emwoori-ui-service';

/**
 * 工单来源数据选择视图视图基类
 *
 * @export
 * @class EMWOORIPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMWOORIPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupViewBase
     */
    protected appDeName: string = 'emwoori';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupViewBase
     */
    protected appDeKey: string = 'emwooriid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupViewBase
     */
    protected appDeMajor: string = 'emwooriname';

    /**
     * 实体服务对象
     *
     * @type {EMWOORIService}
     * @memberof EMWOORIPickupViewBase
     */
    protected appEntityService: EMWOORIService = new EMWOORIService;

    /**
     * 实体权限服务对象
     *
     * @type EMWOORIUIService
     * @memberof EMWOORIPickupViewBase
     */
    public appUIService: EMWOORIUIService = new EMWOORIUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWOORIPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwoori.views.pickupview.caption',
        srfTitle: 'entities.emwoori.views.pickupview.title',
        srfSubTitle: 'entities.emwoori.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWOORIPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupViewBase
     */
	protected viewtag: string = '50cc35f9e4e3daa351112c6f68c23ea3';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWOORIPickupViewBase
     */ 
    protected viewName: string = 'EMWOORIPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWOORIPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWOORIPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWOORIPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emwoori',
            majorPSDEField: 'emwooriname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWOORIPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}