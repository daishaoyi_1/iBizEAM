import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMStoreService from '@/service/emstore/emstore-service';
import EMStoreAuthService from '@/authservice/emstore/emstore-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMStoreUIService from '@/uiservice/emstore/emstore-ui-service';

/**
 * 仓库数据选择视图视图基类
 *
 * @export
 * @class EMSTOREPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMSTOREPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPickupViewBase
     */
    protected appDeName: string = 'emstore';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPickupViewBase
     */
    protected appDeKey: string = 'emstoreid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPickupViewBase
     */
    protected appDeMajor: string = 'emstorename';

    /**
     * 实体服务对象
     *
     * @type {EMStoreService}
     * @memberof EMSTOREPickupViewBase
     */
    protected appEntityService: EMStoreService = new EMStoreService;

    /**
     * 实体权限服务对象
     *
     * @type EMStoreUIService
     * @memberof EMSTOREPickupViewBase
     */
    public appUIService: EMStoreUIService = new EMStoreUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emstore.views.pickupview.caption',
        srfTitle: 'entities.emstore.views.pickupview.title',
        srfSubTitle: 'entities.emstore.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPickupViewBase
     */
	protected viewtag: string = '8f2059ccc34ca18dc2a99d22a5e08cad';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPickupViewBase
     */ 
    protected viewName: string = 'EMSTOREPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMSTOREPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMSTOREPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMSTOREPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emstore',
            majorPSDEField: 'emstorename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}