import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { GridExpViewBase } from '@/studio-core';
import EMEQTypeService from '@/service/emeqtype/emeqtype-service';
import EMEQTypeAuthService from '@/authservice/emeqtype/emeqtype-auth-service';
import GridExpViewEngine from '@engine/view/grid-exp-view-engine';
import EMEQTypeUIService from '@/uiservice/emeqtype/emeqtype-ui-service';

/**
 * 设备类型表格导航视图视图基类
 *
 * @export
 * @class EMEQTypeGridExpViewBase
 * @extends {GridExpViewBase}
 */
export class EMEQTypeGridExpViewBase extends GridExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeGridExpViewBase
     */
    protected appDeName: string = 'emeqtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeGridExpViewBase
     */
    protected appDeKey: string = 'emeqtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeGridExpViewBase
     */
    protected appDeMajor: string = 'emeqtypename';

    /**
     * 实体服务对象
     *
     * @type {EMEQTypeService}
     * @memberof EMEQTypeGridExpViewBase
     */
    protected appEntityService: EMEQTypeService = new EMEQTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQTypeUIService
     * @memberof EMEQTypeGridExpViewBase
     */
    public appUIService: EMEQTypeUIService = new EMEQTypeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQTypeGridExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQTypeGridExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqtype.views.gridexpview.caption',
        srfTitle: 'entities.emeqtype.views.gridexpview.title',
        srfSubTitle: 'entities.emeqtype.views.gridexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQTypeGridExpViewBase
     */
    protected containerModel: any = {
        view_gridexpbar: {
            name: 'gridexpbar',
            type: 'GRIDEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeGridExpViewBase
     */
	protected viewtag: string = 'a19dc5bb2d1b58402ed06fd884af186e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTypeGridExpViewBase
     */ 
    protected viewName: string = 'EMEQTypeGridExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQTypeGridExpViewBase
     */
    public engine: GridExpViewEngine = new GridExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQTypeGridExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQTypeGridExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            gridexpbar: this.$refs.gridexpbar,
            keyPSDEField: 'emeqtype',
            majorPSDEField: 'emeqtypename',
            isLoadDefault: true,
        });
    }

    /**
     * gridexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypeGridExpViewBase
     */
    public gridexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'selectionchange', $event);
    }

    /**
     * gridexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypeGridExpViewBase
     */
    public gridexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'activated', $event);
    }

    /**
     * gridexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTypeGridExpViewBase
     */
    public gridexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQTypeGridExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQTypeGridExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMEQTypeGridExpView
     */
    public viewUID: string = 'eam-core-emeqtype-grid-exp-view';


}