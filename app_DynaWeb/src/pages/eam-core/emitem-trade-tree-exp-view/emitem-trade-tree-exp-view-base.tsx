import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import EMItemTradeService from '@/service/emitem-trade/emitem-trade-service';
import EMItemTradeAuthService from '@/authservice/emitem-trade/emitem-trade-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import EMItemTradeUIService from '@/uiservice/emitem-trade/emitem-trade-ui-service';

/**
 * 物品交易树导航视图视图基类
 *
 * @export
 * @class EMItemTradeTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class EMItemTradeTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTradeTreeExpViewBase
     */
    protected appDeName: string = 'emitemtrade';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemTradeTreeExpViewBase
     */
    protected appDeKey: string = 'emitemtradeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemTradeTreeExpViewBase
     */
    protected appDeMajor: string = 'emitemtradename';

    /**
     * 实体服务对象
     *
     * @type {EMItemTradeService}
     * @memberof EMItemTradeTreeExpViewBase
     */
    protected appEntityService: EMItemTradeService = new EMItemTradeService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemTradeUIService
     * @memberof EMItemTradeTreeExpViewBase
     */
    public appUIService: EMItemTradeUIService = new EMItemTradeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemTradeTreeExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemTradeTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemtrade.views.treeexpview.caption',
        srfTitle: 'entities.emitemtrade.views.treeexpview.title',
        srfSubTitle: 'entities.emitemtrade.views.treeexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemTradeTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: {
            name: 'treeexpbar',
            type: 'TREEEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemTradeTreeExpViewBase
     */
	protected viewtag: string = '4b969cde25eb0874a7aa7a38ff1d82b0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTradeTreeExpViewBase
     */ 
    protected viewName: string = 'EMItemTradeTreeExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemTradeTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemTradeTreeExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemTradeTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'emitemtrade',
            majorPSDEField: 'emitemtradename',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTradeTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTradeTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTradeTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemTradeTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        this.$Notice.warning({ title: '错误', desc: '请添加新建数据向导视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemTradeTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMItemTradeTreeExpView
     */
    public viewUID: string = 'eam-core-emitem-trade-tree-exp-view';


}