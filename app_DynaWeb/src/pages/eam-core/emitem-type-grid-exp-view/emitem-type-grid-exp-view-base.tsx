import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { GridExpViewBase } from '@/studio-core';
import EMItemTypeService from '@/service/emitem-type/emitem-type-service';
import EMItemTypeAuthService from '@/authservice/emitem-type/emitem-type-auth-service';
import GridExpViewEngine from '@engine/view/grid-exp-view-engine';
import EMItemTypeUIService from '@/uiservice/emitem-type/emitem-type-ui-service';

/**
 * 物品类型表格导航视图视图基类
 *
 * @export
 * @class EMItemTypeGridExpViewBase
 * @extends {GridExpViewBase}
 */
export class EMItemTypeGridExpViewBase extends GridExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeGridExpViewBase
     */
    protected appDeName: string = 'emitemtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeGridExpViewBase
     */
    protected appDeKey: string = 'emitemtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeGridExpViewBase
     */
    protected appDeMajor: string = 'emitemtypename';

    /**
     * 实体服务对象
     *
     * @type {EMItemTypeService}
     * @memberof EMItemTypeGridExpViewBase
     */
    protected appEntityService: EMItemTypeService = new EMItemTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemTypeUIService
     * @memberof EMItemTypeGridExpViewBase
     */
    public appUIService: EMItemTypeUIService = new EMItemTypeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemTypeGridExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypeGridExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemtype.views.gridexpview.caption',
        srfTitle: 'entities.emitemtype.views.gridexpview.title',
        srfSubTitle: 'entities.emitemtype.views.gridexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypeGridExpViewBase
     */
    protected containerModel: any = {
        view_gridexpbar: {
            name: 'gridexpbar',
            type: 'GRIDEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeGridExpViewBase
     */
	protected viewtag: string = '9356eabf7983cc8ef55caa36139d7894';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeGridExpViewBase
     */ 
    protected viewName: string = 'EMItemTypeGridExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemTypeGridExpViewBase
     */
    public engine: GridExpViewEngine = new GridExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemTypeGridExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemTypeGridExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            gridexpbar: this.$refs.gridexpbar,
            keyPSDEField: 'emitemtype',
            majorPSDEField: 'emitemtypename',
            isLoadDefault: true,
        });
    }

    /**
     * gridexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeGridExpViewBase
     */
    public gridexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'selectionchange', $event);
    }

    /**
     * gridexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeGridExpViewBase
     */
    public gridexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'activated', $event);
    }

    /**
     * gridexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeGridExpViewBase
     */
    public gridexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('gridexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemTypeGridExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemTypeGridExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMItemTypeGridExpView
     */
    public viewUID: string = 'eam-core-emitem-type-grid-exp-view';


}