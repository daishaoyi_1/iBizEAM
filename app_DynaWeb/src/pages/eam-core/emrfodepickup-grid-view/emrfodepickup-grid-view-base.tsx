import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMRFODEService from '@/service/emrfode/emrfode-service';
import EMRFODEAuthService from '@/authservice/emrfode/emrfode-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMRFODEUIService from '@/uiservice/emrfode/emrfode-ui-service';

/**
 * 现象选择表格视图视图基类
 *
 * @export
 * @class EMRFODEPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMRFODEPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEPickupGridViewBase
     */
    protected appDeName: string = 'emrfode';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEPickupGridViewBase
     */
    protected appDeKey: string = 'emrfodeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEPickupGridViewBase
     */
    protected appDeMajor: string = 'emrfodename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMRFODEService}
     * @memberof EMRFODEPickupGridViewBase
     */
    protected appEntityService: EMRFODEService = new EMRFODEService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFODEUIService
     * @memberof EMRFODEPickupGridViewBase
     */
    public appUIService: EMRFODEUIService = new EMRFODEUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfode.views.pickupgridview.caption',
        srfTitle: 'entities.emrfode.views.pickupgridview.title',
        srfSubTitle: 'entities.emrfode.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEPickupGridViewBase
     */
	protected viewtag: string = '055b4d344fd137972cf9f6e1e520d3f4';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEPickupGridViewBase
     */ 
    protected viewName: string = 'EMRFODEPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFODEPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFODEPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFODEPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emrfode',
            majorPSDEField: 'emrfodename',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMRFODEPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}