import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMItemRInService from '@/service/emitem-rin/emitem-rin-service';
import EMItemRInAuthService from '@/authservice/emitem-rin/emitem-rin-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMItemRInUIService from '@/uiservice/emitem-rin/emitem-rin-ui-service';

/**
 * 入库单选择表格视图视图基类
 *
 * @export
 * @class EMITEMRINPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMITEMRINPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupGridViewBase
     */
    protected appDeName: string = 'emitemrin';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupGridViewBase
     */
    protected appDeKey: string = 'emitemrinid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupGridViewBase
     */
    protected appDeMajor: string = 'emitemrinname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMItemRInService}
     * @memberof EMITEMRINPickupGridViewBase
     */
    protected appEntityService: EMItemRInService = new EMItemRInService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemRInUIService
     * @memberof EMITEMRINPickupGridViewBase
     */
    public appUIService: EMItemRInUIService = new EMItemRInUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMITEMRINPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemrin.views.pickupgridview.caption',
        srfTitle: 'entities.emitemrin.views.pickupgridview.title',
        srfSubTitle: 'entities.emitemrin.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMITEMRINPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupGridViewBase
     */
	protected viewtag: string = '509bfed4d9fede0b165b92970ff8b133';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMRINPickupGridViewBase
     */ 
    protected viewName: string = 'EMITEMRINPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMITEMRINPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMITEMRINPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMITEMRINPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emitemrin',
            majorPSDEField: 'emitemrinname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMRINPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMITEMRINPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}