import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMRFOMOService from '@/service/emrfomo/emrfomo-service';
import EMRFOMOAuthService from '@/authservice/emrfomo/emrfomo-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMRFOMOUIService from '@/uiservice/emrfomo/emrfomo-ui-service';

/**
 * 模式选择表格视图视图基类
 *
 * @export
 * @class EMRFOMOPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMRFOMOPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupGridViewBase
     */
    protected appDeName: string = 'emrfomo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupGridViewBase
     */
    protected appDeKey: string = 'emrfomoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupGridViewBase
     */
    protected appDeMajor: string = 'emrfomoname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMRFOMOService}
     * @memberof EMRFOMOPickupGridViewBase
     */
    protected appEntityService: EMRFOMOService = new EMRFOMOService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFOMOUIService
     * @memberof EMRFOMOPickupGridViewBase
     */
    public appUIService: EMRFOMOUIService = new EMRFOMOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFOMOPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfomo.views.pickupgridview.caption',
        srfTitle: 'entities.emrfomo.views.pickupgridview.title',
        srfSubTitle: 'entities.emrfomo.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFOMOPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupGridViewBase
     */
	protected viewtag: string = '1aacdb29146860825910174be2d23bc0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOMOPickupGridViewBase
     */ 
    protected viewName: string = 'EMRFOMOPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFOMOPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFOMOPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFOMOPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emrfomo',
            majorPSDEField: 'emrfomoname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOMOPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMRFOMOPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}