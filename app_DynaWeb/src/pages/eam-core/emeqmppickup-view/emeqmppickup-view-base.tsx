import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMEQMPService from '@/service/emeqmp/emeqmp-service';
import EMEQMPAuthService from '@/authservice/emeqmp/emeqmp-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMEQMPUIService from '@/uiservice/emeqmp/emeqmp-ui-service';

/**
 * 设备仪表数据选择视图视图基类
 *
 * @export
 * @class EMEQMPPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMEQMPPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupViewBase
     */
    protected appDeName: string = 'emeqmp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupViewBase
     */
    protected appDeKey: string = 'emeqmpid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupViewBase
     */
    protected appDeMajor: string = 'emeqmpname';

    /**
     * 实体服务对象
     *
     * @type {EMEQMPService}
     * @memberof EMEQMPPickupViewBase
     */
    protected appEntityService: EMEQMPService = new EMEQMPService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQMPUIService
     * @memberof EMEQMPPickupViewBase
     */
    public appUIService: EMEQMPUIService = new EMEQMPUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQMPPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqmp.views.pickupview.caption',
        srfTitle: 'entities.emeqmp.views.pickupview.title',
        srfSubTitle: 'entities.emeqmp.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQMPPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupViewBase
     */
	protected viewtag: string = '4dceb916933fd7c51dda2ceafcccb512';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQMPPickupViewBase
     */ 
    protected viewName: string = 'EMEQMPPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQMPPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQMPPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQMPPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emeqmp',
            majorPSDEField: 'emeqmpname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQMPPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}