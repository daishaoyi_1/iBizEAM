import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMPlanService from '@/service/emplan/emplan-service';
import EMPlanAuthService from '@/authservice/emplan/emplan-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMPlanUIService from '@/uiservice/emplan/emplan-ui-service';

/**
 * 计划视图基类
 *
 * @export
 * @class EMPlanEQInfoBase
 * @extends {EditViewBase}
 */
export class EMPlanEQInfoBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEQInfoBase
     */
    protected appDeName: string = 'emplan';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEQInfoBase
     */
    protected appDeKey: string = 'emplanid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEQInfoBase
     */
    protected appDeMajor: string = 'emplanname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEQInfoBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMPlanService}
     * @memberof EMPlanEQInfoBase
     */
    protected appEntityService: EMPlanService = new EMPlanService;

    /**
     * 实体权限服务对象
     *
     * @type EMPlanUIService
     * @memberof EMPlanEQInfoBase
     */
    public appUIService: EMPlanUIService = new EMPlanUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMPlanEQInfoBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPlanEQInfoBase
     */
    protected model: any = {
        srfCaption: 'entities.emplan.views.eqinfo.caption',
        srfTitle: 'entities.emplan.views.eqinfo.title',
        srfSubTitle: 'entities.emplan.views.eqinfo.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPlanEQInfoBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEQInfoBase
     */
	protected viewtag: string = '87016050a52fc831545c659ede007ce9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEQInfoBase
     */ 
    protected viewName: string = 'EMPlanEQInfo';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPlanEQInfoBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPlanEQInfoBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPlanEQInfoBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emplan',
            majorPSDEField: 'emplanname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEQInfoBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEQInfoBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEQInfoBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}