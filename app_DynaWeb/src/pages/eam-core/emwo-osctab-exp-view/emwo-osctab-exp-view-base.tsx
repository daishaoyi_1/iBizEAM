import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMWO_OSCService from '@/service/emwo-osc/emwo-osc-service';
import EMWO_OSCAuthService from '@/authservice/emwo-osc/emwo-osc-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMWO_OSCUIService from '@/uiservice/emwo-osc/emwo-osc-ui-service';

/**
 * 外委保养工单分页导航视图视图基类
 *
 * @export
 * @class EMWO_OSCTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMWO_OSCTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCTabExpViewBase
     */
    protected appDeName: string = 'emwo_osc';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCTabExpViewBase
     */
    protected appDeKey: string = 'emwo_oscid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCTabExpViewBase
     */
    protected appDeMajor: string = 'emwo_oscname';

    /**
     * 实体服务对象
     *
     * @type {EMWO_OSCService}
     * @memberof EMWO_OSCTabExpViewBase
     */
    protected appEntityService: EMWO_OSCService = new EMWO_OSCService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_OSCUIService
     * @memberof EMWO_OSCTabExpViewBase
     */
    public appUIService: EMWO_OSCUIService = new EMWO_OSCUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_OSCTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_OSCTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_osc.views.tabexpview.caption',
        srfTitle: 'entities.emwo_osc.views.tabexpview.title',
        srfSubTitle: 'entities.emwo_osc.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_OSCTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCTabExpViewBase
     */
	protected viewtag: string = 'ae1aa0b3e079475b6ea9674f59c61233';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_OSCTabExpViewBase
     */ 
    protected viewName: string = 'EMWO_OSCTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_OSCTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_OSCTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_OSCTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emwo_osc',
            majorPSDEField: 'emwo_oscname',
            isLoadDefault: true,
        });
    }


}