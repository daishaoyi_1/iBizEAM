import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMEQDebugService from '@/service/emeqdebug/emeqdebug-service';
import EMEQDebugAuthService from '@/authservice/emeqdebug/emeqdebug-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMEQDebugUIService from '@/uiservice/emeqdebug/emeqdebug-ui-service';

/**
 * 调试记录信息视图基类
 *
 * @export
 * @class EMEQDebugCalEditView9Base
 * @extends {EditView9Base}
 */
export class EMEQDebugCalEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQDebugCalEditView9Base
     */
    protected appDeName: string = 'emeqdebug';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQDebugCalEditView9Base
     */
    protected appDeKey: string = 'emeqdebugid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQDebugCalEditView9Base
     */
    protected appDeMajor: string = 'emeqdebugname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQDebugCalEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQDebugService}
     * @memberof EMEQDebugCalEditView9Base
     */
    protected appEntityService: EMEQDebugService = new EMEQDebugService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQDebugUIService
     * @memberof EMEQDebugCalEditView9Base
     */
    public appUIService: EMEQDebugUIService = new EMEQDebugUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQDebugCalEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emeqdebug.views.caleditview9.caption',
        srfTitle: 'entities.emeqdebug.views.caleditview9.title',
        srfSubTitle: 'entities.emeqdebug.views.caleditview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQDebugCalEditView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQDebugCalEditView9Base
     */
	protected viewtag: string = '94c2eda321740f009f0857c9a1a938a3';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQDebugCalEditView9Base
     */ 
    protected viewName: string = 'EMEQDebugCalEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQDebugCalEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQDebugCalEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQDebugCalEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqdebug',
            majorPSDEField: 'emeqdebugname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQDebugCalEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQDebugCalEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQDebugCalEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMEQDebugCalEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}