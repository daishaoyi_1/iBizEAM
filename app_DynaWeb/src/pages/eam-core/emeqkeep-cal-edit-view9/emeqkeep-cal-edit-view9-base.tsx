import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMEQKeepService from '@/service/emeqkeep/emeqkeep-service';
import EMEQKeepAuthService from '@/authservice/emeqkeep/emeqkeep-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMEQKeepUIService from '@/uiservice/emeqkeep/emeqkeep-ui-service';

/**
 * 保养记录信息视图基类
 *
 * @export
 * @class EMEQKeepCalEditView9Base
 * @extends {EditView9Base}
 */
export class EMEQKeepCalEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKeepCalEditView9Base
     */
    protected appDeName: string = 'emeqkeep';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQKeepCalEditView9Base
     */
    protected appDeKey: string = 'emeqkeepid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQKeepCalEditView9Base
     */
    protected appDeMajor: string = 'emeqkeepname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKeepCalEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMEQKeepService}
     * @memberof EMEQKeepCalEditView9Base
     */
    protected appEntityService: EMEQKeepService = new EMEQKeepService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQKeepUIService
     * @memberof EMEQKeepCalEditView9Base
     */
    public appUIService: EMEQKeepUIService = new EMEQKeepUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQKeepCalEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emeqkeep.views.caleditview9.caption',
        srfTitle: 'entities.emeqkeep.views.caleditview9.title',
        srfSubTitle: 'entities.emeqkeep.views.caleditview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQKeepCalEditView9Base
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQKeepCalEditView9Base
     */
	protected viewtag: string = '353c3ac0d0ab9122060c3c572f103876';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQKeepCalEditView9Base
     */ 
    protected viewName: string = 'EMEQKeepCalEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQKeepCalEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQKeepCalEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQKeepCalEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emeqkeep',
            majorPSDEField: 'emeqkeepname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKeepCalEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKeepCalEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQKeepCalEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMEQKeepCalEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}