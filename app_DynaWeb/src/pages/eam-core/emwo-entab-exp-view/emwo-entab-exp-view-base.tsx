import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMWO_ENService from '@/service/emwo-en/emwo-en-service';
import EMWO_ENAuthService from '@/authservice/emwo-en/emwo-en-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMWO_ENUIService from '@/uiservice/emwo-en/emwo-en-ui-service';

/**
 * 能耗登记工单分页导航视图视图基类
 *
 * @export
 * @class EMWO_ENTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMWO_ENTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENTabExpViewBase
     */
    protected appDeName: string = 'emwo_en';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENTabExpViewBase
     */
    protected appDeKey: string = 'emwo_enid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENTabExpViewBase
     */
    protected appDeMajor: string = 'emwo_enname';

    /**
     * 实体服务对象
     *
     * @type {EMWO_ENService}
     * @memberof EMWO_ENTabExpViewBase
     */
    protected appEntityService: EMWO_ENService = new EMWO_ENService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_ENUIService
     * @memberof EMWO_ENTabExpViewBase
     */
    public appUIService: EMWO_ENUIService = new EMWO_ENUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_ENTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_ENTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_en.views.tabexpview.caption',
        srfTitle: 'entities.emwo_en.views.tabexpview.title',
        srfSubTitle: 'entities.emwo_en.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_ENTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENTabExpViewBase
     */
	protected viewtag: string = 'e9a83d27146867bf19a3167f79bcb93b';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENTabExpViewBase
     */ 
    protected viewName: string = 'EMWO_ENTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_ENTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_ENTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_ENTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emwo_en',
            majorPSDEField: 'emwo_enname',
            isLoadDefault: true,
        });
    }


}