import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMItemService from '@/service/emitem/emitem-service';
import EMItemAuthService from '@/authservice/emitem/emitem-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMItemUIService from '@/uiservice/emitem/emitem-ui-service';

/**
 * 物品选择表格视图视图基类
 *
 * @export
 * @class EMITEMPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMITEMPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPickupGridViewBase
     */
    protected appDeName: string = 'emitem';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPickupGridViewBase
     */
    protected appDeKey: string = 'emitemid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPickupGridViewBase
     */
    protected appDeMajor: string = 'emitemname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMItemService}
     * @memberof EMITEMPickupGridViewBase
     */
    protected appEntityService: EMItemService = new EMItemService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemUIService
     * @memberof EMITEMPickupGridViewBase
     */
    public appUIService: EMItemUIService = new EMItemUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMITEMPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitem.views.pickupgridview.caption',
        srfTitle: 'entities.emitem.views.pickupgridview.title',
        srfSubTitle: 'entities.emitem.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMITEMPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPickupGridViewBase
     */
	protected viewtag: string = 'df7ae7ee10a5abf90410c87e17e65ac4';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMITEMPickupGridViewBase
     */ 
    protected viewName: string = 'EMITEMPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMITEMPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMITEMPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMITEMPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emitem',
            majorPSDEField: 'emitemname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMITEMPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMITEMPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}