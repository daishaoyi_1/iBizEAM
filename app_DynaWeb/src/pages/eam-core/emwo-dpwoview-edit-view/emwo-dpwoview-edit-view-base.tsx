import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMWO_DPService from '@/service/emwo-dp/emwo-dp-service';
import EMWO_DPAuthService from '@/authservice/emwo-dp/emwo-dp-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMWO_DPUIService from '@/uiservice/emwo-dp/emwo-dp-ui-service';

/**
 * 点检工单视图基类
 *
 * @export
 * @class EMWO_DPWOViewEditViewBase
 * @extends {EditViewBase}
 */
export class EMWO_DPWOViewEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPWOViewEditViewBase
     */
    protected appDeName: string = 'emwo_dp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPWOViewEditViewBase
     */
    protected appDeKey: string = 'emwo_dpid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPWOViewEditViewBase
     */
    protected appDeMajor: string = 'emwo_dpname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPWOViewEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWO_DPService}
     * @memberof EMWO_DPWOViewEditViewBase
     */
    protected appEntityService: EMWO_DPService = new EMWO_DPService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_DPUIService
     * @memberof EMWO_DPWOViewEditViewBase
     */
    public appUIService: EMWO_DPUIService = new EMWO_DPUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_DPWOViewEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_DPWOViewEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_dp.views.wovieweditview.caption',
        srfTitle: 'entities.emwo_dp.views.wovieweditview.title',
        srfSubTitle: 'entities.emwo_dp.views.wovieweditview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_DPWOViewEditViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPWOViewEditViewBase
     */
	protected viewtag: string = '8fe19057740a8133e7d31ade73d2daf1';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_DPWOViewEditViewBase
     */ 
    protected viewName: string = 'EMWO_DPWOViewEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_DPWOViewEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_DPWOViewEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_DPWOViewEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwo_dp',
            majorPSDEField: 'emwo_dpname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_DPWOViewEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_DPWOViewEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_DPWOViewEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}