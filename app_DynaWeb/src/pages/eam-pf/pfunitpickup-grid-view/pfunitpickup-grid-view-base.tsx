import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import PFUnitService from '@/service/pfunit/pfunit-service';
import PFUnitAuthService from '@/authservice/pfunit/pfunit-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import PFUnitUIService from '@/uiservice/pfunit/pfunit-ui-service';

/**
 * 计量单位选择表格视图视图基类
 *
 * @export
 * @class PFUNITPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class PFUNITPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupGridViewBase
     */
    protected appDeName: string = 'pfunit';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupGridViewBase
     */
    protected appDeKey: string = 'pfunitid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupGridViewBase
     */
    protected appDeMajor: string = 'pfunitname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {PFUnitService}
     * @memberof PFUNITPickupGridViewBase
     */
    protected appEntityService: PFUnitService = new PFUnitService;

    /**
     * 实体权限服务对象
     *
     * @type PFUnitUIService
     * @memberof PFUNITPickupGridViewBase
     */
    public appUIService: PFUnitUIService = new PFUnitUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFUNITPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfunit.views.pickupgridview.caption',
        srfTitle: 'entities.pfunit.views.pickupgridview.title',
        srfSubTitle: 'entities.pfunit.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFUNITPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupGridViewBase
     */
	protected viewtag: string = '3932d179c351679c58cfd2d9c881852d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupGridViewBase
     */ 
    protected viewName: string = 'PFUNITPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFUNITPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFUNITPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFUNITPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'pfunit',
            majorPSDEField: 'pfunitname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof PFUNITPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}