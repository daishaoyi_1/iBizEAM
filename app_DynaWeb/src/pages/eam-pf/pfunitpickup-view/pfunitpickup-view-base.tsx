import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import PFUnitService from '@/service/pfunit/pfunit-service';
import PFUnitAuthService from '@/authservice/pfunit/pfunit-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import PFUnitUIService from '@/uiservice/pfunit/pfunit-ui-service';

/**
 * 计量单位数据选择视图视图基类
 *
 * @export
 * @class PFUNITPickupViewBase
 * @extends {PickupViewBase}
 */
export class PFUNITPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupViewBase
     */
    protected appDeName: string = 'pfunit';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupViewBase
     */
    protected appDeKey: string = 'pfunitid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupViewBase
     */
    protected appDeMajor: string = 'pfunitname';

    /**
     * 实体服务对象
     *
     * @type {PFUnitService}
     * @memberof PFUNITPickupViewBase
     */
    protected appEntityService: PFUnitService = new PFUnitService;

    /**
     * 实体权限服务对象
     *
     * @type PFUnitUIService
     * @memberof PFUNITPickupViewBase
     */
    public appUIService: PFUnitUIService = new PFUnitUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFUNITPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfunit.views.pickupview.caption',
        srfTitle: 'entities.pfunit.views.pickupview.title',
        srfSubTitle: 'entities.pfunit.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFUNITPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupViewBase
     */
	protected viewtag: string = '7735f11e9c3fec6b77e10e7a6a43c1ea';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFUNITPickupViewBase
     */ 
    protected viewName: string = 'PFUNITPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFUNITPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFUNITPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFUNITPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'pfunit',
            majorPSDEField: 'pfunitname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFUNITPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}