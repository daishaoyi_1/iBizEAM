import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import PFDeptService from '@/service/pfdept/pfdept-service';
import PFDeptAuthService from '@/authservice/pfdept/pfdept-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import PFDeptUIService from '@/uiservice/pfdept/pfdept-ui-service';

/**
 * 部门数据选择视图视图基类
 *
 * @export
 * @class PFDEPTPickupViewBase
 * @extends {PickupViewBase}
 */
export class PFDEPTPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFDEPTPickupViewBase
     */
    protected appDeName: string = 'pfdept';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFDEPTPickupViewBase
     */
    protected appDeKey: string = 'pfdeptid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFDEPTPickupViewBase
     */
    protected appDeMajor: string = 'pfdeptname';

    /**
     * 实体服务对象
     *
     * @type {PFDeptService}
     * @memberof PFDEPTPickupViewBase
     */
    protected appEntityService: PFDeptService = new PFDeptService;

    /**
     * 实体权限服务对象
     *
     * @type PFDeptUIService
     * @memberof PFDEPTPickupViewBase
     */
    public appUIService: PFDeptUIService = new PFDeptUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFDEPTPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfdept.views.pickupview.caption',
        srfTitle: 'entities.pfdept.views.pickupview.title',
        srfSubTitle: 'entities.pfdept.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFDEPTPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFDEPTPickupViewBase
     */
	protected viewtag: string = '3d7728acd1e40b1a95e40157b49d0474';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFDEPTPickupViewBase
     */ 
    protected viewName: string = 'PFDEPTPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFDEPTPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFDEPTPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFDEPTPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'pfdept',
            majorPSDEField: 'pfdeptname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFDEPTPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFDEPTPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFDEPTPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}