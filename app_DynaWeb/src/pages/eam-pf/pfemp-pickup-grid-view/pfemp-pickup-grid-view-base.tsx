import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import PFEmpService from '@/service/pfemp/pfemp-service';
import PFEmpAuthService from '@/authservice/pfemp/pfemp-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import PFEmpUIService from '@/uiservice/pfemp/pfemp-ui-service';

/**
 * 职员选择表格视图视图基类
 *
 * @export
 * @class PFEmpPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class PFEmpPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFEmpPickupGridViewBase
     */
    protected appDeName: string = 'pfemp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFEmpPickupGridViewBase
     */
    protected appDeKey: string = 'pfempid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFEmpPickupGridViewBase
     */
    protected appDeMajor: string = 'pfempname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof PFEmpPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {PFEmpService}
     * @memberof PFEmpPickupGridViewBase
     */
    protected appEntityService: PFEmpService = new PFEmpService;

    /**
     * 实体权限服务对象
     *
     * @type PFEmpUIService
     * @memberof PFEmpPickupGridViewBase
     */
    public appUIService: PFEmpUIService = new PFEmpUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFEmpPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfemp.views.pickupgridview.caption',
        srfTitle: 'entities.pfemp.views.pickupgridview.title',
        srfSubTitle: 'entities.pfemp.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFEmpPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFEmpPickupGridViewBase
     */
	protected viewtag: string = '2de395c6144ba440d0769258b67e9ebc';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFEmpPickupGridViewBase
     */ 
    protected viewName: string = 'PFEmpPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFEmpPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFEmpPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFEmpPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'pfemp',
            majorPSDEField: 'pfempname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEmpPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEmpPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEmpPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEmpPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEmpPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEmpPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEmpPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof PFEmpPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}