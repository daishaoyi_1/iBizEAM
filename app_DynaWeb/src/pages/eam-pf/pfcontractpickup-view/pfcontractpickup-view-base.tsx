import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import PFContractService from '@/service/pfcontract/pfcontract-service';
import PFContractAuthService from '@/authservice/pfcontract/pfcontract-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import PFContractUIService from '@/uiservice/pfcontract/pfcontract-ui-service';

/**
 * 合同数据选择视图视图基类
 *
 * @export
 * @class PFCONTRACTPickupViewBase
 * @extends {PickupViewBase}
 */
export class PFCONTRACTPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupViewBase
     */
    protected appDeName: string = 'pfcontract';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupViewBase
     */
    protected appDeKey: string = 'pfcontractid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupViewBase
     */
    protected appDeMajor: string = 'pfcontractname';

    /**
     * 实体服务对象
     *
     * @type {PFContractService}
     * @memberof PFCONTRACTPickupViewBase
     */
    protected appEntityService: PFContractService = new PFContractService;

    /**
     * 实体权限服务对象
     *
     * @type PFContractUIService
     * @memberof PFCONTRACTPickupViewBase
     */
    public appUIService: PFContractUIService = new PFContractUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFCONTRACTPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfcontract.views.pickupview.caption',
        srfTitle: 'entities.pfcontract.views.pickupview.title',
        srfSubTitle: 'entities.pfcontract.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFCONTRACTPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupViewBase
     */
	protected viewtag: string = '8f347c7ccb2077909f31dee8567670cf';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFCONTRACTPickupViewBase
     */ 
    protected viewName: string = 'PFCONTRACTPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFCONTRACTPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFCONTRACTPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFCONTRACTPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'pfcontract',
            majorPSDEField: 'pfcontractname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFCONTRACTPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}