import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import PFEmpService from '@/service/pfemp/pfemp-service';
import PFEmpAuthService from '@/authservice/pfemp/pfemp-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import PFEmpUIService from '@/uiservice/pfemp/pfemp-ui-service';

/**
 * 职员数据选择视图视图基类
 *
 * @export
 * @class PFEMPPickupViewBase
 * @extends {PickupViewBase}
 */
export class PFEMPPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFEMPPickupViewBase
     */
    protected appDeName: string = 'pfemp';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFEMPPickupViewBase
     */
    protected appDeKey: string = 'pfempid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFEMPPickupViewBase
     */
    protected appDeMajor: string = 'pfempname';

    /**
     * 实体服务对象
     *
     * @type {PFEmpService}
     * @memberof PFEMPPickupViewBase
     */
    protected appEntityService: PFEmpService = new PFEmpService;

    /**
     * 实体权限服务对象
     *
     * @type PFEmpUIService
     * @memberof PFEMPPickupViewBase
     */
    public appUIService: PFEmpUIService = new PFEmpUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFEMPPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfemp.views.pickupview.caption',
        srfTitle: 'entities.pfemp.views.pickupview.title',
        srfSubTitle: 'entities.pfemp.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFEMPPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFEMPPickupViewBase
     */
	protected viewtag: string = 'a6123b46ec5bcf787990953c77183655';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFEMPPickupViewBase
     */ 
    protected viewName: string = 'PFEMPPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFEMPPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFEMPPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFEMPPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'pfemp',
            majorPSDEField: 'pfempname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEMPPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEMPPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFEMPPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}