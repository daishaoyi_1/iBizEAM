import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import PFTeamService from '@/service/pfteam/pfteam-service';
import PFTeamAuthService from '@/authservice/pfteam/pfteam-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import PFTeamUIService from '@/uiservice/pfteam/pfteam-ui-service';

/**
 * 班组选择表格视图视图基类
 *
 * @export
 * @class PFTeamPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class PFTeamPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFTeamPickupGridViewBase
     */
    protected appDeName: string = 'pfteam';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFTeamPickupGridViewBase
     */
    protected appDeKey: string = 'pfteamid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFTeamPickupGridViewBase
     */
    protected appDeMajor: string = 'pfteamname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof PFTeamPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {PFTeamService}
     * @memberof PFTeamPickupGridViewBase
     */
    protected appEntityService: PFTeamService = new PFTeamService;

    /**
     * 实体权限服务对象
     *
     * @type PFTeamUIService
     * @memberof PFTeamPickupGridViewBase
     */
    public appUIService: PFTeamUIService = new PFTeamUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFTeamPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfteam.views.pickupgridview.caption',
        srfTitle: 'entities.pfteam.views.pickupgridview.title',
        srfSubTitle: 'entities.pfteam.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFTeamPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFTeamPickupGridViewBase
     */
	protected viewtag: string = '38a5aaef23bb02699edf0f02dd47fecb';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFTeamPickupGridViewBase
     */ 
    protected viewName: string = 'PFTeamPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFTeamPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFTeamPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFTeamPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'pfteam',
            majorPSDEField: 'pfteamname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTeamPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTeamPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTeamPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTeamPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTeamPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTeamPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTeamPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof PFTeamPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}