import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMPlanTemplService from '@/service/emplan-templ/emplan-templ-service';
import EMPlanTemplAuthService from '@/authservice/emplan-templ/emplan-templ-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMPlanTemplUIService from '@/uiservice/emplan-templ/emplan-templ-ui-service';

/**
 * 计划模板选择表格视图视图基类
 *
 * @export
 * @class EMPLANTEMPLPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMPLANTEMPLPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    protected appDeName: string = 'emplantempl';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    protected appDeKey: string = 'emplantemplid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    protected appDeMajor: string = 'emplantemplname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMPlanTemplService}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    protected appEntityService: EMPlanTemplService = new EMPlanTemplService;

    /**
     * 实体权限服务对象
     *
     * @type EMPlanTemplUIService
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public appUIService: EMPlanTemplUIService = new EMPlanTemplUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emplantempl.views.pickupgridview.caption',
        srfTitle: 'entities.emplantempl.views.pickupgridview.title',
        srfSubTitle: 'entities.emplantempl.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
	protected viewtag: string = '6d54e2dce05866a44e5d6344d8d88970';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */ 
    protected viewName: string = 'EMPLANTEMPLPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emplantempl',
            majorPSDEField: 'emplantemplname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMPLANTEMPLPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}