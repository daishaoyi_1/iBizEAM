
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { GridViewBase } from '@/studio-core';
import EMItemROutService from '@/service/emitem-rout/emitem-rout-service';
import EMItemROutAuthService from '@/authservice/emitem-rout/emitem-rout-auth-service';
import GridViewEngine from '@engine/view/grid-view-engine';
import EMItemROutUIService from '@/uiservice/emitem-rout/emitem-rout-ui-service';
import CodeListService from '@service/app/codelist-service';


/**
 * 退货单视图基类
 *
 * @export
 * @class EMItemROutGridViewBase
 * @extends {GridViewBase}
 */
export class EMItemROutGridViewBase extends GridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutGridViewBase
     */
    protected appDeName: string = 'emitemrout';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutGridViewBase
     */
    protected appDeKey: string = 'emitemroutid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutGridViewBase
     */
    protected appDeMajor: string = 'emitemroutname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMItemROutService}
     * @memberof EMItemROutGridViewBase
     */
    protected appEntityService: EMItemROutService = new EMItemROutService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemROutUIService
     * @memberof EMItemROutGridViewBase
     */
    public appUIService: EMItemROutUIService = new EMItemROutUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemROutGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemrout.views.gridview.caption',
        srfTitle: 'entities.emitemrout.views.gridview.title',
        srfSubTitle: 'entities.emitemrout.views.gridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemROutGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutGridViewBase
     */
	protected viewtag: string = '7664a4e46421531f39ec23d90df57fdd';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemROutGridViewBase
     */ 
    protected viewName: string = 'EMItemROutGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemROutGridViewBase
     */
    public engine: GridViewEngine = new GridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemROutGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemROutGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.opendata(args, fullargs, params, $event, xData);
            },
            newdata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.newdata(args, fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emitemrout',
            majorPSDEField: 'emitemroutname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemROutGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemROutGridView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.emitemrout;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emitem && true){
            deResParameters = [
            { pathName: 'emitems', parameterName: 'emitem' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emitemrouts', parameterName: 'emitemrout' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emitem-rout-edit-view9-new', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emitemrout.views.editview9_new.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemROutGridView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emitem && true){
            deResParameters = [
            { pathName: 'emitems', parameterName: 'emitem' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emitemrouts', parameterName: 'emitemrout' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emitem-rout-edit-view9', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emitemrout.views.editview9.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


}