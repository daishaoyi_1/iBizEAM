import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMItemTypeService from '@/service/emitem-type/emitem-type-service';
import EMItemTypeAuthService from '@/authservice/emitem-type/emitem-type-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMItemTypeUIService from '@/uiservice/emitem-type/emitem-type-ui-service';

/**
 * 物品类型数据选择视图视图基类
 *
 * @export
 * @class EMItemTypeTreePickupViewBase
 * @extends {PickupViewBase}
 */
export class EMItemTypeTreePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeTreePickupViewBase
     */
    protected appDeName: string = 'emitemtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeTreePickupViewBase
     */
    protected appDeKey: string = 'emitemtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeTreePickupViewBase
     */
    protected appDeMajor: string = 'emitemtypename';

    /**
     * 实体服务对象
     *
     * @type {EMItemTypeService}
     * @memberof EMItemTypeTreePickupViewBase
     */
    protected appEntityService: EMItemTypeService = new EMItemTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemTypeUIService
     * @memberof EMItemTypeTreePickupViewBase
     */
    public appUIService: EMItemTypeUIService = new EMItemTypeUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypeTreePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemtype.views.treepickupview.caption',
        srfTitle: 'entities.emitemtype.views.treepickupview.title',
        srfSubTitle: 'entities.emitemtype.views.treepickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypeTreePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeTreePickupViewBase
     */
	protected viewtag: string = '90bf4b8e6b158a78bb5d8dfebfb8c134';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeTreePickupViewBase
     */ 
    protected viewName: string = 'EMItemTypeTreePickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemTypeTreePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemTypeTreePickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemTypeTreePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emitemtype',
            majorPSDEField: 'emitemtypename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeTreePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeTreePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeTreePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}