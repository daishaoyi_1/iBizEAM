import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMRFODEService from '@/service/emrfode/emrfode-service';
import EMRFODEAuthService from '@/authservice/emrfode/emrfode-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMRFODEUIService from '@/uiservice/emrfode/emrfode-ui-service';

/**
 * 现象信息视图基类
 *
 * @export
 * @class EMRFODEEditViewBase
 * @extends {EditViewBase}
 */
export class EMRFODEEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditViewBase
     */
    protected appDeName: string = 'emrfode';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditViewBase
     */
    protected appDeKey: string = 'emrfodeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditViewBase
     */
    protected appDeMajor: string = 'emrfodename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMRFODEService}
     * @memberof EMRFODEEditViewBase
     */
    protected appEntityService: EMRFODEService = new EMRFODEService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFODEUIService
     * @memberof EMRFODEEditViewBase
     */
    public appUIService: EMRFODEUIService = new EMRFODEUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMRFODEEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfode.views.editview.caption',
        srfTitle: 'entities.emrfode.views.editview.title',
        srfSubTitle: 'entities.emrfode.views.editview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMRFODEEditView
     */
    public toolBarModels: any = {
        tbitem1: { name: 'tbitem1', caption: 'entities.emrfode.editviewtoolbar_toolbar.tbitem1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emrfode.editviewtoolbar_toolbar.tbitem1.tip', iconcls: 'fa fa-save', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'SaveAndExit', target: '', class: '' } },

        tbitem2: { name: 'tbitem2', caption: 'entities.emrfode.editviewtoolbar_toolbar.tbitem2.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emrfode.editviewtoolbar_toolbar.tbitem2.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditViewBase
     */
	protected viewtag: string = '748d4667e6d4ccba6195f0751c0b3243';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditViewBase
     */ 
    protected viewName: string = 'EMRFODEEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFODEEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFODEEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFODEEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emrfode',
            majorPSDEField: 'emrfodename',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEEditViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1')) {
            this.toolbar_tbitem1_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem2')) {
            this.toolbar_tbitem2_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.SaveAndExit(datas, contextJO,paramJO,  $event, xData,this,"EMRFODE");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem2_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMRFODE");
    }

    /**
     * 保存并关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMRFODEEditViewBase
     */
    public SaveAndExit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (xData && xData.saveAndExit instanceof Function) {
            xData.saveAndExit().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                if(window.parent){
                    window.parent.postMessage([{ ...response.data }],'*');
                }
            });
        } else if (_this.saveAndExit && _this.saveAndExit instanceof Function) {
            _this.saveAndExit().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                if(window.parent){
                    window.parent.postMessage([{ ...response.data }],'*');
                }
            });
        }
    }
    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMRFODEEditViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}