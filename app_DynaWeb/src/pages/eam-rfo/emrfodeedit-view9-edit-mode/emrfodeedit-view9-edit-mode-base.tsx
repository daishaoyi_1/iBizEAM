import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMRFODEService from '@/service/emrfode/emrfode-service';
import EMRFODEAuthService from '@/authservice/emrfode/emrfode-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMRFODEUIService from '@/uiservice/emrfode/emrfode-ui-service';

/**
 * 现象信息视图基类
 *
 * @export
 * @class EMRFODEEditView9_EditModeBase
 * @extends {EditView9Base}
 */
export class EMRFODEEditView9_EditModeBase extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditView9_EditModeBase
     */
    protected appDeName: string = 'emrfode';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditView9_EditModeBase
     */
    protected appDeKey: string = 'emrfodeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditView9_EditModeBase
     */
    protected appDeMajor: string = 'emrfodename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditView9_EditModeBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMRFODEService}
     * @memberof EMRFODEEditView9_EditModeBase
     */
    protected appEntityService: EMRFODEService = new EMRFODEService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFODEUIService
     * @memberof EMRFODEEditView9_EditModeBase
     */
    public appUIService: EMRFODEUIService = new EMRFODEUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEEditView9_EditModeBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfode.views.editview9_editmode.caption',
        srfTitle: 'entities.emrfode.views.editview9_editmode.title',
        srfSubTitle: 'entities.emrfode.views.editview9_editmode.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEEditView9_EditModeBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditView9_EditModeBase
     */
	protected viewtag: string = '0934b5de6b512221467133b7f396c5aa';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEEditView9_EditModeBase
     */ 
    protected viewName: string = 'EMRFODEEditView9_EditMode';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFODEEditView9_EditModeBase
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFODEEditView9_EditModeBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFODEEditView9_EditModeBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emrfode',
            majorPSDEField: 'emrfodename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEEditView9_EditModeBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEEditView9_EditModeBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEEditView9_EditModeBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMRFODEEditView9_EditModeBase
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}