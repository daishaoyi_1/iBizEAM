import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMRFOCAService from '@/service/emrfoca/emrfoca-service';
import EMRFOCAAuthService from '@/authservice/emrfoca/emrfoca-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMRFOCAUIService from '@/uiservice/emrfoca/emrfoca-ui-service';

/**
 * 原因信息视图基类
 *
 * @export
 * @class EMRFOCAEditViewBase
 * @extends {EditViewBase}
 */
export class EMRFOCAEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAEditViewBase
     */
    protected appDeName: string = 'emrfoca';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAEditViewBase
     */
    protected appDeKey: string = 'emrfocaid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAEditViewBase
     */
    protected appDeMajor: string = 'emrfocaname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMRFOCAService}
     * @memberof EMRFOCAEditViewBase
     */
    protected appEntityService: EMRFOCAService = new EMRFOCAService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFOCAUIService
     * @memberof EMRFOCAEditViewBase
     */
    public appUIService: EMRFOCAUIService = new EMRFOCAUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMRFOCAEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFOCAEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfoca.views.editview.caption',
        srfTitle: 'entities.emrfoca.views.editview.title',
        srfSubTitle: 'entities.emrfoca.views.editview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFOCAEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMRFOCAEditView
     */
    public toolBarModels: any = {
        tbitem1: { name: 'tbitem1', caption: 'entities.emrfoca.editviewtoolbar_toolbar.tbitem1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emrfoca.editviewtoolbar_toolbar.tbitem1.tip', iconcls: 'fa fa-save', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'SaveAndExit', target: '', class: '' } },

        tbitem2: { name: 'tbitem2', caption: 'entities.emrfoca.editviewtoolbar_toolbar.tbitem2.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emrfoca.editviewtoolbar_toolbar.tbitem2.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAEditViewBase
     */
	protected viewtag: string = '3eb428b686c4ebedc04a6828493703b7';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAEditViewBase
     */ 
    protected viewName: string = 'EMRFOCAEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFOCAEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFOCAEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFOCAEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emrfoca',
            majorPSDEField: 'emrfocaname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOCAEditViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1')) {
            this.toolbar_tbitem1_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem2')) {
            this.toolbar_tbitem2_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOCAEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOCAEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOCAEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.SaveAndExit(datas, contextJO,paramJO,  $event, xData,this,"EMRFOCA");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem2_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMRFOCA");
    }

    /**
     * 保存并关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMRFOCAEditViewBase
     */
    public SaveAndExit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (xData && xData.saveAndExit instanceof Function) {
            xData.saveAndExit().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                if(window.parent){
                    window.parent.postMessage([{ ...response.data }],'*');
                }
            });
        } else if (_this.saveAndExit && _this.saveAndExit instanceof Function) {
            _this.saveAndExit().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                if(window.parent){
                    window.parent.postMessage([{ ...response.data }],'*');
                }
            });
        }
    }
    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMRFOCAEditViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}