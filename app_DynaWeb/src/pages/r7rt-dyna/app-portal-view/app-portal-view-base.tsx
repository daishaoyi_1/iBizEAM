import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PortalViewBase } from '@/studio-core';

/**
 * 应用门户视图视图基类
 *
 * @export
 * @class AppPortalViewBase
 * @extends {PortalViewBase}
 */
export class AppPortalViewBase extends PortalViewBase {

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof AppPortalViewBase
     */
    protected model: any = {
        srfCaption: 'app.views.appportalview.caption',
        srfTitle: 'app.views.appportalview.title',
        srfSubTitle: 'app.views.appportalview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof AppPortalViewBase
     */
    protected containerModel: any = {
        view_dashboard: {
            name: 'dashboard',
            type: 'DASHBOARD',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof AppPortalViewBase
     */
	protected viewtag: string = 'c081125f98377205ff3d73a0b08c044f';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof AppPortalViewBase
     */ 
    protected viewName: string = 'AppPortalView';



    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof AppPortalViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof AppPortalViewBase
     */
    public engineInit(): void {
    }



    /** 
     * 数据看板部件刷新状态
     * 
     * @type {boolean}
     * @memberof AppPortalViewBase
     */
    public state: boolean = true;

    /** 
     * 刷新
     * 
     * @memberof AppPortalViewBase
     */
    public refresh(args: any){
        this.state = false;
        setTimeout(() => {
            this.state = true;
            this.loadModel();
        }, 0);
    }

}