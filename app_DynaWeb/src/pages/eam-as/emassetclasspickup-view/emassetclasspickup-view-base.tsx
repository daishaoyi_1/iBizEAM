import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMAssetClassService from '@/service/emasset-class/emasset-class-service';
import EMAssetClassAuthService from '@/authservice/emasset-class/emasset-class-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMAssetClassUIService from '@/uiservice/emasset-class/emasset-class-ui-service';

/**
 * 资产类别数据选择视图视图基类
 *
 * @export
 * @class EMASSETCLASSPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMASSETCLASSPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupViewBase
     */
    protected appDeName: string = 'emassetclass';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupViewBase
     */
    protected appDeKey: string = 'emassetclassid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupViewBase
     */
    protected appDeMajor: string = 'emassetclassname';

    /**
     * 实体服务对象
     *
     * @type {EMAssetClassService}
     * @memberof EMASSETCLASSPickupViewBase
     */
    protected appEntityService: EMAssetClassService = new EMAssetClassService;

    /**
     * 实体权限服务对象
     *
     * @type EMAssetClassUIService
     * @memberof EMASSETCLASSPickupViewBase
     */
    public appUIService: EMAssetClassUIService = new EMAssetClassUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMASSETCLASSPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emassetclass.views.pickupview.caption',
        srfTitle: 'entities.emassetclass.views.pickupview.title',
        srfSubTitle: 'entities.emassetclass.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMASSETCLASSPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupViewBase
     */
	protected viewtag: string = 'c379b2f47d4dd23617aa44ed9350ead9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLASSPickupViewBase
     */ 
    protected viewName: string = 'EMASSETCLASSPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMASSETCLASSPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMASSETCLASSPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMASSETCLASSPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emassetclass',
            majorPSDEField: 'emassetclassname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLASSPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}