import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMAssetClearService from '@/service/emasset-clear/emasset-clear-service';
import EMAssetClearAuthService from '@/authservice/emasset-clear/emasset-clear-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMAssetClearUIService from '@/uiservice/emasset-clear/emasset-clear-ui-service';

/**
 * 资产清盘记录视图基类
 *
 * @export
 * @class EMASSETCLEAREditView_908Base
 * @extends {EditViewBase}
 */
export class EMASSETCLEAREditView_908Base extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLEAREditView_908Base
     */
    protected appDeName: string = 'emassetclear';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLEAREditView_908Base
     */
    protected appDeKey: string = 'emassetclearid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLEAREditView_908Base
     */
    protected appDeMajor: string = 'emassetclearname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLEAREditView_908Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMAssetClearService}
     * @memberof EMASSETCLEAREditView_908Base
     */
    protected appEntityService: EMAssetClearService = new EMAssetClearService;

    /**
     * 实体权限服务对象
     *
     * @type EMAssetClearUIService
     * @memberof EMASSETCLEAREditView_908Base
     */
    public appUIService: EMAssetClearUIService = new EMAssetClearUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMASSETCLEAREditView_908Base
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMASSETCLEAREditView_908Base
     */
    protected model: any = {
        srfCaption: 'entities.emassetclear.views.editview_908.caption',
        srfTitle: 'entities.emassetclear.views.editview_908.title',
        srfSubTitle: 'entities.emassetclear.views.editview_908.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMASSETCLEAREditView_908Base
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMASSETCLEAREditView_908
     */
    public toolBarModels: any = {
        deuiaction1: { name: 'deuiaction1', caption: 'entities.emassetclear.editview_908toolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emassetclear.editview_908toolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLEAREditView_908Base
     */
	protected viewtag: string = 'be2aa30daf8afefdd2678354f72afadb';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMASSETCLEAREditView_908Base
     */ 
    protected viewName: string = 'EMASSETCLEAREditView_908';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMASSETCLEAREditView_908Base
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMASSETCLEAREditView_908Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMASSETCLEAREditView_908Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emassetclear',
            majorPSDEField: 'emassetclearname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLEAREditView_908Base
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLEAREditView_908Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLEAREditView_908Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMASSETCLEAREditView_908Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMAssetClear");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMASSETCLEAREditView_908Base
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}