import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMENService from '@/service/emen/emen-service';
import EMENAuthService from '@/authservice/emen/emen-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMENUIService from '@/uiservice/emen/emen-ui-service';

/**
 * 能源数据选择视图视图基类
 *
 * @export
 * @class EMENPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMENPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupViewBase
     */
    protected appDeName: string = 'emen';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupViewBase
     */
    protected appDeKey: string = 'emenid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupViewBase
     */
    protected appDeMajor: string = 'emenname';

    /**
     * 实体服务对象
     *
     * @type {EMENService}
     * @memberof EMENPickupViewBase
     */
    protected appEntityService: EMENService = new EMENService;

    /**
     * 实体权限服务对象
     *
     * @type EMENUIService
     * @memberof EMENPickupViewBase
     */
    public appUIService: EMENUIService = new EMENUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMENPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emen.views.pickupview.caption',
        srfTitle: 'entities.emen.views.pickupview.title',
        srfSubTitle: 'entities.emen.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMENPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupViewBase
     */
	protected viewtag: string = 'a58fef197d0fc4d1c59e4749e8df6f5d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupViewBase
     */ 
    protected viewName: string = 'EMENPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMENPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMENPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMENPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emen',
            majorPSDEField: 'emenname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}