import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMENService from '@/service/emen/emen-service';
import EMENAuthService from '@/authservice/emen/emen-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMENUIService from '@/uiservice/emen/emen-ui-service';

/**
 * 能源选择表格视图视图基类
 *
 * @export
 * @class EMENPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMENPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupGridViewBase
     */
    protected appDeName: string = 'emen';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupGridViewBase
     */
    protected appDeKey: string = 'emenid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupGridViewBase
     */
    protected appDeMajor: string = 'emenname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMENService}
     * @memberof EMENPickupGridViewBase
     */
    protected appEntityService: EMENService = new EMENService;

    /**
     * 实体权限服务对象
     *
     * @type EMENUIService
     * @memberof EMENPickupGridViewBase
     */
    public appUIService: EMENUIService = new EMENUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMENPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emen.views.pickupgridview.caption',
        srfTitle: 'entities.emen.views.pickupgridview.title',
        srfSubTitle: 'entities.emen.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMENPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupGridViewBase
     */
	protected viewtag: string = 'ac7b784ccafb738b22b9987e8d8bb114';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMENPickupGridViewBase
     */ 
    protected viewName: string = 'EMENPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMENPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMENPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMENPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emen',
            majorPSDEField: 'emenname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMENPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMENPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}