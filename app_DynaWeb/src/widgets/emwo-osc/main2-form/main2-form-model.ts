/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwo_oscid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwo_oscname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emwo_oscid',
        prop: 'emwo_oscid',
        dataType: 'GUID',
      },
      {
        name: 'emwo_oscname',
        prop: 'emwo_oscname',
        dataType: 'TEXT',
      },
      {
        name: 'wogroup',
        prop: 'wogroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'wotype',
        prop: 'wotype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'wodate',
        prop: 'wodate',
        dataType: 'DATETIME',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'priority',
        prop: 'priority',
        dataType: 'SSCODELIST',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wodesc',
        prop: 'wodesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionenddate',
        prop: 'regionenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'worklength',
        prop: 'worklength',
        dataType: 'FLOAT',
      },
      {
        name: 'wpersonid',
        prop: 'wpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'wpersonname',
        prop: 'wpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wresult',
        prop: 'wresult',
        dataType: 'TEXT',
      },
      {
        name: 'mfee',
        prop: 'mfee',
        dataType: 'FLOAT',
      },
      {
        name: 'cplanflag',
        prop: 'cplanflag',
        dataType: 'YESNO',
      },
      {
        name: 'arg1',
        prop: 'arg1',
        dataType: 'FLOAT',
      },
      {
        name: 'arg2',
        prop: 'arg2',
        dataType: 'FLOAT',
      },
      {
        name: 'arg3',
        prop: 'arg3',
        dataType: 'FLOAT',
      },
      {
        name: 'arg4',
        prop: 'arg4',
        dataType: 'FLOAT',
      },
      {
        name: 'arg5',
        prop: 'arg5',
        dataType: 'FLOAT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'PICKUP',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emwo_osc',
        prop: 'emwo_oscid',
        dataType: 'FONTKEY',
      },
    ]
  }

}