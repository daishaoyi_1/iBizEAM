/**
 * Main6 部件模型
 *
 * @export
 * @class Main6Model
 */
export default class Main6Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emwo_oscid',
          prop: 'emwo_oscid',
          dataType: 'GUID',
        },
        {
          name: 'emwo_oscname',
          prop: 'emwo_oscname',
          dataType: 'TEXT',
        },
        {
          name: 'wotype',
          prop: 'wotype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'wodate',
          prop: 'wodate',
          dataType: 'DATETIME',
        },
        {
          name: 'wresult',
          prop: 'wresult',
          dataType: 'TEXT',
        },
        {
          name: 'regionbegindate',
          prop: 'regionbegindate',
          dataType: 'DATETIME',
        },
        {
          name: 'regionenddate',
          prop: 'regionenddate',
          dataType: 'DATETIME',
        },
        {
          name: 'worklength',
          prop: 'worklength',
          dataType: 'FLOAT',
        },
        {
          name: 'eqstoplength',
          prop: 'eqstoplength',
          dataType: 'FLOAT',
        },
        {
          name: 'mfee',
          prop: 'mfee',
          dataType: 'FLOAT',
        },
        {
          name: 'rdeptname',
          prop: 'rdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'rteamname',
          prop: 'rteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'cplanflag',
          prop: 'cplanflag',
          dataType: 'YESNO',
        },
        {
          name: 'priority',
          prop: 'priority',
          dataType: 'SSCODELIST',
        },
        {
          name: 'wogroup',
          prop: 'wogroup',
          dataType: 'SSCODELIST',
        },
        {
          name: 'wopname',
          prop: 'wopname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'wooriname',
          prop: 'wooriname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'woteam',
          prop: 'woteam',
          dataType: 'TEXT',
        },
        {
          name: 'arg1',
          prop: 'arg1',
          dataType: 'FLOAT',
        },
        {
          name: 'arg2',
          prop: 'arg2',
          dataType: 'FLOAT',
        },
        {
          name: 'arg3',
          prop: 'arg3',
          dataType: 'FLOAT',
        },
        {
          name: 'arg4',
          prop: 'arg4',
          dataType: 'FLOAT',
        },
        {
          name: 'arg5',
          prop: 'arg5',
          dataType: 'FLOAT',
        },
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'wopid',
          prop: 'wopid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emwo_oscname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwo_oscid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emwo_oscid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'mpersonid',
          prop: 'mpersonid',
          dataType: 'PICKUP',
        },
        {
          name: 'wooriid',
          prop: 'wooriid',
          dataType: 'PICKUP',
        },
        {
          name: 'rempid',
          prop: 'rempid',
          dataType: 'PICKUP',
        },
        {
          name: 'dpid',
          prop: 'dpid',
          dataType: 'PICKUP',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'wpersonid',
          prop: 'wpersonid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'recvpersonid',
          prop: 'recvpersonid',
          dataType: 'PICKUP',
        },
        {
          name: 'emwo_osc',
          prop: 'emwo_oscid',
        },
      {
        name: 'n_emwo_oscname_like',
        prop: 'n_emwo_oscname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_equipname_like',
        prop: 'n_equipname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'n_objname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wogroup_eq',
        prop: 'n_wogroup_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_wooriname_like',
        prop: 'n_wooriname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wostate_eq',
        prop: 'n_wostate_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}