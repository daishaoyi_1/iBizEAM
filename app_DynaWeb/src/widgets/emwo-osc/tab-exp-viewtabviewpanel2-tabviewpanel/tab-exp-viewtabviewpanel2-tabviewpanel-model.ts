/**
 * TabExpViewtabviewpanel2 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel2Model
 */
export default class TabExpViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'activelengths',
      },
      {
        name: 'emwo_oscname',
      },
      {
        name: 'arg4',
      },
      {
        name: 'description',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'val',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'wresult',
      },
      {
        name: 'regionbegindate',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'arg3',
      },
      {
        name: 'worklength',
      },
      {
        name: 'emwo_osc',
        prop: 'emwo_oscid',
      },
      {
        name: 'mdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'regionenddate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'nval',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'mfee',
      },
      {
        name: 'expiredate',
      },
      {
        name: 'arg1',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'content',
      },
      {
        name: 'arg2',
      },
      {
        name: 'archive',
      },
      {
        name: 'wogroup',
      },
      {
        name: 'woteam',
      },
      {
        name: 'wotype',
      },
      {
        name: 'priority',
      },
      {
        name: 'enable',
      },
      {
        name: 'prefee',
      },
      {
        name: 'wostate',
      },
      {
        name: 'wodate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'cplanflag',
      },
      {
        name: 'vrate',
      },
      {
        name: 'arg5',
      },
      {
        name: 'createman',
      },
      {
        name: 'wodesc',
      },
      {
        name: 'wooritype',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'wopname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'dptype',
      },
      {
        name: 'equipcode',
      },
      {
        name: 'objname',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'wooriname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'dpname',
      },
      {
        name: 'wopid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'objid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'wooriid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'dpid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
      {
        name: 'wpersonid',
      },
      {
        name: 'wpersonname',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'recvpersonname',
      },
    ]
  }


}