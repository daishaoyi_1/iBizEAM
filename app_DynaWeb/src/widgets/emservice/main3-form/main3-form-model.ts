/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emservicename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'servicecode',
        prop: 'servicecode',
        dataType: 'TEXT',
      },
      {
        name: 'emservicename',
        prop: 'emservicename',
        dataType: 'TEXT',
      },
      {
        name: 'servicegroup',
        prop: 'servicegroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'labservicelevelid',
        prop: 'labservicelevelid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'labservicetypeid',
        prop: 'labservicetypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'sums',
        prop: 'sums',
        dataType: 'INT',
      },
      {
        name: 'servicestate',
        prop: 'servicestate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'qualitymana',
        prop: 'qualitymana',
        dataType: 'LONGTEXT',
      },
      {
        name: 'qualifications',
        prop: 'qualifications',
        dataType: 'LONGTEXT',
      },
      {
        name: 'prman',
        prop: 'prman',
        dataType: 'TEXT',
      },
      {
        name: 'tel',
        prop: 'tel',
        dataType: 'TEXT',
      },
      {
        name: 'fax',
        prop: 'fax',
        dataType: 'TEXT',
      },
      {
        name: 'zip',
        prop: 'zip',
        dataType: 'TEXT',
      },
      {
        name: 'website',
        prop: 'website',
        dataType: 'TEXT',
      },
      {
        name: 'lsareaid',
        prop: 'lsareaid',
        dataType: 'TEXT',
      },
      {
        name: 'addr',
        prop: 'addr',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'accode',
        prop: 'accode',
        dataType: 'TEXT',
      },
      {
        name: 'accodedesc',
        prop: 'accodedesc',
        dataType: 'TEXT',
      },
      {
        name: 'payway',
        prop: 'payway',
        dataType: 'SSCODELIST',
      },
      {
        name: 'paywaydesc',
        prop: 'paywaydesc',
        dataType: 'TEXT',
      },
      {
        name: 'taxcode',
        prop: 'taxcode',
        dataType: 'TEXT',
      },
      {
        name: 'taxtypeid',
        prop: 'taxtypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'taxdesc',
        prop: 'taxdesc',
        dataType: 'TEXT',
      },
      {
        name: 'shdate',
        prop: 'shdate',
        dataType: 'DATETIME',
      },
      {
        name: 'att',
        prop: 'att',
        dataType: 'TEXT',
      },
      {
        name: 'content',
        prop: 'content',
        dataType: 'LONGTEXT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emserviceid',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'emservice',
        prop: 'emserviceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}