/**
 * Contact 部件模型
 *
 * @export
 * @class ContactModel
 */
export default class ContactModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof ContactModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emservicename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'prman',
        prop: 'prman',
        dataType: 'TEXT',
      },
      {
        name: 'tel',
        prop: 'tel',
        dataType: 'TEXT',
      },
      {
        name: 'fax',
        prop: 'fax',
        dataType: 'TEXT',
      },
      {
        name: 'zip',
        prop: 'zip',
        dataType: 'TEXT',
      },
      {
        name: 'website',
        prop: 'website',
        dataType: 'TEXT',
      },
      {
        name: 'lsareaid',
        prop: 'lsareaid',
        dataType: 'TEXT',
      },
      {
        name: 'addr',
        prop: 'addr',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'emserviceid',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'emservice',
        prop: 'emserviceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}