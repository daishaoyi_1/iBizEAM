/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'servicecode',
          prop: 'servicecode',
          dataType: 'TEXT',
        },
        {
          name: 'emservicename',
          prop: 'emservicename',
          dataType: 'TEXT',
        },
        {
          name: 'servicegroup',
          prop: 'servicegroup',
          dataType: 'NMCODELIST',
        },
        {
          name: 'sums',
          prop: 'sums',
          dataType: 'INT',
        },
        {
          name: 'zzyw',
          prop: 'zzyw',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'labservicelevelid',
          prop: 'labservicelevelid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'servicestate',
          prop: 'servicestate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'prman',
          prop: 'prman',
          dataType: 'TEXT',
        },
        {
          name: 'tel',
          prop: 'tel',
          dataType: 'TEXT',
        },
        {
          name: 'fax',
          prop: 'fax',
          dataType: 'TEXT',
        },
        {
          name: 'zip',
          prop: 'zip',
          dataType: 'TEXT',
        },
        {
          name: 'addr',
          prop: 'addr',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'website',
          prop: 'website',
          dataType: 'TEXT',
        },
        {
          name: 'accode',
          prop: 'accode',
          dataType: 'TEXT',
        },
        {
          name: 'payway',
          prop: 'payway',
          dataType: 'SSCODELIST',
        },
        {
          name: 'srfmajortext',
          prop: 'emservicename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emserviceid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emserviceid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emservice',
          prop: 'emserviceid',
        },
      {
        name: 'n_emservicename_like',
        prop: 'n_emservicename_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_labservicetypeid_eq',
        prop: 'n_labservicetypeid_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_labservicelevelid_eq',
        prop: 'n_labservicelevelid_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_servicestate_eq',
        prop: 'n_servicestate_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}