import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * TreePickupViewpickupviewpanel 部件服务对象
 *
 * @export
 * @class TreePickupViewpickupviewpanelService
 */
export default class TreePickupViewpickupviewpanelService extends ControlService {
}