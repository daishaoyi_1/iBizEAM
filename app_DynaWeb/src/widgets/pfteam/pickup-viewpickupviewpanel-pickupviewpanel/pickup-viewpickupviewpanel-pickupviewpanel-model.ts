/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'teamcode',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'pfteam',
        prop: 'pfteamid',
      },
      {
        name: 'createman',
      },
      {
        name: 'teamtypeid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'teamfn',
      },
      {
        name: 'pfteamname',
      },
      {
        name: 'teaminfo',
      },
      {
        name: 'teamcapacity',
      },
    ]
  }


}