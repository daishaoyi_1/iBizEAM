import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMEQCheckService from '@/service/emeqcheck/emeqcheck-service';
import DefaultService from './default-searchform-service';
import EMEQCheckUIService from '@/uiservice/emeqcheck/emeqcheck-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQCheckService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMEQCheckService = new EMEQCheckService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emeqcheck';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '维修记录';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQCheckUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMEQCheckUIService = new EMEQCheckUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_activedesc_like: null,
        n_equipname_like: null,
        n_objname_like: null,
        n_woname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_activedesc_like: new FormItemModel({ caption: '检修记录(文本包含(%))', detailType: 'FORMITEM', name: 'n_activedesc_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_equipname_like: new FormItemModel({ caption: '设备(文本包含(%))', detailType: 'FORMITEM', name: 'n_equipname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_objname_like: new FormItemModel({ caption: '位置(文本包含(%))', detailType: 'FORMITEM', name: 'n_objname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_woname_like: new FormItemModel({ caption: '工单(文本包含(%))', detailType: 'FORMITEM', name: 'n_woname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}