import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMEQCheckService from '@/service/emeqcheck/emeqcheck-service';
import Main4Service from './main4-form-service';
import EMEQCheckUIService from '@/uiservice/emeqcheck/emeqcheck-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main4EditFormBase}
 */
export class Main4EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main4Service}
     * @memberof Main4EditFormBase
     */
    public service: Main4Service = new Main4Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQCheckService}
     * @memberof Main4EditFormBase
     */
    public appEntityService: EMEQCheckService = new EMEQCheckService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeName: string = 'emeqcheck';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeLogicName: string = '维修记录';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQCheckUIService}
     * @memberof Main4Base
     */  
    public appUIService: EMEQCheckUIService = new EMEQCheckUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        equipname: null,
        objname: null,
        activedate: null,
        woname: null,
        activedesc: null,
        activebdesc: null,
        activeadesc: null,
        regionbegindate: null,
        regionenddate: null,
        activelengths: null,
        eqstoplength: null,
        rempid: null,
        rempname: null,
        rdeptname: null,
        rteamname: null,
        rservicename: null,
        prefee: null,
        mfee: null,
        pfee: null,
        sfee: null,
        rfodename: null,
        rfomoname: null,
        rfocaname: null,
        rfoacname: null,
        emeqcheckid: null,
        emeqcheck: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public rules(): any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '检定记录信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqcheck.main4_form', extractMode: 'ITEM', details: [] } }),

        grouppanel14: new FormGroupPanelModel({ caption: '责任信息', detailType: 'GROUPPANEL', name: 'grouppanel14', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqcheck.main4_form', extractMode: 'ITEM', details: [] } }),

        grouppanel19: new FormGroupPanelModel({ caption: '财务信息', detailType: 'GROUPPANEL', name: 'grouppanel19', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqcheck.main4_form', extractMode: 'ITEM', details: [] } }),

        grouppanel24: new FormGroupPanelModel({ caption: '故障信息', detailType: 'GROUPPANEL', name: 'grouppanel24', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqcheck.main4_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '检修记录标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '检修记录名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activedate: new FormItemModel({
    caption: '检修日期', detailType: 'FORMITEM', name: 'activedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        woname: new FormItemModel({
    caption: '工单', detailType: 'FORMITEM', name: 'woname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activedesc: new FormItemModel({
    caption: '检修记录', detailType: 'FORMITEM', name: 'activedesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activebdesc: new FormItemModel({
    caption: '检修原因', detailType: 'FORMITEM', name: 'activebdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activeadesc: new FormItemModel({
    caption: '检修结果', detailType: 'FORMITEM', name: 'activeadesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionbegindate: new FormItemModel({
    caption: '起始时间', detailType: 'FORMITEM', name: 'regionbegindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionenddate: new FormItemModel({
    caption: '结束时间', detailType: 'FORMITEM', name: 'regionenddate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activelengths: new FormItemModel({
    caption: '持续时间(H)', detailType: 'FORMITEM', name: 'activelengths', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqstoplength: new FormItemModel({
    caption: '停运时间(分)', detailType: 'FORMITEM', name: 'eqstoplength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptname: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rteamname: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rservicename: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'rservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        prefee: new FormItemModel({
    caption: '预算(￥)', detailType: 'FORMITEM', name: 'prefee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mfee: new FormItemModel({
    caption: '材料费(￥)', detailType: 'FORMITEM', name: 'mfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pfee: new FormItemModel({
    caption: '人工费(￥)', detailType: 'FORMITEM', name: 'pfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sfee: new FormItemModel({
    caption: '服务费(￥)', detailType: 'FORMITEM', name: 'sfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfodename: new FormItemModel({
    caption: '现象', detailType: 'FORMITEM', name: 'rfodename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfomoname: new FormItemModel({
    caption: '模式', detailType: 'FORMITEM', name: 'rfomoname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfocaname: new FormItemModel({
    caption: '原因', detailType: 'FORMITEM', name: 'rfocaname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfoacname: new FormItemModel({
    caption: '方案', detailType: 'FORMITEM', name: 'rfoacname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeqcheckid: new FormItemModel({
    caption: '检修记录标识', detailType: 'FORMITEM', name: 'emeqcheckid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main4Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}