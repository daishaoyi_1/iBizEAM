import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import EMEQSpareMapService from '@/service/emeqspare-map/emeqspare-map-service';
import ByEQService from './by-eq-list-service';
import EMEQSpareMapUIService from '@/uiservice/emeqspare-map/emeqspare-map-ui-service';

/**
 * dashboard_sysportlet2_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {ByEQListBase}
 */
export class ByEQListBase extends ListControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByEQListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {ByEQService}
     * @memberof ByEQListBase
     */
    public service: ByEQService = new ByEQService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareMapService}
     * @memberof ByEQListBase
     */
    public appEntityService: EMEQSpareMapService = new EMEQSpareMapService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByEQListBase
     */
    protected appDeName: string = 'emeqsparemap';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByEQListBase
     */
    protected appDeLogicName: string = '备件包引用';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQSpareMapUIService}
     * @memberof ByEQBase
     */  
    public appUIService: EMEQSpareMapUIService = new EMEQSpareMapUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof ByEQListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof ByEQListBase
     */
    public minorSortDir: string = '';




}