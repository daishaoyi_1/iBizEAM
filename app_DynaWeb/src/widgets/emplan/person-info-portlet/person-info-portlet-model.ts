/**
 * PersonInfo 部件模型
 *
 * @export
 * @class PersonInfoModel
 */
export default class PersonInfoModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PersonInfoModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'description',
      },
      {
        name: 'emplan',
        prop: 'emplanid',
      },
      {
        name: 'mdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'prefee',
      },
      {
        name: 'plandesc',
      },
      {
        name: 'mtflag',
      },
      {
        name: 'rempname',
      },
      {
        name: 'plancvl',
      },
      {
        name: 'mpersonname',
      },
      {
        name: 'emplanname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'rempid',
      },
      {
        name: 'plantype',
      },
      {
        name: 'content',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'planinfo',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'archive',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'planstate',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'dpname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'dptype',
      },
      {
        name: 'plantemplname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'objname',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'objid',
      },
      {
        name: 'dpid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'plantemplid',
      },
      {
        name: 'rteamid',
      },
    ]
  }


}
