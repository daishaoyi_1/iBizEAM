/**
 * PersonInfo 部件模型
 *
 * @export
 * @class PersonInfoModel
 */
export default class PersonInfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof PersonInfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emplanid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emplanname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'emplanid',
        prop: 'emplanid',
        dataType: 'GUID',
      },
      {
        name: 'emplan',
        prop: 'emplanid',
        dataType: 'FONTKEY',
      },
    ]
  }

}