/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emplanid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emplanname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emplanid',
        prop: 'emplanid',
        dataType: 'GUID',
      },
      {
        name: 'plantemplname',
        prop: 'plantemplname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emplanname',
        prop: 'emplanname',
        dataType: 'TEXT',
      },
      {
        name: 'plantype',
        prop: 'plantype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'planstate',
        prop: 'planstate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'mdate',
        prop: 'mdate',
        dataType: 'DATETIME',
      },
      {
        name: 'prefee',
        prop: 'prefee',
        dataType: 'CURRENCY',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'mtflag',
        prop: 'mtflag',
        dataType: 'YESNO',
      },
      {
        name: 'emwotype',
        prop: 'emwotype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'plancvl',
        prop: 'plancvl',
        dataType: 'FLOAT',
      },
      {
        name: 'plandesc',
        prop: 'plandesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'dpname',
        prop: 'dpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'archive',
        prop: 'archive',
        dataType: 'SMCODELIST',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'rserviceid',
        prop: 'rserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'plantemplid',
        prop: 'plantemplid',
        dataType: 'PICKUP',
      },
      {
        name: 'dpid',
        prop: 'dpid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'rteamid',
        prop: 'rteamid',
        dataType: 'PICKUP',
      },
      {
        name: 'emplan',
        prop: 'emplanid',
        dataType: 'FONTKEY',
      },
    ]
  }

}