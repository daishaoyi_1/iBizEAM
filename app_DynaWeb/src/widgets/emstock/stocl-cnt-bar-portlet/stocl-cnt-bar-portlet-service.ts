import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * StoclCntBar 部件服务对象
 *
 * @export
 * @class StoclCntBarService
 */
export default class StoclCntBarService extends ControlService {
}
