/**
 * Main5 部件模型
 *
 * @export
 * @class Main5Model
 */
export default class Main5Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main5Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emequipid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emequipname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'eqisservice',
        prop: 'eqisservice',
        dataType: 'YESNO',
      },
      {
        name: 'eqstate',
        prop: 'eqstate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'eqstartdate',
        prop: 'eqstartdate',
        dataType: 'DATE',
      },
      {
        name: 'eqsumstoptime',
        prop: 'eqsumstoptime',
        dataType: 'FLOAT',
      },
      {
        name: 'haltstate',
        prop: 'haltstate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'haltcause',
        prop: 'haltcause',
        dataType: 'TEXT',
      },
      {
        name: 'emequipid',
        prop: 'emequipid',
        dataType: 'GUID',
      },
      {
        name: 'emequip',
        prop: 'emequipid',
        dataType: 'FONTKEY',
      },
    ]
  }

}