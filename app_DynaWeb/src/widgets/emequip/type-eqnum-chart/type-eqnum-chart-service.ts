import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMEquipService from '@/service/emequip/emequip-service';
import TypeEQNumModel from './type-eqnum-chart-model';


/**
 * TypeEQNum 部件服务对象
 *
 * @export
 * @class TypeEQNumService
 */
export default class TypeEQNumService extends ControlService {

    /**
     * 设备档案服务对象
     *
     * @type {EMEquipService}
     * @memberof TypeEQNumService
     */
    public appEntityService: EMEquipService = new EMEquipService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof TypeEQNumService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of TypeEQNumService.
     * 
     * @param {*} [opts={}]
     * @memberof TypeEQNumService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new TypeEQNumModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof TypeEQNumService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}