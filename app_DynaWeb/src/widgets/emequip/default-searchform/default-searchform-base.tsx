import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMEquipService from '@/service/emequip/emequip-service';
import DefaultService from './default-searchform-service';
import EMEquipUIService from '@/uiservice/emequip/emequip-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEquipService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMEquipService = new EMEquipService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emequip';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '设备档案';

    /**
     * 界面UI服务对象
     *
     * @type {EMEquipUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMEquipUIService = new EMEquipUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_equipcode_like: null,
        n_emequipname_like: null,
        n_eqlocationname_eq: null,
        n_equipgroup_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        grouppanel1: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: false, form: this, uiActionGroup: { caption: '', langbase: 'entities.emequip.default_searchform', extractMode: 'ITEM', details: [] } })
, 
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_equipcode_like: new FormItemModel({ caption: '设备代码（%）', detailType: 'FORMITEM', name: 'n_equipcode_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_emequipname_like: new FormItemModel({ caption: '设备名称（%）', detailType: 'FORMITEM', name: 'n_emequipname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_eqlocationname_eq: new FormItemModel({ caption: '位置（=）', detailType: 'FORMITEM', name: 'n_eqlocationname_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_equipgroup_eq: new FormItemModel({ caption: '设备分组（=）', detailType: 'FORMITEM', name: 'n_equipgroup_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}