/**
 * Main4 部件模型
 *
 * @export
 * @class Main4Model
 */
export default class Main4Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main4Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqmaintanceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqmaintancename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'activedate',
        prop: 'activedate',
        dataType: 'DATE',
      },
      {
        name: 'woname',
        prop: 'woname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'activedesc',
        prop: 'activedesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'activebdesc',
        prop: 'activebdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'activeadesc',
        prop: 'activeadesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionenddate',
        prop: 'regionenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'prefee',
        prop: 'prefee',
        dataType: 'FLOAT',
      },
      {
        name: 'mfee',
        prop: 'mfee',
        dataType: 'FLOAT',
      },
      {
        name: 'pfee',
        prop: 'pfee',
        dataType: 'FLOAT',
      },
      {
        name: 'sfee',
        prop: 'sfee',
        dataType: 'FLOAT',
      },
      {
        name: 'rfodename',
        prop: 'rfodename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfomoname',
        prop: 'rfomoname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfocaname',
        prop: 'rfocaname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfoacname',
        prop: 'rfoacname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emeqmaintanceid',
        prop: 'emeqmaintanceid',
        dataType: 'GUID',
      },
      {
        name: 'emeqmaintance',
        prop: 'emeqmaintanceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}