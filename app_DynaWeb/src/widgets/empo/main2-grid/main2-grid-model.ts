/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'empoid',
          prop: 'empoid',
          dataType: 'GUID',
        },
        {
          name: 'pdate',
          prop: 'pdate',
          dataType: 'DATETIME',
        },
        {
          name: 'labservicename',
          prop: 'labservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'eadate',
          prop: 'eadate',
          dataType: 'DATETIME',
        },
        {
          name: 'postate',
          prop: 'postate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'wfstep',
          prop: 'wfstep',
          dataType: 'SSCODELIST',
        },
        {
          name: 'tsfee',
          prop: 'tsfee',
          dataType: 'FLOAT',
        },
        {
          name: 'taxfee',
          prop: 'taxfee',
          dataType: 'FLOAT',
        },
        {
          name: 'poamount',
          prop: 'poamount',
          dataType: 'FLOAT',
        },
        {
          name: 'payway',
          prop: 'payway',
          dataType: 'SSCODELIST',
        },
        {
          name: 'civo',
          prop: 'civo',
          dataType: 'TEXT',
        },
        {
          name: 'tsivo',
          prop: 'tsivo',
          dataType: 'TEXT',
        },
        {
          name: 'taxivo',
          prop: 'taxivo',
          dataType: 'TEXT',
        },
        {
          name: 'apprempname',
          prop: 'apprempname',
          dataType: 'TEXT',
        },
        {
          name: 'apprdate',
          prop: 'apprdate',
          dataType: 'DATETIME',
        },
        {
          name: 'labservicedesc',
          prop: 'labservicedesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emponame',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'empoid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'empoid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'empo',
          prop: 'empoid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}