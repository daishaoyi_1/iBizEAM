/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'content',
      },
      {
        name: 'poamount',
      },
      {
        name: 'eadate',
      },
      {
        name: 'empo',
        prop: 'empoid',
      },
      {
        name: 'postate',
      },
      {
        name: 'fgempname',
      },
      {
        name: 'pdate',
      },
      {
        name: 'civo',
      },
      {
        name: 'labservicedesc',
      },
      {
        name: 'description',
      },
      {
        name: 'emponame',
      },
      {
        name: 'createdate',
      },
      {
        name: 'tsfee',
      },
      {
        name: 'orgid',
      },
      {
        name: 'rempid',
      },
      {
        name: 'taxfee',
      },
      {
        name: 'htjy',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'apprdate',
      },
      {
        name: 'apprempid',
      },
      {
        name: 'taxivo',
      },
      {
        name: 'att',
      },
      {
        name: 'zjlempid',
      },
      {
        name: 'createman',
      },
      {
        name: 'fgempid',
      },
      {
        name: 'maxprice',
      },
      {
        name: 'apprempname',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'enable',
      },
      {
        name: 'rempname',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'payway',
      },
      {
        name: 'poinfo',
      },
      {
        name: 'tsivo',
      },
      {
        name: 'zjlempname',
      },
      {
        name: 'labservicetypeid',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'labserviceid',
      },
    ]
  }


}