import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMEQWLService from '@/service/emeqwl/emeqwl-service';
import Nearest30DayByEQModel from './nearest30-day-by-eq-chart-model';


/**
 * Nearest30DayByEQ 部件服务对象
 *
 * @export
 * @class Nearest30DayByEQService
 */
export default class Nearest30DayByEQService extends ControlService {

    /**
     * 设备运行日志服务对象
     *
     * @type {EMEQWLService}
     * @memberof Nearest30DayByEQService
     */
    public appEntityService: EMEQWLService = new EMEQWLService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof Nearest30DayByEQService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of Nearest30DayByEQService.
     * 
     * @param {*} [opts={}]
     * @memberof Nearest30DayByEQService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new Nearest30DayByEQModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Nearest30DayByEQService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}