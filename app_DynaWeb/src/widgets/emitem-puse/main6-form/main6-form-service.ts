import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMItemPUseService from '@/service/emitem-puse/emitem-puse-service';
import Main6Model from './main6-form-model';
import EMItemService from '@/service/emitem/emitem-service';
import EMServiceService from '@/service/emservice/emservice-service';
import EMPurPlanService from '@/service/empur-plan/empur-plan-service';
import PFEmpService from '@/service/pfemp/pfemp-service';
import EMWOService from '@/service/emwo/emwo-service';
import EMEquipService from '@/service/emequip/emequip-service';
import EMObjectService from '@/service/emobject/emobject-service';
import PFDeptService from '@/service/pfdept/pfdept-service';
import PFTeamService from '@/service/pfteam/pfteam-service';
import EMStoreService from '@/service/emstore/emstore-service';
import EMStorePartService from '@/service/emstore-part/emstore-part-service';


/**
 * Main6 部件服务对象
 *
 * @export
 * @class Main6Service
 */
export default class Main6Service extends ControlService {

    /**
     * 领料单服务对象
     *
     * @type {EMItemPUseService}
     * @memberof Main6Service
     */
    public appEntityService: EMItemPUseService = new EMItemPUseService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof Main6Service
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of Main6Service.
     * 
     * @param {*} [opts={}]
     * @memberof Main6Service
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new Main6Model();
    }

    /**
     * 物品服务对象
     *
     * @type {EMItemService}
     * @memberof Main6Service
     */
    public emitemService: EMItemService = new EMItemService();

    /**
     * 服务商服务对象
     *
     * @type {EMServiceService}
     * @memberof Main6Service
     */
    public emserviceService: EMServiceService = new EMServiceService();

    /**
     * 计划修理服务对象
     *
     * @type {EMPurPlanService}
     * @memberof Main6Service
     */
    public empurplanService: EMPurPlanService = new EMPurPlanService();

    /**
     * 职员服务对象
     *
     * @type {PFEmpService}
     * @memberof Main6Service
     */
    public pfempService: PFEmpService = new PFEmpService();

    /**
     * 工单服务对象
     *
     * @type {EMWOService}
     * @memberof Main6Service
     */
    public emwoService: EMWOService = new EMWOService();

    /**
     * 设备档案服务对象
     *
     * @type {EMEquipService}
     * @memberof Main6Service
     */
    public emequipService: EMEquipService = new EMEquipService();

    /**
     * 对象服务对象
     *
     * @type {EMObjectService}
     * @memberof Main6Service
     */
    public emobjectService: EMObjectService = new EMObjectService();

    /**
     * 部门服务对象
     *
     * @type {PFDeptService}
     * @memberof Main6Service
     */
    public pfdeptService: PFDeptService = new PFDeptService();

    /**
     * 班组服务对象
     *
     * @type {PFTeamService}
     * @memberof Main6Service
     */
    public pfteamService: PFTeamService = new PFTeamService();

    /**
     * 仓库服务对象
     *
     * @type {EMStoreService}
     * @memberof Main6Service
     */
    public emstoreService: EMStoreService = new EMStoreService();

    /**
     * 仓库库位服务对象
     *
     * @type {EMStorePartService}
     * @memberof Main6Service
     */
    public emstorepartService: EMStorePartService = new EMStorePartService();

    /**
     * 远端数据
     *
     * @type {*}
     * @memberof Main6Service
     */
    private remoteCopyData:any = {};

    /**
     * 处理数据
     *
     * @private
     * @param {Promise<any>} promise
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    private doItems(promise: Promise<any>, deKeyField: string, deName: string): Promise<any> {
        return new Promise((resolve, reject) => {
            promise.then((response: any) => {
                if (response && response.status === 200) {
                    const data = response.data;
                    data.forEach((item:any,index:number) =>{
                        item[deName] = item[deKeyField];
                        data[index] = item;
                    });
                    resolve(data);
                } else {
                    reject([])
                }
            }).catch((response: any) => {
                reject([])
            });
        });
    }

    /**
     * 获取跨实体数据集合
     *
     * @param {string} serviceName 服务名称
     * @param {string} interfaceName 接口名称
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any[]>}
     * @memberof  Main6Service
     */
    @Errorlog
    public getItems(serviceName: string, interfaceName: string, context: any = {}, data: any, isloading?: boolean): Promise<any[]> {
        data.page = data.page ? data.page : 0;
        data.size = data.size ? data.size : 1000;
        if (Object.is(serviceName, 'EMItemService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.emitemService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'emitemid', 'emitem');
        }
        if (Object.is(serviceName, 'EMServiceService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.emserviceService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'emserviceid', 'emservice');
        }
        if (Object.is(serviceName, 'EMPurPlanService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.empurplanService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'empurplanid', 'empurplan');
        }
        if (Object.is(serviceName, 'PFEmpService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.pfempService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'pfempid', 'pfemp');
        }
        if (Object.is(serviceName, 'EMWOService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.emwoService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'emwoid', 'emwo');
        }
        if (Object.is(serviceName, 'EMEquipService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.emequipService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'emequipid', 'emequip');
        }
        if (Object.is(serviceName, 'EMObjectService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.emobjectService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'emobjectid', 'emobject');
        }
        if (Object.is(serviceName, 'PFDeptService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.pfdeptService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'pfdeptid', 'pfdept');
        }
        if (Object.is(serviceName, 'PFTeamService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.pfteamService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'pfteamid', 'pfteam');
        }
        if (Object.is(serviceName, 'EMStoreService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.emstoreService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'emstoreid', 'emstore');
        }
        if (Object.is(serviceName, 'EMStorePartService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.emstorepartService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'emstorepartid', 'emstorepart');
        }

        return Promise.reject([])
    }

    /**
     * 启动工作流
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public wfstart(action: string,context: any = {},data: any = {}, isloading?: boolean,localdata?:any): Promise<any> {
        data = this.handleWFData(data);
        context = this.handleRequestData(action,context,data).context;
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](context,data, isloading,localdata);
            } else {
                result = this.appEntityService.WFStart(context,data, isloading,localdata);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 提交工作流
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public wfsubmit(action: string,context: any = {}, data: any = {}, isloading?: boolean,localdata?:any): Promise<any> {
        data = this.handleWFData(data,true);
        context = this.handleRequestData(action,context,data,true).context;
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](context,data, isloading,localdata);
            } else {
                result = this.appEntityService.WFSubmit(context,data, isloading,localdata);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 添加数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public add(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        Object.assign(Data,{emitempuseid: data.emitempuseid, srffrontuf: '1'});
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Create(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public delete(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Remove(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public update(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Update(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public get(action: string,context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Get(Context,Data, isloading);
            }
            result.then((response) => {
                this.setRemoteCopyData(response);
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 加载草稿
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public loadDraft(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        //仿真主键数据
        const PrimaryKey = Util.createUUID();
        Data.emitempuseid = PrimaryKey;
        Data.emitempuse = PrimaryKey;
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.GetDraft(Context,Data, isloading);
            }
            result.then((response) => {
                this.setRemoteCopyData(response);
                response.data.emitempuseid = PrimaryKey;
                this.handleResponse(action, response, true);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

     /**
     * 前台逻辑
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Main6Service
     */
    @Errorlog
    public frontLogic(action:string,context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any)=>{
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                return Promise.reject({ status: 500, data: { title: '失败', message: '系统异常' } });
            }
            result.then((response) => {
                this.handleResponse(action, response,true);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        })
    }

    /**
     * 处理请求数据
     * 
     * @param action 行为 
     * @param data 数据
     * @memberof Main6Service
     */
    public handleRequestData(action: string,context:any, data: any = {},isMerge:boolean = false){
        let mode: any = this.getMode();
        if (!mode && mode.getDataItems instanceof Function) {
            return data;
        }
        let formItemItems: any[] = mode.getDataItems();
        let requestData:any = {};
        if(isMerge && (data && data.viewparams)){
            Object.assign(requestData,data.viewparams);
        }
        formItemItems.forEach((item:any) =>{
            if(item && item.dataType && Object.is(item.dataType,'FONTKEY')){
                if(item && item.prop){
                    requestData[item.prop] = context[item.name];
                }
            }else{
                if(item && item.prop){
                    requestData[item.prop] = data[item.name];
                }
            }
        });
        let tempContext:any = JSON.parse(JSON.stringify(context));
        if(tempContext && tempContext.srfsessionid){
            tempContext.srfsessionkey = tempContext.srfsessionid;
            delete tempContext.srfsessionid;
        }
        return {context:tempContext,data:requestData};
    }

    /**
     * 通过属性名称获取表单项名称
     * 
     * @param name 实体属性名称 
     * @memberof Main6Service
     */
    public getItemNameByDeName(name:string) :string{
        let itemName = name;
        let mode: any = this.getMode();
        if (!mode && mode.getDataItems instanceof Function) {
            return name;
        }
        let formItemItems: any[] = mode.getDataItems();
        formItemItems.forEach((item:any)=>{
            if(item.prop === name){
                itemName = item.name;
            }
        });
        return itemName.trim();
    }

    /**
     * 设置远端数据
     * 
     * @param result 远端请求结果 
     * @memberof Main6Service
     */
    public setRemoteCopyData(result:any){
        if (result && result.status === 200) {
            this.remoteCopyData = Util.deepCopy(result.data);
        }
    }

    /**
     * 获取远端数据
     * 
     * @memberof Main6Service
     */
    public getRemoteCopyData(){
        return this.remoteCopyData;
    }

}