/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updateman',
      },
      {
        name: 'emeqspare',
        prop: 'emeqspareid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'enable',
      },
      {
        name: 'emeqsparename',
      },
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'eqsparecode',
      },
      {
        name: 'eqspareinfo',
      },
      {
        name: 'description',
      },
      {
        name: 'updatedate',
      },
    ]
  }


}