import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * TreeExpView2treeexpbar 部件服务对象
 *
 * @export
 * @class TreeExpView2treeexpbarService
 */
export default class TreeExpView2treeexpbarService extends ControlService {
}