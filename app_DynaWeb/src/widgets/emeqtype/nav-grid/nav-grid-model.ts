/**
 * Nav 部件模型
 *
 * @export
 * @class NavModel
 */
export default class NavModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof NavGridexpbar_gridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof NavGridexpbar_gridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'eqtypecode',
          prop: 'eqtypecode',
          dataType: 'TEXT',
        },
        {
          name: 'emeqtypename',
          prop: 'emeqtypename',
          dataType: 'TEXT',
        },
        {
          name: 'eqtypepname',
          prop: 'eqtypepname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqtypepid',
          prop: 'eqtypepid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqtypename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqtypeid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emeqtypeid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emeqtype',
          prop: 'emeqtypeid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}