import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMEQAHService from '@/service/emeqah/emeqah-service';
import CalendarExpViewcalendarexpbarModel from './calendar-exp-viewcalendarexpbar-calendarexpbar-model';


/**
 * CalendarExpViewcalendarexpbar 部件服务对象
 *
 * @export
 * @class CalendarExpViewcalendarexpbarService
 */
export default class CalendarExpViewcalendarexpbarService extends ControlService {

    /**
     * 活动历史服务对象
     *
     * @type {EMEQAHService}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public appEntityService: EMEQAHService = new EMEQAHService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of CalendarExpViewcalendarexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof CalendarExpViewcalendarexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new CalendarExpViewcalendarexpbarModel();
    }

}