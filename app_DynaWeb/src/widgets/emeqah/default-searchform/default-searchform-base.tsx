import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMEQAHService from '@/service/emeqah/emeqah-service';
import DefaultService from './default-searchform-service';
import EMEQAHUIService from '@/uiservice/emeqah/emeqah-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQAHService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMEQAHService = new EMEQAHService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emeqah';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '活动历史';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQAHUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMEQAHUIService = new EMEQAHUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_rfocaname_like: null,
        n_emeqahname_like: null,
        n_rfodename_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_rfocaname_like: new FormItemModel({ caption: '原因(文本包含(%))', detailType: 'FORMITEM', name: 'n_rfocaname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_emeqahname_like: new FormItemModel({ caption: '活动历史名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_emeqahname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_rfodename_like: new FormItemModel({ caption: '现象(文本包含(%))', detailType: 'FORMITEM', name: 'n_rfodename_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}