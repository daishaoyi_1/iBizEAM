/**
 * ByEQ 部件模型
 *
 * @export
 * @class ByEQModel
 */
export default class ByEQModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ByEQModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'drwgname',
        prop: 'drwgname'
      }
    ]
  }
}