import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMWPListCostService from '@/service/emwplist-cost/emwplist-cost-service';
import Main4Service from './main4-form-service';
import EMWPListCostUIService from '@/uiservice/emwplist-cost/emwplist-cost-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main4EditFormBase}
 */
export class Main4EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main4Service}
     * @memberof Main4EditFormBase
     */
    public service: Main4Service = new Main4Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWPListCostService}
     * @memberof Main4EditFormBase
     */
    public appEntityService: EMWPListCostService = new EMWPListCostService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeName: string = 'emwplistcost';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeLogicName: string = '询价单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWPListCostUIService}
     * @memberof Main4Base
     */  
    public appUIService: EMWPListCostUIService = new EMWPListCostUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        adate: null,
        labservicename: null,
        itemname: null,
        itemdesc: null,
        listprice: null,
        discnt: null,
        price: null,
        taxrate: null,
        pricecdt: null,
        unitname: null,
        unitdesc: null,
        unitrate: null,
        rempid: null,
        rempname: null,
        begindate: null,
        enddate: null,
        intunitflag: null,
        sunitprice: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        unitid: null,
        labserviceid: null,
        itemid: null,
        emwplistcostid: null,
        emwplistcost: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public rules(): any{
        return {
            adate: [
                {
                    required: this.detailsModel.adate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.adate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.adate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.adate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            discnt: [
                {
                    required: this.detailsModel.discnt.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.discnt')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.discnt.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.discnt')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            unitrate: [
                {
                    required: this.detailsModel.unitrate.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.unitrate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.unitrate.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.unitrate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            begindate: [
                {
                    required: this.detailsModel.begindate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.begindate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.begindate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.begindate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            enddate: [
                {
                    required: this.detailsModel.enddate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.enddate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.enddate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.enddate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            intunitflag: [
                {
                    required: this.detailsModel.intunitflag.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.intunitflag')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.intunitflag.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplistcost.main4_form.details.intunitflag')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '询价单信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwplistcost.main4_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel21: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel21', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwplistcost.main4_form', extractMode: 'ITEM', details: [] } }),

        formpage20: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage20', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '询价单标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '询价单名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        adate: new FormItemModel({
    caption: '询价时间', detailType: 'FORMITEM', name: 'adate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        labservicename: new FormItemModel({
    caption: '产品供应商', detailType: 'FORMITEM', name: 'labservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemname: new FormItemModel({
    caption: '物品', detailType: 'FORMITEM', name: 'itemname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemdesc: new FormItemModel({
    caption: '物品备注', detailType: 'FORMITEM', name: 'itemdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        listprice: new FormItemModel({
    caption: '标价', detailType: 'FORMITEM', name: 'listprice', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        discnt: new FormItemModel({
    caption: '折扣(%)', detailType: 'FORMITEM', name: 'discnt', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        price: new FormItemModel({
    caption: '单价', detailType: 'FORMITEM', name: 'price', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        taxrate: new FormItemModel({
    caption: '税率', detailType: 'FORMITEM', name: 'taxrate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        pricecdt: new FormItemModel({
    caption: '价格条件', detailType: 'FORMITEM', name: 'pricecdt', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        unitname: new FormItemModel({
    caption: '询价单位', detailType: 'FORMITEM', name: 'unitname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        unitdesc: new FormItemModel({
    caption: '询价单位备注', detailType: 'FORMITEM', name: 'unitdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        unitrate: new FormItemModel({
    caption: '单位转换率', detailType: 'FORMITEM', name: 'unitrate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '询价人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '询价人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        begindate: new FormItemModel({
    caption: '有效期起始', detailType: 'FORMITEM', name: 'begindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        enddate: new FormItemModel({
    caption: '有效期截至', detailType: 'FORMITEM', name: 'enddate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        intunitflag: new FormItemModel({
    caption: '整单位购买', detailType: 'FORMITEM', name: 'intunitflag', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        sunitprice: new FormItemModel({
    caption: '标准单位单价', detailType: 'FORMITEM', name: 'sunitprice', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        unitid: new FormItemModel({
    caption: '询价单位', detailType: 'FORMITEM', name: 'unitid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        labserviceid: new FormItemModel({
    caption: '产品供应商', detailType: 'FORMITEM', name: 'labserviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemid: new FormItemModel({
    caption: '物品', detailType: 'FORMITEM', name: 'itemid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emwplistcostid: new FormItemModel({
    caption: '询价单标识', detailType: 'FORMITEM', name: 'emwplistcostid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage20',
                    index: 1,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 新建默认值
     * @memberof Main4EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('rempid')) {
            this.data['rempid'] = this.context['srfuserid'];
        }
        if (this.data.hasOwnProperty('rempname')) {
            this.data['rempname'] = this.context['srfuserid'];
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main4Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}