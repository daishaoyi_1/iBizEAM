/**
 * Main4 部件模型
 *
 * @export
 * @class Main4Model
 */
export default class Main4Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main4Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwplistcostid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwplistcostname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'adate',
        prop: 'adate',
        dataType: 'DATETIME',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemdesc',
        prop: 'itemdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'listprice',
        prop: 'listprice',
        dataType: 'FLOAT',
      },
      {
        name: 'discnt',
        prop: 'discnt',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'taxrate',
        prop: 'taxrate',
        dataType: 'FLOAT',
      },
      {
        name: 'pricecdt',
        prop: 'pricecdt',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'unitname',
        prop: 'unitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'unitdesc',
        prop: 'unitdesc',
        dataType: 'TEXT',
      },
      {
        name: 'unitrate',
        prop: 'unitrate',
        dataType: 'FLOAT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'begindate',
        prop: 'begindate',
        dataType: 'DATE',
      },
      {
        name: 'enddate',
        prop: 'enddate',
        dataType: 'DATE',
      },
      {
        name: 'intunitflag',
        prop: 'intunitflag',
        dataType: 'YESNO',
      },
      {
        name: 'sunitprice',
        prop: 'sunitprice',
        dataType: 'FLOAT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'unitid',
        prop: 'unitid',
        dataType: 'PICKUP',
      },
      {
        name: 'labserviceid',
        prop: 'labserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'emwplistcostid',
        prop: 'emwplistcostid',
        dataType: 'GUID',
      },
      {
        name: 'emwplistcost',
        prop: 'emwplistcostid',
        dataType: 'FONTKEY',
      },
    ]
  }

}