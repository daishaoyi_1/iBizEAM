/**
 * TabExpViewtabviewpanel 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanelModel
 */
export default class TabExpViewtabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'entrustlist',
      },
      {
        name: 'applydesc',
      },
      {
        name: 'applystate',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'updateman',
      },
      {
        name: 'dpdesc',
      },
      {
        name: 'mfee',
      },
      {
        name: 'applyinfo',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'shuifei',
      },
      {
        name: 'sfee',
      },
      {
        name: 'emapplyname',
      },
      {
        name: 'applytype',
      },
      {
        name: 'applyedate',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'description',
      },
      {
        name: 'pfee',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'prefee1',
      },
      {
        name: 'createman',
      },
      {
        name: 'rempid',
      },
      {
        name: 'fp',
      },
      {
        name: 'zfy',
      },
      {
        name: 'rempname',
      },
      {
        name: 'applybdate',
      },
      {
        name: 'prefee',
      },
      {
        name: 'closedate',
      },
      {
        name: 'emapply',
        prop: 'emapplyid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'plantype',
      },
      {
        name: 'closeempid',
      },
      {
        name: 'applydate',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'mpersonname',
      },
      {
        name: 'closeempname',
      },
      {
        name: 'orgid',
      },
      {
        name: 'invoiceattach',
      },
      {
        name: 'spyj',
      },
      {
        name: 'enable',
      },
      {
        name: 'priority',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'equipname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'objname',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'equipid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'objid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'rfodeid',
      },
    ]
  }


}