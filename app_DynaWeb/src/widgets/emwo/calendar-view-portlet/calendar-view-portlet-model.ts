/**
 * CalendarView 部件模型
 *
 * @export
 * @class CalendarViewModel
 */
export default class CalendarViewModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof CalendarViewModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'wostate',
      },
      {
        name: 'vrate',
      },
      {
        name: 'mdate',
      },
      {
        name: 'wotype',
      },
      {
        name: 'cplanflag',
      },
      {
        name: 'waitbuy',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'description',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'prefee',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'waitmodi',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emwo',
        prop: 'emwoid',
      },
      {
        name: 'mfee',
      },
      {
        name: 'regionenddate',
      },
      {
        name: 'wogroup',
      },
      {
        name: 'worklength',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'enable',
      },
      {
        name: 'content',
      },
      {
        name: 'woteam',
      },
      {
        name: 'createman',
      },
      {
        name: 'expiredate',
      },
      {
        name: 'yxcb',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'priority',
      },
      {
        name: 'woinfo',
      },
      {
        name: 'nval',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emwoname',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'wodate',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'wresult',
      },
      {
        name: 'wpersonid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'wpersonname',
      },
      {
        name: 'orgid',
      },
      {
        name: 'wodesc',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'rempid',
      },
      {
        name: 'val',
      },
      {
        name: 'archive',
      },
      {
        name: 'regionbegindate',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'wooriname',
      },
      {
        name: 'equipcode',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'wopname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'stype',
      },
      {
        name: 'dptype',
      },
      {
        name: 'dpname',
      },
      {
        name: 'wooritype',
      },
      {
        name: 'sname',
      },
      {
        name: 'objname',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'equipid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'wopid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'wooriid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'dpid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'objid',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
    ]
  }


}
