import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * TabExpView 部件服务对象
 *
 * @export
 * @class TabExpViewService
 */
export default class TabExpViewService extends ControlService {
}
