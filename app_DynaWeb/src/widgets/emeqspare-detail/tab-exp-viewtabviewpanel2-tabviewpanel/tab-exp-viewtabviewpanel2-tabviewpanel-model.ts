/**
 * TabExpViewtabviewpanel2 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel2Model
 */
export default class TabExpViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'emeqsparedetail',
        prop: 'emeqsparedetailid',
      },
      {
        name: 'emeqsparedetailname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'itemname',
      },
      {
        name: 'eqsparename',
      },
      {
        name: 'itemid',
      },
      {
        name: 'eqspareid',
      },
    ]
  }


}