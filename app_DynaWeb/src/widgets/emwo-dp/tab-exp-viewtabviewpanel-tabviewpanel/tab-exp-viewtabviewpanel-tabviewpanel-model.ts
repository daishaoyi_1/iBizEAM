/**
 * TabExpViewtabviewpanel 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanelModel
 */
export default class TabExpViewtabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'eqstoplength',
      },
      {
        name: 'wostate',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'val',
      },
      {
        name: 'wpersonid',
      },
      {
        name: 'emwo_dp',
        prop: 'emwo_dpid',
      },
      {
        name: 'priority',
      },
      {
        name: 'archive',
      },
      {
        name: 'nval',
      },
      {
        name: 'wodate',
      },
      {
        name: 'description',
      },
      {
        name: 'regionbegindate',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'wogroup',
      },
      {
        name: 'woteam',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'wresult',
      },
      {
        name: 'expiredate',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'content',
      },
      {
        name: 'wpersonname',
      },
      {
        name: 'worklength',
      },
      {
        name: 'enable',
      },
      {
        name: 'wodesc',
      },
      {
        name: 'updateman',
      },
      {
        name: 'vrate',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'rempid',
      },
      {
        name: 'mdate',
      },
      {
        name: 'emwo_dpname',
      },
      {
        name: 'createman',
      },
      {
        name: 'prefee',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'wotype',
      },
      {
        name: 'orgid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'regionenddate',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'dpname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'wopname',
      },
      {
        name: 'objname',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'dptype',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'wooritype',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'wooriname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'equipname',
      },
      {
        name: 'dpid',
      },
      {
        name: 'objid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'wopid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'wooriid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
    ]
  }


}