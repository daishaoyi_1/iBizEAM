/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emassetclassid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emassetclassname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'assetclasscode',
        prop: 'assetclasscode',
        dataType: 'TEXT',
      },
      {
        name: 'emassetclassname',
        prop: 'emassetclassname',
        dataType: 'TEXT',
      },
      {
        name: 'assetclasspname',
        prop: 'assetclasspname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'assetclassgroup',
        prop: 'assetclassgroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'life',
        prop: 'life',
        dataType: 'FLOAT',
      },
      {
        name: 'emassetclassid',
        prop: 'emassetclassid',
        dataType: 'GUID',
      },
      {
        name: 'assetclasspid',
        prop: 'assetclasspid',
        dataType: 'PICKUP',
      },
      {
        name: 'emassetclass',
        prop: 'emassetclassid',
        dataType: 'FONTKEY',
      },
    ]
  }

}