/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'description',
      },
      {
        name: 'enable',
      },
      {
        name: 'emrfoca',
        prop: 'emrfocaid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'rfocacode',
      },
      {
        name: 'emrfocaname',
      },
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'rfocainfo',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'objid',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'rfomoid',
      },
    ]
  }


}