import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ByPlan 部件服务对象
 *
 * @export
 * @class ByPlanService
 */
export default class ByPlanService extends ControlService {
}