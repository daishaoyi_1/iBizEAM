/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emitemrinid',
          prop: 'emitemrinid',
          dataType: 'GUID',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'psum',
          prop: 'psum',
          dataType: 'FLOAT',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'FLOAT',
        },
        {
          name: 'shf',
          prop: 'shf',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'FLOAT',
        },
        {
          name: 'batcode',
          prop: 'batcode',
          dataType: 'TEXT',
        },
        {
          name: 'sdate',
          prop: 'sdate',
          dataType: 'DATETIME',
        },
        {
          name: 'storename',
          prop: 'storename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'storepartname',
          prop: 'storepartname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'podetailname',
          prop: 'podetailname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'sempname',
          prop: 'sempname',
          dataType: 'TEXT',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'rinstate',
          prop: 'rinstate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'porempname',
          prop: 'porempname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'avgtsfee',
          prop: 'avgtsfee',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'civo',
          prop: 'civo',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'labservicename',
          prop: 'labservicename',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'sumall',
          prop: 'sumall',
          dataType: 'FLOAT',
        },
        {
          name: 'wfstate',
          prop: 'wfstate',
          dataType: 'WFSTATE',
        },
        {
          name: 'wfstep',
          prop: 'wfstep',
          dataType: 'SSCODELIST',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emitemrinname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emitemrinid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emitemrinid',
          dataType: 'GUID',
        },
        {
          name: 'podetailid',
          prop: 'podetailid',
          dataType: 'PICKUP',
        },
        {
          name: 'storepartid',
          prop: 'storepartid',
          dataType: 'PICKUP',
        },
        {
          name: 'storeid',
          prop: 'storeid',
          dataType: 'PICKUP',
        },
        {
          name: 'emserviceid',
          prop: 'emserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'emitemrin',
          prop: 'emitemrinid',
        },
      {
        name: 'n_itemname_like',
        prop: 'n_itemname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_podetailid_eq',
        prop: 'n_podetailid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_storeid_eq',
        prop: 'n_storeid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_storepartid_eq',
        prop: 'n_storepartid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_rinstate_eq',
        prop: 'n_rinstate_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}