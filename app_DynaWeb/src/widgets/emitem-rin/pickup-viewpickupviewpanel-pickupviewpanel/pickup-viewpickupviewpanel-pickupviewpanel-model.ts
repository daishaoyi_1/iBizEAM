/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emitemrinname',
      },
      {
        name: 'sapreason1',
      },
      {
        name: 'sempname',
      },
      {
        name: 'psum',
      },
      {
        name: 'updateman',
      },
      {
        name: 'description',
      },
      {
        name: 'createman',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'itemrininfo',
      },
      {
        name: 'amount',
      },
      {
        name: 'enable',
      },
      {
        name: 'rin_psum',
      },
      {
        name: 'empname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'emitemrin',
        prop: 'emitemrinid',
      },
      {
        name: 'batcode',
      },
      {
        name: 'rinstate',
      },
      {
        name: 'sapreason',
      },
      {
        name: 'sumall',
      },
      {
        name: 'sap',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'orgid',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'empid',
      },
      {
        name: 'price',
      },
      {
        name: 'sapsm',
      },
      {
        name: 'sempid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'sdate',
      },
      {
        name: 'sapcontrol',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'itemcode',
      },
      {
        name: 'teamid',
      },
      {
        name: 'unitid',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'civo',
      },
      {
        name: 'unitname',
      },
      {
        name: 'poid',
      },
      {
        name: 'avgtsfee',
      },
      {
        name: 'itemname',
      },
      {
        name: 'wplistid',
      },
      {
        name: 'podetailname',
      },
      {
        name: 'itemtypename',
      },
      {
        name: 'avgprice',
      },
      {
        name: 'emservicename',
      },
      {
        name: 'porempname',
      },
      {
        name: 'shf',
      },
      {
        name: 'storename',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'emserviceid',
      },
      {
        name: 'podetailid',
      },
      {
        name: 'storeid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'storepartid',
      },
    ]
  }


}