/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemrinid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemrinname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emitemrinid',
        prop: 'emitemrinid',
        dataType: 'GUID',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'podetailname',
        prop: 'podetailname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'sdate',
        prop: 'sdate',
        dataType: 'DATETIME',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'TEXT',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'TEXT',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'psum',
        prop: 'psum',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'batcode',
        prop: 'batcode',
        dataType: 'TEXT',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'TEXT',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'TEXT',
      },
      {
        name: 'rinstate',
        prop: 'rinstate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'avgtsfee',
        prop: 'avgtsfee',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'civo',
        prop: 'civo',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'sapsm',
        prop: 'sapsm',
        dataType: 'SSCODELIST',
      },
      {
        name: 'shf',
        prop: 'shf',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'porempname',
        prop: 'porempname',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'emitemrinname',
        prop: 'emitemrinname',
        dataType: 'TEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'storepartid',
        prop: 'storepartid',
        dataType: 'PICKUP',
      },
      {
        name: 'storeid',
        prop: 'storeid',
        dataType: 'PICKUP',
      },
      {
        name: 'podetailid',
        prop: 'podetailid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitemrin',
        prop: 'emitemrinid',
        dataType: 'FONTKEY',
      },
    ]
  }

}