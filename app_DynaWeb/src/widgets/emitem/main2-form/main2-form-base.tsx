import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMItemService from '@/service/emitem/emitem-service';
import Main2Service from './main2-form-service';
import EMItemUIService from '@/uiservice/emitem/emitem-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main2EditFormBase}
 */
export class Main2EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main2Service}
     * @memberof Main2EditFormBase
     */
    public service: Main2Service = new Main2Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMItemService}
     * @memberof Main2EditFormBase
     */
    public appEntityService: EMItemService = new EMItemService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeName: string = 'emitem';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeLogicName: string = '物品';

    /**
     * 界面UI服务对象
     *
     * @type {EMItemUIService}
     * @memberof Main2Base
     */  
    public appUIService: EMItemUIService = new EMItemUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        itemcode: null,
        emitemname: null,
        itemgroup: null,
        itemtypename: null,
        acclassname: null,
        itemmodelcode: null,
        itemserialcode: null,
        isassetflag: null,
        checkmethod: null,
        itemdesc: null,
        dens: null,
        isnew: null,
        storename: null,
        storepartname: null,
        stocksum: null,
        unitname: null,
        price: null,
        amount: null,
        lastsum: null,
        highsum: null,
        repsum: null,
        abctype: null,
        lastprice: null,
        sempid: null,
        sempname: null,
        stockinl: null,
        lastindate: null,
        lastaempname: null,
        lastaempid: null,
        labservicename: null,
        empname: null,
        empid: null,
        mservicename: null,
        warrantyday: null,
        no3q: null,
        life: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        emitemid: null,
        emitem: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public majorMessageField: string = 'emitemname';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public rules(): any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: 'ITEM物品信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitem.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel15: new FormGroupPanelModel({ caption: '库存信息', detailType: 'GROUPPANEL', name: 'grouppanel15', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitem.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel31: new FormGroupPanelModel({ caption: '供应信息', detailType: 'GROUPPANEL', name: 'grouppanel31', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitem.main2_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel39: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel39', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitem.main2_form', extractMode: 'ITEM', details: [] } }),

        formpage38: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage38', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '物品标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '物品名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemcode: new FormItemModel({
    caption: '物品代码', detailType: 'FORMITEM', name: 'itemcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        emitemname: new FormItemModel({
    caption: '物品名称', detailType: 'FORMITEM', name: 'emitemname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemgroup: new FormItemModel({
    caption: '物品分组', detailType: 'FORMITEM', name: 'itemgroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemtypename: new FormItemModel({
    caption: '物品类型', detailType: 'FORMITEM', name: 'itemtypename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        acclassname: new FormItemModel({
    caption: '总帐科目', detailType: 'FORMITEM', name: 'acclassname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemmodelcode: new FormItemModel({
    caption: '产品型号', detailType: 'FORMITEM', name: 'itemmodelcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemserialcode: new FormItemModel({
    caption: '产品系列号', detailType: 'FORMITEM', name: 'itemserialcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        isassetflag: new FormItemModel({
    caption: '按资产', detailType: 'FORMITEM', name: 'isassetflag', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        checkmethod: new FormItemModel({
    caption: '验收方法', detailType: 'FORMITEM', name: 'checkmethod', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemdesc: new FormItemModel({
    caption: '物品备注', detailType: 'FORMITEM', name: 'itemdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dens: new FormItemModel({
    caption: '密度', detailType: 'FORMITEM', name: 'dens', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        isnew: new FormItemModel({
    caption: '物品新旧标识', detailType: 'FORMITEM', name: 'isnew', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storename: new FormItemModel({
    caption: '最新存储仓库', detailType: 'FORMITEM', name: 'storename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storepartname: new FormItemModel({
    caption: '最新存储库位', detailType: 'FORMITEM', name: 'storepartname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        stocksum: new FormItemModel({
    caption: '库存量', detailType: 'FORMITEM', name: 'stocksum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        unitname: new FormItemModel({
    caption: '单位', detailType: 'FORMITEM', name: 'unitname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        price: new FormItemModel({
    caption: '平均价', detailType: 'FORMITEM', name: 'price', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        amount: new FormItemModel({
    caption: '库存金额', detailType: 'FORMITEM', name: 'amount', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lastsum: new FormItemModel({
    caption: '最低库存', detailType: 'FORMITEM', name: 'lastsum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        highsum: new FormItemModel({
    caption: '最高库存', detailType: 'FORMITEM', name: 'highsum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        repsum: new FormItemModel({
    caption: '重订量', detailType: 'FORMITEM', name: 'repsum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        abctype: new FormItemModel({
    caption: 'ABC分类', detailType: 'FORMITEM', name: 'abctype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lastprice: new FormItemModel({
    caption: '最新价格', detailType: 'FORMITEM', name: 'lastprice', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sempid: new FormItemModel({
    caption: '库管员', detailType: 'FORMITEM', name: 'sempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sempname: new FormItemModel({
    caption: '库管员', detailType: 'FORMITEM', name: 'sempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        stockinl: new FormItemModel({
    caption: '库存周期(天)', detailType: 'FORMITEM', name: 'stockinl', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lastindate: new FormItemModel({
    caption: '最后入料时间', detailType: 'FORMITEM', name: 'lastindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lastaempname: new FormItemModel({
    caption: '最新请购人', detailType: 'FORMITEM', name: 'lastaempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lastaempid: new FormItemModel({
    caption: '最新请购人', detailType: 'FORMITEM', name: 'lastaempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        labservicename: new FormItemModel({
    caption: '建议供应商', detailType: 'FORMITEM', name: 'labservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empname: new FormItemModel({
    caption: '最新采购员', detailType: 'FORMITEM', name: 'empname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empid: new FormItemModel({
    caption: '最新采购员', detailType: 'FORMITEM', name: 'empid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mservicename: new FormItemModel({
    caption: '制造商', detailType: 'FORMITEM', name: 'mservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        warrantyday: new FormItemModel({
    caption: '保修天数', detailType: 'FORMITEM', name: 'warrantyday', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        no3q: new FormItemModel({
    caption: '不足3家供应商', detailType: 'FORMITEM', name: 'no3q', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        life: new FormItemModel({
    caption: '寿命周期(天)', detailType: 'FORMITEM', name: 'life', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        emitemid: new FormItemModel({
    caption: '物品标识', detailType: 'FORMITEM', name: 'emitemid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage38',
                    index: 1,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 新建默认值
     * @memberof Main2EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('itemgroup')) {
            this.data['itemgroup'] = '1';
        }
        if (this.data.hasOwnProperty('isnew')) {
            this.data['isnew'] = '1';
        }
        if (this.data.hasOwnProperty('stocksum')) {
            this.data['stocksum'] = 0;
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main2Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}