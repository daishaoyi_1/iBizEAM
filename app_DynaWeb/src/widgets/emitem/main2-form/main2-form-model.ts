/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'itemcode',
        prop: 'itemcode',
        dataType: 'TEXT',
      },
      {
        name: 'emitemname',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'itemgroup',
        prop: 'itemgroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'itemtypename',
        prop: 'itemtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'acclassname',
        prop: 'acclassname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemmodelcode',
        prop: 'itemmodelcode',
        dataType: 'TEXT',
      },
      {
        name: 'itemserialcode',
        prop: 'itemserialcode',
        dataType: 'TEXT',
      },
      {
        name: 'isassetflag',
        prop: 'isassetflag',
        dataType: 'YESNO',
      },
      {
        name: 'checkmethod',
        prop: 'checkmethod',
        dataType: 'TEXT',
      },
      {
        name: 'itemdesc',
        prop: 'itemdesc',
        dataType: 'TEXT',
      },
      {
        name: 'dens',
        prop: 'dens',
        dataType: 'FLOAT',
      },
      {
        name: 'isnew',
        prop: 'isnew',
        dataType: 'SSCODELIST',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'stocksum',
        prop: 'stocksum',
        dataType: 'FLOAT',
      },
      {
        name: 'unitname',
        prop: 'unitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'lastsum',
        prop: 'lastsum',
        dataType: 'FLOAT',
      },
      {
        name: 'highsum',
        prop: 'highsum',
        dataType: 'FLOAT',
      },
      {
        name: 'repsum',
        prop: 'repsum',
        dataType: 'FLOAT',
      },
      {
        name: 'abctype',
        prop: 'abctype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'lastprice',
        prop: 'lastprice',
        dataType: 'FLOAT',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'TEXT',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'TEXT',
      },
      {
        name: 'stockinl',
        prop: 'stockinl',
        dataType: 'FLOAT',
      },
      {
        name: 'lastindate',
        prop: 'lastindate',
        dataType: 'DATETIME',
      },
      {
        name: 'lastaempname',
        prop: 'lastaempname',
        dataType: 'TEXT',
      },
      {
        name: 'lastaempid',
        prop: 'lastaempid',
        dataType: 'TEXT',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'TEXT',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'TEXT',
      },
      {
        name: 'mservicename',
        prop: 'mservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'warrantyday',
        prop: 'warrantyday',
        dataType: 'FLOAT',
      },
      {
        name: 'no3q',
        prop: 'no3q',
        dataType: 'YESNO',
      },
      {
        name: 'life',
        prop: 'life',
        dataType: 'INT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emitemid',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
        dataType: 'FONTKEY',
      },
    ]
  }

}