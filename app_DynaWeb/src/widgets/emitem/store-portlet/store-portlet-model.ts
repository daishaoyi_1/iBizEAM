/**
 * Store 部件模型
 *
 * @export
 * @class StoreModel
 */
export default class StoreModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StoreModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'lastprice',
      },
      {
        name: 'objid',
      },
      {
        name: 'iteminfo',
      },
      {
        name: 'isassetflag',
      },
      {
        name: 'highsum',
      },
      {
        name: 'createdate',
      },
      {
        name: 'amount',
      },
      {
        name: 'isbatchflag',
      },
      {
        name: 'checkmethod',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
      },
      {
        name: 'stockdesum',
      },
      {
        name: 'abctype',
      },
      {
        name: 'shfprice',
      },
      {
        name: 'repsum',
      },
      {
        name: 'updateman',
      },
      {
        name: 'enable',
      },
      {
        name: 'stockextime',
      },
      {
        name: 'isnew',
      },
      {
        name: 'itemnid',
      },
      {
        name: 'itemserialcode',
      },
      {
        name: 'lastaempname',
      },
      {
        name: 'empid',
      },
      {
        name: 'registerdat',
      },
      {
        name: 'itemmodelcode',
      },
      {
        name: 'itemncode',
      },
      {
        name: 'itemcode',
      },
      {
        name: 'sempid',
      },
      {
        name: 'no3q',
      },
      {
        name: 'emitemname',
      },
      {
        name: 'dens',
      },
      {
        name: 'sapcontrol',
      },
      {
        name: 'batchtype',
      },
      {
        name: 'itemgroup',
      },
      {
        name: 'lastaempid',
      },
      {
        name: 'lastsum',
      },
      {
        name: 'empname',
      },
      {
        name: 'stocksum',
      },
      {
        name: 'createman',
      },
      {
        name: 'lastindate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'sapcontrolcode',
      },
      {
        name: 'life',
      },
      {
        name: 'price',
      },
      {
        name: 'costcenterid',
      },
      {
        name: 'stockinl',
      },
      {
        name: 'itemdesc',
      },
      {
        name: 'sempname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'warrantyday',
      },
      {
        name: 'storename',
      },
      {
        name: 'unitname',
      },
      {
        name: 'emcabname',
      },
      {
        name: 'storecode',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'mservicename',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'itemtypecode',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'itemtypename',
      },
      {
        name: 'emcabid',
      },
      {
        name: 'unitid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'mserviceid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'storeid',
      },
    ]
  }


}
