/**
 * Store 部件模型
 *
 * @export
 * @class StoreModel
 */
export default class StoreModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof StoreModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'stocksum',
        prop: 'stocksum',
        dataType: 'FLOAT',
      },
      {
        name: 'unitname',
        prop: 'unitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'lastsum',
        prop: 'lastsum',
        dataType: 'FLOAT',
      },
      {
        name: 'highsum',
        prop: 'highsum',
        dataType: 'FLOAT',
      },
      {
        name: 'repsum',
        prop: 'repsum',
        dataType: 'FLOAT',
      },
      {
        name: 'abctype',
        prop: 'abctype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'lastprice',
        prop: 'lastprice',
        dataType: 'FLOAT',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'TEXT',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'TEXT',
      },
      {
        name: 'stockinl',
        prop: 'stockinl',
        dataType: 'FLOAT',
      },
      {
        name: 'lastindate',
        prop: 'lastindate',
        dataType: 'DATETIME',
      },
      {
        name: 'lastaempid',
        prop: 'lastaempid',
        dataType: 'TEXT',
      },
      {
        name: 'lastaempname',
        prop: 'lastaempname',
        dataType: 'TEXT',
      },
      {
        name: 'emitemid',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
        dataType: 'FONTKEY',
      },
    ]
  }

}