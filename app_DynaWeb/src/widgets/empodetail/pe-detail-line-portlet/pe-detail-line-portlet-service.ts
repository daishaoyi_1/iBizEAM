import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * PeDetailLine 部件服务对象
 *
 * @export
 * @class PeDetailLineService
 */
export default class PeDetailLineService extends ControlService {
}
