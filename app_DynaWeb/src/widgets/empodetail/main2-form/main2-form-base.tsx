import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMPODetailService from '@/service/empodetail/empodetail-service';
import Main2Service from './main2-form-service';
import EMPODetailUIService from '@/uiservice/empodetail/empodetail-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main2EditFormBase}
 */
export class Main2EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main2Service}
     * @memberof Main2EditFormBase
     */
    public service: Main2Service = new Main2Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMPODetailService}
     * @memberof Main2EditFormBase
     */
    public appEntityService: EMPODetailService = new EMPODetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeName: string = 'empodetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeLogicName: string = '订单条目';

    /**
     * 界面UI服务对象
     *
     * @type {EMPODetailUIService}
     * @memberof Main2Base
     */  
    public appUIService: EMPODetailUIService = new EMPODetailUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main2EditFormBase
     */
    protected formKeyItemName: string = 'empodetailid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        empodetailid: null,
        poname: null,
        wplistname: null,
        itemname: null,
        itemdesc: null,
        listprice: null,
        discnt: null,
        psum: null,
        price: null,
        taxrate: null,
        totalprice: null,
        unitname: null,
        unitrate: null,
        civo: null,
        podetailstate: null,
        yiju: null,
        civocopy: null,
        useto: null,
        equipid: null,
        objid: null,
        equips: null,
        rempid: null,
        rempname: null,
        rdate: null,
        rsum: null,
        runitname: null,
        empid: null,
        empname: null,
        rprice: null,
        avgtaxfee: null,
        avgtsfee: null,
        shf: null,
        amount: null,
        sumall: null,
        attprice: null,
        orgid: null,
        description: null,
        empodetail: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public rules(): any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.empodetail.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel20: new FormGroupPanelModel({ caption: '采购申请信息', detailType: 'GROUPPANEL', name: 'grouppanel20', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.empodetail.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel25: new FormGroupPanelModel({ caption: '收货信息', detailType: 'GROUPPANEL', name: 'grouppanel25', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.empodetail.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '其它', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.empodetail.main2_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '订单条目号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '订单条目名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empodetailid: new FormItemModel({
    caption: '订单条目号', detailType: 'FORMITEM', name: 'empodetailid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        poname: new FormItemModel({
    caption: '订单', detailType: 'FORMITEM', name: 'poname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        wplistname: new FormItemModel({
    caption: '采购申请', detailType: 'FORMITEM', name: 'wplistname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        itemname: new FormItemModel({
    caption: '物品', detailType: 'FORMITEM', name: 'itemname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        itemdesc: new FormItemModel({
    caption: '物品备注', detailType: 'FORMITEM', name: 'itemdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        listprice: new FormItemModel({
    caption: '标价', detailType: 'FORMITEM', name: 'listprice', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        discnt: new FormItemModel({
    caption: '折扣(%)', detailType: 'FORMITEM', name: 'discnt', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        psum: new FormItemModel({
    caption: '订货数量', detailType: 'FORMITEM', name: 'psum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        price: new FormItemModel({
    caption: '单价', detailType: 'FORMITEM', name: 'price', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        taxrate: new FormItemModel({
    caption: '税率', detailType: 'FORMITEM', name: 'taxrate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        totalprice: new FormItemModel({
    caption: '总价', detailType: 'FORMITEM', name: 'totalprice', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        unitname: new FormItemModel({
    caption: '订货单位', detailType: 'FORMITEM', name: 'unitname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        unitrate: new FormItemModel({
    caption: '单位转换率', detailType: 'FORMITEM', name: 'unitrate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        civo: new FormItemModel({
    caption: '发票号', detailType: 'FORMITEM', name: 'civo', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        podetailstate: new FormItemModel({
    caption: '条目状态', detailType: 'FORMITEM', name: 'podetailstate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        yiju: new FormItemModel({
    caption: '验收凭据', detailType: 'FORMITEM', name: 'yiju', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        civocopy: new FormItemModel({
    caption: '发票存根', detailType: 'FORMITEM', name: 'civocopy', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        useto: new FormItemModel({
    caption: '用途', detailType: 'FORMITEM', name: 'useto', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equips: new FormItemModel({
    caption: '设备集合', detailType: 'FORMITEM', name: 'equips', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '收货人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '收货人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdate: new FormItemModel({
    caption: '收货日期', detailType: 'FORMITEM', name: 'rdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rsum: new FormItemModel({
    caption: '收货数量', detailType: 'FORMITEM', name: 'rsum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        runitname: new FormItemModel({
    caption: '收货单位', detailType: 'FORMITEM', name: 'runitname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empid: new FormItemModel({
    caption: '记账人', detailType: 'FORMITEM', name: 'empid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empname: new FormItemModel({
    caption: '记账人', detailType: 'FORMITEM', name: 'empname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rprice: new FormItemModel({
    caption: '收货单价', detailType: 'FORMITEM', name: 'rprice', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        avgtaxfee: new FormItemModel({
    caption: '均摊关税', detailType: 'FORMITEM', name: 'avgtaxfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        avgtsfee: new FormItemModel({
    caption: '均摊运杂费', detailType: 'FORMITEM', name: 'avgtsfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        shf: new FormItemModel({
    caption: '税费', detailType: 'FORMITEM', name: 'shf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        amount: new FormItemModel({
    caption: '物品金额', detailType: 'FORMITEM', name: 'amount', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sumall: new FormItemModel({
    caption: '含税总金额', detailType: 'FORMITEM', name: 'sumall', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        attprice: new FormItemModel({
    caption: '价格波动提醒', detailType: 'FORMITEM', name: 'attprice', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main2Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}