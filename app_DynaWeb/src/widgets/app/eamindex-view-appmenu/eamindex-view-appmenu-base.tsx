import { Prop, Provide, Vue, Model, Watch } from 'vue-property-decorator';
import EAMIndexViewModel from './eamindex-view-appmenu-model';
import AuthService from '@/authservice/auth-service';
import { Subject, Subscription } from 'rxjs';

/**
 * 应用菜单基类
 */
export class EAMIndexViewBase extends Vue {

    /**
     * 获取应用上下文
     *
     * @memberof EAMIndexViewBase
     */
    get context(): any {
        return this.$appService.contextStore.appContext || {};
    }

    /**
     * 名称
     *
     * @type {string}
     * @memberof EAMIndexViewBase
     */
    @Prop() public name?: string;

    /**
     * 视图通讯对象
     *
     * @type {Subject<ViewState>}
     * @memberof EAMIndexViewBase
     */
    @Prop() public viewState!: Subject<ViewState>;

    /**
     * 视图参数
     *
     * @type {*}
     * @memberof EAMIndexViewBase
     */
    @Prop() public viewparams!: any;

    /**
     * 菜单收缩改变
     *
     * @type {boolean}
     * @memberof EAMIndexViewBase
     */
    @Model() public collapsechange?: boolean;

    /**
     * 监听菜单收缩
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof EAMIndexViewBase
     */
    @Watch('collapsechange')
    onCollapsechangeChange(newVal: any, oldVal: any) {
        if (newVal !== this.isCollapse) {
            this.isCollapse = !this.isCollapse;
        }
    }

    /**
     * 当前模式，菜单在顶部还是在底部
     *
     * @type {*}
     * @memberof EAMIndexViewBase
     */
    @Prop() mode: any;

    /**
     * 应用起始页面
     *
     * @type {boolean}
     * @memberof EAMIndexViewBase
     */
    @Prop({ default: false }) isDefaultPage?: boolean;

    /**
     * 空白视图模式
     *
     * @type {boolean}
     * @memberof EAMIndexViewBase
     */
    @Prop({ default: false }) isBlankMode?:boolean;

    /**
     * 默认打开视图
     *
     * @type {*}
     * @memberof EAMIndexViewBase
     */
    @Prop() defPSAppView: any;

    /**
     * 默认激活的index
     *
     * @type {*}
     * @memberof EAMIndexViewBase
     */
    @Provide() defaultActive: any = null;

    /**
     * 当前选中主题
     *
     * @type {*}
     * @memberof EAMIndexViewBase
     */
    @Prop() selectTheme: any;

    /**
     * 默认打开的index数组
     *
     * @type {any[]}
     * @memberof EAMIndexViewBase
     */
    @Provide() public defaultOpeneds: any[] = [];

    /**
     * 是否展开
     *
     * @type {boolean}
     * @memberof EAMIndexViewBase
     */
    @Provide() public isCollapse: boolean = false;

    /**
     * 触发方式，默认click
     *
     * @type {string}
     * @memberof EAMIndexViewBase
     */
    @Provide() trigger: string = 'click';

    /**
     * 视图状态事件
     *
     * @public
     * @type {(Subscription | undefined)}
     * @memberof EAMIndexViewBase
     */
    public viewStateEvent: Subscription | undefined;

    /**
     * 获取部件类型
     *
     * @returns {string}
     * @memberof EAMIndexViewBase
     */
    public getControlType(): string {
        return 'APPMENU'
    }

    /**
     * 菜单模型
     *
     * @public
     * @type {EAMIndexViewModel}
     * @memberof EAMIndexViewBase
     */
    public menuMode: EAMIndexViewModel = new EAMIndexViewModel();

    /**
     * 菜单数据
     *
     * @public
     * @type {any[]}
     * @memberof EAMIndexViewBase
     */
    @Provide()
    public menus: any[] = [];
    
    /**
     * 主菜单数据
     *
     * @public
     * @type {any[]}
     * @memberof ProductNewBase
     */
    @Provide()
    public mainMenus: any[] = [];

    /**
     * 建构权限服务对象
     *
     * @type {AuthService}
     * @memberof EAMIndexViewBase
     */
    public authService:AuthService = new AuthService({ $store: this.$store });

    /**
     * vue  生命周期
     *
     * @memberof EAMIndexViewBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof EAMIndexViewBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                this.load(data);
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof EAMIndexViewBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof EAMIndexViewBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 处理菜单默认选中项
     *
     * @public
     * @memberof EAMIndexViewBase
     */
    public doMenuSelect(): void {
        if (!this.isDefaultPage || this.isBlankMode) {
            return;
        }
        const appFuncs: any[] = this.menuMode.getAppFuncs();
        if (this.$route && this.$route.matched && this.$route.matched.length == 2) { // 存在二级路由
            const [{ }, matched] = this.$route.matched;
            const appfunc: any = appFuncs.find((_appfunc: any) => Object.is(_appfunc.routepath, matched.path) && Object.is(_appfunc.appfuncyype, 'APPVIEW'));
            if (appfunc) {
                this.computeMenuSelect(this.menus, appfunc.appfunctag);
            }
            return;
        } else if (this.defPSAppView && Object.keys(this.defPSAppView).length > 0) { // 存在默认视图
            const appfunc: any = appFuncs.find((_appfunc: any) => Object.is(_appfunc.routepath, this.defPSAppView.routepath) && Object.is(_appfunc.appfuncyype, 'APPVIEW'));
            if (appfunc) {
                this.computeMenuSelect(this.menus, appfunc.appfunctag);
            }
            const viewparam: any = {};
            const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, this.defPSAppView.deResParameters, this.defPSAppView.parameters, [], viewparam);
            this.$router.push(path);
            return;
        }

        this.computeMenuSelect(this.menus, '');
        let item = this.compute(this.menus, this.defaultActive);
        if (Object.keys(item).length === 0) {
            return;
        }
        if(!item.hidden){
            this.click(item);
        }
    }

    /**
     * 计算菜单选中项
     *
     * @public
     * @param {any[]} items
     * @param {string} appfunctag
     * @returns {boolean}
     * @memberof EAMIndexViewBase
     */
    public computeMenuSelect(items: any[], appfunctag: string): boolean {
        const appFuncs: any[] = this.menuMode.getAppFuncs();
        return items.some((item: any) => {
            if (Object.is(appfunctag, '') && !Object.is(item.appfunctag, '') && item.opendefault) {
                const appfunc = appFuncs.find((_appfunc: any) => Object.is(_appfunc.appfunctag, item.appfunctag));
                if (appfunc.routepath) {
                    this.defaultActive = item.name;
                    this.setHideSideBar(item);
                    return true;
                }
            }
            if (Object.is(item.appfunctag, appfunctag) && item.opendefault) {
                this.setHideSideBar(item);
                this.defaultActive = item.name;
                return true;
            }
            if (item.items && item.items.length > 0) {
                const state = this.computeMenuSelect(item.items, appfunctag);
                if (state) {
                    this.defaultOpeneds.push(item.name);
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * 设置是否隐藏菜单栏
     *
     * @public
     * @param {*} item
     * @memberof EAMIndexViewBase
     */
    public setHideSideBar(item: any): void {
        if (item.hidesidebar) {
            this.$emit('collapsechange', true);
        }
    }

    /**
     * 获取菜单项数据
     *
     * @public
     * @param {any[]} items
     * @param {string} name
     * @returns
     * @memberof EAMIndexViewBase
     */
    public compute(items: any[], name: string) {
        const item: any = {};
        items.some((_item: any) => {
            if (name && Object.is(_item.name, name)) {
                Object.assign(item, _item);
                return true;
            }
            if (_item.items && Array.isArray(_item.items)) {
                const subItem = this.compute(_item.items, name);
                if (Object.keys(subItem).length > 0) {
                    Object.assign(item, subItem);
                    return true;
                }
            }
            return false;
        });
        return item;
    }

    /**
     * 菜单项选中处理
     *
     * @param {*} index
     * @param {any[]} indexs
     * @returns
     * @memberof EAMIndexViewBase
     */
    public select(index: any, indexs: any[]) {
        let item = this.compute(this.menus, index);
        if (Object.keys(item).length === 0) {
            return;
        }
        this.click(item);
    }

    /**
     * 菜单点击
     *
     * @param {*} item 菜单数据
     * @memberof EAMIndexViewBase
     */
    public click(item: any) {
        if (item) {
            let judge = true;
            switch (item.appfunctag) {
                case 'Auto33': 
                    this.clickAuto33(item); break;
                case 'Auto37': 
                    this.clickAuto37(item); break;
                case 'Auto38': 
                    this.clickAuto38(item); break;
                case 'Auto50': 
                    this.clickAuto50(item); break;
                case 'Auto17': 
                    this.clickAuto17(item); break;
                case 'Auto8': 
                    this.clickAuto8(item); break;
                case 'Auto16': 
                    this.clickAuto16(item); break;
                case 'Auto60': 
                    this.clickAuto60(item); break;
                case 'Auto5': 
                    this.clickAuto5(item); break;
                case 'Auto67': 
                    this.clickAuto67(item); break;
                case 'Auto27': 
                    this.clickAuto27(item); break;
                case 'Auto7': 
                    this.clickAuto7(item); break;
                case 'Auto32': 
                    this.clickAuto32(item); break;
                case 'Auto4': 
                    this.clickAuto4(item); break;
                case 'Auto41': 
                    this.clickAuto41(item); break;
                case 'Auto9': 
                    this.clickAuto9(item); break;
                case 'Auto59': 
                    this.clickAuto59(item); break;
                case 'Auto65': 
                    this.clickAuto65(item); break;
                case 'Auto29': 
                    this.clickAuto29(item); break;
                case 'Auto11': 
                    this.clickAuto11(item); break;
                case 'Auto63': 
                    this.clickAuto63(item); break;
                case 'Auto12': 
                    this.clickAuto12(item); break;
                case 'Auto62': 
                    this.clickAuto62(item); break;
                case 'Auto46': 
                    this.clickAuto46(item); break;
                case 'Auto1': 
                    this.clickAuto1(item); break;
                case 'Auto28': 
                    this.clickAuto28(item); break;
                case 'Auto13': 
                    this.clickAuto13(item); break;
                case 'Auto66': 
                    this.clickAuto66(item); break;
                case 'Auto18': 
                    this.clickAuto18(item); break;
                case 'Auto30': 
                    this.clickAuto30(item); break;
                case 'Auto54': 
                    this.clickAuto54(item); break;
                case 'AppFunc': 
                    this.clickAppFunc(item); break;
                case 'Auto35': 
                    this.clickAuto35(item); break;
                case 'Auto39': 
                    this.clickAuto39(item); break;
                case 'Auto48': 
                    this.clickAuto48(item); break;
                case 'AppFunc3': 
                    this.clickAppFunc3(item); break;
                case 'Auto6': 
                    this.clickAuto6(item); break;
                case 'Auto26': 
                    this.clickAuto26(item); break;
                case 'Auto14': 
                    this.clickAuto14(item); break;
                case 'Auto64': 
                    this.clickAuto64(item); break;
                case 'Auto21': 
                    this.clickAuto21(item); break;
                case 'Auto36': 
                    this.clickAuto36(item); break;
                case 'AppFunc4': 
                    this.clickAppFunc4(item); break;
                case 'Auto31': 
                    this.clickAuto31(item); break;
                case 'Auto20': 
                    this.clickAuto20(item); break;
                case 'Auto10': 
                    this.clickAuto10(item); break;
                case 'Auto19': 
                    this.clickAuto19(item); break;
                case 'Auto56': 
                    this.clickAuto56(item); break;
                case 'Auto15': 
                    this.clickAuto15(item); break;
                case 'AppFunc2': 
                    this.clickAppFunc2(item); break;
                case 'Auto51': 
                    this.clickAuto51(item); break;
                case 'Auto22': 
                    this.clickAuto22(item); break;
                case 'Auto3': 
                    this.clickAuto3(item); break;
                case 'Auto34': 
                    this.clickAuto34(item); break;
                default:
                    judge = false;
                    console.warn('未指定应用功能');
            }
            if (judge && this.$uiState.isStyle2()) {
                this.$appService.navHistory.reset();
                this.$appService.viewStore.reset();
            }
        }
    }
    
    /**
     * 事故记录<活动>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto33(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqdebugs', parameterName: 'emeqdebug' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 工作台
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto37(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'appportalview', parameterName: 'appportalview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 维修记录<活动>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto38(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqchecks', parameterName: 'emeqcheck' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 现象<故障>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto50(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emrfodes', parameterName: 'emrfode' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 外委工单查询<工单>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto17(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emwo_oscs', parameterName: 'emwo_osc' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 还料单<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto8(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emitemprtns', parameterName: 'emitemprtn' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 外委申请查询<工单>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto16(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emapplies', parameterName: 'emapply' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 运行监控
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto60(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqmonitors', parameterName: 'emeqmonitor' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 能耗工单查询<工单>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto5(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emwo_ens', parameterName: 'emwo_en' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 关键点记录
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto67(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqkprcds', parameterName: 'emeqkprcd' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 更换安装<活动>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto27(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqsetups', parameterName: 'emeqsetup' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 领料单<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto7(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emitempuses', parameterName: 'emitempuse' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 能耗<能耗>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto32(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emenconsums', parameterName: 'emenconsum' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 调整单<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto4(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emitemcs', parameterName: 'emitemcs' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 模式<故障>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto41(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emrfomos', parameterName: 'emrfomo' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 内部工单查询<工单>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto9(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emwo_inners', parameterName: 'emwo_inner' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 设备仪表读数
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto59(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqmpmtrs', parameterName: 'emeqmpmtr' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 运行日志
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto65(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqwls', parameterName: 'emeqwl' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 固定资产台账<资产>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto29(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emassets', parameterName: 'emasset' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 仓库<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto11(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emstores', parameterName: 'emstore' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 关键点
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto63(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqkps', parameterName: 'emeqkp' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 位置<设备>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto12(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqlocations', parameterName: 'emeqlocation' },
            { pathName: 'treeexpview', parameterName: 'treeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 设备仪表
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto62(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqmps', parameterName: 'emeqmp' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 报废资产<资产>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto46(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emassets', parameterName: 'emasset' },
            { pathName: 'gridview_bf', parameterName: 'gridview_bf' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 设备档案<设备>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto1(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqtypes', parameterName: 'emeqtype' },
            { pathName: 'treeexpview', parameterName: 'treeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 保养记录<活动>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto28(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqkeeps', parameterName: 'emeqkeep' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 出库单<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto13(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emitemrouts', parameterName: 'emitemrout' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 故障知识库
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto66(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emrfodes', parameterName: 'emrfode' },
            { pathName: 'gridexpview', parameterName: 'gridexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 损益单<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto18(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emitempls', parameterName: 'emitempl' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 方案<故障>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto30(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emrfoacs', parameterName: 'emrfoac' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 采购申请流程
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto54(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emwplists', parameterName: 'emwplist' },
            { pathName: 'wpprocesstreeexpview', parameterName: 'wpprocesstreeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    /**
     * iBiz开放平台
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAppFunc(item: any = {}){
       const localdata: any = this.$store.getters.getLocalData();
       const url = `https://www.ibizlab.cn/#/common_index/partner_curdevcenterdashboardview`;
       window.open(url, '_blank');
    }
    
    /**
     * 能源<能耗>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto35(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emen', parameterName: 'emen' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 资产盘点记录<资产>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto39(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emassetclears', parameterName: 'emassetclear' },
            { pathName: 'gridview_5564', parameterName: 'gridview_5564' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 原因<故障>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto48(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emrfocas', parameterName: 'emrfoca' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    /**
     * 模型设计工具
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAppFunc3(item: any = {}){
       const localdata: any = this.$store.getters.getLocalData();
       const url = `http://mos.ibizlab.cn/mos/?#/common_mosindex/srfkeys=433D8BEB-6024-4E94-953C-3807DF994584/sysdesign_pssystemmaintabexpview/sysdesign_pssystemmainview9`;
       window.open(url, '_blank');
    }
    
    /**
     * 计划模板<计划>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto6(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emplantempls', parameterName: 'emplantempl' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 现象分类<故障>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto26(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emrfodetypes', parameterName: 'emrfodetype' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 物品<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto14(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emitemtypes', parameterName: 'emitemtype' },
            { pathName: 'itemtreeexpview', parameterName: 'itemtreeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 活动日历导航
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto64(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqahs', parameterName: 'emeqah' },
            { pathName: 'calendarexpview', parameterName: 'calendarexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 设备类型<设备>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto21(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqtypes', parameterName: 'emeqtype' },
            { pathName: 'treeexpview2', parameterName: 'treeexpview2' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 服务商审批<采购>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto36(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emservices', parameterName: 'emservice' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    /**
     * iBiz论坛
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAppFunc4(item: any = {}){
       const localdata: any = this.$store.getters.getLocalData();
       const url = `https://bbs.ibizlab.cn/`;
       window.open(url, '_blank');
    }
    
    /**
     * 抢修记录<活动>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto31(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqmaintances', parameterName: 'emeqmaintance' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 文档<设备>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto20(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emdrwgs', parameterName: 'emdrwg' },
            { pathName: 'treeexpview', parameterName: 'treeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 备件包<设备>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto10(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emeqspares', parameterName: 'emeqspare' },
            { pathName: 'gridexpview', parameterName: 'gridexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 库存明细<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto19(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emstocks', parameterName: 'emstock' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 工单日历
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto56(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emwos', parameterName: 'emwo' },
            { pathName: 'calendarview', parameterName: 'calendarview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 计划<计划>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto15(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emplans', parameterName: 'emplan' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    /**
     * 项目文件
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAppFunc2(item: any = {}){
       const localdata: any = this.$store.getters.getLocalData();
       const url = `https://gitee.com/ibizlab/iBizEAM`;
       window.open(url, '_blank');
    }
    
    /**
     * 资产科目<资产>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto51(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emassetclasses', parameterName: 'emassetclass' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 物品类型<物资>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto22(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emitemtypes', parameterName: 'emitemtype' },
            { pathName: 'infotreeexpview', parameterName: 'infotreeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 点检工单查询<工单>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto3(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emwo_dps', parameterName: 'emwo_dp' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 服务商评估<采购>
     *
     * @param {*} [item={}]
     * @memberof EAMIndexView
     */
    public clickAuto34(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'emserviceevls', parameterName: 'emserviceevl' },
            { pathName: 'tabexpview', parameterName: 'tabexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }

    /**
     * 数据加载
     *
     * @param {*} data
     * @memberof EAMIndexViewBase
     */
    public load(data: any) {
        this.handleMenusResource(this.menuMode.getAppMenuItems());
    }

    /**
     * 通过统一资源标识计算菜单
     *
     * @param {*} data
     * @memberof EAMIndexViewBase
     */
    public handleMenusResource(inputMenus:Array<any>){
        let mainMenus: Array<any> = [];
        if(this.$store.getters['authresource/getEnablePermissionValid']){
            this.computedEffectiveMenus(inputMenus);
            this.computeParentMenus(inputMenus);
        }
        this.dataProcess(inputMenus);
        if(inputMenus && inputMenus.length > 0){
            inputMenus.forEach((menu: any) =>{
                if(Object.is(menu.name,'main_menus') && menu.items.length > 0){
                    mainMenus = [...menu.items];
                }
            })
        }
        this.menus = inputMenus;
        this.mainMenus = mainMenus;
        this.doMenuSelect();
    }

    /**
     * 计算有效菜单项
     *
     * @param {*} inputMenus
     * @memberof EAMIndexViewBase
     */
    public computedEffectiveMenus(inputMenus:Array<any>){
        inputMenus.forEach((_item:any) =>{
            if(!this.authService.getMenusPermission(_item)){
                _item.hidden = true;
                if (_item.items && _item.items.length > 0) {
                    this.computedEffectiveMenus(_item.items);
                }
            }
        })
    }

    /**
     * 计算父项菜单项是否隐藏
     *
     * @param {*} inputMenus
     * @memberof EAMIndexViewBase
     */
    public computeParentMenus(inputMenus:Array<any>){
        if(inputMenus && inputMenus.length >0){
            inputMenus.forEach((item:any) =>{
                if(item.hidden && item.items && item.items.length >0){
                    item.items.map((singleItem:any) =>{
                        if(!singleItem.hidden){
                            item.hidden = false;
                        }else{
                            if(singleItem.items && singleItem.items.length >0){
                                singleItem.items.map((grandsonItem:any) =>{
                                    if(!grandsonItem.hidden){
                                        item.hidden = false;
                                    }
                                })
                            }
                        }
                        if(item.items && item.items.length >0){
                            this.computeParentMenus(item.items);
                        }
                    })
                }
            })
        }
    }

    /**
     * 数据处理
     *
     * @public
     * @param {any[]} items
     * @memberof EAMIndexViewBase
     */
    public dataProcess(items: any[]): void {
        items.forEach((_item: any) => {
            if (_item.expanded) {
                this.defaultOpeneds.push(_item.name);
            }
            if (_item.items && _item.items.length > 0) {
                this.dataProcess(_item.items)
            }
        });
    }

    /**
     * 提示框主题样式
     *
     * @readonly
     * @type {string}
     * @memberof EAMIndexViewBase
     */
    get popperClass(): string {
        return 'app-popper-menu ' + this.selectTheme;
    }
}