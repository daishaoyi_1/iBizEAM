import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import IndexViewModel from './index-view-appmenu-model';


/**
 * IndexView 部件服务对象
 *
 * @export
 * @class IndexViewService
 */
export default class IndexViewService extends ControlService {

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof IndexViewService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of IndexViewService.
     * 
     * @param {*} [opts={}]
     * @memberof IndexViewService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new IndexViewModel();
    }

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof IndexView
     */
    @Errorlog
    public get(params: any = {}): Promise<any> {
        return Http.getInstance().get('v7/index-viewappmenu', params);
    }

}