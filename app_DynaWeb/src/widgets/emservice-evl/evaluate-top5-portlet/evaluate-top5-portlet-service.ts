import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * EvaluateTop5 部件服务对象
 *
 * @export
 * @class EvaluateTop5Service
 */
export default class EvaluateTop5Service extends ControlService {
}
