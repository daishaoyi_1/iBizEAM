/**
 * TabExpViewtabviewpanel4 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel4Model
 */
export default class TabExpViewtabviewpanel4Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel4Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emserviceevl',
        prop: 'emserviceevlid',
      },
      {
        name: 'emserviceevlname',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'evlresult9',
      },
      {
        name: 'evlresult5',
      },
      {
        name: 'evlresult7',
      },
      {
        name: 'evlresult6',
      },
      {
        name: 'evlresult1',
      },
      {
        name: 'createdate',
      },
      {
        name: 'evlmark1',
      },
      {
        name: 'evlresult4',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'evlresult8',
      },
      {
        name: 'createman',
      },
      {
        name: 'empname',
      },
      {
        name: 'evldate',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'evlmark',
      },
      {
        name: 'evlresult',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'evlregion',
      },
      {
        name: 'evlresult3',
      },
      {
        name: 'evlresult2',
      },
      {
        name: 'enable',
      },
      {
        name: 'empid',
      },
      {
        name: 'serviceevlstate',
      },
      {
        name: 'description',
      },
      {
        name: 'servicename',
      },
      {
        name: 'serviceid',
      },
    ]
  }


}