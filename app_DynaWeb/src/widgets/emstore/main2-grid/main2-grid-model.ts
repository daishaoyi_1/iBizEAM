/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'storecode',
          prop: 'storecode',
          dataType: 'TEXT',
        },
        {
          name: 'emstorename',
          prop: 'emstorename',
          dataType: 'TEXT',
        },
        {
          name: 'storetypeid',
          prop: 'storetypeid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emstorename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emstoreid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emstoreid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emstore',
          prop: 'emstoreid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}