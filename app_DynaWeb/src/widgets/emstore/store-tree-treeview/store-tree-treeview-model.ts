/**
 * StoreTree 部件模型
 *
 * @export
 * @class StoreTreeModel
 */
export default class StoreTreeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StoreTreeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'description',
      },
      {
        name: 'storecode',
      },
      {
        name: 'createdate',
      },
      {
        name: 'storeinfo',
      },
      {
        name: 'standpriceflag',
      },
      {
        name: 'poweravgflag',
      },
      {
        name: 'createman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'emstore',
        prop: 'emstoreid',
      },
      {
        name: 'newstoretypeid',
      },
      {
        name: 'costcenterid',
      },
      {
        name: 'mgrpersonid',
      },
      {
        name: 'orgid',
      },
      {
        name: 'ioalgo',
      },
      {
        name: 'empid',
      },
      {
        name: 'storetypeid',
      },
      {
        name: 'storeaddr',
      },
      {
        name: 'updateman',
      },
      {
        name: 'empname',
      },
      {
        name: 'emstorename',
      },
      {
        name: 'enable',
      },
      {
        name: 'storetel',
      },
      {
        name: 'storefax',
      },
    ]
  }


}