/**
 * TradeTree 部件模型
 *
 * @export
 * @class TradeTreeModel
 */
export default class TradeTreeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TradeTreeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'sempname',
      },
      {
        name: 'batcode',
      },
      {
        name: 'emitemtradename',
      },
      {
        name: 'civo',
      },
      {
        name: 'price',
      },
      {
        name: 'createdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'aempname',
      },
      {
        name: 'orgid',
      },
      {
        name: 'sdate',
      },
      {
        name: 'tradestate',
      },
      {
        name: 'pusetype',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'aempid',
      },
      {
        name: 'psum',
      },
      {
        name: 'amount',
      },
      {
        name: 'inoutflag',
      },
      {
        name: 'deptname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'shf',
      },
      {
        name: 'deptid',
      },
      {
        name: 'emitemtradetype',
      },
      {
        name: 'sempid',
      },
      {
        name: 'emitemtrade',
        prop: 'emitemtradeid',
      },
      {
        name: 'itemtypegroup',
      },
      {
        name: 'enable',
      },
      {
        name: 'description',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'storename',
      },
      {
        name: 'shfprice',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'stockamount',
      },
      {
        name: 'rname',
      },
      {
        name: 'itemcode',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'teamname',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'teamid',
      },
      {
        name: 'rid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'storeid',
      },
      {
        name: 'storepartid',
      },
    ]
  }


}