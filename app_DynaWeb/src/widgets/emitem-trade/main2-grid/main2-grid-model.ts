/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emitemtradeid',
          prop: 'emitemtradeid',
          dataType: 'GUID',
        },
        {
          name: 'emitemtradetype',
          prop: 'emitemtradetype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'sdate',
          prop: 'sdate',
          dataType: 'DATETIME',
        },
        {
          name: 'inoutflag',
          prop: 'inoutflag',
          dataType: 'INT',
        },
        {
          name: 'storename',
          prop: 'storename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'storepartname',
          prop: 'storepartname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'psum',
          prop: 'psum',
          dataType: 'FLOAT',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'FLOAT',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'FLOAT',
        },
        {
          name: 'batcode',
          prop: 'batcode',
          dataType: 'TEXT',
        },
        {
          name: 'sempname',
          prop: 'sempname',
          dataType: 'TEXT',
        },
        {
          name: 'rname',
          prop: 'rname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'tradestate',
          prop: 'tradestate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'storepartid',
          prop: 'storepartid',
          dataType: 'PICKUP',
        },
        {
          name: 'storeid',
          prop: 'storeid',
          dataType: 'PICKUP',
        },
        {
          name: 'teamid',
          prop: 'teamid',
          dataType: 'PICKUP',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emitemtradename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emitemtradeid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emitemtradeid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'rid',
          prop: 'rid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfdatatype',
          prop: 'emitemtradetype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'emitemtrade',
          prop: 'emitemtradeid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}