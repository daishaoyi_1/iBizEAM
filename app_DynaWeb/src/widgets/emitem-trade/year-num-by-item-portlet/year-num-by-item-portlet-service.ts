import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * YearNumByItem 部件服务对象
 *
 * @export
 * @class YearNumByItemService
 */
export default class YearNumByItemService extends ControlService {
}
