import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import EMWO_INNERService from '@/service/emwo-inner/emwo-inner-service';
import Main6Service from './main6-grid-service';
import EMWO_INNERUIService from '@/uiservice/emwo-inner/emwo-inner-ui-service';
import { FormItemModel } from '@/model/form-detail';

/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {Main6GridBase}
 */
export class Main6GridBase extends GridControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main6GridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {Main6Service}
     * @memberof Main6GridBase
     */
    public service: Main6Service = new Main6Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWO_INNERService}
     * @memberof Main6GridBase
     */
    public appEntityService: EMWO_INNERService = new EMWO_INNERService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main6GridBase
     */
    protected appDeName: string = 'emwo_inner';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main6GridBase
     */
    protected appDeLogicName: string = '内部工单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWO_INNERUIService}
     * @memberof Main6Base
     */  
    public appUIService: EMWO_INNERUIService = new EMWO_INNERUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof Main6Base
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof Main6Base
     */  
    public majorInfoColName:string = "emwo_innername";

    /**
     * 列主键属性名称
     *
     * @type {string}
     * @memberof Main6GridBase
     */
    public columnKeyName: string = "emwo_innerid";

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof Main6Base
     */
    protected localStorageTag: string = 'emwo_inner_main6_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof Main6GridBase
     */
    public allColumns: any[] = [
        {
            name: 'emwo_innerid',
            label: '工单编号',
            langtag: 'entities.emwo_inner.main6_grid.columns.emwo_innerid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'priority',
            label: '优先级',
            langtag: 'entities.emwo_inner.main6_grid.columns.priority',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'emwo_innername',
            label: '工单名称',
            langtag: 'entities.emwo_inner.main6_grid.columns.emwo_innername',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'equipname',
            label: '设备',
            langtag: 'entities.emwo_inner.main6_grid.columns.equipname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'objname',
            label: '位置',
            langtag: 'entities.emwo_inner.main6_grid.columns.objname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'wodate',
            label: '执行日期',
            langtag: 'entities.emwo_inner.main6_grid.columns.wodate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'wfstep',
            label: '流程步骤',
            langtag: 'entities.emwo_inner.main6_grid.columns.wfstep',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'rteamname',
            label: '责任班组',
            langtag: 'entities.emwo_inner.main6_grid.columns.rteamname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'wresult',
            label: '执行结果',
            langtag: 'entities.emwo_inner.main6_grid.columns.wresult',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'regionbegindate',
            label: '起始时间',
            langtag: 'entities.emwo_inner.main6_grid.columns.regionbegindate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'regionenddate',
            label: '结束时间',
            langtag: 'entities.emwo_inner.main6_grid.columns.regionenddate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'worklength',
            label: '实际工时(分)',
            langtag: 'entities.emwo_inner.main6_grid.columns.worklength',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'eqstoplength',
            label: '停运时间(分)',
            langtag: 'entities.emwo_inner.main6_grid.columns.eqstoplength',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'mfee',
            label: '约合材料费(￥)',
            langtag: 'entities.emwo_inner.main6_grid.columns.mfee',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'cplanflag',
            label: '转计划标志',
            langtag: 'entities.emwo_inner.main6_grid.columns.cplanflag',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'wogroup',
            label: '工单分组',
            langtag: 'entities.emwo_inner.main6_grid.columns.wogroup',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'wotype',
            label: '工单类型',
            langtag: 'entities.emwo_inner.main6_grid.columns.wotype',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'wopname',
            label: '上级工单',
            langtag: 'entities.emwo_inner.main6_grid.columns.wopname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'wooriname',
            label: '工单来源',
            langtag: 'entities.emwo_inner.main6_grid.columns.wooriname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'woteam',
            label: '工单组',
            langtag: 'entities.emwo_inner.main6_grid.columns.woteam',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'waitmodi',
            label: '等待修理时(分)',
            langtag: 'entities.emwo_inner.main6_grid.columns.waitmodi',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'waitbuy',
            label: '等待配件时(分)',
            langtag: 'entities.emwo_inner.main6_grid.columns.waitbuy',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof Main6GridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 是否启用分组
     *
     * @type {boolean}
     * @memberof Main6Base
     */
    public isEnableGroup:boolean = false;

    /**
     * 分组属性
     *
     * @type {string}
     * @memberof Main6Base
     */
    public groupAppField:string ="";

    /**
     * 分组属性代码表标识
     *
     * @type {string}
     * @memberof Main6Base
     */
    public groupAppFieldCodelistTag:string ="";

    /**
     * 分组属性代码表类型
     * 
     * @type {string}
     * @memberof Main6Base
     */
    public groupAppFieldCodelistType: string = "";

    /**
     * 分组模式
     *
     * @type {string}
     * @memberof Main6Base
     */
    public groupMode:string ="NONE";

    /**
     * 分组代码表标识
     * 
     * @type {string}
     * @memberof Main6Base
     */
    public codelistTag: string = "";

    /**
     * 分组代码表类型
     * 
     * @type {string}
     * @memberof Main6Base
     */
    public codelistType: string = "";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main6GridBase
     */
    public rules() {
        return {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '工单编号 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '工单编号 值不能为空', trigger: 'blur' },
        ],
    }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main6Base
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof Main6Base
     */
    public hasRowEdit: any = {
        'emwo_innerid':false,
        'priority':false,
        'emwo_innername':false,
        'equipname':false,
        'objname':false,
        'wodate':false,
        'wfstep':false,
        'rteamname':false,
        'wresult':false,
        'regionbegindate':false,
        'regionenddate':false,
        'worklength':false,
        'eqstoplength':false,
        'mfee':false,
        'cplanflag':false,
        'wogroup':false,
        'wotype':false,
        'wopname':false,
        'wooriname':false,
        'woteam':false,
        'waitmodi':false,
        'waitbuy':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof Main6Base
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        let className: string = '';
        if(args.column.property){
          let col = this.allColumns.find((item:any)=>{
              return Object.is(args.column.property,item.name);
          })
          if(col !== undefined){
              if(col.isEnableRowEdit && this.actualIsOpenEdit ){
                className += 'edit-cell ';
              }
          } else {
              className += 'info-cell';
          }
        }
        if(this.groupAppField && args.columnIndex === 0 && !this.isSingleSelect) {
            if(args.row.children && args.row.children.length > 0) {
                className += this.computeGroupRow(args.row.children, args.row);
            }
        }
        return className;
    }
    
    /**
     * 计算分组行checkbox选中样式
     *
     * @param {*} rows 当前分组行下的所有数据
     * @returns {*} currentRow 当前分组行
     * @memberof MainBase
     */
    public computeGroupRow(rows: any[], currentRow: any) {
        let count: number = 0;
        this.selections.forEach((select: any) => {
            rows.forEach((row: any) => {
                if(row.groupById === select.groupById) {
                    count++;
                }
            })
        })
        if(count === rows.length) {
            (this.$refs.multipleTable as any).toggleRowSelection(currentRow, true);
            return 'cell-select-all ';
        } else if(count !== 0 && count < rows.length) {
            return 'cell-indeterminate '
        } else if(count === 0) {
            (this.$refs.multipleTable as any).toggleRowSelection(currentRow, false);
            return '';
        }
    }

    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof Main6GridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'priority',
                srfkey: 'EMPRIORITY',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'wfstep',
                srfkey: 'EMWOWFSTEP',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'cplanflag',
                srfkey: 'YesNo',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'wogroup',
                srfkey: 'EMWOGROUP',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'wotype',
                srfkey: 'EMWOTYPE1',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof Main6Base
     */
    public updateDefault(row: any){                    
    }

    /**
    * 合并分组行
    * 
    * @memberof Main6Base
    */
    public arraySpanMethod({row, column, rowIndex, columnIndex} : any) {
        let allColumns:Array<any> = ['emwo_innerid','priority','emwo_innername','equipname','objname','wodate','wfstep','rteamname','wresult','regionbegindate','regionenddate','worklength','eqstoplength','mfee','cplanflag','wogroup','wotype','wopname','wooriname','woteam','waitmodi','waitbuy'];
        if(row && row.children) {
            if(columnIndex == (this.isSingleSelect ? 0:1)) {
                return [1, allColumns.length+1];
            } else if(columnIndex > (this.isSingleSelect ? 0:1)) {
                return [0,0];
            }
        }
    }

	/**
     * 分组方法
     * 
     * @memberof Main6Base
     */
    public group(){
        if(Object.is(this.groupMode,"AUTO")){
            this.drawGroup();
        }else if(Object.is(this.groupMode,"CODELIST")){
            this.drawCodelistGroup();
        }
    }

    /**
     * 获取表格分组相关代码表
     * 
     * @param {string}  codelistType 代码表类型
     * @param {string}  codelistTag 代码表标识
     * @memberof Main6Base
     */
    public async getGroupCodelist(codelistType: string,codelistTag:string){
        let codelist: Array<any> = [];
        // 动态代码表
        if (Object.is(codelistType, "DYNAMIC")) {
             codelist = await this.codeListService.getItems(codelistTag);
        // 静态代码表
        } else if(Object.is(codelistType, "STATIC")){
            codelist = this.$store.getters.getCodeListItems(codelistTag);
        }
        return codelist;
    }

    /**
     * 根据分组代码表绘制分组列表
     * 
     * @memberof Main6Base
     */
    public async drawCodelistGroup(){
        if(!this.isEnableGroup) return;
        // 分组
        let allGroup: Array<any> = [];
        let allGroupField: Array<any> =[];
        let groupTree:Array<any> = [];
        allGroup = await this.getGroupCodelist(this.codelistType,this.codelistTag);
        allGroupField = await this.getGroupCodelist(this.groupAppFieldCodelistType,this.groupAppFieldCodelistTag);
        if(allGroup.length == 0){
            console.warn("分组数据无效");
        }
        allGroup.forEach((group: any,i: number)=>{
            let children:Array<any> = [];
            this.items.forEach((item: any,j: number)=>{
                if(allGroupField && allGroupField.length > 0){
                    const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                    if(arr && arr.length>0) {
                        if(Object.is(group.value,arr[0].value)){
                            item.groupById = Number((i+1) * 100 + (j+1) * 1);
                            item.group = '';
                            children.push(item);
                        }
                    }
                }else if(Object.is(group.value,item[this.groupAppField])){
                    item.groupById = Number((i+1) * 100 + (j+1) * 1);
                    item.group = '';
                    children.push(item);
                }
            });
            const tree: any ={
                groupById: Number((i+1)*100),
                group: group.label,
                emwo_innerid:'',
                priority:'',
                emwo_innername:'',
                equipname:'',
                objname:'',
                wodate:'',
                wfstep:'',
                rteamname:'',
                wresult:'',
                regionbegindate:'',
                regionenddate:'',
                worklength:'',
                eqstoplength:'',
                mfee:'',
                cplanflag:'',
                wogroup:'',
                wotype:'',
                wopname:'',
                wooriname:'',
                woteam:'',
                waitmodi:'',
                waitbuy:'',
                children: children
            }
            groupTree.push(tree);
        });
        let child:Array<any> = [];
        this.items.forEach((item: any,index: number)=>{
            let i: number = 0;
            if(allGroupField && allGroupField.length > 0){
                const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                if(arr && arr.length>0) {
                    i = allGroup.findIndex((group: any)=>Object.is(group.value,arr[0].value));
                }
            }else{
                i = allGroup.findIndex((group: any)=>Object.is(group.value,item[this.groupAppField]));
            }
            if(i < 0){
                item.groupById = Number((allGroup.length+1) * 100 + (index+1) * 1);
                item.group = '';
                child.push(item);
            }
        })
        const Tree: any = {
            groupById: Number((allGroup.length+1)*100),
            group: '其他',
            emwo_innerid:'',
            priority:'',
            emwo_innername:'',
            equipname:'',
            objname:'',
            wodate:'',
            wfstep:'',
            rteamname:'',
            wresult:'',
            regionbegindate:'',
            regionenddate:'',
            worklength:'',
            eqstoplength:'',
            mfee:'',
            cplanflag:'',
            wogroup:'',
            wotype:'',
            wopname:'',
            wooriname:'',
            woteam:'',
            waitmodi:'',
            waitbuy:'',
            children: child
        }
        if(child && child.length > 0){
            groupTree.push(Tree);
        }
        this.items = groupTree;
        if(this.actualIsOpenEdit) {
            for(let i = 0; i < this.items.length; i++) {
                this.gridItemsModel.push(this.getGridRowModel());
            }
        }
    }

    /**
     * 绘制分组
     * 
     * @memberof Main6Base
     */
    public async drawGroup(){
        if(!this.isEnableGroup) return;
        // 分组
        let allGroup: Array<any> = [];
        let allGroupField: Array<any> =[];
        allGroupField = await this.getGroupCodelist(this.groupAppFieldCodelistType,this.groupAppFieldCodelistTag);
        this.items.forEach((item: any)=>{
            if(item.hasOwnProperty(this.groupAppField)){
                if(allGroupField && allGroupField.length > 0){
                    const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                    allGroup.push(arr[0].label);
                }else{
                    allGroup.push(item[this.groupAppField]);
                }
            }
        });
        let groupTree:Array<any> = [];
        allGroup = [...new Set(allGroup)];
        if(allGroup.length == 0){
            console.warn("分组数据无效");
        }
        // 组装数据
        allGroup.forEach((group: any, groupIndex: number)=>{
            let children:Array<any> = [];
            this.items.forEach((item: any,itemIndex: number)=>{
                if(allGroupField && allGroupField.length > 0){
                    const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                    if(Object.is(group,arr[0].label)){
                        item.groupById = Number((groupIndex+1) * 100 + (itemIndex+1) * 1);
                        item.group = '';
                        children.push(item);
                    }
                }else if(Object.is(group,item[this.groupAppField])){
                    item.groupById = Number((groupIndex+1) * 100 + (itemIndex+1) * 1);
                    item.group = '';
                    children.push(item);
                }
            });
            group = group ? group : '其他';
            const tree: any ={
                groupById: Number((groupIndex+1)*100),
                group: group,
                emwo_innerid:'',
                priority:'',
                emwo_innername:'',
                equipname:'',
                objname:'',
                wodate:'',
                wfstep:'',
                rteamname:'',
                wresult:'',
                regionbegindate:'',
                regionenddate:'',
                worklength:'',
                eqstoplength:'',
                mfee:'',
                cplanflag:'',
                wogroup:'',
                wotype:'',
                wopname:'',
                wooriname:'',
                woteam:'',
                waitmodi:'',
                waitbuy:'',
                children: children,
            }
            groupTree.push(tree);
        });
        this.items = groupTree;
        if(this.actualIsOpenEdit) {
            for(let i = 0; i < this.items.length; i++) {
                this.gridItemsModel.push(this.getGridRowModel());
            }
        }
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof Main6Base
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.emwo_innerid === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}