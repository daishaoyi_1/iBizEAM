/**
 * WPTree 部件模型
 *
 * @export
 * @class WPTreeModel
 */
export default class WPTreeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof WPTreeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'apprdate',
      },
      {
        name: 'adate',
      },
      {
        name: 'wplistids',
      },
      {
        name: 'm3q',
      },
      {
        name: 'need3q',
      },
      {
        name: 'emwplistname',
      },
      {
        name: 'description',
      },
      {
        name: 'hdate',
      },
      {
        name: 'equips',
      },
      {
        name: 'qcnt',
      },
      {
        name: 'createdate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'itemdesc',
      },
      {
        name: 'createman',
      },
      {
        name: 'emwplist',
        prop: 'emwplistid',
      },
      {
        name: 'enable',
      },
      {
        name: 'wplisttype',
      },
      {
        name: 'itemanditemdesc',
      },
      {
        name: 'costamount',
      },
      {
        name: 'useto',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'iscancel',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'lastdate',
      },
      {
        name: 'apprdesc',
      },
      {
        name: 'wpliststate',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'wplistdp',
      },
      {
        name: 'asum',
      },
      {
        name: 'wplistinfo',
      },
      {
        name: 'updateman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'pamount',
      },
      {
        name: 'deltype',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'no3q',
      },
      {
        name: 'itemcode',
      },
      {
        name: 'wplistcostname',
      },
      {
        name: 'unitname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'emservicename',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'itemname_show',
      },
      {
        name: 'avgprice',
      },
      {
        name: 'equipname',
      },
      {
        name: 'teamname',
      },
      {
        name: 'objname',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'unitid',
      },
      {
        name: 'teamid',
      },
      {
        name: 'objid',
      },
      {
        name: 'emserviceid',
      },
      {
        name: 'wplistcostid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'aempid',
      },
      {
        name: 'aempname',
      },
      {
        name: 'deptid',
      },
      {
        name: 'deptname',
      },
      {
        name: 'apprempid',
      },
      {
        name: 'apprempname',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
    ]
  }


}