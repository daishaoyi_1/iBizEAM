import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * WpProcessTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class WpProcessTreeExpViewtreeexpbarService
 */
export default class WpProcessTreeExpViewtreeexpbarService extends ControlService {
}