import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMWPListService from '@/service/emwplist/emwplist-service';
import Main7Service from './main7-form-service';
import EMWPListUIService from '@/uiservice/emwplist/emwplist-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main7EditFormBase}
 */
export class Main7EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main7EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main7Service}
     * @memberof Main7EditFormBase
     */
    public service: Main7Service = new Main7Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWPListService}
     * @memberof Main7EditFormBase
     */
    public appEntityService: EMWPListService = new EMWPListService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main7EditFormBase
     */
    protected appDeName: string = 'emwplist';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main7EditFormBase
     */
    protected appDeLogicName: string = '采购申请';

    /**
     * 界面UI服务对象
     *
     * @type {EMWPListUIService}
     * @memberof Main7Base
     */  
    public appUIService: EMWPListUIService = new EMWPListUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main7EditFormBase
     */
    protected formKeyItemName: string = 'emwplistid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main7EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        emwplistid: null,
        wplisttype: null,
        adate: null,
        useto: null,
        equipname: null,
        objname: null,
        equips: null,
        aempid: null,
        aempname: null,
        deptid: null,
        deptname: null,
        teamid: null,
        teamname: null,
        lastdate: null,
        hdate: null,
        need3q: null,
        wpliststate: null,
        m3q: null,
        itemname: null,
        rempid: null,
        rempname: null,
        asum: null,
        itemdesc: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        objid: null,
        itemid: null,
        equipid: null,
        emwplist: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main7EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main7EditFormBase
     */
    public rules(): any{
        return {
            wplisttype: [
                {
                    required: this.detailsModel.wplisttype.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.wplisttype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wplisttype.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.wplisttype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            adate: [
                {
                    required: this.detailsModel.adate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.adate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.adate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.adate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            useto: [
                {
                    required: this.detailsModel.useto.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.useto')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.useto.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.useto')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            equipname: [
                {
                    required: this.detailsModel.equipname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.equipname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.equipname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.equipname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            objname: [
                {
                    required: this.detailsModel.objname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.objname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.objname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.objname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            equips: [
                {
                    required: this.detailsModel.equips.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.equips')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.equips.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.equips')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            deptname: [
                {
                    required: this.detailsModel.deptname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.deptname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.deptname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.deptname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            wpliststate: [
                {
                    required: this.detailsModel.wpliststate.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplist.main7_form.details.wpliststate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wpliststate.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplist.main7_form.details.wpliststate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            m3q: [
                {
                    required: this.detailsModel.m3q.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplist.main7_form.details.m3q')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.m3q.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplist.main7_form.details.m3q')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            itemname: [
                {
                    required: this.detailsModel.itemname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.itemname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.itemname.required,
                    type: 'string',
                    message: `${this.$t('entities.emwplist.main7_form.details.itemname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            asum: [
                {
                    required: this.detailsModel.asum.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplist.main7_form.details.asum')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.asum.required,
                    type: 'number',
                    message: `${this.$t('entities.emwplist.main7_form.details.asum')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main7Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main7EditFormBase
     */
    public detailsModel: any = {
        grouppanel1: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwplist.main7_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '申请信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwplist.main7_form', extractMode: 'ITEM', details: [] } }),

        grouppanel18: new FormGroupPanelModel({ caption: '采购内容', detailType: 'GROUPPANEL', name: 'grouppanel18', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwplist.main7_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel27: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel27', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwplist.main7_form', extractMode: 'ITEM', details: [] } }),

        formpage26: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage26', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '采购申请号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '采购申请名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emwplistid: new FormItemModel({
    caption: '采购申请号(自动)', detailType: 'FORMITEM', name: 'emwplistid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        wplisttype: new FormItemModel({
    caption: '请购类型', detailType: 'FORMITEM', name: 'wplisttype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        adate: new FormItemModel({
    caption: '申请日期', detailType: 'FORMITEM', name: 'adate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        useto: new FormItemModel({
    caption: '用途', detailType: 'FORMITEM', name: 'useto', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: false, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: false, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        equips: new FormItemModel({
    caption: '设备集合', detailType: 'FORMITEM', name: 'equips', visible: false, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        aempid: new FormItemModel({
    caption: '申请人', detailType: 'FORMITEM', name: 'aempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        aempname: new FormItemModel({
    caption: '申请人', detailType: 'FORMITEM', name: 'aempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        deptid: new FormItemModel({
    caption: '申请部门', detailType: 'FORMITEM', name: 'deptid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        deptname: new FormItemModel({
    caption: '申请部门', detailType: 'FORMITEM', name: 'deptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        teamid: new FormItemModel({
    caption: '申请班组', detailType: 'FORMITEM', name: 'teamid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        teamname: new FormItemModel({
    caption: '申请班组', detailType: 'FORMITEM', name: 'teamname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        lastdate: new FormItemModel({
    caption: '上次请购时间', detailType: 'FORMITEM', name: 'lastdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        hdate: new FormItemModel({
    caption: '希望到货日期', detailType: 'FORMITEM', name: 'hdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        need3q: new FormItemModel({
    caption: '需要3次询价', detailType: 'FORMITEM', name: 'need3q', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpliststate: new FormItemModel({
    caption: '请购状态', detailType: 'FORMITEM', name: 'wpliststate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 0,
}),

        m3q: new FormItemModel({
    caption: '经理指定询价数', detailType: 'FORMITEM', name: 'm3q', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        itemname: new FormItemModel({
    caption: '物品', detailType: 'FORMITEM', name: 'itemname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '采购员', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '采购员', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        asum: new FormItemModel({
    caption: '请购数', detailType: 'FORMITEM', name: 'asum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        itemdesc: new FormItemModel({
    caption: '物品备注', detailType: 'FORMITEM', name: 'itemdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        objid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemid: new FormItemModel({
    caption: '物品', detailType: 'FORMITEM', name: 'itemid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage26',
                    index: 1,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof Main7EditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string; newVal: any; oldVal: any }): Promise<void> {
                


















        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = true;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'NOTEQ', 'EQUIP')) {
                ret = false;
            }
            this.detailsModel.equipname.required = ret;
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'EQUIP')) {
                ret = true;
            }
            this.detailsModel.equipname.setDisabled(!ret);
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'EQUIP')) {
                ret = true;
            }
            this.detailsModel.equipname.setVisible(ret);
        }

        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = true;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'NOTEQ', 'EQLOCATION') && this.$verify.testCond(_useto, 'NOTEQ', 'EQTYPE')) {
                ret = false;
            }
            this.detailsModel.objname.required = ret;
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'EQLOCATION') || this.$verify.testCond(_useto, 'EQ', 'EQTYPE')) {
                ret = true;
            }
            this.detailsModel.objname.setDisabled(!ret);
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'EQLOCATION') || this.$verify.testCond(_useto, 'EQ', 'EQTYPE')) {
                ret = true;
            }
            this.detailsModel.objname.setVisible(ret);
        }

        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = true;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'NOTEQ', 'MS')) {
                ret = false;
            }
            this.detailsModel.equips.required = ret;
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'MS')) {
                ret = true;
            }
            this.detailsModel.equips.setDisabled(!ret);
        }
        if (Object.is(name, '') || Object.is(name, 'useto')) {
            let ret = false;
            const _useto = this.data.useto;
            if (this.$verify.testCond(_useto, 'EQ', 'MS')) {
                ret = true;
            }
            this.detailsModel.equips.setVisible(ret);
        }


























        if (Object.is(name, 'aempname')) {
            const details: string[] = ['teamname', 'deptid', 'teamid', 'deptname'];
            this.updateFormItems('FormUpdateByAempid', this.data, details, true);
        }
        if (Object.is(name, 'itemname')) {
            const details: string[] = ['rempid', 'rempname'];
            this.updateFormItems('GetREMP', this.data, details, true);
        }
    }

    /**
     * 新建默认值
     * @memberof Main7EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('adate')) {
            this.data['adate'] = this.$util.dateFormat(new Date());
        }
        if (this.data.hasOwnProperty('aempid')) {
            this.data['aempid'] = this.context['srfuserid'];
        }
        if (this.data.hasOwnProperty('aempname')) {
            this.data['aempname'] = this.context['srfusername'];
        }
        if (this.data.hasOwnProperty('teamname')) {
            this.data['teamname'] = this.context['TEAMID'];
        }
        if (this.data.hasOwnProperty('wpliststate')) {
            this.data['wpliststate'] = 0;
        }
        if (this.data.hasOwnProperty('m3q')) {
            this.data['m3q'] = 0;
        }
        if (this.data.hasOwnProperty('orgid')) {
            this.data['orgid'] = 'TOP';
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main7Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}