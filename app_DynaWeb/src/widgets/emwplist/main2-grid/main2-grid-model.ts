/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emwplistid',
          prop: 'emwplistid',
          dataType: 'GUID',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'itemcode',
          prop: 'itemcode',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'asum',
          prop: 'asum',
          dataType: 'FLOAT',
        },
        {
          name: 'wplistdp',
          prop: 'wplistdp',
          dataType: 'TEXT',
        },
        {
          name: 'itemdesc',
          prop: 'itemdesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'wfstep',
          prop: 'wfstep',
          dataType: 'SSCODELIST',
        },
        {
          name: 'adate',
          prop: 'adate',
          dataType: 'DATETIME',
        },
        {
          name: 'lastdate',
          prop: 'lastdate',
          dataType: 'DATETIME',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'wpliststate',
          prop: 'wpliststate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'wplistcostname',
          prop: 'wplistcostname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'useto',
          prop: 'useto',
          dataType: 'SSCODELIST',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'teamname',
          prop: 'teamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'apprdate',
          prop: 'apprdate',
          dataType: 'DATETIME',
        },
        {
          name: 'hdate',
          prop: 'hdate',
          dataType: 'DATETIME',
        },
        {
          name: 'wplisttype',
          prop: 'wplisttype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'qcnt',
          prop: 'qcnt',
          dataType: 'INT',
        },
        {
          name: 'need3q',
          prop: 'need3q',
          dataType: 'YESNO',
        },
        {
          name: 'srfmajortext',
          prop: 'emwplistname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emwplistid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwplistid',
          dataType: 'GUID',
        },
        {
          name: 'deptid',
          prop: 'deptid',
          dataType: 'PICKUP',
        },
        {
          name: 'rempid',
          prop: 'rempid',
          dataType: 'PICKUP',
        },
        {
          name: 'apprempid',
          prop: 'apprempid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'teamid',
          prop: 'teamid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'wplistcostid',
          prop: 'wplistcostid',
          dataType: 'PICKUP',
        },
        {
          name: 'aempid',
          prop: 'aempid',
          dataType: 'PICKUP',
        },
        {
          name: 'emserviceid',
          prop: 'emserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'emwplist',
          prop: 'emwplistid',
        },
      {
        name: 'n_itemname_like',
        prop: 'n_itemname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_adate_gtandeq',
        prop: 'n_adate_gtandeq',
        dataType: 'DATETIME',
      },
      {
        name: 'n_adate_ltandeq',
        prop: 'n_adate_ltandeq',
        dataType: 'DATETIME',
      },
      {
        name: 'n_emservicename_eq',
        prop: 'n_emservicename_eq',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wpliststate_eq',
        prop: 'n_wpliststate_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_emserviceid_eq',
        prop: 'n_emserviceid_eq',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}