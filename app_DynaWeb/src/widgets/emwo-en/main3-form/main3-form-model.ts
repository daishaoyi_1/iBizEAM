/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwo_enid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwo_enname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emwo_enid',
        prop: 'emwo_enid',
        dataType: 'GUID',
      },
      {
        name: 'emwo_enname',
        prop: 'emwo_enname',
        dataType: 'TEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wodate',
        prop: 'wodate',
        dataType: 'DATETIME',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'dpname',
        prop: 'dpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wpersonid',
        prop: 'wpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'wpersonname',
        prop: 'wpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'bdate',
        prop: 'bdate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'lastval',
        prop: 'lastval',
        dataType: 'FLOAT',
      },
      {
        name: 'curval',
        prop: 'curval',
        dataType: 'FLOAT',
      },
      {
        name: 'vrate',
        prop: 'vrate',
        dataType: 'FLOAT',
      },
      {
        name: 'nval',
        prop: 'nval',
        dataType: 'FLOAT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptid',
        prop: 'rdeptid',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wogroup',
        prop: 'wogroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'wopname',
        prop: 'wopname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wooriname',
        prop: 'wooriname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wooritype',
        prop: 'wooritype',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'archive',
        prop: 'archive',
        dataType: 'SMCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'wooriid',
        prop: 'wooriid',
        dataType: 'PICKUP',
      },
      {
        name: 'dpid',
        prop: 'dpid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'wopid',
        prop: 'wopid',
        dataType: 'PICKUP',
      },
      {
        name: 'emwo_en',
        prop: 'emwo_enid',
        dataType: 'FONTKEY',
      },
    ]
  }

}