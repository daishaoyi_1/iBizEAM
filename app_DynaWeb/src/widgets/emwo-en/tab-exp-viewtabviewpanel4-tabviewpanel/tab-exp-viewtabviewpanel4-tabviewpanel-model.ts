/**
 * TabExpViewtabviewpanel4 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel4Model
 */
export default class TabExpViewtabviewpanel4Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel4Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'enable',
      },
      {
        name: 'woteam',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'regionbegindate',
      },
      {
        name: 'rempname',
      },
      {
        name: 'expiredate',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'prefee',
      },
      {
        name: 'updateman',
      },
      {
        name: 'content',
      },
      {
        name: 'rempid',
      },
      {
        name: 'priority',
      },
      {
        name: 'mdate',
      },
      {
        name: 'emwo_enname',
      },
      {
        name: 'val',
      },
      {
        name: 'regionenddate',
      },
      {
        name: 'wodate',
      },
      {
        name: 'createman',
      },
      {
        name: 'description',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'vrate',
      },
      {
        name: 'curval',
      },
      {
        name: 'wostate',
      },
      {
        name: 'worklength',
      },
      {
        name: 'wogroup',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'wpersonname',
      },
      {
        name: 'emwo_en',
        prop: 'emwo_enid',
      },
      {
        name: 'lastval',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'woteam_show',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'bdate',
      },
      {
        name: 'wpersonid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'wotype',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'wresult',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'nval',
      },
      {
        name: 'archive',
      },
      {
        name: 'wodesc',
      },
      {
        name: 'orgid',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'rfoacname',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'rfocaname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'dptype',
      },
      {
        name: 'objname',
      },
      {
        name: 'wopname_show',
      },
      {
        name: 'wopname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'dpname',
      },
      {
        name: 'wooriname',
      },
      {
        name: 'wooritype',
      },
      {
        name: 'equipname',
      },
      {
        name: 'dpid',
      },
      {
        name: 'objid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'wooriid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'rfomoid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'rfocaid',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'wopid',
      },
      {
        name: 'rfoacid',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
    ]
  }


}