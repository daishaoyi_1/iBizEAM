/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqkpid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqkpname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'kpcode',
        prop: 'kpcode',
        dataType: 'TEXT',
      },
      {
        name: 'emeqkpname',
        prop: 'emeqkpname',
        dataType: 'TEXT',
      },
      {
        name: 'kptypeid',
        prop: 'kptypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'normalrefval',
        prop: 'normalrefval',
        dataType: 'TEXT',
      },
      {
        name: 'kpscope',
        prop: 'kpscope',
        dataType: 'TEXT',
      },
      {
        name: 'kpdesc',
        prop: 'kpdesc',
        dataType: 'TEXT',
      },
      {
        name: 'emeqkpid',
        prop: 'emeqkpid',
        dataType: 'GUID',
      },
      {
        name: 'emeqkp',
        prop: 'emeqkpid',
        dataType: 'FONTKEY',
      },
    ]
  }

}