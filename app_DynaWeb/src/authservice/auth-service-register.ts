/**
 * 实体权限服务注册中心
 *
 * @export
 * @class AuthServiceRegister
 */
export class AuthServiceRegister {

    /**
     * 所有实体权限服务Map
     *
     * @protected
     * @type {*}
     * @memberof AuthServiceRegister
     */
    protected allAuthService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体权限服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof AuthServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of AuthServiceRegister.
     * @memberof AuthServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof AuthServiceRegister
     */
    protected init(): void {
                this.allAuthService.set('emobjmap', () => import('@/authservice/emobj-map/emobj-map-auth-service'));
        this.allAuthService.set('emeqwl', () => import('@/authservice/emeqwl/emeqwl-auth-service'));
        this.allAuthService.set('emresitem', () => import('@/authservice/emres-item/emres-item-auth-service'));
        this.allAuthService.set('emwork', () => import('@/authservice/emwork/emwork-auth-service'));
        this.allAuthService.set('emeqmpmtr', () => import('@/authservice/emeqmpmtr/emeqmpmtr-auth-service'));
        this.allAuthService.set('emeqlctmap', () => import('@/authservice/emeqlctmap/emeqlctmap-auth-service'));
        this.allAuthService.set('emeqmp', () => import('@/authservice/emeqmp/emeqmp-auth-service'));
        this.allAuthService.set('emeqkprcd', () => import('@/authservice/emeqkprcd/emeqkprcd-auth-service'));
        this.allAuthService.set('emdprct', () => import('@/authservice/emdprct/emdprct-auth-service'));
        this.allAuthService.set('emeqkpmap', () => import('@/authservice/emeqkpmap/emeqkpmap-auth-service'));
        this.allAuthService.set('emplan', () => import('@/authservice/emplan/emplan-auth-service'));
        this.allAuthService.set('emeqtype', () => import('@/authservice/emeqtype/emeqtype-auth-service'));
        this.allAuthService.set('emitem', () => import('@/authservice/emitem/emitem-auth-service'));
        this.allAuthService.set('emequip', () => import('@/authservice/emequip/emequip-auth-service'));
        this.allAuthService.set('emdrwgmap', () => import('@/authservice/emdrwgmap/emdrwgmap-auth-service'));
        this.allAuthService.set('emeqmonitor', () => import('@/authservice/emeqmonitor/emeqmonitor-auth-service'));
        this.allAuthService.set('emcab', () => import('@/authservice/emcab/emcab-auth-service'));
        this.allAuthService.set('emrfodemap', () => import('@/authservice/emrfodemap/emrfodemap-auth-service'));
        this.allAuthService.set('emeqlocation', () => import('@/authservice/emeqlocation/emeqlocation-auth-service'));
        this.allAuthService.set('emservice', () => import('@/authservice/emservice/emservice-auth-service'));
        this.allAuthService.set('emitemtype', () => import('@/authservice/emitem-type/emitem-type-auth-service'));
        this.allAuthService.set('emeqkp', () => import('@/authservice/emeqkp/emeqkp-auth-service'));
        this.allAuthService.set('emstorepart', () => import('@/authservice/emstore-part/emstore-part-auth-service'));
        this.allAuthService.set('empodetail', () => import('@/authservice/empodetail/empodetail-auth-service'));
        this.allAuthService.set('emobject', () => import('@/authservice/emobject/emobject-auth-service'));
        this.allAuthService.set('emapply', () => import('@/authservice/emapply/emapply-auth-service'));
        this.allAuthService.set('emeqlctgss', () => import('@/authservice/emeqlctgss/emeqlctgss-auth-service'));
        this.allAuthService.set('emwo_osc', () => import('@/authservice/emwo-osc/emwo-osc-auth-service'));
        this.allAuthService.set('emmachinecategory', () => import('@/authservice/emmachine-category/emmachine-category-auth-service'));
        this.allAuthService.set('emdrwg', () => import('@/authservice/emdrwg/emdrwg-auth-service'));
        this.allAuthService.set('emrfode', () => import('@/authservice/emrfode/emrfode-auth-service'));
        this.allAuthService.set('emstore', () => import('@/authservice/emstore/emstore-auth-service'));
        this.allAuthService.set('emeqdebug', () => import('@/authservice/emeqdebug/emeqdebug-auth-service'));
        this.allAuthService.set('emeqlctfdj', () => import('@/authservice/emeqlctfdj/emeqlctfdj-auth-service'));
        this.allAuthService.set('emwplistcost', () => import('@/authservice/emwplist-cost/emwplist-cost-auth-service'));
        this.allAuthService.set('pfteam', () => import('@/authservice/pfteam/pfteam-auth-service'));
        this.allAuthService.set('pfemppostmap', () => import('@/authservice/pfemp-post-map/pfemp-post-map-auth-service'));
        this.allAuthService.set('emacclass', () => import('@/authservice/emacclass/emacclass-auth-service'));
        this.allAuthService.set('pfcontract', () => import('@/authservice/pfcontract/pfcontract-auth-service'));
        this.allAuthService.set('emeitires', () => import('@/authservice/emeitires/emeitires-auth-service'));
        this.allAuthService.set('emoutput', () => import('@/authservice/emoutput/emoutput-auth-service'));
        this.allAuthService.set('emrfodetype', () => import('@/authservice/emrfodetype/emrfodetype-auth-service'));
        this.allAuthService.set('dynachart', () => import('@/authservice/dyna-chart/dyna-chart-auth-service'));
        this.allAuthService.set('empurplan', () => import('@/authservice/empur-plan/empur-plan-auth-service'));
        this.allAuthService.set('emproduct', () => import('@/authservice/emproduct/emproduct-auth-service'));
        this.allAuthService.set('embrand', () => import('@/authservice/embrand/embrand-auth-service'));
        this.allAuthService.set('pfdept', () => import('@/authservice/pfdept/pfdept-auth-service'));
        this.allAuthService.set('emeqsparedetail', () => import('@/authservice/emeqspare-detail/emeqspare-detail-auth-service'));
        this.allAuthService.set('pfemp', () => import('@/authservice/pfemp/pfemp-auth-service'));
        this.allAuthService.set('emstock', () => import('@/authservice/emstock/emstock-auth-service'));
        this.allAuthService.set('dynadashboard', () => import('@/authservice/dyna-dashboard/dyna-dashboard-auth-service'));
        this.allAuthService.set('emwoori', () => import('@/authservice/emwoori/emwoori-auth-service'));
        this.allAuthService.set('emplandetail', () => import('@/authservice/emplan-detail/emplan-detail-auth-service'));
        this.allAuthService.set('emplantempl', () => import('@/authservice/emplan-templ/emplan-templ-auth-service'));
        this.allAuthService.set('emwo_dp', () => import('@/authservice/emwo-dp/emwo-dp-auth-service'));
        this.allAuthService.set('emen', () => import('@/authservice/emen/emen-auth-service'));
        this.allAuthService.set('empo', () => import('@/authservice/empo/empo-auth-service'));
        this.allAuthService.set('pfunit', () => import('@/authservice/pfunit/pfunit-auth-service'));
        this.allAuthService.set('emitemprtn', () => import('@/authservice/emitem-prtn/emitem-prtn-auth-service'));
        this.allAuthService.set('emberth', () => import('@/authservice/emberth/emberth-auth-service'));
        this.allAuthService.set('emeqlcttires', () => import('@/authservice/emeqlcttires/emeqlcttires-auth-service'));
        this.allAuthService.set('emitemrout', () => import('@/authservice/emitem-rout/emitem-rout-auth-service'));
        this.allAuthService.set('emeqkeep', () => import('@/authservice/emeqkeep/emeqkeep-auth-service'));
        this.allAuthService.set('emitemrin', () => import('@/authservice/emitem-rin/emitem-rin-auth-service'));
        this.allAuthService.set('emeqmaintance', () => import('@/authservice/emeqmaintance/emeqmaintance-auth-service'));
        this.allAuthService.set('emitempuse', () => import('@/authservice/emitem-puse/emitem-puse-auth-service'));
        this.allAuthService.set('emeqsetup', () => import('@/authservice/emeqsetup/emeqsetup-auth-service'));
        this.allAuthService.set('emitemtrade', () => import('@/authservice/emitem-trade/emitem-trade-auth-service'));
        this.allAuthService.set('emwo_en', () => import('@/authservice/emwo-en/emwo-en-auth-service'));
        this.allAuthService.set('emrfoac', () => import('@/authservice/emrfoac/emrfoac-auth-service'));
        this.allAuthService.set('emassetclear', () => import('@/authservice/emasset-clear/emasset-clear-auth-service'));
        this.allAuthService.set('emmachmodel', () => import('@/authservice/emmach-model/emmach-model-auth-service'));
        this.allAuthService.set('emassetclass', () => import('@/authservice/emasset-class/emasset-class-auth-service'));
        this.allAuthService.set('emeqspare', () => import('@/authservice/emeqspare/emeqspare-auth-service'));
        this.allAuthService.set('emitempl', () => import('@/authservice/emitem-pl/emitem-pl-auth-service'));
        this.allAuthService.set('emeqah', () => import('@/authservice/emeqah/emeqah-auth-service'));
        this.allAuthService.set('emwo', () => import('@/authservice/emwo/emwo-auth-service'));
        this.allAuthService.set('emeqlctrhy', () => import('@/authservice/emeqlctrhy/emeqlctrhy-auth-service'));
        this.allAuthService.set('emitemcs', () => import('@/authservice/emitem-cs/emitem-cs-auth-service'));
        this.allAuthService.set('emenconsum', () => import('@/authservice/emenconsum/emenconsum-auth-service'));
        this.allAuthService.set('emserviceevl', () => import('@/authservice/emservice-evl/emservice-evl-auth-service'));
        this.allAuthService.set('emplancdt', () => import('@/authservice/emplan-cdt/emplan-cdt-auth-service'));
        this.allAuthService.set('emeqsparemap', () => import('@/authservice/emeqspare-map/emeqspare-map-auth-service'));
        this.allAuthService.set('emoutputrct', () => import('@/authservice/emoutput-rct/emoutput-rct-auth-service'));
        this.allAuthService.set('emrfoca', () => import('@/authservice/emrfoca/emrfoca-auth-service'));
        this.allAuthService.set('emwo_inner', () => import('@/authservice/emwo-inner/emwo-inner-auth-service'));
        this.allAuthService.set('emwplist', () => import('@/authservice/emwplist/emwplist-auth-service'));
        this.allAuthService.set('emrfomo', () => import('@/authservice/emrfomo/emrfomo-auth-service'));
        this.allAuthService.set('emasset', () => import('@/authservice/emasset/emasset-auth-service'));
        this.allAuthService.set('emeqcheck', () => import('@/authservice/emeqcheck/emeqcheck-auth-service'));
    }

    /**
     * 加载实体权限服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allAuthService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体权限服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const authService: any = await this.loadService(name);
        if (authService && authService.default) {
            const instance: any = new authService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const authServiceRegister: AuthServiceRegister = new AuthServiceRegister();