import EMPlanDetailAuthServiceBase from './emplan-detail-auth-service-base';


/**
 * 计划步骤权限服务对象
 *
 * @export
 * @class EMPlanDetailAuthService
 * @extends {EMPlanDetailAuthServiceBase}
 */
export default class EMPlanDetailAuthService extends EMPlanDetailAuthServiceBase {

    /**
     * Creates an instance of  EMPlanDetailAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanDetailAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}