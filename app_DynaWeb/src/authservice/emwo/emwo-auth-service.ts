import EMWOAuthServiceBase from './emwo-auth-service-base';


/**
 * 工单权限服务对象
 *
 * @export
 * @class EMWOAuthService
 * @extends {EMWOAuthServiceBase}
 */
export default class EMWOAuthService extends EMWOAuthServiceBase {

    /**
     * Creates an instance of  EMWOAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWOAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}