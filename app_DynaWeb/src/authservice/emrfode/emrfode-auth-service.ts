import EMRFODEAuthServiceBase from './emrfode-auth-service-base';


/**
 * 现象权限服务对象
 *
 * @export
 * @class EMRFODEAuthService
 * @extends {EMRFODEAuthServiceBase}
 */
export default class EMRFODEAuthService extends EMRFODEAuthServiceBase {

    /**
     * Creates an instance of  EMRFODEAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODEAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}