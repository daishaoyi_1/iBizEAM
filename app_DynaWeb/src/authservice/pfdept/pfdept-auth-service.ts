import PFDeptAuthServiceBase from './pfdept-auth-service-base';


/**
 * 部门权限服务对象
 *
 * @export
 * @class PFDeptAuthService
 * @extends {PFDeptAuthServiceBase}
 */
export default class PFDeptAuthService extends PFDeptAuthServiceBase {

    /**
     * Creates an instance of  PFDeptAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFDeptAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}