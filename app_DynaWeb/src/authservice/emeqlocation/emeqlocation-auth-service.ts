import EMEQLocationAuthServiceBase from './emeqlocation-auth-service-base';


/**
 * 位置权限服务对象
 *
 * @export
 * @class EMEQLocationAuthService
 * @extends {EMEQLocationAuthServiceBase}
 */
export default class EMEQLocationAuthService extends EMEQLocationAuthServiceBase {

    /**
     * Creates an instance of  EMEQLocationAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLocationAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}