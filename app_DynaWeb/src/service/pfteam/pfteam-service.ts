import { Http } from '@/utils';
import { Util } from '@/utils';
import PFTeamServiceBase from './pfteam-service-base';


/**
 * 班组服务对象
 *
 * @export
 * @class PFTeamService
 * @extends {PFTeamServiceBase}
 */
export default class PFTeamService extends PFTeamServiceBase {

    /**
     * Creates an instance of  PFTeamService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFTeamService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}