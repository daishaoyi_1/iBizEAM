import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 计划服务对象基类
 *
 * @export
 * @class EMPlanServiceBase
 * @extends {EntityServie}
 */
export default class EMPlanServiceBase extends EntityService {

    /**
     * Creates an instance of  EMPlanServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMPlanServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emplan';
        this.APPDEKEY = 'emplanid';
        this.APPDENAME = 'emplans';
        this.APPDETEXT = 'emplanname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan){
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan){
            let res:any = Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan){
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emplans/${context.emplan}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.emservice && context.emplantempl && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.emacclass && context.emplantempl && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.pfteam && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.emservice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.emplantempl && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        if(context.emacclass && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emplans`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emplancdts',JSON.stringify(res.data.emplancdts?res.data.emplancdts:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emplandetails',JSON.stringify(res.data.emplandetails?res.data.emplandetails:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplans/${context.emplan}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emplans/${context.emplan}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan){
            let res:any = Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.pfteam && context.emplan){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.emservice && context.emplan){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.emplantempl && context.emplan){
            let res:any = Http.getInstance().delete(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.emequip && context.emplan){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emplans/${context.emplan}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplan){
            let res:any = Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplans/${context.emplan}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emplans/${context.emplan}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan){
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.emservice && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.emacclass && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        if(context.emacclass && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplan) delete tempData.emplan;
            if(tempData.emplanid) delete tempData.emplanid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/getdraft`,tempData,isloading);
            res.data.emplan = data.emplan;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emplan) delete tempData.emplan;
        if(tempData.emplanid) delete tempData.emplanid;
        let res:any = await  Http.getInstance().get(`/emplans/getdraft`,tempData,isloading);
        res.data.emplan = data.emplan;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emplans/${context.emplan}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emplans/${context.emplan}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emplans/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.emplantempl && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emplans/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emplans/searchdefault`,tempData,isloading);
    }
}