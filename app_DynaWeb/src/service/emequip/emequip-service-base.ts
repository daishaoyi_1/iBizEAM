import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 设备档案服务对象基类
 *
 * @export
 * @class EMEquipServiceBase
 * @extends {EntityServie}
 */
export default class EMEquipServiceBase extends EntityService {

    /**
     * Creates an instance of  EMEquipServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEquipServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMEquipServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emequip';
        this.APPDEKEY = 'emequipid';
        this.APPDENAME = 'emequips';
        this.APPDETEXT = 'emequipname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emequips/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emequips/searchdefault`,tempData,isloading);
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emapplies',JSON.stringify(res.data.emapplies?res.data.emapplies:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emenconsums',JSON.stringify(res.data.emenconsums?res.data.emenconsums:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqahs',JSON.stringify(res.data.emeqahs?res.data.emeqahs:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqchecks',JSON.stringify(res.data.emeqchecks?res.data.emeqchecks:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqdebugs',JSON.stringify(res.data.emeqdebugs?res.data.emeqdebugs:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqkeeps',JSON.stringify(res.data.emeqkeeps?res.data.emeqkeeps:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqmaintances',JSON.stringify(res.data.emeqmaintances?res.data.emeqmaintances:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqsetups',JSON.stringify(res.data.emeqsetups?res.data.emeqsetups:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqwls',JSON.stringify(res.data.emeqwls?res.data.emeqwls:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emitempuses',JSON.stringify(res.data.emitempuses?res.data.emitempuses:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emplans',JSON.stringify(res.data.emplans?res.data.emplans:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_dps',JSON.stringify(res.data.emwo_dps?res.data.emwo_dps:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_ens',JSON.stringify(res.data.emwo_ens?res.data.emwo_ens:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_inners',JSON.stringify(res.data.emwo_inners?res.data.emwo_inners:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_oscs',JSON.stringify(res.data.emwo_oscs?res.data.emwo_oscs:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emwos',JSON.stringify(res.data.emwos?res.data.emwos:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emequips`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emapplies',JSON.stringify(res.data.emapplies?res.data.emapplies:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emenconsums',JSON.stringify(res.data.emenconsums?res.data.emenconsums:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqahs',JSON.stringify(res.data.emeqahs?res.data.emeqahs:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqchecks',JSON.stringify(res.data.emeqchecks?res.data.emeqchecks:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqdebugs',JSON.stringify(res.data.emeqdebugs?res.data.emeqdebugs:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqkeeps',JSON.stringify(res.data.emeqkeeps?res.data.emeqkeeps:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqmaintances',JSON.stringify(res.data.emeqmaintances?res.data.emeqmaintances:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqsetups',JSON.stringify(res.data.emeqsetups?res.data.emeqsetups:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emeqwls',JSON.stringify(res.data.emeqwls?res.data.emeqwls:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emitempuses',JSON.stringify(res.data.emitempuses?res.data.emitempuses:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emplans',JSON.stringify(res.data.emplans?res.data.emplans:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_dps',JSON.stringify(res.data.emwo_dps?res.data.emwo_dps:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_ens',JSON.stringify(res.data.emwo_ens?res.data.emwo_ens:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_inners',JSON.stringify(res.data.emwo_inners?res.data.emwo_inners:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emwo_oscs',JSON.stringify(res.data.emwo_oscs?res.data.emwo_oscs:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emwos',JSON.stringify(res.data.emwos?res.data.emwos:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emequips/${context.emequip}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emequip) delete tempData.emequip;
            if(tempData.emequipid) delete tempData.emequipid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/getdraft`,tempData,isloading);
            res.data.emequip = data.emequip;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emequip) delete tempData.emequip;
        if(tempData.emequipid) delete tempData.emequipid;
        let res:any = await  Http.getInstance().get(`/emequips/getdraft`,tempData,isloading);
        res.data.emequip = data.emequip;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emequips/${context.emequip}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emequips/${context.emequip}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchEQTypeTree接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async FetchEQTypeTree(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/fetcheqtypetree`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emequips/fetcheqtypetree`,tempData,isloading);
        return res;
    }

    /**
     * searchEQTypeTree接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async searchEQTypeTree(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/searcheqtypetree`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emequips/searcheqtypetree`,tempData,isloading);
    }

    /**
     * FetchTypeEQNum接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async FetchTypeEQNum(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/fetchtypeeqnum`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emequips/fetchtypeeqnum`,tempData,isloading);
        return res;
    }

    /**
     * searchTypeEQNum接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEquipServiceBase
     */
    public async searchTypeEQNum(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/searchtypeeqnum`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emequips/searchtypeeqnum`,tempData,isloading);
    }
}