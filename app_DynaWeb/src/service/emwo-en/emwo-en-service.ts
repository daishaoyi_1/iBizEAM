import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWO_ENServiceBase from './emwo-en-service-base';


/**
 * 能耗登记工单服务对象
 *
 * @export
 * @class EMWO_ENService
 * @extends {EMWO_ENServiceBase}
 */
export default class EMWO_ENService extends EMWO_ENServiceBase {

    /**
     * Creates an instance of  EMWO_ENService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_ENService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}