/**
 * 实体数据服务注册中心
 *
 * @export
 * @class EntityServiceRegister
 */
export class EntityServiceRegister {

    /**
     * 所有实体数据服务Map
     *
     * @protected
     * @type {*}
     * @memberof EntityServiceRegister
     */
    protected allEntityService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体数据服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof EntityServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of EntityServiceRegister.
     * @memberof EntityServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof EntityServiceRegister
     */
    protected init(): void {
                this.allEntityService.set('emobjmap', () => import('@/service/emobj-map/emobj-map-service'));
        this.allEntityService.set('emeqwl', () => import('@/service/emeqwl/emeqwl-service'));
        this.allEntityService.set('emresitem', () => import('@/service/emres-item/emres-item-service'));
        this.allEntityService.set('emwork', () => import('@/service/emwork/emwork-service'));
        this.allEntityService.set('emeqmpmtr', () => import('@/service/emeqmpmtr/emeqmpmtr-service'));
        this.allEntityService.set('emeqlctmap', () => import('@/service/emeqlctmap/emeqlctmap-service'));
        this.allEntityService.set('emeqmp', () => import('@/service/emeqmp/emeqmp-service'));
        this.allEntityService.set('emeqkprcd', () => import('@/service/emeqkprcd/emeqkprcd-service'));
        this.allEntityService.set('emdprct', () => import('@/service/emdprct/emdprct-service'));
        this.allEntityService.set('emeqkpmap', () => import('@/service/emeqkpmap/emeqkpmap-service'));
        this.allEntityService.set('emplan', () => import('@/service/emplan/emplan-service'));
        this.allEntityService.set('emeqtype', () => import('@/service/emeqtype/emeqtype-service'));
        this.allEntityService.set('emitem', () => import('@/service/emitem/emitem-service'));
        this.allEntityService.set('emequip', () => import('@/service/emequip/emequip-service'));
        this.allEntityService.set('emdrwgmap', () => import('@/service/emdrwgmap/emdrwgmap-service'));
        this.allEntityService.set('emeqmonitor', () => import('@/service/emeqmonitor/emeqmonitor-service'));
        this.allEntityService.set('emcab', () => import('@/service/emcab/emcab-service'));
        this.allEntityService.set('emrfodemap', () => import('@/service/emrfodemap/emrfodemap-service'));
        this.allEntityService.set('emeqlocation', () => import('@/service/emeqlocation/emeqlocation-service'));
        this.allEntityService.set('emservice', () => import('@/service/emservice/emservice-service'));
        this.allEntityService.set('emitemtype', () => import('@/service/emitem-type/emitem-type-service'));
        this.allEntityService.set('emeqkp', () => import('@/service/emeqkp/emeqkp-service'));
        this.allEntityService.set('emstorepart', () => import('@/service/emstore-part/emstore-part-service'));
        this.allEntityService.set('empodetail', () => import('@/service/empodetail/empodetail-service'));
        this.allEntityService.set('emobject', () => import('@/service/emobject/emobject-service'));
        this.allEntityService.set('emapply', () => import('@/service/emapply/emapply-service'));
        this.allEntityService.set('emeqlctgss', () => import('@/service/emeqlctgss/emeqlctgss-service'));
        this.allEntityService.set('emwo_osc', () => import('@/service/emwo-osc/emwo-osc-service'));
        this.allEntityService.set('emmachinecategory', () => import('@/service/emmachine-category/emmachine-category-service'));
        this.allEntityService.set('emdrwg', () => import('@/service/emdrwg/emdrwg-service'));
        this.allEntityService.set('emrfode', () => import('@/service/emrfode/emrfode-service'));
        this.allEntityService.set('emstore', () => import('@/service/emstore/emstore-service'));
        this.allEntityService.set('emeqdebug', () => import('@/service/emeqdebug/emeqdebug-service'));
        this.allEntityService.set('emeqlctfdj', () => import('@/service/emeqlctfdj/emeqlctfdj-service'));
        this.allEntityService.set('emwplistcost', () => import('@/service/emwplist-cost/emwplist-cost-service'));
        this.allEntityService.set('pfteam', () => import('@/service/pfteam/pfteam-service'));
        this.allEntityService.set('pfemppostmap', () => import('@/service/pfemp-post-map/pfemp-post-map-service'));
        this.allEntityService.set('emacclass', () => import('@/service/emacclass/emacclass-service'));
        this.allEntityService.set('pfcontract', () => import('@/service/pfcontract/pfcontract-service'));
        this.allEntityService.set('emeitires', () => import('@/service/emeitires/emeitires-service'));
        this.allEntityService.set('emoutput', () => import('@/service/emoutput/emoutput-service'));
        this.allEntityService.set('emrfodetype', () => import('@/service/emrfodetype/emrfodetype-service'));
        this.allEntityService.set('dynachart', () => import('@/service/dyna-chart/dyna-chart-service'));
        this.allEntityService.set('empurplan', () => import('@/service/empur-plan/empur-plan-service'));
        this.allEntityService.set('emproduct', () => import('@/service/emproduct/emproduct-service'));
        this.allEntityService.set('embrand', () => import('@/service/embrand/embrand-service'));
        this.allEntityService.set('pfdept', () => import('@/service/pfdept/pfdept-service'));
        this.allEntityService.set('emeqsparedetail', () => import('@/service/emeqspare-detail/emeqspare-detail-service'));
        this.allEntityService.set('pfemp', () => import('@/service/pfemp/pfemp-service'));
        this.allEntityService.set('emstock', () => import('@/service/emstock/emstock-service'));
        this.allEntityService.set('dynadashboard', () => import('@/service/dyna-dashboard/dyna-dashboard-service'));
        this.allEntityService.set('emwoori', () => import('@/service/emwoori/emwoori-service'));
        this.allEntityService.set('emplandetail', () => import('@/service/emplan-detail/emplan-detail-service'));
        this.allEntityService.set('emplantempl', () => import('@/service/emplan-templ/emplan-templ-service'));
        this.allEntityService.set('emwo_dp', () => import('@/service/emwo-dp/emwo-dp-service'));
        this.allEntityService.set('emen', () => import('@/service/emen/emen-service'));
        this.allEntityService.set('empo', () => import('@/service/empo/empo-service'));
        this.allEntityService.set('pfunit', () => import('@/service/pfunit/pfunit-service'));
        this.allEntityService.set('emitemprtn', () => import('@/service/emitem-prtn/emitem-prtn-service'));
        this.allEntityService.set('emberth', () => import('@/service/emberth/emberth-service'));
        this.allEntityService.set('emeqlcttires', () => import('@/service/emeqlcttires/emeqlcttires-service'));
        this.allEntityService.set('emitemrout', () => import('@/service/emitem-rout/emitem-rout-service'));
        this.allEntityService.set('emeqkeep', () => import('@/service/emeqkeep/emeqkeep-service'));
        this.allEntityService.set('emitemrin', () => import('@/service/emitem-rin/emitem-rin-service'));
        this.allEntityService.set('emeqmaintance', () => import('@/service/emeqmaintance/emeqmaintance-service'));
        this.allEntityService.set('emitempuse', () => import('@/service/emitem-puse/emitem-puse-service'));
        this.allEntityService.set('emeqsetup', () => import('@/service/emeqsetup/emeqsetup-service'));
        this.allEntityService.set('emitemtrade', () => import('@/service/emitem-trade/emitem-trade-service'));
        this.allEntityService.set('emwo_en', () => import('@/service/emwo-en/emwo-en-service'));
        this.allEntityService.set('emrfoac', () => import('@/service/emrfoac/emrfoac-service'));
        this.allEntityService.set('emassetclear', () => import('@/service/emasset-clear/emasset-clear-service'));
        this.allEntityService.set('emmachmodel', () => import('@/service/emmach-model/emmach-model-service'));
        this.allEntityService.set('emassetclass', () => import('@/service/emasset-class/emasset-class-service'));
        this.allEntityService.set('emeqspare', () => import('@/service/emeqspare/emeqspare-service'));
        this.allEntityService.set('emitempl', () => import('@/service/emitem-pl/emitem-pl-service'));
        this.allEntityService.set('emeqah', () => import('@/service/emeqah/emeqah-service'));
        this.allEntityService.set('emwo', () => import('@/service/emwo/emwo-service'));
        this.allEntityService.set('emeqlctrhy', () => import('@/service/emeqlctrhy/emeqlctrhy-service'));
        this.allEntityService.set('emitemcs', () => import('@/service/emitem-cs/emitem-cs-service'));
        this.allEntityService.set('emenconsum', () => import('@/service/emenconsum/emenconsum-service'));
        this.allEntityService.set('emserviceevl', () => import('@/service/emservice-evl/emservice-evl-service'));
        this.allEntityService.set('emplancdt', () => import('@/service/emplan-cdt/emplan-cdt-service'));
        this.allEntityService.set('emeqsparemap', () => import('@/service/emeqspare-map/emeqspare-map-service'));
        this.allEntityService.set('emoutputrct', () => import('@/service/emoutput-rct/emoutput-rct-service'));
        this.allEntityService.set('emrfoca', () => import('@/service/emrfoca/emrfoca-service'));
        this.allEntityService.set('emwo_inner', () => import('@/service/emwo-inner/emwo-inner-service'));
        this.allEntityService.set('emwplist', () => import('@/service/emwplist/emwplist-service'));
        this.allEntityService.set('emrfomo', () => import('@/service/emrfomo/emrfomo-service'));
        this.allEntityService.set('emasset', () => import('@/service/emasset/emasset-service'));
        this.allEntityService.set('emeqcheck', () => import('@/service/emeqcheck/emeqcheck-service'));
    }

    /**
     * 加载实体数据服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allEntityService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体数据服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const entityServiceRegister: EntityServiceRegister = new EntityServiceRegister();