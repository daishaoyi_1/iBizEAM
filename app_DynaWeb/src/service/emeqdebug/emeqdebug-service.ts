import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQDebugServiceBase from './emeqdebug-service-base';


/**
 * 事故记录服务对象
 *
 * @export
 * @class EMEQDebugService
 * @extends {EMEQDebugServiceBase}
 */
export default class EMEQDebugService extends EMEQDebugServiceBase {

    /**
     * Creates an instance of  EMEQDebugService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQDebugService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}