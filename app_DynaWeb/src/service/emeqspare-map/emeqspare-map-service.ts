import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQSpareMapServiceBase from './emeqspare-map-service-base';


/**
 * 备件包引用服务对象
 *
 * @export
 * @class EMEQSpareMapService
 * @extends {EMEQSpareMapServiceBase}
 */
export default class EMEQSpareMapService extends EMEQSpareMapServiceBase {

    /**
     * Creates an instance of  EMEQSpareMapService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareMapService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}