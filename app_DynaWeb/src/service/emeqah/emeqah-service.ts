import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQAHServiceBase from './emeqah-service-base';


/**
 * 活动历史服务对象
 *
 * @export
 * @class EMEQAHService
 * @extends {EMEQAHServiceBase}
 */
export default class EMEQAHService extends EMEQAHServiceBase {

    /**
     * Creates an instance of  EMEQAHService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQAHService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}