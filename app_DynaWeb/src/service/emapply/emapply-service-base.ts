import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 外委申请服务对象基类
 *
 * @export
 * @class EMApplyServiceBase
 * @extends {EntityServie}
 */
export default class EMApplyServiceBase extends EntityService {

    /**
     * Creates an instance of  EMApplyServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMApplyServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMApplyServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emapply';
        this.APPDEKEY = 'emapplyid';
        this.APPDENAME = 'emapplies';
        this.APPDETEXT = 'emapplyname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emapplies/${context.emapply}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emapplies/${context.emapply}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies`,data,isloading);
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emapplies`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emapplies/${context.emapply}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emapplies/${context.emapply}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}`,isloading);
            return res;
        }
        if(context.emequip && context.emapply){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emapplies/${context.emapply}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emapplies/${context.emapply}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emapplies/${context.emapply}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emapplies/${context.emapply}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emapply) delete tempData.emapply;
            if(tempData.emapplyid) delete tempData.emapplyid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/getdraft`,tempData,isloading);
            res.data.emapply = data.emapply;
            
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emapply) delete tempData.emapply;
            if(tempData.emapplyid) delete tempData.emapplyid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emapplies/getdraft`,tempData,isloading);
            res.data.emapply = data.emapply;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emapply) delete tempData.emapply;
        if(tempData.emapplyid) delete tempData.emapplyid;
        let res:any = await  Http.getInstance().get(`/emapplies/getdraft`,tempData,isloading);
        res.data.emapply = data.emapply;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/${context.emapply}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emapplies/${context.emapply}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CheckValue接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async CheckValue(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/checkvalue`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/${context.emapply}/checkvalue`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emapplies/${context.emapply}/checkvalue`,data,isloading);
            return res;
    }

    /**
     * Confirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Confirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/confirm`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/${context.emapply}/confirm`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emapplies/${context.emapply}/confirm`,data,isloading);
            return res;
    }

    /**
     * FormUpdateByEmquipId接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async FormUpdateByEmquipId(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/formupdatebyemquipid`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emapplies/${context.emapply}/formupdatebyemquipid`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/emapplies/${context.emapply}/formupdatebyemquipid`,data,isloading);
            return res;
    }

    /**
     * GenId接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async GenId(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/genid`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/${context.emapply}/genid`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emapplies/${context.emapply}/genid`,data,isloading);
            return res;
    }

    /**
     * Rejected接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Rejected(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/rejected`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/${context.emapply}/rejected`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emapplies/${context.emapply}/rejected`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/${context.emapply}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emapplies/${context.emapply}/save`,data,isloading);
            
            return res;
    }

    /**
     * Submit接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async Submit(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/${context.emapply}/submit`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emapply){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/${context.emapply}/submit`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emapplies/${context.emapply}/submit`,data,isloading);
            return res;
    }

    /**
     * FetchConfirmed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async FetchConfirmed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/fetchconfirmed`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emapplies/fetchconfirmed`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emapplies/fetchconfirmed`,tempData,isloading);
        return res;
    }

    /**
     * searchConfirmed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async searchConfirmed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/searchconfirmed`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/searchconfirmed`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emapplies/searchconfirmed`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emapplies/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emapplies/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/searchdefault`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emapplies/searchdefault`,tempData,isloading);
    }

    /**
     * FetchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async FetchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emapplies/fetchdraft`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emapplies/fetchdraft`,tempData,isloading);
        return res;
    }

    /**
     * searchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async searchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/searchdraft`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/searchdraft`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emapplies/searchdraft`,tempData,isloading);
    }

    /**
     * FetchToConfirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async FetchToConfirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emapplies/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emapplies/fetchtoconfirm`,tempData,isloading);
        return res;
    }

    /**
     * searchToConfirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMApplyServiceBase
     */
    public async searchToConfirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emapplies/searchtoconfirm`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emapplies/searchtoconfirm`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emapplies/searchtoconfirm`,tempData,isloading);
    }
}