import EMWPListService from '@/service/emwplist/emwplist-service';
import { Verify } from '@/utils/verify/verify';


/**
 * FillItem
 *
 * @export
 * @class FillItemLogicBase
 */
export default class FillItemLogicBase {

    /**
     * 名称
     * 
     * @memberof  FillItemLogicBase
     */
    private name:string ="FillItem";

    /**
     * 唯一标识
     * 
     * @memberof  FillItemLogicBase
     */
    private id:string = "2e25b3c06629d5d06df38cb48d7cb944";

    /**
     * 默认参数名称
     * 
     * @memberof  FillItemLogicBase
     */
    private defaultParamName:string = "Default";

    /**
     * 参数集合
     * 
     * @memberof  FillItemLogicBase
     */
    private paramsMap:Map<string,any> = new Map();

    /**
     * Creates an instance of  FillItemLogicBase.
     * 
     * @param {*} [opts={}]
     * @memberof  FillItemLogicBase
     */
    constructor(opts: any = {}) {
        this.initParams(opts);
    }

    /**
     * 初始化参数集合
     * 
     * @param {*} [opts={}]
     * @memberof  FillItemLogicBase
     */
    public initParams(opts:any){
        this.paramsMap.set('Default',opts);
        this.paramsMap.set('wplist',{});
    }


    /**
     * 计算0节点结果
     * 
     * @param params 传入参数
     */
    public compute0Cond(params:any):boolean{
        return true;
    }

    /**
     * 计算1节点结果
     * 
     * @param params 传入参数
     */
    public compute1Cond(params:any):boolean{
        return true;
    }

    /**
     * 计算2节点结果
     * 
     * @param params 传入参数
     */
    public compute2Cond(params:any):boolean{
        return true;
    }

    /**
     * 执行逻辑
     * 
     * @param context 应用上下文
     * @param params 传入参数
     */
    public onExecute(context:any,params:any,isloading:boolean){
        return this.executeBegin(context,params,isloading);
    }


    /**
    * 获取采购申请信息
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executeDeaction1(context:any,params:any,isloading:boolean){
        // 行为处理节点
        let result: any;
        let actionParam:any = this.paramsMap.get('wplist');
        const targetService:EMWPListService = new EMWPListService();
        if (targetService['Get'] && targetService['Get'] instanceof Function) {
            result = await targetService['Get'](actionParam.context,actionParam.data, false);
        }
        if(result && result.status == 200){
            Object.assign(actionParam.data,result.data);
        if(this.compute1Cond(params)){
            return this.executePrepareparam2(context,params,isloading);   
        }
        }
    }

    /**
    * 开始
    * 
    * @param params 传入参数
    */
    private async executeBegin(context:any,params:any,isloading:boolean){
        //开始节点
        if(this.compute0Cond(params)){
            return this.executePrepareparam1(context,params,isloading);   
        }
    }

    /**
    * 采购申请参数
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executePrepareparam1(context:any,params:any,isloading:boolean){
        // 准备参数节点
    let tempDstParam0Context:any = this.paramsMap.get('wplist').context?this.paramsMap.get('wplist').context:{};
    let tempDstParam0Data:any = this.paramsMap.get('wplist').data?this.paramsMap.get('wplist').data:{};
    let tempSrcParam0Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    Object.assign(tempDstParam0Context,{emwplist:tempSrcParam0Data['emwplistid']});
    Object.assign(tempDstParam0Data,{emwplistid:tempSrcParam0Data['emwplistid']});
    this.paramsMap.set('wplist',{data:tempDstParam0Data,context:tempDstParam0Context});
        if(this.compute2Cond(params)){
            return this.executeDeaction1(context,params,isloading);   
        }
    }

    /**
    * 填充物品信息
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executePrepareparam2(context:any,params:any,isloading:boolean){
        // 准备参数节点
    let tempDstParam0Context:any = this.paramsMap.get('Default').context?this.paramsMap.get('Default').context:{};
    let tempDstParam0Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    let tempSrcParam0Data:any = this.paramsMap.get('wplist').data?this.paramsMap.get('wplist').data:{};
    Object.assign(tempDstParam0Data,{itemdesc:tempSrcParam0Data['itemdesc']});
    this.paramsMap.set('Default',{data:tempDstParam0Data,context:tempDstParam0Context});
    let tempDstParam1Context:any = this.paramsMap.get('Default').context?this.paramsMap.get('Default').context:{};
    let tempDstParam1Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    let tempSrcParam1Data:any = this.paramsMap.get('wplist').data?this.paramsMap.get('wplist').data:{};
    Object.assign(tempDstParam1Data,{itemid:tempSrcParam1Data['itemid']});
    this.paramsMap.set('Default',{data:tempDstParam1Data,context:tempDstParam1Context});
    let tempDstParam2Context:any = this.paramsMap.get('Default').context?this.paramsMap.get('Default').context:{};
    let tempDstParam2Data:any = this.paramsMap.get('Default').data?this.paramsMap.get('Default').data:{};
    let tempSrcParam2Data:any = this.paramsMap.get('wplist').data?this.paramsMap.get('wplist').data:{};
    Object.assign(tempDstParam2Data,{itemname:tempSrcParam2Data['itemname']});
    this.paramsMap.set('Default',{data:tempDstParam2Data,context:tempDstParam2Context});
        return this.paramsMap.get(this.defaultParamName).data;
    }


}