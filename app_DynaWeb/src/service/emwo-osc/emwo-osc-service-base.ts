import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 外委保养工单服务对象基类
 *
 * @export
 * @class EMWO_OSCServiceBase
 * @extends {EntityServie}
 */
export default class EMWO_OSCServiceBase extends EntityService {

    /**
     * Creates an instance of  EMWO_OSCServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_OSCServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMWO_OSCServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emwo_osc';
        this.APPDEKEY = 'emwo_oscid';
        this.APPDENAME = 'emwo_oscs';
        this.APPDETEXT = 'emwo_oscname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emwo_oscs/${context.emwo_osc}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs`,data,isloading);
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emwo_oscs`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emwo_oscs/${context.emwo_osc}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}`,isloading);
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emwo_oscs/${context.emwo_osc}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emwo_oscs/${context.emwo_osc}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwo_osc) delete tempData.emwo_osc;
            if(tempData.emwo_oscid) delete tempData.emwo_oscid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/getdraft`,tempData,isloading);
            res.data.emwo_osc = data.emwo_osc;
            
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emwo_osc) delete tempData.emwo_osc;
            if(tempData.emwo_oscid) delete tempData.emwo_oscid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/getdraft`,tempData,isloading);
            res.data.emwo_osc = data.emwo_osc;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emwo_osc) delete tempData.emwo_osc;
        if(tempData.emwo_oscid) delete tempData.emwo_oscid;
        let res:any = await  Http.getInstance().get(`/emwo_oscs/getdraft`,tempData,isloading);
        res.data.emwo_osc = data.emwo_osc;
        
        return res;
    }

    /**
     * Acceptance接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Acceptance(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/acceptance`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/acceptance`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwo_oscs/${context.emwo_osc}/acceptance`,data,isloading);
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwo_oscs/${context.emwo_osc}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CheckValue接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async CheckValue(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/checkvalue`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/checkvalue`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwo_oscs/${context.emwo_osc}/checkvalue`,data,isloading);
            return res;
    }

    /**
     * FormUpdateByEmequipid接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async FormUpdateByEmequipid(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/formupdatebyemequipid`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/formupdatebyemequipid`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/emwo_oscs/${context.emwo_osc}/formupdatebyemequipid`,data,isloading);
            return res;
    }

    /**
     * GenId接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async GenId(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/genid`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/genid`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwo_oscs/${context.emwo_osc}/genid`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emwo_oscs/${context.emwo_osc}/save`,data,isloading);
            
            return res;
    }

    /**
     * Submit接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async Submit(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/submit`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/submit`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwo_oscs/${context.emwo_osc}/submit`,data,isloading);
            return res;
    }

    /**
     * UnAcceptance接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async UnAcceptance(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/unacceptance`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_osc){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/${context.emwo_osc}/unacceptance`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwo_oscs/${context.emwo_osc}/unacceptance`,data,isloading);
            return res;
    }

    /**
     * FetchCalendar接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async FetchCalendar(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/fetchcalendar`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/fetchcalendar`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwo_oscs/fetchcalendar`,tempData,isloading);
        return res;
    }

    /**
     * searchCalendar接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async searchCalendar(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/searchcalendar`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/searchcalendar`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emwo_oscs/searchcalendar`,tempData,isloading);
    }

    /**
     * FetchConfirmed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async FetchConfirmed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/fetchconfirmed`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/fetchconfirmed`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwo_oscs/fetchconfirmed`,tempData,isloading);
        return res;
    }

    /**
     * searchConfirmed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async searchConfirmed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/searchconfirmed`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/searchconfirmed`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emwo_oscs/searchconfirmed`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwo_oscs/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/searchdefault`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emwo_oscs/searchdefault`,tempData,isloading);
    }

    /**
     * FetchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async FetchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/fetchdraft`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/fetchdraft`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwo_oscs/fetchdraft`,tempData,isloading);
        return res;
    }

    /**
     * searchDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async searchDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/searchdraft`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/searchdraft`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emwo_oscs/searchdraft`,tempData,isloading);
    }

    /**
     * FetchToConfirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async FetchToConfirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_oscs/fetchtoconfirm`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwo_oscs/fetchtoconfirm`,tempData,isloading);
        return res;
    }

    /**
     * searchToConfirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_OSCServiceBase
     */
    public async searchToConfirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_oscs/searchtoconfirm`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emwo_oscs/searchtoconfirm`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emwo_oscs/searchtoconfirm`,tempData,isloading);
    }
}