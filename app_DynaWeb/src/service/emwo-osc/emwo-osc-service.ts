import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWO_OSCServiceBase from './emwo-osc-service-base';


/**
 * 外委保养工单服务对象
 *
 * @export
 * @class EMWO_OSCService
 * @extends {EMWO_OSCServiceBase}
 */
export default class EMWO_OSCService extends EMWO_OSCServiceBase {

    /**
     * Creates an instance of  EMWO_OSCService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_OSCService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}