import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 计划条件服务对象基类
 *
 * @export
 * @class EMPlanCDTServiceBase
 * @extends {EntityServie}
 */
export default class EMPlanCDTServiceBase extends EntityService {

    /**
     * Creates an instance of  EMPlanCDTServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanCDTServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMPlanCDTServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emplancdt';
        this.APPDEKEY = 'emplancdtid';
        this.APPDENAME = 'emplancdts';
        this.APPDETEXT = 'emplancdtname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
        if(context.emplan && context.emplancdt){
            let res:any = Http.getInstance().get(`/emplans/${context.emplan}/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emplancdts/${context.emplancdt}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        if(context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/emplancdts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emplancdts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emplancdts/${context.emplancdt}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.pfteam && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.emservice && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.emequip && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
        if(context.emplan && context.emplancdt){
            let res:any = Http.getInstance().delete(`/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emplancdts/${context.emplancdt}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
        if(context.emplan && context.emplancdt){
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emplancdts/${context.emplancdt}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplancdt) delete tempData.emplancdt;
            if(tempData.emplancdtid) delete tempData.emplancdtid;
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/emplancdts/getdraft`,tempData,isloading);
            res.data.emplancdt = data.emplancdt;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emplancdt) delete tempData.emplancdt;
        if(tempData.emplancdtid) delete tempData.emplancdtid;
        let res:any = await  Http.getInstance().get(`/emplancdts/getdraft`,tempData,isloading);
        res.data.emplancdt = data.emplancdt;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emplancdts/${context.emplancdt}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.emplancdt){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emplancdts/${context.emplancdt}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emplans/${context.emplan}/emplancdts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emplancdts/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanCDTServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplans/${context.emplan}/emplancdts/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emplancdts/searchdefault`,tempData,isloading);
    }
}