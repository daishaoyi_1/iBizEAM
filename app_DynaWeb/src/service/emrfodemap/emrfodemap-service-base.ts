import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 现象引用服务对象基类
 *
 * @export
 * @class EMRFODEMapServiceBase
 * @extends {EntityServie}
 */
export default class EMRFODEMapServiceBase extends EntityService {

    /**
     * Creates an instance of  EMRFODEMapServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODEMapServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMRFODEMapServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emrfodemap';
        this.APPDEKEY = 'emrfodemapid';
        this.APPDENAME = 'emrfodemaps';
        this.APPDETEXT = 'emrfodemapname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfodemap){
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfodemaps/${context.emrfodemap}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emrfodemaps/${context.emrfodemap}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfodemaps`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emrfodemaps`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfodemap){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfodes/${context.emrfode}/emrfodemaps/${context.emrfodemap}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emrfodemaps/${context.emrfodemap}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfodemap){
            let res:any = Http.getInstance().delete(`/emrfodes/${context.emrfode}/emrfodemaps/${context.emrfodemap}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emrfodemaps/${context.emrfodemap}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfodemap){
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfodemaps/${context.emrfodemap}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emrfodemaps/${context.emrfodemap}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfodemap) delete tempData.emrfodemap;
            if(tempData.emrfodemapid) delete tempData.emrfodemapid;
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfodemaps/getdraft`,tempData,isloading);
            res.data.emrfodemap = data.emrfodemap;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emrfodemap) delete tempData.emrfodemap;
        if(tempData.emrfodemapid) delete tempData.emrfodemapid;
        let res:any = await  Http.getInstance().get(`/emrfodemaps/getdraft`,tempData,isloading);
        res.data.emrfodemap = data.emrfodemap;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfodemap){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfodemaps/${context.emrfodemap}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emrfodemaps/${context.emrfodemap}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfodemap){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfodemaps/${context.emrfodemap}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emrfodemaps/${context.emrfodemap}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfodemaps/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emrfodemaps/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFODEMapServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfodemaps/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emrfodemaps/searchdefault`,tempData,isloading);
    }
}