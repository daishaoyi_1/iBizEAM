import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWO_INNERServiceBase from './emwo-inner-service-base';


/**
 * 内部工单服务对象
 *
 * @export
 * @class EMWO_INNERService
 * @extends {EMWO_INNERServiceBase}
 */
export default class EMWO_INNERService extends EMWO_INNERServiceBase {

    /**
     * Creates an instance of  EMWO_INNERService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_INNERService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}