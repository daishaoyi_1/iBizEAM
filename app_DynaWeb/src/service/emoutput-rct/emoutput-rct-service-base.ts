import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 产能服务对象基类
 *
 * @export
 * @class EMOutputRctServiceBase
 * @extends {EntityServie}
 */
export default class EMOutputRctServiceBase extends EntityService {

    /**
     * Creates an instance of  EMOutputRctServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMOutputRctServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMOutputRctServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emoutputrct';
        this.APPDEKEY = 'emoutputrctid';
        this.APPDENAME = 'emoutputrcts';
        this.APPDETEXT = 'emoutputrctname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && context.emoutputrct){
            let res:any = Http.getInstance().get(`/emoutputs/${context.emoutput}/emoutputrcts/${context.emoutputrct}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emoutputrcts/${context.emoutputrct}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emoutputs/${context.emoutput}/emoutputrcts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emoutputrcts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && context.emoutputrct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emoutputs/${context.emoutput}/emoutputrcts/${context.emoutputrct}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emoutputrcts/${context.emoutputrct}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && context.emoutputrct){
            let res:any = Http.getInstance().delete(`/emoutputs/${context.emoutput}/emoutputrcts/${context.emoutputrct}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emoutputrcts/${context.emoutputrct}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && context.emoutputrct){
            let res:any = await Http.getInstance().get(`/emoutputs/${context.emoutput}/emoutputrcts/${context.emoutputrct}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emoutputrcts/${context.emoutputrct}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emoutputrct) delete tempData.emoutputrct;
            if(tempData.emoutputrctid) delete tempData.emoutputrctid;
            let res:any = await Http.getInstance().get(`/emoutputs/${context.emoutput}/emoutputrcts/getdraft`,tempData,isloading);
            res.data.emoutputrct = data.emoutputrct;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emoutputrct) delete tempData.emoutputrct;
        if(tempData.emoutputrctid) delete tempData.emoutputrctid;
        let res:any = await  Http.getInstance().get(`/emoutputrcts/getdraft`,tempData,isloading);
        res.data.emoutputrct = data.emoutputrct;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && context.emoutputrct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emoutputs/${context.emoutput}/emoutputrcts/${context.emoutputrct}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emoutputrcts/${context.emoutputrct}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && context.emoutputrct){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emoutputs/${context.emoutput}/emoutputrcts/${context.emoutputrct}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emoutputrcts/${context.emoutputrct}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emoutputs/${context.emoutput}/emoutputrcts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emoutputrcts/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMOutputRctServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emoutput && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emoutputs/${context.emoutput}/emoutputrcts/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emoutputrcts/searchdefault`,tempData,isloading);
    }
}