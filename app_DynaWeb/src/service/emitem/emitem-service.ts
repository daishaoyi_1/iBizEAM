import { Http } from '@/utils';
import { Util } from '@/utils';
import EMItemServiceBase from './emitem-service-base';


/**
 * 物品服务对象
 *
 * @export
 * @class EMItemService
 * @extends {EMItemServiceBase}
 */
export default class EMItemService extends EMItemServiceBase {

    /**
     * Creates an instance of  EMItemService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}