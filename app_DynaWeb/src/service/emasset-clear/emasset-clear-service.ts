import { Http } from '@/utils';
import { Util } from '@/utils';
import EMAssetClearServiceBase from './emasset-clear-service-base';


/**
 * 资产清盘记录服务对象
 *
 * @export
 * @class EMAssetClearService
 * @extends {EMAssetClearServiceBase}
 */
export default class EMAssetClearService extends EMAssetClearServiceBase {

    /**
     * Creates an instance of  EMAssetClearService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClearService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}