import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 资产清盘记录服务对象基类
 *
 * @export
 * @class EMAssetClearServiceBase
 * @extends {EntityServie}
 */
export default class EMAssetClearServiceBase extends EntityService {

    /**
     * Creates an instance of  EMAssetClearServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClearServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMAssetClearServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emassetclear';
        this.APPDEKEY = 'emassetclearid';
        this.APPDENAME = 'emassetclears';
        this.APPDETEXT = 'emassetclearname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && context.emassetclear){
            let res:any = Http.getInstance().get(`/emassets/${context.emasset}/emassetclears/${context.emassetclear}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emassetclears/${context.emassetclear}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emassets/${context.emasset}/emassetclears`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emassetclears`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && context.emassetclear){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emassets/${context.emasset}/emassetclears/${context.emassetclear}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emassetclears/${context.emassetclear}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && context.emassetclear){
            let res:any = Http.getInstance().delete(`/emassets/${context.emasset}/emassetclears/${context.emassetclear}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emassetclears/${context.emassetclear}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && context.emassetclear){
            let res:any = await Http.getInstance().get(`/emassets/${context.emasset}/emassetclears/${context.emassetclear}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emassetclears/${context.emassetclear}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emassetclear) delete tempData.emassetclear;
            if(tempData.emassetclearid) delete tempData.emassetclearid;
            let res:any = await Http.getInstance().get(`/emassets/${context.emasset}/emassetclears/getdraft`,tempData,isloading);
            res.data.emassetclear = data.emassetclear;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emassetclear) delete tempData.emassetclear;
        if(tempData.emassetclearid) delete tempData.emassetclearid;
        let res:any = await  Http.getInstance().get(`/emassetclears/getdraft`,tempData,isloading);
        res.data.emassetclear = data.emassetclear;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && context.emassetclear){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emassets/${context.emasset}/emassetclears/${context.emassetclear}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emassetclears/${context.emassetclear}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Clear接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async Clear(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && context.emassetclear){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emassets/${context.emasset}/emassetclears/${context.emassetclear}/clear`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emassetclears/${context.emassetclear}/clear`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && context.emassetclear){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emassets/${context.emasset}/emassetclears/${context.emassetclear}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emassetclears/${context.emassetclear}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emassets/${context.emasset}/emassetclears/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emassetclears/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMAssetClearServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emasset && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emassets/${context.emasset}/emassetclears/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emassetclears/searchdefault`,tempData,isloading);
    }
}