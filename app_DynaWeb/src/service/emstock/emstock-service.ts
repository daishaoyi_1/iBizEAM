import { Http } from '@/utils';
import { Util } from '@/utils';
import EMStockServiceBase from './emstock-service-base';


/**
 * 库存服务对象
 *
 * @export
 * @class EMStockService
 * @extends {EMStockServiceBase}
 */
export default class EMStockService extends EMStockServiceBase {

    /**
     * Creates an instance of  EMStockService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStockService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}