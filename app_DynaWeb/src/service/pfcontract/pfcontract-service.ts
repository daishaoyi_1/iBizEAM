import { Http } from '@/utils';
import { Util } from '@/utils';
import PFContractServiceBase from './pfcontract-service-base';


/**
 * 合同服务对象
 *
 * @export
 * @class PFContractService
 * @extends {PFContractServiceBase}
 */
export default class PFContractService extends PFContractServiceBase {

    /**
     * Creates an instance of  PFContractService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFContractService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}