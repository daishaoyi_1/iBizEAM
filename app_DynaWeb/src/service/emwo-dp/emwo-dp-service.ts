import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWO_DPServiceBase from './emwo-dp-service-base';


/**
 * 点检工单服务对象
 *
 * @export
 * @class EMWO_DPService
 * @extends {EMWO_DPServiceBase}
 */
export default class EMWO_DPService extends EMWO_DPServiceBase {

    /**
     * Creates an instance of  EMWO_DPService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_DPService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}