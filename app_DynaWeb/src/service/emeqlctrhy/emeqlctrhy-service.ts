import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQLCTRHYServiceBase from './emeqlctrhy-service-base';


/**
 * 润滑油位置服务对象
 *
 * @export
 * @class EMEQLCTRHYService
 * @extends {EMEQLCTRHYServiceBase}
 */
export default class EMEQLCTRHYService extends EMEQLCTRHYServiceBase {

    /**
     * Creates an instance of  EMEQLCTRHYService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTRHYService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}