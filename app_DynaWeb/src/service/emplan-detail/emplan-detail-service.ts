import { Http } from '@/utils';
import { Util } from '@/utils';
import EMPlanDetailServiceBase from './emplan-detail-service-base';


/**
 * 计划步骤服务对象
 *
 * @export
 * @class EMPlanDetailService
 * @extends {EMPlanDetailServiceBase}
 */
export default class EMPlanDetailService extends EMPlanDetailServiceBase {

    /**
     * Creates an instance of  EMPlanDetailService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanDetailService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}