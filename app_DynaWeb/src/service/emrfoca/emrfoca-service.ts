import { Http } from '@/utils';
import { Util } from '@/utils';
import EMRFOCAServiceBase from './emrfoca-service-base';


/**
 * 原因服务对象
 *
 * @export
 * @class EMRFOCAService
 * @extends {EMRFOCAServiceBase}
 */
export default class EMRFOCAService extends EMRFOCAServiceBase {

    /**
     * Creates an instance of  EMRFOCAService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOCAService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}