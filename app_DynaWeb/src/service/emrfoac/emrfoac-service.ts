import { Http } from '@/utils';
import { Util } from '@/utils';
import EMRFOACServiceBase from './emrfoac-service-base';


/**
 * 方案服务对象
 *
 * @export
 * @class EMRFOACService
 * @extends {EMRFOACServiceBase}
 */
export default class EMRFOACService extends EMRFOACServiceBase {

    /**
     * Creates an instance of  EMRFOACService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOACService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}