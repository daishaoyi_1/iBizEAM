import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 方案服务对象基类
 *
 * @export
 * @class EMRFOACServiceBase
 * @extends {EntityServie}
 */
export default class EMRFOACServiceBase extends EntityService {

    /**
     * Creates an instance of  EMRFOACServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOACServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMRFOACServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emrfoac';
        this.APPDEKEY = 'emrfoacid';
        this.APPDENAME = 'emrfoacs';
        this.APPDETEXT = 'emrfoacname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoac){
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}/select`,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoac){
            let res:any = Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}/select`,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoac){
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfoacs/${context.emrfoac}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emrfoacs/${context.emrfoac}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfoacs`,data,isloading);
            
            return res;
        }
        if(context.emrfode && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfoacs`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emrfoacs`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}`,data,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfodes/${context.emrfode}/emrfoacs/${context.emrfoac}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emrfoacs/${context.emrfoac}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoac){
            let res:any = Http.getInstance().delete(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}`,isloading);
            return res;
        }
        if(context.emrfomo && context.emrfoac){
            let res:any = Http.getInstance().delete(`/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}`,isloading);
            return res;
        }
        if(context.emrfode && context.emrfoac){
            let res:any = Http.getInstance().delete(`/emrfodes/${context.emrfode}/emrfoacs/${context.emrfoac}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emrfoacs/${context.emrfoac}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoac){
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}`,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoac){
            let res:any = await Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}`,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoac){
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfoacs/${context.emrfoac}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emrfoacs/${context.emrfoac}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfoac) delete tempData.emrfoac;
            if(tempData.emrfoacid) delete tempData.emrfoacid;
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/getdraft`,tempData,isloading);
            res.data.emrfoac = data.emrfoac;
            
            return res;
        }
        if(context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfoac) delete tempData.emrfoac;
            if(tempData.emrfoacid) delete tempData.emrfoacid;
            let res:any = await Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfoacs/getdraft`,tempData,isloading);
            res.data.emrfoac = data.emrfoac;
            
            return res;
        }
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfoac) delete tempData.emrfoac;
            if(tempData.emrfoacid) delete tempData.emrfoacid;
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfoacs/getdraft`,tempData,isloading);
            res.data.emrfoac = data.emrfoac;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emrfoac) delete tempData.emrfoac;
        if(tempData.emrfoacid) delete tempData.emrfoacid;
        let res:any = await  Http.getInstance().get(`/emrfoacs/getdraft`,tempData,isloading);
        res.data.emrfoac = data.emrfoac;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfoacs/${context.emrfoac}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emrfoacs/${context.emrfoac}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}/save`,data,isloading);
            
            return res;
        }
        if(context.emrfomo && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfoacs/${context.emrfoac}/save`,data,isloading);
            
            return res;
        }
        if(context.emrfode && context.emrfoac){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfoacs/${context.emrfoac}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emrfoacs/${context.emrfoac}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emrfomos/${context.emrfomo}/emrfoacs/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfoacs/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emrfoacs/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOACServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/emrfoacs/searchdefault`,tempData,isloading);
        }
        if(context.emrfomo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfomos/${context.emrfomo}/emrfoacs/searchdefault`,tempData,isloading);
        }
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfoacs/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emrfoacs/searchdefault`,tempData,isloading);
    }
}