import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQKPServiceBase from './emeqkp-service-base';


/**
 * 设备关键点服务对象
 *
 * @export
 * @class EMEQKPService
 * @extends {EMEQKPServiceBase}
 */
export default class EMEQKPService extends EMEQKPServiceBase {

    /**
     * Creates an instance of  EMEQKPService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}