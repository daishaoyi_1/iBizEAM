import { Http } from '@/utils';
import { Util } from '@/utils';
import EMItemROutServiceBase from './emitem-rout-service-base';


/**
 * 退货单服务对象
 *
 * @export
 * @class EMItemROutService
 * @extends {EMItemROutServiceBase}
 */
export default class EMItemROutService extends EMItemROutServiceBase {

    /**
     * Creates an instance of  EMItemROutService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemROutService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}