import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 维护保养服务对象基类
 *
 * @export
 * @class EMEQKeepServiceBase
 * @extends {EntityServie}
 */
export default class EMEQKeepServiceBase extends EntityService {

    /**
     * Creates an instance of  EMEQKeepServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKeepServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMEQKeepServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emeqkeep';
        this.APPDEKEY = 'emeqkeepid';
        this.APPDENAME = 'emeqkeeps';
        this.APPDETEXT = 'emeqkeepname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqkeep){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqkeep){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emeqkeeps/${context.emeqkeep}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps`,data,isloading);
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emeqkeeps`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emeqkeeps`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqkeep){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqkeep){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emeqkeeps/${context.emeqkeep}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqkeep){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}`,isloading);
            return res;
        }
        if(context.emequip && context.emeqkeep){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emeqkeeps/${context.emeqkeep}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqkeep){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqkeep){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emeqkeeps/${context.emeqkeep}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqkeep) delete tempData.emeqkeep;
            if(tempData.emeqkeepid) delete tempData.emeqkeepid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/getdraft`,tempData,isloading);
            res.data.emeqkeep = data.emeqkeep;
            
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqkeep) delete tempData.emeqkeep;
            if(tempData.emeqkeepid) delete tempData.emeqkeepid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emeqkeeps/getdraft`,tempData,isloading);
            res.data.emeqkeep = data.emeqkeep;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emeqkeep) delete tempData.emeqkeep;
        if(tempData.emeqkeepid) delete tempData.emeqkeepid;
        let res:any = await  Http.getInstance().get(`/emeqkeeps/getdraft`,tempData,isloading);
        res.data.emeqkeep = data.emeqkeep;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqkeep){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqkeep){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emeqkeeps/${context.emeqkeep}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emeqkeep){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emeqkeep){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emeqkeeps/${context.emeqkeep}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emeqkeeps/${context.emeqkeep}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchCalendar接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async FetchCalendar(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/fetchcalendar`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emeqkeeps/fetchcalendar`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emeqkeeps/fetchcalendar`,tempData,isloading);
        return res;
    }

    /**
     * searchCalendar接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async searchCalendar(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/searchcalendar`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emeqkeeps/searchcalendar`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emeqkeeps/searchcalendar`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emeqkeeps/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emeqkeeps/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQKeepServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emeqkeeps/searchdefault`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emeqkeeps/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emeqkeeps/searchdefault`,tempData,isloading);
    }
}