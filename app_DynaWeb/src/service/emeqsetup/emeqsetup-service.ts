import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQSetupServiceBase from './emeqsetup-service-base';


/**
 * 更换安装服务对象
 *
 * @export
 * @class EMEQSetupService
 * @extends {EMEQSetupServiceBase}
 */
export default class EMEQSetupService extends EMEQSetupServiceBase {

    /**
     * Creates an instance of  EMEQSetupService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSetupService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}