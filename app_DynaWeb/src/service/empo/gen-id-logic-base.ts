import EMPOService from '@/service/empo/empo-service';
import { Verify } from '@/utils/verify/verify';


/**
 * 生成订单编号
 *
 * @export
 * @class GenIdLogicBase
 */
export default class GenIdLogicBase {

    /**
     * 名称
     * 
     * @memberof  GenIdLogicBase
     */
    private name:string ="GenId";

    /**
     * 唯一标识
     * 
     * @memberof  GenIdLogicBase
     */
    private id:string = "6e892c7c9a8937f434959e92e307fcdb";

    /**
     * 默认参数名称
     * 
     * @memberof  GenIdLogicBase
     */
    private defaultParamName:string = "Default";

    /**
     * 参数集合
     * 
     * @memberof  GenIdLogicBase
     */
    private paramsMap:Map<string,any> = new Map();

    /**
     * Creates an instance of  GenIdLogicBase.
     * 
     * @param {*} [opts={}]
     * @memberof  GenIdLogicBase
     */
    constructor(opts: any = {}) {
        this.initParams(opts);
    }

    /**
     * 初始化参数集合
     * 
     * @param {*} [opts={}]
     * @memberof  GenIdLogicBase
     */
    public initParams(opts:any){
        this.paramsMap.set('Default',opts);
    }


    /**
     * 计算0节点结果
     * 
     * @param params 传入参数
     */
    public compute0Cond(params:any):boolean{
        return true;
    }

    /**
     * 执行逻辑
     * 
     * @param context 应用上下文
     * @param params 传入参数
     */
    public onExecute(context:any,params:any,isloading:boolean){
        return this.executeBegin(context,params,isloading);
    }


    /**
    * 生成订单编号
    * 
    * @param context 应用上下文
    * @param params 传入参数
    */
    private async executeDeaction1(context:any,params:any,isloading:boolean){
        // 行为处理节点
        let result: any;
        let actionParam:any = this.paramsMap.get('Default');
        const targetService:EMPOService = new EMPOService();
        if (targetService['GenId'] && targetService['GenId'] instanceof Function) {
            result = await targetService['GenId'](actionParam.context,actionParam.data, false);
        }
        if(result && result.status == 200){
            Object.assign(actionParam.data,result.data);
        return this.paramsMap.get(this.defaultParamName).data;
        }
    }

    /**
    * 开始
    * 
    * @param params 传入参数
    */
    private async executeBegin(context:any,params:any,isloading:boolean){
        //开始节点
        if(this.compute0Cond(params)){
            return this.executeDeaction1(context,params,isloading);   
        }
    }


}