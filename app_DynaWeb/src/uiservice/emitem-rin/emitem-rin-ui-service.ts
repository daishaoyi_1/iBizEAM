import EMItemRInUIServiceBase from './emitem-rin-ui-service-base';

/**
 * 入库单UI服务对象
 *
 * @export
 * @class EMItemRInUIService
 */
export default class EMItemRInUIService extends EMItemRInUIServiceBase {

    /**
     * Creates an instance of  EMItemRInUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemRInUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}