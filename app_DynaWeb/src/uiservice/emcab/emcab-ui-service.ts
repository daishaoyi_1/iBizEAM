import EMCabUIServiceBase from './emcab-ui-service-base';

/**
 * 货架UI服务对象
 *
 * @export
 * @class EMCabUIService
 */
export default class EMCabUIService extends EMCabUIServiceBase {

    /**
     * Creates an instance of  EMCabUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMCabUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}