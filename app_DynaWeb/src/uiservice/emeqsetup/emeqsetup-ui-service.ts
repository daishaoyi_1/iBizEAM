import EMEQSetupUIServiceBase from './emeqsetup-ui-service-base';

/**
 * 更换安装UI服务对象
 *
 * @export
 * @class EMEQSetupUIService
 */
export default class EMEQSetupUIService extends EMEQSetupUIServiceBase {

    /**
     * Creates an instance of  EMEQSetupUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSetupUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}