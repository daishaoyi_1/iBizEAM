import PFTeamUIServiceBase from './pfteam-ui-service-base';

/**
 * 班组UI服务对象
 *
 * @export
 * @class PFTeamUIService
 */
export default class PFTeamUIService extends PFTeamUIServiceBase {

    /**
     * Creates an instance of  PFTeamUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFTeamUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}