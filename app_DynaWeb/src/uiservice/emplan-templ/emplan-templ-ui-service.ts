import EMPlanTemplUIServiceBase from './emplan-templ-ui-service-base';

/**
 * 计划模板UI服务对象
 *
 * @export
 * @class EMPlanTemplUIService
 */
export default class EMPlanTemplUIService extends EMPlanTemplUIServiceBase {

    /**
     * Creates an instance of  EMPlanTemplUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanTemplUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}