import EMEQLCTRHYUIServiceBase from './emeqlctrhy-ui-service-base';

/**
 * 润滑油位置UI服务对象
 *
 * @export
 * @class EMEQLCTRHYUIService
 */
export default class EMEQLCTRHYUIService extends EMEQLCTRHYUIServiceBase {

    /**
     * Creates an instance of  EMEQLCTRHYUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTRHYUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}