import EMWOUIServiceBase from './emwo-ui-service-base';

/**
 * 工单UI服务对象
 *
 * @export
 * @class EMWOUIService
 */
export default class EMWOUIService extends EMWOUIServiceBase {

    /**
     * Creates an instance of  EMWOUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWOUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}