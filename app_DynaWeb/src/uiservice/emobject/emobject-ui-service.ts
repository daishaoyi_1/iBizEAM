import EMObjectUIServiceBase from './emobject-ui-service-base';

/**
 * 对象UI服务对象
 *
 * @export
 * @class EMObjectUIService
 */
export default class EMObjectUIService extends EMObjectUIServiceBase {

    /**
     * Creates an instance of  EMObjectUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMObjectUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}