import EMDRWGUIServiceBase from './emdrwg-ui-service-base';

/**
 * 文档UI服务对象
 *
 * @export
 * @class EMDRWGUIService
 */
export default class EMDRWGUIService extends EMDRWGUIServiceBase {

    /**
     * Creates an instance of  EMDRWGUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDRWGUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}