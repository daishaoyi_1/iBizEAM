import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取studio链接数据
mock.onGet('./assets/json/view-config.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
                "emeneditview9_editmode": {
            "title": "能源信息",
            "caption": "能源信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_en",
            "viewname": "EMENEditView9_EditMode",
            "viewtag": "0089caf9ab821aeaeda565978c0e097f"
        },
        "emwo_oscgridview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCGridView",
            "viewtag": "00c09bb36299b6b39ba5abd33ee723d4"
        },
        "emplandetaileditview": {
            "title": "计划步骤",
            "caption": "计划步骤",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANDETAILEditView",
            "viewtag": "00e3d76156363c782e8894c768a85068"
        },
        "emeqlctmapgridview9": {
            "title": "位置关系表格视图",
            "caption": "位置关系",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTMapGridView9",
            "viewtag": "00f3aac01d9c0c04bed003657fa70ae7"
        },
        "emserviceevltoconfirmgridview": {
            "title": "服务商评估",
            "caption": "服务商评估-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlToConfirmGridView",
            "viewtag": "0132c2b9aa7ca236c4e7c41240a62130"
        },
        "emstorepartpickupview": {
            "title": "仓库库位数据选择视图",
            "caption": "仓库库位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPARTPickupView",
            "viewtag": "01453141ea85827b19e188b8c52e16af"
        },
        "emplaneditview_editmode": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanEditView_EditMode",
            "viewtag": "0253e6741426642884ddcd44ae7a0184"
        },
        "emassetpickupview": {
            "title": "资产数据选择视图",
            "caption": "资产",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETPickupView",
            "viewtag": "0289b9f5c79a8693cf827b118d901d10"
        },
        "emitemdashboardview": {
            "title": "物品数据看板视图",
            "caption": "物品",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemDashboardView",
            "viewtag": "02ca228e50a4e671905b03ce430fab1e"
        },
        "emstockbystoregridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByStoreGridView",
            "viewtag": "035638d53a8da50a80a1fde73dfad083"
        },
        "emwplistconfimcostgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListConfimCostGridView",
            "viewtag": "04a35187fcb64fe9d48f07b2aab33838"
        },
        "equipportalview": {
            "title": "设备模块看板视图",
            "caption": "",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EquipPortalView",
            "viewtag": "0520b23d5bb5af350da521f2dbc5c2fa"
        },
        "emstoretreeexpview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStoreTreeExpView",
            "viewtag": "0548169aafa606b4990699b27d4a9108"
        },
        "emrfodepickupgridview": {
            "title": "现象选择表格视图",
            "caption": "现象",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEPickupGridView",
            "viewtag": "055b4d344fd137972cf9f6e1e520d3f4"
        },
        "emdrwggridview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGGridView",
            "viewtag": "0592eb74edf2b1127834dd3507259701"
        },
        "emassetclassgridview": {
            "title": "资产类别",
            "caption": "资产类别",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSGridView",
            "viewtag": "05a0031df62884e8d4dad74a1a24bd43"
        },
        "empopickupview": {
            "title": "订单数据选择视图",
            "caption": "订单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPickupView",
            "viewtag": "06c26a5e7a81cb5c9c9f64ea3b3b5939"
        },
        "pfdepteditview": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFDEPTEditView",
            "viewtag": "06c6b805e8d7109e4222873485e2576d"
        },
        "emrfodetypeeditview": {
            "title": "现象分类",
            "caption": "现象分类",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODETYPEEditView",
            "viewtag": "070c48603306b920cb57df387c97ee9c"
        },
        "emstockgridview": {
            "title": "库存物品",
            "caption": "库存物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStockGridView",
            "viewtag": "07279dd9879d2bfb5563b2b0f66a289f"
        },
        "emeqsparedetailgridview9": {
            "title": "备件物品",
            "caption": "备件物品",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareDetailGridView9",
            "viewtag": "0757687c45eb04f0eb6cd62b03181000"
        },
        "emasseteditview9_editmode": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_as",
            "viewname": "EMASSETEditView9_EditMode",
            "viewtag": "085e41723e9e63477395e90fd746e108"
        },
        "emoutputpickupgridview": {
            "title": "能力选择表格视图",
            "caption": "能力",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTPickupGridView",
            "viewtag": "08abd10b5d3f81d118c4404d877e8681"
        },
        "emrfodeeditview9_editmode": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEEditView9_EditMode",
            "viewtag": "0934b5de6b512221467133b7f396c5aa"
        },
        "emserviceeditview9": {
            "title": "PUR服务商信息",
            "caption": "PUR服务商信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEditView9",
            "viewtag": "09c4f6d6064ab5dd1a29d839526bc308"
        },
        "emproducteditview9_editmode": {
            "title": "试用品信息",
            "caption": "试用品信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPRODUCTEditView9_EditMode",
            "viewtag": "0a0d744cb767cb454e59e1a14ad1a8aa"
        },
        "emeqlctgssgridview": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTGSSGridView",
            "viewtag": "0a1716c8b1771381f72547ea0905a55b"
        },
        "emwo_enconfirmedgridview": {
            "title": "能耗登记工单表格视图",
            "caption": "能耗工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENConfirmedGridView",
            "viewtag": "0ac7f785d30a6ec3312287fd463ec80d"
        },
        "emeqtypepickupview": {
            "title": "设备类型数据选择视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTYPEPickupView",
            "viewtag": "0afbbb2206c9aae01ad6d582abaa3cc2"
        },
        "emwocalendarexpview": {
            "title": "工单日历导航视图",
            "caption": "工单",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOCalendarExpView",
            "viewtag": "0bb9dec249e00829188ae7bda6c6b1f7"
        },
        "emeqsetupeditview9_editmode": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPEditView9_EditMode",
            "viewtag": "0bc604ccec5ed3b8a546b1ea3fd02b99"
        },
        "emwo_dpdraftgridview": {
            "title": "点检工单表格视图",
            "caption": "点检工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPDraftGridView",
            "viewtag": "0ca528774952657d3cdd86c50e054b9f"
        },
        "emplantabexpview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanTabExpView",
            "viewtag": "0ddc6ccb720e1efbb14137944af01942"
        },
        "emitemtypepickupview": {
            "title": "物品类型数据选择视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMTYPEPickupView",
            "viewtag": "0e0f28d9a5f4009783645d0b45af3013"
        },
        "emstorepickupgridview": {
            "title": "仓库选择表格视图",
            "caption": "仓库",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPickupGridView",
            "viewtag": "0eb4c5454a76692d83d7272a46d59adb"
        },
        "emequipeditview9": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipEditView9",
            "viewtag": "0f00641d113dca697da05070ac4accb9"
        },
        "empoonordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOOnOrderGridView",
            "viewtag": "103ab6f822e991b7210bbf0ca8873f00"
        },
        "emitemroutdraftgridview": {
            "title": "退货单",
            "caption": "退货单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutDraftGridView",
            "viewtag": "109c14e854f457525c7797b45744c748"
        },
        "emitemrouteditview9": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutEditView9",
            "viewtag": "10a0cea9cf2866b7d289dcffc6958208"
        },
        "emstockpickupview": {
            "title": "库存数据选择视图",
            "caption": "库存",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOCKPickupView",
            "viewtag": "10e3b1128d4eebf59fe7de662b08f78c"
        },
        "emwplisttreeexpview": {
            "title": "采购申请树导航视图",
            "caption": "采购申请",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListTreeExpView",
            "viewtag": "1128e40159b4541c85e308241925fcb9"
        },
        "emrfodemapdataview": {
            "title": "现象引用数据视图",
            "caption": "现象引用",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEMapDataView",
            "viewtag": "114fc486b1911d57a272f584a5df3d89"
        },
        "emoutputpickupview": {
            "title": "能力数据选择视图",
            "caption": "能力",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTPickupView",
            "viewtag": "119b9545f6ba61c83f95ac71b50db71d"
        },
        "emwo_dpeditview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPEditView",
            "viewtag": "119e1010e501c58f6d236f6bf480021c"
        },
        "emwplistconfirmcosteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListConfirmCostEditView9",
            "viewtag": "11e67a70cadbf7d63b54c716ffa2098a"
        },
        "emeqmaintanceeditview": {
            "title": "抢修记录编辑视图",
            "caption": "抢修记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQMAINTANCEEditView",
            "viewtag": "12248eb6957f5373ae74541a36a9ea43"
        },
        "emwo_dptabexpview": {
            "title": "点检工单分页导航视图",
            "caption": "点检工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPTabExpView",
            "viewtag": "122781dc9376a420a1f304b8dbed1d73"
        },
        "emeqsetuppickupgridview": {
            "title": "更换安装选择表格视图",
            "caption": "更换安装",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPPickupGridView",
            "viewtag": "12bbbf70589724763fa2338816ff18eb"
        },
        "emoutputrctgridview": {
            "title": "产能",
            "caption": "产能",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOutputRctGridView",
            "viewtag": "13866f67fb27d4a30afd86a8be48addb"
        },
        "emitemeditview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMEditView",
            "viewtag": "1466ad3713562c3bd52965229e4c7000"
        },
        "empogridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPOGridView",
            "viewtag": "15b885e7c1fb7b99a631173b06ff4281"
        },
        "emitemoptionview": {
            "title": "物品选项操作视图",
            "caption": "物品",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemOptionView",
            "viewtag": "15ce78a424df6640f2b3224effae37cf"
        },
        "emassetcleargridview": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEARGridView",
            "viewtag": "162a6be086726ea1b5ff9180fce9ae0a"
        },
        "emitemplconfirmedgridview": {
            "title": "损溢单",
            "caption": "损溢单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLConfirmedGridView",
            "viewtag": "163aad656c51d4d36328410aaa86e230"
        },
        "emplaneditview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanEditView",
            "viewtag": "185d93f3a80fff73e9c2c9aef7de0fc6"
        },
        "emdrwgeditview_editmode": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGEditView_EditMode",
            "viewtag": "19bde26f9a8676ddda7f45a3ca65c1e3"
        },
        "emwo_endraftgridview": {
            "title": "能耗登记工单表格视图",
            "caption": "能耗工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENDraftGridView",
            "viewtag": "19fbe10d79bf2021a6a8ab360fd81787"
        },
        "emitemallgridview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemAllGridView",
            "viewtag": "1a2772432ee331e220bc761642890f00"
        },
        "pfteampickupview": {
            "title": "班组数据选择视图",
            "caption": "班组",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFTEAMPickupView",
            "viewtag": "1a3909429db5bd3f88ce516fdc7554c6"
        },
        "emrfodetabexpview": {
            "title": "现象",
            "caption": "现象",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODETabExpView",
            "viewtag": "1a4fa502229e9c405dd08b07692825b0"
        },
        "emrfomopickupgridview": {
            "title": "模式选择表格视图",
            "caption": "模式",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOMOPickupGridView",
            "viewtag": "1aacdb29146860825910174be2d23bc0"
        },
        "emitemtypepickupgridview": {
            "title": "物品类型选择表格视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypePickupGridView",
            "viewtag": "1b7f904dec9040918d6f342629e85d73"
        },
        "emitempusedraftgridview": {
            "title": "领料单",
            "caption": "领料单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseDraftGridView",
            "viewtag": "1bb323bed9c16ff28037e88166574dcc"
        },
        "emstoreoptionview": {
            "title": "仓库选项操作视图",
            "caption": "仓库",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStoreOptionView",
            "viewtag": "1c33ac2a63e59b33dcf794ef8a874d05"
        },
        "emrfodemapeditview": {
            "title": "现象引用编辑视图",
            "caption": "现象引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEMapEditView",
            "viewtag": "1cae1169a285d7d403c0b5e715c7b3ee"
        },
        "appindex": {
            "title": "iBizEAM",
            "caption": "iBizEAM",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "eam_core",
            "viewname": "appIndex",
            "viewtag": "1d8af4bbdd217b074c71f8fd1bf4a30d"
        },
        "emwo_oscwovieweditview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCWOViewEditView",
            "viewtag": "1dbea6147c18d41d86921cf624bef2c2"
        },
        "emplanpickupview": {
            "title": "计划数据选择视图",
            "caption": "计划",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANPickupView",
            "viewtag": "1def25c7e51403af4ba84bae7e112131"
        },
        "pfteameditview": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFTEAMEditView",
            "viewtag": "1e19989e348ce8d7571eceacddbd0944"
        },
        "emrfoacgridview": {
            "title": "方案",
            "caption": "方案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOACGridView",
            "viewtag": "1e3dbb9d0adafeb06d43e775ddabddd7"
        },
        "empodetailwaitbookgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailWaitBookGridView",
            "viewtag": "1f9b603f4067a0d088548b57979a7dbc"
        },
        "emeqlctgsseditview": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTGSSEditView",
            "viewtag": "1fa759621fff666a52ab4eb6dafff97c"
        },
        "emeqmaintanceeditview9_editmode": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQMAINTANCEEditView9_EditMode",
            "viewtag": "20295e2346e4bd54a5f9781803002162"
        },
        "emmachmodelpickupgridview": {
            "title": "机型选择表格视图",
            "caption": "机型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHMODELPickupGridView",
            "viewtag": "20b6ae957ff698adfd329b39954dd830"
        },
        "emplanoptionview": {
            "title": "计划选项操作视图",
            "caption": "计划",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANOptionView",
            "viewtag": "216fe122405a3a1f319f2f0d182d42b1"
        },
        "emeqcheckeditview9_editmode": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQCHECKEditView9_EditMode",
            "viewtag": "21bdae607968a85052f7279eacae7b89"
        },
        "emeqkprcdkprcdeditview": {
            "title": "关键点记录编辑视图",
            "caption": "关键点记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPRCDKPRcdEditView",
            "viewtag": "240a6507480f8fa237effe04dad87dfb"
        },
        "emwplistcosteditview": {
            "title": "询价单编辑视图",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTCOSTEditView",
            "viewtag": "2578541b45cdfbd0a0762ed8fc37b2eb"
        },
        "emwo_eneditview_editmode": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENEditView_EditMode",
            "viewtag": "2599e0806f570c3ed6227d8519f248bb"
        },
        "emitemrouttoconfirmgridview": {
            "title": "退货单",
            "caption": "退货单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutToConfirmGridView",
            "viewtag": "25bd514fa7094f014fb6974eba95273e"
        },
        "emeqsetuppickupview": {
            "title": "更换安装数据选择视图",
            "caption": "更换安装",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPPickupView",
            "viewtag": "269220e541a85f5d1ceed999f9328d3a"
        },
        "emitemcseditview9_editmode": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemCSEditView9_EditMode",
            "viewtag": "2754fe39c13f14d2a8322d4d7b652c2a"
        },
        "emitemroutconfirmedgridview": {
            "title": "退货单",
            "caption": "退货单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutConfirmedGridView",
            "viewtag": "28e2f6273345cec47333a28bf5c335f4"
        },
        "emequipoptionview": {
            "title": "设备档案选项操作视图",
            "caption": "设备档案",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipOptionView",
            "viewtag": "292ebc316dc63c88b5c116e02d9018c7"
        },
        "emwo_innerconfirmedgridview": {
            "title": "内部工单表格视图",
            "caption": "内部工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERConfirmedGridView",
            "viewtag": "2948a8e676d62eddefbae79d59b58bbd"
        },
        "emwplistfillcosteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListFillCostEditView9",
            "viewtag": "29e431887c745568ea52c8a3007bb20b"
        },
        "emapplytoconfirmgridview": {
            "title": "外委申请表格视图",
            "caption": "外委申请-待完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyToConfirmGridView",
            "viewtag": "2a10043fa7a43a4c6c82169c05189e6a"
        },
        "emitempusewaitissuegridview": {
            "title": "领料单",
            "caption": "领料单-待发料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseWaitIssueGridView",
            "viewtag": "2a4de29000e2ea8ce3fd17c3a5dbf271"
        },
        "emeqlctgsspickupview": {
            "title": "钢丝绳位置数据选择视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSPickupView",
            "viewtag": "2aaab51f278ba3de2d63422407acfbf8"
        },
        "emdrwgtreeexpview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGTreeExpView",
            "viewtag": "2ad8d02d422d99d808a84e9f957dc18d"
        },
        "emeqkeepeditview": {
            "title": "维护保养编辑视图",
            "caption": "维护保养",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQKEEPEditView",
            "viewtag": "2b3b2b2ecc24c32a3487b467d8e05d60"
        },
        "emrfomopickupview": {
            "title": "模式数据选择视图",
            "caption": "模式",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOMOPickupView",
            "viewtag": "2b7b1e11e03a9b07c03faec090fd16bf"
        },
        "emwplistcostfillcostview": {
            "title": "询价单填报",
            "caption": "询价单填报",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostFillCostView",
            "viewtag": "2b834cfcd8d9f9cee463041259277a6a"
        },
        "emeqlocationeditview9": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONEditView9",
            "viewtag": "2c523676e4ec4af92e3a47b0f8fed3ad"
        },
        "emmachmodelpickupview": {
            "title": "机型数据选择视图",
            "caption": "机型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHMODELPickupView",
            "viewtag": "2d6398a6f115defd4bd4270a8ed55b40"
        },
        "pfemppickupgridview": {
            "title": "职员选择表格视图",
            "caption": "职员",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpPickupGridView",
            "viewtag": "2de395c6144ba440d0769258b67e9ebc"
        },
        "emequipdashboardview9": {
            "title": "设备主信息图表数据看板视图",
            "caption": "设备档案",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipDashboardView9",
            "viewtag": "2f26049b75c05d4889e37df62d0d267c"
        },
        "emitempusewaitissueeditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseWaitIssueEditView9",
            "viewtag": "303dd1476dc3df715f9b5321397a2202"
        },
        "emserviceevleditview9_editmode": {
            "title": "服务商评估信息",
            "caption": "服务商评估信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlEditView9_EditMode",
            "viewtag": "30e16490e31ea72f6e68e36a0786cb6a"
        },
        "emwplistgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTGridView",
            "viewtag": "3150377a55d770ffe851fcbd08c18a35"
        },
        "emeqkpgridview": {
            "title": "设备关键点表格视图",
            "caption": "设备关键点",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPGridView",
            "viewtag": "317a9f3498d4a3f23bfec576bd157e56"
        },
        "emeqtypetreepickupview": {
            "title": "设备类型数据选择视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeTreePickupView",
            "viewtag": "31e1fff53fdfa78f312b61bc47211cc8"
        },
        "empurplanpickupview": {
            "title": "计划修理数据选择视图",
            "caption": "计划修理",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPURPLANPickupView",
            "viewtag": "33b626dfdee0554e09a307eefdea4896"
        },
        "emeqsparepickupgridview": {
            "title": "备件包选择表格视图",
            "caption": "备件包",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREPickupGridView",
            "viewtag": "33ca6b67909689f3d04473d97bbd4c4f"
        },
        "emwplistcostpickupview": {
            "title": "询价单数据选择视图",
            "caption": "询价单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTCOSTPickupView",
            "viewtag": "346240dfc688117c781dadf6bf79b3b1"
        },
        "emeqsparemapdashboardview9": {
            "title": "备件包引用数据看板视图",
            "caption": "备件包引用",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapDashboardView9",
            "viewtag": "34a7edbd90b0fe840d08ceb7ef0b4cbc"
        },
        "emstoregridview": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreGridView",
            "viewtag": "3534d5896788f77d8d0dde9386485713"
        },
        "emeqkeepcaleditview9": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQKeepCalEditView9",
            "viewtag": "353c3ac0d0ab9122060c3c572f103876"
        },
        "empotabexpview": {
            "title": "订单",
            "caption": "订单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOTabExpView",
            "viewtag": "359bb95633b500194ecf234b1459452c"
        },
        "emstorepartpickupgridview": {
            "title": "仓库库位选择表格视图",
            "caption": "仓库库位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPARTPickupGridView",
            "viewtag": "38468e08ff871576829e622e36629deb"
        },
        "pfteampickupgridview": {
            "title": "班组选择表格视图",
            "caption": "班组",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFTeamPickupGridView",
            "viewtag": "38a5aaef23bb02699edf0f02dd47fecb"
        },
        "emeqlocationgridview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLocationGridView",
            "viewtag": "38d5dcb60cd7d3412059c26be3602ecc"
        },
        "emeqmpmpeditview": {
            "title": "设备仪表编辑视图",
            "caption": "设备仪表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMPEditView",
            "viewtag": "38db8c9b2c93a54ba48027a93cbf3d03"
        },
        "pfunitpickupgridview": {
            "title": "计量单位选择表格视图",
            "caption": "计量单位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFUNITPickupGridView",
            "viewtag": "3932d179c351679c58cfd2d9c881852d"
        },
        "emeqlocationpickupgridview": {
            "title": "位置选择表格视图",
            "caption": "位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONPickupGridView",
            "viewtag": "39363b96eca809c2828d816b7e5d607d"
        },
        "empopickupgridview": {
            "title": "订单选择表格视图",
            "caption": "订单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPickupGridView",
            "viewtag": "394411062207c50d005bbe000962ad27"
        },
        "emserviceevlgridview": {
            "title": "服务商评估",
            "caption": "服务商评估",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlGridView",
            "viewtag": "39d206e27bf57393aa31bf1bb6b17f37"
        },
        "emeqkppickupgridview": {
            "title": "设备关键点选择表格视图",
            "caption": "设备关键点",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPPickupGridView",
            "viewtag": "3afd1103f5b90336a2cb4f656c200a01"
        },
        "emwo_osceditview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCEditView",
            "viewtag": "3b0e147c4d3f9e791aafb299eef39650"
        },
        "emstoreeditview_9924": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreEditView_9924",
            "viewtag": "3c46a01296e97a49e90307240629ed99"
        },
        "emeqmaintanceeditview9": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQMAINTANCEEditView9",
            "viewtag": "3c5559fb9e781437028ff3fc861517ba"
        },
        "emitemtypeinfotreeexpview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeInfoTreeExpView",
            "viewtag": "3d19af9a5f72a337dfa93c58c0d5f227"
        },
        "pfdeptpickupview": {
            "title": "部门数据选择视图",
            "caption": "部门",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFDEPTPickupView",
            "viewtag": "3d7728acd1e40b1a95e40157b49d0474"
        },
        "emitemrinputingridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemRInPutInGridView",
            "viewtag": "3e16ea7b3e288e9959ef0ac358e7a6ce"
        },
        "emeqlocationmaininfo": {
            "title": "位置数据看板视图",
            "caption": "位置",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationMainInfo",
            "viewtag": "3e348700238167e494cde09e6253fa02"
        },
        "pfcontracteditview9": {
            "title": "合同信息",
            "caption": "合同信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTEditView9",
            "viewtag": "3e90ec032300c8950c5b8c7ff4edf40d"
        },
        "emrfocaeditview": {
            "title": "原因信息",
            "caption": "原因信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOCAEditView",
            "viewtag": "3eb428b686c4ebedc04a6828493703b7"
        },
        "emrfodegridexpview": {
            "title": "现象表格导航视图",
            "caption": "现象",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEGridExpView",
            "viewtag": "3ef63ff5f7d8be11eb29af71827c47cf"
        },
        "empodetaileditview": {
            "title": "订单条目编辑视图",
            "caption": "订单条目",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILEditView",
            "viewtag": "40dd04e608716d937cb5908e642bea24"
        },
        "emwo_oscconfirmedgridview": {
            "title": "外委保养工单表格视图",
            "caption": "外委工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCConfirmedGridView",
            "viewtag": "415ff231f89f0b6fa17baba30a560b42"
        },
        "emitemcsdraftgridview": {
            "title": "调整单",
            "caption": "调整单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSDraftGridView",
            "viewtag": "420e6af39c1614b98aa38db8902ba6e2"
        },
        "emeqkeepgridview": {
            "title": "保养记录",
            "caption": "保养记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQKeepGridView",
            "viewtag": "42aa98021989fcf477a065d668336104"
        },
        "emeqsparegridexpview": {
            "title": "备件包表格导航视图",
            "caption": "备件包",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareGridExpView",
            "viewtag": "43c620e663f2e9764a9aa525a38ec369"
        },
        "emeqmpgridview": {
            "title": "设备仪表表格视图",
            "caption": "设备仪表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPGridView",
            "viewtag": "43cff8771ff4fde73dfd4be7fd48b222"
        },
        "emrfodegridview": {
            "title": "现象",
            "caption": "现象",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEGridView",
            "viewtag": "4405371c7c3e613713d9dcf4de83d8d0"
        },
        "emeqlctfdjgridview": {
            "title": "发动机位置",
            "caption": "发动机位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTFDJGridView",
            "viewtag": "445b44d8c4ce5bf47630b421d6ca9961"
        },
        "emrfoacpickupgridview": {
            "title": "方案选择表格视图",
            "caption": "方案",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOACPickupGridView",
            "viewtag": "4471a75e676439c54695de7c5ad1b885"
        },
        "emeqlcttiresgridview": {
            "title": "轮胎位置",
            "caption": "轮胎位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTTIRESGridView",
            "viewtag": "44b8980bcd120000ebe0bb37410c78e9"
        },
        "emitemprtneditview9_editmode": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPRtnEditView9_EditMode",
            "viewtag": "44c3bc79b5eccb74f523f63235cdd7d8"
        },
        "emitembaseinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemBaseInfoView",
            "viewtag": "48250c1f117fe631eed2a559cda68077"
        },
        "emoutputeditview": {
            "title": "能力信息",
            "caption": "能力信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTEditView",
            "viewtag": "486d3ddff5048c869644092c23d353f0"
        },
        "emservicecontactview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>联系信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceContactView9",
            "viewtag": "495ba635480a210157e05b963957a667"
        },
        "emwo_innerdraftgridview": {
            "title": "内部工单表格视图",
            "caption": "内部工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERDraftGridView",
            "viewtag": "49c8223dd448d81860ddfb9112fb509b"
        },
        "emrfomoeditview": {
            "title": "模式信息",
            "caption": "模式信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOMOEditView",
            "viewtag": "49f2e84d6486e12ff68cf1ec43ee66d3"
        },
        "emwoindexpickupview": {
            "title": "工单数据选择视图",
            "caption": "工单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOIndexPickupView",
            "viewtag": "4a2fa2737712ee534a0502031dbb4b6f"
        },
        "emwo_eneditview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENEditView",
            "viewtag": "4a903038091d04051a9a4e006312555b"
        },
        "emitemtradetreeexpview": {
            "title": "物品交易树导航视图",
            "caption": "物品交易",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTradeTreeExpView",
            "viewtag": "4b969cde25eb0874a7aa7a38ff1d82b0"
        },
        "emwplistcostconfirmgridview9": {
            "title": "询价单表格视图",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListCostConfirmGridView9",
            "viewtag": "4c8528d153eba80ddba5cb1d644a0591"
        },
        "pfuniteditview": {
            "title": "计量单位",
            "caption": "计量单位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFUNITEditView",
            "viewtag": "4d1641867ab84096c5996c78ba56070d"
        },
        "emeqsparemapeditview_editmode": {
            "title": "备件包引用",
            "caption": "备件包引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareMapEditView_EditMode",
            "viewtag": "4dabff2562c3bfc073c38a4ab33a6eca"
        },
        "emeqmppickupview": {
            "title": "设备仪表数据选择视图",
            "caption": "设备仪表",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPPickupView",
            "viewtag": "4dceb916933fd7c51dda2ceafcccb512"
        },
        "emwplistcosteditview9": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostEditView9",
            "viewtag": "4df1109de543f926257d1c32e7fc53b0"
        },
        "emrfocapickupview": {
            "title": "原因数据选择视图",
            "caption": "原因",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOCAPickupView",
            "viewtag": "4dfde07b484fab438363b02dd0348793"
        },
        "emeqmpmtrgridview": {
            "title": "设备仪表读数表格视图",
            "caption": "设备仪表读数",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMTRGridView",
            "viewtag": "4e2fd1c162041c4f06b88aba71d584ed"
        },
        "emstoreparteditview_7215": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStorePartEditView_7215",
            "viewtag": "4f4244adc92f1504235ed8c59211c9e1"
        },
        "emwopickupgridview": {
            "title": "工单选择表格视图",
            "caption": "工单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOPickupGridView",
            "viewtag": "4fb41066362194e1d35664a060f64799"
        },
        "emeqcheckeditview9": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQCHECKEditView9",
            "viewtag": "500207009d71cecb020cf263397b500d"
        },
        "emeqlctgssgridview_cqyj": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_yj",
            "viewname": "EMEQLCTGSSGridView_CQYJ",
            "viewtag": "502552e88f019afbab9ebf8c53070d10"
        },
        "emitempusedrafteditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseDraftEditView9",
            "viewtag": "502f6483cbc49cf1faf4b7c3df519aa9"
        },
        "emitemrinpickupgridview": {
            "title": "入库单选择表格视图",
            "caption": "入库单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINPickupGridView",
            "viewtag": "509bfed4d9fede0b165b92970ff8b133"
        },
        "emwooripickupview": {
            "title": "工单来源数据选择视图",
            "caption": "工单来源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOORIPickupView",
            "viewtag": "50cc35f9e4e3daa351112c6f68c23ea3"
        },
        "emitempuseeditview9_editmode": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseEditView9_EditMode",
            "viewtag": "5108a63df0ab1cd61348549c64c069b8"
        },
        "emwocalendarview": {
            "title": "工单日历视图",
            "caption": "工单",
            "viewtype": "DECALENDARVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOCalendarView",
            "viewtag": "53d75eb263d54d235c0da11105639f16"
        },
        "emeqsparemapgridview": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREMAPGridView",
            "viewtag": "552138461c902f2a1cb8554453391857"
        },
        "emitemrineditview9": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINEditView9",
            "viewtag": "552147bb15bbb8300acad5a559a95ea3"
        },
        "emwplistpodgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListPodGridView",
            "viewtag": "56568b39e9179fe0b12f352c6490221f"
        },
        "emitemtypepickuptreeview": {
            "title": "物品类型选择树视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPTREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypePickupTreeView",
            "viewtag": "56ecad33ce61fb67a9daecb447004f2d"
        },
        "emservicedraftgridview": {
            "title": "服务商",
            "caption": "服务商审批-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceDraftGridView",
            "viewtag": "579e32e3f2e03af2af039d05613098f8"
        },
        "emmachinecategorypickupgridview": {
            "title": "机种编号选择表格视图",
            "caption": "机种编号",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHINECATEGORYPickupGridView",
            "viewtag": "57d80263c00e369ddbd278061a9981ee"
        },
        "emeqlctfdjeditview": {
            "title": "发动机位置",
            "caption": "发动机位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTFDJEditView",
            "viewtag": "58e6dd7c0fe5752c2b317451e629d89d"
        },
        "empodetailcloseddetailgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailClosedDetailGridView",
            "viewtag": "5a1845eb3feb87e0fe29956f8d6e29b0"
        },
        "emeqmppickupgridview": {
            "title": "设备仪表选择表格视图",
            "caption": "设备仪表",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPPickupGridView",
            "viewtag": "5a322b7e71d91c113657a576853dd13a"
        },
        "emplancardview": {
            "title": "计划数据视图",
            "caption": "计划",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanCardView",
            "viewtag": "5af189f6dea6e37151bdb37e24897145"
        },
        "emwo_enwovieweditview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENWOViewEditView",
            "viewtag": "5b0140697252eddda4dbf4e1a66cdc6e"
        },
        "emitemtypeeditview_itemtype_editmode": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_itemtype_EditMode",
            "viewtag": "5b63ed70ed1ca8ebc671669aac5e63ba"
        },
        "emitemtypeeditview_itemtype": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_itemtype",
            "viewtag": "5be4cac9f53d75e0eca3b5987e0e2677"
        },
        "emitemeditview_editmode": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMEditView_EditMode",
            "viewtag": "5c0a65559afbe6fb674bc9fd36d8e193"
        },
        "emeqtypeoptionview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeOptionView",
            "viewtag": "5c4ac2e2dba36f35760f6c206f3b4e5f"
        },
        "emapplyredirectview": {
            "title": "外委申请数据重定向视图",
            "caption": "外委申请",
            "viewtype": "DEREDIRECTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyRedirectView",
            "viewtag": "5c9af52de9f23dc0e64f1bce3609ec38"
        },
        "emitempuseeditview9_new": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseEditView9_New",
            "viewtag": "5de6771dc62975a70c1d13a4ce989ab6"
        },
        "emitemrouteditview9_editmode": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemROutEditView9_EditMode",
            "viewtag": "5f6047533888d42e8c5dedf0a3b4cc6a"
        },
        "emoutputgridview": {
            "title": "能力",
            "caption": "能力",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTGridView",
            "viewtag": "5f6e6cc85bd7fc451ef61a99b49787f1"
        },
        "emitemtabexpview": {
            "title": "物品分页导航视图",
            "caption": "物品",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTabExpView",
            "viewtag": "60330d8050c9f91a156a6703579fb40b"
        },
        "emeqsparemapgridview9": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSPAREMAPGridView9",
            "viewtag": "609b218c1dd4dbe4307505f2bd53c435"
        },
        "emitempleditview": {
            "title": "损溢单编辑视图",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPLEditView",
            "viewtag": "6181715e6cd0436c6933472a358eec0b"
        },
        "empodetailpickupview": {
            "title": "订单条目数据选择视图",
            "caption": "订单条目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILPickupView",
            "viewtag": "61a889ee67b58613b54261c8b316e865"
        },
        "emequippickupgridview": {
            "title": "设备档案选择表格视图",
            "caption": "设备档案",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipPickupGridView",
            "viewtag": "624d3a245a821df7a36d57f38cd864d7"
        },
        "emitempuseeditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseEditView9",
            "viewtag": "6278a8755e7c057e90b7f419dffab2b7"
        },
        "emitempldraftgridview": {
            "title": "损溢单",
            "caption": "损溢单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLDraftGridView",
            "viewtag": "62e28fcacfabf0c4d80cc97d1270956e"
        },
        "emeqlocationeditview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLocationEditView",
            "viewtag": "63040d0edb19f18ef80a6d3438adf02f"
        },
        "embrandpickupview": {
            "title": "品牌数据选择视图",
            "caption": "品牌",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBRANDPickupView",
            "viewtag": "63a7edd572cad31943b6be0ee736c806"
        },
        "emoutputeditview9": {
            "title": "能力信息",
            "caption": "能力信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTEditView9",
            "viewtag": "63ba7a93ba5ecf63930d366e13ceb109"
        },
        "emplantempleditview_editmode": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanTemplEditView_EditMode",
            "viewtag": "63fad1adc19625c9890d9fd2e1ef37e1"
        },
        "emwo_innerwovieweditview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERWOViewEditView",
            "viewtag": "657b8397c50938a3f3d636353a0d332c"
        },
        "emserviceevleditview": {
            "title": "服务商评估编辑视图",
            "caption": "服务商评估",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlEditView",
            "viewtag": "65d43977b26303b1d274d2ac46133167"
        },
        "emeqaheqcalendarexpview": {
            "title": "设备活动日历导航视图",
            "caption": "活动历史",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHEqCalendarExpView",
            "viewtag": "66ce65d2786eb2f62372b4e73ff0a728"
        },
        "emitempltabexpview": {
            "title": "损溢单分页导航视图",
            "caption": "损溢单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLTabExpView",
            "viewtag": "6710ded1610085281d4901168c056032"
        },
        "emwplistpickupgridview": {
            "title": "采购申请选择表格视图",
            "caption": "采购申请",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTPickupGridView",
            "viewtag": "6830863d50cf63d8314dd2deb129cd89"
        },
        "emeqcheckcaleditview9": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQCheckCalEditView9",
            "viewtag": "6857fec47fc1a673fac07954629f47ac"
        },
        "emeqwleqgridview": {
            "title": "设备运行日志表格视图",
            "caption": "设备运行日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLEqGridView",
            "viewtag": "6a8f435e39b455b8b33169028d5bcd3e"
        },
        "emeqahgridview": {
            "title": "活动历史表格视图",
            "caption": "活动历史",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHGridView",
            "viewtag": "6bba1fc5e156e5aaa94eb350637fb08d"
        },
        "emeqkprcdgridview": {
            "title": "关键点记录表格视图",
            "caption": "关键点记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPRCDGridView",
            "viewtag": "6bc4ce8ac2641ebe0407ac32f2986ae5"
        },
        "emoutputrcteditview9": {
            "title": "产能信息",
            "caption": "产能信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTRCTEditView9",
            "viewtag": "6d01bf1a43648232f9fb53382d238b4f"
        },
        "emplantemplpickupgridview": {
            "title": "计划模板选择表格视图",
            "caption": "计划模板",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLPickupGridView",
            "viewtag": "6d54e2dce05866a44e5d6344d8d88970"
        },
        "emassetcleareditview": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEAREditView",
            "viewtag": "6e677f2108b1752ad846374540ad4ff8"
        },
        "pfempgridview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEmpGridView",
            "viewtag": "6e7c5ea7db0b8ea4c2596f311efdd845"
        },
        "emstoreparteditview_9836": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREPARTEditView_9836",
            "viewtag": "6ed353dafbdb80061264857e413f75a2"
        },
        "emwo_dpeditview_editmode": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPEditView_EditMode",
            "viewtag": "6fb5ca35e0d59463f44bac912db60e41"
        },
        "emacclasspickupview": {
            "title": "总帐科目数据选择视图",
            "caption": "总帐科目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMACCLASSPickupView",
            "viewtag": "7016b925f12b320d5c647f897e38fa5b"
        },
        "emeqlocationalllocgridview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationAllLocGridView",
            "viewtag": "71b43e5369cdfe0770c39fde6dfa8e0e"
        },
        "emeqsparelistexpview": {
            "title": "备件包列表导航视图",
            "caption": "备件包",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareListExpView",
            "viewtag": "71b4edf7cdd4b9d3478087870e7e68be"
        },
        "pfempeditview_editmode": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEmpEditView_EditMode",
            "viewtag": "71c750515090559d69b3c0db7a1c3fd9"
        },
        "emapplyconfirmedgridview": {
            "title": "外委申请表格视图",
            "caption": "外委申请-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyConfirmedGridView",
            "viewtag": "72f2f809f9620542b6acb201a20eebad"
        },
        "emitemcsgridview": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemCSGridView",
            "viewtag": "730aceca5f8b20e95fcdffa2472b4a8a"
        },
        "emberthpickupview": {
            "title": "泊位数据选择视图",
            "caption": "泊位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBERTHPickupView",
            "viewtag": "7475e2606e2a7347b47d5a79ca3b99e5"
        },
        "emeqahcalendarexpview": {
            "title": "活动历史日历导航视图",
            "caption": "活动历史",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHCalendarExpView",
            "viewtag": "748b93fda990b371e4d2f30b4fa1f5ed"
        },
        "emrfodeeditview": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEEditView",
            "viewtag": "748d4667e6d4ccba6195f0751c0b3243"
        },
        "emassetclasspickupgridview": {
            "title": "资产类别选择表格视图",
            "caption": "资产类别",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSPickupGridView",
            "viewtag": "74be11e87e6ffc79e17704796ef68b87"
        },
        "emenconsumeditview9_editmode": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMEditView9_EditMode",
            "viewtag": "75471a76d775ace2f58a2929c23b583d"
        },
        "emitemtypeitemtreeexpview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeItemTreeExpView",
            "viewtag": "755d8b776122b66fe066883bea8d4758"
        },
        "emitemroutgridview": {
            "title": "退货单",
            "caption": "退货单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemROutGridView",
            "viewtag": "7664a4e46421531f39ec23d90df57fdd"
        },
        "emitemplgridview": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPLGridView",
            "viewtag": "766d76ae92a58c976054a9831e5c5179"
        },
        "emwo_oscdraftgridview": {
            "title": "外委保养工单表格视图",
            "caption": "外委工单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCDraftGridView",
            "viewtag": "769d403fee61b28dc20e8381b80e3864"
        },
        "emitemgridview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemGridView",
            "viewtag": "76c13839742af42179aa9b3fd629c82c"
        },
        "pfunitpickupview": {
            "title": "计量单位数据选择视图",
            "caption": "计量单位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFUNITPickupView",
            "viewtag": "7735f11e9c3fec6b77e10e7a6a43c1ea"
        },
        "emitemtypeeditview": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMTYPEEditView",
            "viewtag": "7768979ed866e114bb7bb0842f62f6b4"
        },
        "emitemringridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemRInGridView",
            "viewtag": "77d480c55593b8d58451b51efe5813ce"
        },
        "emplantempleditview": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLEditView",
            "viewtag": "7884acf2b8b18d9a50e12942f3128228"
        },
        "emwo_dpconfirmedgridview": {
            "title": "点检工单表格视图",
            "caption": "点检工单-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPConfirmedGridView",
            "viewtag": "78e9c7af7d6711e0558d453815350786"
        },
        "emberthpickupgridview": {
            "title": "泊位选择表格视图",
            "caption": "泊位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBERTHPickupGridView",
            "viewtag": "7a126db5c05bd4b29cab0a6a55b114f3"
        },
        "emdrwgeditview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGEditView",
            "viewtag": "7a9c3ab625972c38c06940cd10257de6"
        },
        "emitemrouteditview9_new": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutEditView9_New",
            "viewtag": "7ac89ff0c5ca55542b9876c2b374d2c6"
        },
        "empoeditview9_editmode": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPOEditView9_EditMode",
            "viewtag": "7b1569e8338f5ee6aa54a3553ad3e19c"
        },
        "emeqlcttirespickupgridview": {
            "title": "轮胎位置选择表格视图",
            "caption": "轮胎位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTTIRESPickupGridView",
            "viewtag": "7b869dc8094e17d8288b824d30df7bce"
        },
        "emitemsupplyinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemSupplyInfoView",
            "viewtag": "7ba54a011959c3ea3a5baf92e94eb14c"
        },
        "emwoindexpickupdataview": {
            "title": "工单索引关系选择数据视图",
            "caption": "工单",
            "viewtype": "DEINDEXPICKUPDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOIndexPickupDataView",
            "viewtag": "7bb9f753ee9f57cf1393c6ba98e5c853"
        },
        "empodetaileditview9": {
            "title": "采购订单条目",
            "caption": "采购订单条目",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILEditView9",
            "viewtag": "7be5e771850c9c4d7f6df6a13b70fcec"
        },
        "emeqsetupcaleditview9": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSetupCalEditView9",
            "viewtag": "7c0a47fb86fdf9cdc9e80e4b91c21bb4"
        },
        "emwplistcostgridview": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListCostGridView",
            "viewtag": "7d0b8f0446a8cffee3b89f45a73cddfe"
        },
        "emeqlcttireseditview": {
            "title": "轮胎位置",
            "caption": "轮胎位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTTIRESEditView",
            "viewtag": "7d3a52b65d5c2bf9277956b302537016"
        },
        "pfcontractpickupgridview": {
            "title": "合同选择表格视图",
            "caption": "合同",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTPickupGridView",
            "viewtag": "7d8ab68f17958ba6c18636889a50eede"
        },
        "emwo_innereditview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNEREditView",
            "viewtag": "7e9489faf90675cd1b3d1054bb4f503d"
        },
        "emrfodedashboardview9": {
            "title": "现象数据看板视图",
            "caption": "现象",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEDashboardView9",
            "viewtag": "7e94985f9897365ba210b5415ddf5c93"
        },
        "emeqcheckeditview": {
            "title": "维修记录编辑视图",
            "caption": "维修记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQCHECKEditView",
            "viewtag": "7f3e81d98edf2a47786fbe32981a13c1"
        },
        "emeqlocationpickupview": {
            "title": "位置数据选择视图",
            "caption": "位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONPickupView",
            "viewtag": "7fb82463d76fcdbfd7275eea8bb7b0ac"
        },
        "emrfodepickupview": {
            "title": "现象数据选择视图",
            "caption": "现象",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEPickupView",
            "viewtag": "80221890668d8913600c5db871486b97"
        },
        "emwplistwaitpogridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWaitPoGridView",
            "viewtag": "806d7a88ab47572fe9e1aae54cb433a3"
        },
        "emrfomogridview": {
            "title": "模式",
            "caption": "模式",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOMOGridView",
            "viewtag": "8135285403a076359c132c2a995861d2"
        },
        "emplantemplgridview": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanTemplGridView",
            "viewtag": "818dae9a24ae673ffa7ecae81e941d70"
        },
        "empoplaceordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPlaceOrderGridView",
            "viewtag": "81c57e20fee46d4fef9a18dc3cec482a"
        },
        "emacclasspickupgridview": {
            "title": "总帐科目选择表格视图",
            "caption": "总帐科目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMACCLASSPickupGridView",
            "viewtag": "82686d401a6da9cae639f8fd63aa4d0f"
        },
        "emwplistpickupview": {
            "title": "采购申请数据选择视图",
            "caption": "采购申请",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTPickupView",
            "viewtag": "826b9bc8847e18dcee5e1e3eacf560a3"
        },
        "emitemcsconfirmedgridview": {
            "title": "调整单",
            "caption": "调整单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSConfirmedGridView",
            "viewtag": "82967f7d4a3126b28f1597b67824641e"
        },
        "emitemprtnconfirmedgridview": {
            "title": "还料单",
            "caption": "还料单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnConfirmedGridView",
            "viewtag": "84958c0db69ada49e5e90b1e45ae5d90"
        },
        "empodetaileditview9_editmode": {
            "title": "采购订单条目",
            "caption": "采购订单条目",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILEditView9_EditMode",
            "viewtag": "84d120de0de83cb37cb771bd0c4b5d81"
        },
        "pfdeptpickupgridview": {
            "title": "部门选择表格视图",
            "caption": "部门",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFDeptPickupGridView",
            "viewtag": "85f2674d89aa3b8e671a395ce5b2cb2f"
        },
        "emservicegridview": {
            "title": "服务商表格视图",
            "caption": "服务商",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceGridView",
            "viewtag": "86204b5f5f55b366f03344bb7879ddb6"
        },
        "emitemrouteditview": {
            "title": "退货单编辑视图",
            "caption": "退货单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMROUTEditView",
            "viewtag": "86ced5558f8e999137787fdba75542ee"
        },
        "emeqkeepeditview9": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQKEEPEditView9",
            "viewtag": "86e56ab704d36902d2aae7d637137f20"
        },
        "emstockpickupgridview": {
            "title": "库存选择表格视图",
            "caption": "库存",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockPickupGridView",
            "viewtag": "86ef0a3afe602ae2a25ca237b6d4f1e8"
        },
        "emplaneqinfo": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanEQInfo",
            "viewtag": "87016050a52fc831545c659ede007ce9"
        },
        "emitemprtndraftgridview": {
            "title": "还料单",
            "caption": "还料单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnDraftGridView",
            "viewtag": "87a6125547cab555e77c23783ed6ede6"
        },
        "emwplisteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListEditView9",
            "viewtag": "88d143d54405b4be360898038f9b063d"
        },
        "emitemprtneditview": {
            "title": "还料单编辑视图",
            "caption": "还料单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPRTNEditView",
            "viewtag": "8934d09effcfcb9e671c4b6985a4e866"
        },
        "emwooripickupgridview": {
            "title": "工单来源选择表格视图",
            "caption": "工单来源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOORIPickupGridView",
            "viewtag": "8a1837155cd47ac9f3f0642a57fca79e"
        },
        "emeqdebugeditview9": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQDEBUGEditView9",
            "viewtag": "8a885752e9a423481ad308d090fcee3c"
        },
        "emservicetabexpview": {
            "title": "服务商分页导航视图",
            "caption": "服务商",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceTabExpView",
            "viewtag": "8be9c1cd2f19480433b485ba1ba9ce4a"
        },
        "emitempleditview9": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLEditView9",
            "viewtag": "8bf8ee1be774fa0a1d65fc7bfc72a8b2"
        },
        "emeqsetupeditview9": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSETUPEditView9",
            "viewtag": "8c749d4a8356b239f620c4a255a841b4"
        },
        "emitemcseditview": {
            "title": "库间调整单编辑视图",
            "caption": "库间调整单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMCSEditView",
            "viewtag": "8cee95d3c188a9c2f3d80feb1b8df45a"
        },
        "emplanpersoninfo": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanPersonInfo",
            "viewtag": "8d7cb8aa5c4b1610df5496e17b45b5d7"
        },
        "emwo_engridview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENGridView",
            "viewtag": "8ea8aa006c78935c7458076c256ed7f5"
        },
        "emeqspareeditview_editmode": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareEditView_EditMode",
            "viewtag": "8eff83f01a100457d3fdca36f73d2c2f"
        },
        "emstorepickupview": {
            "title": "仓库数据选择视图",
            "caption": "仓库",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPickupView",
            "viewtag": "8f2059ccc34ca18dc2a99d22a5e08cad"
        },
        "pfcontractpickupview": {
            "title": "合同数据选择视图",
            "caption": "合同",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTPickupView",
            "viewtag": "8f347c7ccb2077909f31dee8567670cf"
        },
        "emwplistcostgridview9": {
            "title": "询价单表格视图",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostGridView9",
            "viewtag": "8f6ebd50823b9699bb236852630bbe91"
        },
        "emwplistwaitcostgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWaitCostGridView",
            "viewtag": "8f803db56612f140a8aa1db7e20cb083"
        },
        "emwo_dpwovieweditview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPWOViewEditView",
            "viewtag": "8fe19057740a8133e7d31ade73d2daf1"
        },
        "emwotreeexpview": {
            "title": "工单树导航视图",
            "caption": "工单",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOTreeExpView",
            "viewtag": "9049845cbefcb1d6090c3dd11c79f3f3"
        },
        "emeqlcttirespickupview": {
            "title": "轮胎位置数据选择视图",
            "caption": "轮胎位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTTIRESPickupView",
            "viewtag": "90bdc3d4449bdcbe11978314fff1ba85"
        },
        "emitemtypetreepickupview": {
            "title": "物品类型数据选择视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeTreePickupView",
            "viewtag": "90bf4b8e6b158a78bb5d8dfebfb8c134"
        },
        "emserviceeditview": {
            "title": "服务商编辑视图",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMSERVICEEditView",
            "viewtag": "90d8b7da6f21a045122d4ea0c64fa29c"
        },
        "emeqtypepickuptreeview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEPICKUPTREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypePickupTreeView",
            "viewtag": "914a020cb21ac8bd1c4907426f54d425"
        },
        "emservicefileview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>附件信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceFileView9",
            "viewtag": "91bcb09663c995dc401910837f85dd3c"
        },
        "emeqtypegridview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeGridView",
            "viewtag": "9203b7b7233acc8a849ae1fe0751b71d"
        },
        "emeqmaintancecaleditview9": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQMaintanceCalEditView9",
            "viewtag": "92c6d8367243f17c036c555ccd0685f5"
        },
        "emitemtypegridexpview": {
            "title": "物品类型表格导航视图",
            "caption": "物品类型",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeGridExpView",
            "viewtag": "9356eabf7983cc8ef55caa36139d7894"
        },
        "emeqsparedetailtabexpview": {
            "title": "备件包明细分页导航视图",
            "caption": "备件包明细",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareDetailTabExpView",
            "viewtag": "935b5026e8ed2eaf101678127b4c886d"
        },
        "emwopickupview": {
            "title": "工单数据选择视图",
            "caption": "工单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOPickupView",
            "viewtag": "93b80c8e1720f441ef0b4c82bb913cd8"
        },
        "emwo_innertabexpview": {
            "title": "内部工单分页导航视图",
            "caption": "内部工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERTabExpView",
            "viewtag": "9420cc9679f9c817cd6443c3174f9c25"
        },
        "emwo_dpgridview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPGridView",
            "viewtag": "942d9a3df08e8481023353a4c8d5a214"
        },
        "emeqdebugcaleditview9": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQDebugCalEditView9",
            "viewtag": "94c2eda321740f009f0857c9a1a938a3"
        },
        "emitemprtneditview9_new": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnEditView9_New",
            "viewtag": "955e1bb8f139a40db83ac9006f28658e"
        },
        "emapplygridview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyGridView",
            "viewtag": "98517f1365f57372a1eccd6cd684269c"
        },
        "emrfoaceditview": {
            "title": "方案信息",
            "caption": "方案信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOACEditView",
            "viewtag": "98e75fc068ba39de5f5c6bebadaed1f1"
        },
        "emwplistcostpickupgridview": {
            "title": "询价单选择表格视图",
            "caption": "询价单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTCOSTPickupGridView",
            "viewtag": "99349c6bd9c65e81311f710e78c1b142"
        },
        "emenconsumeditview9": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMENCONSUMEditView9",
            "viewtag": "9a94ab9f4c627afad7415703f3b8f355"
        },
        "emeqspareeditview": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareEditView",
            "viewtag": "9ad8bf408db9d42fb34d6d78043761a1"
        },
        "emoutputrcteditview": {
            "title": "产能信息",
            "caption": "产能信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTRCTEditView",
            "viewtag": "9cd35c9bc3e545cee773e0cea07d56c1"
        },
        "emengridview": {
            "title": "能源",
            "caption": "能源",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENGridView",
            "viewtag": "9e5c37bde166a6f60c577c250232219b"
        },
        "emeqlctgsstabexpview": {
            "title": "钢丝绳位置超期预警",
            "caption": "钢丝绳位置超期预警",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSTabExpView",
            "viewtag": "9e62cb01fbbe5363b01a110b91022c17"
        },
        "emeqlocationoptionview": {
            "title": "位置新建",
            "caption": "位置新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationOptionView",
            "viewtag": "9edfff48da0f53879ff0dd78f482ef93"
        },
        "emitempleditview9_editmode": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPLEditView9_EditMode",
            "viewtag": "a02dc89fa9fd209c5d026768a845da45"
        },
        "emeqtypegridexpview": {
            "title": "设备类型表格导航视图",
            "caption": "设备类型",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeGridExpView",
            "viewtag": "a19dc5bb2d1b58402ed06fd884af186e"
        },
        "pfdeptgridview": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFDeptGridView",
            "viewtag": "a1a05460920a396fe3d9e34b86359395"
        },
        "empoeditview": {
            "title": "订单编辑视图",
            "caption": "订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPOEditView",
            "viewtag": "a24e9444423b5704e24aa1c1f9a85958"
        },
        "emitemrineditview9_editmode": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMRINEditView9_EditMode",
            "viewtag": "a2778f6b969251f5760dcaf8d167bf48"
        },
        "emserviceevltabexpview": {
            "title": "服务商评估分页导航视图",
            "caption": "服务商评估",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlTabExpView",
            "viewtag": "a2783bd8977552b60dec3e905852e094"
        },
        "emeqsparedetaileditview_editmode": {
            "title": "备件包明细",
            "caption": "备件包明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILEditView_EditMode",
            "viewtag": "a3005005ff60e18b70b799f1dc550b73"
        },
        "emserviceevlgridview9": {
            "title": "服务商评估",
            "caption": "服务商评估",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlGridView9",
            "viewtag": "a305d37d554d07d048a4dcbb84a634ec"
        },
        "empoeditview9": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPOEditView9",
            "viewtag": "a34bb9c8c4dfd43234dce4be299bd300"
        },
        "emwogridview": {
            "title": "工单表格视图",
            "caption": "工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOGridView",
            "viewtag": "a3efc0d77c8f9a3de0b4383f3deed8f0"
        },
        "emobjmaplocationtreeview": {
            "title": "位置信息",
            "caption": "位置信息",
            "viewtype": "DETREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMObjMapLocationTreeView",
            "viewtag": "a47832b9d879fe6dd889f3d00ccfe8bc"
        },
        "emequipgridview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEquipGridView",
            "viewtag": "a4e9d7a917afd5362f23f4bca68e200d"
        },
        "emenpickupview": {
            "title": "能源数据选择视图",
            "caption": "能源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENPickupView",
            "viewtag": "a58fef197d0fc4d1c59e4749e8df6f5d"
        },
        "emitemtypeeditview_editmode": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_EditMode",
            "viewtag": "a5c5f73147356ef7d84421cf98e9249d"
        },
        "pfemppickupview": {
            "title": "职员数据选择视图",
            "caption": "职员",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEMPPickupView",
            "viewtag": "a6123b46ec5bcf787990953c77183655"
        },
        "emworedirectview": {
            "title": "工单数据重定向视图",
            "caption": "工单",
            "viewtype": "DEREDIRECTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWORedirectView",
            "viewtag": "a7684cbce4182b622c8e22309b187e41"
        },
        "emitempusepickupview": {
            "title": "领料单数据选择视图",
            "caption": "领料单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEPickupView",
            "viewtag": "a86a260215306f31bbd6eeda64adc877"
        },
        "emeqmaintancegridview": {
            "title": "抢修记录",
            "caption": "抢修记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQMaintanceGridView",
            "viewtag": "a92d07c9e3394f4480e7f4d855cc7b6f"
        },
        "emplangridview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanGridView",
            "viewtag": "a9aa8c74f995dc76e5a84bc69dc7a09d"
        },
        "emeqmonitormonitoreditview": {
            "title": "设备状态监控编辑视图",
            "caption": "设备状态监控",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMonitorMonitorEditView",
            "viewtag": "aa068da88f32981d6652442e3a254ec1"
        },
        "emeqsparedetailgridview": {
            "title": "备件物品",
            "caption": "备件物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILGridView",
            "viewtag": "aa5fa85d7744eaebafcf48d098bffb5c"
        },
        "emequipeditview_editmode": {
            "title": "基本信息",
            "caption": "基本信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEquipEditView_EditMode",
            "viewtag": "aa666a468a06fdfeabce4cf5a62f4a53"
        },
        "emeqlctgsspickupgridview": {
            "title": "钢丝绳位置选择表格视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSPickupGridView",
            "viewtag": "ac51f4b22aececba1896b8410f2400d0"
        },
        "emeqlctrhyeditview": {
            "title": "润滑油位置",
            "caption": "润滑油位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTRHYEditView",
            "viewtag": "ac6e3f3bd886ba1148a012eec84bf4f2"
        },
        "emenpickupgridview": {
            "title": "能源选择表格视图",
            "caption": "能源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENPickupGridView",
            "viewtag": "ac7b784ccafb738b22b9987e8d8bb114"
        },
        "emplandetailgridview": {
            "title": "计划步骤",
            "caption": "计划步骤",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANDETAILGridView",
            "viewtag": "ac9bb351e3dba8f091c44e7786eb02bc"
        },
        "emitempusepickupgridview": {
            "title": "领料单选择表格视图",
            "caption": "领料单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEPickupGridView",
            "viewtag": "acf29e56426bf396fba28fe99fdb866e"
        },
        "emitemtypeoptionview": {
            "title": "物品类型选项操作视图",
            "caption": "物品类型",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeOptionView",
            "viewtag": "acfcc9a7037bd5113703381688f0bf28"
        },
        "emitemprtntabexpview": {
            "title": "还料单分页导航视图",
            "caption": "还料单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnTabExpView",
            "viewtag": "add22def704924475ab10c8be6ace5b0"
        },
        "emitempusetabexpview": {
            "title": "领料单分页导航视图",
            "caption": "领料单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseTabExpView",
            "viewtag": "adda1a8a1008b45ec3ec3f18e6b3d783"
        },
        "emwo_osctabexpview": {
            "title": "外委保养工单分页导航视图",
            "caption": "外委保养工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCTabExpView",
            "viewtag": "ae1aa0b3e079475b6ea9674f59c61233"
        },
        "emitemtypetreeexpview": {
            "title": "物品类型树导航视图",
            "caption": "物品类型",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeTreeExpView",
            "viewtag": "aec7c9eacce53b0676bd7cae6b582556"
        },
        "emwo_innergridview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNERGridView",
            "viewtag": "af0e2452822a162c66e34bd0d3a19ea0"
        },
        "emitempleditview9_new": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLEditView9_New",
            "viewtag": "af78185ed15aced32ccdb856ab18800f"
        },
        "emenconsumeditview": {
            "title": "能耗编辑视图",
            "caption": "能耗",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMEditView",
            "viewtag": "af8646eb833927fc245f5796095f348f"
        },
        "emplantemplpickupview": {
            "title": "计划模板数据选择视图",
            "caption": "计划模板",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLPickupView",
            "viewtag": "af9d8c60ce06ba3603b22614943cbb1f"
        },
        "pfcontracteditview9_editmode": {
            "title": "合同信息",
            "caption": "合同信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "PFCONTRACTEditView9_EditMode",
            "viewtag": "b0045626f4cfbdd8b57721097bb8cefc"
        },
        "emwo_dptoconfirmgridview": {
            "title": "点检工单表格视图",
            "caption": "点检工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPToConfirmGridView",
            "viewtag": "b12a7720cec635a9a1578bb6039d9a96"
        },
        "emwplistcancelgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCancelGridView",
            "viewtag": "b13e101ad1443892d4be795b4bd4a75d"
        },
        "emeqsparedetaileditview": {
            "title": "备件包明细",
            "caption": "备件包明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILEditView",
            "viewtag": "b1c6777053675a11a1fe388bfc4628a5"
        },
        "empurplanpickupgridview": {
            "title": "计划修理选择表格视图",
            "caption": "计划修理",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPURPLANPickupGridView",
            "viewtag": "b2092712bdbacc5a50f7d573f6e7b04a"
        },
        "emequipeditview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQUIPEditView",
            "viewtag": "b254fe1d5b26dd3886f058a7cddcce0f"
        },
        "emasseteditview9": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMASSETEditView9",
            "viewtag": "b320f16ca6c24804c84ee1932e70a7cf"
        },
        "emequipeditview2": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW2",
            "viewmodule": "eam_core",
            "viewname": "EMEquipEditView2",
            "viewtag": "b3367b62370679aa51992b9e0b33d111"
        },
        "emserviceinfoview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>基本信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceInfoView9",
            "viewtag": "b3b417c94c38d6298e4735fd625cd8e9"
        },
        "emenconsumpickupview": {
            "title": "能耗数据选择视图",
            "caption": "能耗",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMPickupView",
            "viewtag": "b55cf53524c4186283d9c122f3353c9c"
        },
        "emstoregridview_7848": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreGridView_7848",
            "viewtag": "b562196e1ed0c13d4cd044ab4d709de3"
        },
        "empodetailpickupgridview": {
            "title": "订单条目选择表格视图",
            "caption": "订单条目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILPickupGridView",
            "viewtag": "b5856be28a055635fa82ccc842d1baa4"
        },
        "emitempuseissuedgridview": {
            "title": "领料单",
            "caption": "领料单-已发料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseIssuedGridView",
            "viewtag": "b5c9664bedbbcbe686bbe677059534ed"
        },
        "emassetclasseditview": {
            "title": "ASSET资产类别信息",
            "caption": "ASSET资产类别信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSEditView",
            "viewtag": "b69cb790c1f5a61a9a42b7ce7b57cefc"
        },
        "emeqsetupeditview": {
            "title": "更换安装编辑视图",
            "caption": "更换安装",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPEditView",
            "viewtag": "b724cb66129d92f24acf807429c1605a"
        },
        "emeqsparegridview": {
            "title": "备件包",
            "caption": "备件包",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREGridView",
            "viewtag": "b7aaaa3e31c2e7c46ca0feb1adb54602"
        },
        "emplancdtgridview": {
            "title": "计划条件",
            "caption": "计划条件",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANCDTGridView",
            "viewtag": "b7c90a58ce6c082c5fad41f3c27aad92"
        },
        "emeqlctmapeditview": {
            "title": "位置关系编辑视图",
            "caption": "位置关系",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTMapEditView",
            "viewtag": "b7fce7bb684df2ed02a90aa19383d7fc"
        },
        "emwplisteditview9_editmode": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListEditView9_EditMode",
            "viewtag": "b847bd180b226a72ff29542975cb23f7"
        },
        "emserviceevlconfirmedgridview": {
            "title": "服务商评估",
            "caption": "服务商评估-已完成",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlConfirmedGridView",
            "viewtag": "b84bde120417204a204834c4694e16c2"
        },
        "emenconsumpickupgridview": {
            "title": "能耗选择表格视图",
            "caption": "能耗",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMPickupGridView",
            "viewtag": "b86b997f6fb361731b53e29d08557fb6"
        },
        "emwplistpoeditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListPOEditView9",
            "viewtag": "b89a04d208a822c7beac6eb000ce5b0c"
        },
        "pfdepteditview_editmode": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFDeptEditView_EditMode",
            "viewtag": "b93682392a777cbfaeaefd9e9f172e2a"
        },
        "emstockbystorepartgridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByStorePartGridView",
            "viewtag": "bbb344cd621ba67d4dcf1fbb6b236bfe"
        },
        "emplancdteditview": {
            "title": "计划条件",
            "caption": "计划条件",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANCDTEditView",
            "viewtag": "bbc65441377739da2d77916b646dd70a"
        },
        "emstoreparteditview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREPARTEditView",
            "viewtag": "bc319c9209e9f335091052750f7e1b69"
        },
        "emequiptabexpview": {
            "title": "设备档案分页导航视图",
            "caption": "设备档案",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipTabExpView",
            "viewtag": "bc356258e1901571397cf49dc6544990"
        },
        "emassetgridview": {
            "title": "固定资产台账",
            "caption": "固定资产台账",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETGridView",
            "viewtag": "bc9d00f607e2000b1b5a172a71520d7f"
        },
        "emplandashboardview": {
            "title": "计划数据看板视图",
            "caption": "计划",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanDashboardView",
            "viewtag": "bca4b3cd0229443bc02feea015f7de0a"
        },
        "emrfodequickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEQuickView",
            "viewtag": "bd45b689975018f06258516944c89c3f"
        },
        "emwplistdraftgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListDraftGridView",
            "viewtag": "bde1b77bd059da16604e86e878b11d72"
        },
        "emwplistgridview_ydh": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTGridView_Ydh",
            "viewtag": "bdfcb67170db78d009e92a3eb2e57774"
        },
        "pfempeditview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEMPEditView",
            "viewtag": "be0bbc94760e637fc6020302265d05e2"
        },
        "emeqkppickupview": {
            "title": "设备关键点数据选择视图",
            "caption": "设备关键点",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPPickupView",
            "viewtag": "be10622b09c9bb6768d4826fb707c9d5"
        },
        "emrfodeeditview9": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEEditView9",
            "viewtag": "be109eac97029563943b8b94f6857db6"
        },
        "emassetcleareditview_908": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEAREditView_908",
            "viewtag": "be2aa30daf8afefdd2678354f72afadb"
        },
        "pfunitgridview": {
            "title": "计量单位",
            "caption": "计量单位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFUNITGridView",
            "viewtag": "bec533d34e217347615a8b242a08b8de"
        },
        "emservicedashboardview": {
            "title": "综合评估",
            "caption": "综合评估",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceDashboardView",
            "viewtag": "bf640bd21a518e5c94e42532e6d09102"
        },
        "appportalview": {
            "title": "应用门户视图",
            "caption": "",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "r7rt_dyna",
            "viewname": "AppPortalView",
            "viewtag": "c081125f98377205ff3d73a0b08c044f"
        },
        "pfemptreeexpview": {
            "title": "职员树导航视图",
            "caption": "职员",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpTreeExpView",
            "viewtag": "c0ffd1a64ee278e3a0d5083d6d5da9cd"
        },
        "emitemrouttabexpview": {
            "title": "退货单分页导航视图",
            "caption": "退货单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutTabExpView",
            "viewtag": "c17f22907dc7efe8a85e1e203e43de0f"
        },
        "emwo_osctoconfirmgridview": {
            "title": "外委保养工单表格视图",
            "caption": "外委工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCToConfirmGridView",
            "viewtag": "c24958575e192a431c2405aa4d36fcae"
        },
        "emeqlctgsseditview9_editmode": {
            "title": "钢丝绳位置编辑视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_yj",
            "viewname": "EMEQLCTGSSEditView9_EditMode",
            "viewtag": "c3074577e619044ffb339a4f65bbfe07"
        },
        "emeqsparepickupview": {
            "title": "备件包数据选择视图",
            "caption": "备件包",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREPickupView",
            "viewtag": "c34866637c2982d192c601dec0848b8b"
        },
        "emassetclasspickupview": {
            "title": "资产类别数据选择视图",
            "caption": "资产类别",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSPickupView",
            "viewtag": "c379b2f47d4dd23617aa44ed9350ead9"
        },
        "emassetpickupgridview": {
            "title": "资产选择表格视图",
            "caption": "资产",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETPickupGridView",
            "viewtag": "c37cc465f06ab2e97e128b8e52883bcf"
        },
        "pfcontractgridview": {
            "title": "合同",
            "caption": "合同",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "PFCONTRACTGridView",
            "viewtag": "c3a16c92e483c5db51a1453821a8de82"
        },
        "emeqlctgsstreeexpview": {
            "title": "钢丝绳位置树导航",
            "caption": "钢丝绳位置树导航",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSTreeExpView",
            "viewtag": "c3e594cee0ee5850e34a7e2947f310dc"
        },
        "emitemrineditview9_new": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemRInEditView9_New",
            "viewtag": "c579d0f23bd0308b3af2ee80904ae8f2"
        },
        "emeneditview9": {
            "title": "能源信息",
            "caption": "能源信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMENEditView9",
            "viewtag": "c601e8736c48c191cf2740871a10440a"
        },
        "emapplyeqgridview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyEqGridView",
            "viewtag": "c62517f6a868389ea644de4b8790d4ba"
        },
        "emserviceeditview9_editmode": {
            "title": "PUR服务商信息",
            "caption": "PUR服务商信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEditView9_EditMode",
            "viewtag": "c79df131f664b758d3e8ddb529474c06"
        },
        "emitemrinpickupview": {
            "title": "入库单数据选择视图",
            "caption": "入库单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINPickupView",
            "viewtag": "c87da32eb30d04c1d169fd5a8e0b8785"
        },
        "emitemprtngridview": {
            "title": "还料单",
            "caption": "还料单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPRtnGridView",
            "viewtag": "c95ea1be897e96ff4a6ada5a442a7623"
        },
        "emitemtradegridview": {
            "title": "物品交易表格视图",
            "caption": "物品交易",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMTRADEGridView",
            "viewtag": "ca5cd44873cd8430b40b5e41f7a35345"
        },
        "emitemcseditview9": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSEditView9",
            "viewtag": "ca98345de8ab6a9449837d8902911ced"
        },
        "emeqtypeeditview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTYPEEditView",
            "viewtag": "cab4c2ee7c1f254af30d488c67f111bf"
        },
        "emserviceconfirmedgridview": {
            "title": "服务商",
            "caption": "服务商审批-已审",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceConfirmedGridView",
            "viewtag": "cab8ad86db4c1b855682bfc66b502748"
        },
        "emeqlctrhygridview": {
            "title": "润滑油位置",
            "caption": "润滑油位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTRHYGridView",
            "viewtag": "cbc85b568266a8900a647f28d3ad0ba0"
        },
        "emservicepickupview": {
            "title": "服务商数据选择视图",
            "caption": "服务商",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSERVICEPickupView",
            "viewtag": "cdfe5a6d58226ddd299d2cb375ea5d7d"
        },
        "emstoreeditview_editmode": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreEditView_EditMode",
            "viewtag": "ce213bd09fa0f212143e0851e524666e"
        },
        "emassetclasseditview_4778": {
            "title": "ASSET资产类别信息",
            "caption": "ASSET资产类别信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSEditView_4778",
            "viewtag": "ced46b2425a7dfee81a5dbb378e0a4f1"
        },
        "emstorepartoptionview": {
            "title": "仓库库位选项操作视图",
            "caption": "仓库库位",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStorePartOptionView",
            "viewtag": "cfd7741276197e36fd625f9b9b2d88d8"
        },
        "emequippickupview": {
            "title": "设备档案数据选择视图",
            "caption": "设备档案",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQUIPPickupView",
            "viewtag": "d087abf090bddc28dd98b6f18cffdd6d"
        },
        "emstockbycabgridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByCabGridView",
            "viewtag": "d140f5a95827b139d413cf9b18773dd2"
        },
        "emequipallgridview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipAllGridView",
            "viewtag": "d1d6cb9b2d57a256bf318cadc8283fee"
        },
        "emeqlocationtreeexpview": {
            "title": "位置树导航视图",
            "caption": "位置",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationTreeExpView",
            "viewtag": "d352394004ff4ccbd04efa771ac4de31"
        },
        "emeqwlgridview": {
            "title": "设备运行日志表格视图",
            "caption": "设备运行日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLGridView",
            "viewtag": "d356942c19f630e557776f680e09fcc1"
        },
        "pfteameditview_editmode": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFTEAMEditView_EditMode",
            "viewtag": "d4f94decb7184690e0e42e50f508d40c"
        },
        "eamindexview": {
            "title": "设备资产管理首页视图",
            "caption": "设备资产管理",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "eam_core",
            "viewname": "EAMIndexView",
            "viewtag": "d4fc933d0652ea3cc51652584dedea85"
        },
        "emitempuseeditview": {
            "title": "领料单编辑视图",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPUSEEditView",
            "viewtag": "d532258598a5c0d1ea8b0175cc67de2c"
        },
        "emeqtypepickupgridview": {
            "title": "设备类型选择表格视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTYPEPickupGridView",
            "viewtag": "d538a88bd80b36139966529b8dbf583a"
        },
        "emservicepickupgridview": {
            "title": "服务商选择表格视图",
            "caption": "服务商",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSERVICEPickupGridView",
            "viewtag": "d593b2d80c234760dfbf636a42f75be8"
        },
        "emproducteditview9": {
            "title": "试用品信息",
            "caption": "试用品信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPRODUCTEditView9",
            "viewtag": "d679c28852d1b3f8b0a30ba2077a54b7"
        },
        "emstockeditview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStockEditView",
            "viewtag": "d7b44c1faa4fc6b72aebd8b2c614bfb7"
        },
        "emeqtypetreeexpview2": {
            "title": "设备类型",
            "caption": "设备档案",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeTreeExpView2",
            "viewtag": "d7f2a4b86b6ae7aa14fcc934df27a4d7"
        },
        "emdrwgoptionview": {
            "title": "文档选项操作视图",
            "caption": "文档",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGOptionView",
            "viewtag": "d86ec0fbc00d232370aee5ee83457a76"
        },
        "emeqcheckgridview": {
            "title": "维修记录",
            "caption": "维修记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQCheckGridView",
            "viewtag": "d8eae04a6f51db5bf686622a970795f0"
        },
        "emeqdebugeditview9_editmode": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQDEBUGEditView9_EditMode",
            "viewtag": "d90f00306c4fcdfb13d65e7a717f2885"
        },
        "emitemprtneditview9": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnEditView9",
            "viewtag": "d94f5d129f7936e5f5c571bf2068be15"
        },
        "emeitirespickupgridview": {
            "title": "轮胎清单选择表格视图",
            "caption": "轮胎清单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEITIRESPickupGridView",
            "viewtag": "d99dd7cfd98eac4ea5385c1b6da2201e"
        },
        "empoclosedordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOClosedOrderGridView",
            "viewtag": "d9bee0a4048fb812d2a851938602427b"
        },
        "emrfocagridview": {
            "title": "原因",
            "caption": "原因",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOCAGridView",
            "viewtag": "da122d5a100dff78f3f76a6201e27f0f"
        },
        "emrfodeeditview9_edit": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEEditView9_Edit",
            "viewtag": "da89f6980abbc42db880db5b05470e65"
        },
        "emwo_innereditview_editmode": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNEREditView_EditMode",
            "viewtag": "daaaa16c3f1f29f70e6352aef85b22ef"
        },
        "emrfocapickupgridview": {
            "title": "原因选择表格视图",
            "caption": "原因",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOCAPickupGridView",
            "viewtag": "dac2c85d395c1d0a1afbe5cc39f45816"
        },
        "emitemstoreinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemStoreInfoView",
            "viewtag": "db592a4b543ef2ea1fb91b6805f68a6a"
        },
        "emeqtypeeditview_editmode": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeEditView_EditMode",
            "viewtag": "db9b2fb852bb259bd500089beef19c61"
        },
        "empodetailgridview9": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailGridView9",
            "viewtag": "dc2f773a5e407787be4a68cd2f29247c"
        },
        "empodetailwaitcheckgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailWaitCheckGridView",
            "viewtag": "dcccd4c62cf07ba28f6071787d460762"
        },
        "emwplistcosteditview9_editmode": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTCOSTEditView9_EditMode",
            "viewtag": "de84daa41e0fc0f58294103ec30af059"
        },
        "emrfodetypegridview": {
            "title": "现象分类",
            "caption": "现象分类",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODETYPEGridView",
            "viewtag": "defd8a4e03a63ebc27e506e4199f3200"
        },
        "emeqsetupgridview": {
            "title": "更换安装",
            "caption": "更换安装",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSetupGridView",
            "viewtag": "defeeb7ee5b3358def1997a85ff6289c"
        },
        "emitempickupgridview": {
            "title": "物品选择表格视图",
            "caption": "物品",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPickupGridView",
            "viewtag": "df7ae7ee10a5abf90410c87e17e65ac4"
        },
        "emdrwgmapeditview": {
            "title": "文档引用编辑视图",
            "caption": "文档引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGMapEditView",
            "viewtag": "e00d5e38d3661d0d5d1797d313e16f8f"
        },
        "embrandpickupgridview": {
            "title": "品牌选择表格视图",
            "caption": "品牌",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBRANDPickupGridView",
            "viewtag": "e034a24d119e4532e57d5e28f9b1e0e3"
        },
        "emenconsumgridview": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENConsumGridView",
            "viewtag": "e061fe672e4d11586e531ad4fa55ce41"
        },
        "emstorepartgridview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStorePartGridView",
            "viewtag": "e118656a010369e81d161e507441de04"
        },
        "emapplyeditview_editmode": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyEditView_EditMode",
            "viewtag": "e1b47f8f3d7a6fe6f40571d16857cc7b"
        },
        "emitemrineditview": {
            "title": "入库单编辑视图",
            "caption": "入库单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMRINEditView",
            "viewtag": "e26d7053bd5ea2d2fd678e1f7731ad4a"
        },
        "emservicefinanceview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>财务信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceFinanceView9",
            "viewtag": "e2c05d3cdcf1bc87579a6f3416edac21"
        },
        "emequipruninfo": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipRunInfo",
            "viewtag": "e2c2c943106301828b1c52e8793a2176"
        },
        "emplanpickupgridview": {
            "title": "计划选择表格视图",
            "caption": "计划",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANPickupGridView",
            "viewtag": "e54b992c55f6f3aa9b7b632737840c92"
        },
        "emitempusegridview": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseGridView",
            "viewtag": "e5c10121dc26d75c7da94d9bd571d3da"
        },
        "emrfoacpickupview": {
            "title": "方案数据选择视图",
            "caption": "方案",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOACPickupView",
            "viewtag": "e65d9e21d03473e58b782d7d671bc276"
        },
        "emeqdebuggridview": {
            "title": "事故记录",
            "caption": "事故记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQDebugGridView",
            "viewtag": "e65dcbab6eb072d8431d53f2f345732d"
        },
        "emobjectpickupgridview": {
            "title": "对象选择表格视图",
            "caption": "对象",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOBJECTPickupGridView",
            "viewtag": "e695986e78ad685904b96a6505b1ddca"
        },
        "emproductgridview": {
            "title": "试用品",
            "caption": "试用品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPRODUCTGridView",
            "viewtag": "e6c618d665717e070647d0c23d83d480"
        },
        "emassetgridview_bf": {
            "title": "报废资产",
            "caption": "报废资产",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETGridView_BF",
            "viewtag": "e6ddb704fbc23386585a8a5d43be267a"
        },
        "emeqsparequickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareQuickView",
            "viewtag": "e7364391d0c70ff537c5be5d3f10dfa0"
        },
        "emitemcstoconfirmgridview": {
            "title": "调整单",
            "caption": "调整单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSToConfirmGridView",
            "viewtag": "e7887647c56c421d25f83d5ba21402e6"
        },
        "emeqkpkpeditview": {
            "title": "设备关键点编辑视图",
            "caption": "设备关键点",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPKPEditView",
            "viewtag": "e7c9f7b124d1db97aff0f33511cbd104"
        },
        "emeqsparemapeditview": {
            "title": "备件包引用",
            "caption": "备件包引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREMAPEditView",
            "viewtag": "e82a39260dbfb2f73d8be6e71a52c2d7"
        },
        "emservicetoconfirmgridview": {
            "title": "服务商",
            "caption": "服务商审批-待审",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceToConfirmGridView",
            "viewtag": "e85d625f0d17a4817c530edcfe7e9402"
        },
        "emitemtypegridview": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeGridView",
            "viewtag": "e86320543443824a7479f216d4eb84dd"
        },
        "emeqmpmtrmpmtreditview": {
            "title": "设备仪表读数编辑视图",
            "caption": "设备仪表读数",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMTRMpMtrEditView",
            "viewtag": "e98c492508d458d79387030b61b6dd31"
        },
        "emwo_entabexpview": {
            "title": "能耗登记工单分页导航视图",
            "caption": "能耗登记工单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENTabExpView",
            "viewtag": "e9a83d27146867bf19a3167f79bcb93b"
        },
        "emassettabexpview": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETTabExpView",
            "viewtag": "e9ae358b11db60121bb5e47e8dcb53e6"
        },
        "emeqmonitorgridview": {
            "title": "设备状态监控表格视图",
            "caption": "设备状态监控",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMonitorGridView",
            "viewtag": "e9ba2fe172aa93f79346e2c03900bc7e"
        },
        "emeitirespickupview": {
            "title": "轮胎清单数据选择视图",
            "caption": "轮胎清单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEITIRESPickupView",
            "viewtag": "eacd73a421bbceea58188203dc9405fc"
        },
        "emeqwlwleditview": {
            "title": "设备运行日志编辑视图",
            "caption": "设备运行日志",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLWLEditView",
            "viewtag": "ec0312bd2a639c08b3094b1f6b20ad0d"
        },
        "emstoreeditview": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREEditView",
            "viewtag": "ec1172970db8fbec8885a98ccc48c63a"
        },
        "emeqsparemapdataview9": {
            "title": "备件包引用数据视图",
            "caption": "备件包引用",
            "viewtype": "DEDATAVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapDataView9",
            "viewtag": "ecd16633f1a4f160824ee020d81a2b47"
        },
        "emassetcleargridview_5564": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMAssetClearGridView_5564",
            "viewtag": "ecf2279c9f44fd6596006a64cbffc14b"
        },
        "emitempltoconfirmgridview": {
            "title": "损溢单",
            "caption": "损溢单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLToConfirmGridView",
            "viewtag": "edac923b4905d9c4a6e087881de8a58c"
        },
        "empodetailgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILGridView",
            "viewtag": "edaf26978aab85f5ab5dc6359e9655f3"
        },
        "emeqahtreeexpview": {
            "title": "活动历史树导航视图",
            "caption": "活动历史",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHTreeExpView",
            "viewtag": "eddb77a067db7fc2f6334994fe71766b"
        },
        "emeqsparemaindashboardview9": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMainDashboardView9",
            "viewtag": "ee0078ab458bfa82654013369f231ff7"
        },
        "emmachinecategorypickupview": {
            "title": "机种编号数据选择视图",
            "caption": "机种编号",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHINECATEGORYPickupView",
            "viewtag": "ee492c577145764bb0989359ae32389e"
        },
        "emeqsparemapequipgridview9": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapEquipGridView9",
            "viewtag": "ee7e4f1105b7a8d49865dce1ecc8c8e3"
        },
        "pfempdeptempgridview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpDeptEmpGridView",
            "viewtag": "eeec3c5c46e35e4f1f0fc0a34eb98430"
        },
        "emwplisteditview": {
            "title": "采购申请编辑视图",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTEditView",
            "viewtag": "ef4429451a203d96e773fb741067651c"
        },
        "pfteamgridview": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFTEAMGridView",
            "viewtag": "efbee2b8d270bc72b36a2078a0e30f7d"
        },
        "emwo_innertoconfirmgridview": {
            "title": "内部工单表格视图",
            "caption": "内部工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERToConfirmGridView",
            "viewtag": "f1da0428ebbea71e9c702a00fb9693e4"
        },
        "emeqtypetreeexpview": {
            "title": "设备类型",
            "caption": "设备档案",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeTreeExpView",
            "viewtag": "f20fe1c25cbe71af2d010714d457fb76"
        },
        "emplanequipgridview9": {
            "title": "计划表格视图",
            "caption": "计划",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPlanEquipGridView9",
            "viewtag": "f260202db7d4c8dc6682203f6065ea39"
        },
        "emdrwgmapequipgridview9": {
            "title": "文档引用表格视图",
            "caption": "文档引用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGMapEquipGridView9",
            "viewtag": "f2c8226b1bc64d2a61bb930256ff8107"
        },
        "emapplytabexpview": {
            "title": "外委申请分页导航视图",
            "caption": "外委申请",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyTabExpView",
            "viewtag": "f2f9d266225351e3a2789bfdc0ebbbc8"
        },
        "emitemrinwaitingridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemRInWaitInGridView",
            "viewtag": "f3190fca73848a4abef70ac8cda986c6"
        },
        "emapplyeditview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyEditView",
            "viewtag": "f366a3e8143913a8f057703ca7661570"
        },
        "emeqdebugeditview": {
            "title": "事故记录编辑视图",
            "caption": "事故记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQDEBUGEditView",
            "viewtag": "f4005b33455b908b8d9aacb411cff7d9"
        },
        "emwplistingridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListInGridView",
            "viewtag": "f4dc3178eec2de4fa221e2d1e7039413"
        },
        "emobjectpickupview": {
            "title": "对象数据选择视图",
            "caption": "对象",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOBJECTPickupView",
            "viewtag": "f5364bc2c7121b5c69e1c1078834cf27"
        },
        "emserviceevleditview9": {
            "title": "服务商评估信息",
            "caption": "服务商评估信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlEditView9",
            "viewtag": "f6d2173e0ae33dbece6bdfdeaf95d1ee"
        },
        "emwplistwpprocesstreeexpview": {
            "title": "采购申请树导航视图",
            "caption": "采购申请",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWpProcessTreeExpView",
            "viewtag": "f81772d450a3ab9c1feff292677c36c6"
        },
        "emserviceevldraftgridview": {
            "title": "服务商评估",
            "caption": "服务商评估-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlDraftGridView",
            "viewtag": "f85dd6d3e49ebd448d5d26b96f25c492"
        },
        "emitemprtntoconfirmgridview": {
            "title": "还料单",
            "caption": "还料单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnToConfirmGridView",
            "viewtag": "f96f9d02b87e4abaeb0f9bf39652b2a9"
        },
        "emitempickupview": {
            "title": "物品数据选择视图",
            "caption": "物品",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPickupView",
            "viewtag": "fa04156d05b0d18ead9b60506caa0b2a"
        },
        "emeqkeepeditview9_editmode": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQKEEPEditView9_EditMode",
            "viewtag": "facdfc0f727bfac0ee9a9022bdf871a1"
        },
        "emwo_osceditview_editmode": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCEditView_EditMode",
            "viewtag": "fc8feaff3126201ebe2e5e6012c98864"
        },
        "emeneditview": {
            "title": "能源编辑视图",
            "caption": "能源",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENEditView",
            "viewtag": "fcad8f2a88f17850ec89613d841c8044"
        },
        "emapplydraftgridview": {
            "title": "外委申请表格视图",
            "caption": "外委申请-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyDraftGridView",
            "viewtag": "fd685e2bf7add896097137ac4375a6d5"
        },
        "emitemcstabexpview": {
            "title": "库间调整单分页导航视图",
            "caption": "库间调整单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSTabExpView",
            "viewtag": "fdff4fa9c40fbd3e00f3d72eccaa2b1e"
        },
        "emwo_entoconfirmgridview": {
            "title": "能耗登记工单表格视图",
            "caption": "能耗工单-执行中",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENToConfirmGridView",
            "viewtag": "ffe9e17048677d8ad86ac390dbbb5e66"
        }
    }];
});

// 获取视图消息分组信息
mock.onGet('./assets/json/view-message-group.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
    }];
});