import qs from 'qs';
import { MockAdapter } from '@/mock/mock-adapter';
const mock = MockAdapter.getInstance();

// 模拟数据
const mockDatas: Array<any> = [
];


//getwflink
mock.onGet(new RegExp(/^\/wfcore\/eam-app-dynaweb\/emwplists\/[a-zA-Z0-9\-\;]+\/usertasks\/[a-zA-Z0-9\-\;]+\/ways$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: getwflink");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status,[
        {"sequenceFlowId":"dfdsfdsfdsfdsfds","sequenceFlowName":"同意",
         "taskId":"aaaaddddccccddddd","processDefinitionKey":"support-workorders-approve-v1",
         "processInstanceId":"ddlfldldfldsfds","refViewKey":""},
        {"sequenceFlowId":"ddssdfdfdfdfsfdf","sequenceFlowName":"不同意",
         "taskId":"aaaaddddccccddddd","processDefinitionKey":"support-workorders-approve-v1",
         "processInstanceId":"ddfdsldlfdlldsf","refViewKey":"workorder_ltform_editview"}
        ]];
});

// getwfstep
mock.onGet(new RegExp(/^\/wfcore\/eam-app-dynaweb\/emwplists\/process-definitions-nodes$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: getwfstep");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, [
        {"userTaskId":"sddfddfd-dfdf-fdfd-fdf-dfdfd",
        "userTaskName":"待审",
        "cnt":0,
        "processDefinitionKey":"support-workorders-approve-v1",
        "processDefinitionName":"工单审批流程v1"
        },
        {"userTaskId":"sddfddfd-dfdf-fdfd-fdf-87927",
        "userTaskName":"待分配",
        "cnt":3,
        "processDefinitionKey":"support-workorders-approve-v1",
        "processDefinitionName":"工单审批流程v1"}
        ]];
});

// createBatch
mock.onPost(new RegExp(/^\/emwplists\/batch$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: createBatch");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, {}];
});

// updateBatch
mock.onPut(new RegExp(/^\/emwplists\/batch$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: updateBatch");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, {}];
});

// removeBatch
mock.onDelete(new RegExp(/^\/emwplists\/batch$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: removeBatch");
    console.table({url:config.url, method: config.method, data:config.data});
    console.groupEnd();
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, {}];
    }
    return [status, {}];
});



// Select
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Select");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items);
    console.groupEnd();
    console.groupEnd();
    return [status, _items];
});


// Select
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Select");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items);
    console.groupEnd();
    console.groupEnd();
    return [status, _items];
});


// Select
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Select");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items);
    console.groupEnd();
    console.groupEnd();
    return [status, _items];
});


// Select
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Select");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items);
    console.groupEnd();
    console.groupEnd();
    return [status, _items];
});


// Select
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Select");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items);
    console.groupEnd();
    console.groupEnd();
    return [status, _items];
});

// Select
mock.onGet(new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Select");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/select$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items);
    console.groupEnd();
    console.groupEnd();
    return [status, _items];
});

    
// Create
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Create");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Create
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Create");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Create
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Create");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Create
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Create");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Create
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Create");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// Create
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Create");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas[0]);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas[0]];
});

    
// Update
mock.onPut(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Update");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Update
mock.onPut(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Update");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Update
mock.onPut(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Update");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Update
mock.onPut(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Update");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Update
mock.onPut(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Update");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// Update
mock.onPut(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Update");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});












// GetDraft
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/getdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    // GetDraft
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});


// GetDraft
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/getdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    // GetDraft
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});


// GetDraft
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/getdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    // GetDraft
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});


// GetDraft
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/getdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    // GetDraft
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});


// GetDraft
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/getdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    // GetDraft
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

// GetDraft
mock.onGet(new RegExp(/^\/emwplists\/getdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    // GetDraft
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckKey
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckKey");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckKey
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckKey");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckKey
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckKey");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckKey
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckKey");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckKey
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckKey");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// CheckKey
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/checkkey$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckKey");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkkey$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});

    
// CheckValue
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckValue");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckValue
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckValue");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckValue
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckValue");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckValue
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckValue");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// CheckValue
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckValue");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// CheckValue
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/checkvalue$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: CheckValue");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/checkvalue$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});

    
// Confirm
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Confirm");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Confirm
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Confirm");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Confirm
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Confirm");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Confirm
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Confirm");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Confirm
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Confirm");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// Confirm
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/confirm$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Confirm");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/confirm$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});

    
// FillCosted
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FillCosted");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FillCosted
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FillCosted");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FillCosted
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FillCosted");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FillCosted
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FillCosted");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FillCosted
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FillCosted");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// FillCosted
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/fillcosted$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FillCosted");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/fillcosted$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});

    
// FormUpdateByAempid
mock.onPut(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FormUpdateByAempid");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FormUpdateByAempid
mock.onPut(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FormUpdateByAempid");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FormUpdateByAempid
mock.onPut(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FormUpdateByAempid");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FormUpdateByAempid
mock.onPut(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FormUpdateByAempid");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// FormUpdateByAempid
mock.onPut(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FormUpdateByAempid");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// FormUpdateByAempid
mock.onPut(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/formupdatebyaempid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FormUpdateByAempid");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/formupdatebyaempid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});

    
// GenId
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenId");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenId
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenId");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenId
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenId");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenId
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenId");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenId
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenId");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// GenId
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/genid$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenId");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genid$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});

    
// GenPO
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenPO");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenPO
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenPO");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenPO
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenPO");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenPO
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenPO");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// GenPO
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenPO");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// GenPO
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/genpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GenPO");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/genpo$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});






    
// Save
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Save");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Save
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Save");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Save
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Save");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Save
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Save");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Save
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Save");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// Save
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/save$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Save");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/save$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});

    
// Submit
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Submit");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Submit
mock.onPost(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Submit");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Submit
mock.onPost(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Submit");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Submit
mock.onPost(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Submit");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});

    
// Submit
mock.onPost(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Submit");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table({});
    console.groupEnd();
    console.groupEnd();
    return [status, {}];
});
        
// Submit
mock.onPost(new RegExp(/^\/emwplists\/?([a-zA-Z0-9\-\;]{0,35})\/submit$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Submit");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/submit$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    //let items = mockDatas ? mockDatas : [];
    //let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
      let data = JSON.parse(config.data);
    mockDatas.forEach((item)=>{
        if(item['emwplistid'] == tempValue['emwplistid'] ){
            for(let value in data){
              if(item.hasOwnProperty(value)){
                  item[value] = data[value];
              }
            }
        }
    })
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(data);
    console.groupEnd();
    console.groupEnd();
    return [status, data];
});


// FetchCancel
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchCancel");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchCancel
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchCancel");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchCancel
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchCancel");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchCancel
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchCancel");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchCancel
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchCancel");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchcancel$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchCancel
mock.onGet(new RegExp(/^\/emwplists\/fetchcancel$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchCancel");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchCancel
mock.onGet(new RegExp(/^\/emwplists\/fetchcancel(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchCancel");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchConfimCost
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchConfimCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchConfimCost
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchConfimCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchConfimCost
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchConfimCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchConfimCost
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchConfimCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchConfimCost
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchConfimCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchconfimcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchConfimCost
mock.onGet(new RegExp(/^\/emwplists\/fetchconfimcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchConfimCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchConfimCost
mock.onGet(new RegExp(/^\/emwplists\/fetchconfimcost(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchConfimCost");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchDefault
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDefault");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDefault
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDefault");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDefault
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDefault");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDefault
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDefault");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDefault
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDefault");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdefault$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchDefault
mock.onGet(new RegExp(/^\/emwplists\/fetchdefault$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDefault");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchDefault
mock.onGet(new RegExp(/^\/emwplists\/fetchdefault(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDefault");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchDraft
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDraft
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDraft
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDraft
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchDraft
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchdraft$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchDraft
mock.onGet(new RegExp(/^\/emwplists\/fetchdraft$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchDraft
mock.onGet(new RegExp(/^\/emwplists\/fetchdraft(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchDraft");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchIn
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchIn");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchIn
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchIn");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchIn
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchIn");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchIn
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchIn");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchIn
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchIn");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchin$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchIn
mock.onGet(new RegExp(/^\/emwplists\/fetchin$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchIn");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchIn
mock.onGet(new RegExp(/^\/emwplists\/fetchin(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchIn");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchMain6
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchMain6
mock.onGet(new RegExp(/^\/emwplists\/fetchmain6$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchMain6
mock.onGet(new RegExp(/^\/emwplists\/fetchmain6(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchMain6_8692
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6_8692");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6_8692
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6_8692");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6_8692
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6_8692");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6_8692
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6_8692");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchMain6_8692
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6_8692");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchmain6_8692$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchMain6_8692
mock.onGet(new RegExp(/^\/emwplists\/fetchmain6_8692$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6_8692");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchMain6_8692
mock.onGet(new RegExp(/^\/emwplists\/fetchmain6_8692(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchMain6_8692");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchWaitCost
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitCost
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitCost
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitCost
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitCost
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitcost$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchWaitCost
mock.onGet(new RegExp(/^\/emwplists\/fetchwaitcost$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitCost");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchWaitCost
mock.onGet(new RegExp(/^\/emwplists\/fetchwaitcost(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitCost");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchWaitPo
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitPo");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitPo
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitPo");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitPo
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitPo");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitPo
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitPo");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWaitPo
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitPo");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwaitpo$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchWaitPo
mock.onGet(new RegExp(/^\/emwplists\/fetchwaitpo$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitPo");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchWaitPo
mock.onGet(new RegExp(/^\/emwplists\/fetchwaitpo(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWaitPo");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});


// FetchWpStateNum
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWpStateNum");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWpStateNum
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWpStateNum");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstoreid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWpStateNum
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWpStateNum");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emstorepartid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWpStateNum
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWpStateNum");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emserviceid','emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});


// FetchWpStateNum
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWpStateNum");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    const paramArray:Array<any> = ['emitemid'];
    let tempValue: any = {};
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/fetchwpstatenum$/).exec(config.url);
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    if (items.length > 0 && paramArray.length > 0) {
        paramArray.forEach((paramkey: any) => {
            if (tempValue[paramkey] && tempValue[paramkey].indexOf(";") > 0) {
                let keysGrounp: Array<any> = tempValue[paramkey].split(new RegExp(/[\;]/));
                let tempArray: Array<any> = [];
                keysGrounp.forEach((singlekey: any) => {
                    let _items =  items.filter((item: any) => { return item[paramkey] == singlekey });
                   if(_items.length >0){
                    tempArray.push(..._items);
                   }
                })
                items = tempArray;
            } else {
                items = items.filter((item: any) => { return item[paramkey] == tempValue[paramkey] });
            }
        })
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(items);
    console.groupEnd();
    console.groupEnd();
    return [status, items];
});
    
// FetchWpStateNum
mock.onGet(new RegExp(/^\/emwplists\/fetchwpstatenum$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWpStateNum");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(mockDatas);
    console.groupEnd();
    console.groupEnd();
    return [status, mockDatas ? mockDatas : []];
});

// FetchWpStateNum
mock.onGet(new RegExp(/^\/emwplists\/fetchwpstatenum(\?[\w-./?%&=,]*)*$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: FetchWpStateNum");
    console.table({url:config.url, method: config.method, data:config.data});
    if(config.url.includes('page')){
        let url = config.url.split('?')[1];
        let params  =  qs.parse(url);
        Object.assign(config, params);
    }
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    let total = mockDatas.length;
    let records: Array<any> = [];
    if(!config.page || !config.size){
        records = mockDatas;
    }else{
        if((config.page-1)*config.size < total){
          records = mockDatas.slice(config.page,config.size);
        }
    }
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(records ?  records : []);
    console.groupEnd();
    console.groupEnd();
    return [status, records ?  records : []];
});

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现
// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现
// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现
// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现
// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现
// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现
// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现

// URI参数传递情况未实现
// URI参数传递情况未实现


// Remove
mock.onDelete(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Remove");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Remove
mock.onDelete(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Remove");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Remove
mock.onDelete(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Remove");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Remove
mock.onDelete(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Remove");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Remove
mock.onDelete(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Remove");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Remove
mock.onDelete(new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Remove");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Get
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Get");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Get
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Get");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Get
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Get");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Get
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Get");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Get
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Get");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// Get
mock.onGet(new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: Get");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// GetREMP
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetREMP");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstoreid','emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// GetREMP
mock.onGet(new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetREMP");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstoreid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstores\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// GetREMP
mock.onGet(new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetREMP");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emstorepartid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emstoreparts\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// GetREMP
mock.onGet(new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetREMP");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emserviceid','emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emservices\/([a-zA-Z0-9\-\;]{1,35})\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// GetREMP
mock.onGet(new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetREMP");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emitemid','emwplistid'];
    const matchArray:any = new RegExp(/^\/emitems\/([a-zA-Z0-9\-\;]{1,35})\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});

// GetREMP
mock.onGet(new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/)).reply((config: any) => {
    console.groupCollapsed("实体:emwplist 方法: GetREMP");
    console.table({url:config.url, method: config.method, data:config.data});
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }    
    const paramArray:Array<any> = ['emwplistid'];
    const matchArray:any = new RegExp(/^\/emwplists\/([a-zA-Z0-9\-\;]{1,35})\/getremp$/).exec(config.url);
    let tempValue: any = {};
    if(matchArray && matchArray.length >1 && paramArray && paramArray.length >0){
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
    }
    let items = mockDatas ? mockDatas : [];
    let _items = items.find((item: any) => Object.is(item.emwplistid, tempValue.emwplistid));
    console.groupCollapsed("response数据  status: "+status+" data: ");
    console.table(_items?_items:{});
    console.groupEnd();
    console.groupEnd();
    return [status, _items?_items:{}];
});
