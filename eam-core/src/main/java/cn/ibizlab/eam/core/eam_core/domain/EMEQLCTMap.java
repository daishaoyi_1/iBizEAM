package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[位置关系]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQLCTMAP_BASE", resultMap = "EMEQLCTMapResultMap")
public class EMEQLCTMap extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 位置关系标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqlctmapid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqlctmapid")
    @JsonProperty("emeqlctmapid")
    private String emeqlctmapid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 位置关系
     */
    @DEField(defaultValue = "VAR_EQLOCATIONID||-||VAR_EQLOCATIONPID")
    @TableField(value = "emeqlctmapname")
    @JSONField(name = "emeqlctmapname")
    @JsonProperty("emeqlctmapname")
    private String emeqlctmapname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 主设备
     */
    @TableField(exist = false)
    @JSONField(name = "majorequipname")
    @JsonProperty("majorequipname")
    private String majorequipname;
    /**
     * 位置代码
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationcode")
    @JsonProperty("eqlocationcode")
    private String eqlocationcode;
    /**
     * 上级位置
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationpname")
    @JsonProperty("eqlocationpname")
    private String eqlocationpname;
    /**
     * 位置描述
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationdesc")
    @JsonProperty("eqlocationdesc")
    private String eqlocationdesc;
    /**
     * 主设备
     */
    @TableField(exist = false)
    @JSONField(name = "majorequipid")
    @JsonProperty("majorequipid")
    private String majorequipid;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    private String eqlocationname;
    /**
     * 位置
     */
    @TableField(value = "eqlocationid")
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    private String eqlocationid;
    /**
     * 上级位置
     */
    @TableField(value = "eqlocationpid")
    @JSONField(name = "eqlocationpid")
    @JsonProperty("eqlocationpid")
    private String eqlocationpid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocationp;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [位置关系]
     */
    public void setEmeqlctmapname(String emeqlctmapname) {
        this.emeqlctmapname = emeqlctmapname;
        this.modify("emeqlctmapname", emeqlctmapname);
    }

    /**
     * 设置 [位置]
     */
    public void setEqlocationid(String eqlocationid) {
        this.eqlocationid = eqlocationid;
        this.modify("eqlocationid", eqlocationid);
    }

    /**
     * 设置 [上级位置]
     */
    public void setEqlocationpid(String eqlocationpid) {
        this.eqlocationpid = eqlocationpid;
        this.modify("eqlocationpid", eqlocationpid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqlctmapid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


