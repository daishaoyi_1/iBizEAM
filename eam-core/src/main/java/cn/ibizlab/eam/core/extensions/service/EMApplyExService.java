package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.EMApplyServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMApply;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Primary;

import java.sql.Timestamp;
import java.util.*;

/**
 * 实体[外委申请] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMApplyExService")
public class EMApplyExService extends EMApplyServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    @Override
    public boolean create(EMApply et) {
        Calendar calendar = Calendar.getInstance();
        // 希望开始时间
        if (et.getApplybdate() == null) {
            et.setApplybdate(new Timestamp(System.currentTimeMillis()));
        }
        // 希望完成时间=希望开始时间+10天
        if (et.getApplyedate() == null && et.getApplybdate() != null) {
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 10);
            et.setApplyedate(new Timestamp(calendar.getTimeInMillis()));
        }
        return super.create(et);
    }
}

