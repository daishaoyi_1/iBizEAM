package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQKPRCD;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPRCDSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQKPRCD] 服务对象接口
 */
public interface IEMEQKPRCDService extends IService<EMEQKPRCD> {

    boolean create(EMEQKPRCD et);
    void createBatch(List<EMEQKPRCD> list);
    boolean update(EMEQKPRCD et);
    void updateBatch(List<EMEQKPRCD> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQKPRCD get(String key);
    EMEQKPRCD getDraft(EMEQKPRCD et);
    boolean checkKey(EMEQKPRCD et);
    boolean save(EMEQKPRCD et);
    void saveBatch(List<EMEQKPRCD> list);
    Page<EMEQKPRCD> searchDefault(EMEQKPRCDSearchContext context);
    List<EMEQKPRCD> selectByKpid(String emeqkpid);
    void removeByKpid(String emeqkpid);
    List<EMEQKPRCD> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQKPRCD> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQKPRCD> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQKPRCD> getEmeqkprcdByIds(List<String> ids);
    List<EMEQKPRCD> getEmeqkprcdByEntities(List<EMEQKPRCD> entities);
}


