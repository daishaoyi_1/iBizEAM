package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWGMap;
/**
 * 关系型数据实体[EMDRWGMap] 查询条件对象
 */
@Slf4j
@Data
public class EMDRWGMapSearchContext extends QueryWrapperContext<EMDRWGMap> {

	private String n_emdrwgmapname_like;//[文档引用]
	public void setN_emdrwgmapname_like(String n_emdrwgmapname_like) {
        this.n_emdrwgmapname_like = n_emdrwgmapname_like;
        if(!ObjectUtils.isEmpty(this.n_emdrwgmapname_like)){
            this.getSearchCond().like("emdrwgmapname", n_emdrwgmapname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_refobjname_eq;//[引用对象]
	public void setN_refobjname_eq(String n_refobjname_eq) {
        this.n_refobjname_eq = n_refobjname_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjname_eq)){
            this.getSearchCond().eq("refobjname", n_refobjname_eq);
        }
    }
	private String n_refobjname_like;//[引用对象]
	public void setN_refobjname_like(String n_refobjname_like) {
        this.n_refobjname_like = n_refobjname_like;
        if(!ObjectUtils.isEmpty(this.n_refobjname_like)){
            this.getSearchCond().like("refobjname", n_refobjname_like);
        }
    }
	private String n_drwgname_eq;//[文档]
	public void setN_drwgname_eq(String n_drwgname_eq) {
        this.n_drwgname_eq = n_drwgname_eq;
        if(!ObjectUtils.isEmpty(this.n_drwgname_eq)){
            this.getSearchCond().eq("drwgname", n_drwgname_eq);
        }
    }
	private String n_drwgname_like;//[文档]
	public void setN_drwgname_like(String n_drwgname_like) {
        this.n_drwgname_like = n_drwgname_like;
        if(!ObjectUtils.isEmpty(this.n_drwgname_like)){
            this.getSearchCond().like("drwgname", n_drwgname_like);
        }
    }
	private String n_refobjid_eq;//[引用对象]
	public void setN_refobjid_eq(String n_refobjid_eq) {
        this.n_refobjid_eq = n_refobjid_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjid_eq)){
            this.getSearchCond().eq("refobjid", n_refobjid_eq);
        }
    }
	private String n_drwgid_eq;//[文档]
	public void setN_drwgid_eq(String n_drwgid_eq) {
        this.n_drwgid_eq = n_drwgid_eq;
        if(!ObjectUtils.isEmpty(this.n_drwgid_eq)){
            this.getSearchCond().eq("drwgid", n_drwgid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emdrwgmapname", query)
            );
		 }
	}
}



