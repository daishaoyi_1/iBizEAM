package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmpPostMap;
import cn.ibizlab.eam.core.eam_pf.filter.PFEmpPostMapSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFEmpPostMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFEmpPostMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[人事关系] 服务对象接口实现
 */
@Slf4j
@Service("PFEmpPostMapServiceImpl")
public class PFEmpPostMapServiceImpl extends ServiceImpl<PFEmpPostMapMapper, PFEmpPostMap> implements IPFEmpPostMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFPostService pfpostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFEmpPostMap et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfemppostmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFEmpPostMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFEmpPostMap et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("pfemppostmapid", et.getPfemppostmapid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfemppostmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFEmpPostMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFEmpPostMap get(String key) {
        PFEmpPostMap et = getById(key);
        if(et == null){
            et = new PFEmpPostMap();
            et.setPfemppostmapid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PFEmpPostMap getDraft(PFEmpPostMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(PFEmpPostMap et) {
        return (!ObjectUtils.isEmpty(et.getPfemppostmapid())) && (!Objects.isNull(this.getById(et.getPfemppostmapid())));
    }
    @Override
    @Transactional
    public boolean save(PFEmpPostMap et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFEmpPostMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFEmpPostMap> list) {
        list.forEach(item->fillParentData(item));
        List<PFEmpPostMap> create = new ArrayList<>();
        List<PFEmpPostMap> update = new ArrayList<>();
        for (PFEmpPostMap et : list) {
            if (ObjectUtils.isEmpty(et.getPfemppostmapid()) || ObjectUtils.isEmpty(getById(et.getPfemppostmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFEmpPostMap> list) {
        list.forEach(item->fillParentData(item));
        List<PFEmpPostMap> create = new ArrayList<>();
        List<PFEmpPostMap> update = new ArrayList<>();
        for (PFEmpPostMap et : list) {
            if (ObjectUtils.isEmpty(et.getPfemppostmapid()) || ObjectUtils.isEmpty(getById(et.getPfemppostmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<PFEmpPostMap> selectByPostid(String pfpostid) {
        return baseMapper.selectByPostid(pfpostid);
    }
    @Override
    public void removeByPostid(String pfpostid) {
        this.remove(new QueryWrapper<PFEmpPostMap>().eq("postid",pfpostid));
    }

	@Override
    public List<PFEmpPostMap> selectByTeamid(String pfteamid) {
        return baseMapper.selectByTeamid(pfteamid);
    }
    @Override
    public void removeByTeamid(String pfteamid) {
        this.remove(new QueryWrapper<PFEmpPostMap>().eq("teamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFEmpPostMap> searchDefault(PFEmpPostMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFEmpPostMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFEmpPostMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(PFEmpPostMap et){
        //实体关系[DER1N_PFEMPPOSTMAP_PFPOST_POSTID]
        if(!ObjectUtils.isEmpty(et.getPostid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFPost post=et.getPost();
            if(ObjectUtils.isEmpty(post)){
                cn.ibizlab.eam.core.eam_pf.domain.PFPost majorEntity=pfpostService.get(et.getPostid());
                et.setPost(majorEntity);
                post=majorEntity;
            }
            et.setPostname(post.getPfpostname());
        }
        //实体关系[DER1N_PFEMPPOSTMAP_PFTEAM_TEAMID]
        if(!ObjectUtils.isEmpty(et.getTeamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam team=et.getTeam();
            if(ObjectUtils.isEmpty(team)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getTeamid());
                et.setTeam(majorEntity);
                team=majorEntity;
            }
            et.setTeamname(team.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFEmpPostMap> getPfemppostmapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFEmpPostMap> getPfemppostmapByEntities(List<PFEmpPostMap> entities) {
        List ids =new ArrayList();
        for(PFEmpPostMap entity : entities){
            Serializable id=entity.getPfemppostmapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPFEmpPostMapService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



