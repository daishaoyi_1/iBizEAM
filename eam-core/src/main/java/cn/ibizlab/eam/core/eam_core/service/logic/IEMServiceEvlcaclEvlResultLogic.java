package cn.ibizlab.eam.core.eam_core.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;

/**
 * 关系型数据实体[caclEvlResult] 对象
 */
public interface IEMServiceEvlcaclEvlResultLogic {

    void execute(EMServiceEvl et) ;

}
