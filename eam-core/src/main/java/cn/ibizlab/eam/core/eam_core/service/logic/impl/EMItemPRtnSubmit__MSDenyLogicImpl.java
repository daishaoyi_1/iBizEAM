package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMItemPRtnSubmit__MSDenyLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;

/**
 * 关系型数据实体[Submit__MSDeny] 对象
 */
@Slf4j
@Service
public class EMItemPRtnSubmit__MSDenyLogicImpl implements IEMItemPRtnSubmit__MSDenyLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService emitemprtnservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService getEmitemprtnService() {
        return this.emitemprtnservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMItemPRtn et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emitemprtnsubmit__msdenydefault", et);
            cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn emitemprtnsubmit__msdenytemp = new cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn();
            kieSession.insert(emitemprtnsubmit__msdenytemp); 
            kieSession.setGlobal("emitemprtnsubmit__msdenytemp", emitemprtnsubmit__msdenytemp);
            kieSession.setGlobal("emitemprtnservice", emitemprtnservice);
            kieSession.setGlobal("iBzSysEmitemprtnDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emitemprtnsubmit__msdeny");

        } catch (Exception e) {
            throw new RuntimeException("执行[行为[Submit]主状态拒绝逻辑]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
