package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPO;
import cn.ibizlab.eam.core.eam_core.filter.EMPOSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPO] 服务对象接口
 */
public interface IEMPOService extends IService<EMPO> {

    boolean create(EMPO et);
    void createBatch(List<EMPO> list);
    boolean update(EMPO et);
    void updateBatch(List<EMPO> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMPO get(String key);
    EMPO getDraft(EMPO et);
    EMPO arrival(EMPO et);
    boolean arrivalBatch(List<EMPO> etList);
    boolean checkKey(EMPO et);
    EMPO placeOrder(EMPO et);
    boolean save(EMPO et);
    void saveBatch(List<EMPO> list);
    Page<EMPO> searchClosedOrder(EMPOSearchContext context);
    Page<EMPO> searchDefault(EMPOSearchContext context);
    Page<EMPO> searchOnOrder(EMPOSearchContext context);
    Page<EMPO> searchPlaceOrder(EMPOSearchContext context);
    List<EMPO> selectByLabserviceid(String emserviceid);
    void removeByLabserviceid(String emserviceid);
    List<EMPO> selectByApprempid(String pfempid);
    void removeByApprempid(String pfempid);
    List<EMPO> selectByFgempid(String pfempid);
    void removeByFgempid(String pfempid);
    List<EMPO> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMPO> selectByZjlempid(String pfempid);
    void removeByZjlempid(String pfempid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPO> getEmpoByIds(List<String> ids);
    List<EMPO> getEmpoByEntities(List<EMPO> entities);
}


