

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMPlanTemplInheritMapping {

    @Mappings({
        @Mapping(source ="emplantemplid",target = "emresrefobjid"),
        @Mapping(source ="emplantemplname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMResRefObj toEmresrefobj(EMPlanTempl minorEntity);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emplantemplid"),
        @Mapping(source ="emresrefobjname" ,target = "emplantemplname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMPlanTempl toEmplantempl(EMResRefObj majorEntity);

    List<EMResRefObj> toEmresrefobj(List<EMPlanTempl> minorEntities);

    List<EMPlanTempl> toEmplantempl(List<EMResRefObj> majorEntities);

}


