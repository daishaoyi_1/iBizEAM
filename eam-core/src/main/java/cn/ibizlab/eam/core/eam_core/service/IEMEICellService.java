package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEICell;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEICell] 服务对象接口
 */
public interface IEMEICellService extends IService<EMEICell> {

    boolean create(EMEICell et);
    void createBatch(List<EMEICell> list);
    boolean update(EMEICell et);
    void updateBatch(List<EMEICell> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEICell get(String key);
    EMEICell getDraft(EMEICell et);
    boolean checkKey(EMEICell et);
    boolean save(EMEICell et);
    void saveBatch(List<EMEICell> list);
    Page<EMEICell> searchDefault(EMEICellSearchContext context);
    List<EMEICell> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMEICell> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEICell> selectByItempuseid(String emitempuseid);
    void removeByItempuseid(String emitempuseid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEICell> getEmeicellByIds(List<String> ids);
    List<EMEICell> getEmeicellByEntities(List<EMEICell> entities);
}


