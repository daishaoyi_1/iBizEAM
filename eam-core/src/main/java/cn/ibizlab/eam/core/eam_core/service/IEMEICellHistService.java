package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEICellHist;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellHistSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEICellHist] 服务对象接口
 */
public interface IEMEICellHistService extends IService<EMEICellHist> {

    boolean create(EMEICellHist et);
    void createBatch(List<EMEICellHist> list);
    boolean update(EMEICellHist et);
    void updateBatch(List<EMEICellHist> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEICellHist get(String key);
    EMEICellHist getDraft(EMEICellHist et);
    boolean checkKey(EMEICellHist et);
    boolean save(EMEICellHist et);
    void saveBatch(List<EMEICellHist> list);
    Page<EMEICellHist> searchDefault(EMEICellHistSearchContext context);
    List<EMEICellHist> selectByEiobjid(String emeicellid);
    void removeByEiobjid(String emeicellid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEICellHist> getEmeicellhistByIds(List<String> ids);
    List<EMEICellHist> getEmeicellhistByEntities(List<EMEICellHist> entities);
}


