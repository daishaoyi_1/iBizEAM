package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[招投标信息]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMBIDINQUIRY_BASE", resultMap = "EMBidinquiryResultMap")
public class EMBidinquiry extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 录入人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 录入时间
     */
    @TableField(value = "adate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "adate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    private Timestamp adate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 价格
     */
    @TableField(value = "tariff")
    @JSONField(name = "tariff")
    @JsonProperty("tariff")
    private String tariff;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 业绩
     */
    @TableField(value = "achievement")
    @JSONField(name = "achievement")
    @JsonProperty("achievement")
    private String achievement;
    /**
     * 招投标信息名称
     */
    @DEField(defaultValue = "招投标信息")
    @TableField(value = "embidinquiryname")
    @JSONField(name = "embidinquiryname")
    @JsonProperty("embidinquiryname")
    private String embidinquiryname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 售后服务
     */
    @TableField(value = "afterservice")
    @JSONField(name = "afterservice")
    @JsonProperty("afterservice")
    private String afterservice;
    /**
     * -
     */
    @TableField(exist = false)
    @JSONField(name = "embidresult")
    @JsonProperty("embidresult")
    private String embidresult;
    /**
     * 资质
     */
    @TableField(value = "natural")
    @JSONField(name = "natural")
    @JsonProperty("natural")
    private String natural;
    /**
     * 招投标信息标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "embidinquiryid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "embidinquiryid")
    @JsonProperty("embidinquiryid")
    private String embidinquiryid;
    /**
     * 技术参数
     */
    @TableField(value = "techparam")
    @JSONField(name = "techparam")
    @JsonProperty("techparam")
    private String techparam;
    /**
     * 录入人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    private String emservicename;
    /**
     * 计划修理
     */
    @TableField(exist = false)
    @JSONField(name = "empurplanname")
    @JsonProperty("empurplanname")
    private String empurplanname;
    /**
     * 服务商
     */
    @TableField(value = "emserviceid")
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    private String emserviceid;
    /**
     * 计划修理
     */
    @TableField(value = "empurplanid")
    @JSONField(name = "empurplanid")
    @JsonProperty("empurplanid")
    private String empurplanid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPurPlan empurplan;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService emservice;



    /**
     * 设置 [录入人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [录入时间]
     */
    public void setAdate(Timestamp adate) {
        this.adate = adate;
        this.modify("adate", adate);
    }

    /**
     * 格式化日期 [录入时间]
     */
    public String formatAdate() {
        if (this.adate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(adate);
    }
    /**
     * 设置 [价格]
     */
    public void setTariff(String tariff) {
        this.tariff = tariff;
        this.modify("tariff", tariff);
    }

    /**
     * 设置 [业绩]
     */
    public void setAchievement(String achievement) {
        this.achievement = achievement;
        this.modify("achievement", achievement);
    }

    /**
     * 设置 [招投标信息名称]
     */
    public void setEmbidinquiryname(String embidinquiryname) {
        this.embidinquiryname = embidinquiryname;
        this.modify("embidinquiryname", embidinquiryname);
    }

    /**
     * 设置 [售后服务]
     */
    public void setAfterservice(String afterservice) {
        this.afterservice = afterservice;
        this.modify("afterservice", afterservice);
    }

    /**
     * 设置 [资质]
     */
    public void setNatural(String natural) {
        this.natural = natural;
        this.modify("natural", natural);
    }

    /**
     * 设置 [技术参数]
     */
    public void setTechparam(String techparam) {
        this.techparam = techparam;
        this.modify("techparam", techparam);
    }

    /**
     * 设置 [录入人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [服务商]
     */
    public void setEmserviceid(String emserviceid) {
        this.emserviceid = emserviceid;
        this.modify("emserviceid", emserviceid);
    }

    /**
     * 设置 [计划修理]
     */
    public void setEmpurplanid(String empurplanid) {
        this.empurplanid = empurplanid;
        this.modify("empurplanid", empurplanid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("embidinquiryid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


