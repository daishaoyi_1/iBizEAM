package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatterySetup;
/**
 * 关系型数据实体[EMEIBatterySetup] 查询条件对象
 */
@Slf4j
@Data
public class EMEIBatterySetupSearchContext extends QueryWrapperContext<EMEIBatterySetup> {

	private String n_empname_eq;//[换装人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[换装人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_activeinfo_like;//[换装信息]
	public void setN_activeinfo_like(String n_activeinfo_like) {
        this.n_activeinfo_like = n_activeinfo_like;
        if(!ObjectUtils.isEmpty(this.n_activeinfo_like)){
            this.getSearchCond().like("activeinfo", n_activeinfo_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_empid_eq;//[换装人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_emeibatterysetupname_like;//[电瓶换装记录名称]
	public void setN_emeibatterysetupname_like(String n_emeibatterysetupname_like) {
        this.n_emeibatterysetupname_like = n_emeibatterysetupname_like;
        if(!ObjectUtils.isEmpty(this.n_emeibatterysetupname_like)){
            this.getSearchCond().like("emeibatterysetupname", n_emeibatterysetupname_like);
        }
    }
	private String n_eiobjname_eq;//[换下电瓶]
	public void setN_eiobjname_eq(String n_eiobjname_eq) {
        this.n_eiobjname_eq = n_eiobjname_eq;
        if(!ObjectUtils.isEmpty(this.n_eiobjname_eq)){
            this.getSearchCond().eq("eiobjname", n_eiobjname_eq);
        }
    }
	private String n_eiobjname_like;//[换下电瓶]
	public void setN_eiobjname_like(String n_eiobjname_like) {
        this.n_eiobjname_like = n_eiobjname_like;
        if(!ObjectUtils.isEmpty(this.n_eiobjname_like)){
            this.getSearchCond().like("eiobjname", n_eiobjname_like);
        }
    }
	private String n_eqlocationname_eq;//[位置]
	public void setN_eqlocationname_eq(String n_eqlocationname_eq) {
        this.n_eqlocationname_eq = n_eqlocationname_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_eq)){
            this.getSearchCond().eq("eqlocationname", n_eqlocationname_eq);
        }
    }
	private String n_eqlocationname_like;//[位置]
	public void setN_eqlocationname_like(String n_eqlocationname_like) {
        this.n_eqlocationname_like = n_eqlocationname_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_like)){
            this.getSearchCond().like("eqlocationname", n_eqlocationname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_eiobjnname_eq;//[换上电瓶]
	public void setN_eiobjnname_eq(String n_eiobjnname_eq) {
        this.n_eiobjnname_eq = n_eiobjnname_eq;
        if(!ObjectUtils.isEmpty(this.n_eiobjnname_eq)){
            this.getSearchCond().eq("eiobjnname", n_eiobjnname_eq);
        }
    }
	private String n_eiobjnname_like;//[换上电瓶]
	public void setN_eiobjnname_like(String n_eiobjnname_like) {
        this.n_eiobjnname_like = n_eiobjnname_like;
        if(!ObjectUtils.isEmpty(this.n_eiobjnname_like)){
            this.getSearchCond().like("eiobjnname", n_eiobjnname_like);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_eiobjnid_eq;//[换上电瓶]
	public void setN_eiobjnid_eq(String n_eiobjnid_eq) {
        this.n_eiobjnid_eq = n_eiobjnid_eq;
        if(!ObjectUtils.isEmpty(this.n_eiobjnid_eq)){
            this.getSearchCond().eq("eiobjnid", n_eiobjnid_eq);
        }
    }
	private String n_eiobjid_eq;//[换下电瓶]
	public void setN_eiobjid_eq(String n_eiobjid_eq) {
        this.n_eiobjid_eq = n_eiobjid_eq;
        if(!ObjectUtils.isEmpty(this.n_eiobjid_eq)){
            this.getSearchCond().eq("eiobjid", n_eiobjid_eq);
        }
    }
	private String n_eqlocationid_eq;//[位置]
	public void setN_eqlocationid_eq(String n_eqlocationid_eq) {
        this.n_eqlocationid_eq = n_eqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationid_eq)){
            this.getSearchCond().eq("eqlocationid", n_eqlocationid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeibatterysetupname", query)
            );
		 }
	}
}



