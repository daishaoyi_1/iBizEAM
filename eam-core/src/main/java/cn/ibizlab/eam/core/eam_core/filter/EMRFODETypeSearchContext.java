package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEType;
/**
 * 关系型数据实体[EMRFODEType] 查询条件对象
 */
@Slf4j
@Data
public class EMRFODETypeSearchContext extends QueryWrapperContext<EMRFODEType> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emrfodetypename_like;//[现象分类名称]
	public void setN_emrfodetypename_like(String n_emrfodetypename_like) {
        this.n_emrfodetypename_like = n_emrfodetypename_like;
        if(!ObjectUtils.isEmpty(this.n_emrfodetypename_like)){
            this.getSearchCond().like("emrfodetypename", n_emrfodetypename_like);
        }
    }
	private String n_rfodetypeinfo_like;//[现象分类信息]
	public void setN_rfodetypeinfo_like(String n_rfodetypeinfo_like) {
        this.n_rfodetypeinfo_like = n_rfodetypeinfo_like;
        if(!ObjectUtils.isEmpty(this.n_rfodetypeinfo_like)){
            this.getSearchCond().like("rfodetypeinfo", n_rfodetypeinfo_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emrfodetypename", query)
            );
		 }
	}
}



