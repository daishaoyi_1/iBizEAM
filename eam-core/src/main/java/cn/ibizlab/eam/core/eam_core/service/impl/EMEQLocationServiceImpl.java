package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLocation;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLocationSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQLocationMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[位置] 服务对象接口实现
 */
@Slf4j
@Service("EMEQLocationServiceImpl")
public class EMEQLocationServiceImpl extends ServiceImpl<EMEQLocationMapper, EMEQLocation> implements IEMEQLocationService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssetService emassetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatterySetupService emeibatterysetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryService emeibatteryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamSetupService emeicamsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamService emeicamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICellSetupService emeicellsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICellService emeicellService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIToolService emeitoolService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLCTMapService emeqlctmapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQLocation et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqlocationid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQLocation> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQLocation et) {
        fillParentData(et);
        emobjectService.update(emeqlocationInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqlocationid", et.getEmeqlocationid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqlocationid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQLocation> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQLocation get(String key) {
        EMEQLocation et = getById(key);
        if(et == null){
            et = new EMEQLocation();
            et.setEmeqlocationid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQLocation getDraft(EMEQLocation et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQLocation et) {
        return (!ObjectUtils.isEmpty(et.getEmeqlocationid())) && (!Objects.isNull(this.getById(et.getEmeqlocationid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQLocation et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQLocation et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQLocation> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQLocation> create = new ArrayList<>();
        List<EMEQLocation> update = new ArrayList<>();
        for (EMEQLocation et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqlocationid()) || ObjectUtils.isEmpty(getById(et.getEmeqlocationid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQLocation> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQLocation> create = new ArrayList<>();
        List<EMEQLocation> update = new ArrayList<>();
        for (EMEQLocation et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqlocationid()) || ObjectUtils.isEmpty(getById(et.getEmeqlocationid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQLocation> selectByMajorequipid(String emequipid) {
        return baseMapper.selectByMajorequipid(emequipid);
    }
    @Override
    public void removeByMajorequipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQLocation>().eq("majorequipid",emequipid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQLocation> searchDefault(EMEQLocationSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLocation> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLocation>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 下级位置
     */
    @Override
    public Page<EMEQLocation> searchSub(EMEQLocationSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLocation> pages=baseMapper.searchSub(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLocation>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQLocation et){
        //实体关系[DER1N_EMEQLOCATION_EMEQUIP_MAJOREQUIPID]
        if(!ObjectUtils.isEmpty(et.getMajorequipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip majorequip=et.getMajorequip();
            if(ObjectUtils.isEmpty(majorequip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getMajorequipid());
                et.setMajorequip(majorEntity);
                majorequip=majorEntity;
            }
            et.setMajorequipname(majorequip.getEquipinfo());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQLocationInheritMapping emeqlocationInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQLocation et){
        if(ObjectUtils.isEmpty(et.getEmeqlocationid()))
            et.setEmeqlocationid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emeqlocationInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","EQLOCATION");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQLocation> getEmeqlocationByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQLocation> getEmeqlocationByEntities(List<EMEQLocation> entities) {
        List ids =new ArrayList();
        for(EMEQLocation entity : entities){
            Serializable id=entity.getEmeqlocationid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQLocationService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



