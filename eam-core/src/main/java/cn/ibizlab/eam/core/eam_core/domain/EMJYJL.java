package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[加油记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMJYJL_BASE", resultMap = "EMJYJLResultMap")
public class EMJYJL extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 人员
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 加油品种
     */
    @TableField(value = "jiayoupz")
    @JSONField(name = "jiayoupz")
    @JsonProperty("jiayoupz")
    private String jiayoupz;
    /**
     * 加油设备
     */
    @TableField(value = "jiayoushebei")
    @JSONField(name = "jiayoushebei")
    @JsonProperty("jiayoushebei")
    private String jiayoushebei;
    /**
     * 暂不生产领料单
     */
    @DEField(defaultValue = "0")
    @TableField(value = "iszbsc")
    @JSONField(name = "iszbsc")
    @JsonProperty("iszbsc")
    private Integer iszbsc;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 部门
     */
    @TableField(value = "deptid")
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    private String deptid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 加油人
     */
    @TableField(value = "jiayouname")
    @JSONField(name = "jiayouname")
    @JsonProperty("jiayouname")
    private String jiayouname;
    /**
     * 加油时间
     */
    @TableField(value = "jiayoushijian")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "jiayoushijian", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("jiayoushijian")
    private Timestamp jiayoushijian;
    /**
     * 密度
     */
    @TableField(value = "dens")
    @JSONField(name = "dens")
    @JsonProperty("dens")
    private Double dens;
    /**
     * 加油量（升）
     */
    @TableField(value = "jiayouliang")
    @JSONField(name = "jiayouliang")
    @JsonProperty("jiayouliang")
    private Double jiayouliang;
    /**
     * 已生成领料单
     */
    @DEField(defaultValue = "0")
    @TableField(value = "iscpuse")
    @JSONField(name = "iscpuse")
    @JsonProperty("iscpuse")
    private Integer iscpuse;
    /**
     * 部门
     */
    @TableField(value = "deptname")
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    private String deptname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 加油记录标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emjyjlid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emjyjlid")
    @JsonProperty("emjyjlid")
    private String emjyjlid;
    /**
     * 加油记录名称
     */
    @DEField(defaultValue = "var_jiayoushebei||to_char(var_jiayoushijian,yymmdd)")
    @TableField(value = "emjyjlname")
    @JSONField(name = "emjyjlname")
    @JsonProperty("emjyjlname")
    private String emjyjlname;
    /**
     * 加油人ID
     */
    @TableField(value = "jiayouid")
    @JSONField(name = "jiayouid")
    @JsonProperty("jiayouid")
    private String jiayouid;
    /**
     * 领用数（kg）
     */
    @TableField(exist = false)
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;
    /**
     * 人员
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 物品密度
     */
    @TableField(exist = false)
    @JSONField(name = "itemdens")
    @JsonProperty("itemdens")
    private Double itemdens;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    private String teamname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 设备组织
     */
    @TableField(exist = false)
    @JSONField(name = "eqorgid")
    @JsonProperty("eqorgid")
    private String eqorgid;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 领料单
     */
    @TableField(exist = false)
    @JSONField(name = "itempusename")
    @JsonProperty("itempusename")
    private String itempusename;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 领料单
     */
    @TableField(value = "itempuseid")
    @JSONField(name = "itempuseid")
    @JsonProperty("itempuseid")
    private String itempuseid;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 班组
     */
    @TableField(value = "teamid")
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    private String teamid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemPUse itempuse;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam team;



    /**
     * 设置 [人员]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [加油品种]
     */
    public void setJiayoupz(String jiayoupz) {
        this.jiayoupz = jiayoupz;
        this.modify("jiayoupz", jiayoupz);
    }

    /**
     * 设置 [加油设备]
     */
    public void setJiayoushebei(String jiayoushebei) {
        this.jiayoushebei = jiayoushebei;
        this.modify("jiayoushebei", jiayoushebei);
    }

    /**
     * 设置 [暂不生产领料单]
     */
    public void setIszbsc(Integer iszbsc) {
        this.iszbsc = iszbsc;
        this.modify("iszbsc", iszbsc);
    }

    /**
     * 设置 [部门]
     */
    public void setDeptid(String deptid) {
        this.deptid = deptid;
        this.modify("deptid", deptid);
    }

    /**
     * 设置 [加油人]
     */
    public void setJiayouname(String jiayouname) {
        this.jiayouname = jiayouname;
        this.modify("jiayouname", jiayouname);
    }

    /**
     * 设置 [加油时间]
     */
    public void setJiayoushijian(Timestamp jiayoushijian) {
        this.jiayoushijian = jiayoushijian;
        this.modify("jiayoushijian", jiayoushijian);
    }

    /**
     * 格式化日期 [加油时间]
     */
    public String formatJiayoushijian() {
        if (this.jiayoushijian == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(jiayoushijian);
    }
    /**
     * 设置 [密度]
     */
    public void setDens(Double dens) {
        this.dens = dens;
        this.modify("dens", dens);
    }

    /**
     * 设置 [加油量（升）]
     */
    public void setJiayouliang(Double jiayouliang) {
        this.jiayouliang = jiayouliang;
        this.modify("jiayouliang", jiayouliang);
    }

    /**
     * 设置 [已生成领料单]
     */
    public void setIscpuse(Integer iscpuse) {
        this.iscpuse = iscpuse;
        this.modify("iscpuse", iscpuse);
    }

    /**
     * 设置 [部门]
     */
    public void setDeptname(String deptname) {
        this.deptname = deptname;
        this.modify("deptname", deptname);
    }

    /**
     * 设置 [加油记录名称]
     */
    public void setEmjyjlname(String emjyjlname) {
        this.emjyjlname = emjyjlname;
        this.modify("emjyjlname", emjyjlname);
    }

    /**
     * 设置 [加油人ID]
     */
    public void setJiayouid(String jiayouid) {
        this.jiayouid = jiayouid;
        this.modify("jiayouid", jiayouid);
    }

    /**
     * 设置 [人员]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [领料单]
     */
    public void setItempuseid(String itempuseid) {
        this.itempuseid = itempuseid;
        this.modify("itempuseid", itempuseid);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [班组]
     */
    public void setTeamid(String teamid) {
        this.teamid = teamid;
        this.modify("teamid", teamid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emjyjlid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


