package cn.ibizlab.eam.core.eam_pf.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[合同]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_PFCONTRACT_BASE", resultMap = "PFContractResultMap")
public class PFContract extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 需说明事项
     */
    @TableField(value = "attention")
    @JSONField(name = "attention")
    @JsonProperty("attention")
    private String attention;
    /**
     * 合同对方分组
     */
    @TableField(value = "contractobjgroup")
    @JSONField(name = "contractobjgroup")
    @JsonProperty("contractobjgroup")
    private String contractobjgroup;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 已付金额(万元)
     */
    @TableField(value = "payamount")
    @JSONField(name = "payamount")
    @JsonProperty("payamount")
    private String payamount;
    /**
     * 资质证书
     */
    @TableField(value = "certdesc")
    @JSONField(name = "certdesc")
    @JsonProperty("certdesc")
    private String certdesc;
    /**
     * 附件
     */
    @TableField(value = "att")
    @JSONField(name = "att")
    @JsonProperty("att")
    private String att;
    /**
     * 合同名称
     */
    @TableField(value = "pfcontractname")
    @JSONField(name = "pfcontractname")
    @JsonProperty("pfcontractname")
    private String pfcontractname;
    /**
     * 合同履约记录
     */
    @TableField(value = "performrecord")
    @JSONField(name = "performrecord")
    @JsonProperty("performrecord")
    private String performrecord;
    /**
     * 备注
     */
    @TableField(value = "contractdesc")
    @JSONField(name = "contractdesc")
    @JsonProperty("contractdesc")
    private String contractdesc;
    /**
     * 验收标准
     */
    @TableField(value = "checkstandard")
    @JSONField(name = "checkstandard")
    @JsonProperty("checkstandard")
    private String checkstandard;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 审批状态
     */
    @TableField(value = "apprstate")
    @JSONField(name = "apprstate")
    @JsonProperty("apprstate")
    private String apprstate;
    /**
     * 每次付款数
     */
    @TableField(value = "peramount")
    @JSONField(name = "peramount")
    @JsonProperty("peramount")
    private String peramount;
    /**
     * 合同标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "pfcontractid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "pfcontractid")
    @JsonProperty("pfcontractid")
    private String pfcontractid;
    /**
     * 合同付款情况
     */
    @TableField(value = "paydesc")
    @JSONField(name = "paydesc")
    @JsonProperty("paydesc")
    private String paydesc;
    /**
     * 付款期数
     */
    @TableField(value = "paycnt")
    @JSONField(name = "paycnt")
    @JsonProperty("paycnt")
    private String paycnt;
    /**
     * 履行期限
     */
    @TableField(value = "performline")
    @JSONField(name = "performline")
    @JsonProperty("performline")
    private String performline;
    /**
     * 合同数量
     */
    @TableField(value = "contractnum")
    @JSONField(name = "contractnum")
    @JsonProperty("contractnum")
    private Integer contractnum;
    /**
     * 附件名称
     */
    @TableField(value = "attdesc")
    @JSONField(name = "attdesc")
    @JsonProperty("attdesc")
    private String attdesc;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 合同原件
     */
    @TableField(value = "contractdoc")
    @JSONField(name = "contractdoc")
    @JsonProperty("contractdoc")
    private String contractdoc;
    /**
     * 调整说明
     */
    @TableField(value = "changedesc")
    @JSONField(name = "changedesc")
    @JsonProperty("changedesc")
    private String changedesc;
    /**
     * 合同序列号
     */
    @TableField(value = "contractno")
    @JSONField(name = "contractno")
    @JsonProperty("contractno")
    private String contractno;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 盖章单位
     */
    @TableField(value = "rorgid")
    @JSONField(name = "rorgid")
    @JsonProperty("rorgid")
    private String rorgid;
    /**
     * 履约情况
     */
    @TableField(value = "performstate")
    @JSONField(name = "performstate")
    @JsonProperty("performstate")
    private String performstate;
    /**
     * 其它事项说明
     */
    @TableField(value = "declaration")
    @JSONField(name = "declaration")
    @JsonProperty("declaration")
    private String declaration;
    /**
     * 合同内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 付款理由
     */
    @TableField(value = "payreason")
    @JSONField(name = "payreason")
    @JsonProperty("payreason")
    private String payreason;
    /**
     * 许可证
     */
    @TableField(value = "licdesc")
    @JSONField(name = "licdesc")
    @JsonProperty("licdesc")
    private String licdesc;
    /**
     * 上报时间
     */
    @TableField(value = "sdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "sdate", format = "yyyy-MM-dd")
    @JsonProperty("sdate")
    private Timestamp sdate;
    /**
     * 审批备注
     */
    @TableField(value = "apprdesc")
    @JSONField(name = "apprdesc")
    @JsonProperty("apprdesc")
    private String apprdesc;
    /**
     * 合同单价(￥)
     */
    @TableField(value = "contractprice")
    @JSONField(name = "contractprice")
    @JsonProperty("contractprice")
    private String contractprice;
    /**
     * 合同类型
     */
    @TableField(value = "contracttypeid")
    @JSONField(name = "contracttypeid")
    @JsonProperty("contracttypeid")
    private String contracttypeid;
    /**
     * 价款(万元)
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private String amount;
    /**
     * 乙方
     */
    @TableField(value = "contractgroup")
    @JSONField(name = "contractgroup")
    @JsonProperty("contractgroup")
    private Integer contractgroup;
    /**
     * 其他资料
     */
    @TableField(value = "docdesc")
    @JSONField(name = "docdesc")
    @JsonProperty("docdesc")
    private String docdesc;
    /**
     * 合同质量
     */
    @TableField(value = "contractqa")
    @JSONField(name = "contractqa")
    @JsonProperty("contractqa")
    private String contractqa;
    /**
     * 订立日期
     */
    @TableField(value = "contractdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "contractdate", format = "yyyy-MM-dd")
    @JsonProperty("contractdate")
    private Timestamp contractdate;
    /**
     * 收到资料名称
     */
    @TableField(value = "recvdoc")
    @JSONField(name = "recvdoc")
    @JsonProperty("recvdoc")
    private String recvdoc;
    /**
     * 审批时间
     */
    @TableField(value = "apprdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "apprdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    private Timestamp apprdate;
    /**
     * 履行地点
     */
    @TableField(value = "performplace")
    @JSONField(name = "performplace")
    @JsonProperty("performplace")
    private String performplace;
    /**
     * 合同信息
     */
    @TableField(exist = false)
    @JSONField(name = "contractinfo")
    @JsonProperty("contractinfo")
    private String contractinfo;
    /**
     * 合同对方
     */
    @TableField(value = "contractobjid")
    @JSONField(name = "contractobjid")
    @JsonProperty("contractobjid")
    private String contractobjid;
    /**
     * 履行方式
     */
    @TableField(value = "performway")
    @JSONField(name = "performway")
    @JsonProperty("performway")
    private String performway;
    /**
     * 变更内容
     */
    @TableField(value = "changecontent")
    @JSONField(name = "changecontent")
    @JsonProperty("changecontent")
    private String changecontent;
    /**
     * 合同代码
     */
    @TableField(value = "contractcode")
    @JSONField(name = "contractcode")
    @JsonProperty("contractcode")
    private String contractcode;
    /**
     * 合同标准
     */
    @TableField(value = "contracttender")
    @JSONField(name = "contracttender")
    @JsonProperty("contracttender")
    private String contracttender;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    private String emservicename;
    /**
     * 服务商
     */
    @TableField(value = "emserviceid")
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    private String emserviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService emservice;



    /**
     * 设置 [需说明事项]
     */
    public void setAttention(String attention) {
        this.attention = attention;
        this.modify("attention", attention);
    }

    /**
     * 设置 [合同对方分组]
     */
    public void setContractobjgroup(String contractobjgroup) {
        this.contractobjgroup = contractobjgroup;
        this.modify("contractobjgroup", contractobjgroup);
    }

    /**
     * 设置 [已付金额(万元)]
     */
    public void setPayamount(String payamount) {
        this.payamount = payamount;
        this.modify("payamount", payamount);
    }

    /**
     * 设置 [资质证书]
     */
    public void setCertdesc(String certdesc) {
        this.certdesc = certdesc;
        this.modify("certdesc", certdesc);
    }

    /**
     * 设置 [附件]
     */
    public void setAtt(String att) {
        this.att = att;
        this.modify("att", att);
    }

    /**
     * 设置 [合同名称]
     */
    public void setPfcontractname(String pfcontractname) {
        this.pfcontractname = pfcontractname;
        this.modify("pfcontractname", pfcontractname);
    }

    /**
     * 设置 [合同履约记录]
     */
    public void setPerformrecord(String performrecord) {
        this.performrecord = performrecord;
        this.modify("performrecord", performrecord);
    }

    /**
     * 设置 [备注]
     */
    public void setContractdesc(String contractdesc) {
        this.contractdesc = contractdesc;
        this.modify("contractdesc", contractdesc);
    }

    /**
     * 设置 [验收标准]
     */
    public void setCheckstandard(String checkstandard) {
        this.checkstandard = checkstandard;
        this.modify("checkstandard", checkstandard);
    }

    /**
     * 设置 [审批状态]
     */
    public void setApprstate(String apprstate) {
        this.apprstate = apprstate;
        this.modify("apprstate", apprstate);
    }

    /**
     * 设置 [每次付款数]
     */
    public void setPeramount(String peramount) {
        this.peramount = peramount;
        this.modify("peramount", peramount);
    }

    /**
     * 设置 [合同付款情况]
     */
    public void setPaydesc(String paydesc) {
        this.paydesc = paydesc;
        this.modify("paydesc", paydesc);
    }

    /**
     * 设置 [付款期数]
     */
    public void setPaycnt(String paycnt) {
        this.paycnt = paycnt;
        this.modify("paycnt", paycnt);
    }

    /**
     * 设置 [履行期限]
     */
    public void setPerformline(String performline) {
        this.performline = performline;
        this.modify("performline", performline);
    }

    /**
     * 设置 [合同数量]
     */
    public void setContractnum(Integer contractnum) {
        this.contractnum = contractnum;
        this.modify("contractnum", contractnum);
    }

    /**
     * 设置 [附件名称]
     */
    public void setAttdesc(String attdesc) {
        this.attdesc = attdesc;
        this.modify("attdesc", attdesc);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [合同原件]
     */
    public void setContractdoc(String contractdoc) {
        this.contractdoc = contractdoc;
        this.modify("contractdoc", contractdoc);
    }

    /**
     * 设置 [调整说明]
     */
    public void setChangedesc(String changedesc) {
        this.changedesc = changedesc;
        this.modify("changedesc", changedesc);
    }

    /**
     * 设置 [合同序列号]
     */
    public void setContractno(String contractno) {
        this.contractno = contractno;
        this.modify("contractno", contractno);
    }

    /**
     * 设置 [盖章单位]
     */
    public void setRorgid(String rorgid) {
        this.rorgid = rorgid;
        this.modify("rorgid", rorgid);
    }

    /**
     * 设置 [履约情况]
     */
    public void setPerformstate(String performstate) {
        this.performstate = performstate;
        this.modify("performstate", performstate);
    }

    /**
     * 设置 [其它事项说明]
     */
    public void setDeclaration(String declaration) {
        this.declaration = declaration;
        this.modify("declaration", declaration);
    }

    /**
     * 设置 [合同内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [付款理由]
     */
    public void setPayreason(String payreason) {
        this.payreason = payreason;
        this.modify("payreason", payreason);
    }

    /**
     * 设置 [许可证]
     */
    public void setLicdesc(String licdesc) {
        this.licdesc = licdesc;
        this.modify("licdesc", licdesc);
    }

    /**
     * 设置 [上报时间]
     */
    public void setSdate(Timestamp sdate) {
        this.sdate = sdate;
        this.modify("sdate", sdate);
    }

    /**
     * 格式化日期 [上报时间]
     */
    public String formatSdate() {
        if (this.sdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(sdate);
    }
    /**
     * 设置 [审批备注]
     */
    public void setApprdesc(String apprdesc) {
        this.apprdesc = apprdesc;
        this.modify("apprdesc", apprdesc);
    }

    /**
     * 设置 [合同单价(￥)]
     */
    public void setContractprice(String contractprice) {
        this.contractprice = contractprice;
        this.modify("contractprice", contractprice);
    }

    /**
     * 设置 [合同类型]
     */
    public void setContracttypeid(String contracttypeid) {
        this.contracttypeid = contracttypeid;
        this.modify("contracttypeid", contracttypeid);
    }

    /**
     * 设置 [价款(万元)]
     */
    public void setAmount(String amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [乙方]
     */
    public void setContractgroup(Integer contractgroup) {
        this.contractgroup = contractgroup;
        this.modify("contractgroup", contractgroup);
    }

    /**
     * 设置 [其他资料]
     */
    public void setDocdesc(String docdesc) {
        this.docdesc = docdesc;
        this.modify("docdesc", docdesc);
    }

    /**
     * 设置 [合同质量]
     */
    public void setContractqa(String contractqa) {
        this.contractqa = contractqa;
        this.modify("contractqa", contractqa);
    }

    /**
     * 设置 [订立日期]
     */
    public void setContractdate(Timestamp contractdate) {
        this.contractdate = contractdate;
        this.modify("contractdate", contractdate);
    }

    /**
     * 格式化日期 [订立日期]
     */
    public String formatContractdate() {
        if (this.contractdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(contractdate);
    }
    /**
     * 设置 [收到资料名称]
     */
    public void setRecvdoc(String recvdoc) {
        this.recvdoc = recvdoc;
        this.modify("recvdoc", recvdoc);
    }

    /**
     * 设置 [审批时间]
     */
    public void setApprdate(Timestamp apprdate) {
        this.apprdate = apprdate;
        this.modify("apprdate", apprdate);
    }

    /**
     * 格式化日期 [审批时间]
     */
    public String formatApprdate() {
        if (this.apprdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(apprdate);
    }
    /**
     * 设置 [履行地点]
     */
    public void setPerformplace(String performplace) {
        this.performplace = performplace;
        this.modify("performplace", performplace);
    }

    /**
     * 设置 [合同对方]
     */
    public void setContractobjid(String contractobjid) {
        this.contractobjid = contractobjid;
        this.modify("contractobjid", contractobjid);
    }

    /**
     * 设置 [履行方式]
     */
    public void setPerformway(String performway) {
        this.performway = performway;
        this.modify("performway", performway);
    }

    /**
     * 设置 [变更内容]
     */
    public void setChangecontent(String changecontent) {
        this.changecontent = changecontent;
        this.modify("changecontent", changecontent);
    }

    /**
     * 设置 [合同代码]
     */
    public void setContractcode(String contractcode) {
        this.contractcode = contractcode;
        this.modify("contractcode", contractcode);
    }

    /**
     * 设置 [合同标准]
     */
    public void setContracttender(String contracttender) {
        this.contracttender = contracttender;
        this.modify("contracttender", contracttender);
    }

    /**
     * 设置 [服务商]
     */
    public void setEmserviceid(String emserviceid) {
        this.emserviceid = emserviceid;
        this.modify("emserviceid", emserviceid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("pfcontractid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


