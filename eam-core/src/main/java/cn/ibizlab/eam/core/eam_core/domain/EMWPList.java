package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[采购申请]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMWPLIST_BASE", resultMap = "EMWPListResultMap")
public class EMWPList extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 批准日期
     */
    @TableField(value = "apprdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "apprdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    private Timestamp apprdate;
    /**
     * 申请日期
     */
    @TableField(value = "adate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "adate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    private Timestamp adate;
    /**
     * 采购申请
     */
    @TableField(exist = false)
    @JSONField(name = "wplistids")
    @JsonProperty("wplistids")
    private String wplistids;
    /**
     * 经理指定询价数
     */
    @DEField(defaultValue = "0")
    @TableField(value = "m3q")
    @JSONField(name = "m3q")
    @JsonProperty("m3q")
    private Integer m3q;
    /**
     * 需要3次询价
     */
    @DEField(defaultValue = "0")
    @TableField(value = "need3q")
    @JSONField(name = "need3q")
    @JsonProperty("need3q")
    private Integer need3q;
    /**
     * 采购申请名称
     */
    @DEField(defaultValue = "物品||VAR_ITEMID")
    @TableField(value = "emwplistname")
    @JSONField(name = "emwplistname")
    @JsonProperty("emwplistname")
    private String emwplistname;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 希望到货日期
     */
    @TableField(value = "hdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "hdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("hdate")
    private Timestamp hdate;
    /**
     * 设备集合
     */
    @TableField(value = "equips")
    @JSONField(name = "equips")
    @JsonProperty("equips")
    private String equips;
    /**
     * 询价记录数
     */
    @TableField(exist = false)
    @JSONField(name = "qcnt")
    @JsonProperty("qcnt")
    private Integer qcnt;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 物品备注
     */
    @TableField(value = "itemdesc")
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    private String itemdesc;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 采购申请号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emwplistid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emwplistid")
    @JsonProperty("emwplistid")
    private String emwplistid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 请购类型
     */
    @TableField(value = "wplisttype")
    @JSONField(name = "wplisttype")
    @JsonProperty("wplisttype")
    private String wplisttype;
    /**
     * 物品和备注
     */
    @TableField(value = "itemanditemdesc")
    @JSONField(name = "itemanditemdesc")
    @JsonProperty("itemanditemdesc")
    private String itemanditemdesc;
    /**
     * 询价总金额
     */
    @TableField(exist = false)
    @JSONField(name = "costamount")
    @JsonProperty("costamount")
    private Double costamount;
    /**
     * 用途
     */
    @DEField(defaultValue = "EQUIP")
    @TableField(value = "useto")
    @JSONField(name = "useto")
    @JsonProperty("useto")
    private String useto;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 是否为取消标志
     */
    @DEField(defaultValue = "0")
    @TableField(value = "iscancel")
    @JSONField(name = "iscancel")
    @JsonProperty("iscancel")
    private Integer iscancel;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 上次请购时间
     */
    @TableField(value = "lastdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "lastdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastdate")
    private Timestamp lastdate;
    /**
     * 审核意见
     */
    @TableField(value = "apprdesc")
    @JSONField(name = "apprdesc")
    @JsonProperty("apprdesc")
    private String apprdesc;
    /**
     * 请购状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "wpliststate")
    @JSONField(name = "wpliststate")
    @JsonProperty("wpliststate")
    private Integer wpliststate;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 处理结果
     */
    @TableField(value = "wplistdp")
    @JSONField(name = "wplistdp")
    @JsonProperty("wplistdp")
    private String wplistdp;
    /**
     * 请购数
     */
    @TableField(value = "asum")
    @JSONField(name = "asum")
    @JsonProperty("asum")
    private Double asum;
    /**
     * 采购申请信息
     */
    @TableField(exist = false)
    @JSONField(name = "wplistinfo")
    @JsonProperty("wplistinfo")
    private String wplistinfo;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 预计总金额
     */
    @TableField(exist = false)
    @JSONField(name = "pamount")
    @JsonProperty("pamount")
    private Double pamount;
    /**
     * 删除标识
     */
    @DEField(defaultValue = "0")
    @TableField(value = "deltype")
    @JSONField(name = "deltype")
    @JsonProperty("deltype")
    private Integer deltype;
    /**
     * 产品供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    private String labservicename;
    /**
     * 不足3家供应商
     */
    @TableField(exist = false)
    @JSONField(name = "no3q")
    @JsonProperty("no3q")
    private Integer no3q;
    /**
     * 物品代码
     */
    @TableField(exist = false)
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    private String itemcode;
    /**
     * 询价结果
     */
    @TableField(exist = false)
    @JSONField(name = "wplistcostname")
    @JsonProperty("wplistcostname")
    private String wplistcostname;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    private String unitname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    private String emservicename;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    private String itembtypeid;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname_show")
    @JsonProperty("itemname_show")
    private String itemnameShow;
    /**
     * 物品均价
     */
    @TableField(exist = false)
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    private Double avgprice;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 申请班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    private String teamname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 产品供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    private String labserviceid;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    private String unitid;
    /**
     * 申请班组
     */
    @TableField(value = "teamid")
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    private String teamid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 服务商
     */
    @TableField(value = "emserviceid")
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    private String emserviceid;
    /**
     * 询价结果
     */
    @TableField(value = "wplistcostid")
    @JSONField(name = "wplistcostid")
    @JsonProperty("wplistcostid")
    private String wplistcostid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 申请人
     */
    @TableField(value = "aempid")
    @JSONField(name = "aempid")
    @JsonProperty("aempid")
    private String aempid;
    /**
     * 申请人
     */
    @TableField(exist = false)
    @JSONField(name = "aempname")
    @JsonProperty("aempname")
    private String aempname;
    /**
     * 申请部门
     */
    @TableField(value = "deptid")
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    private String deptid;
    /**
     * 申请部门
     */
    @TableField(exist = false)
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    private String deptname;
    /**
     * 批准人
     */
    @TableField(value = "apprempid")
    @JSONField(name = "apprempid")
    @JsonProperty("apprempid")
    private String apprempid;
    /**
     * 批准人
     */
    @TableField(exist = false)
    @JSONField(name = "apprempname")
    @JsonProperty("apprempname")
    private String apprempname;
    /**
     * 采购员
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 采购员
     */
    @TableField(exist = false)
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService emservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWPListCost wplistcost;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;

    /**
     * 批准人
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp apprfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp emrempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam team;



    /**
     * 设置 [批准日期]
     */
    public void setApprdate(Timestamp apprdate) {
        this.apprdate = apprdate;
        this.modify("apprdate", apprdate);
    }

    /**
     * 格式化日期 [批准日期]
     */
    public String formatApprdate() {
        if (this.apprdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(apprdate);
    }
    /**
     * 设置 [申请日期]
     */
    public void setAdate(Timestamp adate) {
        this.adate = adate;
        this.modify("adate", adate);
    }

    /**
     * 格式化日期 [申请日期]
     */
    public String formatAdate() {
        if (this.adate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(adate);
    }
    /**
     * 设置 [经理指定询价数]
     */
    public void setM3q(Integer m3q) {
        this.m3q = m3q;
        this.modify("m3q", m3q);
    }

    /**
     * 设置 [需要3次询价]
     */
    public void setNeed3q(Integer need3q) {
        this.need3q = need3q;
        this.modify("need3q", need3q);
    }

    /**
     * 设置 [采购申请名称]
     */
    public void setEmwplistname(String emwplistname) {
        this.emwplistname = emwplistname;
        this.modify("emwplistname", emwplistname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [希望到货日期]
     */
    public void setHdate(Timestamp hdate) {
        this.hdate = hdate;
        this.modify("hdate", hdate);
    }

    /**
     * 格式化日期 [希望到货日期]
     */
    public String formatHdate() {
        if (this.hdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(hdate);
    }
    /**
     * 设置 [设备集合]
     */
    public void setEquips(String equips) {
        this.equips = equips;
        this.modify("equips", equips);
    }

    /**
     * 设置 [物品备注]
     */
    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
        this.modify("itemdesc", itemdesc);
    }

    /**
     * 设置 [请购类型]
     */
    public void setWplisttype(String wplisttype) {
        this.wplisttype = wplisttype;
        this.modify("wplisttype", wplisttype);
    }

    /**
     * 设置 [物品和备注]
     */
    public void setItemanditemdesc(String itemanditemdesc) {
        this.itemanditemdesc = itemanditemdesc;
        this.modify("itemanditemdesc", itemanditemdesc);
    }

    /**
     * 设置 [用途]
     */
    public void setUseto(String useto) {
        this.useto = useto;
        this.modify("useto", useto);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [是否为取消标志]
     */
    public void setIscancel(Integer iscancel) {
        this.iscancel = iscancel;
        this.modify("iscancel", iscancel);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [上次请购时间]
     */
    public void setLastdate(Timestamp lastdate) {
        this.lastdate = lastdate;
        this.modify("lastdate", lastdate);
    }

    /**
     * 格式化日期 [上次请购时间]
     */
    public String formatLastdate() {
        if (this.lastdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastdate);
    }
    /**
     * 设置 [审核意见]
     */
    public void setApprdesc(String apprdesc) {
        this.apprdesc = apprdesc;
        this.modify("apprdesc", apprdesc);
    }

    /**
     * 设置 [请购状态]
     */
    public void setWpliststate(Integer wpliststate) {
        this.wpliststate = wpliststate;
        this.modify("wpliststate", wpliststate);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [处理结果]
     */
    public void setWplistdp(String wplistdp) {
        this.wplistdp = wplistdp;
        this.modify("wplistdp", wplistdp);
    }

    /**
     * 设置 [请购数]
     */
    public void setAsum(Double asum) {
        this.asum = asum;
        this.modify("asum", asum);
    }

    /**
     * 设置 [删除标识]
     */
    public void setDeltype(Integer deltype) {
        this.deltype = deltype;
        this.modify("deltype", deltype);
    }

    /**
     * 设置 [申请班组]
     */
    public void setTeamid(String teamid) {
        this.teamid = teamid;
        this.modify("teamid", teamid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [服务商]
     */
    public void setEmserviceid(String emserviceid) {
        this.emserviceid = emserviceid;
        this.modify("emserviceid", emserviceid);
    }

    /**
     * 设置 [询价结果]
     */
    public void setWplistcostid(String wplistcostid) {
        this.wplistcostid = wplistcostid;
        this.modify("wplistcostid", wplistcostid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [申请人]
     */
    public void setAempid(String aempid) {
        this.aempid = aempid;
        this.modify("aempid", aempid);
    }

    /**
     * 设置 [申请部门]
     */
    public void setDeptid(String deptid) {
        this.deptid = deptid;
        this.modify("deptid", deptid);
    }

    /**
     * 设置 [批准人]
     */
    public void setApprempid(String apprempid) {
        this.apprempid = apprempid;
        this.modify("apprempid", apprempid);
    }

    /**
     * 设置 [采购员]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emwplistid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


