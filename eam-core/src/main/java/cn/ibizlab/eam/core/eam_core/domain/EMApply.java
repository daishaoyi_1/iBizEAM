package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[外委申请]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMAPPLY_BASE", resultMap = "EMApplyResultMap")
public class EMApply extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 外委类型
     */
    @TableField(value = "entrustlist")
    @JSONField(name = "entrustlist")
    @JsonProperty("entrustlist")
    private String entrustlist;
    /**
     * 外委申请内容
     */
    @TableField(value = "applydesc")
    @JSONField(name = "applydesc")
    @JsonProperty("applydesc")
    private String applydesc;
    /**
     * 申请汇报状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "applystate")
    @JSONField(name = "applystate")
    @JsonProperty("applystate")
    private Integer applystate;
    /**
     * 持续时间(H)
     */
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 处理备注
     */
    @TableField(value = "dpdesc")
    @JSONField(name = "dpdesc")
    @JsonProperty("dpdesc")
    private String dpdesc;
    /**
     * 材料费(￥)
     */
    @TableField(value = "mfee")
    @JSONField(name = "mfee")
    @JsonProperty("mfee")
    private String mfee;
    /**
     * 申请信息
     */
    @TableField(exist = false)
    @JSONField(name = "applyinfo")
    @JsonProperty("applyinfo")
    private String applyinfo;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 税费
     */
    @TableField(value = "shuifei")
    @JSONField(name = "shuifei")
    @JsonProperty("shuifei")
    private Double shuifei;
    /**
     * 服务费(￥)
     */
    @TableField(value = "sfee")
    @JSONField(name = "sfee")
    @JsonProperty("sfee")
    private Double sfee;
    /**
     * 外委申请名称
     */
    @TableField(value = "emapplyname")
    @JSONField(name = "emapplyname")
    @JsonProperty("emapplyname")
    private String emapplyname;
    /**
     * 申请类型
     */
    @TableField(value = "applytype")
    @JSONField(name = "applytype")
    @JsonProperty("applytype")
    private String applytype;
    /**
     * 希望完成早于
     */
    @TableField(value = "applyedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "applyedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("applyedate")
    private Timestamp applyedate;
    /**
     * 申请人
     */
    @TableField(value = "mpersonid")
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    private String mpersonid;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 人工费(￥)
     */
    @TableField(value = "pfee")
    @JSONField(name = "pfee")
    @JsonProperty("pfee")
    private Double pfee;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 预算费用(￥)
     */
    @TableField(value = "prefee1")
    @JSONField(name = "prefee1")
    @JsonProperty("prefee1")
    private Double prefee1;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 责任人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 发票号
     */
    @TableField(value = "fp")
    @JSONField(name = "fp")
    @JsonProperty("fp")
    private String fp;
    /**
     * 总费用
     */
    @TableField(exist = false)
    @JSONField(name = "zfy")
    @JsonProperty("zfy")
    private Double zfy;
    /**
     * 责任人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 希望开始于
     */
    @TableField(value = "applybdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "applybdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("applybdate")
    private Timestamp applybdate;
    /**
     * 费用(￥)
     */
    @TableField(value = "prefee")
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    private Double prefee;
    /**
     * 关闭日期
     */
    @TableField(value = "closedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "closedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("closedate")
    private Timestamp closedate;
    /**
     * 外委编号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emapplyid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emapplyid")
    @JsonProperty("emapplyid")
    private String emapplyid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 计划类型
     */
    @TableField(value = "plantype")
    @JSONField(name = "plantype")
    @JsonProperty("plantype")
    private String plantype;
    /**
     * 关闭人
     */
    @TableField(value = "closeempid")
    @JSONField(name = "closeempid")
    @JsonProperty("closeempid")
    private String closeempid;
    /**
     * 申请日期
     */
    @TableField(value = "applydate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "applydate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("applydate")
    private Timestamp applydate;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptname")
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    private String rdeptname;
    /**
     * 申请人
     */
    @TableField(value = "mpersonname")
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    private String mpersonname;
    /**
     * 关闭人
     */
    @TableField(value = "closeempname")
    @JSONField(name = "closeempname")
    @JsonProperty("closeempname")
    private String closeempname;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 发票附件
     */
    @TableField(value = "invoiceattach")
    @JSONField(name = "invoiceattach")
    @JsonProperty("invoiceattach")
    private String invoiceattach;
    /**
     * 审批意见
     */
    @TableField(value = "spyj")
    @JSONField(name = "spyj")
    @JsonProperty("spyj")
    private String spyj;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 优先级
     */
    @DEField(defaultValue = "2")
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptid")
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    private String rdeptid;
    /**
     * 模式
     */
    @TableField(exist = false)
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    private String rfomoname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    private String rservicename;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 现象
     */
    @TableField(exist = false)
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    private String rfodename;
    /**
     * 方案
     */
    @TableField(exist = false)
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    private String rfoacname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    private String rteamname;
    /**
     * 原因
     */
    @TableField(exist = false)
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    private String rfocaname;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    private String rteamid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 方案
     */
    @TableField(value = "rfoacid")
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    private String rfoacid;
    /**
     * 模式
     */
    @TableField(value = "rfomoid")
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    private String rfomoid;
    /**
     * 服务商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    private String rserviceid;
    /**
     * 原因
     */
    @TableField(value = "rfocaid")
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    private String rfocaid;
    /**
     * 现象
     */
    @TableField(value = "rfodeid")
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    private String rfodeid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [外委类型]
     */
    public void setEntrustlist(String entrustlist) {
        this.entrustlist = entrustlist;
        this.modify("entrustlist", entrustlist);
    }

    /**
     * 设置 [外委申请内容]
     */
    public void setApplydesc(String applydesc) {
        this.applydesc = applydesc;
        this.modify("applydesc", applydesc);
    }

    /**
     * 设置 [申请汇报状态]
     */
    public void setApplystate(Integer applystate) {
        this.applystate = applystate;
        this.modify("applystate", applystate);
    }

    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [处理备注]
     */
    public void setDpdesc(String dpdesc) {
        this.dpdesc = dpdesc;
        this.modify("dpdesc", dpdesc);
    }

    /**
     * 设置 [材料费(￥)]
     */
    public void setMfee(String mfee) {
        this.mfee = mfee;
        this.modify("mfee", mfee);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [税费]
     */
    public void setShuifei(Double shuifei) {
        this.shuifei = shuifei;
        this.modify("shuifei", shuifei);
    }

    /**
     * 设置 [服务费(￥)]
     */
    public void setSfee(Double sfee) {
        this.sfee = sfee;
        this.modify("sfee", sfee);
    }

    /**
     * 设置 [外委申请名称]
     */
    public void setEmapplyname(String emapplyname) {
        this.emapplyname = emapplyname;
        this.modify("emapplyname", emapplyname);
    }

    /**
     * 设置 [申请类型]
     */
    public void setApplytype(String applytype) {
        this.applytype = applytype;
        this.modify("applytype", applytype);
    }

    /**
     * 设置 [希望完成早于]
     */
    public void setApplyedate(Timestamp applyedate) {
        this.applyedate = applyedate;
        this.modify("applyedate", applyedate);
    }

    /**
     * 格式化日期 [希望完成早于]
     */
    public String formatApplyedate() {
        if (this.applyedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(applyedate);
    }
    /**
     * 设置 [申请人]
     */
    public void setMpersonid(String mpersonid) {
        this.mpersonid = mpersonid;
        this.modify("mpersonid", mpersonid);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [人工费(￥)]
     */
    public void setPfee(Double pfee) {
        this.pfee = pfee;
        this.modify("pfee", pfee);
    }

    /**
     * 设置 [预算费用(￥)]
     */
    public void setPrefee1(Double prefee1) {
        this.prefee1 = prefee1;
        this.modify("prefee1", prefee1);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [发票号]
     */
    public void setFp(String fp) {
        this.fp = fp;
        this.modify("fp", fp);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [希望开始于]
     */
    public void setApplybdate(Timestamp applybdate) {
        this.applybdate = applybdate;
        this.modify("applybdate", applybdate);
    }

    /**
     * 格式化日期 [希望开始于]
     */
    public String formatApplybdate() {
        if (this.applybdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(applybdate);
    }
    /**
     * 设置 [费用(￥)]
     */
    public void setPrefee(Double prefee) {
        this.prefee = prefee;
        this.modify("prefee", prefee);
    }

    /**
     * 设置 [关闭日期]
     */
    public void setClosedate(Timestamp closedate) {
        this.closedate = closedate;
        this.modify("closedate", closedate);
    }

    /**
     * 格式化日期 [关闭日期]
     */
    public String formatClosedate() {
        if (this.closedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(closedate);
    }
    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [计划类型]
     */
    public void setPlantype(String plantype) {
        this.plantype = plantype;
        this.modify("plantype", plantype);
    }

    /**
     * 设置 [关闭人]
     */
    public void setCloseempid(String closeempid) {
        this.closeempid = closeempid;
        this.modify("closeempid", closeempid);
    }

    /**
     * 设置 [申请日期]
     */
    public void setApplydate(Timestamp applydate) {
        this.applydate = applydate;
        this.modify("applydate", applydate);
    }

    /**
     * 格式化日期 [申请日期]
     */
    public String formatApplydate() {
        if (this.applydate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(applydate);
    }
    /**
     * 设置 [责任部门]
     */
    public void setRdeptname(String rdeptname) {
        this.rdeptname = rdeptname;
        this.modify("rdeptname", rdeptname);
    }

    /**
     * 设置 [申请人]
     */
    public void setMpersonname(String mpersonname) {
        this.mpersonname = mpersonname;
        this.modify("mpersonname", mpersonname);
    }

    /**
     * 设置 [关闭人]
     */
    public void setCloseempname(String closeempname) {
        this.closeempname = closeempname;
        this.modify("closeempname", closeempname);
    }

    /**
     * 设置 [发票附件]
     */
    public void setInvoiceattach(String invoiceattach) {
        this.invoiceattach = invoiceattach;
        this.modify("invoiceattach", invoiceattach);
    }

    /**
     * 设置 [审批意见]
     */
    public void setSpyj(String spyj) {
        this.spyj = spyj;
        this.modify("spyj", spyj);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority) {
        this.priority = priority;
        this.modify("priority", priority);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptid(String rdeptid) {
        this.rdeptid = rdeptid;
        this.modify("rdeptid", rdeptid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [方案]
     */
    public void setRfoacid(String rfoacid) {
        this.rfoacid = rfoacid;
        this.modify("rfoacid", rfoacid);
    }

    /**
     * 设置 [模式]
     */
    public void setRfomoid(String rfomoid) {
        this.rfomoid = rfomoid;
        this.modify("rfomoid", rfomoid);
    }

    /**
     * 设置 [服务商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [原因]
     */
    public void setRfocaid(String rfocaid) {
        this.rfocaid = rfocaid;
        this.modify("rfocaid", rfocaid);
    }

    /**
     * 设置 [现象]
     */
    public void setRfodeid(String rfodeid) {
        this.rfodeid = rfodeid;
        this.modify("rfodeid", rfodeid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emapplyid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


