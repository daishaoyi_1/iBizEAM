package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.EMDRWGServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[文档] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMDRWGExService")
public class EMDRWGExService extends EMDRWGServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [GenId:自动生成id] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMDRWG genId(EMDRWG et) {
        return super.genId(et);
    }
}

