package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[客户]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMCUSTOMREL_BASE", resultMap = "EMCustomRelResultMap")
public class EMCustomRel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 客户类型
     */
    @TableField(value = "customstyle")
    @JSONField(name = "customstyle")
    @JsonProperty("customstyle")
    private String customstyle;
    /**
     * 参与人员
     */
    @TableField(value = "canyurenyuan")
    @JSONField(name = "canyurenyuan")
    @JsonProperty("canyurenyuan")
    private String canyurenyuan;
    /**
     * 客户等级
     */
    @TableField(value = "degree")
    @JSONField(name = "degree")
    @JsonProperty("degree")
    private String degree;
    /**
     * 船公司
     */
    @TableField(value = "chuangongsi")
    @JSONField(name = "chuangongsi")
    @JsonProperty("chuangongsi")
    private String chuangongsi;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 上海港量
     */
    @TableField(value = "shgliang")
    @JSONField(name = "shgliang")
    @JsonProperty("shgliang")
    private String shgliang;
    /**
     * 起运港
     */
    @TableField(value = "qiyungang")
    @JSONField(name = "qiyungang")
    @JsonProperty("qiyungang")
    private String qiyungang;
    /**
     * 宁波量
     */
    @TableField(value = "lygliang")
    @JSONField(name = "lygliang")
    @JsonProperty("lygliang")
    private String lygliang;
    /**
     * 客户名称
     */
    @TableField(value = "emcustomrelname")
    @JSONField(name = "emcustomrelname")
    @JsonProperty("emcustomrelname")
    private String emcustomrelname;
    /**
     * 货名
     */
    @TableField(value = "huoming")
    @JSONField(name = "huoming")
    @JsonProperty("huoming")
    private String huoming;
    /**
     * 箱型
     */
    @TableField(value = "xiangxing")
    @JSONField(name = "xiangxing")
    @JsonProperty("xiangxing")
    private String xiangxing;
    /**
     * 目的港
     */
    @TableField(value = "mudigang")
    @JSONField(name = "mudigang")
    @JsonProperty("mudigang")
    private String mudigang;
    /**
     * 联系人
     */
    @TableField(value = "lianxiren")
    @JSONField(name = "lianxiren")
    @JsonProperty("lianxiren")
    private String lianxiren;
    /**
     * 天津港量
     */
    @TableField(value = "tjgliang")
    @JSONField(name = "tjgliang")
    @JsonProperty("tjgliang")
    private String tjgliang;
    /**
     * 行业影响力
     */
    @TableField(value = "hangyeyingxiang")
    @JSONField(name = "hangyeyingxiang")
    @JsonProperty("hangyeyingxiang")
    private String hangyeyingxiang;
    /**
     * 进出口
     */
    @TableField(value = "inout")
    @JSONField(name = "inout")
    @JsonProperty("inout")
    private String inout;
    /**
     * 备注
     */
    @TableField(value = "beizhu")
    @JSONField(name = "beizhu")
    @JsonProperty("beizhu")
    private String beizhu;
    /**
     * 企业简介
     */
    @TableField(value = "qiyejianjie")
    @JSONField(name = "qiyejianjie")
    @JsonProperty("qiyejianjie")
    private String qiyejianjie;
    /**
     * 规模
     */
    @TableField(value = "guimo")
    @JSONField(name = "guimo")
    @JsonProperty("guimo")
    private String guimo;
    /**
     * 地址
     */
    @TableField(value = "address")
    @JSONField(name = "address")
    @JsonProperty("address")
    private String address;
    /**
     * 传真
     */
    @TableField(value = "chuanzhen")
    @JSONField(name = "chuanzhen")
    @JsonProperty("chuanzhen")
    private String chuanzhen;
    /**
     * 运输条款
     */
    @TableField(value = "yunshutiaokuan")
    @JSONField(name = "yunshutiaokuan")
    @JsonProperty("yunshutiaokuan")
    private String yunshutiaokuan;
    /**
     * 邮箱
     */
    @TableField(value = "youxiang")
    @JSONField(name = "youxiang")
    @JsonProperty("youxiang")
    private String youxiang;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * TEU/年
     */
    @TableField(value = "ccount")
    @JSONField(name = "ccount")
    @JsonProperty("ccount")
    private Double ccount;
    /**
     * 客户编号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emcustomrelid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emcustomrelid")
    @JsonProperty("emcustomrelid")
    private String emcustomrelid;
    /**
     * 联系电话
     */
    @TableField(value = "lianxidianhua")
    @JSONField(name = "lianxidianhua")
    @JsonProperty("lianxidianhua")
    private String lianxidianhua;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 货物重量
     */
    @TableField(value = "cweight")
    @JSONField(name = "cweight")
    @JsonProperty("cweight")
    private Double cweight;
    /**
     * 法人
     */
    @TableField(value = "faren")
    @JSONField(name = "faren")
    @JsonProperty("faren")
    private String faren;
    /**
     * 客户需求
     */
    @TableField(value = "kehuxuqiu")
    @JSONField(name = "kehuxuqiu")
    @JsonProperty("kehuxuqiu")
    private String kehuxuqiu;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 法人联系方式
     */
    @TableField(value = "farenlianxi")
    @JSONField(name = "farenlianxi")
    @JsonProperty("farenlianxi")
    private String farenlianxi;
    /**
     * 行业
     */
    @TableField(value = "hangye")
    @JSONField(name = "hangye")
    @JsonProperty("hangye")
    private String hangye;
    /**
     * 货代及联系方式
     */
    @TableField(value = "chengyunren")
    @JSONField(name = "chengyunren")
    @JsonProperty("chengyunren")
    private String chengyunren;
    /**
     * TEU/月
     */
    @TableField(value = "yuexiangliang")
    @JSONField(name = "yuexiangliang")
    @JsonProperty("yuexiangliang")
    private String yuexiangliang;
    /**
     * 客户片区
     */
    @TableField(value = "customarea")
    @JSONField(name = "customarea")
    @JsonProperty("customarea")
    private String customarea;
    /**
     * 青岛港量
     */
    @TableField(value = "qdgliang")
    @JSONField(name = "qdgliang")
    @JsonProperty("qdgliang")
    private String qdgliang;
    /**
     * 流向
     */
    @TableField(value = "trendto")
    @JSONField(name = "trendto")
    @JsonProperty("trendto")
    private String trendto;
    /**
     * 性质
     */
    @TableField(value = "xingzhi")
    @JSONField(name = "xingzhi")
    @JsonProperty("xingzhi")
    private String xingzhi;
    /**
     * 最近走访时间
     */
    @TableField(value = "recentaccess")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "recentaccess", format = "yyyy-MM-dd")
    @JsonProperty("recentaccess")
    private Timestamp recentaccess;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 航线
     */
    @TableField(value = "tranroad")
    @JSONField(name = "tranroad")
    @JsonProperty("tranroad")
    private String tranroad;
    /**
     * 运输方式
     */
    @TableField(value = "yunshufangshi")
    @JSONField(name = "yunshufangshi")
    @JsonProperty("yunshufangshi")
    private String yunshufangshi;



    /**
     * 设置 [客户类型]
     */
    public void setCustomstyle(String customstyle) {
        this.customstyle = customstyle;
        this.modify("customstyle", customstyle);
    }

    /**
     * 设置 [参与人员]
     */
    public void setCanyurenyuan(String canyurenyuan) {
        this.canyurenyuan = canyurenyuan;
        this.modify("canyurenyuan", canyurenyuan);
    }

    /**
     * 设置 [客户等级]
     */
    public void setDegree(String degree) {
        this.degree = degree;
        this.modify("degree", degree);
    }

    /**
     * 设置 [船公司]
     */
    public void setChuangongsi(String chuangongsi) {
        this.chuangongsi = chuangongsi;
        this.modify("chuangongsi", chuangongsi);
    }

    /**
     * 设置 [上海港量]
     */
    public void setShgliang(String shgliang) {
        this.shgliang = shgliang;
        this.modify("shgliang", shgliang);
    }

    /**
     * 设置 [起运港]
     */
    public void setQiyungang(String qiyungang) {
        this.qiyungang = qiyungang;
        this.modify("qiyungang", qiyungang);
    }

    /**
     * 设置 [宁波量]
     */
    public void setLygliang(String lygliang) {
        this.lygliang = lygliang;
        this.modify("lygliang", lygliang);
    }

    /**
     * 设置 [客户名称]
     */
    public void setEmcustomrelname(String emcustomrelname) {
        this.emcustomrelname = emcustomrelname;
        this.modify("emcustomrelname", emcustomrelname);
    }

    /**
     * 设置 [货名]
     */
    public void setHuoming(String huoming) {
        this.huoming = huoming;
        this.modify("huoming", huoming);
    }

    /**
     * 设置 [箱型]
     */
    public void setXiangxing(String xiangxing) {
        this.xiangxing = xiangxing;
        this.modify("xiangxing", xiangxing);
    }

    /**
     * 设置 [目的港]
     */
    public void setMudigang(String mudigang) {
        this.mudigang = mudigang;
        this.modify("mudigang", mudigang);
    }

    /**
     * 设置 [联系人]
     */
    public void setLianxiren(String lianxiren) {
        this.lianxiren = lianxiren;
        this.modify("lianxiren", lianxiren);
    }

    /**
     * 设置 [天津港量]
     */
    public void setTjgliang(String tjgliang) {
        this.tjgliang = tjgliang;
        this.modify("tjgliang", tjgliang);
    }

    /**
     * 设置 [行业影响力]
     */
    public void setHangyeyingxiang(String hangyeyingxiang) {
        this.hangyeyingxiang = hangyeyingxiang;
        this.modify("hangyeyingxiang", hangyeyingxiang);
    }

    /**
     * 设置 [进出口]
     */
    public void setInout(String inout) {
        this.inout = inout;
        this.modify("inout", inout);
    }

    /**
     * 设置 [备注]
     */
    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
        this.modify("beizhu", beizhu);
    }

    /**
     * 设置 [企业简介]
     */
    public void setQiyejianjie(String qiyejianjie) {
        this.qiyejianjie = qiyejianjie;
        this.modify("qiyejianjie", qiyejianjie);
    }

    /**
     * 设置 [规模]
     */
    public void setGuimo(String guimo) {
        this.guimo = guimo;
        this.modify("guimo", guimo);
    }

    /**
     * 设置 [地址]
     */
    public void setAddress(String address) {
        this.address = address;
        this.modify("address", address);
    }

    /**
     * 设置 [传真]
     */
    public void setChuanzhen(String chuanzhen) {
        this.chuanzhen = chuanzhen;
        this.modify("chuanzhen", chuanzhen);
    }

    /**
     * 设置 [运输条款]
     */
    public void setYunshutiaokuan(String yunshutiaokuan) {
        this.yunshutiaokuan = yunshutiaokuan;
        this.modify("yunshutiaokuan", yunshutiaokuan);
    }

    /**
     * 设置 [邮箱]
     */
    public void setYouxiang(String youxiang) {
        this.youxiang = youxiang;
        this.modify("youxiang", youxiang);
    }

    /**
     * 设置 [TEU/年]
     */
    public void setCcount(Double ccount) {
        this.ccount = ccount;
        this.modify("ccount", ccount);
    }

    /**
     * 设置 [联系电话]
     */
    public void setLianxidianhua(String lianxidianhua) {
        this.lianxidianhua = lianxidianhua;
        this.modify("lianxidianhua", lianxidianhua);
    }

    /**
     * 设置 [货物重量]
     */
    public void setCweight(Double cweight) {
        this.cweight = cweight;
        this.modify("cweight", cweight);
    }

    /**
     * 设置 [法人]
     */
    public void setFaren(String faren) {
        this.faren = faren;
        this.modify("faren", faren);
    }

    /**
     * 设置 [客户需求]
     */
    public void setKehuxuqiu(String kehuxuqiu) {
        this.kehuxuqiu = kehuxuqiu;
        this.modify("kehuxuqiu", kehuxuqiu);
    }

    /**
     * 设置 [法人联系方式]
     */
    public void setFarenlianxi(String farenlianxi) {
        this.farenlianxi = farenlianxi;
        this.modify("farenlianxi", farenlianxi);
    }

    /**
     * 设置 [行业]
     */
    public void setHangye(String hangye) {
        this.hangye = hangye;
        this.modify("hangye", hangye);
    }

    /**
     * 设置 [货代及联系方式]
     */
    public void setChengyunren(String chengyunren) {
        this.chengyunren = chengyunren;
        this.modify("chengyunren", chengyunren);
    }

    /**
     * 设置 [TEU/月]
     */
    public void setYuexiangliang(String yuexiangliang) {
        this.yuexiangliang = yuexiangliang;
        this.modify("yuexiangliang", yuexiangliang);
    }

    /**
     * 设置 [客户片区]
     */
    public void setCustomarea(String customarea) {
        this.customarea = customarea;
        this.modify("customarea", customarea);
    }

    /**
     * 设置 [青岛港量]
     */
    public void setQdgliang(String qdgliang) {
        this.qdgliang = qdgliang;
        this.modify("qdgliang", qdgliang);
    }

    /**
     * 设置 [流向]
     */
    public void setTrendto(String trendto) {
        this.trendto = trendto;
        this.modify("trendto", trendto);
    }

    /**
     * 设置 [性质]
     */
    public void setXingzhi(String xingzhi) {
        this.xingzhi = xingzhi;
        this.modify("xingzhi", xingzhi);
    }

    /**
     * 设置 [最近走访时间]
     */
    public void setRecentaccess(Timestamp recentaccess) {
        this.recentaccess = recentaccess;
        this.modify("recentaccess", recentaccess);
    }

    /**
     * 格式化日期 [最近走访时间]
     */
    public String formatRecentaccess() {
        if (this.recentaccess == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(recentaccess);
    }
    /**
     * 设置 [航线]
     */
    public void setTranroad(String tranroad) {
        this.tranroad = tranroad;
        this.modify("tranroad", tranroad);
    }

    /**
     * 设置 [运输方式]
     */
    public void setYunshufangshi(String yunshufangshi) {
        this.yunshufangshi = yunshufangshi;
        this.modify("yunshufangshi", yunshufangshi);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emcustomrelid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


