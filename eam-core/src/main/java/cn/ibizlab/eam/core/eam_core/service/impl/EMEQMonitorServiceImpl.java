package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMonitor;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMonitorSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQMonitorService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQMonitorMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备状态监控] 服务对象接口实现
 */
@Slf4j
@Service("EMEQMonitorServiceImpl")
public class EMEQMonitorServiceImpl extends ServiceImpl<EMEQMonitorMapper, EMEQMonitor> implements IEMEQMonitorService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQMonitor et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqmonitorid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQMonitor> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQMonitor et) {
        fillParentData(et);
        emdprctService.update(emeqmonitorInheritMapping.toEmdprct(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqmonitorid", et.getEmeqmonitorid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqmonitorid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQMonitor> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emdprctService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQMonitor get(String key) {
        EMEQMonitor et = getById(key);
        if(et == null){
            et = new EMEQMonitor();
            et.setEmeqmonitorid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQMonitor getDraft(EMEQMonitor et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQMonitor et) {
        return (!ObjectUtils.isEmpty(et.getEmeqmonitorid())) && (!Objects.isNull(this.getById(et.getEmeqmonitorid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQMonitor et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQMonitor et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQMonitor> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQMonitor> create = new ArrayList<>();
        List<EMEQMonitor> update = new ArrayList<>();
        for (EMEQMonitor et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqmonitorid()) || ObjectUtils.isEmpty(getById(et.getEmeqmonitorid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQMonitor> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQMonitor> create = new ArrayList<>();
        List<EMEQMonitor> update = new ArrayList<>();
        for (EMEQMonitor et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqmonitorid()) || ObjectUtils.isEmpty(getById(et.getEmeqmonitorid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQMonitor> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQMonitor>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEQMonitor> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }
    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMEQMonitor>().eq("woid",emwoid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQMonitor> searchDefault(EMEQMonitorSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQMonitor> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQMonitor>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQMonitor et){
        //实体关系[DER1N_EMEQMONITOR_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEQMONITOR_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQMonitorInheritMapping emeqmonitorInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService emdprctService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQMonitor et){
        if(ObjectUtils.isEmpty(et.getEmeqmonitorid()))
            et.setEmeqmonitorid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMDPRCT emdprct =emeqmonitorInheritMapping.toEmdprct(et);
        emdprct.set("emdprcttype","EQMONITOR");
        emdprctService.create(emdprct);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQMonitor> getEmeqmonitorByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQMonitor> getEmeqmonitorByEntities(List<EMEQMonitor> entities) {
        List ids =new ArrayList();
        for(EMEQMonitor entity : entities){
            Serializable id=entity.getEmeqmonitorid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQMonitorService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



