package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanCDT;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanCDTSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMPlanCDTMapper extends BaseMapper<EMPlanCDT> {

    Page<EMPlanCDT> searchDefault(IPage page, @Param("srf") EMPlanCDTSearchContext context, @Param("ew") Wrapper<EMPlanCDT> wrapper);
    @Override
    EMPlanCDT selectById(Serializable id);
    @Override
    int insert(EMPlanCDT entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMPlanCDT entity);
    @Override
    int update(@Param(Constants.ENTITY) EMPlanCDT entity, @Param("ew") Wrapper<EMPlanCDT> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMPlanCDT> selectByEquipid(@Param("emequipid") Serializable emequipid);

    List<EMPlanCDT> selectByDpid(@Param("emobjectid") Serializable emobjectid);

    List<EMPlanCDT> selectByObjid(@Param("emobjectid") Serializable emobjectid);

    List<EMPlanCDT> selectByPlanid(@Param("emplanid") Serializable emplanid);

}
