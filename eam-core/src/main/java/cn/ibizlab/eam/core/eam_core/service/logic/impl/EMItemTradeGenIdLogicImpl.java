package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMItemTradeGenIdLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;

/**
 * 关系型数据实体[GenId] 对象
 */
@Slf4j
@Service
public class EMItemTradeGenIdLogicImpl implements IEMItemTradeGenIdLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService getEmitemtradeService() {
        return this.emitemtradeservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMItemTrade et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emitemtradegeniddefault", et);
            kieSession.setGlobal("emitemtradeservice", emitemtradeservice);
            kieSession.setGlobal("iBzSysEmitemtradeDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emitemtradegenid");

        } catch (Exception e) {
            throw new RuntimeException("执行[生成id]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
