package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[领料单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMITEMPUSE_BASE", resultMap = "EMItemPUseResultMap")
public class EMItemPUse extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @JSONField(name = "remark")
    @JsonProperty("remark")
    private String remark;
    /**
     * 领料单名称
     */
    @DEField(defaultValue = "PUSE")
    @TableField(value = "emitempusename")
    @JSONField(name = "emitempusename")
    @JsonProperty("emitempusename")
    private String emitempusename;
    /**
     * 领料单信息
     */
    @TableField(exist = false)
    @JSONField(name = "itempuseinfo")
    @JsonProperty("itempuseinfo")
    private String itempuseinfo;
    /**
     * 删除标识
     */
    @DEField(defaultValue = "0")
    @TableField(value = "deltype")
    @JSONField(name = "deltype")
    @JsonProperty("deltype")
    private Integer deltype;
    /**
     * 处理意见
     */
    @TableField(value = "opinion")
    @JSONField(name = "opinion")
    @JsonProperty("opinion")
    private String opinion;
    /**
     * 当前仓库库存
     */
    @TableField(exist = false)
    @JSONField(name = "stocknum")
    @JsonProperty("stocknum")
    private Double stocknum;
    /**
     * 审核成功次数
     */
    @DEField(defaultValue = "0")
    @TableField(value = "approknum")
    @JSONField(name = "approknum")
    @JsonProperty("approknum")
    private Integer approknum;
    /**
     * 设备集合
     */
    @TableField(value = "equips")
    @JSONField(name = "equips")
    @JsonProperty("equips")
    private String equips;
    /**
     * 实发数
     */
    @TableField(value = "psum")
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 单价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;
    /**
     * 批次
     */
    @DEField(defaultValue = "NA")
    @TableField(value = "batcode")
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    private String batcode;
    /**
     * 备注
     */
    @TableField(value = "bz")
    @JSONField(name = "bz")
    @JsonProperty("bz")
    private String bz;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 总金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * sap传输异常文本
     */
    @TableField(value = "sapreason1")
    @JSONField(name = "sapreason1")
    @JsonProperty("sapreason1")
    private String sapreason1;
    /**
     * 领料状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "pusestate")
    @JSONField(name = "pusestate")
    @JsonProperty("pusestate")
    private Integer pusestate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 审核意见
     */
    @TableField(value = "apprdesc")
    @JSONField(name = "apprdesc")
    @JsonProperty("apprdesc")
    private String apprdesc;
    /**
     * sap成本中心
     */
    @TableField(value = "sapcbzx")
    @JSONField(name = "sapcbzx")
    @JsonProperty("sapcbzx")
    private String sapcbzx;
    /**
     * sap领料用途
     */
    @TableField(value = "sapllyt")
    @JSONField(name = "sapllyt")
    @JsonProperty("sapllyt")
    private String sapllyt;
    /**
     * 请领实发差
     */
    @TableField(exist = false)
    @JSONField(name = "numdiff")
    @JsonProperty("numdiff")
    private Double numdiff;
    /**
     * 请领数
     */
    @TableField(value = "asum")
    @JSONField(name = "asum")
    @JsonProperty("asum")
    private Double asum;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * sap传输状态
     */
    @TableField(value = "sap")
    @JSONField(name = "sap")
    @JsonProperty("sap")
    private Integer sap;
    /**
     * 发料日期
     */
    @TableField(value = "sdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "sdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    private Timestamp sdate;
    /**
     * 寿命周期
     */
    @TableField(value = "life2")
    @JSONField(name = "life2")
    @JsonProperty("life2")
    private Integer life2;
    /**
     * 领料单号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emitempuseid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emitempuseid")
    @JsonProperty("emitempuseid")
    private String emitempuseid;
    /**
     * 未摊销数量
     */
    @TableField(value = "stock2num")
    @JSONField(name = "stock2num")
    @JsonProperty("stock2num")
    private Double stock2num;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 用途
     */
    @DEField(defaultValue = "EQUIP")
    @TableField(value = "useto")
    @JSONField(name = "useto")
    @JsonProperty("useto")
    private String useto;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 领料分类
     */
    @DEField(defaultValue = "INNER")
    @TableField(value = "pusetype")
    @JSONField(name = "pusetype")
    @JsonProperty("pusetype")
    private String pusetype;
    /**
     * sap控制
     */
    @TableField(value = "sapcontrol")
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    private Integer sapcontrol;
    /**
     * 申请日期
     */
    @TableField(value = "adate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "adate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    private Timestamp adate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 批准日期
     */
    @TableField(value = "apprdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "apprdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    private Timestamp apprdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 寿命周期(天)
     */
    @TableField(exist = false)
    @JSONField(name = "life")
    @JsonProperty("life")
    private Integer life;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "storename")
    @JsonProperty("storename")
    private String storename;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    private String unitname;
    /**
     * 领料位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    private String unitid;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    private String itembtypeid;
    /**
     * 领料班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    private String teamname;
    /**
     * 物品分组
     */
    @TableField(exist = false)
    @JSONField(name = "itemgroup")
    @JsonProperty("itemgroup")
    private Integer itemgroup;
    /**
     * 建议供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    private String labservicename;
    /**
     * 物品均价
     */
    @TableField(exist = false)
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    private Double avgprice;
    /**
     * 领料物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 制造商
     */
    @TableField(exist = false)
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    private String mservicename;
    /**
     * 库位
     */
    @TableField(exist = false)
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    private String storepartname;
    /**
     * 领料设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 采购计划
     */
    @TableField(exist = false)
    @JSONField(name = "purplanname")
    @JsonProperty("purplanname")
    private String purplanname;
    /**
     * 领料工单
     */
    @TableField(exist = false)
    @JSONField(name = "woname")
    @JsonProperty("woname")
    private String woname;
    /**
     * 仓库
     */
    @TableField(value = "storeid")
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    private String storeid;
    /**
     * 领料物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 领料班组
     */
    @TableField(value = "teamid")
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    private String teamid;
    /**
     * 领料设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 建议供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    private String labserviceid;
    /**
     * 领料工单
     */
    @TableField(value = "woid")
    @JSONField(name = "woid")
    @JsonProperty("woid")
    private String woid;
    /**
     * 制造商
     */
    @TableField(value = "mserviceid")
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    private String mserviceid;
    /**
     * 领料位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 采购计划
     */
    @TableField(value = "purplanid")
    @JSONField(name = "purplanid")
    @JsonProperty("purplanid")
    private String purplanid;
    /**
     * 库位
     */
    @TableField(value = "storepartid")
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    private String storepartid;
    /**
     * 申请人
     */
    @TableField(value = "aempid")
    @JSONField(name = "aempid")
    @JsonProperty("aempid")
    private String aempid;
    /**
     * 申请人
     */
    @TableField(exist = false)
    @JSONField(name = "aempname")
    @JsonProperty("aempname")
    private String aempname;
    /**
     * 发料人
     */
    @TableField(value = "sempid")
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    private String sempid;
    /**
     * 发料人
     */
    @TableField(exist = false)
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    private String sempname;
    /**
     * 领料人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 领料人
     */
    @TableField(exist = false)
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 批准人
     */
    @TableField(value = "apprempid")
    @JSONField(name = "apprempid")
    @JsonProperty("apprempid")
    private String apprempid;
    /**
     * 批准人
     */
    @TableField(exist = false)
    @JSONField(name = "apprempname")
    @JsonProperty("apprempname")
    private String apprempname;
    /**
     * 领料部门
     */
    @TableField(value = "deptid")
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    private String deptid;
    /**
     * 领料部门
     */
    @TableField(exist = false)
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    private String deptname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPurPlan purplan;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService mservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStore store;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;

    /**
     * 批准人
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfeappre;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfeemp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp spfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam team;



    /**
     * 设置 [备注]
     */
    public void setRemark(String remark) {
        this.remark = remark;
        this.modify("remark", remark);
    }

    /**
     * 设置 [领料单名称]
     */
    public void setEmitempusename(String emitempusename) {
        this.emitempusename = emitempusename;
        this.modify("emitempusename", emitempusename);
    }

    /**
     * 设置 [删除标识]
     */
    public void setDeltype(Integer deltype) {
        this.deltype = deltype;
        this.modify("deltype", deltype);
    }

    /**
     * 设置 [处理意见]
     */
    public void setOpinion(String opinion) {
        this.opinion = opinion;
        this.modify("opinion", opinion);
    }

    /**
     * 设置 [审核成功次数]
     */
    public void setApproknum(Integer approknum) {
        this.approknum = approknum;
        this.modify("approknum", approknum);
    }

    /**
     * 设置 [设备集合]
     */
    public void setEquips(String equips) {
        this.equips = equips;
        this.modify("equips", equips);
    }

    /**
     * 设置 [实发数]
     */
    public void setPsum(Double psum) {
        this.psum = psum;
        this.modify("psum", psum);
    }

    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [批次]
     */
    public void setBatcode(String batcode) {
        this.batcode = batcode;
        this.modify("batcode", batcode);
    }

    /**
     * 设置 [备注]
     */
    public void setBz(String bz) {
        this.bz = bz;
        this.modify("bz", bz);
    }

    /**
     * 设置 [总金额]
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [sap传输异常文本]
     */
    public void setSapreason1(String sapreason1) {
        this.sapreason1 = sapreason1;
        this.modify("sapreason1", sapreason1);
    }

    /**
     * 设置 [领料状态]
     */
    public void setPusestate(Integer pusestate) {
        this.pusestate = pusestate;
        this.modify("pusestate", pusestate);
    }

    /**
     * 设置 [审核意见]
     */
    public void setApprdesc(String apprdesc) {
        this.apprdesc = apprdesc;
        this.modify("apprdesc", apprdesc);
    }

    /**
     * 设置 [sap成本中心]
     */
    public void setSapcbzx(String sapcbzx) {
        this.sapcbzx = sapcbzx;
        this.modify("sapcbzx", sapcbzx);
    }

    /**
     * 设置 [sap领料用途]
     */
    public void setSapllyt(String sapllyt) {
        this.sapllyt = sapllyt;
        this.modify("sapllyt", sapllyt);
    }

    /**
     * 设置 [请领数]
     */
    public void setAsum(Double asum) {
        this.asum = asum;
        this.modify("asum", asum);
    }

    /**
     * 设置 [sap传输状态]
     */
    public void setSap(Integer sap) {
        this.sap = sap;
        this.modify("sap", sap);
    }

    /**
     * 设置 [发料日期]
     */
    public void setSdate(Timestamp sdate) {
        this.sdate = sdate;
        this.modify("sdate", sdate);
    }

    /**
     * 格式化日期 [发料日期]
     */
    public String formatSdate() {
        if (this.sdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sdate);
    }
    /**
     * 设置 [寿命周期]
     */
    public void setLife2(Integer life2) {
        this.life2 = life2;
        this.modify("life2", life2);
    }

    /**
     * 设置 [未摊销数量]
     */
    public void setStock2num(Double stock2num) {
        this.stock2num = stock2num;
        this.modify("stock2num", stock2num);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [用途]
     */
    public void setUseto(String useto) {
        this.useto = useto;
        this.modify("useto", useto);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [领料分类]
     */
    public void setPusetype(String pusetype) {
        this.pusetype = pusetype;
        this.modify("pusetype", pusetype);
    }

    /**
     * 设置 [sap控制]
     */
    public void setSapcontrol(Integer sapcontrol) {
        this.sapcontrol = sapcontrol;
        this.modify("sapcontrol", sapcontrol);
    }

    /**
     * 设置 [申请日期]
     */
    public void setAdate(Timestamp adate) {
        this.adate = adate;
        this.modify("adate", adate);
    }

    /**
     * 格式化日期 [申请日期]
     */
    public String formatAdate() {
        if (this.adate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(adate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [批准日期]
     */
    public void setApprdate(Timestamp apprdate) {
        this.apprdate = apprdate;
        this.modify("apprdate", apprdate);
    }

    /**
     * 格式化日期 [批准日期]
     */
    public String formatApprdate() {
        if (this.apprdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(apprdate);
    }
    /**
     * 设置 [仓库]
     */
    public void setStoreid(String storeid) {
        this.storeid = storeid;
        this.modify("storeid", storeid);
    }

    /**
     * 设置 [领料物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [领料班组]
     */
    public void setTeamid(String teamid) {
        this.teamid = teamid;
        this.modify("teamid", teamid);
    }

    /**
     * 设置 [领料设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [建议供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [领料工单]
     */
    public void setWoid(String woid) {
        this.woid = woid;
        this.modify("woid", woid);
    }

    /**
     * 设置 [制造商]
     */
    public void setMserviceid(String mserviceid) {
        this.mserviceid = mserviceid;
        this.modify("mserviceid", mserviceid);
    }

    /**
     * 设置 [领料位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [采购计划]
     */
    public void setPurplanid(String purplanid) {
        this.purplanid = purplanid;
        this.modify("purplanid", purplanid);
    }

    /**
     * 设置 [库位]
     */
    public void setStorepartid(String storepartid) {
        this.storepartid = storepartid;
        this.modify("storepartid", storepartid);
    }

    /**
     * 设置 [申请人]
     */
    public void setAempid(String aempid) {
        this.aempid = aempid;
        this.modify("aempid", aempid);
    }

    /**
     * 设置 [发料人]
     */
    public void setSempid(String sempid) {
        this.sempid = sempid;
        this.modify("sempid", sempid);
    }

    /**
     * 设置 [领料人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [批准人]
     */
    public void setApprempid(String apprempid) {
        this.apprempid = apprempid;
        this.modify("apprempid", apprempid);
    }

    /**
     * 设置 [领料部门]
     */
    public void setDeptid(String deptid) {
        this.deptid = deptid;
        this.modify("deptid", deptid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emitempuseid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


