package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEquip;
/**
 * 关系型数据实体[EMEquip] 查询条件对象
 */
@Slf4j
@Data
public class EMEquipSearchContext extends QueryWrapperContext<EMEquip> {

	private String n_emequipname_like;//[设备名称]
	public void setN_emequipname_like(String n_emequipname_like) {
        this.n_emequipname_like = n_emequipname_like;
        if(!ObjectUtils.isEmpty(this.n_emequipname_like)){
            this.getSearchCond().like("emequipname", n_emequipname_like);
        }
    }
	private String n_deptid_eq;//[专责部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_haltstate_eq;//[停机类型]
	public void setN_haltstate_eq(String n_haltstate_eq) {
        this.n_haltstate_eq = n_haltstate_eq;
        if(!ObjectUtils.isEmpty(this.n_haltstate_eq)){
            this.getSearchCond().eq("haltstate", n_haltstate_eq);
        }
    }
	private String n_eqstate_eq;//[设备状态]
	public void setN_eqstate_eq(String n_eqstate_eq) {
        this.n_eqstate_eq = n_eqstate_eq;
        if(!ObjectUtils.isEmpty(this.n_eqstate_eq)){
            this.getSearchCond().eq("eqstate", n_eqstate_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_equipcode_like;//[设备代码]
	public void setN_equipcode_like(String n_equipcode_like) {
        this.n_equipcode_like = n_equipcode_like;
        if(!ObjectUtils.isEmpty(this.n_equipcode_like)){
            this.getSearchCond().like("equipcode", n_equipcode_like);
        }
    }
	private String n_empid_eq;//[专责人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private Integer n_equipgroup_eq;//[设备分组]
	public void setN_equipgroup_eq(Integer n_equipgroup_eq) {
        this.n_equipgroup_eq = n_equipgroup_eq;
        if(!ObjectUtils.isEmpty(this.n_equipgroup_eq)){
            this.getSearchCond().eq("equipgroup", n_equipgroup_eq);
        }
    }
	private String n_deptname_eq;//[专责部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[专责部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_equipinfo_like;//[设备信息]
	public void setN_equipinfo_like(String n_equipinfo_like) {
        this.n_equipinfo_like = n_equipinfo_like;
        if(!ObjectUtils.isEmpty(this.n_equipinfo_like)){
            this.getSearchCond().like("equipinfo", n_equipinfo_like);
        }
    }
	private String n_empname_eq;//[专责人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[专责人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_rteamname_eq;//[责任班组]
	public void setN_rteamname_eq(String n_rteamname_eq) {
        this.n_rteamname_eq = n_rteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamname_eq)){
            this.getSearchCond().eq("rteamname", n_rteamname_eq);
        }
    }
	private String n_rteamname_like;//[责任班组]
	public void setN_rteamname_like(String n_rteamname_like) {
        this.n_rteamname_like = n_rteamname_like;
        if(!ObjectUtils.isEmpty(this.n_rteamname_like)){
            this.getSearchCond().like("rteamname", n_rteamname_like);
        }
    }
	private String n_emmachinecategoryname_eq;//[机种]
	public void setN_emmachinecategoryname_eq(String n_emmachinecategoryname_eq) {
        this.n_emmachinecategoryname_eq = n_emmachinecategoryname_eq;
        if(!ObjectUtils.isEmpty(this.n_emmachinecategoryname_eq)){
            this.getSearchCond().eq("emmachinecategoryname", n_emmachinecategoryname_eq);
        }
    }
	private String n_emmachinecategoryname_like;//[机种]
	public void setN_emmachinecategoryname_like(String n_emmachinecategoryname_like) {
        this.n_emmachinecategoryname_like = n_emmachinecategoryname_like;
        if(!ObjectUtils.isEmpty(this.n_emmachinecategoryname_like)){
            this.getSearchCond().like("emmachinecategoryname", n_emmachinecategoryname_like);
        }
    }
	private String n_mservicename_eq;//[制造商]
	public void setN_mservicename_eq(String n_mservicename_eq) {
        this.n_mservicename_eq = n_mservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_mservicename_eq)){
            this.getSearchCond().eq("mservicename", n_mservicename_eq);
        }
    }
	private String n_mservicename_like;//[制造商]
	public void setN_mservicename_like(String n_mservicename_like) {
        this.n_mservicename_like = n_mservicename_like;
        if(!ObjectUtils.isEmpty(this.n_mservicename_like)){
            this.getSearchCond().like("mservicename", n_mservicename_like);
        }
    }
	private String n_eqtypecode_like;//[设备类型代码]
	public void setN_eqtypecode_like(String n_eqtypecode_like) {
        this.n_eqtypecode_like = n_eqtypecode_like;
        if(!ObjectUtils.isEmpty(this.n_eqtypecode_like)){
            this.getSearchCond().like("eqtypecode", n_eqtypecode_like);
        }
    }
	private String n_eqtypename_eq;//[设备类型]
	public void setN_eqtypename_eq(String n_eqtypename_eq) {
        this.n_eqtypename_eq = n_eqtypename_eq;
        if(!ObjectUtils.isEmpty(this.n_eqtypename_eq)){
            this.getSearchCond().eq("eqtypename", n_eqtypename_eq);
        }
    }
	private String n_eqtypename_like;//[设备类型]
	public void setN_eqtypename_like(String n_eqtypename_like) {
        this.n_eqtypename_like = n_eqtypename_like;
        if(!ObjectUtils.isEmpty(this.n_eqtypename_like)){
            this.getSearchCond().like("eqtypename", n_eqtypename_like);
        }
    }
	private String n_emmachmodelname_eq;//[机型]
	public void setN_emmachmodelname_eq(String n_emmachmodelname_eq) {
        this.n_emmachmodelname_eq = n_emmachmodelname_eq;
        if(!ObjectUtils.isEmpty(this.n_emmachmodelname_eq)){
            this.getSearchCond().eq("emmachmodelname", n_emmachmodelname_eq);
        }
    }
	private String n_emmachmodelname_like;//[机型]
	public void setN_emmachmodelname_like(String n_emmachmodelname_like) {
        this.n_emmachmodelname_like = n_emmachmodelname_like;
        if(!ObjectUtils.isEmpty(this.n_emmachmodelname_like)){
            this.getSearchCond().like("emmachmodelname", n_emmachmodelname_like);
        }
    }
	private String n_acclassname_eq;//[总帐科目]
	public void setN_acclassname_eq(String n_acclassname_eq) {
        this.n_acclassname_eq = n_acclassname_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassname_eq)){
            this.getSearchCond().eq("acclassname", n_acclassname_eq);
        }
    }
	private String n_acclassname_like;//[总帐科目]
	public void setN_acclassname_like(String n_acclassname_like) {
        this.n_acclassname_like = n_acclassname_like;
        if(!ObjectUtils.isEmpty(this.n_acclassname_like)){
            this.getSearchCond().like("acclassname", n_acclassname_like);
        }
    }
	private String n_labservicename_eq;//[产品供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[产品供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_assetname_eq;//[资产]
	public void setN_assetname_eq(String n_assetname_eq) {
        this.n_assetname_eq = n_assetname_eq;
        if(!ObjectUtils.isEmpty(this.n_assetname_eq)){
            this.getSearchCond().eq("assetname", n_assetname_eq);
        }
    }
	private String n_assetname_like;//[资产]
	public void setN_assetname_like(String n_assetname_like) {
        this.n_assetname_like = n_assetname_like;
        if(!ObjectUtils.isEmpty(this.n_assetname_like)){
            this.getSearchCond().like("assetname", n_assetname_like);
        }
    }
	private String n_embrandname_eq;//[品牌]
	public void setN_embrandname_eq(String n_embrandname_eq) {
        this.n_embrandname_eq = n_embrandname_eq;
        if(!ObjectUtils.isEmpty(this.n_embrandname_eq)){
            this.getSearchCond().eq("embrandname", n_embrandname_eq);
        }
    }
	private String n_embrandname_like;//[品牌]
	public void setN_embrandname_like(String n_embrandname_like) {
        this.n_embrandname_like = n_embrandname_like;
        if(!ObjectUtils.isEmpty(this.n_embrandname_like)){
            this.getSearchCond().like("embrandname", n_embrandname_like);
        }
    }
	private String n_eqlocationname_eq;//[位置]
	public void setN_eqlocationname_eq(String n_eqlocationname_eq) {
        this.n_eqlocationname_eq = n_eqlocationname_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_eq)){
            this.getSearchCond().eq("eqlocationname", n_eqlocationname_eq);
        }
    }
	private String n_eqlocationname_like;//[位置]
	public void setN_eqlocationname_like(String n_eqlocationname_like) {
        this.n_eqlocationname_like = n_eqlocationname_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_like)){
            this.getSearchCond().like("eqlocationname", n_eqlocationname_like);
        }
    }
	private String n_equippname_eq;//[上级设备]
	public void setN_equippname_eq(String n_equippname_eq) {
        this.n_equippname_eq = n_equippname_eq;
        if(!ObjectUtils.isEmpty(this.n_equippname_eq)){
            this.getSearchCond().eq("equippname", n_equippname_eq);
        }
    }
	private String n_equippname_like;//[上级设备]
	public void setN_equippname_like(String n_equippname_like) {
        this.n_equippname_like = n_equippname_like;
        if(!ObjectUtils.isEmpty(this.n_equippname_like)){
            this.getSearchCond().like("equippname", n_equippname_like);
        }
    }
	private String n_contractname_eq;//[合同]
	public void setN_contractname_eq(String n_contractname_eq) {
        this.n_contractname_eq = n_contractname_eq;
        if(!ObjectUtils.isEmpty(this.n_contractname_eq)){
            this.getSearchCond().eq("contractname", n_contractname_eq);
        }
    }
	private String n_contractname_like;//[合同]
	public void setN_contractname_like(String n_contractname_like) {
        this.n_contractname_like = n_contractname_like;
        if(!ObjectUtils.isEmpty(this.n_contractname_like)){
            this.getSearchCond().like("contractname", n_contractname_like);
        }
    }
	private String n_emberthname_eq;//[泊位]
	public void setN_emberthname_eq(String n_emberthname_eq) {
        this.n_emberthname_eq = n_emberthname_eq;
        if(!ObjectUtils.isEmpty(this.n_emberthname_eq)){
            this.getSearchCond().eq("emberthname", n_emberthname_eq);
        }
    }
	private String n_emberthname_like;//[泊位]
	public void setN_emberthname_like(String n_emberthname_like) {
        this.n_emberthname_like = n_emberthname_like;
        if(!ObjectUtils.isEmpty(this.n_emberthname_like)){
            this.getSearchCond().like("emberthname", n_emberthname_like);
        }
    }
	private String n_rservicename_eq;//[服务提供商]
	public void setN_rservicename_eq(String n_rservicename_eq) {
        this.n_rservicename_eq = n_rservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_rservicename_eq)){
            this.getSearchCond().eq("rservicename", n_rservicename_eq);
        }
    }
	private String n_rservicename_like;//[服务提供商]
	public void setN_rservicename_like(String n_rservicename_like) {
        this.n_rservicename_like = n_rservicename_like;
        if(!ObjectUtils.isEmpty(this.n_rservicename_like)){
            this.getSearchCond().like("rservicename", n_rservicename_like);
        }
    }
	private String n_eqlocationid_eq;//[位置]
	public void setN_eqlocationid_eq(String n_eqlocationid_eq) {
        this.n_eqlocationid_eq = n_eqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationid_eq)){
            this.getSearchCond().eq("eqlocationid", n_eqlocationid_eq);
        }
    }
	private String n_emmachinecategoryid_eq;//[机种]
	public void setN_emmachinecategoryid_eq(String n_emmachinecategoryid_eq) {
        this.n_emmachinecategoryid_eq = n_emmachinecategoryid_eq;
        if(!ObjectUtils.isEmpty(this.n_emmachinecategoryid_eq)){
            this.getSearchCond().eq("emmachinecategoryid", n_emmachinecategoryid_eq);
        }
    }
	private String n_emberthid_eq;//[泊位]
	public void setN_emberthid_eq(String n_emberthid_eq) {
        this.n_emberthid_eq = n_emberthid_eq;
        if(!ObjectUtils.isEmpty(this.n_emberthid_eq)){
            this.getSearchCond().eq("emberthid", n_emberthid_eq);
        }
    }
	private String n_rteamid_eq;//[责任班组]
	public void setN_rteamid_eq(String n_rteamid_eq) {
        this.n_rteamid_eq = n_rteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamid_eq)){
            this.getSearchCond().eq("rteamid", n_rteamid_eq);
        }
    }
	private String n_embrandid_eq;//[品牌]
	public void setN_embrandid_eq(String n_embrandid_eq) {
        this.n_embrandid_eq = n_embrandid_eq;
        if(!ObjectUtils.isEmpty(this.n_embrandid_eq)){
            this.getSearchCond().eq("embrandid", n_embrandid_eq);
        }
    }
	private String n_equippid_eq;//[上级设备]
	public void setN_equippid_eq(String n_equippid_eq) {
        this.n_equippid_eq = n_equippid_eq;
        if(!ObjectUtils.isEmpty(this.n_equippid_eq)){
            this.getSearchCond().eq("equippid", n_equippid_eq);
        }
    }
	private String n_labserviceid_eq;//[产品供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_rserviceid_eq;//[服务提供商]
	public void setN_rserviceid_eq(String n_rserviceid_eq) {
        this.n_rserviceid_eq = n_rserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_rserviceid_eq)){
            this.getSearchCond().eq("rserviceid", n_rserviceid_eq);
        }
    }
	private String n_emmachmodelid_eq;//[机型]
	public void setN_emmachmodelid_eq(String n_emmachmodelid_eq) {
        this.n_emmachmodelid_eq = n_emmachmodelid_eq;
        if(!ObjectUtils.isEmpty(this.n_emmachmodelid_eq)){
            this.getSearchCond().eq("emmachmodelid", n_emmachmodelid_eq);
        }
    }
	private String n_contractid_eq;//[合同]
	public void setN_contractid_eq(String n_contractid_eq) {
        this.n_contractid_eq = n_contractid_eq;
        if(!ObjectUtils.isEmpty(this.n_contractid_eq)){
            this.getSearchCond().eq("contractid", n_contractid_eq);
        }
    }
	private String n_eqtypeid_eq;//[设备类型]
	public void setN_eqtypeid_eq(String n_eqtypeid_eq) {
        this.n_eqtypeid_eq = n_eqtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqtypeid_eq)){
            this.getSearchCond().eq("eqtypeid", n_eqtypeid_eq);
        }
    }
	private String n_acclassid_eq;//[总帐科目]
	public void setN_acclassid_eq(String n_acclassid_eq) {
        this.n_acclassid_eq = n_acclassid_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassid_eq)){
            this.getSearchCond().eq("acclassid", n_acclassid_eq);
        }
    }
	private String n_assetid_eq;//[资产]
	public void setN_assetid_eq(String n_assetid_eq) {
        this.n_assetid_eq = n_assetid_eq;
        if(!ObjectUtils.isEmpty(this.n_assetid_eq)){
            this.getSearchCond().eq("assetid", n_assetid_eq);
        }
    }
	private String n_mserviceid_eq;//[制造商]
	public void setN_mserviceid_eq(String n_mserviceid_eq) {
        this.n_mserviceid_eq = n_mserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_mserviceid_eq)){
            this.getSearchCond().eq("mserviceid", n_mserviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emequipname", query)
            );
		 }
	}
}



