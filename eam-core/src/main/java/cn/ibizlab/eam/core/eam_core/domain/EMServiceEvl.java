package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[服务商评估]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMSERVICEEVL_BASE", resultMap = "EMServiceEvlResultMap")
public class EMServiceEvl extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 服务商评估标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emserviceevlid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emserviceevlid")
    @JsonProperty("emserviceevlid")
    private String emserviceevlid;
    /**
     * 服务商评估名称
     */
    @DEField(defaultValue = "-")
    @TableField(value = "emserviceevlname")
    @JSONField(name = "emserviceevlname")
    @JsonProperty("emserviceevlname")
    private String emserviceevlname;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 售后(100分,10%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult9")
    @JSONField(name = "evlresult9")
    @JsonProperty("evlresult9")
    private Integer evlresult9;
    /**
     * 供货能力(100分,10%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult5")
    @JSONField(name = "evlresult5")
    @JsonProperty("evlresult5")
    private Integer evlresult5;
    /**
     * 距离(100分,5%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult7")
    @JSONField(name = "evlresult7")
    @JsonProperty("evlresult7")
    private Integer evlresult7;
    /**
     * 及时性(100分,10%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult6")
    @JSONField(name = "evlresult6")
    @JsonProperty("evlresult6")
    private Integer evlresult6;
    /**
     * 资质(100分,5%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult1")
    @JSONField(name = "evlresult1")
    @JsonProperty("evlresult1")
    private Integer evlresult1;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 评价
     */
    @TableField(value = "evlmark1")
    @JSONField(name = "evlmark1")
    @JsonProperty("evlmark1")
    private String evlmark1;
    /**
     * 安全性能(100分,10%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult4")
    @JSONField(name = "evlresult4")
    @JsonProperty("evlresult4")
    private Integer evlresult4;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 质量管理体系(100分,5%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult8")
    @JSONField(name = "evlresult8")
    @JsonProperty("evlresult8")
    private Integer evlresult8;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 评估人
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 评估时间
     */
    @TableField(value = "evldate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "evldate", format = "yyyy-MM-dd")
    @JsonProperty("evldate")
    private Timestamp evldate;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 评价
     */
    @TableField(value = "evlmark")
    @JSONField(name = "evlmark")
    @JsonProperty("evlmark")
    private String evlmark;
    /**
     * 总分数
     */
    @TableField(exist = false)
    @JSONField(name = "evlresult")
    @JsonProperty("evlresult")
    private Integer evlresult;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 评估区间
     */
    @TableField(value = "evlregion")
    @JSONField(name = "evlregion")
    @JsonProperty("evlregion")
    private String evlregion;
    /**
     * 价格(100分,20%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult3")
    @JSONField(name = "evlresult3")
    @JsonProperty("evlresult3")
    private Integer evlresult3;
    /**
     * 质量(100分,25%)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "evlresult2")
    @JSONField(name = "evlresult2")
    @JsonProperty("evlresult2")
    private Integer evlresult2;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 评估人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 评估状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "serviceevlstate")
    @JSONField(name = "serviceevlstate")
    @JsonProperty("serviceevlstate")
    private String serviceevlstate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "servicename")
    @JsonProperty("servicename")
    private String servicename;
    /**
     * 服务商
     */
    @TableField(value = "serviceid")
    @JSONField(name = "serviceid")
    @JsonProperty("serviceid")
    private String serviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService service;



    /**
     * 设置 [服务商评估名称]
     */
    public void setEmserviceevlname(String emserviceevlname) {
        this.emserviceevlname = emserviceevlname;
        this.modify("emserviceevlname", emserviceevlname);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [售后(100分,10%)]
     */
    public void setEvlresult9(Integer evlresult9) {
        this.evlresult9 = evlresult9;
        this.modify("evlresult9", evlresult9);
    }

    /**
     * 设置 [供货能力(100分,10%)]
     */
    public void setEvlresult5(Integer evlresult5) {
        this.evlresult5 = evlresult5;
        this.modify("evlresult5", evlresult5);
    }

    /**
     * 设置 [距离(100分,5%)]
     */
    public void setEvlresult7(Integer evlresult7) {
        this.evlresult7 = evlresult7;
        this.modify("evlresult7", evlresult7);
    }

    /**
     * 设置 [及时性(100分,10%)]
     */
    public void setEvlresult6(Integer evlresult6) {
        this.evlresult6 = evlresult6;
        this.modify("evlresult6", evlresult6);
    }

    /**
     * 设置 [资质(100分,5%)]
     */
    public void setEvlresult1(Integer evlresult1) {
        this.evlresult1 = evlresult1;
        this.modify("evlresult1", evlresult1);
    }

    /**
     * 设置 [评价]
     */
    public void setEvlmark1(String evlmark1) {
        this.evlmark1 = evlmark1;
        this.modify("evlmark1", evlmark1);
    }

    /**
     * 设置 [安全性能(100分,10%)]
     */
    public void setEvlresult4(Integer evlresult4) {
        this.evlresult4 = evlresult4;
        this.modify("evlresult4", evlresult4);
    }

    /**
     * 设置 [质量管理体系(100分,5%)]
     */
    public void setEvlresult8(Integer evlresult8) {
        this.evlresult8 = evlresult8;
        this.modify("evlresult8", evlresult8);
    }

    /**
     * 设置 [评估人]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [评估时间]
     */
    public void setEvldate(Timestamp evldate) {
        this.evldate = evldate;
        this.modify("evldate", evldate);
    }

    /**
     * 格式化日期 [评估时间]
     */
    public String formatEvldate() {
        if (this.evldate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(evldate);
    }
    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [评价]
     */
    public void setEvlmark(String evlmark) {
        this.evlmark = evlmark;
        this.modify("evlmark", evlmark);
    }

    /**
     * 设置 [评估区间]
     */
    public void setEvlregion(String evlregion) {
        this.evlregion = evlregion;
        this.modify("evlregion", evlregion);
    }

    /**
     * 设置 [价格(100分,20%)]
     */
    public void setEvlresult3(Integer evlresult3) {
        this.evlresult3 = evlresult3;
        this.modify("evlresult3", evlresult3);
    }

    /**
     * 设置 [质量(100分,25%)]
     */
    public void setEvlresult2(Integer evlresult2) {
        this.evlresult2 = evlresult2;
        this.modify("evlresult2", evlresult2);
    }

    /**
     * 设置 [评估人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [评估状态]
     */
    public void setServiceevlstate(String serviceevlstate) {
        this.serviceevlstate = serviceevlstate;
        this.modify("serviceevlstate", serviceevlstate);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [服务商]
     */
    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
        this.modify("serviceid", serviceid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emserviceevlid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


