package cn.ibizlab.eam.core.eam_pf.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_pf.domain.PFUnit;
/**
 * 关系型数据实体[PFUnit] 查询条件对象
 */
@Slf4j
@Data
public class PFUnitSearchContext extends QueryWrapperContext<PFUnit> {

	private String n_pfunitname_like;//[计量单位名称]
	public void setN_pfunitname_like(String n_pfunitname_like) {
        this.n_pfunitname_like = n_pfunitname_like;
        if(!ObjectUtils.isEmpty(this.n_pfunitname_like)){
            this.getSearchCond().like("pfunitname", n_pfunitname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pfunitname", query)
            );
		 }
	}
}



