package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWPList;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWPList] 服务对象接口
 */
public interface IEMWPListService extends IService<EMWPList> {

    boolean create(EMWPList et);
    void createBatch(List<EMWPList> list);
    boolean update(EMWPList et);
    void updateBatch(List<EMWPList> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWPList get(String key);
    EMWPList getDraft(EMWPList et);
    boolean checkKey(EMWPList et);
    EMWPList confirm(EMWPList et);
    boolean confirmBatch(List<EMWPList> etList);
    EMWPList fillCosted(EMWPList et);
    boolean fillCostedBatch(List<EMWPList> etList);
    EMWPList formUpdateByAempid(EMWPList et);
    EMWPList genPO(EMWPList et);
    boolean genPOBatch(List<EMWPList> etList);
    EMWPList getREMP(EMWPList et);
    boolean save(EMWPList et);
    void saveBatch(List<EMWPList> list);
    EMWPList submit(EMWPList et);
    Page<EMWPList> searchCancel(EMWPListSearchContext context);
    Page<EMWPList> searchConfimCost(EMWPListSearchContext context);
    Page<EMWPList> searchDefault(EMWPListSearchContext context);
    Page<EMWPList> searchDraft(EMWPListSearchContext context);
    Page<EMWPList> searchIn(EMWPListSearchContext context);
    Page<EMWPList> searchMain6(EMWPListSearchContext context);
    Page<EMWPList> searchMain6_8692(EMWPListSearchContext context);
    Page<EMWPList> searchWaitCost(EMWPListSearchContext context);
    Page<EMWPList> searchWaitPo(EMWPListSearchContext context);
    Page<Map> searchWpStateNum(EMWPListSearchContext context);
    List<EMWPList> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMWPList> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    List<EMWPList> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMWPList> selectByEmserviceid(String emserviceid);
    void removeByEmserviceid(String emserviceid);
    List<EMWPList> selectByWplistcostid(String emwplistcostid);
    void removeByWplistcostid(String emwplistcostid);
    List<EMWPList> selectByDeptid(String pfdeptid);
    void removeByDeptid(String pfdeptid);
    List<EMWPList> selectByAempid(String pfempid);
    void removeByAempid(String pfempid);
    List<EMWPList> selectByApprempid(String pfempid);
    void removeByApprempid(String pfempid);
    List<EMWPList> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMWPList> selectByTeamid(String pfteamid);
    void removeByTeamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWPList> getEmwplistByIds(List<String> ids);
    List<EMWPList> getEmwplistByEntities(List<EMWPList> entities);
}


