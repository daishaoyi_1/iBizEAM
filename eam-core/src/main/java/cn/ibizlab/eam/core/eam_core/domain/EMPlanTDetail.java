package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划模板步骤]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPLANTDETAIL_BASE", resultMap = "EMPlanTDetailResultMap")
public class EMPlanTDetail extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 计划模板步骤标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emplantdetailid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emplantdetailid")
    @JsonProperty("emplantdetailid")
    private String emplantdetailid;
    /**
     * 持续时间(H)
     */
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 停运时间(分)
     */
    @TableField(value = "eqstoplength")
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;
    /**
     * 计划步骤内容
     */
    @TableField(value = "plandetaildesc")
    @JSONField(name = "plandetaildesc")
    @JsonProperty("plandetaildesc")
    private String plandetaildesc;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonid")
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    private String recvpersonid;
    /**
     * 计划模板步骤名称
     */
    @TableField(value = "emplantdetailname")
    @JSONField(name = "emplantdetailname")
    @JsonProperty("emplantdetailname")
    private String emplantdetailname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 排序
     */
    @TableField(value = "orderflag")
    @JSONField(name = "orderflag")
    @JsonProperty("orderflag")
    private Integer orderflag;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 生成工单种类
     */
    @DEField(defaultValue = "INNER")
    @TableField(value = "emwotype")
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    private String emwotype;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonname")
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    private String recvpersonname;
    /**
     * 归档
     */
    @TableField(value = "archive")
    @JSONField(name = "archive")
    @JsonProperty("archive")
    private String archive;
    /**
     * 详细内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 计划模板
     */
    @TableField(exist = false)
    @JSONField(name = "plantemplname")
    @JsonProperty("plantemplname")
    private String plantemplname;
    /**
     * 计划模板
     */
    @TableField(value = "plantemplid")
    @JSONField(name = "plantemplid")
    @JsonProperty("plantemplid")
    private String plantemplid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl plantempl;



    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [停运时间(分)]
     */
    public void setEqstoplength(Double eqstoplength) {
        this.eqstoplength = eqstoplength;
        this.modify("eqstoplength", eqstoplength);
    }

    /**
     * 设置 [计划步骤内容]
     */
    public void setPlandetaildesc(String plandetaildesc) {
        this.plandetaildesc = plandetaildesc;
        this.modify("plandetaildesc", plandetaildesc);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [接收人]
     */
    public void setRecvpersonid(String recvpersonid) {
        this.recvpersonid = recvpersonid;
        this.modify("recvpersonid", recvpersonid);
    }

    /**
     * 设置 [计划模板步骤名称]
     */
    public void setEmplantdetailname(String emplantdetailname) {
        this.emplantdetailname = emplantdetailname;
        this.modify("emplantdetailname", emplantdetailname);
    }

    /**
     * 设置 [排序]
     */
    public void setOrderflag(Integer orderflag) {
        this.orderflag = orderflag;
        this.modify("orderflag", orderflag);
    }

    /**
     * 设置 [生成工单种类]
     */
    public void setEmwotype(String emwotype) {
        this.emwotype = emwotype;
        this.modify("emwotype", emwotype);
    }

    /**
     * 设置 [接收人]
     */
    public void setRecvpersonname(String recvpersonname) {
        this.recvpersonname = recvpersonname;
        this.modify("recvpersonname", recvpersonname);
    }

    /**
     * 设置 [归档]
     */
    public void setArchive(String archive) {
        this.archive = archive;
        this.modify("archive", archive);
    }

    /**
     * 设置 [详细内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [计划模板]
     */
    public void setPlantemplid(String plantemplid) {
        this.plantemplid = plantemplid;
        this.modify("plantemplid", plantemplid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emplantdetailid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


