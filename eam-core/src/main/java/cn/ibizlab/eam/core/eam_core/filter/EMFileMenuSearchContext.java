package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMFileMenu;
/**
 * 关系型数据实体[EMFileMenu] 查询条件对象
 */
@Slf4j
@Data
public class EMFileMenuSearchContext extends QueryWrapperContext<EMFileMenu> {

	private String n_emfilemenuname_like;//[文件夹目录名称]
	public void setN_emfilemenuname_like(String n_emfilemenuname_like) {
        this.n_emfilemenuname_like = n_emfilemenuname_like;
        if(!ObjectUtils.isEmpty(this.n_emfilemenuname_like)){
            this.getSearchCond().like("emfilemenuname", n_emfilemenuname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emfilemenuname", query)
            );
		 }
	}
}



