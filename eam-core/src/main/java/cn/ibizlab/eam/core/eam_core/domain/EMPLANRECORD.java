package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[触发记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "T_EMPLANRECORD", resultMap = "EMPLANRECORDResultMap")
public class EMPLANRECORD extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 触发记录标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emplanrecordid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emplanrecordid")
    @JsonProperty("emplanrecordid")
    private String emplanrecordid;
    /**
     * 触发记录名称
     */
    @TableField(value = "emplanrecordname")
    @JSONField(name = "emplanrecordname")
    @JsonProperty("emplanrecordname")
    private String emplanrecordname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 是否触发
     */
    @TableField(value = "istrigger")
    @JSONField(name = "istrigger")
    @JsonProperty("istrigger")
    private Integer istrigger;
    /**
     * 计划编号
     */
    @TableField(value = "emplanid")
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    private String emplanid;
    /**
     * 计划名称
     */
    @TableField(exist = false)
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    private String emplanname;
    /**
     * 原因
     */
    @TableField(value = "triggerca")
    @JSONField(name = "triggerca")
    @JsonProperty("triggerca")
    private String triggerca;
    /**
     * 结果
     */
    @TableField(value = "triggerre")
    @JSONField(name = "triggerre")
    @JsonProperty("triggerre")
    private String triggerre;
    /**
     * 备注
     */
    @TableField(value = "remark")
    @JSONField(name = "remark")
    @JsonProperty("remark")
    private String remark;
    /**
     * 触发时间
     */
    @TableField(value = "triggerdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "triggerdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("triggerdate")
    private Timestamp triggerdate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPlan emplan;



    /**
     * 设置 [触发记录名称]
     */
    public void setEmplanrecordname(String emplanrecordname) {
        this.emplanrecordname = emplanrecordname;
        this.modify("emplanrecordname", emplanrecordname);
    }

    /**
     * 设置 [是否触发]
     */
    public void setIstrigger(Integer istrigger) {
        this.istrigger = istrigger;
        this.modify("istrigger", istrigger);
    }

    /**
     * 设置 [计划编号]
     */
    public void setEmplanid(String emplanid) {
        this.emplanid = emplanid;
        this.modify("emplanid", emplanid);
    }

    /**
     * 设置 [原因]
     */
    public void setTriggerca(String triggerca) {
        this.triggerca = triggerca;
        this.modify("triggerca", triggerca);
    }

    /**
     * 设置 [结果]
     */
    public void setTriggerre(String triggerre) {
        this.triggerre = triggerre;
        this.modify("triggerre", triggerre);
    }

    /**
     * 设置 [备注]
     */
    public void setRemark(String remark) {
        this.remark = remark;
        this.modify("remark", remark);
    }

    /**
     * 设置 [触发时间]
     */
    public void setTriggerdate(Timestamp triggerdate) {
        this.triggerdate = triggerdate;
        this.modify("triggerdate", triggerdate);
    }

    /**
     * 格式化日期 [触发时间]
     */
    public String formatTriggerdate() {
        if (this.triggerdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(triggerdate);
    }

    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emplanrecordid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


