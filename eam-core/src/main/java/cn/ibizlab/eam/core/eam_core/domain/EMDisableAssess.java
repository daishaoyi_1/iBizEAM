package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备停用考核]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMDISABLEASSESS_BASE", resultMap = "EMDisableAssessResultMap")
public class EMDisableAssess extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 设备停用考核名称
     */
    @TableField(value = "emdisableassessname")
    @JSONField(name = "emdisableassessname")
    @JsonProperty("emdisableassessname")
    private String emdisableassessname;
    /**
     * 考核名称
     */
    @TableField(value = "assessnsme")
    @JSONField(name = "assessnsme")
    @JsonProperty("assessnsme")
    private String assessnsme;
    /**
     * 设备停用考核标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emdisableassessid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emdisableassessid")
    @JsonProperty("emdisableassessid")
    private String emdisableassessid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 制表日期
     */
    @TableField(value = "tabledate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "tabledate", format = "yyyy-MM-dd")
    @JsonProperty("tabledate")
    private Timestamp tabledate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 考核金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private String amount;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    private String pfteamname;
    /**
     * 班组
     */
    @TableField(value = "pfteamid")
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    private String pfteamid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam pfteam;



    /**
     * 设置 [设备停用考核名称]
     */
    public void setEmdisableassessname(String emdisableassessname) {
        this.emdisableassessname = emdisableassessname;
        this.modify("emdisableassessname", emdisableassessname);
    }

    /**
     * 设置 [考核名称]
     */
    public void setAssessnsme(String assessnsme) {
        this.assessnsme = assessnsme;
        this.modify("assessnsme", assessnsme);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [制表日期]
     */
    public void setTabledate(Timestamp tabledate) {
        this.tabledate = tabledate;
        this.modify("tabledate", tabledate);
    }

    /**
     * 格式化日期 [制表日期]
     */
    public String formatTabledate() {
        if (this.tabledate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(tabledate);
    }
    /**
     * 设置 [考核金额]
     */
    public void setAmount(String amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [班组]
     */
    public void setPfteamid(String pfteamid) {
        this.pfteamid = pfteamid;
        this.modify("pfteamid", pfteamid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emdisableassessid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


