package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMRiggingH;
import cn.ibizlab.eam.core.eam_core.filter.EMRiggingHSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMRiggingHMapper extends BaseMapper<EMRiggingH> {

    Page<EMRiggingH> searchDefault(IPage page, @Param("srf") EMRiggingHSearchContext context, @Param("ew") Wrapper<EMRiggingH> wrapper);
    @Override
    EMRiggingH selectById(Serializable id);
    @Override
    int insert(EMRiggingH entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMRiggingH entity);
    @Override
    int update(@Param(Constants.ENTITY) EMRiggingH entity, @Param("ew") Wrapper<EMRiggingH> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMRiggingH> selectByEmitempuseid(@Param("emitempuseid") Serializable emitempuseid);

    List<EMRiggingH> selectByEmriggingid(@Param("emriggingid") Serializable emriggingid);

}
