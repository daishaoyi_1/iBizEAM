package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[资源引用对象]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMRESREFOBJ_BASE", resultMap = "EMResRefObjResultMap")
public class EMResRefObj extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 起始时间
     */
    @TableField(value = "regionbegindate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionbegindate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    private Timestamp regionbegindate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 资源引用对象名称
     */
    @TableField(value = "emresrefobjname")
    @JSONField(name = "emresrefobjname")
    @JsonProperty("emresrefobjname")
    private String emresrefobjname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 引用对象信息
     */
    @TableField(exist = false)
    @JSONField(name = "resrefobjinfo")
    @JsonProperty("resrefobjinfo")
    private String resrefobjinfo;
    /**
     * 资源引用对象标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emresrefobjid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emresrefobjid")
    @JsonProperty("emresrefobjid")
    private String emresrefobjid;
    /**
     * 引用对象类型
     */
    @TableField(value = "emresrefobjtype")
    @JSONField(name = "emresrefobjtype")
    @JsonProperty("emresrefobjtype")
    private String emresrefobjtype;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(defaultValue = "SYSTEM", preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 上级引用对象
     */
    @TableField(exist = false)
    @JSONField(name = "resrefobjpname")
    @JsonProperty("resrefobjpname")
    private String resrefobjpname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 上级引用对象
     */
    @TableField(value = "resrefobjpid")
    @JSONField(name = "resrefobjpid")
    @JsonProperty("resrefobjpid")
    private String resrefobjpid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMResRefObj resrefobjp;



    /**
     * 设置 [起始时间]
     */
    public void setRegionbegindate(Timestamp regionbegindate) {
        this.regionbegindate = regionbegindate;
        this.modify("regionbegindate", regionbegindate);
    }

    /**
     * 格式化日期 [起始时间]
     */
    public String formatRegionbegindate() {
        if (this.regionbegindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionbegindate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [资源引用对象名称]
     */
    public void setEmresrefobjname(String emresrefobjname) {
        this.emresrefobjname = emresrefobjname;
        this.modify("emresrefobjname", emresrefobjname);
    }

    /**
     * 设置 [引用对象类型]
     */
    public void setEmresrefobjtype(String emresrefobjtype) {
        this.emresrefobjtype = emresrefobjtype;
        this.modify("emresrefobjtype", emresrefobjtype);
    }

    /**
     * 设置 [上级引用对象]
     */
    public void setResrefobjpid(String resrefobjpid) {
        this.resrefobjpid = resrefobjpid;
        this.modify("resrefobjpid", resrefobjpid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emresrefobjid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


