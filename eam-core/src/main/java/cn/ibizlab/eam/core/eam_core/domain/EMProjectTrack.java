package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划修理项目跟踪表]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPROJECTTRACK_BASE", resultMap = "EMProjectTrackResultMap")
public class EMProjectTrack extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 负责人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 跟踪日期
     */
    @TableField(value = "trackdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "trackdate", format = "yyyy-MM-dd")
    @JsonProperty("trackdate")
    private Timestamp trackdate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 计划修理项目跟踪表标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emprojecttrackid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emprojecttrackid")
    @JsonProperty("emprojecttrackid")
    private String emprojecttrackid;
    /**
     * 负责人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 价格
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 跟踪信息
     */
    @TableField(value = "trackinfo")
    @JSONField(name = "trackinfo")
    @JsonProperty("trackinfo")
    private String trackinfo;
    /**
     * 计划修理项目跟踪表名称
     */
    @DEField(defaultValue = "计划修理项目跟踪表")
    @TableField(value = "emprojecttrackname")
    @JSONField(name = "emprojecttrackname")
    @JsonProperty("emprojecttrackname")
    private String emprojecttrackname;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    private String emservicename;
    /**
     * 项目内容
     */
    @TableField(exist = false)
    @JSONField(name = "empurplanname")
    @JsonProperty("empurplanname")
    private String empurplanname;
    /**
     * 服务商
     */
    @TableField(value = "emserviceid")
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    private String emserviceid;
    /**
     * 项目内容
     */
    @TableField(value = "empurplanid")
    @JSONField(name = "empurplanid")
    @JsonProperty("empurplanid")
    private String empurplanid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPurPlan empurplan;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService emservice;



    /**
     * 设置 [负责人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [跟踪日期]
     */
    public void setTrackdate(Timestamp trackdate) {
        this.trackdate = trackdate;
        this.modify("trackdate", trackdate);
    }

    /**
     * 格式化日期 [跟踪日期]
     */
    public String formatTrackdate() {
        if (this.trackdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(trackdate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [负责人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [价格]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [跟踪信息]
     */
    public void setTrackinfo(String trackinfo) {
        this.trackinfo = trackinfo;
        this.modify("trackinfo", trackinfo);
    }

    /**
     * 设置 [计划修理项目跟踪表名称]
     */
    public void setEmprojecttrackname(String emprojecttrackname) {
        this.emprojecttrackname = emprojecttrackname;
        this.modify("emprojecttrackname", emprojecttrackname);
    }

    /**
     * 设置 [服务商]
     */
    public void setEmserviceid(String emserviceid) {
        this.emserviceid = emserviceid;
        this.modify("emserviceid", emserviceid);
    }

    /**
     * 设置 [项目内容]
     */
    public void setEmpurplanid(String empurplanid) {
        this.empurplanid = empurplanid;
        this.modify("empurplanid", empurplanid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emprojecttrackid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


