package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListCostSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMWPListCostService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMWPListCostMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[询价单] 服务对象接口实现
 */
@Slf4j
@Service("EMWPListCostServiceImpl")
public class EMWPListCostServiceImpl extends ServiceImpl<EMWPListCostMapper, EMWPListCost> implements IEMWPListCostService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFUnitService pfunitService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMWPListCost et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwplistcostid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMWPListCost> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMWPListCost et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emwplistcostid", et.getEmwplistcostid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwplistcostid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMWPListCost> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMWPListCost get(String key) {
        EMWPListCost et = getById(key);
        if(et == null){
            et = new EMWPListCost();
            et.setEmwplistcostid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMWPListCost getDraft(EMWPListCost et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMWPListCost et) {
        return (!ObjectUtils.isEmpty(et.getEmwplistcostid())) && (!Objects.isNull(this.getById(et.getEmwplistcostid())));
    }
    @Override
    @Transactional
    public EMWPListCost confirm(EMWPListCost et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean confirmBatch(List<EMWPListCost> etList) {
        for(EMWPListCost et : etList) {
            confirm(et);
        }
        return true;
    }

    @Override
    @Transactional
    public EMWPListCost fillItem(EMWPListCost et) {
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMWPListCost et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMWPListCost et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMWPListCost> list) {
        list.forEach(item->fillParentData(item));
        List<EMWPListCost> create = new ArrayList<>();
        List<EMWPListCost> update = new ArrayList<>();
        for (EMWPListCost et : list) {
            if (ObjectUtils.isEmpty(et.getEmwplistcostid()) || ObjectUtils.isEmpty(getById(et.getEmwplistcostid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMWPListCost> list) {
        list.forEach(item->fillParentData(item));
        List<EMWPListCost> create = new ArrayList<>();
        List<EMWPListCost> update = new ArrayList<>();
        for (EMWPListCost et : list) {
            if (ObjectUtils.isEmpty(et.getEmwplistcostid()) || ObjectUtils.isEmpty(getById(et.getEmwplistcostid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMWPListCost> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }
    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMWPListCost>().eq("itemid",emitemid));
    }

	@Override
    public List<EMWPListCost> selectByLabserviceid(String emserviceid) {
        return baseMapper.selectByLabserviceid(emserviceid);
    }
    @Override
    public void removeByLabserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMWPListCost>().eq("labserviceid",emserviceid));
    }

	@Override
    public List<EMWPListCost> selectByWplistid(String emwplistid) {
        return baseMapper.selectByWplistid(emwplistid);
    }
    @Override
    public void removeByWplistid(String emwplistid) {
        this.remove(new QueryWrapper<EMWPListCost>().eq("wplistid",emwplistid));
    }

	@Override
    public List<EMWPListCost> selectByUnitid(String pfunitid) {
        return baseMapper.selectByUnitid(pfunitid);
    }
    @Override
    public void removeByUnitid(String pfunitid) {
        this.remove(new QueryWrapper<EMWPListCost>().eq("unitid",pfunitid));
    }


    /**
     * 查询集合 CostByItem
     */
    @Override
    public Page<EMWPListCost> searchCostByItem(EMWPListCostSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPListCost> pages=baseMapper.searchCostByItem(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPListCost>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMWPListCost> searchDefault(EMWPListCostSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPListCost> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPListCost>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMWPListCost et){
        //实体关系[DER1N_EMWPLISTCOST_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setSunitid(item.getUnitid());
            et.setSunitname(item.getUnitname());
            et.setItemname(item.getEmitemname());
            et.setAvgprice(item.getPrice());
        }
        //实体关系[DER1N_EMWPLISTCOST_EMSERVICE_LABSERVICEID]
        if(!ObjectUtils.isEmpty(et.getLabserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService labservice=et.getLabservice();
            if(ObjectUtils.isEmpty(labservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getLabserviceid());
                et.setLabservice(majorEntity);
                labservice=majorEntity;
            }
            et.setLabservicename(labservice.getEmservicename());
        }
        //实体关系[DER1N_EMWPLISTCOST_PFUNIT_UNITID]
        if(!ObjectUtils.isEmpty(et.getUnitid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFUnit unit=et.getUnit();
            if(ObjectUtils.isEmpty(unit)){
                cn.ibizlab.eam.core.eam_pf.domain.PFUnit majorEntity=pfunitService.get(et.getUnitid());
                et.setUnit(majorEntity);
                unit=majorEntity;
            }
            et.setUnitname(unit.getPfunitname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMWPListCost> getEmwplistcostByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMWPListCost> getEmwplistcostByEntities(List<EMWPListCost> entities) {
        List ids =new ArrayList();
        for(EMWPListCost entity : entities){
            Serializable id=entity.getEmwplistcostid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMWPListCostService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



