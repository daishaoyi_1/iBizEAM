package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFUnit;
import cn.ibizlab.eam.core.eam_pf.filter.PFUnitSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFUnitService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFUnitMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计量单位] 服务对象接口实现
 */
@Slf4j
@Service("PFUnitServiceImpl")
public class PFUnitServiceImpl extends ServiceImpl<PFUnitMapper, PFUnit> implements IPFUnitService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssetService emassetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPODetailService empodetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPurPlanService empurplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListCostService emwplistcostService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFUnit et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfunitid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFUnit> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFUnit et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("pfunitid", et.getPfunitid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfunitid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFUnit> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFUnit get(String key) {
        PFUnit et = getById(key);
        if(et == null){
            et = new PFUnit();
            et.setPfunitid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PFUnit getDraft(PFUnit et) {
        return et;
    }

    @Override
    public boolean checkKey(PFUnit et) {
        return (!ObjectUtils.isEmpty(et.getPfunitid())) && (!Objects.isNull(this.getById(et.getPfunitid())));
    }
    @Override
    @Transactional
    public boolean save(PFUnit et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFUnit et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFUnit> list) {
        List<PFUnit> create = new ArrayList<>();
        List<PFUnit> update = new ArrayList<>();
        for (PFUnit et : list) {
            if (ObjectUtils.isEmpty(et.getPfunitid()) || ObjectUtils.isEmpty(getById(et.getPfunitid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFUnit> list) {
        List<PFUnit> create = new ArrayList<>();
        List<PFUnit> update = new ArrayList<>();
        for (PFUnit et : list) {
            if (ObjectUtils.isEmpty(et.getPfunitid()) || ObjectUtils.isEmpty(getById(et.getPfunitid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFUnit> searchDefault(PFUnitSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFUnit> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFUnit>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFUnit> getPfunitByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFUnit> getPfunitByEntities(List<PFUnit> entities) {
        List ids =new ArrayList();
        for(PFUnit entity : entities){
            Serializable id=entity.getPfunitid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPFUnitService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



