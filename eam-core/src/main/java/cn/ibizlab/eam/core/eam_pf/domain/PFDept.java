package cn.ibizlab.eam.core.eam_pf.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[部门]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_PFDEPT_BASE", resultMap = "PFDeptResultMap")
public class PFDept extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门代码
     */
    @TableField(value = "deptcode")
    @JSONField(name = "deptcode")
    @JsonProperty("deptcode")
    private String deptcode;
    /**
     * 主管
     */
    @TableField(value = "mgrempname")
    @JSONField(name = "mgrempname")
    @JsonProperty("mgrempname")
    private String mgrempname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 部门信息
     */
    @TableField(exist = false)
    @JSONField(name = "deptinfo")
    @JsonProperty("deptinfo")
    private String deptinfo;
    /**
     * 职能
     */
    @TableField(value = "deptfn")
    @JSONField(name = "deptfn")
    @JsonProperty("deptfn")
    private String deptfn;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 主部门编码
     */
    @TableField(value = "maindeptcode")
    @JSONField(name = "maindeptcode")
    @JsonProperty("maindeptcode")
    private String maindeptcode;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 上级部门
     */
    @TableField(value = "deptpid")
    @JSONField(name = "deptpid")
    @JsonProperty("deptpid")
    private String deptpid;
    /**
     * 部门标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "pfdeptid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "pfdeptid")
    @JsonProperty("pfdeptid")
    private String pfdeptid;
    /**
     * 主管
     */
    @TableField(value = "mgrempid")
    @JSONField(name = "mgrempid")
    @JsonProperty("mgrempid")
    private String mgrempid;
    /**
     * 部门名称
     */
    @TableField(value = "pfdeptname")
    @JSONField(name = "pfdeptname")
    @JsonProperty("pfdeptname")
    private String pfdeptname;
    /**
     * 统计归口部门
     */
    @TableField(value = "sdept")
    @JSONField(name = "sdept")
    @JsonProperty("sdept")
    private Integer sdept;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 上级部门
     */
    @TableField(value = "deptpname")
    @JSONField(name = "deptpname")
    @JsonProperty("deptpname")
    private String deptpname;



    /**
     * 设置 [部门代码]
     */
    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
        this.modify("deptcode", deptcode);
    }

    /**
     * 设置 [主管]
     */
    public void setMgrempname(String mgrempname) {
        this.mgrempname = mgrempname;
        this.modify("mgrempname", mgrempname);
    }

    /**
     * 设置 [职能]
     */
    public void setDeptfn(String deptfn) {
        this.deptfn = deptfn;
        this.modify("deptfn", deptfn);
    }

    /**
     * 设置 [主部门编码]
     */
    public void setMaindeptcode(String maindeptcode) {
        this.maindeptcode = maindeptcode;
        this.modify("maindeptcode", maindeptcode);
    }

    /**
     * 设置 [上级部门]
     */
    public void setDeptpid(String deptpid) {
        this.deptpid = deptpid;
        this.modify("deptpid", deptpid);
    }

    /**
     * 设置 [主管]
     */
    public void setMgrempid(String mgrempid) {
        this.mgrempid = mgrempid;
        this.modify("mgrempid", mgrempid);
    }

    /**
     * 设置 [部门名称]
     */
    public void setPfdeptname(String pfdeptname) {
        this.pfdeptname = pfdeptname;
        this.modify("pfdeptname", pfdeptname);
    }

    /**
     * 设置 [统计归口部门]
     */
    public void setSdept(Integer sdept) {
        this.sdept = sdept;
        this.modify("sdept", sdept);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [上级部门]
     */
    public void setDeptpname(String deptpname) {
        this.deptpname = deptpname;
        this.modify("deptpname", deptpname);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("pfdeptid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


