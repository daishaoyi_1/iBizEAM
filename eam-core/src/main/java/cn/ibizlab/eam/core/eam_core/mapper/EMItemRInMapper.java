package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMItemRIn;
import cn.ibizlab.eam.core.eam_core.filter.EMItemRInSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMItemRInMapper extends BaseMapper<EMItemRIn> {

    Page<EMItemRIn> searchDefault(IPage page, @Param("srf") EMItemRInSearchContext context, @Param("ew") Wrapper<EMItemRIn> wrapper);
    Page<EMItemRIn> searchPutIn(IPage page, @Param("srf") EMItemRInSearchContext context, @Param("ew") Wrapper<EMItemRIn> wrapper);
    Page<EMItemRIn> searchWaitIn(IPage page, @Param("srf") EMItemRInSearchContext context, @Param("ew") Wrapper<EMItemRIn> wrapper);
    @Override
    EMItemRIn selectById(Serializable id);
    @Override
    int insert(EMItemRIn entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMItemRIn entity);
    @Override
    int update(@Param(Constants.ENTITY) EMItemRIn entity, @Param("ew") Wrapper<EMItemRIn> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMItemRIn> selectByItemid(@Param("emitemid") Serializable emitemid);

    List<EMItemRIn> selectByPodetailid(@Param("empodetailid") Serializable empodetailid);

    List<EMItemRIn> selectByEmserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMItemRIn> selectByStorepartid(@Param("emstorepartid") Serializable emstorepartid);

    List<EMItemRIn> selectByStoreid(@Param("emstoreid") Serializable emstoreid);

    List<EMItemRIn> selectByEmpid(@Param("pfempid") Serializable pfempid);

    List<EMItemRIn> selectBySempid(@Param("pfempid") Serializable pfempid);

}
