package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[工索具日报]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEIGSJRB_BASE", resultMap = "EMEIGSJRBResultMap")
public class EMEIGSJRB extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工索具日报名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emeigsjrbname")
    @JSONField(name = "emeigsjrbname")
    @JsonProperty("emeigsjrbname")
    private String emeigsjrbname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 工索具日报标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeigsjrbid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeigsjrbid")
    @JsonProperty("emeigsjrbid")
    private String emeigsjrbid;
    /**
     * 领用日期
     */
    @TableField(value = "ldate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "ldate", format = "yyyy-MM-dd")
    @JsonProperty("ldate")
    private Timestamp ldate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 报废数量
     */
    @TableField(value = "discnt")
    @JSONField(name = "discnt")
    @JsonProperty("discnt")
    private Double discnt;
    /**
     * 上期存量
     */
    @DEField(defaultValue = "0")
    @TableField(value = "laststockcnt")
    @JSONField(name = "laststockcnt")
    @JsonProperty("laststockcnt")
    private Double laststockcnt;
    /**
     * 领用次数
     */
    @TableField(value = "lcnt")
    @JSONField(name = "lcnt")
    @JsonProperty("lcnt")
    private Integer lcnt;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 日期
     */
    @TableField(value = "begindate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "begindate", format = "yyyy-MM-dd")
    @JsonProperty("begindate")
    private Timestamp begindate;
    /**
     * 归还日期
     */
    @TableField(value = "hdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "hdate", format = "yyyy-MM-dd")
    @JsonProperty("hdate")
    private Timestamp hdate;
    /**
     * 结束日期
     */
    @TableField(value = "enddate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "enddate", format = "yyyy-MM-dd")
    @JsonProperty("enddate")
    private Timestamp enddate;
    /**
     * 外委次数
     */
    @TableField(value = "wcnt")
    @JSONField(name = "wcnt")
    @JsonProperty("wcnt")
    private Integer wcnt;
    /**
     * 本期存量
     */
    @DEField(defaultValue = "0")
    @TableField(value = "stockcnt")
    @JSONField(name = "stockcnt")
    @JsonProperty("stockcnt")
    private Double stockcnt;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 报废原因
     */
    @TableField(value = "gsjrbdesc")
    @JSONField(name = "gsjrbdesc")
    @JsonProperty("gsjrbdesc")
    private String gsjrbdesc;
    /**
     * 保养次数
     */
    @TableField(value = "bcnt")
    @JSONField(name = "bcnt")
    @JsonProperty("bcnt")
    private Integer bcnt;
    /**
     * 工索具名称
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 型号
     */
    @TableField(exist = false)
    @JSONField(name = "modelcode")
    @JsonProperty("modelcode")
    private String modelcode;
    /**
     * 工索具名称
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;



    /**
     * 设置 [工索具日报名称]
     */
    public void setEmeigsjrbname(String emeigsjrbname) {
        this.emeigsjrbname = emeigsjrbname;
        this.modify("emeigsjrbname", emeigsjrbname);
    }

    /**
     * 设置 [领用日期]
     */
    public void setLdate(Timestamp ldate) {
        this.ldate = ldate;
        this.modify("ldate", ldate);
    }

    /**
     * 格式化日期 [领用日期]
     */
    public String formatLdate() {
        if (this.ldate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(ldate);
    }
    /**
     * 设置 [报废数量]
     */
    public void setDiscnt(Double discnt) {
        this.discnt = discnt;
        this.modify("discnt", discnt);
    }

    /**
     * 设置 [上期存量]
     */
    public void setLaststockcnt(Double laststockcnt) {
        this.laststockcnt = laststockcnt;
        this.modify("laststockcnt", laststockcnt);
    }

    /**
     * 设置 [领用次数]
     */
    public void setLcnt(Integer lcnt) {
        this.lcnt = lcnt;
        this.modify("lcnt", lcnt);
    }

    /**
     * 设置 [日期]
     */
    public void setBegindate(Timestamp begindate) {
        this.begindate = begindate;
        this.modify("begindate", begindate);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatBegindate() {
        if (this.begindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(begindate);
    }
    /**
     * 设置 [归还日期]
     */
    public void setHdate(Timestamp hdate) {
        this.hdate = hdate;
        this.modify("hdate", hdate);
    }

    /**
     * 格式化日期 [归还日期]
     */
    public String formatHdate() {
        if (this.hdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(hdate);
    }
    /**
     * 设置 [结束日期]
     */
    public void setEnddate(Timestamp enddate) {
        this.enddate = enddate;
        this.modify("enddate", enddate);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatEnddate() {
        if (this.enddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(enddate);
    }
    /**
     * 设置 [外委次数]
     */
    public void setWcnt(Integer wcnt) {
        this.wcnt = wcnt;
        this.modify("wcnt", wcnt);
    }

    /**
     * 设置 [本期存量]
     */
    public void setStockcnt(Double stockcnt) {
        this.stockcnt = stockcnt;
        this.modify("stockcnt", stockcnt);
    }

    /**
     * 设置 [报废原因]
     */
    public void setGsjrbdesc(String gsjrbdesc) {
        this.gsjrbdesc = gsjrbdesc;
        this.modify("gsjrbdesc", gsjrbdesc);
    }

    /**
     * 设置 [保养次数]
     */
    public void setBcnt(Integer bcnt) {
        this.bcnt = bcnt;
        this.modify("bcnt", bcnt);
    }

    /**
     * 设置 [工索具名称]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeigsjrbid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


