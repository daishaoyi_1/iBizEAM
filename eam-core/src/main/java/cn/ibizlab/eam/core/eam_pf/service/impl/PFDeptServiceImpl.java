package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFDept;
import cn.ibizlab.eam.core.eam_pf.filter.PFDeptSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFDeptService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFDeptMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[部门] 服务对象接口实现
 */
@Slf4j
@Service("PFDeptServiceImpl")
public class PFDeptServiceImpl extends ServiceImpl<PFDeptMapper, PFDept> implements IPFDeptService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwoDpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwoEnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwoOscService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFDept et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfdeptid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFDept> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFDept et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("pfdeptid", et.getPfdeptid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfdeptid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFDept> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFDept get(String key) {
        PFDept et = getById(key);
        if(et == null){
            et = new PFDept();
            et.setPfdeptid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PFDept getDraft(PFDept et) {
        return et;
    }

    @Override
    public boolean checkKey(PFDept et) {
        return (!ObjectUtils.isEmpty(et.getPfdeptid())) && (!Objects.isNull(this.getById(et.getPfdeptid())));
    }
    @Override
    @Transactional
    public boolean save(PFDept et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFDept et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFDept> list) {
        List<PFDept> create = new ArrayList<>();
        List<PFDept> update = new ArrayList<>();
        for (PFDept et : list) {
            if (ObjectUtils.isEmpty(et.getPfdeptid()) || ObjectUtils.isEmpty(getById(et.getPfdeptid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFDept> list) {
        List<PFDept> create = new ArrayList<>();
        List<PFDept> update = new ArrayList<>();
        for (PFDept et : list) {
            if (ObjectUtils.isEmpty(et.getPfdeptid()) || ObjectUtils.isEmpty(getById(et.getPfdeptid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFDept> searchDefault(PFDeptSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFDept> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFDept>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 顶级部门
     */
    @Override
    public Page<PFDept> searchTopDept(PFDeptSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFDept> pages=baseMapper.searchTopDept(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFDept>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFDept> getPfdeptByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFDept> getPfdeptByEntities(List<PFDept> entities) {
        List ids =new ArrayList();
        for(PFDept entity : entities){
            Serializable id=entity.getPfdeptid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPFDeptService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



