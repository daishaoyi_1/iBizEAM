package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划时刻设置]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "T_PLANSCHEDULE", resultMap = "PLANSCHEDULEResultMap")
public class PLANSCHEDULE extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 计划时刻设置标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "planscheduleid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "planscheduleid")
    @JsonProperty("planscheduleid")
    private String planscheduleid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 计划时刻设置名称
     */
    @TableField(value = "planschedulename")
    @JSONField(name = "planschedulename")
    @JsonProperty("planschedulename")
    private String planschedulename;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 时刻类型
     */
    @TableField(value = "scheduletype")
    @JSONField(name = "scheduletype")
    @JsonProperty("scheduletype")
    private String scheduletype;
    /**
     * 循环开始时间
     */
    @TableField(value = "cyclestarttime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "cyclestarttime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cyclestarttime")
    private Timestamp cyclestarttime;
    /**
     * 循环结束时间
     */
    @TableField(value = "cycleendtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "cycleendtime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cycleendtime")
    private Timestamp cycleendtime;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 间隔时间
     */
    @TableField(value = "intervalminute")
    @JSONField(name = "intervalminute")
    @JsonProperty("intervalminute")
    private Integer intervalminute;
    /**
     * 运行日期
     */
    @TableField(value = "rundate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "rundate", format = "yyyy-MM-dd")
    @JsonProperty("rundate")
    private Timestamp rundate;
    /**
     * 执行时间
     */
    @TableField(value = "runtime")
    @JsonFormat(pattern = "HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "runtime", format = "HH:mm:ss")
    @JsonProperty("runtime")
    private Timestamp runtime;
    /**
     * 时刻参数
     */
    @TableField(value = "scheduleparam")
    @JSONField(name = "scheduleparam")
    @JsonProperty("scheduleparam")
    private String scheduleparam;
    /**
     * 时刻参数
     */
    @TableField(value = "scheduleparam2")
    @JSONField(name = "scheduleparam2")
    @JsonProperty("scheduleparam2")
    private String scheduleparam2;
    /**
     * 时刻参数
     */
    @TableField(value = "scheduleparam3")
    @JSONField(name = "scheduleparam3")
    @JsonProperty("scheduleparam3")
    private String scheduleparam3;
    /**
     * 时刻参数
     */
    @TableField(value = "scheduleparam4")
    @JSONField(name = "scheduleparam4")
    @JsonProperty("scheduleparam4")
    private String scheduleparam4;
    /**
     * 时刻设置状态
     */
    @TableField(value = "schedulestate")
    @JSONField(name = "schedulestate")
    @JsonProperty("schedulestate")
    private String schedulestate;
    /**
     * 计划编号
     */
    @TableField(value = "emplanid")
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    private String emplanid;
    /**
     * 计划名称
     */
    @TableField(exist = false)
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    private String emplanname;
    /**
     * 持续时间
     */
    @TableField(value = "lastminute")
    @JSONField(name = "lastminute")
    @JsonProperty("lastminute")
    private Integer lastminute;
    /**
     * 定时任务
     */
    @TableField(value = "taskid")
    @JSONField(name = "taskid")
    @JsonProperty("taskid")
    private String taskid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPlan emplan;



    /**
     * 设置 [计划时刻设置名称]
     */
    public void setPlanschedulename(String planschedulename) {
        this.planschedulename = planschedulename;
        this.modify("planschedulename", planschedulename);
    }

    /**
     * 设置 [时刻类型]
     */
    public void setScheduletype(String scheduletype) {
        this.scheduletype = scheduletype;
        this.modify("scheduletype", scheduletype);
    }

    /**
     * 设置 [循环开始时间]
     */
    public void setCyclestarttime(Timestamp cyclestarttime) {
        this.cyclestarttime = cyclestarttime;
        this.modify("cyclestarttime", cyclestarttime);
    }

    /**
     * 格式化日期 [循环开始时间]
     */
    public String formatCyclestarttime() {
        if (this.cyclestarttime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(cyclestarttime);
    }
    /**
     * 设置 [循环结束时间]
     */
    public void setCycleendtime(Timestamp cycleendtime) {
        this.cycleendtime = cycleendtime;
        this.modify("cycleendtime", cycleendtime);
    }

    /**
     * 格式化日期 [循环结束时间]
     */
    public String formatCycleendtime() {
        if (this.cycleendtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(cycleendtime);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [间隔时间]
     */
    public void setIntervalminute(Integer intervalminute) {
        this.intervalminute = intervalminute;
        this.modify("intervalminute", intervalminute);
    }

    /**
     * 设置 [运行日期]
     */
    public void setRundate(Timestamp rundate) {
        this.rundate = rundate;
        this.modify("rundate", rundate);
    }

    /**
     * 格式化日期 [运行日期]
     */
    public String formatRundate() {
        if (this.rundate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(rundate);
    }
    /**
     * 设置 [执行时间]
     */
    public void setRuntime(Timestamp runtime) {
        this.runtime = runtime;
        this.modify("runtime", runtime);
    }

    /**
     * 格式化日期 [执行时间]
     */
    public String formatRuntime() {
        if (this.runtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(runtime);
    }
    /**
     * 设置 [时刻参数]
     */
    public void setScheduleparam(String scheduleparam) {
        this.scheduleparam = scheduleparam;
        this.modify("scheduleparam", scheduleparam);
    }

    /**
     * 设置 [时刻参数]
     */
    public void setScheduleparam2(String scheduleparam2) {
        this.scheduleparam2 = scheduleparam2;
        this.modify("scheduleparam2", scheduleparam2);
    }

    /**
     * 设置 [时刻参数]
     */
    public void setScheduleparam3(String scheduleparam3) {
        this.scheduleparam3 = scheduleparam3;
        this.modify("scheduleparam3", scheduleparam3);
    }

    /**
     * 设置 [时刻参数]
     */
    public void setScheduleparam4(String scheduleparam4) {
        this.scheduleparam4 = scheduleparam4;
        this.modify("scheduleparam4", scheduleparam4);
    }

    /**
     * 设置 [时刻设置状态]
     */
    public void setSchedulestate(String schedulestate) {
        this.schedulestate = schedulestate;
        this.modify("schedulestate", schedulestate);
    }

    /**
     * 设置 [计划编号]
     */
    public void setEmplanid(String emplanid) {
        this.emplanid = emplanid;
        this.modify("emplanid", emplanid);
    }

    /**
     * 设置 [持续时间]
     */
    public void setLastminute(Integer lastminute) {
        this.lastminute = lastminute;
        this.modify("lastminute", lastminute);
    }

    /**
     * 设置 [定时任务]
     */
    public void setTaskid(String taskid) {
        this.taskid = taskid;
        this.modify("taskid", taskid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("planscheduleid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


