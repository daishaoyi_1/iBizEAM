package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMBrand;
import cn.ibizlab.eam.core.eam_core.filter.EMBrandSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMBrandService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMBrandMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[品牌] 服务对象接口实现
 */
@Slf4j
@Service("EMBrandServiceImpl")
public class EMBrandServiceImpl extends ServiceImpl<EMBrandMapper, EMBrand> implements IEMBrandService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMBrand et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmbrandid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMBrand> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMBrand et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("embrandid", et.getEmbrandid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmbrandid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMBrand> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMBrand get(String key) {
        EMBrand et = getById(key);
        if(et == null){
            et = new EMBrand();
            et.setEmbrandid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMBrand getDraft(EMBrand et) {
        return et;
    }

    @Override
    public boolean checkKey(EMBrand et) {
        return (!ObjectUtils.isEmpty(et.getEmbrandid())) && (!Objects.isNull(this.getById(et.getEmbrandid())));
    }
    @Override
    @Transactional
    public boolean save(EMBrand et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMBrand et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMBrand> list) {
        List<EMBrand> create = new ArrayList<>();
        List<EMBrand> update = new ArrayList<>();
        for (EMBrand et : list) {
            if (ObjectUtils.isEmpty(et.getEmbrandid()) || ObjectUtils.isEmpty(getById(et.getEmbrandid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMBrand> list) {
        List<EMBrand> create = new ArrayList<>();
        List<EMBrand> update = new ArrayList<>();
        for (EMBrand et : list) {
            if (ObjectUtils.isEmpty(et.getEmbrandid()) || ObjectUtils.isEmpty(getById(et.getEmbrandid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMBrand> searchDefault(EMBrandSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMBrand> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMBrand>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMBrand> getEmbrandByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMBrand> getEmbrandByEntities(List<EMBrand> entities) {
        List ids =new ArrayList();
        for(EMBrand entity : entities){
            Serializable id=entity.getEmbrandid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMBrandService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



