package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[序列号]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "T_IBZSEQ", resultMap = "IBZSEQResultMap")
public class IBZSEQ extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 序列号标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "ibzseqid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "ibzseqid")
    @JsonProperty("ibzseqid")
    private String ibzseqid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 序列号名称
     */
    @TableField(value = "ibzseqname")
    @JSONField(name = "ibzseqname")
    @JsonProperty("ibzseqname")
    private String ibzseqname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 实体名称
     */
    @TableField(value = "entity")
    @JSONField(name = "entity")
    @JsonProperty("entity")
    private String entity;
    /**
     * 前缀
     */
    @TableField(value = "prefix")
    @JSONField(name = "prefix")
    @JsonProperty("prefix")
    private String prefix;
    /**
     * 日期参数格式
     */
    @TableField(value = "dateformat")
    @JSONField(name = "dateformat")
    @JsonProperty("dateformat")
    private String dateformat;
    /**
     * 序列号长度
     */
    @DEField(name = "serialnumber")
    @TableField(value = "serialnumber")
    @JSONField(name = "seriallength")
    @JsonProperty("seriallength")
    private Integer seriallength;
    /**
     * 自定义规则
     */
    @TableField(value = "seqformat")
    @JSONField(name = "seqformat")
    @JsonProperty("seqformat")
    private String seqformat;
    /**
     * 后缀
     */
    @TableField(value = "suffix")
    @JSONField(name = "suffix")
    @JsonProperty("suffix")
    private String suffix;
    /**
     * 参数
     */
    @TableField(value = "params")
    @JSONField(name = "params")
    @JsonProperty("params")
    private String params;
    /**
     * 生成规则
     */
    @TableField(value = "seqtype")
    @JSONField(name = "seqtype")
    @JsonProperty("seqtype")
    private String seqtype;
    /**
     * 当前序列号
     */
    @TableField(value = "curoffset")
    @JSONField(name = "curoffset")
    @JsonProperty("curoffset")
    private Long curoffset;
    /**
     * 当前ID
     */
    @TableField(value = "curid")
    @JSONField(name = "curid")
    @JsonProperty("curid")
    private String curid;
    /**
     * 日期
     */
    @TableField(value = "datetime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "datetime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("datetime")
    private Timestamp datetime;



    /**
     * 设置 [序列号名称]
     */
    public void setIbzseqname(String ibzseqname) {
        this.ibzseqname = ibzseqname;
        this.modify("ibzseqname", ibzseqname);
    }

    /**
     * 设置 [实体名称]
     */
    public void setEntity(String entity) {
        this.entity = entity;
        this.modify("entity", entity);
    }

    /**
     * 设置 [前缀]
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
        this.modify("prefix", prefix);
    }

    /**
     * 设置 [日期参数格式]
     */
    public void setDateformat(String dateformat) {
        this.dateformat = dateformat;
        this.modify("dateformat", dateformat);
    }

    /**
     * 设置 [序列号长度]
     */
    public void setSeriallength(Integer seriallength) {
        this.seriallength = seriallength;
        this.modify("serialnumber", seriallength);
    }

    /**
     * 设置 [自定义规则]
     */
    public void setSeqformat(String seqformat) {
        this.seqformat = seqformat;
        this.modify("seqformat", seqformat);
    }

    /**
     * 设置 [后缀]
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
        this.modify("suffix", suffix);
    }

    /**
     * 设置 [参数]
     */
    public void setParams(String params) {
        this.params = params;
        this.modify("params", params);
    }

    /**
     * 设置 [生成规则]
     */
    public void setSeqtype(String seqtype) {
        this.seqtype = seqtype;
        this.modify("seqtype", seqtype);
    }

    /**
     * 设置 [当前序列号]
     */
    public void setCuroffset(Long curoffset) {
        this.curoffset = curoffset;
        this.modify("curoffset", curoffset);
    }

    /**
     * 设置 [当前ID]
     */
    public void setCurid(String curid) {
        this.curid = curid;
        this.modify("curid", curid);
    }

    /**
     * 设置 [日期]
     */
    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
        this.modify("datetime", datetime);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDatetime() {
        if (this.datetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(datetime);
    }

    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("ibzseqid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


