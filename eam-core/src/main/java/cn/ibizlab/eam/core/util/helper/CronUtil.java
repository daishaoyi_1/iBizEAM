package cn.ibizlab.eam.core.util.helper;

import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerUtils;
import org.quartz.impl.triggers.CronTriggerImpl;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @ClassName CronUtil
 * @Deacription TODO
 * @Author 张耀阳
 * @Date 2021/2/3 13:42
 * @Version 1.0
 **/
@Slf4j
@Component
public class CronUtil {
    private static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String createMonthCronExpression(PLANSCHEDULE et){
        StringBuilder sb = new StringBuilder();
        //月份
        String months = et.getScheduleparam();

        //每月日期
        String days = et.getScheduleparam2();

        //执行时间
        Timestamp runtime = et.getRuntime();
        String format = sdf.format(runtime);
        Date parse=null;
        try {
             parse = sdf.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(parse);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        sb.append(second);
        sb.append(" ");
        sb.append(minute);
        sb.append(" ");
        sb.append(hour);
        sb.append(" ");
        sb.append(days);
        sb.append(" ");
        sb.append(months);
        sb.append(" ? *");
        String cron = sb.toString();
        return cron;
    }



    public static String createWeekCronExpression(PLANSCHEDULE et){
        StringBuilder sb = new StringBuilder();
        StringBuilder sbDay = new StringBuilder();

        //每周的日期
        String weekOfDay = et.getScheduleparam2();
        String[] split = weekOfDay.split(",");
        for (String s : split) {
            int day = Integer.parseInt(s);
            sbDay.append(day+1);
            sbDay.append(",");
        }
        String newWeekOfDay = sbDay.toString().substring(0, sbDay.toString().length() - 1);
        //执行时间
        Timestamp runtime = et.getRuntime();
        String format = sdf.format(runtime);
        Date parse=null;
        try {
            parse = sdf.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(parse);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        sb.append(second);
        sb.append(" ");
        sb.append(minute);
        sb.append(" ");
        sb.append(hour);
        sb.append(" ? * ");
        sb.append(newWeekOfDay);
        String cron = sb.toString();
        return cron;
    }


    public static String createDayCronExpression(PLANSCHEDULE et){

        StringBuilder sb = new StringBuilder();
        //执行时间
        Timestamp runtime = et.getRuntime();
        String format = sdf.format(runtime);
        Date parse=null;
        try {
            parse = sdf.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(parse);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        sb.append(second);
        sb.append(" ");
        sb.append(minute);
        sb.append(" ");
        sb.append(hour);
        sb.append(" * * ?");
        String cron = sb.toString();

        return cron;
    }

    //指定时间
    public static String createCustomizeCronExpression(PLANSCHEDULE et){

        StringBuilder sb = new StringBuilder();
        //运行日期
        Timestamp rundate = et.getRundate();
        //执行时间
        Timestamp runtime = et.getRuntime();
        String formatRuntime = sdf.format(runtime);
        String formatRundate = sdf.format(rundate);
        Date parseRuntime=null;
        Date parseRundate=null;
        try {
            parseRuntime = sdf.parse(formatRuntime);
            parseRundate = sdf.parse(formatRundate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendarRuntime=Calendar.getInstance();
        Calendar calendarRundate=Calendar.getInstance();
        calendarRuntime.setTime(parseRuntime);
        calendarRundate.setTime(parseRundate);
        int hour = calendarRuntime.get(Calendar.HOUR_OF_DAY);
        int minute = calendarRuntime.get(Calendar.MINUTE);
        int second = calendarRuntime.get(Calendar.SECOND);

        int year = calendarRundate.get(Calendar.YEAR);
        //Calendar的月份是从 0开始算的
        int month = calendarRundate.get(Calendar.MONTH);
        int day = calendarRundate.get(Calendar.DAY_OF_MONTH);

        sb.append(second);
        sb.append(" ");
        sb.append(minute);
        sb.append(" ");
        sb.append(hour);
        sb.append(" ");
        sb.append(day);
        sb.append(" ");
        sb.append(month+1);
        sb.append(" ? *");
        String cron = sb.toString();

        return cron;
    }


    //自定义时间
    public static String createAssignCronExpression(PLANSCHEDULE et){

        //开始执行时间
        Timestamp cyclestarttime = et.getCyclestarttime();
        //间隔天数
        Integer intervalminute = et.getIntervalminute();
        StringBuilder sb = new StringBuilder();
        String format = sdf.format(cyclestarttime);
        Date parse=null;
        try {
            parse= sdf.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parse);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);


        sb.append(second);
        sb.append(" ");
        sb.append(minute);
        sb.append(" ");
        sb.append(hour);
        sb.append(" ");
        sb.append(day);
        sb.append("/");
        sb.append(intervalminute);
        sb.append(" ");
        sb.append(month+1);
        sb.append(" ? *");
        String cron = sb.toString();
        return cron;
    }


    /**
     *
     * @param cron
     * @return
     */
    public static Date getNextTriggerTime(String cron){
        if(!CronExpression.isValidExpression(cron)){
            return null;
        }
        CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity("Caclulate Date").withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
        Date time0 = trigger.getStartTime();
        Date time1 = trigger.getFireTimeAfter(time0);
        return time1;
    }
}
