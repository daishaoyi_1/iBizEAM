package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMItemSubMap;
import cn.ibizlab.eam.core.eam_core.filter.EMItemSubMapSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMItemSubMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMItemSubMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[物品替换件] 服务对象接口实现
 */
@Slf4j
@Service("EMItemSubMapServiceImpl")
public class EMItemSubMapServiceImpl extends ServiceImpl<EMItemSubMapMapper, EMItemSubMap> implements IEMItemSubMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMItemSubMap et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemsubmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMItemSubMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMItemSubMap et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emitemsubmapid", et.getEmitemsubmapid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemsubmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMItemSubMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMItemSubMap get(String key) {
        EMItemSubMap et = getById(key);
        if(et == null){
            et = new EMItemSubMap();
            et.setEmitemsubmapid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMItemSubMap getDraft(EMItemSubMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMItemSubMap et) {
        return (!ObjectUtils.isEmpty(et.getEmitemsubmapid())) && (!Objects.isNull(this.getById(et.getEmitemsubmapid())));
    }
    @Override
    @Transactional
    public boolean save(EMItemSubMap et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMItemSubMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMItemSubMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemSubMap> create = new ArrayList<>();
        List<EMItemSubMap> update = new ArrayList<>();
        for (EMItemSubMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemsubmapid()) || ObjectUtils.isEmpty(getById(et.getEmitemsubmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMItemSubMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemSubMap> create = new ArrayList<>();
        List<EMItemSubMap> update = new ArrayList<>();
        for (EMItemSubMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemsubmapid()) || ObjectUtils.isEmpty(getById(et.getEmitemsubmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMItemSubMap> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }
    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMItemSubMap>().eq("itemid",emitemid));
    }

	@Override
    public List<EMItemSubMap> selectBySubitemid(String emitemid) {
        return baseMapper.selectBySubitemid(emitemid);
    }
    @Override
    public void removeBySubitemid(String emitemid) {
        this.remove(new QueryWrapper<EMItemSubMap>().eq("subitemid",emitemid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMItemSubMap> searchDefault(EMItemSubMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemSubMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemSubMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMItemSubMap et){
        //实体关系[DER1N_EMITEMSUBMAP_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemname(item.getEmitemname());
        }
        //实体关系[DER1N_EMITEMSUBMAP_EMITEM_SUBITEMID]
        if(!ObjectUtils.isEmpty(et.getSubitemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem subitem=et.getSubitem();
            if(ObjectUtils.isEmpty(subitem)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getSubitemid());
                et.setSubitem(majorEntity);
                subitem=majorEntity;
            }
            et.setSubitemname(subitem.getEmitemname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMItemSubMap> getEmitemsubmapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMItemSubMap> getEmitemsubmapByEntities(List<EMItemSubMap> entities) {
        List ids =new ArrayList();
        for(EMItemSubMap entity : entities){
            Serializable id=entity.getEmitemsubmapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMItemSubMapService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



