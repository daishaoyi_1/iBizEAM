package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMRiggingH;
import cn.ibizlab.eam.core.eam_core.filter.EMRiggingHSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMRiggingHService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMRiggingHMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[工具库出入记录] 服务对象接口实现
 */
@Slf4j
@Service("EMRiggingHServiceImpl")
public class EMRiggingHServiceImpl extends ServiceImpl<EMRiggingHMapper, EMRiggingH> implements IEMRiggingHService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRiggingService emriggingService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMRiggingH et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmrigginghid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMRiggingH> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMRiggingH et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emrigginghid", et.getEmrigginghid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmrigginghid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMRiggingH> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMRiggingH get(String key) {
        EMRiggingH et = getById(key);
        if(et == null){
            et = new EMRiggingH();
            et.setEmrigginghid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMRiggingH getDraft(EMRiggingH et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMRiggingH et) {
        return (!ObjectUtils.isEmpty(et.getEmrigginghid())) && (!Objects.isNull(this.getById(et.getEmrigginghid())));
    }
    @Override
    @Transactional
    public boolean save(EMRiggingH et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMRiggingH et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMRiggingH> list) {
        list.forEach(item->fillParentData(item));
        List<EMRiggingH> create = new ArrayList<>();
        List<EMRiggingH> update = new ArrayList<>();
        for (EMRiggingH et : list) {
            if (ObjectUtils.isEmpty(et.getEmrigginghid()) || ObjectUtils.isEmpty(getById(et.getEmrigginghid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMRiggingH> list) {
        list.forEach(item->fillParentData(item));
        List<EMRiggingH> create = new ArrayList<>();
        List<EMRiggingH> update = new ArrayList<>();
        for (EMRiggingH et : list) {
            if (ObjectUtils.isEmpty(et.getEmrigginghid()) || ObjectUtils.isEmpty(getById(et.getEmrigginghid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMRiggingH> selectByEmitempuseid(String emitempuseid) {
        return baseMapper.selectByEmitempuseid(emitempuseid);
    }
    @Override
    public void removeByEmitempuseid(String emitempuseid) {
        this.remove(new QueryWrapper<EMRiggingH>().eq("emitempuseid",emitempuseid));
    }

	@Override
    public List<EMRiggingH> selectByEmriggingid(String emriggingid) {
        return baseMapper.selectByEmriggingid(emriggingid);
    }
    @Override
    public void removeByEmriggingid(String emriggingid) {
        this.remove(new QueryWrapper<EMRiggingH>().eq("emriggingid",emriggingid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMRiggingH> searchDefault(EMRiggingHSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMRiggingH> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMRiggingH>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMRiggingH et){
        //实体关系[DER1N_EMRIGGINGH_EMITEMPUSE_EMITEMPUSEID]
        if(!ObjectUtils.isEmpty(et.getEmitempuseid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemPUse emitempuse=et.getEmitempuse();
            if(ObjectUtils.isEmpty(emitempuse)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemPUse majorEntity=emitempuseService.get(et.getEmitempuseid());
                et.setEmitempuse(majorEntity);
                emitempuse=majorEntity;
            }
            et.setEmitempusename(emitempuse.getEmitempusename());
        }
        //实体关系[DER1N_EMRIGGINGH_EMRIGGING_EMRIGGINGID]
        if(!ObjectUtils.isEmpty(et.getEmriggingid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRigging emrigging=et.getEmrigging();
            if(ObjectUtils.isEmpty(emrigging)){
                cn.ibizlab.eam.core.eam_core.domain.EMRigging majorEntity=emriggingService.get(et.getEmriggingid());
                et.setEmrigging(majorEntity);
                emrigging=majorEntity;
            }
            et.setEmriggingname(emrigging.getEmriggingname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMRiggingH> getEmrigginghByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMRiggingH> getEmrigginghByEntities(List<EMRiggingH> entities) {
        List ids =new ArrayList();
        for(EMRiggingH entity : entities){
            Serializable id=entity.getEmrigginghid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMRiggingHService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



