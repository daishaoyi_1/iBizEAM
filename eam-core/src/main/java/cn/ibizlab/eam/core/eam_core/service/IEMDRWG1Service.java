package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMDRWG1;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWG1SearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMDRWG1] 服务对象接口
 */
public interface IEMDRWG1Service extends IService<EMDRWG1> {

    boolean create(EMDRWG1 et);
    void createBatch(List<EMDRWG1> list);
    boolean update(EMDRWG1 et);
    void updateBatch(List<EMDRWG1> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMDRWG1 get(String key);
    EMDRWG1 getDraft(EMDRWG1 et);
    boolean checkKey(EMDRWG1 et);
    boolean save(EMDRWG1 et);
    void saveBatch(List<EMDRWG1> list);
    Page<EMDRWG1> searchDefault(EMDRWG1SearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMDRWG1> getEmdrwg1ByIds(List<String> ids);
    List<EMDRWG1> getEmdrwg1ByEntities(List<EMDRWG1> entities);
}


