package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[领料单换料记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPURCHANGEHIS_BASE", resultMap = "EMPurchangeHisResultMap")
public class EMPurchangeHis extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 领料单换料记录名称
     */
    @DEField(defaultValue = "换料记录名称")
    @TableField(value = "empurchangehisname")
    @JSONField(name = "empurchangehisname")
    @JsonProperty("empurchangehisname")
    private String empurchangehisname;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 备注
     */
    @TableField(value = "backit")
    @JSONField(name = "backit")
    @JsonProperty("backit")
    private String backit;
    /**
     * 部门
     */
    @TableField(value = "pfdeptname")
    @JSONField(name = "pfdeptname")
    @JsonProperty("pfdeptname")
    private String pfdeptname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 换料人
     */
    @TableField(value = "pfempname")
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    private String pfempname;
    /**
     * 部门
     */
    @TableField(value = "pfdeptid")
    @JSONField(name = "pfdeptid")
    @JsonProperty("pfdeptid")
    private String pfdeptid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 换料时间
     */
    @TableField(value = "hdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "hdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("hdate")
    private Timestamp hdate;
    /**
     * 换料数量
     */
    @TableField(value = "changenum")
    @JSONField(name = "changenum")
    @JsonProperty("changenum")
    private Double changenum;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 原因
     */
    @TableField(value = "resson")
    @JSONField(name = "resson")
    @JsonProperty("resson")
    private String resson;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 换料记录号
     */
    @DEField(isKeyField = true)
    @TableId(value = "empurchangehisid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "empurchangehisid")
    @JsonProperty("empurchangehisid")
    private String empurchangehisid;
    /**
     * 换料人
     */
    @TableField(value = "pfempid")
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    private String pfempid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    private String unitname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 领料单
     */
    @TableField(exist = false)
    @JSONField(name = "emitempusename")
    @JsonProperty("emitempusename")
    private String emitempusename;
    /**
     * 实发数
     */
    @TableField(exist = false)
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;
    /**
     * 领料单
     */
    @TableField(value = "emitempuseid")
    @JSONField(name = "emitempuseid")
    @JsonProperty("emitempuseid")
    private String emitempuseid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemPUse emitempuse;



    /**
     * 设置 [领料单换料记录名称]
     */
    public void setEmpurchangehisname(String empurchangehisname) {
        this.empurchangehisname = empurchangehisname;
        this.modify("empurchangehisname", empurchangehisname);
    }

    /**
     * 设置 [备注]
     */
    public void setBackit(String backit) {
        this.backit = backit;
        this.modify("backit", backit);
    }

    /**
     * 设置 [部门]
     */
    public void setPfdeptname(String pfdeptname) {
        this.pfdeptname = pfdeptname;
        this.modify("pfdeptname", pfdeptname);
    }

    /**
     * 设置 [换料人]
     */
    public void setPfempname(String pfempname) {
        this.pfempname = pfempname;
        this.modify("pfempname", pfempname);
    }

    /**
     * 设置 [部门]
     */
    public void setPfdeptid(String pfdeptid) {
        this.pfdeptid = pfdeptid;
        this.modify("pfdeptid", pfdeptid);
    }

    /**
     * 设置 [换料时间]
     */
    public void setHdate(Timestamp hdate) {
        this.hdate = hdate;
        this.modify("hdate", hdate);
    }

    /**
     * 格式化日期 [换料时间]
     */
    public String formatHdate() {
        if (this.hdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(hdate);
    }
    /**
     * 设置 [换料数量]
     */
    public void setChangenum(Double changenum) {
        this.changenum = changenum;
        this.modify("changenum", changenum);
    }

    /**
     * 设置 [原因]
     */
    public void setResson(String resson) {
        this.resson = resson;
        this.modify("resson", resson);
    }

    /**
     * 设置 [换料人]
     */
    public void setPfempid(String pfempid) {
        this.pfempid = pfempid;
        this.modify("pfempid", pfempid);
    }

    /**
     * 设置 [领料单]
     */
    public void setEmitempuseid(String emitempuseid) {
        this.emitempuseid = emitempuseid;
        this.modify("emitempuseid", emitempuseid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("empurchangehisid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


