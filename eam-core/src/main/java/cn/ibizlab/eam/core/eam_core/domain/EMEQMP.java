package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备仪表]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQMP_BASE", resultMap = "EMEQMPResultMap")
public class EMEQMP extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 设备仪表代码
     */
    @TableField(value = "mpcode")
    @JSONField(name = "mpcode")
    @JsonProperty("mpcode")
    private String mpcode;
    /**
     * 设备仪表名称
     */
    @TableField(value = "emeqmpname")
    @JSONField(name = "emeqmpname")
    @JsonProperty("emeqmpname")
    private String emeqmpname;
    /**
     * 仪表备注
     */
    @TableField(value = "mpdesc")
    @JSONField(name = "mpdesc")
    @JsonProperty("mpdesc")
    private String mpdesc;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 设备仪表信息
     */
    @TableField(exist = false)
    @JSONField(name = "mpinfo")
    @JsonProperty("mpinfo")
    private String mpinfo;
    /**
     * 设备仪表标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqmpid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqmpid")
    @JsonProperty("emeqmpid")
    private String emeqmpid;
    /**
     * 正常参考值
     */
    @TableField(value = "normalrefval")
    @JSONField(name = "normalrefval")
    @JsonProperty("normalrefval")
    private String normalrefval;
    /**
     * 设备仪表类型
     */
    @TableField(value = "mptypeid")
    @JSONField(name = "mptypeid")
    @JsonProperty("mptypeid")
    private String mptypeid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 仪表范围
     */
    @TableField(value = "mpscope")
    @JSONField(name = "mpscope")
    @JsonProperty("mpscope")
    private String mpscope;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;



    /**
     * 设置 [设备仪表代码]
     */
    public void setMpcode(String mpcode) {
        this.mpcode = mpcode;
        this.modify("mpcode", mpcode);
    }

    /**
     * 设置 [设备仪表名称]
     */
    public void setEmeqmpname(String emeqmpname) {
        this.emeqmpname = emeqmpname;
        this.modify("emeqmpname", emeqmpname);
    }

    /**
     * 设置 [仪表备注]
     */
    public void setMpdesc(String mpdesc) {
        this.mpdesc = mpdesc;
        this.modify("mpdesc", mpdesc);
    }

    /**
     * 设置 [正常参考值]
     */
    public void setNormalrefval(String normalrefval) {
        this.normalrefval = normalrefval;
        this.modify("normalrefval", normalrefval);
    }

    /**
     * 设置 [设备仪表类型]
     */
    public void setMptypeid(String mptypeid) {
        this.mptypeid = mptypeid;
        this.modify("mptypeid", mptypeid);
    }

    /**
     * 设置 [仪表范围]
     */
    public void setMpscope(String mpscope) {
        this.mpscope = mpscope;
        this.modify("mpscope", mpscope);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqmpid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


