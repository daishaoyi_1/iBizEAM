

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEMap;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMRFODEMapInheritMapping {

    @Mappings({
        @Mapping(source ="emrfodemapid",target = "emobjmapid"),
        @Mapping(source ="emrfodemapname",target = "emobjmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="rfodeid",target = "objid"),
        @Mapping(source ="refobjid",target = "objpid"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObjMap toEmobjmap(EMRFODEMap minorEntity);

    @Mappings({
        @Mapping(source ="emobjmapid" ,target = "emrfodemapid"),
        @Mapping(source ="emobjmapname" ,target = "emrfodemapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objid",target = "rfodeid"),
        @Mapping(source ="objpid",target = "refobjid"),
    })
    EMRFODEMap toEmrfodemap(EMObjMap majorEntity);

    List<EMObjMap> toEmobjmap(List<EMRFODEMap> minorEntities);

    List<EMRFODEMap> toEmrfodemap(List<EMObjMap> majorEntities);

}


