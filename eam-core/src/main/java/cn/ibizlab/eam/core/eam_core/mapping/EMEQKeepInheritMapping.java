

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKeep;
import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQKeepInheritMapping {

    @Mappings({
        @Mapping(source ="emeqkeepid",target = "emeqahid"),
        @Mapping(source ="emeqkeepname",target = "emeqahname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQAH toEmeqah(EMEQKeep minorEntity);

    @Mappings({
        @Mapping(source ="emeqahid" ,target = "emeqkeepid"),
        @Mapping(source ="emeqahname" ,target = "emeqkeepname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQKeep toEmeqkeep(EMEQAH majorEntity);

    List<EMEQAH> toEmeqah(List<EMEQKeep> minorEntities);

    List<EMEQKeep> toEmeqkeep(List<EMEQAH> majorEntities);

}


