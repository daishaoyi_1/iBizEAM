

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKP;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQKPInheritMapping {

    @Mappings({
        @Mapping(source ="emeqkpid",target = "emobjectid"),
        @Mapping(source ="emeqkpname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="kpcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMEQKP minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emeqkpid"),
        @Mapping(source ="emobjectname" ,target = "emeqkpname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "kpcode"),
    })
    EMEQKP toEmeqkp(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMEQKP> minorEntities);

    List<EMEQKP> toEmeqkp(List<EMObject> majorEntities);

}


