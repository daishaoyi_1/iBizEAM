package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMProduct;
import cn.ibizlab.eam.core.eam_core.filter.EMProductSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMProduct] 服务对象接口
 */
public interface IEMProductService extends IService<EMProduct> {

    boolean create(EMProduct et);
    void createBatch(List<EMProduct> list);
    boolean update(EMProduct et);
    void updateBatch(List<EMProduct> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMProduct get(String key);
    EMProduct getDraft(EMProduct et);
    boolean checkKey(EMProduct et);
    boolean save(EMProduct et);
    void saveBatch(List<EMProduct> list);
    Page<EMProduct> searchDefault(EMProductSearchContext context);
    List<EMProduct> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMProduct> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    List<EMProduct> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMProduct> selectByServiceid(String emserviceid);
    void removeByServiceid(String emserviceid);
    List<EMProduct> selectByTeamid(String pfteamid);
    void removeByTeamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMProduct> getEmproductByIds(List<String> ids);
    List<EMProduct> getEmproductByEntities(List<EMProduct> entities);
}


