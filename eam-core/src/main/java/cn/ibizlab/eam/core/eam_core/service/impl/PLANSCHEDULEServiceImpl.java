package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULESearchContext;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULEService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.PLANSCHEDULEMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划时刻设置] 服务对象接口实现
 */
@Slf4j
@Service("PLANSCHEDULEServiceImpl")
public class PLANSCHEDULEServiceImpl extends ServiceImpl<PLANSCHEDULEMapper, PLANSCHEDULE> implements IPLANSCHEDULEService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PLANSCHEDULE et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PLANSCHEDULE> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PLANSCHEDULE et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("planscheduleid", et.getPlanscheduleid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PLANSCHEDULE> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PLANSCHEDULE get(String key) {
        PLANSCHEDULE et = getById(key);
        if(et == null){
            et = new PLANSCHEDULE();
            et.setPlanscheduleid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PLANSCHEDULE getDraft(PLANSCHEDULE et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(PLANSCHEDULE et) {
        return (!ObjectUtils.isEmpty(et.getPlanscheduleid())) && (!Objects.isNull(this.getById(et.getPlanscheduleid())));
    }
    @Override
    @Transactional
    public PLANSCHEDULE genTask(PLANSCHEDULE et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean genTaskBatch(List<PLANSCHEDULE> etList) {
        for(PLANSCHEDULE et : etList) {
            genTask(et);
        }
        return true;
    }

    @Override
    @Transactional
    public boolean save(PLANSCHEDULE et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PLANSCHEDULE et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PLANSCHEDULE> list) {
        list.forEach(item->fillParentData(item));
        List<PLANSCHEDULE> create = new ArrayList<>();
        List<PLANSCHEDULE> update = new ArrayList<>();
        for (PLANSCHEDULE et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PLANSCHEDULE> list) {
        list.forEach(item->fillParentData(item));
        List<PLANSCHEDULE> create = new ArrayList<>();
        List<PLANSCHEDULE> update = new ArrayList<>();
        for (PLANSCHEDULE et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<PLANSCHEDULE> selectByEmplanid(String emplanid) {
        return baseMapper.selectByEmplanid(emplanid);
    }
    @Override
    public void removeByEmplanid(String emplanid) {
        this.remove(new QueryWrapper<PLANSCHEDULE>().eq("emplanid",emplanid));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<PLANSCHEDULE> searchDefault(PLANSCHEDULESearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PLANSCHEDULE> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PLANSCHEDULE>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 数据集2
     */
    @Override
    public Page<PLANSCHEDULE> searchIndexDER(PLANSCHEDULESearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PLANSCHEDULE> pages=baseMapper.searchIndexDER(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PLANSCHEDULE>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(PLANSCHEDULE et){
        //实体关系[DER1N_PLANSCHEDULE_EMPLAN_EMPLANID]
        if(!ObjectUtils.isEmpty(et.getEmplanid())){
            cn.ibizlab.eam.core.eam_core.domain.EMPlan emplan=et.getEmplan();
            if(ObjectUtils.isEmpty(emplan)){
                cn.ibizlab.eam.core.eam_core.domain.EMPlan majorEntity=emplanService.get(et.getEmplanid());
                et.setEmplan(majorEntity);
                emplan=majorEntity;
            }
            et.setEmplanname(emplan.getEmplanname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PLANSCHEDULE> getPlanscheduleByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PLANSCHEDULE> getPlanscheduleByEntities(List<PLANSCHEDULE> entities) {
        List ids =new ArrayList();
        for(PLANSCHEDULE entity : entities){
            Serializable id=entity.getPlanscheduleid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPLANSCHEDULEService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



