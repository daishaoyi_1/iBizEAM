package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[工具库出入记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMRIGGINGH_BASE", resultMap = "EMRiggingHResultMap")
public class EMRiggingH extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工索具出入记录名称
     */
    @DEField(defaultValue = "未定义")
    @TableField(value = "emrigginghname")
    @JSONField(name = "emrigginghname")
    @JsonProperty("emrigginghname")
    private String emrigginghname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 工索具出入记录标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emrigginghid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emrigginghid")
    @JsonProperty("emrigginghid")
    private String emrigginghid;
    /**
     * 工具库出入日期
     */
    @TableField(value = "iodate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "iodate", format = "yyyy-MM-dd")
    @JsonProperty("iodate")
    private Timestamp iodate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 数量
     */
    @DEField(defaultValue = "0")
    @TableField(value = "numb")
    @JSONField(name = "numb")
    @JsonProperty("numb")
    private Integer numb;
    /**
     * 出入库状态
     */
    @TableField(value = "iostate")
    @JSONField(name = "iostate")
    @JsonProperty("iostate")
    private String iostate;
    /**
     * 工索具清单
     */
    @TableField(exist = false)
    @JSONField(name = "emriggingname")
    @JsonProperty("emriggingname")
    private String emriggingname;
    /**
     * 领料单
     */
    @TableField(exist = false)
    @JSONField(name = "emitempusename")
    @JsonProperty("emitempusename")
    private String emitempusename;
    /**
     * 工索具清单
     */
    @TableField(value = "emriggingid")
    @JSONField(name = "emriggingid")
    @JsonProperty("emriggingid")
    private String emriggingid;
    /**
     * 领料单
     */
    @TableField(value = "emitempuseid")
    @JSONField(name = "emitempuseid")
    @JsonProperty("emitempuseid")
    private String emitempuseid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemPUse emitempuse;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRigging emrigging;



    /**
     * 设置 [工索具出入记录名称]
     */
    public void setEmrigginghname(String emrigginghname) {
        this.emrigginghname = emrigginghname;
        this.modify("emrigginghname", emrigginghname);
    }

    /**
     * 设置 [工具库出入日期]
     */
    public void setIodate(Timestamp iodate) {
        this.iodate = iodate;
        this.modify("iodate", iodate);
    }

    /**
     * 格式化日期 [工具库出入日期]
     */
    public String formatIodate() {
        if (this.iodate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(iodate);
    }
    /**
     * 设置 [数量]
     */
    public void setNumb(Integer numb) {
        this.numb = numb;
        this.modify("numb", numb);
    }

    /**
     * 设置 [出入库状态]
     */
    public void setIostate(String iostate) {
        this.iostate = iostate;
        this.modify("iostate", iostate);
    }

    /**
     * 设置 [工索具清单]
     */
    public void setEmriggingid(String emriggingid) {
        this.emriggingid = emriggingid;
        this.modify("emriggingid", emriggingid);
    }

    /**
     * 设置 [领料单]
     */
    public void setEmitempuseid(String emitempuseid) {
        this.emitempuseid = emitempuseid;
        this.modify("emitempuseid", emitempuseid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emrigginghid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


