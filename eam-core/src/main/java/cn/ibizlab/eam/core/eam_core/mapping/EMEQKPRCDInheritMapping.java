

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKPRCD;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQKPRCDInheritMapping {

    @Mappings({
        @Mapping(source ="emeqkprcdid",target = "emdprctid"),
        @Mapping(source ="emeqkprcdname",target = "emdprctname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="kpname",target = "dpname"),
        @Mapping(source ="kpid",target = "dpid"),
    })
    EMDPRCT toEmdprct(EMEQKPRCD minorEntity);

    @Mappings({
        @Mapping(source ="emdprctid" ,target = "emeqkprcdid"),
        @Mapping(source ="emdprctname" ,target = "emeqkprcdname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="dpname",target = "kpname"),
        @Mapping(source ="dpid",target = "kpid"),
    })
    EMEQKPRCD toEmeqkprcd(EMDPRCT majorEntity);

    List<EMDPRCT> toEmdprct(List<EMEQKPRCD> minorEntities);

    List<EMEQKPRCD> toEmeqkprcd(List<EMDPRCT> majorEntities);

}


