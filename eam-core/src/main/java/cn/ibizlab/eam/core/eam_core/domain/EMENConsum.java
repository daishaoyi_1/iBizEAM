package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[能耗]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMENCONSUM_BASE", resultMap = "EMENConsumResultMap")
public class EMENConsum extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 单价
     */
    @DEField(defaultValue = "0")
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;
    /**
     * 部门
     */
    @TableField(value = "deptid")
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    private String deptid;
    /**
     * 能耗标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emenconsumid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emenconsumid")
    @JsonProperty("emenconsumid")
    private String emenconsumid;
    /**
     * 领料分类
     */
    @TableField(value = "pusetype")
    @JSONField(name = "pusetype")
    @JsonProperty("pusetype")
    private String pusetype;
    /**
     * 倍率
     */
    @DEField(defaultValue = "1")
    @TableField(value = "vrate")
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    private Double vrate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 总金额
     */
    @TableField(exist = false)
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 能耗值
     */
    @TableField(value = "nval")
    @JSONField(name = "nval")
    @JsonProperty("nval")
    private Double nval;
    /**
     * 上次记录值
     */
    @TableField(value = "lastval")
    @JSONField(name = "lastval")
    @JsonProperty("lastval")
    private Double lastval;
    /**
     * 部门
     */
    @TableField(value = "deptname")
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    private String deptname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 本次记录值
     */
    @TableField(value = "curval")
    @JSONField(name = "curval")
    @JsonProperty("curval")
    private Double curval;
    /**
     * 能耗名称
     */
    @DEField(defaultValue = "VAR_EQUIPID||to_char(VAR_EDATE,YYYY-MM-DD)")
    @TableField(value = "emenconsumname")
    @JSONField(name = "emenconsumname")
    @JsonProperty("emenconsumname")
    private String emenconsumname;
    /**
     * 上次采集时间
     */
    @TableField(value = "bdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "bdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    private Timestamp bdate;
    /**
     * 采集时间
     */
    @TableField(value = "edate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "edate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    private Timestamp edate;
    /**
     * 能源单价
     */
    @TableField(exist = false)
    @JSONField(name = "enprice")
    @JsonProperty("enprice")
    private Double enprice;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 物品二级类
     */
    @TableField(exist = false)
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    private String itemmtypename;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "woname")
    @JsonProperty("woname")
    private String woname;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    private String unitname;
    /**
     * 能源库存单价
     */
    @TableField(exist = false)
    @JSONField(name = "itemprice")
    @JsonProperty("itemprice")
    private Double itemprice;
    /**
     * 能源类型
     */
    @TableField(exist = false)
    @JSONField(name = "energytypeid")
    @JsonProperty("energytypeid")
    private String energytypeid;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipcode")
    @JsonProperty("equipcode")
    private String equipcode;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 能源
     */
    @TableField(exist = false)
    @JSONField(name = "enname")
    @JsonProperty("enname")
    private String enname;
    /**
     * 设备统计归类
     */
    @TableField(exist = false)
    @JSONField(name = "sname")
    @JsonProperty("sname")
    private String sname;
    /**
     * 物品类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    private String itemtypeid;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    private String itembtypeid;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    private String itembtypename;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 物品二级类
     */
    @TableField(exist = false)
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    private String itemmtypeid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 工单
     */
    @TableField(value = "woid")
    @JSONField(name = "woid")
    @JsonProperty("woid")
    private String woid;
    /**
     * 能源
     */
    @TableField(value = "enid")
    @JSONField(name = "enid")
    @JsonProperty("enid")
    private String enid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEN en;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wo;



    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [部门]
     */
    public void setDeptid(String deptid) {
        this.deptid = deptid;
        this.modify("deptid", deptid);
    }

    /**
     * 设置 [领料分类]
     */
    public void setPusetype(String pusetype) {
        this.pusetype = pusetype;
        this.modify("pusetype", pusetype);
    }

    /**
     * 设置 [倍率]
     */
    public void setVrate(Double vrate) {
        this.vrate = vrate;
        this.modify("vrate", vrate);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [能耗值]
     */
    public void setNval(Double nval) {
        this.nval = nval;
        this.modify("nval", nval);
    }

    /**
     * 设置 [上次记录值]
     */
    public void setLastval(Double lastval) {
        this.lastval = lastval;
        this.modify("lastval", lastval);
    }

    /**
     * 设置 [部门]
     */
    public void setDeptname(String deptname) {
        this.deptname = deptname;
        this.modify("deptname", deptname);
    }

    /**
     * 设置 [本次记录值]
     */
    public void setCurval(Double curval) {
        this.curval = curval;
        this.modify("curval", curval);
    }

    /**
     * 设置 [能耗名称]
     */
    public void setEmenconsumname(String emenconsumname) {
        this.emenconsumname = emenconsumname;
        this.modify("emenconsumname", emenconsumname);
    }

    /**
     * 设置 [上次采集时间]
     */
    public void setBdate(Timestamp bdate) {
        this.bdate = bdate;
        this.modify("bdate", bdate);
    }

    /**
     * 格式化日期 [上次采集时间]
     */
    public String formatBdate() {
        if (this.bdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(bdate);
    }
    /**
     * 设置 [采集时间]
     */
    public void setEdate(Timestamp edate) {
        this.edate = edate;
        this.modify("edate", edate);
    }

    /**
     * 格式化日期 [采集时间]
     */
    public String formatEdate() {
        if (this.edate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(edate);
    }
    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [工单]
     */
    public void setWoid(String woid) {
        this.woid = woid;
        this.modify("woid", woid);
    }

    /**
     * 设置 [能源]
     */
    public void setEnid(String enid) {
        this.enid = enid;
        this.modify("enid", enid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emenconsumid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


