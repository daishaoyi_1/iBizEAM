package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPlanTDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanTDetailSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPlanTDetail] 服务对象接口
 */
public interface IEMPlanTDetailService extends IService<EMPlanTDetail> {

    boolean create(EMPlanTDetail et);
    void createBatch(List<EMPlanTDetail> list);
    boolean update(EMPlanTDetail et);
    void updateBatch(List<EMPlanTDetail> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMPlanTDetail get(String key);
    EMPlanTDetail getDraft(EMPlanTDetail et);
    boolean checkKey(EMPlanTDetail et);
    boolean save(EMPlanTDetail et);
    void saveBatch(List<EMPlanTDetail> list);
    Page<EMPlanTDetail> searchDefault(EMPlanTDetailSearchContext context);
    List<EMPlanTDetail> selectByPlantemplid(String emplantemplid);
    void removeByPlantemplid(String emplantemplid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPlanTDetail> getEmplantdetailByIds(List<String> ids);
    List<EMPlanTDetail> getEmplantdetailByEntities(List<EMPlanTDetail> entities);
}


