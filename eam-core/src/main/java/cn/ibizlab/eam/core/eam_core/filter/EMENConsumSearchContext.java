package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMENConsum;
/**
 * 关系型数据实体[EMENConsum] 查询条件对象
 */
@Slf4j
@Data
public class EMENConsumSearchContext extends QueryWrapperContext<EMENConsum> {

	private String n_deptid_eq;//[部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_pusetype_eq;//[领料分类]
	public void setN_pusetype_eq(String n_pusetype_eq) {
        this.n_pusetype_eq = n_pusetype_eq;
        if(!ObjectUtils.isEmpty(this.n_pusetype_eq)){
            this.getSearchCond().eq("pusetype", n_pusetype_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_deptname_eq;//[部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_emenconsumname_like;//[能耗名称]
	public void setN_emenconsumname_like(String n_emenconsumname_like) {
        this.n_emenconsumname_like = n_emenconsumname_like;
        if(!ObjectUtils.isEmpty(this.n_emenconsumname_like)){
            this.getSearchCond().like("emenconsumname", n_emenconsumname_like);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_woname_eq;//[工单]
	public void setN_woname_eq(String n_woname_eq) {
        this.n_woname_eq = n_woname_eq;
        if(!ObjectUtils.isEmpty(this.n_woname_eq)){
            this.getSearchCond().eq("woname", n_woname_eq);
        }
    }
	private String n_woname_like;//[工单]
	public void setN_woname_like(String n_woname_like) {
        this.n_woname_like = n_woname_like;
        if(!ObjectUtils.isEmpty(this.n_woname_like)){
            this.getSearchCond().like("woname", n_woname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_enname_eq;//[能源]
	public void setN_enname_eq(String n_enname_eq) {
        this.n_enname_eq = n_enname_eq;
        if(!ObjectUtils.isEmpty(this.n_enname_eq)){
            this.getSearchCond().eq("enname", n_enname_eq);
        }
    }
	private String n_enname_like;//[能源]
	public void setN_enname_like(String n_enname_like) {
        this.n_enname_like = n_enname_like;
        if(!ObjectUtils.isEmpty(this.n_enname_like)){
            this.getSearchCond().like("enname", n_enname_like);
        }
    }
	private String n_sname_like;//[设备统计归类]
	public void setN_sname_like(String n_sname_like) {
        this.n_sname_like = n_sname_like;
        if(!ObjectUtils.isEmpty(this.n_sname_like)){
            this.getSearchCond().like("sname", n_sname_like);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_woid_eq;//[工单]
	public void setN_woid_eq(String n_woid_eq) {
        this.n_woid_eq = n_woid_eq;
        if(!ObjectUtils.isEmpty(this.n_woid_eq)){
            this.getSearchCond().eq("woid", n_woid_eq);
        }
    }
	private String n_enid_eq;//[能源]
	public void setN_enid_eq(String n_enid_eq) {
        this.n_enid_eq = n_enid_eq;
        if(!ObjectUtils.isEmpty(this.n_enid_eq)){
            this.getSearchCond().eq("enid", n_enid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emenconsumname", query)
            );
		 }
	}
}



