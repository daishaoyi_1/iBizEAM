package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEICamDrop;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamDropSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEICamDropService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEICamDropMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[探头摘除记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEICamDropServiceImpl")
public class EMEICamDropServiceImpl extends ServiceImpl<EMEICamDropMapper, EMEICamDrop> implements IEMEICamDropService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamService emeicamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEICamDrop et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeicamdropid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEICamDrop> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEICamDrop et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeicamdropid", et.getEmeicamdropid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeicamdropid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEICamDrop> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEICamDrop get(String key) {
        EMEICamDrop et = getById(key);
        if(et == null){
            et = new EMEICamDrop();
            et.setEmeicamdropid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEICamDrop getDraft(EMEICamDrop et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEICamDrop et) {
        return (!ObjectUtils.isEmpty(et.getEmeicamdropid())) && (!Objects.isNull(this.getById(et.getEmeicamdropid())));
    }
    @Override
    @Transactional
    public boolean save(EMEICamDrop et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEICamDrop et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEICamDrop> list) {
        list.forEach(item->fillParentData(item));
        List<EMEICamDrop> create = new ArrayList<>();
        List<EMEICamDrop> update = new ArrayList<>();
        for (EMEICamDrop et : list) {
            if (ObjectUtils.isEmpty(et.getEmeicamdropid()) || ObjectUtils.isEmpty(getById(et.getEmeicamdropid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEICamDrop> list) {
        list.forEach(item->fillParentData(item));
        List<EMEICamDrop> create = new ArrayList<>();
        List<EMEICamDrop> update = new ArrayList<>();
        for (EMEICamDrop et : list) {
            if (ObjectUtils.isEmpty(et.getEmeicamdropid()) || ObjectUtils.isEmpty(getById(et.getEmeicamdropid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEICamDrop> selectByEiobjid(String emeicamid) {
        return baseMapper.selectByEiobjid(emeicamid);
    }
    @Override
    public void removeByEiobjid(String emeicamid) {
        this.remove(new QueryWrapper<EMEICamDrop>().eq("eiobjid",emeicamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEICamDrop> searchDefault(EMEICamDropSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEICamDrop> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEICamDrop>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEICamDrop et){
        //实体关系[DER1N_EMEICAMDROP_EMEICAM_EIOBJID]
        if(!ObjectUtils.isEmpty(et.getEiobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEICam eiobj=et.getEiobj();
            if(ObjectUtils.isEmpty(eiobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMEICam majorEntity=emeicamService.get(et.getEiobjid());
                et.setEiobj(majorEntity);
                eiobj=majorEntity;
            }
            et.setEiobjname(eiobj.getEmeicamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEICamDrop> getEmeicamdropByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEICamDrop> getEmeicamdropByEntities(List<EMEICamDrop> entities) {
        List ids =new ArrayList();
        for(EMEICamDrop entity : entities){
            Serializable id=entity.getEmeicamdropid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEICamDropService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



