package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[探头]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEICAM_BASE", resultMap = "EMEICamResultMap")
public class EMEICam extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 探头名称
     */
    @TableField(value = "emeicamname")
    @JSONField(name = "emeicamname")
    @JsonProperty("emeicamname")
    private String emeicamname;
    /**
     * 探头信息
     */
    @TableField(exist = false)
    @JSONField(name = "eicaminfo")
    @JsonProperty("eicaminfo")
    private String eicaminfo;
    /**
     * 型号
     */
    @TableField(value = "eqmodelcode")
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    private String eqmodelcode;
    /**
     * 探头编号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeicamid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeicamid")
    @JsonProperty("emeicamid")
    private String emeicamid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 负责人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 报废原因
     */
    @TableField(value = "disdesc")
    @JSONField(name = "disdesc")
    @JsonProperty("disdesc")
    private String disdesc;
    /**
     * 状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "eistate")
    @JSONField(name = "eistate")
    @JsonProperty("eistate")
    private String eistate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 地址
     */
    @TableField(value = "macaddr")
    @JSONField(name = "macaddr")
    @JsonProperty("macaddr")
    private String macaddr;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 报废日期
     */
    @TableField(value = "disdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "disdate", format = "yyyy-MM-dd")
    @JsonProperty("disdate")
    private Timestamp disdate;
    /**
     * 负责人
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 领料单
     */
    @TableField(exist = false)
    @JSONField(name = "itempusename")
    @JsonProperty("itempusename")
    private String itempusename;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    private String eqlocationname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 领料单
     */
    @TableField(value = "itempuseid")
    @JSONField(name = "itempuseid")
    @JsonProperty("itempuseid")
    private String itempuseid;
    /**
     * 位置
     */
    @TableField(value = "eqlocationid")
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    private String eqlocationid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemPUse itempuse;



    /**
     * 设置 [探头名称]
     */
    public void setEmeicamname(String emeicamname) {
        this.emeicamname = emeicamname;
        this.modify("emeicamname", emeicamname);
    }

    /**
     * 设置 [型号]
     */
    public void setEqmodelcode(String eqmodelcode) {
        this.eqmodelcode = eqmodelcode;
        this.modify("eqmodelcode", eqmodelcode);
    }

    /**
     * 设置 [负责人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [报废原因]
     */
    public void setDisdesc(String disdesc) {
        this.disdesc = disdesc;
        this.modify("disdesc", disdesc);
    }

    /**
     * 设置 [状态]
     */
    public void setEistate(String eistate) {
        this.eistate = eistate;
        this.modify("eistate", eistate);
    }

    /**
     * 设置 [地址]
     */
    public void setMacaddr(String macaddr) {
        this.macaddr = macaddr;
        this.modify("macaddr", macaddr);
    }

    /**
     * 设置 [报废日期]
     */
    public void setDisdate(Timestamp disdate) {
        this.disdate = disdate;
        this.modify("disdate", disdate);
    }

    /**
     * 格式化日期 [报废日期]
     */
    public String formatDisdate() {
        if (this.disdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(disdate);
    }
    /**
     * 设置 [负责人]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [领料单]
     */
    public void setItempuseid(String itempuseid) {
        this.itempuseid = itempuseid;
        this.modify("itempuseid", itempuseid);
    }

    /**
     * 设置 [位置]
     */
    public void setEqlocationid(String eqlocationid) {
        this.eqlocationid = eqlocationid;
        this.modify("eqlocationid", eqlocationid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeicamid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


