package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[订单条目]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPODETAIL_BASE", resultMap = "EMPODetailResultMap")
public class EMPODetail extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 发票号
     */
    @TableField(value = "civo")
    @JSONField(name = "civo")
    @JsonProperty("civo")
    private String civo;
    /**
     * 收货日期
     */
    @TableField(value = "rdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "rdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("rdate")
    private Timestamp rdate;
    /**
     * 验收凭据
     */
    @TableField(value = "yiju")
    @JSONField(name = "yiju")
    @JsonProperty("yiju")
    private String yiju;
    /**
     * 是否为重启单
     */
    @DEField(defaultValue = "0")
    @TableField(value = "isrestart")
    @JSONField(name = "isrestart")
    @JsonProperty("isrestart")
    private Integer isrestart;
    /**
     * 发票存根
     */
    @TableField(value = "civocopy")
    @JSONField(name = "civocopy")
    @JsonProperty("civocopy")
    private String civocopy;
    /**
     * 数量差
     */
    @TableField(exist = false)
    @JSONField(name = "sumdiff")
    @JsonProperty("sumdiff")
    private Double sumdiff;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 收货价差
     */
    @TableField(exist = false)
    @JSONField(name = "pricediff")
    @JsonProperty("pricediff")
    private Double pricediff;
    /**
     * 税率
     */
    @DEField(defaultValue = "0")
    @TableField(value = "taxrate")
    @JSONField(name = "taxrate")
    @JsonProperty("taxrate")
    private Double taxrate;
    /**
     * 均摊关税
     */
    @DEField(defaultValue = "0")
    @TableField(value = "avgtaxfee")
    @JSONField(name = "avgtaxfee")
    @JsonProperty("avgtaxfee")
    private Double avgtaxfee;
    /**
     * 标价
     */
    @TableField(value = "listprice")
    @JSONField(name = "listprice")
    @JsonProperty("listprice")
    private Double listprice;
    /**
     * 物品金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 单价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 单位转换率
     */
    @DEField(defaultValue = "1")
    @TableField(value = "unitrate")
    @JSONField(name = "unitrate")
    @JsonProperty("unitrate")
    private Double unitrate;
    /**
     * 订单条目号
     */
    @DEField(isKeyField = true)
    @TableId(value = "empodetailid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "empodetailid")
    @JsonProperty("empodetailid")
    private String empodetailid;
    /**
     * 物品备注
     */
    @TableField(value = "itemdesc")
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    private String itemdesc;
    /**
     * 税费
     */
    @DEField(defaultValue = "0")
    @TableField(value = "shf")
    @JSONField(name = "shf")
    @JsonProperty("shf")
    private Double shf;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 总价
     */
    @TableField(exist = false)
    @JSONField(name = "totalprice")
    @JsonProperty("totalprice")
    private Double totalprice;
    /**
     * 条目状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "podetailstate")
    @JSONField(name = "podetailstate")
    @JsonProperty("podetailstate")
    private Integer podetailstate;
    /**
     * 收货数量
     */
    @TableField(value = "rsum")
    @JSONField(name = "rsum")
    @JsonProperty("rsum")
    private Double rsum;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 订单条目信息
     */
    @TableField(exist = false)
    @JSONField(name = "podetailinfo")
    @JsonProperty("podetailinfo")
    private String podetailinfo;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * sap税率
     */
    @TableField(value = "sapsl")
    @JSONField(name = "sapsl")
    @JsonProperty("sapsl")
    private String sapsl;
    /**
     * 均摊运杂费
     */
    @DEField(defaultValue = "0")
    @TableField(value = "avgtsfee")
    @JSONField(name = "avgtsfee")
    @JsonProperty("avgtsfee")
    private Double avgtsfee;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 含税总金额
     */
    @TableField(exist = false)
    @JSONField(name = "sumall")
    @JsonProperty("sumall")
    private Double sumall;
    /**
     * 折扣(%)
     */
    @DEField(defaultValue = "100")
    @TableField(value = "discnt")
    @JSONField(name = "discnt")
    @JsonProperty("discnt")
    private Double discnt;
    /**
     * 价格波动提醒
     */
    @TableField(exist = false)
    @JSONField(name = "attprice")
    @JsonProperty("attprice")
    private Integer attprice;
    /**
     * 顺序号
     */
    @TableField(value = "orderflag")
    @JSONField(name = "orderflag")
    @JsonProperty("orderflag")
    private Integer orderflag;
    /**
     * 收货单价
     */
    @TableField(value = "rprice")
    @JSONField(name = "rprice")
    @JsonProperty("rprice")
    private Double rprice;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 订货数量
     */
    @TableField(value = "psum")
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;
    /**
     * 订单条目名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "empodetailname")
    @JSONField(name = "empodetailname")
    @JsonProperty("empodetailname")
    private String empodetailname;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    private String labservicename;
    /**
     * 标准单位
     */
    @TableField(exist = false)
    @JSONField(name = "sunitid")
    @JsonProperty("sunitid")
    private String sunitid;
    /**
     * 用途
     */
    @TableField(exist = false)
    @JSONField(name = "useto")
    @JsonProperty("useto")
    private String useto;
    /**
     * 订单流程步骤
     */
    @TableField(exist = false)
    @JSONField(name = "powfstep")
    @JsonProperty("powfstep")
    private String powfstep;
    /**
     * 标准单位
     */
    @TableField(exist = false)
    @JSONField(name = "sunitname")
    @JsonProperty("sunitname")
    private String sunitname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    private String labserviceid;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 设备集合
     */
    @TableField(exist = false)
    @JSONField(name = "equips")
    @JsonProperty("equips")
    private String equips;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 收货单位
     */
    @TableField(exist = false)
    @JSONField(name = "runitname")
    @JsonProperty("runitname")
    private String runitname;
    /**
     * 物品均价
     */
    @TableField(exist = false)
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    private Double avgprice;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    private String itembtypeid;
    /**
     * 采购申请
     */
    @TableField(exist = false)
    @JSONField(name = "wplistname")
    @JsonProperty("wplistname")
    private String wplistname;
    /**
     * 申请班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    private String teamid;
    /**
     * 订单状态
     */
    @TableField(exist = false)
    @JSONField(name = "postate")
    @JsonProperty("postate")
    private Integer postate;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 订单
     */
    @TableField(exist = false)
    @JSONField(name = "poname")
    @JsonProperty("poname")
    private String poname;
    /**
     * 订货单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    private String unitname;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 订货单位
     */
    @TableField(value = "unitid")
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    private String unitid;
    /**
     * 订单
     */
    @TableField(value = "poid")
    @JSONField(name = "poid")
    @JsonProperty("poid")
    private String poid;
    /**
     * 收货单位
     */
    @TableField(value = "runitid")
    @JSONField(name = "runitid")
    @JsonProperty("runitid")
    private String runitid;
    /**
     * 采购申请
     */
    @TableField(value = "wplistid")
    @JSONField(name = "wplistid")
    @JsonProperty("wplistid")
    private String wplistid;
    /**
     * 收货人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 收货人
     */
    @TableField(exist = false)
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 订单采购员
     */
    @TableField(exist = false)
    @JSONField(name = "porempname")
    @JsonProperty("porempname")
    private String porempname;
    /**
     * 订单采购员
     */
    @TableField(exist = false)
    @JSONField(name = "porempid")
    @JsonProperty("porempid")
    private String porempid;
    /**
     * 记账人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 记账人
     */
    @TableField(exist = false)
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPO po;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWPList wplist;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pferempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFUnit runit;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFUnit unit;



    /**
     * 设置 [发票号]
     */
    public void setCivo(String civo) {
        this.civo = civo;
        this.modify("civo", civo);
    }

    /**
     * 设置 [收货日期]
     */
    public void setRdate(Timestamp rdate) {
        this.rdate = rdate;
        this.modify("rdate", rdate);
    }

    /**
     * 格式化日期 [收货日期]
     */
    public String formatRdate() {
        if (this.rdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(rdate);
    }
    /**
     * 设置 [验收凭据]
     */
    public void setYiju(String yiju) {
        this.yiju = yiju;
        this.modify("yiju", yiju);
    }

    /**
     * 设置 [是否为重启单]
     */
    public void setIsrestart(Integer isrestart) {
        this.isrestart = isrestart;
        this.modify("isrestart", isrestart);
    }

    /**
     * 设置 [发票存根]
     */
    public void setCivocopy(String civocopy) {
        this.civocopy = civocopy;
        this.modify("civocopy", civocopy);
    }

    /**
     * 设置 [税率]
     */
    public void setTaxrate(Double taxrate) {
        this.taxrate = taxrate;
        this.modify("taxrate", taxrate);
    }

    /**
     * 设置 [均摊关税]
     */
    public void setAvgtaxfee(Double avgtaxfee) {
        this.avgtaxfee = avgtaxfee;
        this.modify("avgtaxfee", avgtaxfee);
    }

    /**
     * 设置 [标价]
     */
    public void setListprice(Double listprice) {
        this.listprice = listprice;
        this.modify("listprice", listprice);
    }

    /**
     * 设置 [物品金额]
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [单位转换率]
     */
    public void setUnitrate(Double unitrate) {
        this.unitrate = unitrate;
        this.modify("unitrate", unitrate);
    }

    /**
     * 设置 [物品备注]
     */
    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
        this.modify("itemdesc", itemdesc);
    }

    /**
     * 设置 [税费]
     */
    public void setShf(Double shf) {
        this.shf = shf;
        this.modify("shf", shf);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [条目状态]
     */
    public void setPodetailstate(Integer podetailstate) {
        this.podetailstate = podetailstate;
        this.modify("podetailstate", podetailstate);
    }

    /**
     * 设置 [收货数量]
     */
    public void setRsum(Double rsum) {
        this.rsum = rsum;
        this.modify("rsum", rsum);
    }

    /**
     * 设置 [sap税率]
     */
    public void setSapsl(String sapsl) {
        this.sapsl = sapsl;
        this.modify("sapsl", sapsl);
    }

    /**
     * 设置 [均摊运杂费]
     */
    public void setAvgtsfee(Double avgtsfee) {
        this.avgtsfee = avgtsfee;
        this.modify("avgtsfee", avgtsfee);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [折扣(%)]
     */
    public void setDiscnt(Double discnt) {
        this.discnt = discnt;
        this.modify("discnt", discnt);
    }

    /**
     * 设置 [顺序号]
     */
    public void setOrderflag(Integer orderflag) {
        this.orderflag = orderflag;
        this.modify("orderflag", orderflag);
    }

    /**
     * 设置 [收货单价]
     */
    public void setRprice(Double rprice) {
        this.rprice = rprice;
        this.modify("rprice", rprice);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [订货数量]
     */
    public void setPsum(Double psum) {
        this.psum = psum;
        this.modify("psum", psum);
    }

    /**
     * 设置 [订单条目名称]
     */
    public void setEmpodetailname(String empodetailname) {
        this.empodetailname = empodetailname;
        this.modify("empodetailname", empodetailname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [订货单位]
     */
    public void setUnitid(String unitid) {
        this.unitid = unitid;
        this.modify("unitid", unitid);
    }

    /**
     * 设置 [订单]
     */
    public void setPoid(String poid) {
        this.poid = poid;
        this.modify("poid", poid);
    }

    /**
     * 设置 [收货单位]
     */
    public void setRunitid(String runitid) {
        this.runitid = runitid;
        this.modify("runitid", runitid);
    }

    /**
     * 设置 [采购申请]
     */
    public void setWplistid(String wplistid) {
        this.wplistid = wplistid;
        this.modify("wplistid", wplistid);
    }

    /**
     * 设置 [收货人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [记账人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("empodetailid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


