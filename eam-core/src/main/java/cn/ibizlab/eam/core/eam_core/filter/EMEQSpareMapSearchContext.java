package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareMap;
/**
 * 关系型数据实体[EMEQSpareMap] 查询条件对象
 */
@Slf4j
@Data
public class EMEQSpareMapSearchContext extends QueryWrapperContext<EMEQSpareMap> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emeqsparemapname_like;//[备件包引用]
	public void setN_emeqsparemapname_like(String n_emeqsparemapname_like) {
        this.n_emeqsparemapname_like = n_emeqsparemapname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqsparemapname_like)){
            this.getSearchCond().like("emeqsparemapname", n_emeqsparemapname_like);
        }
    }
	private String n_refobjname_eq;//[引用对象]
	public void setN_refobjname_eq(String n_refobjname_eq) {
        this.n_refobjname_eq = n_refobjname_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjname_eq)){
            this.getSearchCond().eq("refobjname", n_refobjname_eq);
        }
    }
	private String n_refobjname_like;//[引用对象]
	public void setN_refobjname_like(String n_refobjname_like) {
        this.n_refobjname_like = n_refobjname_like;
        if(!ObjectUtils.isEmpty(this.n_refobjname_like)){
            this.getSearchCond().like("refobjname", n_refobjname_like);
        }
    }
	private String n_eqsparename_eq;//[备件包]
	public void setN_eqsparename_eq(String n_eqsparename_eq) {
        this.n_eqsparename_eq = n_eqsparename_eq;
        if(!ObjectUtils.isEmpty(this.n_eqsparename_eq)){
            this.getSearchCond().eq("eqsparename", n_eqsparename_eq);
        }
    }
	private String n_eqsparename_like;//[备件包]
	public void setN_eqsparename_like(String n_eqsparename_like) {
        this.n_eqsparename_like = n_eqsparename_like;
        if(!ObjectUtils.isEmpty(this.n_eqsparename_like)){
            this.getSearchCond().like("eqsparename", n_eqsparename_like);
        }
    }
	private String n_refobjid_eq;//[对象标识]
	public void setN_refobjid_eq(String n_refobjid_eq) {
        this.n_refobjid_eq = n_refobjid_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjid_eq)){
            this.getSearchCond().eq("refobjid", n_refobjid_eq);
        }
    }
	private String n_eqspareid_eq;//[备件包]
	public void setN_eqspareid_eq(String n_eqspareid_eq) {
        this.n_eqspareid_eq = n_eqspareid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqspareid_eq)){
            this.getSearchCond().eq("eqspareid", n_eqspareid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqsparemapname", query)
            );
		 }
	}
}



