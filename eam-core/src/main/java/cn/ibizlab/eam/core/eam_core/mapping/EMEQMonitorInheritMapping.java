

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMonitor;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQMonitorInheritMapping {

    @Mappings({
        @Mapping(source ="emeqmonitorid",target = "emdprctid"),
        @Mapping(source ="emeqmonitorname",target = "emdprctname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="equipname",target = "dpname"),
        @Mapping(source ="equipid",target = "dpid"),
    })
    EMDPRCT toEmdprct(EMEQMonitor minorEntity);

    @Mappings({
        @Mapping(source ="emdprctid" ,target = "emeqmonitorid"),
        @Mapping(source ="emdprctname" ,target = "emeqmonitorname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="dpname",target = "equipname"),
        @Mapping(source ="dpid",target = "equipid"),
    })
    EMEQMonitor toEmeqmonitor(EMDPRCT majorEntity);

    List<EMDPRCT> toEmdprct(List<EMEQMonitor> minorEntities);

    List<EMEQMonitor> toEmeqmonitor(List<EMDPRCT> majorEntities);

}


