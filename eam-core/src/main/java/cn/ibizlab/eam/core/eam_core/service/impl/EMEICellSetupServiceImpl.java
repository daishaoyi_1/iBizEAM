package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEICellSetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellSetupSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEICellSetupService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEICellSetupMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[对讲机分发记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEICellSetupServiceImpl")
public class EMEICellSetupServiceImpl extends ServiceImpl<EMEICellSetupMapper, EMEICellSetup> implements IEMEICellSetupService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICellService emeicellService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService emeqlocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEICellSetup et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeicellsetupid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEICellSetup> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEICellSetup et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeicellsetupid", et.getEmeicellsetupid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeicellsetupid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEICellSetup> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEICellSetup get(String key) {
        EMEICellSetup et = getById(key);
        if(et == null){
            et = new EMEICellSetup();
            et.setEmeicellsetupid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEICellSetup getDraft(EMEICellSetup et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEICellSetup et) {
        return (!ObjectUtils.isEmpty(et.getEmeicellsetupid())) && (!Objects.isNull(this.getById(et.getEmeicellsetupid())));
    }
    @Override
    @Transactional
    public boolean save(EMEICellSetup et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEICellSetup et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEICellSetup> list) {
        list.forEach(item->fillParentData(item));
        List<EMEICellSetup> create = new ArrayList<>();
        List<EMEICellSetup> update = new ArrayList<>();
        for (EMEICellSetup et : list) {
            if (ObjectUtils.isEmpty(et.getEmeicellsetupid()) || ObjectUtils.isEmpty(getById(et.getEmeicellsetupid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEICellSetup> list) {
        list.forEach(item->fillParentData(item));
        List<EMEICellSetup> create = new ArrayList<>();
        List<EMEICellSetup> update = new ArrayList<>();
        for (EMEICellSetup et : list) {
            if (ObjectUtils.isEmpty(et.getEmeicellsetupid()) || ObjectUtils.isEmpty(getById(et.getEmeicellsetupid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEICellSetup> selectByEiobjid(String emeicellid) {
        return baseMapper.selectByEiobjid(emeicellid);
    }
    @Override
    public void removeByEiobjid(String emeicellid) {
        this.remove(new QueryWrapper<EMEICellSetup>().eq("eiobjid",emeicellid));
    }

	@Override
    public List<EMEICellSetup> selectByEqlocationid(String emeqlocationid) {
        return baseMapper.selectByEqlocationid(emeqlocationid);
    }
    @Override
    public void removeByEqlocationid(String emeqlocationid) {
        this.remove(new QueryWrapper<EMEICellSetup>().eq("eqlocationid",emeqlocationid));
    }

	@Override
    public List<EMEICellSetup> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEICellSetup>().eq("equipid",emequipid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEICellSetup> searchDefault(EMEICellSetupSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEICellSetup> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEICellSetup>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEICellSetup et){
        //实体关系[DER1N_EMEICELLSETUP_EMEICELL_EIOBJID]
        if(!ObjectUtils.isEmpty(et.getEiobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEICell eiobj=et.getEiobj();
            if(ObjectUtils.isEmpty(eiobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMEICell majorEntity=emeicellService.get(et.getEiobjid());
                et.setEiobj(majorEntity);
                eiobj=majorEntity;
            }
            et.setEiobjname(eiobj.getEmeicellname());
        }
        //实体关系[DER1N_EMEICELLSETUP_EMEQLOCATION_EQLOCATIONID]
        if(!ObjectUtils.isEmpty(et.getEqlocationid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation=et.getEqlocation();
            if(ObjectUtils.isEmpty(eqlocation)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQLocation majorEntity=emeqlocationService.get(et.getEqlocationid());
                et.setEqlocation(majorEntity);
                eqlocation=majorEntity;
            }
            et.setEqlocationname(eqlocation.getEqlocationinfo());
        }
        //实体关系[DER1N_EMEICELLSETUP_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEICellSetup> getEmeicellsetupByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEICellSetup> getEmeicellsetupByEntities(List<EMEICellSetup> entities) {
        List ids =new ArrayList();
        for(EMEICellSetup entity : entities){
            Serializable id=entity.getEmeicellsetupid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEICellSetupService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



