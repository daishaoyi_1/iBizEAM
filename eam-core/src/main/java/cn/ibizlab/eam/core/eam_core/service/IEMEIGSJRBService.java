package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEIGSJRB;
import cn.ibizlab.eam.core.eam_core.filter.EMEIGSJRBSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEIGSJRB] 服务对象接口
 */
public interface IEMEIGSJRBService extends IService<EMEIGSJRB> {

    boolean create(EMEIGSJRB et);
    void createBatch(List<EMEIGSJRB> list);
    boolean update(EMEIGSJRB et);
    void updateBatch(List<EMEIGSJRB> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEIGSJRB get(String key);
    EMEIGSJRB getDraft(EMEIGSJRB et);
    boolean checkKey(EMEIGSJRB et);
    boolean save(EMEIGSJRB et);
    void saveBatch(List<EMEIGSJRB> list);
    Page<EMEIGSJRB> searchDefault(EMEIGSJRBSearchContext context);
    List<EMEIGSJRB> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEIGSJRB> getEmeigsjrbByIds(List<String> ids);
    List<EMEIGSJRB> getEmeigsjrbByEntities(List<EMEIGSJRB> entities);
}


