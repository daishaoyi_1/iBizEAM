package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[资产类别]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMASSETCLASS_BASE", resultMap = "EMAssetClassResultMap")
public class EMAssetClass extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 资产科目代码
     */
    @TableField(value = "assetclasscode")
    @JSONField(name = "assetclasscode")
    @JsonProperty("assetclasscode")
    private String assetclasscode;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * RESERVER
     */
    @TableField(value = "reserver")
    @JSONField(name = "reserver")
    @JsonProperty("reserver")
    private String reserver;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * RESERVER2
     */
    @TableField(value = "reserver2")
    @JSONField(name = "reserver2")
    @JsonProperty("reserver2")
    private String reserver2;
    /**
     * 资产类别名称
     */
    @TableField(value = "emassetclassname")
    @JSONField(name = "emassetclassname")
    @JsonProperty("emassetclassname")
    private String emassetclassname;
    /**
     * 固定资产科目分类
     */
    @TableField(value = "assetclassgroup")
    @JSONField(name = "assetclassgroup")
    @JsonProperty("assetclassgroup")
    private String assetclassgroup;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 资产类别标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emassetclassid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emassetclassid")
    @JsonProperty("emassetclassid")
    private String emassetclassid;
    /**
     * 折旧期
     */
    @TableField(value = "life")
    @JSONField(name = "life")
    @JsonProperty("life")
    private Double life;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 资产科目信息
     */
    @TableField(exist = false)
    @JSONField(name = "assetclassinfo")
    @JsonProperty("assetclassinfo")
    private String assetclassinfo;
    /**
     * 上级科目
     */
    @TableField(exist = false)
    @JSONField(name = "assetclasspname")
    @JsonProperty("assetclasspname")
    private String assetclasspname;
    /**
     * 上级科目代码
     */
    @TableField(exist = false)
    @JSONField(name = "assetclasspcode")
    @JsonProperty("assetclasspcode")
    private String assetclasspcode;
    /**
     * 上级科目编号
     */
    @TableField(value = "assetclasspid")
    @JSONField(name = "assetclasspid")
    @JsonProperty("assetclasspid")
    private String assetclasspid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMAssetClass assetclassp;



    /**
     * 设置 [资产科目代码]
     */
    public void setAssetclasscode(String assetclasscode) {
        this.assetclasscode = assetclasscode;
        this.modify("assetclasscode", assetclasscode);
    }

    /**
     * 设置 [RESERVER]
     */
    public void setReserver(String reserver) {
        this.reserver = reserver;
        this.modify("reserver", reserver);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [RESERVER2]
     */
    public void setReserver2(String reserver2) {
        this.reserver2 = reserver2;
        this.modify("reserver2", reserver2);
    }

    /**
     * 设置 [资产类别名称]
     */
    public void setEmassetclassname(String emassetclassname) {
        this.emassetclassname = emassetclassname;
        this.modify("emassetclassname", emassetclassname);
    }

    /**
     * 设置 [固定资产科目分类]
     */
    public void setAssetclassgroup(String assetclassgroup) {
        this.assetclassgroup = assetclassgroup;
        this.modify("assetclassgroup", assetclassgroup);
    }

    /**
     * 设置 [折旧期]
     */
    public void setLife(Double life) {
        this.life = life;
        this.modify("life", life);
    }

    /**
     * 设置 [上级科目编号]
     */
    public void setAssetclasspid(String assetclasspid) {
        this.assetclasspid = assetclasspid;
        this.modify("assetclasspid", assetclasspid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emassetclassid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


