package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[项目招投标]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMBIDDING_BASE", resultMap = "EMBiddingResultMap")
public class EMBidding extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 项目招投标标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "embiddingid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "embiddingid")
    @JsonProperty("embiddingid")
    private String embiddingid;
    /**
     * 技术参数
     */
    @TableField(value = "parameter")
    @JSONField(name = "parameter")
    @JsonProperty("parameter")
    private String parameter;
    /**
     * 资质
     */
    @TableField(value = "qualification")
    @JSONField(name = "qualification")
    @JsonProperty("qualification")
    private String qualification;
    /**
     * 价格
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private String price;
    /**
     * 业绩
     */
    @TableField(value = "achievement")
    @JSONField(name = "achievement")
    @JsonProperty("achievement")
    private String achievement;
    /**
     * 项目招投标名称
     */
    @TableField(value = "embiddingname")
    @JSONField(name = "embiddingname")
    @JsonProperty("embiddingname")
    private String embiddingname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    private String emservicename;
    /**
     * 服务商
     */
    @TableField(value = "emserviceid")
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    private String emserviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService emservice;



    /**
     * 设置 [技术参数]
     */
    public void setParameter(String parameter) {
        this.parameter = parameter;
        this.modify("parameter", parameter);
    }

    /**
     * 设置 [资质]
     */
    public void setQualification(String qualification) {
        this.qualification = qualification;
        this.modify("qualification", qualification);
    }

    /**
     * 设置 [价格]
     */
    public void setPrice(String price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [业绩]
     */
    public void setAchievement(String achievement) {
        this.achievement = achievement;
        this.modify("achievement", achievement);
    }

    /**
     * 设置 [项目招投标名称]
     */
    public void setEmbiddingname(String embiddingname) {
        this.embiddingname = embiddingname;
        this.modify("embiddingname", embiddingname);
    }

    /**
     * 设置 [服务商]
     */
    public void setEmserviceid(String emserviceid) {
        this.emserviceid = emserviceid;
        this.modify("emserviceid", emserviceid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("embiddingid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


