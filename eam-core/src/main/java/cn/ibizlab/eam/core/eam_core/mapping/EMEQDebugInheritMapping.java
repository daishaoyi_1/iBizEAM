

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQDebug;
import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQDebugInheritMapping {

    @Mappings({
        @Mapping(source ="emeqdebugid",target = "emeqahid"),
        @Mapping(source ="emeqdebugname",target = "emeqahname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQAH toEmeqah(EMEQDebug minorEntity);

    @Mappings({
        @Mapping(source ="emeqahid" ,target = "emeqdebugid"),
        @Mapping(source ="emeqahname" ,target = "emeqdebugname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQDebug toEmeqdebug(EMEQAH majorEntity);

    List<EMEQAH> toEmeqah(List<EMEQDebug> minorEntities);

    List<EMEQDebug> toEmeqdebug(List<EMEQAH> majorEntities);

}


