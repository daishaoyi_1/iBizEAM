package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatterySetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatterySetupSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEIBatterySetupService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEIBatterySetupMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[电瓶换装记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEIBatterySetupServiceImpl")
public class EMEIBatterySetupServiceImpl extends ServiceImpl<EMEIBatterySetupMapper, EMEIBatterySetup> implements IEMEIBatterySetupService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryService emeibatteryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService emeqlocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEIBatterySetup et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeibatterysetupid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEIBatterySetup> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEIBatterySetup et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeibatterysetupid", et.getEmeibatterysetupid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeibatterysetupid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEIBatterySetup> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEIBatterySetup get(String key) {
        EMEIBatterySetup et = getById(key);
        if(et == null){
            et = new EMEIBatterySetup();
            et.setEmeibatterysetupid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEIBatterySetup getDraft(EMEIBatterySetup et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEIBatterySetup et) {
        return (!ObjectUtils.isEmpty(et.getEmeibatterysetupid())) && (!Objects.isNull(this.getById(et.getEmeibatterysetupid())));
    }
    @Override
    @Transactional
    public boolean save(EMEIBatterySetup et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEIBatterySetup et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEIBatterySetup> list) {
        list.forEach(item->fillParentData(item));
        List<EMEIBatterySetup> create = new ArrayList<>();
        List<EMEIBatterySetup> update = new ArrayList<>();
        for (EMEIBatterySetup et : list) {
            if (ObjectUtils.isEmpty(et.getEmeibatterysetupid()) || ObjectUtils.isEmpty(getById(et.getEmeibatterysetupid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEIBatterySetup> list) {
        list.forEach(item->fillParentData(item));
        List<EMEIBatterySetup> create = new ArrayList<>();
        List<EMEIBatterySetup> update = new ArrayList<>();
        for (EMEIBatterySetup et : list) {
            if (ObjectUtils.isEmpty(et.getEmeibatterysetupid()) || ObjectUtils.isEmpty(getById(et.getEmeibatterysetupid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEIBatterySetup> selectByEiobjid(String emeibatteryid) {
        return baseMapper.selectByEiobjid(emeibatteryid);
    }
    @Override
    public void removeByEiobjid(String emeibatteryid) {
        this.remove(new QueryWrapper<EMEIBatterySetup>().eq("eiobjid",emeibatteryid));
    }

	@Override
    public List<EMEIBatterySetup> selectByEiobjnid(String emeibatteryid) {
        return baseMapper.selectByEiobjnid(emeibatteryid);
    }
    @Override
    public void removeByEiobjnid(String emeibatteryid) {
        this.remove(new QueryWrapper<EMEIBatterySetup>().eq("eiobjnid",emeibatteryid));
    }

	@Override
    public List<EMEIBatterySetup> selectByEqlocationid(String emeqlocationid) {
        return baseMapper.selectByEqlocationid(emeqlocationid);
    }
    @Override
    public void removeByEqlocationid(String emeqlocationid) {
        this.remove(new QueryWrapper<EMEIBatterySetup>().eq("eqlocationid",emeqlocationid));
    }

	@Override
    public List<EMEIBatterySetup> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEIBatterySetup>().eq("equipid",emequipid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEIBatterySetup> searchDefault(EMEIBatterySetupSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEIBatterySetup> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEIBatterySetup>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEIBatterySetup et){
        //实体关系[DER1N_EMEIBATTERYSETUP_EMEIBATTERY_EIOBJID]
        if(!ObjectUtils.isEmpty(et.getEiobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEIBattery eiobj=et.getEiobj();
            if(ObjectUtils.isEmpty(eiobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMEIBattery majorEntity=emeibatteryService.get(et.getEiobjid());
                et.setEiobj(majorEntity);
                eiobj=majorEntity;
            }
            et.setEiobjname(eiobj.getEmeibatteryname());
        }
        //实体关系[DER1N_EMEIBATTERYSETUP_EMEIBATTERY_EIOBJNID]
        if(!ObjectUtils.isEmpty(et.getEiobjnid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEIBattery eiobjn=et.getEiobjn();
            if(ObjectUtils.isEmpty(eiobjn)){
                cn.ibizlab.eam.core.eam_core.domain.EMEIBattery majorEntity=emeibatteryService.get(et.getEiobjnid());
                et.setEiobjn(majorEntity);
                eiobjn=majorEntity;
            }
            et.setEiobjnname(eiobjn.getEmeibatteryname());
        }
        //实体关系[DER1N_EMEIBATTERYSETUP_EMEQLOCATION_EQLOCATIONID]
        if(!ObjectUtils.isEmpty(et.getEqlocationid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation=et.getEqlocation();
            if(ObjectUtils.isEmpty(eqlocation)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQLocation majorEntity=emeqlocationService.get(et.getEqlocationid());
                et.setEqlocation(majorEntity);
                eqlocation=majorEntity;
            }
            et.setEqlocationname(eqlocation.getEqlocationinfo());
        }
        //实体关系[DER1N_EMEIBATTERYSETUP_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEIBatterySetup> getEmeibatterysetupByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEIBatterySetup> getEmeibatterysetupByEntities(List<EMEIBatterySetup> entities) {
        List ids =new ArrayList();
        for(EMEIBatterySetup entity : entities){
            Serializable id=entity.getEmeibatterysetupid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEIBatterySetupService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



