package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMAsset;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMAssetMapper extends BaseMapper<EMAsset> {

    Page<EMAsset> searchAssetBf(IPage page, @Param("srf") EMAssetSearchContext context, @Param("ew") Wrapper<EMAsset> wrapper);
    Page<EMAsset> searchDefault(IPage page, @Param("srf") EMAssetSearchContext context, @Param("ew") Wrapper<EMAsset> wrapper);
    @Override
    EMAsset selectById(Serializable id);
    @Override
    int insert(EMAsset entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMAsset entity);
    @Override
    int update(@Param(Constants.ENTITY) EMAsset entity, @Param("ew") Wrapper<EMAsset> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMAsset> selectByAcclassid(@Param("emacclassid") Serializable emacclassid);

    List<EMAsset> selectByAssetclassid(@Param("emassetclassid") Serializable emassetclassid);

    List<EMAsset> selectByEqlocationid(@Param("emeqlocationid") Serializable emeqlocationid);

    List<EMAsset> selectByLabserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMAsset> selectByMserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMAsset> selectByRserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMAsset> selectByContractid(@Param("pfcontractid") Serializable pfcontractid);

    List<EMAsset> selectByUnitid(@Param("pfunitid") Serializable pfunitid);

}
