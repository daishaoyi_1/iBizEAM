package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[货架]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMCAB_BASE", resultMap = "EMCabResultMap")
public class EMCab extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ARG2
     */
    @TableField(value = "arg2")
    @JSONField(name = "arg2")
    @JsonProperty("arg2")
    private Integer arg2;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 货架管理
     */
    @TableField(value = "storepartgl")
    @JSONField(name = "storepartgl")
    @JsonProperty("storepartgl")
    private String storepartgl;
    /**
     * ARG1
     */
    @TableField(value = "arg1")
    @JSONField(name = "arg1")
    @JsonProperty("arg1")
    private Integer arg1;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * ARG0
     */
    @TableField(value = "arg0")
    @JSONField(name = "arg0")
    @JsonProperty("arg0")
    private Integer arg0;
    /**
     * 货架标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emcabid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emcabid")
    @JsonProperty("emcabid")
    private String emcabid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 货架名称
     */
    @TableField(value = "emcabname")
    @JSONField(name = "emcabname")
    @JsonProperty("emcabname")
    private String emcabname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * GRAPHPARAM
     */
    @TableField(value = "graphparam")
    @JSONField(name = "graphparam")
    @JsonProperty("graphparam")
    private Integer graphparam;
    /**
     * 库位
     */
    @TableField(exist = false)
    @JSONField(name = "emstorepartname")
    @JsonProperty("emstorepartname")
    private String emstorepartname;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "emstorename")
    @JsonProperty("emstorename")
    private String emstorename;
    /**
     * 库位
     */
    @TableField(value = "emstorepartid")
    @JSONField(name = "emstorepartid")
    @JsonProperty("emstorepartid")
    private String emstorepartid;
    /**
     * 仓库
     */
    @TableField(value = "emstoreid")
    @JSONField(name = "emstoreid")
    @JsonProperty("emstoreid")
    private String emstoreid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStorePart emstorepart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStore emstore;



    /**
     * 设置 [ARG2]
     */
    public void setArg2(Integer arg2) {
        this.arg2 = arg2;
        this.modify("arg2", arg2);
    }

    /**
     * 设置 [货架管理]
     */
    public void setStorepartgl(String storepartgl) {
        this.storepartgl = storepartgl;
        this.modify("storepartgl", storepartgl);
    }

    /**
     * 设置 [ARG1]
     */
    public void setArg1(Integer arg1) {
        this.arg1 = arg1;
        this.modify("arg1", arg1);
    }

    /**
     * 设置 [ARG0]
     */
    public void setArg0(Integer arg0) {
        this.arg0 = arg0;
        this.modify("arg0", arg0);
    }

    /**
     * 设置 [货架名称]
     */
    public void setEmcabname(String emcabname) {
        this.emcabname = emcabname;
        this.modify("emcabname", emcabname);
    }

    /**
     * 设置 [GRAPHPARAM]
     */
    public void setGraphparam(Integer graphparam) {
        this.graphparam = graphparam;
        this.modify("graphparam", graphparam);
    }

    /**
     * 设置 [库位]
     */
    public void setEmstorepartid(String emstorepartid) {
        this.emstorepartid = emstorepartid;
        this.modify("emstorepartid", emstorepartid);
    }

    /**
     * 设置 [仓库]
     */
    public void setEmstoreid(String emstoreid) {
        this.emstoreid = emstoreid;
        this.modify("emstoreid", emstoreid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emcabid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


