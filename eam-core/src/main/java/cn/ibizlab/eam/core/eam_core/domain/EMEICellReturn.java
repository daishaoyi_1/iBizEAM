package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[对讲机归还记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEICELLRETURN_BASE", resultMap = "EMEICellReturnResultMap")
public class EMEICellReturn extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 对讲机归还记录标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeicellreturnid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeicellreturnid")
    @JsonProperty("emeicellreturnid")
    private String emeicellreturnid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 归还信息
     */
    @TableField(exist = false)
    @JSONField(name = "activeinfo")
    @JsonProperty("activeinfo")
    private String activeinfo;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 对讲机归还记录名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emeicellreturnname")
    @JSONField(name = "emeicellreturnname")
    @JsonProperty("emeicellreturnname")
    private String emeicellreturnname;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 归还记录
     */
    @TableField(value = "activedesc")
    @JSONField(name = "activedesc")
    @JsonProperty("activedesc")
    private String activedesc;
    /**
     * 归还人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 归还人
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 归还时间
     */
    @TableField(value = "activedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "activedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activedate")
    private Timestamp activedate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 对讲机
     */
    @TableField(exist = false)
    @JSONField(name = "eiobjname")
    @JsonProperty("eiobjname")
    private String eiobjname;
    /**
     * 对讲机
     */
    @TableField(value = "eiobjid")
    @JSONField(name = "eiobjid")
    @JsonProperty("eiobjid")
    private String eiobjid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEICell eiobj;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [对讲机归还记录名称]
     */
    public void setEmeicellreturnname(String emeicellreturnname) {
        this.emeicellreturnname = emeicellreturnname;
        this.modify("emeicellreturnname", emeicellreturnname);
    }

    /**
     * 设置 [归还记录]
     */
    public void setActivedesc(String activedesc) {
        this.activedesc = activedesc;
        this.modify("activedesc", activedesc);
    }

    /**
     * 设置 [归还人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [归还人]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [归还时间]
     */
    public void setActivedate(Timestamp activedate) {
        this.activedate = activedate;
        this.modify("activedate", activedate);
    }

    /**
     * 格式化日期 [归还时间]
     */
    public String formatActivedate() {
        if (this.activedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activedate);
    }
    /**
     * 设置 [对讲机]
     */
    public void setEiobjid(String eiobjid) {
        this.eiobjid = eiobjid;
        this.modify("eiobjid", eiobjid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeicellreturnid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


