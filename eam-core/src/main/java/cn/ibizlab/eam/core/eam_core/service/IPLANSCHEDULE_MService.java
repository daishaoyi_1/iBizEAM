package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_M;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_MSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PLANSCHEDULE_M] 服务对象接口
 */
public interface IPLANSCHEDULE_MService extends IService<PLANSCHEDULE_M> {

    boolean create(PLANSCHEDULE_M et);
    void createBatch(List<PLANSCHEDULE_M> list);
    boolean update(PLANSCHEDULE_M et);
    void updateBatch(List<PLANSCHEDULE_M> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PLANSCHEDULE_M get(String key);
    PLANSCHEDULE_M getDraft(PLANSCHEDULE_M et);
    boolean checkKey(PLANSCHEDULE_M et);
    boolean save(PLANSCHEDULE_M et);
    void saveBatch(List<PLANSCHEDULE_M> list);
    Page<PLANSCHEDULE_M> searchDefault(PLANSCHEDULE_MSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PLANSCHEDULE_M> getPlanscheduleMByIds(List<String> ids);
    List<PLANSCHEDULE_M> getPlanscheduleMByEntities(List<PLANSCHEDULE_M> entities);
}


