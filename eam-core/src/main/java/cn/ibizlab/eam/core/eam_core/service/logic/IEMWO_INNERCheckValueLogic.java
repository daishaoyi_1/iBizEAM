package cn.ibizlab.eam.core.eam_core.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.eam.core.eam_core.domain.EMWO_INNER;

/**
 * 关系型数据实体[CheckValue] 对象
 */
public interface IEMWO_INNERCheckValueLogic {

    void execute(EMWO_INNER et) ;

}
