

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanDetail;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMPlanDetailInheritMapping {

    @Mappings({
        @Mapping(source ="emplandetailid",target = "emresrefobjid"),
        @Mapping(source ="emplandetailname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="planid",target = "resrefobjpid"),
    })
    EMResRefObj toEmresrefobj(EMPlanDetail minorEntity);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emplandetailid"),
        @Mapping(source ="emresrefobjname" ,target = "emplandetailname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="resrefobjpid",target = "planid"),
    })
    EMPlanDetail toEmplandetail(EMResRefObj majorEntity);

    List<EMResRefObj> toEmresrefobj(List<EMPlanDetail> minorEntities);

    List<EMPlanDetail> toEmplandetail(List<EMResRefObj> majorEntities);

}


