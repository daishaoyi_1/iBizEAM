package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanCDT;
/**
 * 关系型数据实体[EMPlanCDT] 查询条件对象
 */
@Slf4j
@Data
public class EMPlanCDTSearchContext extends QueryWrapperContext<EMPlanCDT> {

	private String n_dpvaltype_eq;//[测点值类型]
	public void setN_dpvaltype_eq(String n_dpvaltype_eq) {
        this.n_dpvaltype_eq = n_dpvaltype_eq;
        if(!ObjectUtils.isEmpty(this.n_dpvaltype_eq)){
            this.getSearchCond().eq("dpvaltype", n_dpvaltype_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_triggerdp_eq;//[触发操作]
	public void setN_triggerdp_eq(String n_triggerdp_eq) {
        this.n_triggerdp_eq = n_triggerdp_eq;
        if(!ObjectUtils.isEmpty(this.n_triggerdp_eq)){
            this.getSearchCond().eq("triggerdp", n_triggerdp_eq);
        }
    }
	private String n_emplancdtname_like;//[计划条件名称]
	public void setN_emplancdtname_like(String n_emplancdtname_like) {
        this.n_emplancdtname_like = n_emplancdtname_like;
        if(!ObjectUtils.isEmpty(this.n_emplancdtname_like)){
            this.getSearchCond().like("emplancdtname", n_emplancdtname_like);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_dpname_eq;//[测点]
	public void setN_dpname_eq(String n_dpname_eq) {
        this.n_dpname_eq = n_dpname_eq;
        if(!ObjectUtils.isEmpty(this.n_dpname_eq)){
            this.getSearchCond().eq("dpname", n_dpname_eq);
        }
    }
	private String n_dpname_like;//[测点]
	public void setN_dpname_like(String n_dpname_like) {
        this.n_dpname_like = n_dpname_like;
        if(!ObjectUtils.isEmpty(this.n_dpname_like)){
            this.getSearchCond().like("dpname", n_dpname_like);
        }
    }
	private String n_planname_eq;//[计划]
	public void setN_planname_eq(String n_planname_eq) {
        this.n_planname_eq = n_planname_eq;
        if(!ObjectUtils.isEmpty(this.n_planname_eq)){
            this.getSearchCond().eq("planname", n_planname_eq);
        }
    }
	private String n_planname_like;//[计划]
	public void setN_planname_like(String n_planname_like) {
        this.n_planname_like = n_planname_like;
        if(!ObjectUtils.isEmpty(this.n_planname_like)){
            this.getSearchCond().like("planname", n_planname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_dpid_eq;//[测点]
	public void setN_dpid_eq(String n_dpid_eq) {
        this.n_dpid_eq = n_dpid_eq;
        if(!ObjectUtils.isEmpty(this.n_dpid_eq)){
            this.getSearchCond().eq("dpid", n_dpid_eq);
        }
    }
	private String n_planid_eq;//[计划]
	public void setN_planid_eq(String n_planid_eq) {
        this.n_planid_eq = n_planid_eq;
        if(!ObjectUtils.isEmpty(this.n_planid_eq)){
            this.getSearchCond().eq("planid", n_planid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emplancdtname", query)
            );
		 }
	}
}



