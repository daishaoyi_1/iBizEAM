package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[点检工单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMWO_DP_BASE", resultMap = "EMWO_DPResultMap")
public class EMWO_DP extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 停运时间(分)
     */
    @TableField(value = "eqstoplength")
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;
    /**
     * 工单状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "wostate")
    @JSONField(name = "wostate")
    @JsonProperty("wostate")
    private Integer wostate;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 值
     */
    @TableField(value = "val")
    @JSONField(name = "val")
    @JsonProperty("val")
    private String val;
    /**
     * 工单编号
     */
    @DEField(name = "emwo_dpid", isKeyField = true)
    @TableId(value = "emwo_dpid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emwo_dpid")
    @JsonProperty("emwo_dpid")
    private String emwoDpid;
    /**
     * 优先级
     */
    @DEField(defaultValue = "2")
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 归档
     */
    @TableField(value = "archive")
    @JSONField(name = "archive")
    @JsonProperty("archive")
    private String archive;
    /**
     * 数值
     */
    @TableField(value = "nval")
    @JSONField(name = "nval")
    @JsonProperty("nval")
    private Double nval;
    /**
     * 执行日期
     */
    @TableField(value = "wodate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "wodate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("wodate")
    private Timestamp wodate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 实际执行时间
     */
    @TableField(value = "regionbegindate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionbegindate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    private Timestamp regionbegindate;
    /**
     * 持续时间(H)
     */
    @DEField(defaultValue = "8")
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;
    /**
     * 工单分组
     */
    @DEField(defaultValue = "TASK")
    @TableField(value = "wogroup")
    @JSONField(name = "wogroup")
    @JsonProperty("wogroup")
    private String wogroup;
    /**
     * 工单组
     */
    @TableField(exist = false)
    @JSONField(name = "woteam")
    @JsonProperty("woteam")
    private String woteam;
    /**
     * 执行结果
     */
    @TableField(value = "wresult")
    @JSONField(name = "wresult")
    @JsonProperty("wresult")
    private String wresult;
    /**
     * 过期日期
     */
    @TableField(value = "expiredate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "expiredate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expiredate")
    private Timestamp expiredate;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 详细内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 实际工时(分)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "worklength")
    @JSONField(name = "worklength")
    @JsonProperty("worklength")
    private Double worklength;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 工单内容
     */
    @TableField(value = "wodesc")
    @JSONField(name = "wodesc")
    @JsonProperty("wodesc")
    private String wodesc;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 倍率
     */
    @DEField(defaultValue = "1")
    @TableField(value = "vrate")
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    private Double vrate;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 工单分组
     */
    @TableField(exist = false)
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    private String emwotype;
    /**
     * 制定时间
     */
    @TableField(value = "mdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "mdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    private Timestamp mdate;
    /**
     * 工单名称
     */
    @DEField(name = "emwo_dpname")
    @TableField(value = "emwo_dpname")
    @JSONField(name = "emwo_dpname")
    @JsonProperty("emwo_dpname")
    private String emwoDpname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 预算(￥)
     */
    @TableField(value = "prefee")
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    private Double prefee;
    /**
     * 工单类型
     */
    @DEField(defaultValue = "DP")
    @TableField(value = "wotype")
    @JSONField(name = "wotype")
    @JsonProperty("wotype")
    private String wotype;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 结束时间
     */
    @TableField(value = "regionenddate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionenddate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionenddate")
    private Timestamp regionenddate;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    private String rteamname;
    /**
     * 模式
     */
    @TableField(exist = false)
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    private String rfomoname;
    /**
     * 测点
     */
    @TableField(exist = false)
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    private String dpname;
    /**
     * 现象
     */
    @TableField(exist = false)
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    private String rfodename;
    /**
     * 上级工单
     */
    @TableField(exist = false)
    @JSONField(name = "wopname")
    @JsonProperty("wopname")
    private String wopname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 原因
     */
    @TableField(exist = false)
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    private String rfocaname;
    /**
     * 测点类型
     */
    @TableField(exist = false)
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    private String dptype;
    /**
     * 方案
     */
    @TableField(exist = false)
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    private String rfoacname;
    /**
     * 来源类型
     */
    @TableField(exist = false)
    @JSONField(name = "wooritype")
    @JsonProperty("wooritype")
    private String wooritype;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    private String acclassname;
    /**
     * 工单来源
     */
    @TableField(exist = false)
    @JSONField(name = "wooriname")
    @JsonProperty("wooriname")
    private String wooriname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    private String rservicename;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 测点
     */
    @TableField(value = "dpid")
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    private String dpid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    private String acclassid;
    /**
     * 方案
     */
    @TableField(value = "rfoacid")
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    private String rfoacid;
    /**
     * 上级工单
     */
    @TableField(value = "wopid")
    @JSONField(name = "wopid")
    @JsonProperty("wopid")
    private String wopid;
    /**
     * 原因
     */
    @TableField(value = "rfocaid")
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    private String rfocaid;
    /**
     * 现象
     */
    @TableField(value = "rfodeid")
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    private String rfodeid;
    /**
     * 服务商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    private String rserviceid;
    /**
     * 模式
     */
    @TableField(value = "rfomoid")
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    private String rfomoid;
    /**
     * 工单来源
     */
    @TableField(value = "wooriid")
    @JSONField(name = "wooriid")
    @JsonProperty("wooriid")
    private String wooriid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    private String rteamid;
    /**
     * 制定人
     */
    @TableField(value = "mpersonid")
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    private String mpersonid;
    /**
     * 制定人
     */
    @TableField(exist = false)
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    private String mpersonname;
    /**
     * 责任人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 责任人
     */
    @TableField(exist = false)
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptid")
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    private String rdeptid;
    /**
     * 责任部门
     */
    @TableField(exist = false)
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    private String rdeptname;
    /**
     * 执行人
     */
    @TableField(value = "wpersonid")
    @JSONField(name = "wpersonid")
    @JsonProperty("wpersonid")
    private String wpersonid;
    /**
     * 执行人
     */
    @TableField(exist = false)
    @JSONField(name = "wpersonname")
    @JsonProperty("wpersonname")
    private String wpersonname;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonid")
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    private String recvpersonid;
    /**
     * 接收人
     */
    @TableField(exist = false)
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    private String recvpersonname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject dp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWOORI woori;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wop;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfedpid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pferecvsomid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfemdpid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfewpersonid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [停运时间(分)]
     */
    public void setEqstoplength(Double eqstoplength) {
        this.eqstoplength = eqstoplength;
        this.modify("eqstoplength", eqstoplength);
    }

    /**
     * 设置 [工单状态]
     */
    public void setWostate(Integer wostate) {
        this.wostate = wostate;
        this.modify("wostate", wostate);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [值]
     */
    public void setVal(String val) {
        this.val = val;
        this.modify("val", val);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority) {
        this.priority = priority;
        this.modify("priority", priority);
    }

    /**
     * 设置 [归档]
     */
    public void setArchive(String archive) {
        this.archive = archive;
        this.modify("archive", archive);
    }

    /**
     * 设置 [数值]
     */
    public void setNval(Double nval) {
        this.nval = nval;
        this.modify("nval", nval);
    }

    /**
     * 设置 [执行日期]
     */
    public void setWodate(Timestamp wodate) {
        this.wodate = wodate;
        this.modify("wodate", wodate);
    }

    /**
     * 格式化日期 [执行日期]
     */
    public String formatWodate() {
        if (this.wodate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(wodate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [实际执行时间]
     */
    public void setRegionbegindate(Timestamp regionbegindate) {
        this.regionbegindate = regionbegindate;
        this.modify("regionbegindate", regionbegindate);
    }

    /**
     * 格式化日期 [实际执行时间]
     */
    public String formatRegionbegindate() {
        if (this.regionbegindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionbegindate);
    }
    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [工单分组]
     */
    public void setWogroup(String wogroup) {
        this.wogroup = wogroup;
        this.modify("wogroup", wogroup);
    }

    /**
     * 设置 [执行结果]
     */
    public void setWresult(String wresult) {
        this.wresult = wresult;
        this.modify("wresult", wresult);
    }

    /**
     * 设置 [过期日期]
     */
    public void setExpiredate(Timestamp expiredate) {
        this.expiredate = expiredate;
        this.modify("expiredate", expiredate);
    }

    /**
     * 格式化日期 [过期日期]
     */
    public String formatExpiredate() {
        if (this.expiredate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(expiredate);
    }
    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [详细内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [实际工时(分)]
     */
    public void setWorklength(Double worklength) {
        this.worklength = worklength;
        this.modify("worklength", worklength);
    }

    /**
     * 设置 [工单内容]
     */
    public void setWodesc(String wodesc) {
        this.wodesc = wodesc;
        this.modify("wodesc", wodesc);
    }

    /**
     * 设置 [倍率]
     */
    public void setVrate(Double vrate) {
        this.vrate = vrate;
        this.modify("vrate", vrate);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [制定时间]
     */
    public void setMdate(Timestamp mdate) {
        this.mdate = mdate;
        this.modify("mdate", mdate);
    }

    /**
     * 格式化日期 [制定时间]
     */
    public String formatMdate() {
        if (this.mdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(mdate);
    }
    /**
     * 设置 [工单名称]
     */
    public void setEmwoDpname(String emwoDpname) {
        this.emwoDpname = emwoDpname;
        this.modify("emwo_dpname", emwoDpname);
    }

    /**
     * 设置 [预算(￥)]
     */
    public void setPrefee(Double prefee) {
        this.prefee = prefee;
        this.modify("prefee", prefee);
    }

    /**
     * 设置 [工单类型]
     */
    public void setWotype(String wotype) {
        this.wotype = wotype;
        this.modify("wotype", wotype);
    }

    /**
     * 设置 [结束时间]
     */
    public void setRegionenddate(Timestamp regionenddate) {
        this.regionenddate = regionenddate;
        this.modify("regionenddate", regionenddate);
    }

    /**
     * 格式化日期 [结束时间]
     */
    public String formatRegionenddate() {
        if (this.regionenddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionenddate);
    }
    /**
     * 设置 [测点]
     */
    public void setDpid(String dpid) {
        this.dpid = dpid;
        this.modify("dpid", dpid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [方案]
     */
    public void setRfoacid(String rfoacid) {
        this.rfoacid = rfoacid;
        this.modify("rfoacid", rfoacid);
    }

    /**
     * 设置 [上级工单]
     */
    public void setWopid(String wopid) {
        this.wopid = wopid;
        this.modify("wopid", wopid);
    }

    /**
     * 设置 [原因]
     */
    public void setRfocaid(String rfocaid) {
        this.rfocaid = rfocaid;
        this.modify("rfocaid", rfocaid);
    }

    /**
     * 设置 [现象]
     */
    public void setRfodeid(String rfodeid) {
        this.rfodeid = rfodeid;
        this.modify("rfodeid", rfodeid);
    }

    /**
     * 设置 [服务商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [模式]
     */
    public void setRfomoid(String rfomoid) {
        this.rfomoid = rfomoid;
        this.modify("rfomoid", rfomoid);
    }

    /**
     * 设置 [工单来源]
     */
    public void setWooriid(String wooriid) {
        this.wooriid = wooriid;
        this.modify("wooriid", wooriid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }

    /**
     * 设置 [制定人]
     */
    public void setMpersonid(String mpersonid) {
        this.mpersonid = mpersonid;
        this.modify("mpersonid", mpersonid);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptid(String rdeptid) {
        this.rdeptid = rdeptid;
        this.modify("rdeptid", rdeptid);
    }

    /**
     * 设置 [执行人]
     */
    public void setWpersonid(String wpersonid) {
        this.wpersonid = wpersonid;
        this.modify("wpersonid", wpersonid);
    }

    /**
     * 设置 [接收人]
     */
    public void setRecvpersonid(String recvpersonid) {
        this.recvpersonid = recvpersonid;
        this.modify("recvpersonid", recvpersonid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emwo_dpid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


