package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEquip;
import cn.ibizlab.eam.core.eam_core.filter.EMEquipSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEquipService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEquipMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备档案] 服务对象接口实现
 */
@Slf4j
@Service("EMEquipServiceImpl")
public class EMEquipServiceImpl extends ServiceImpl<EMEquipMapper, EMEquip> implements IEMEquipService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMApplyService emapplyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssessMXService emassessmxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService emdprctService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatterySetupService emeibatterysetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryService emeibatteryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamSetupService emeicamsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamService emeicamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICellSetupService emeicellsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICellService emeicellService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEITIResService emeitiresService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIToolService emeitoolService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMENConsumService emenconsumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQAHService emeqahService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQCheckService emeqcheckService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQDebugService emeqdebugService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKeepService emeqkeepService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKPRCDService emeqkprcdService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLCTFDJService emeqlctfdjService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLCTGSSService emeqlctgssService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLCTRHYService emeqlctrhyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLCTTIResService emeqlcttiresService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService emeqlocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMaintanceService emeqmaintanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMonitorService emeqmonitorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMPMTRService emeqmpmtrService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMPService emeqmpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSetupService emeqsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSTopMoniService emeqstopmoniService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSTopService emeqstopService;

    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQWLService emeqwlService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMJYJLService emjyjlService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMOutputRctService emoutputrctService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanCDTService emplancdtService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanDetailService emplandetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMProductService emproductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRepairCostService emrepaircostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResEmpService emresempService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResItemService emresitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResServiceService emresserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwoDpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwoEnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwoOscService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_PTService emwoPtService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMACClassService emacclassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssetService emassetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMBerthService emberthService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMBrandService embrandService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQTypeService emeqtypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMMachineCategoryService emmachinecategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMMachModelService emmachmodelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFContractService pfcontractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEquip et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmequipid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEquip> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEquip et) {
        fillParentData(et);
        emobjectService.update(emequipInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emequipid", et.getEmequipid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmequipid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEquip> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEquip get(String key) {
        EMEquip et = getById(key);
        if(et == null){
            et = new EMEquip();
            et.setEmequipid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEquip getDraft(EMEquip et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEquip et) {
        return (!ObjectUtils.isEmpty(et.getEmequipid())) && (!Objects.isNull(this.getById(et.getEmequipid())));
    }
    @Override
    @Transactional
    public boolean save(EMEquip et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEquip et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEquip> list) {
        list.forEach(item->fillParentData(item));
        List<EMEquip> create = new ArrayList<>();
        List<EMEquip> update = new ArrayList<>();
        for (EMEquip et : list) {
            if (ObjectUtils.isEmpty(et.getEmequipid()) || ObjectUtils.isEmpty(getById(et.getEmequipid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEquip> list) {
        list.forEach(item->fillParentData(item));
        List<EMEquip> create = new ArrayList<>();
        List<EMEquip> update = new ArrayList<>();
        for (EMEquip et : list) {
            if (ObjectUtils.isEmpty(et.getEmequipid()) || ObjectUtils.isEmpty(getById(et.getEmequipid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEquip> selectByAcclassid(String emacclassid) {
        return baseMapper.selectByAcclassid(emacclassid);
    }
    @Override
    public void removeByAcclassid(String emacclassid) {
        this.remove(new QueryWrapper<EMEquip>().eq("acclassid",emacclassid));
    }

	@Override
    public List<EMEquip> selectByAssetid(String emassetid) {
        return baseMapper.selectByAssetid(emassetid);
    }
    @Override
    public void removeByAssetid(String emassetid) {
        this.remove(new QueryWrapper<EMEquip>().eq("assetid",emassetid));
    }

	@Override
    public List<EMEquip> selectByEmberthid(String emberthid) {
        return baseMapper.selectByEmberthid(emberthid);
    }
    @Override
    public void removeByEmberthid(String emberthid) {
        this.remove(new QueryWrapper<EMEquip>().eq("emberthid",emberthid));
    }

	@Override
    public List<EMEquip> selectByEmbrandid(String embrandid) {
        return baseMapper.selectByEmbrandid(embrandid);
    }
    @Override
    public void removeByEmbrandid(String embrandid) {
        this.remove(new QueryWrapper<EMEquip>().eq("embrandid",embrandid));
    }

	@Override
    public List<EMEquip> selectByEqlocationid(String emeqlocationid) {
        return baseMapper.selectByEqlocationid(emeqlocationid);
    }
    @Override
    public void removeByEqlocationid(String emeqlocationid) {
        this.remove(new QueryWrapper<EMEquip>().eq("eqlocationid",emeqlocationid));
    }

	@Override
    public List<EMEquip> selectByEqtypeid(String emeqtypeid) {
        return baseMapper.selectByEqtypeid(emeqtypeid);
    }
    @Override
    public void removeByEqtypeid(String emeqtypeid) {
        this.remove(new QueryWrapper<EMEquip>().eq("eqtypeid",emeqtypeid));
    }

	@Override
    public List<EMEquip> selectByEquippid(String emequipid) {
        return baseMapper.selectByEquippid(emequipid);
    }
    @Override
    public void removeByEquippid(String emequipid) {
        this.remove(new QueryWrapper<EMEquip>().eq("equippid",emequipid));
    }

	@Override
    public List<EMEquip> selectByEmmachinecategoryid(String emmachinecategoryid) {
        return baseMapper.selectByEmmachinecategoryid(emmachinecategoryid);
    }
    @Override
    public void removeByEmmachinecategoryid(String emmachinecategoryid) {
        this.remove(new QueryWrapper<EMEquip>().eq("emmachinecategoryid",emmachinecategoryid));
    }

	@Override
    public List<EMEquip> selectByEmmachmodelid(String emmachmodelid) {
        return baseMapper.selectByEmmachmodelid(emmachmodelid);
    }
    @Override
    public void removeByEmmachmodelid(String emmachmodelid) {
        this.remove(new QueryWrapper<EMEquip>().eq("emmachmodelid",emmachmodelid));
    }

	@Override
    public List<EMEquip> selectByLabserviceid(String emserviceid) {
        return baseMapper.selectByLabserviceid(emserviceid);
    }
    @Override
    public void removeByLabserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMEquip>().eq("labserviceid",emserviceid));
    }

	@Override
    public List<EMEquip> selectByMserviceid(String emserviceid) {
        return baseMapper.selectByMserviceid(emserviceid);
    }
    @Override
    public void removeByMserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMEquip>().eq("mserviceid",emserviceid));
    }

	@Override
    public List<EMEquip> selectByRserviceid(String emserviceid) {
        return baseMapper.selectByRserviceid(emserviceid);
    }
    @Override
    public void removeByRserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMEquip>().eq("rserviceid",emserviceid));
    }

	@Override
    public List<EMEquip> selectByContractid(String pfcontractid) {
        return baseMapper.selectByContractid(pfcontractid);
    }
    @Override
    public void removeByContractid(String pfcontractid) {
        this.remove(new QueryWrapper<EMEquip>().eq("contractid",pfcontractid));
    }

	@Override
    public List<EMEquip> selectByRteamid(String pfteamid) {
        return baseMapper.selectByRteamid(pfteamid);
    }
    @Override
    public void removeByRteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMEquip>().eq("rteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEquip> searchDefault(EMEquipSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEquip> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEquip>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 类型下设备
     */
    @Override
    public Page<EMEquip> searchEQTypeTree(EMEquipSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEquip> pages=baseMapper.searchEQTypeTree(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEquip>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 各类型设备数量
     */
    @Override
    public Page<Map> searchTypeEQNum(EMEquipSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Map> pages=baseMapper.searchTypeEQNum(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Map>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEquip et){
        //实体关系[DER1N_EMEQUIP_EMACCLASS_ACCLASSID]
        if(!ObjectUtils.isEmpty(et.getAcclassid())){
            cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass=et.getAcclass();
            if(ObjectUtils.isEmpty(acclass)){
                cn.ibizlab.eam.core.eam_core.domain.EMACClass majorEntity=emacclassService.get(et.getAcclassid());
                et.setAcclass(majorEntity);
                acclass=majorEntity;
            }
            et.setAcclassname(acclass.getEmacclassname());
        }
        //实体关系[DER1N_EMEQUIP_EMASSET_ASSETID]
        if(!ObjectUtils.isEmpty(et.getAssetid())){
            cn.ibizlab.eam.core.eam_core.domain.EMAsset asset=et.getAsset();
            if(ObjectUtils.isEmpty(asset)){
                cn.ibizlab.eam.core.eam_core.domain.EMAsset majorEntity=emassetService.get(et.getAssetid());
                et.setAsset(majorEntity);
                asset=majorEntity;
            }
            et.setAssetclasscode(asset.getAssetclasscode());
            et.setAssetname(asset.getEmassetname());
            et.setAssetclassname(asset.getAssetclassname());
            et.setAssetclassid(asset.getAssetclassid());
            et.setAssetcode(asset.getAssetcode());
        }
        //实体关系[DER1N_EMEQUIP_EMBERTH_EMBERTHID]
        if(!ObjectUtils.isEmpty(et.getEmberthid())){
            cn.ibizlab.eam.core.eam_core.domain.EMBerth emberth=et.getEmberth();
            if(ObjectUtils.isEmpty(emberth)){
                cn.ibizlab.eam.core.eam_core.domain.EMBerth majorEntity=emberthService.get(et.getEmberthid());
                et.setEmberth(majorEntity);
                emberth=majorEntity;
            }
            et.setEmberthcode(emberth.getEmberthname());
            et.setEmberthname(emberth.getEmberthname());
        }
        //实体关系[DER1N_EMEQUIP_EMBRAND_EMBRANDID]
        if(!ObjectUtils.isEmpty(et.getEmbrandid())){
            cn.ibizlab.eam.core.eam_core.domain.EMBrand embrand=et.getEmbrand();
            if(ObjectUtils.isEmpty(embrand)){
                cn.ibizlab.eam.core.eam_core.domain.EMBrand majorEntity=embrandService.get(et.getEmbrandid());
                et.setEmbrand(majorEntity);
                embrand=majorEntity;
            }
            et.setEmbrandcode(embrand.getEmbrandcode());
            et.setEmbrandname(embrand.getEmbrandname());
        }
        //实体关系[DER1N_EMEQUIP_EMEQLOCATION_EQLOCATIONID]
        if(!ObjectUtils.isEmpty(et.getEqlocationid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation=et.getEqlocation();
            if(ObjectUtils.isEmpty(eqlocation)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQLocation majorEntity=emeqlocationService.get(et.getEqlocationid());
                et.setEqlocation(majorEntity);
                eqlocation=majorEntity;
            }
            et.setEqlocationname(eqlocation.getEqlocationinfo());
            et.setEqlocationcode(eqlocation.getEqlocationcode());
        }
        //实体关系[DER1N_EMEQUIP_EMEQTYPE_EQTYPEID]
        if(!ObjectUtils.isEmpty(et.getEqtypeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQType eqtype=et.getEqtype();
            if(ObjectUtils.isEmpty(eqtype)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQType majorEntity=emeqtypeService.get(et.getEqtypeid());
                et.setEqtype(majorEntity);
                eqtype=majorEntity;
            }
            et.setStype(eqtype.getStype());
            et.setEqtypecode(eqtype.getEqtypecode());
            et.setEqtypename(eqtype.getEqtypeinfo());
            et.setEqtypepid(eqtype.getEqtypepid());
            et.setSname(eqtype.getSname());
        }
        //实体关系[DER1N_EMEQUIP_EMEQUIP_EQUIPPID]
        if(!ObjectUtils.isEmpty(et.getEquippid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equipp=et.getEquipp();
            if(ObjectUtils.isEmpty(equipp)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquippid());
                et.setEquipp(majorEntity);
                equipp=majorEntity;
            }
            et.setEquippcode(equipp.getEquipcode());
            et.setEquippname(equipp.getEmequipname());
        }
        //实体关系[DER1N_EMEQUIP_EMMACHINECATEGORY_EMMACHINECATEGORYID]
        if(!ObjectUtils.isEmpty(et.getEmmachinecategoryid())){
            cn.ibizlab.eam.core.eam_core.domain.EMMachineCategory emmachinecategory=et.getEmmachinecategory();
            if(ObjectUtils.isEmpty(emmachinecategory)){
                cn.ibizlab.eam.core.eam_core.domain.EMMachineCategory majorEntity=emmachinecategoryService.get(et.getEmmachinecategoryid());
                et.setEmmachinecategory(majorEntity);
                emmachinecategory=majorEntity;
            }
            et.setJzbh1(emmachinecategory.getMachtypecode());
            et.setEmmachinecategoryname(emmachinecategory.getEmmachinecategoryname());
            et.setMachtypecode(emmachinecategory.getMachtypecode());
        }
        //实体关系[DER1N_EMEQUIP_EMMACHMODEL_EMMACHMODELID]
        if(!ObjectUtils.isEmpty(et.getEmmachmodelid())){
            cn.ibizlab.eam.core.eam_core.domain.EMMachModel emmachmodel=et.getEmmachmodel();
            if(ObjectUtils.isEmpty(emmachmodel)){
                cn.ibizlab.eam.core.eam_core.domain.EMMachModel majorEntity=emmachmodelService.get(et.getEmmachmodelid());
                et.setEmmachmodel(majorEntity);
                emmachmodel=majorEntity;
            }
            et.setEmmachmodelname(emmachmodel.getEmmachmodelname());
        }
        //实体关系[DER1N_EMEQUIP_EMSERVICE_LABSERVICEID]
        if(!ObjectUtils.isEmpty(et.getLabserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService labservice=et.getLabservice();
            if(ObjectUtils.isEmpty(labservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getLabserviceid());
                et.setLabservice(majorEntity);
                labservice=majorEntity;
            }
            et.setLabservicename(labservice.getEmservicename());
        }
        //实体关系[DER1N_EMEQUIP_EMSERVICE_MSERVICEID]
        if(!ObjectUtils.isEmpty(et.getMserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService mservice=et.getMservice();
            if(ObjectUtils.isEmpty(mservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getMserviceid());
                et.setMservice(majorEntity);
                mservice=majorEntity;
            }
            et.setMservicename(mservice.getEmservicename());
        }
        //实体关系[DER1N_EMEQUIP_EMSERVICE_RSERVICEID]
        if(!ObjectUtils.isEmpty(et.getRserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService rservice=et.getRservice();
            if(ObjectUtils.isEmpty(rservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getRserviceid());
                et.setRservice(majorEntity);
                rservice=majorEntity;
            }
            et.setRservicename(rservice.getEmservicename());
        }
        //实体关系[DER1N_EMEQUIP_PFCONTRACT_CONTRACTID]
        if(!ObjectUtils.isEmpty(et.getContractid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFContract contract=et.getContract();
            if(ObjectUtils.isEmpty(contract)){
                cn.ibizlab.eam.core.eam_pf.domain.PFContract majorEntity=pfcontractService.get(et.getContractid());
                et.setContract(majorEntity);
                contract=majorEntity;
            }
            et.setContractname(contract.getPfcontractname());
        }
        //实体关系[DER1N_EMEQUIP_PFTEAM_RTEAMID]
        if(!ObjectUtils.isEmpty(et.getRteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam=et.getRteam();
            if(ObjectUtils.isEmpty(rteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getRteamid());
                et.setRteam(majorEntity);
                rteam=majorEntity;
            }
            et.setRteamname(rteam.getPfteamname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEquipInheritMapping emequipInheritMapping;
    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEquip et){
        if(ObjectUtils.isEmpty(et.getEmequipid()))
            et.setEmequipid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emequipInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","EQUIP");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEquip> getEmequipByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEquip> getEmequipByEntities(List<EMEquip> entities) {
        List ids =new ArrayList();
        for(EMEquip entity : entities){
            Serializable id=entity.getEmequipid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEquipService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



