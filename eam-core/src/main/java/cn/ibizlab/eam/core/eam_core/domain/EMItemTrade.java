package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[物品交易]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMITEMTRADE_BASE", resultMap = "EMItemTradeResultMap")
public class EMItemTrade extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 批次
     */
    @DEField(defaultValue = "NA")
    @TableField(value = "batcode")
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    private String batcode;
    /**
     * 物品交易名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emitemtradename")
    @JSONField(name = "emitemtradename")
    @JsonProperty("emitemtradename")
    private String emitemtradename;
    /**
     * 发票号
     */
    @TableField(value = "civo")
    @JSONField(name = "civo")
    @JsonProperty("civo")
    private String civo;
    /**
     * 单价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 出入日期
     */
    @TableField(value = "sdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "sdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    private Timestamp sdate;
    /**
     * 交易状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "tradestate")
    @JSONField(name = "tradestate")
    @JsonProperty("tradestate")
    private Integer tradestate;
    /**
     * 领料分类
     */
    @DEField(defaultValue = "INNER")
    @TableField(value = "pusetype")
    @JSONField(name = "pusetype")
    @JsonProperty("pusetype")
    private String pusetype;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 数量
     */
    @TableField(value = "psum")
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;
    /**
     * 总金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 出入标志
     */
    @TableField(value = "inoutflag")
    @JSONField(name = "inoutflag")
    @JsonProperty("inoutflag")
    private Integer inoutflag;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 税费
     */
    @DEField(defaultValue = "0")
    @TableField(value = "shf")
    @JSONField(name = "shf")
    @JsonProperty("shf")
    private Double shf;
    /**
     * 交易分组
     */
    @TableField(value = "emitemtradetype")
    @JSONField(name = "emitemtradetype")
    @JsonProperty("emitemtradetype")
    private String emitemtradetype;
    /**
     * 物品交易标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emitemtradeid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emitemtradeid")
    @JsonProperty("emitemtradeid")
    private String emitemtradeid;
    /**
     * 物品类型分组
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypegroup")
    @JsonProperty("itemtypegroup")
    private Integer itemtypegroup;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 物品2级类
     */
    @TableField(exist = false)
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    private String itemmtypeid;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    private String itembtypename;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "storename")
    @JsonProperty("storename")
    private String storename;
    /**
     * 平均税费
     */
    @TableField(exist = false)
    @JSONField(name = "shfprice")
    @JsonProperty("shfprice")
    private Double shfprice;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    private String itembtypeid;
    /**
     * 物品库存金额
     */
    @TableField(exist = false)
    @JSONField(name = "stockamount")
    @JsonProperty("stockamount")
    private Double stockamount;
    /**
     * 关联单
     */
    @TableField(exist = false)
    @JSONField(name = "rname")
    @JsonProperty("rname")
    private String rname;
    /**
     * 物品代码
     */
    @TableField(exist = false)
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    private String itemcode;
    /**
     * 库位
     */
    @TableField(exist = false)
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    private String storepartname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    private String teamname;
    /**
     * 物品二级类
     */
    @TableField(exist = false)
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    private String itemmtypename;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    private String labservicename;
    /**
     * 物品类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    private String itemtypeid;
    /**
     * 供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    private String labserviceid;
    /**
     * 班组
     */
    @TableField(value = "teamid")
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    private String teamid;
    /**
     * 关联单
     */
    @TableField(value = "rid")
    @JSONField(name = "rid")
    @JsonProperty("rid")
    private String rid;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 仓库
     */
    @TableField(value = "storeid")
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    private String storeid;
    /**
     * 库位
     */
    @TableField(value = "storepartid")
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    private String storepartid;
    /**
     * 申请人
     */
    @TableField(value = "aempid")
    @JSONField(name = "aempid")
    @JsonProperty("aempid")
    private String aempid;
    /**
     * 申请人
     */
    @TableField(exist = false)
    @JSONField(name = "aempname")
    @JsonProperty("aempname")
    private String aempname;
    /**
     * 部门
     */
    @TableField(value = "deptid")
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    private String deptid;
    /**
     * 部门
     */
    @TableField(exist = false)
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    private String deptname;
    /**
     * 库管员
     */
    @TableField(value = "sempid")
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    private String sempid;
    /**
     * 库管员
     */
    @TableField(exist = false)
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    private String sempname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemTrade r;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStore store;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfeaempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfesempmid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam team;



    /**
     * 设置 [批次]
     */
    public void setBatcode(String batcode) {
        this.batcode = batcode;
        this.modify("batcode", batcode);
    }

    /**
     * 设置 [物品交易名称]
     */
    public void setEmitemtradename(String emitemtradename) {
        this.emitemtradename = emitemtradename;
        this.modify("emitemtradename", emitemtradename);
    }

    /**
     * 设置 [发票号]
     */
    public void setCivo(String civo) {
        this.civo = civo;
        this.modify("civo", civo);
    }

    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [出入日期]
     */
    public void setSdate(Timestamp sdate) {
        this.sdate = sdate;
        this.modify("sdate", sdate);
    }

    /**
     * 格式化日期 [出入日期]
     */
    public String formatSdate() {
        if (this.sdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sdate);
    }
    /**
     * 设置 [交易状态]
     */
    public void setTradestate(Integer tradestate) {
        this.tradestate = tradestate;
        this.modify("tradestate", tradestate);
    }

    /**
     * 设置 [领料分类]
     */
    public void setPusetype(String pusetype) {
        this.pusetype = pusetype;
        this.modify("pusetype", pusetype);
    }

    /**
     * 设置 [数量]
     */
    public void setPsum(Double psum) {
        this.psum = psum;
        this.modify("psum", psum);
    }

    /**
     * 设置 [总金额]
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [出入标志]
     */
    public void setInoutflag(Integer inoutflag) {
        this.inoutflag = inoutflag;
        this.modify("inoutflag", inoutflag);
    }

    /**
     * 设置 [税费]
     */
    public void setShf(Double shf) {
        this.shf = shf;
        this.modify("shf", shf);
    }

    /**
     * 设置 [交易分组]
     */
    public void setEmitemtradetype(String emitemtradetype) {
        this.emitemtradetype = emitemtradetype;
        this.modify("emitemtradetype", emitemtradetype);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [班组]
     */
    public void setTeamid(String teamid) {
        this.teamid = teamid;
        this.modify("teamid", teamid);
    }

    /**
     * 设置 [关联单]
     */
    public void setRid(String rid) {
        this.rid = rid;
        this.modify("rid", rid);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [仓库]
     */
    public void setStoreid(String storeid) {
        this.storeid = storeid;
        this.modify("storeid", storeid);
    }

    /**
     * 设置 [库位]
     */
    public void setStorepartid(String storepartid) {
        this.storepartid = storepartid;
        this.modify("storepartid", storepartid);
    }

    /**
     * 设置 [申请人]
     */
    public void setAempid(String aempid) {
        this.aempid = aempid;
        this.modify("aempid", aempid);
    }

    /**
     * 设置 [部门]
     */
    public void setDeptid(String deptid) {
        this.deptid = deptid;
        this.modify("deptid", deptid);
    }

    /**
     * 设置 [库管员]
     */
    public void setSempid(String sempid) {
        this.sempid = sempid;
        this.modify("sempid", sempid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emitemtradeid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


