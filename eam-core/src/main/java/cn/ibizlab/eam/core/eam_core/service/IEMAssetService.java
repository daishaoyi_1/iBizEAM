package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMAsset;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMAsset] 服务对象接口
 */
public interface IEMAssetService extends IService<EMAsset> {

    boolean create(EMAsset et);
    void createBatch(List<EMAsset> list);
    boolean update(EMAsset et);
    void updateBatch(List<EMAsset> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMAsset get(String key);
    EMAsset getDraft(EMAsset et);
    boolean checkKey(EMAsset et);
    boolean save(EMAsset et);
    void saveBatch(List<EMAsset> list);
    Page<EMAsset> searchAssetBf(EMAssetSearchContext context);
    Page<EMAsset> searchDefault(EMAssetSearchContext context);
    List<EMAsset> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMAsset> selectByAssetclassid(String emassetclassid);
    void removeByAssetclassid(String emassetclassid);
    List<EMAsset> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMAsset> selectByLabserviceid(String emserviceid);
    void removeByLabserviceid(String emserviceid);
    List<EMAsset> selectByMserviceid(String emserviceid);
    void removeByMserviceid(String emserviceid);
    List<EMAsset> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMAsset> selectByContractid(String pfcontractid);
    void removeByContractid(String pfcontractid);
    List<EMAsset> selectByUnitid(String pfunitid);
    void removeByUnitid(String pfunitid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMAsset> getEmassetByIds(List<String> ids);
    List<EMAsset> getEmassetByEntities(List<EMAsset> entities);
}


