package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMApply;
import cn.ibizlab.eam.core.eam_core.filter.EMApplySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMApply] 服务对象接口
 */
public interface IEMApplyService extends IService<EMApply> {

    boolean create(EMApply et);
    void createBatch(List<EMApply> list);
    boolean update(EMApply et);
    void updateBatch(List<EMApply> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMApply get(String key);
    EMApply getDraft(EMApply et);
    boolean checkKey(EMApply et);
    EMApply confirm(EMApply et);
    EMApply formUpdateByEmquipId(EMApply et);
    EMApply rejected(EMApply et);
    boolean save(EMApply et);
    void saveBatch(List<EMApply> list);
    EMApply submit(EMApply et);
    Page<EMApply> searchConfirmed(EMApplySearchContext context);
    Page<EMApply> searchDefault(EMApplySearchContext context);
    Page<EMApply> searchDraft(EMApplySearchContext context);
    Page<EMApply> searchToConfirm(EMApplySearchContext context);
    List<EMApply> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMApply> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMApply> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMApply> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMApply> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMApply> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMApply> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMApply> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMApply> getEmapplyByIds(List<String> ids);
    List<EMApply> getEmapplyByEntities(List<EMApply> entities);
}


