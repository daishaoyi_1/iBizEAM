

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTDetail;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMPlanTDetailInheritMapping {

    @Mappings({
        @Mapping(source ="emplantdetailid",target = "emresrefobjid"),
        @Mapping(source ="emplantdetailname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="plantemplid",target = "resrefobjpid"),
    })
    EMResRefObj toEmresrefobj(EMPlanTDetail minorEntity);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emplantdetailid"),
        @Mapping(source ="emresrefobjname" ,target = "emplantdetailname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="resrefobjpid",target = "plantemplid"),
    })
    EMPlanTDetail toEmplantdetail(EMResRefObj majorEntity);

    List<EMResRefObj> toEmresrefobj(List<EMPlanTDetail> minorEntities);

    List<EMPlanTDetail> toEmplantdetail(List<EMResRefObj> majorEntities);

}


