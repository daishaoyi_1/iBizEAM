package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMItemROut;
/**
 * 关系型数据实体[EMItemROut] 查询条件对象
 */
@Slf4j
@Data
public class EMItemROutSearchContext extends QueryWrapperContext<EMItemROut> {

	private String n_emitemroutname_like;//[退货单名称]
	public void setN_emitemroutname_like(String n_emitemroutname_like) {
        this.n_emitemroutname_like = n_emitemroutname_like;
        if(!ObjectUtils.isEmpty(this.n_emitemroutname_like)){
            this.getSearchCond().like("emitemroutname", n_emitemroutname_like);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_itemroutinfo_like;//[退货单信息]
	public void setN_itemroutinfo_like(String n_itemroutinfo_like) {
        this.n_itemroutinfo_like = n_itemroutinfo_like;
        if(!ObjectUtils.isEmpty(this.n_itemroutinfo_like)){
            this.getSearchCond().like("itemroutinfo", n_itemroutinfo_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private Integer n_tradestate_eq;//[退货状态]
	public void setN_tradestate_eq(Integer n_tradestate_eq) {
        this.n_tradestate_eq = n_tradestate_eq;
        if(!ObjectUtils.isEmpty(this.n_tradestate_eq)){
            this.getSearchCond().eq("tradestate", n_tradestate_eq);
        }
    }
	private String n_storepartname_eq;//[库位]
	public void setN_storepartname_eq(String n_storepartname_eq) {
        this.n_storepartname_eq = n_storepartname_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartname_eq)){
            this.getSearchCond().eq("storepartname", n_storepartname_eq);
        }
    }
	private String n_storepartname_like;//[库位]
	public void setN_storepartname_like(String n_storepartname_like) {
        this.n_storepartname_like = n_storepartname_like;
        if(!ObjectUtils.isEmpty(this.n_storepartname_like)){
            this.getSearchCond().like("storepartname", n_storepartname_like);
        }
    }
	private String n_storename_eq;//[仓库]
	public void setN_storename_eq(String n_storename_eq) {
        this.n_storename_eq = n_storename_eq;
        if(!ObjectUtils.isEmpty(this.n_storename_eq)){
            this.getSearchCond().eq("storename", n_storename_eq);
        }
    }
	private String n_storename_like;//[仓库]
	public void setN_storename_like(String n_storename_like) {
        this.n_storename_like = n_storename_like;
        if(!ObjectUtils.isEmpty(this.n_storename_like)){
            this.getSearchCond().like("storename", n_storename_like);
        }
    }
	private String n_rname_eq;//[入库单]
	public void setN_rname_eq(String n_rname_eq) {
        this.n_rname_eq = n_rname_eq;
        if(!ObjectUtils.isEmpty(this.n_rname_eq)){
            this.getSearchCond().eq("rname", n_rname_eq);
        }
    }
	private String n_rname_like;//[入库单]
	public void setN_rname_like(String n_rname_like) {
        this.n_rname_like = n_rname_like;
        if(!ObjectUtils.isEmpty(this.n_rname_like)){
            this.getSearchCond().like("rname", n_rname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_storeid_eq;//[仓库]
	public void setN_storeid_eq(String n_storeid_eq) {
        this.n_storeid_eq = n_storeid_eq;
        if(!ObjectUtils.isEmpty(this.n_storeid_eq)){
            this.getSearchCond().eq("storeid", n_storeid_eq);
        }
    }
	private String n_rid_eq;//[入库单]
	public void setN_rid_eq(String n_rid_eq) {
        this.n_rid_eq = n_rid_eq;
        if(!ObjectUtils.isEmpty(this.n_rid_eq)){
            this.getSearchCond().eq("rid", n_rid_eq);
        }
    }
	private String n_storepartid_eq;//[库位]
	public void setN_storepartid_eq(String n_storepartid_eq) {
        this.n_storepartid_eq = n_storepartid_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartid_eq)){
            this.getSearchCond().eq("storepartid", n_storepartid_eq);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_sempid_eq;//[退货人]
	public void setN_sempid_eq(String n_sempid_eq) {
        this.n_sempid_eq = n_sempid_eq;
        if(!ObjectUtils.isEmpty(this.n_sempid_eq)){
            this.getSearchCond().eq("sempid", n_sempid_eq);
        }
    }
	private String n_sempname_eq;//[退货人]
	public void setN_sempname_eq(String n_sempname_eq) {
        this.n_sempname_eq = n_sempname_eq;
        if(!ObjectUtils.isEmpty(this.n_sempname_eq)){
            this.getSearchCond().eq("sempname", n_sempname_eq);
        }
    }
	private String n_sempname_like;//[退货人]
	public void setN_sempname_like(String n_sempname_like) {
        this.n_sempname_like = n_sempname_like;
        if(!ObjectUtils.isEmpty(this.n_sempname_like)){
            this.getSearchCond().like("sempname", n_sempname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emitemroutname", query)
            );
		 }
	}
}



