package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[轮胎位置]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQLCTTIRES_BASE", resultMap = "EMEQLCTTIResResultMap")
public class EMEQLCTTIRes extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 轮胎状态
     */
    @TableField(value = "tiresstate")
    @JSONField(name = "tiresstate")
    @JsonProperty("tiresstate")
    private String tiresstate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 使用气压
     */
    @TableField(value = "par")
    @JSONField(name = "par")
    @JsonProperty("par")
    private String par;
    /**
     * 价格
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private String amount;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 图形8*8=11-88
     */
    @TableField(value = "picparams")
    @JSONField(name = "picparams")
    @JsonProperty("picparams")
    private String picparams;
    /**
     * 预警期限(天)
     */
    @DEField(defaultValue = "450")
    @TableField(value = "valve")
    @JSONField(name = "valve")
    @JsonProperty("valve")
    private Integer valve;
    /**
     * 更换时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "replacedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("replacedate")
    private Timestamp replacedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 型号
     */
    @TableField(value = "eqmodelcode")
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    private String eqmodelcode;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 新旧标志
     */
    @TableField(value = "newoldflag")
    @JSONField(name = "newoldflag")
    @JsonProperty("newoldflag")
    private String newoldflag;
    /**
     * 厂牌
     */
    @TableField(value = "changp")
    @JSONField(name = "changp")
    @JsonProperty("changp")
    private String changp;
    /**
     * 材质层数
     */
    @TableField(value = "systemparam")
    @JSONField(name = "systemparam")
    @JsonProperty("systemparam")
    private String systemparam;
    /**
     * 更换原因
     */
    @TableField(exist = false)
    @JSONField(name = "replacereason")
    @JsonProperty("replacereason")
    private String replacereason;
    /**
     * 轮胎备注
     */
    @TableField(value = "lctdesc")
    @JSONField(name = "lctdesc")
    @JsonProperty("lctdesc")
    private String lctdesc;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 轮胎车型
     */
    @DEField(defaultValue = "LTD")
    @TableField(value = "tirestype")
    @JSONField(name = "tirestype")
    @JsonProperty("tirestype")
    private String tirestype;
    /**
     * 轮胎信息
     */
    @TableField(exist = false)
    @JSONField(name = "lcttiresinfo")
    @JsonProperty("lcttiresinfo")
    private String lcttiresinfo;
    /**
     * 有内胎
     */
    @TableField(value = "haveinner")
    @JSONField(name = "haveinner")
    @JsonProperty("haveinner")
    private Integer haveinner;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 出厂编号
     */
    @TableField(exist = false)
    @JSONField(name = "mccode")
    @JsonProperty("mccode")
    private String mccode;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    private String labservicename;
    /**
     * 制造商
     */
    @TableField(exist = false)
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    private String mservicename;
    /**
     * 位置信息
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationinfo")
    @JsonProperty("eqlocationinfo")
    private String eqlocationinfo;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 位置标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqlocationid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqlocationid")
    @JsonProperty("emeqlocationid")
    private String emeqlocationid;
    /**
     * 供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    private String labserviceid;
    /**
     * 制造商
     */
    @TableField(value = "mserviceid")
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    private String mserviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService mservice;



    /**
     * 设置 [轮胎状态]
     */
    public void setTiresstate(String tiresstate) {
        this.tiresstate = tiresstate;
        this.modify("tiresstate", tiresstate);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [使用气压]
     */
    public void setPar(String par) {
        this.par = par;
        this.modify("par", par);
    }

    /**
     * 设置 [价格]
     */
    public void setAmount(String amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [图形8*8=11-88]
     */
    public void setPicparams(String picparams) {
        this.picparams = picparams;
        this.modify("picparams", picparams);
    }

    /**
     * 设置 [预警期限(天)]
     */
    public void setValve(Integer valve) {
        this.valve = valve;
        this.modify("valve", valve);
    }

    /**
     * 设置 [型号]
     */
    public void setEqmodelcode(String eqmodelcode) {
        this.eqmodelcode = eqmodelcode;
        this.modify("eqmodelcode", eqmodelcode);
    }

    /**
     * 设置 [新旧标志]
     */
    public void setNewoldflag(String newoldflag) {
        this.newoldflag = newoldflag;
        this.modify("newoldflag", newoldflag);
    }

    /**
     * 设置 [厂牌]
     */
    public void setChangp(String changp) {
        this.changp = changp;
        this.modify("changp", changp);
    }

    /**
     * 设置 [材质层数]
     */
    public void setSystemparam(String systemparam) {
        this.systemparam = systemparam;
        this.modify("systemparam", systemparam);
    }

    /**
     * 设置 [轮胎备注]
     */
    public void setLctdesc(String lctdesc) {
        this.lctdesc = lctdesc;
        this.modify("lctdesc", lctdesc);
    }

    /**
     * 设置 [轮胎车型]
     */
    public void setTirestype(String tirestype) {
        this.tirestype = tirestype;
        this.modify("tirestype", tirestype);
    }

    /**
     * 设置 [有内胎]
     */
    public void setHaveinner(Integer haveinner) {
        this.haveinner = haveinner;
        this.modify("haveinner", haveinner);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [制造商]
     */
    public void setMserviceid(String mserviceid) {
        this.mserviceid = mserviceid;
        this.modify("mserviceid", mserviceid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqlocationid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


