package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPLAN_BASE", resultMap = "EMPlanResultMap")
public class EMPlan extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 计划编号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emplanid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    private String emplanid;
    /**
     * 制定时间
     */
    @TableField(value = "mdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "mdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    private Timestamp mdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 预算(￥)
     */
    @TableField(value = "prefee")
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    private String prefee;
    /**
     * 计划内容
     */
    @TableField(value = "plandesc")
    @JSONField(name = "plandesc")
    @JsonProperty("plandesc")
    private String plandesc;
    /**
     * 多任务?
     */
    @DEField(defaultValue = "0")
    @TableField(value = "mtflag")
    @JSONField(name = "mtflag")
    @JsonProperty("mtflag")
    private Integer mtflag;
    /**
     * 计划周期(天)
     */
    @TableField(value = "plancvl")
    @JSONField(name = "plancvl")
    @JsonProperty("plancvl")
    private Double plancvl;
    /**
     * 计划名称
     */
    @TableField(value = "emplanname")
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    private String emplanname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 生成工单种类
     */
    @DEField(defaultValue = "INNER")
    @TableField(value = "emwotype")
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    private String emwotype;
    /**
     * 计划类型
     */
    @TableField(value = "plantype")
    @JSONField(name = "plantype")
    @JsonProperty("plantype")
    private String plantype;
    /**
     * 详细内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 计划信息
     */
    @TableField(exist = false)
    @JSONField(name = "planinfo")
    @JsonProperty("planinfo")
    private String planinfo;
    /**
     * 停运时间(分)
     */
    @TableField(value = "eqstoplength")
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonid")
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    private String recvpersonid;
    /**
     * 归档
     */
    @TableField(value = "archive")
    @JSONField(name = "archive")
    @JsonProperty("archive")
    private String archive;
    /**
     * 持续时间(H)
     */
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;
    /**
     * 计划状态
     */
    @DEField(defaultValue = "1")
    @TableField(value = "planstate")
    @JSONField(name = "planstate")
    @JsonProperty("planstate")
    private String planstate;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonname")
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    private String recvpersonname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 测点
     */
    @TableField(exist = false)
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    private String dpname;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    private String acclassname;
    /**
     * 测点类型
     */
    @TableField(exist = false)
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    private String dptype;
    /**
     * 计划模板
     */
    @TableField(exist = false)
    @JSONField(name = "plantemplname")
    @JsonProperty("plantemplname")
    private String plantemplname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    private String rteamname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    private String rservicename;
    /**
     * 服务商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    private String rserviceid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 测点
     */
    @TableField(value = "dpid")
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    private String dpid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    private String acclassid;
    /**
     * 计划模板
     */
    @TableField(value = "plantemplid")
    @JSONField(name = "plantemplid")
    @JsonProperty("plantemplid")
    private String plantemplid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    private String rteamid;
    /**
     * 任务时刻
     */
    @TableField(value = "cron")
    @JSONField(name = "cron")
    @JsonProperty("cron")
    private String cron;
    /**
     * 制定人
     */
    @TableField(value = "mpersonid")
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    private String mpersonid;
    /**
     * 制定人
     */
    @TableField(exist = false)
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    private String mpersonname;
    /**
     * 责任人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 责任人
     */
    @TableField(exist = false)
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptid")
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    private String rdeptid;
    /**
     * 责任部门
     */
    @TableField(exist = false)
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    private String rdeptname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject dp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl plantempl;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempersonid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [制定时间]
     */
    public void setMdate(Timestamp mdate) {
        this.mdate = mdate;
        this.modify("mdate", mdate);
    }

    /**
     * 格式化日期 [制定时间]
     */
    public String formatMdate() {
        if (this.mdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(mdate);
    }
    /**
     * 设置 [预算(￥)]
     */
    public void setPrefee(String prefee) {
        this.prefee = prefee;
        this.modify("prefee", prefee);
    }

    /**
     * 设置 [计划内容]
     */
    public void setPlandesc(String plandesc) {
        this.plandesc = plandesc;
        this.modify("plandesc", plandesc);
    }

    /**
     * 设置 [多任务?]
     */
    public void setMtflag(Integer mtflag) {
        this.mtflag = mtflag;
        this.modify("mtflag", mtflag);
    }

    /**
     * 设置 [计划周期(天)]
     */
    public void setPlancvl(Double plancvl) {
        this.plancvl = plancvl;
        this.modify("plancvl", plancvl);
    }

    /**
     * 设置 [计划名称]
     */
    public void setEmplanname(String emplanname) {
        this.emplanname = emplanname;
        this.modify("emplanname", emplanname);
    }

    /**
     * 设置 [生成工单种类]
     */
    public void setEmwotype(String emwotype) {
        this.emwotype = emwotype;
        this.modify("emwotype", emwotype);
    }

    /**
     * 设置 [计划类型]
     */
    public void setPlantype(String plantype) {
        this.plantype = plantype;
        this.modify("plantype", plantype);
    }

    /**
     * 设置 [详细内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [停运时间(分)]
     */
    public void setEqstoplength(Double eqstoplength) {
        this.eqstoplength = eqstoplength;
        this.modify("eqstoplength", eqstoplength);
    }

    /**
     * 设置 [接收人]
     */
    public void setRecvpersonid(String recvpersonid) {
        this.recvpersonid = recvpersonid;
        this.modify("recvpersonid", recvpersonid);
    }

    /**
     * 设置 [归档]
     */
    public void setArchive(String archive) {
        this.archive = archive;
        this.modify("archive", archive);
    }

    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [计划状态]
     */
    public void setPlanstate(String planstate) {
        this.planstate = planstate;
        this.modify("planstate", planstate);
    }

    /**
     * 设置 [接收人]
     */
    public void setRecvpersonname(String recvpersonname) {
        this.recvpersonname = recvpersonname;
        this.modify("recvpersonname", recvpersonname);
    }

    /**
     * 设置 [服务商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [测点]
     */
    public void setDpid(String dpid) {
        this.dpid = dpid;
        this.modify("dpid", dpid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [计划模板]
     */
    public void setPlantemplid(String plantemplid) {
        this.plantemplid = plantemplid;
        this.modify("plantemplid", plantemplid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }

    /**
     * 设置 [任务时刻]
     */
    public void setCron(String cron) {
        this.cron = cron;
        this.modify("cron", cron);
    }

    /**
     * 设置 [制定人]
     */
    public void setMpersonid(String mpersonid) {
        this.mpersonid = mpersonid;
        this.modify("mpersonid", mpersonid);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptid(String rdeptid) {
        this.rdeptid = rdeptid;
        this.modify("rdeptid", rdeptid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emplanid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


