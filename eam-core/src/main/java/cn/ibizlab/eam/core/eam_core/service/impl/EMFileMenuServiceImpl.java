package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMFileMenu;
import cn.ibizlab.eam.core.eam_core.filter.EMFileMenuSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMFileMenuService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMFileMenuMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[文件夹目录] 服务对象接口实现
 */
@Slf4j
@Service("EMFileMenuServiceImpl")
public class EMFileMenuServiceImpl extends ServiceImpl<EMFileMenuMapper, EMFileMenu> implements IEMFileMenuService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMFileMenu et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmfilemenuid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMFileMenu> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMFileMenu et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emfilemenuid", et.getEmfilemenuid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmfilemenuid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMFileMenu> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMFileMenu get(String key) {
        EMFileMenu et = getById(key);
        if(et == null){
            et = new EMFileMenu();
            et.setEmfilemenuid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMFileMenu getDraft(EMFileMenu et) {
        return et;
    }

    @Override
    public boolean checkKey(EMFileMenu et) {
        return (!ObjectUtils.isEmpty(et.getEmfilemenuid())) && (!Objects.isNull(this.getById(et.getEmfilemenuid())));
    }
    @Override
    @Transactional
    public boolean save(EMFileMenu et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMFileMenu et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMFileMenu> list) {
        List<EMFileMenu> create = new ArrayList<>();
        List<EMFileMenu> update = new ArrayList<>();
        for (EMFileMenu et : list) {
            if (ObjectUtils.isEmpty(et.getEmfilemenuid()) || ObjectUtils.isEmpty(getById(et.getEmfilemenuid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMFileMenu> list) {
        List<EMFileMenu> create = new ArrayList<>();
        List<EMFileMenu> update = new ArrayList<>();
        for (EMFileMenu et : list) {
            if (ObjectUtils.isEmpty(et.getEmfilemenuid()) || ObjectUtils.isEmpty(getById(et.getEmfilemenuid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMFileMenu> searchDefault(EMFileMenuSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMFileMenu> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMFileMenu>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMFileMenu> getEmfilemenuByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMFileMenu> getEmfilemenuByEntities(List<EMFileMenu> entities) {
        List ids =new ArrayList();
        for(EMFileMenu entity : entities){
            Serializable id=entity.getEmfilemenuid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMFileMenuService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



