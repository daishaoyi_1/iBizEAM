package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[模式]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMRFOMO_BASE", resultMap = "EMRFOMOResultMap")
public class EMRFOMO extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 模式名称
     */
    @TableField(value = "emrfomoname")
    @JSONField(name = "emrfomoname")
    @JsonProperty("emrfomoname")
    private String emrfomoname;
    /**
     * 对象编号
     */
    @TableField(exist = false)
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 信息
     */
    @TableField(exist = false)
    @JSONField(name = "rfomoinfo")
    @JsonProperty("rfomoinfo")
    private String rfomoinfo;
    /**
     * 模式代码
     */
    @TableField(value = "rfomocode")
    @JSONField(name = "rfomocode")
    @JsonProperty("rfomocode")
    private String rfomocode;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 模式标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emrfomoid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emrfomoid")
    @JsonProperty("emrfomoid")
    private String emrfomoid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 现象
     */
    @TableField(exist = false)
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    private String rfodename;
    /**
     * 现象
     */
    @TableField(value = "rfodeid")
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    private String rfodeid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode;



    /**
     * 设置 [模式名称]
     */
    public void setEmrfomoname(String emrfomoname) {
        this.emrfomoname = emrfomoname;
        this.modify("emrfomoname", emrfomoname);
    }

    /**
     * 设置 [模式代码]
     */
    public void setRfomocode(String rfomocode) {
        this.rfomocode = rfomocode;
        this.modify("rfomocode", rfomocode);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [现象]
     */
    public void setRfodeid(String rfodeid) {
        this.rfodeid = rfodeid;
        this.modify("rfodeid", rfodeid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emrfomoid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


