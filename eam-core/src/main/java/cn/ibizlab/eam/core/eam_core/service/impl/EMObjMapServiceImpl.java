package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import cn.ibizlab.eam.core.eam_core.filter.EMObjMapSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMObjMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMObjMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[对象关系] 服务对象接口实现
 */
@Slf4j
@Service("EMObjMapServiceImpl")
public class EMObjMapServiceImpl extends ServiceImpl<EMObjMapMapper, EMObjMap> implements IEMObjMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMObjMap et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmobjmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMObjMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMObjMap et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emobjmapid", et.getEmobjmapid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmobjmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMObjMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMObjMap get(String key) {
        EMObjMap et = getById(key);
        if(et == null){
            et = new EMObjMap();
            et.setEmobjmapid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMObjMap getDraft(EMObjMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMObjMap et) {
        return (!ObjectUtils.isEmpty(et.getEmobjmapid())) && (!Objects.isNull(this.getById(et.getEmobjmapid())));
    }
    @Override
    @Transactional
    public boolean save(EMObjMap et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMObjMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMObjMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMObjMap> create = new ArrayList<>();
        List<EMObjMap> update = new ArrayList<>();
        for (EMObjMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmobjmapid()) || ObjectUtils.isEmpty(getById(et.getEmobjmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMObjMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMObjMap> create = new ArrayList<>();
        List<EMObjMap> update = new ArrayList<>();
        for (EMObjMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmobjmapid()) || ObjectUtils.isEmpty(getById(et.getEmobjmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMObjMap> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMObjMap>().eq("objid",emobjectid));
    }

	@Override
    public List<EMObjMap> selectByObjpid(String emobjectid) {
        return baseMapper.selectByObjpid(emobjectid);
    }
    @Override
    public void removeByObjpid(String emobjectid) {
        this.remove(new QueryWrapper<EMObjMap>().eq("objpid",emobjectid));
    }


    /**
     * 查询集合 子位置
     */
    @Override
    public Page<EMObjMap> searchChildLocation(EMObjMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMObjMap> pages=baseMapper.searchChildLocation(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMObjMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMObjMap> searchDefault(EMObjMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMObjMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMObjMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 IndexDER
     */
    @Override
    public Page<EMObjMap> searchIndexDER(EMObjMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMObjMap> pages=baseMapper.searchIndexDER(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMObjMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 LocationByEQ
     */
    @Override
    public Page<EMObjMap> searchLocationByEQ(EMObjMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMObjMap> pages=baseMapper.searchLocationByEQ(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMObjMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMObjMap et){
        //实体关系[DER1N_EMOBJMAP_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setMajorequipname(obj.getMajorequipname());
            et.setMajorequipid(obj.getMajorequipid());
            et.setObjtype(obj.getEmobjecttype());
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMOBJMAP_EMOBJECT_OBJPID]
        if(!ObjectUtils.isEmpty(et.getObjpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject objp=et.getObjp();
            if(ObjectUtils.isEmpty(objp)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjpid());
                et.setObjp(majorEntity);
                objp=majorEntity;
            }
            et.setObjptype(objp.getEmobjecttype());
            et.setObjpname(objp.getEmobjectname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMObjMap> getEmobjmapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMObjMap> getEmobjmapByEntities(List<EMObjMap> entities) {
        List ids =new ArrayList();
        for(EMObjMap entity : entities){
            Serializable id=entity.getEmobjmapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMObjMapService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



