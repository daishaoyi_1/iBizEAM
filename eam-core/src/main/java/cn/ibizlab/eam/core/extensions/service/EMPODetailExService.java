package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.EMItem;
import cn.ibizlab.eam.core.eam_core.domain.EMItemRIn;
import cn.ibizlab.eam.core.eam_core.filter.EMPODetailSearchContext;
import cn.ibizlab.eam.core.eam_core.service.impl.EMPODetailServiceImpl;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMPODetail;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

import java.math.RoundingMode;
import java.util.*;

/**
 * 实体[订单条目] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMPODetailExService")
public class EMPODetailExService extends EMPODetailServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [Check:验货完成] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPODetail check(EMPODetail et) {
        et=this.get(et.getEmpodetailid());
        if (et==null){
            throw new RuntimeException("订单条目不存在,无法完成操作");
        }

        EMPODetailSearchContext empoDetailSearchContext = new EMPODetailSearchContext();
        //et.getEmpodetailid() 存的是一个或多个采购信息的id,用','分隔
        empoDetailSearchContext.setN_empodetailid_in(et.getEmpodetailid());
        List<EMPODetail> empoDetailList = this.searchDefault(empoDetailSearchContext).getContent();
        //查询每个订单条目的实收数，若为0，则直接关闭
        for (EMPODetail empoDetail : empoDetailList) {
            if (empoDetail.getRsum()==0){
                empoDetail.setPodetailstate(StaticDict.EMPODETAILSTATE.ITEM_20.getValue());
                empoDetail.setWfstep(null);
                empoDetail.setWfstate(Integer.parseInt(StaticDict.WFStates.ITEM_2.getValue()));
            }else{
                //待记账
                empoDetail.setWfstep(StaticDict.EMPODETAILWFSTEP.ITEM_20.getValue());
            }
            Aops.getSelf(this).update(empoDetail);
        }
        return super.check(et);
    }
    /**
     * [CreateRin:生成入库单] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPODetail createRin(EMPODetail et) {
        et = this.get(et.getEmpodetailid());
        if (et==null){
            throw new RuntimeException("订单条目不存在,无法完成操作");
        }

        EMItem emItem = emitemService.get(et.getItemid());
        EMItemRIn emItemRIn = new EMItemRIn();
        emItemRIn.setAmount(et.getAmount());
        emItemRIn.setBatcode("NA");
        emItemRIn.setDescription(et.getDescription());
        emItemRIn.setEmpid(et.getEmpid());
        emItemRIn.setItemid(et.getItemid());
        emItemRIn.setOrgid(et.getOrgid());
        emItemRIn.setPodetailid(et.getEmpodetailid());
        emItemRIn.setPrice(NumberUtils.toScaledBigDecimal(et.getRsum()*et.getUnitrate(),2, RoundingMode.HALF_UP).doubleValue());
        emItemRIn.setPsum(NumberUtils.toScaledBigDecimal(et.getRsum()*et.getUnitrate(),3, RoundingMode.HALF_UP).doubleValue());
        emItemRIn.setRinstate(Integer.valueOf(StaticDict.EMTRADESTATE.ITEM_10.getValue()));
        emItemRIn.setWfstep(StaticDict.EMITEMRINWPSTEP.ITEM_10.getValue());
        emItemRIn.setSempid(et.getRempid());
        emItemRIn.setStoreid(emItem.getStoreid());
        emItemRIn.setStorepartid(emItem.getStorepartid());
        emItemRIn.setEmserviceid(et.getLabserviceid());
        emitemrinService.create(emItemRIn);
        return super.createRin(et);
    }
}

