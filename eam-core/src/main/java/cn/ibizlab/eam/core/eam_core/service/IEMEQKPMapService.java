package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQKPMap;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPMapSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQKPMap] 服务对象接口
 */
public interface IEMEQKPMapService extends IService<EMEQKPMap> {

    boolean create(EMEQKPMap et);
    void createBatch(List<EMEQKPMap> list);
    boolean update(EMEQKPMap et);
    void updateBatch(List<EMEQKPMap> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQKPMap get(String key);
    EMEQKPMap getDraft(EMEQKPMap et);
    boolean checkKey(EMEQKPMap et);
    boolean save(EMEQKPMap et);
    void saveBatch(List<EMEQKPMap> list);
    Page<EMEQKPMap> searchDefault(EMEQKPMapSearchContext context);
    List<EMEQKPMap> selectByKpid(String emeqkpid);
    void removeByKpid(String emeqkpid);
    List<EMEQKPMap> selectByRefobjid(String emobjectid);
    void removeByRefobjid(String emobjectid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQKPMap> getEmeqkpmapByIds(List<String> ids);
    List<EMEQKPMap> getEmeqkpmapByEntities(List<EMEQKPMap> entities);
}


