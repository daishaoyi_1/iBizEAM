package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;
/**
 * 关系型数据实体[EMWPListCost] 查询条件对象
 */
@Slf4j
@Data
public class EMWPListCostSearchContext extends QueryWrapperContext<EMWPListCost> {

	private String n_wplistcostinfo_like;//[询价信息]
	public void setN_wplistcostinfo_like(String n_wplistcostinfo_like) {
        this.n_wplistcostinfo_like = n_wplistcostinfo_like;
        if(!ObjectUtils.isEmpty(this.n_wplistcostinfo_like)){
            this.getSearchCond().like("wplistcostinfo", n_wplistcostinfo_like);
        }
    }
	private String n_rempname_eq;//[询价人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[询价人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_rempid_eq;//[询价人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_emwplistcostname_like;//[询价单名称]
	public void setN_emwplistcostname_like(String n_emwplistcostname_like) {
        this.n_emwplistcostname_like = n_emwplistcostname_like;
        if(!ObjectUtils.isEmpty(this.n_emwplistcostname_like)){
            this.getSearchCond().like("emwplistcostname", n_emwplistcostname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_unitname_eq;//[询价单位]
	public void setN_unitname_eq(String n_unitname_eq) {
        this.n_unitname_eq = n_unitname_eq;
        if(!ObjectUtils.isEmpty(this.n_unitname_eq)){
            this.getSearchCond().eq("unitname", n_unitname_eq);
        }
    }
	private String n_unitname_like;//[询价单位]
	public void setN_unitname_like(String n_unitname_like) {
        this.n_unitname_like = n_unitname_like;
        if(!ObjectUtils.isEmpty(this.n_unitname_like)){
            this.getSearchCond().like("unitname", n_unitname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_labservicename_eq;//[产品供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[产品供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_wplistid_eq;//[采购申请号]
	public void setN_wplistid_eq(String n_wplistid_eq) {
        this.n_wplistid_eq = n_wplistid_eq;
        if(!ObjectUtils.isEmpty(this.n_wplistid_eq)){
            this.getSearchCond().eq("wplistid", n_wplistid_eq);
        }
    }
	private String n_labserviceid_eq;//[产品供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_unitid_eq;//[询价单位]
	public void setN_unitid_eq(String n_unitid_eq) {
        this.n_unitid_eq = n_unitid_eq;
        if(!ObjectUtils.isEmpty(this.n_unitid_eq)){
            this.getSearchCond().eq("unitid", n_unitid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emwplistcostname", query)
            );
		 }
	}
}



