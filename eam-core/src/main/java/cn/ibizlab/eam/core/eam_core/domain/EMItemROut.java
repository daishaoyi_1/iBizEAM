package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[退货单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMITEMROUT_BASE", resultMap = "EMItemROutResultMap")
public class EMItemROut extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * sap传输异常文本
     */
    @TableField(value = "sapreason1")
    @JSONField(name = "sapreason1")
    @JsonProperty("sapreason1")
    private String sapreason1;
    /**
     * 退货单名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emitemroutname")
    @JSONField(name = "emitemroutname")
    @JsonProperty("emitemroutname")
    private String emitemroutname;
    /**
     * 批次
     */
    @DEField(defaultValue = "NA")
    @TableField(value = "batcode")
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    private String batcode;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 单价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;
    /**
     * 退货单号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emitemroutid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emitemroutid")
    @JsonProperty("emitemroutid")
    private String emitemroutid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 总金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * sap传输状态
     */
    @TableField(value = "sap")
    @JSONField(name = "sap")
    @JsonProperty("sap")
    private String sap;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 出库日期
     */
    @TableField(value = "sdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "sdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    private Timestamp sdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 退货单信息
     */
    @TableField(exist = false)
    @JSONField(name = "itemroutinfo")
    @JsonProperty("itemroutinfo")
    private String itemroutinfo;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 退货状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "tradestate")
    @JSONField(name = "tradestate")
    @JsonProperty("tradestate")
    private Integer tradestate;
    /**
     * 税费
     */
    @TableField(value = "shf")
    @JSONField(name = "shf")
    @JsonProperty("shf")
    private Double shf;
    /**
     * sap控制
     */
    @TableField(value = "sapcontrol")
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    private Integer sapcontrol;
    /**
     * sap传输失败原因
     */
    @TableField(value = "sapreason")
    @JSONField(name = "sapreason")
    @JsonProperty("sapreason")
    private String sapreason;
    /**
     * 数量
     */
    @TableField(value = "psum")
    @JSONField(name = "psum")
    @JsonProperty("psum")
    private Double psum;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    private String teamid;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    private String labserviceid;
    /**
     * 发票号
     */
    @TableField(exist = false)
    @JSONField(name = "civo")
    @JsonProperty("civo")
    private String civo;
    /**
     * 库位
     */
    @TableField(exist = false)
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    private String storepartname;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "storename")
    @JsonProperty("storename")
    private String storename;
    /**
     * 入库单
     */
    @TableField(exist = false)
    @JSONField(name = "rname")
    @JsonProperty("rname")
    private String rname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    private String itemname;
    /**
     * 仓库
     */
    @TableField(value = "storeid")
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    private String storeid;
    /**
     * 入库单
     */
    @TableField(value = "rid")
    @JSONField(name = "rid")
    @JsonProperty("rid")
    private String rid;
    /**
     * 库位
     */
    @TableField(value = "storepartid")
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    private String storepartid;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    private String itemid;
    /**
     * 退货人
     */
    @TableField(value = "sempid")
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    private String sempid;
    /**
     * 退货人
     */
    @TableField(exist = false)
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    private String sempname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemRIn r;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStore store;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp spfempid;



    /**
     * 设置 [sap传输异常文本]
     */
    public void setSapreason1(String sapreason1) {
        this.sapreason1 = sapreason1;
        this.modify("sapreason1", sapreason1);
    }

    /**
     * 设置 [退货单名称]
     */
    public void setEmitemroutname(String emitemroutname) {
        this.emitemroutname = emitemroutname;
        this.modify("emitemroutname", emitemroutname);
    }

    /**
     * 设置 [批次]
     */
    public void setBatcode(String batcode) {
        this.batcode = batcode;
        this.modify("batcode", batcode);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [总金额]
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [sap传输状态]
     */
    public void setSap(String sap) {
        this.sap = sap;
        this.modify("sap", sap);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [出库日期]
     */
    public void setSdate(Timestamp sdate) {
        this.sdate = sdate;
        this.modify("sdate", sdate);
    }

    /**
     * 格式化日期 [出库日期]
     */
    public String formatSdate() {
        if (this.sdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sdate);
    }
    /**
     * 设置 [退货状态]
     */
    public void setTradestate(Integer tradestate) {
        this.tradestate = tradestate;
        this.modify("tradestate", tradestate);
    }

    /**
     * 设置 [税费]
     */
    public void setShf(Double shf) {
        this.shf = shf;
        this.modify("shf", shf);
    }

    /**
     * 设置 [sap控制]
     */
    public void setSapcontrol(Integer sapcontrol) {
        this.sapcontrol = sapcontrol;
        this.modify("sapcontrol", sapcontrol);
    }

    /**
     * 设置 [sap传输失败原因]
     */
    public void setSapreason(String sapreason) {
        this.sapreason = sapreason;
        this.modify("sapreason", sapreason);
    }

    /**
     * 设置 [数量]
     */
    public void setPsum(Double psum) {
        this.psum = psum;
        this.modify("psum", psum);
    }

    /**
     * 设置 [仓库]
     */
    public void setStoreid(String storeid) {
        this.storeid = storeid;
        this.modify("storeid", storeid);
    }

    /**
     * 设置 [入库单]
     */
    public void setRid(String rid) {
        this.rid = rid;
        this.modify("rid", rid);
    }

    /**
     * 设置 [库位]
     */
    public void setStorepartid(String storepartid) {
        this.storepartid = storepartid;
        this.modify("storepartid", storepartid);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [退货人]
     */
    public void setSempid(String sempid) {
        this.sempid = sempid;
        this.modify("sempid", sempid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emitemroutid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


