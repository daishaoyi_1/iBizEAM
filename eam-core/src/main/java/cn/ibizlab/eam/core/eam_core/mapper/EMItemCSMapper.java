package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMItemCS;
import cn.ibizlab.eam.core.eam_core.filter.EMItemCSSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMItemCSMapper extends BaseMapper<EMItemCS> {

    Page<EMItemCS> searchConfirmed(IPage page, @Param("srf") EMItemCSSearchContext context, @Param("ew") Wrapper<EMItemCS> wrapper);
    Page<EMItemCS> searchDefault(IPage page, @Param("srf") EMItemCSSearchContext context, @Param("ew") Wrapper<EMItemCS> wrapper);
    Page<EMItemCS> searchDraft(IPage page, @Param("srf") EMItemCSSearchContext context, @Param("ew") Wrapper<EMItemCS> wrapper);
    Page<EMItemCS> searchToConfirm(IPage page, @Param("srf") EMItemCSSearchContext context, @Param("ew") Wrapper<EMItemCS> wrapper);
    @Override
    EMItemCS selectById(Serializable id);
    @Override
    int insert(EMItemCS entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMItemCS entity);
    @Override
    int update(@Param(Constants.ENTITY) EMItemCS entity, @Param("ew") Wrapper<EMItemCS> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMItemCS> selectByRid(@Param("emitemrinid") Serializable emitemrinid);

    List<EMItemCS> selectByItemid(@Param("emitemid") Serializable emitemid);

    List<EMItemCS> selectByStockid(@Param("emstockid") Serializable emstockid);

    List<EMItemCS> selectByStorepartid(@Param("emstorepartid") Serializable emstorepartid);

    List<EMItemCS> selectByStoreid(@Param("emstoreid") Serializable emstoreid);

    List<EMItemCS> selectBySempid(@Param("pfempid") Serializable pfempid);

}
