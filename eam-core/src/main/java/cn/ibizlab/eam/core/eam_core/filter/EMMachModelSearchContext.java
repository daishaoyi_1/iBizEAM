package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMMachModel;
/**
 * 关系型数据实体[EMMachModel] 查询条件对象
 */
@Slf4j
@Data
public class EMMachModelSearchContext extends QueryWrapperContext<EMMachModel> {

	private String n_emmachmodelname_like;//[机型名称]
	public void setN_emmachmodelname_like(String n_emmachmodelname_like) {
        this.n_emmachmodelname_like = n_emmachmodelname_like;
        if(!ObjectUtils.isEmpty(this.n_emmachmodelname_like)){
            this.getSearchCond().like("emmachmodelname", n_emmachmodelname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emmachmodelname", query)
            );
		 }
	}
}



