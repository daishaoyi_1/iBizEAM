

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_O;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface PLANSCHEDULE_OInheritMapping {

    @Mappings({
        @Mapping(source ="planscheduleOid",target = "planscheduleid"),
        @Mapping(source ="planscheduleOname",target = "planschedulename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE toPlanschedule(PLANSCHEDULE_O minorEntity);

    @Mappings({
        @Mapping(source ="planscheduleid" ,target = "planscheduleOid"),
        @Mapping(source ="planschedulename" ,target = "planscheduleOname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE_O toPlanscheduleO(PLANSCHEDULE majorEntity);

    List<PLANSCHEDULE> toPlanschedule(List<PLANSCHEDULE_O> minorEntities);

    List<PLANSCHEDULE_O> toPlanscheduleO(List<PLANSCHEDULE> majorEntities);

}


