package cn.ibizlab.eam.core.eam_core.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;

/**
 * 关系型数据实体[Remove__MSDeny] 对象
 */
public interface IEMItemPRtnRemove__MSDenyLogic {

    void execute(EMItemPRtn et) ;

}
