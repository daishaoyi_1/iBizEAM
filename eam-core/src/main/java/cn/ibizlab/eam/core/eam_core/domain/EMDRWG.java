package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[文档]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMDRWG_BASE", resultMap = "EMDRWGResultMap")
public class EMDRWG extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最后借阅人
     */
    @TableField(value = "bpersonid")
    @JSONField(name = "bpersonid")
    @JsonProperty("bpersonid")
    private String bpersonid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 保管人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 文档代码
     */
    @TableField(value = "drwgcode")
    @JSONField(name = "drwgcode")
    @JsonProperty("drwgcode")
    private String drwgcode;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 保管人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 档案文件
     */
    @TableField(value = "efilecontent")
    @JSONField(name = "efilecontent")
    @JsonProperty("efilecontent")
    private String efilecontent;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 文档类型
     */
    @TableField(value = "drwgtype")
    @JSONField(name = "drwgtype")
    @JsonProperty("drwgtype")
    private String drwgtype;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 存放位置
     */
    @TableField(value = "lct")
    @JSONField(name = "lct")
    @JsonProperty("lct")
    private String lct;
    /**
     * 文档名称
     */
    @TableField(value = "emdrwgname")
    @JSONField(name = "emdrwgname")
    @JsonProperty("emdrwgname")
    private String emdrwgname;
    /**
     * 文档状态
     */
    @DEField(defaultValue = "1")
    @TableField(value = "drwgstate")
    @JSONField(name = "drwgstate")
    @JsonProperty("drwgstate")
    private String drwgstate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 文档标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emdrwgid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emdrwgid")
    @JsonProperty("emdrwgid")
    private String emdrwgid;
    /**
     * 文档信息
     */
    @TableField(exist = false)
    @JSONField(name = "drwginfo")
    @JsonProperty("drwginfo")
    private String drwginfo;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 最后借阅人
     */
    @TableField(value = "bpersonname")
    @JSONField(name = "bpersonname")
    @JsonProperty("bpersonname")
    private String bpersonname;
    /**
     * 部门查询
     */
    @TableField(exist = false)
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    private String deptid;



    /**
     * 设置 [最后借阅人]
     */
    public void setBpersonid(String bpersonid) {
        this.bpersonid = bpersonid;
        this.modify("bpersonid", bpersonid);
    }

    /**
     * 设置 [内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [保管人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [文档代码]
     */
    public void setDrwgcode(String drwgcode) {
        this.drwgcode = drwgcode;
        this.modify("drwgcode", drwgcode);
    }

    /**
     * 设置 [保管人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [档案文件]
     */
    public void setEfilecontent(String efilecontent) {
        this.efilecontent = efilecontent;
        this.modify("efilecontent", efilecontent);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [文档类型]
     */
    public void setDrwgtype(String drwgtype) {
        this.drwgtype = drwgtype;
        this.modify("drwgtype", drwgtype);
    }

    /**
     * 设置 [存放位置]
     */
    public void setLct(String lct) {
        this.lct = lct;
        this.modify("lct", lct);
    }

    /**
     * 设置 [文档名称]
     */
    public void setEmdrwgname(String emdrwgname) {
        this.emdrwgname = emdrwgname;
        this.modify("emdrwgname", emdrwgname);
    }

    /**
     * 设置 [文档状态]
     */
    public void setDrwgstate(String drwgstate) {
        this.drwgstate = drwgstate;
        this.modify("drwgstate", drwgstate);
    }

    /**
     * 设置 [最后借阅人]
     */
    public void setBpersonname(String bpersonname) {
        this.bpersonname = bpersonname;
        this.modify("bpersonname", bpersonname);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emdrwgid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


