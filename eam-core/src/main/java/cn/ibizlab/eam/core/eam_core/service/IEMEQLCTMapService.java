package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTMap;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTMapSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQLCTMap] 服务对象接口
 */
public interface IEMEQLCTMapService extends IService<EMEQLCTMap> {

    boolean create(EMEQLCTMap et);
    void createBatch(List<EMEQLCTMap> list);
    boolean update(EMEQLCTMap et);
    void updateBatch(List<EMEQLCTMap> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQLCTMap get(String key);
    EMEQLCTMap getDraft(EMEQLCTMap et);
    boolean checkKey(EMEQLCTMap et);
    boolean save(EMEQLCTMap et);
    void saveBatch(List<EMEQLCTMap> list);
    Page<EMEQLCTMap> searchByLocation(EMEQLCTMapSearchContext context);
    Page<EMEQLCTMap> searchChildLocation(EMEQLCTMapSearchContext context);
    Page<EMEQLCTMap> searchDefault(EMEQLCTMapSearchContext context);
    Page<EMEQLCTMap> searchRootLocation(EMEQLCTMapSearchContext context);
    List<EMEQLCTMap> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMEQLCTMap> selectByEqlocationpid(String emeqlocationid);
    void removeByEqlocationpid(String emeqlocationid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQLCTMap> getEmeqlctmapByIds(List<String> ids);
    List<EMEQLCTMap> getEmeqlctmapByEntities(List<EMEQLCTMap> entities);
}


