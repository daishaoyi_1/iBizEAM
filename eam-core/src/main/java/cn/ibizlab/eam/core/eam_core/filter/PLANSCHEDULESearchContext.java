package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
/**
 * 关系型数据实体[PLANSCHEDULE] 查询条件对象
 */
@Slf4j
@Data
public class PLANSCHEDULESearchContext extends QueryWrapperContext<PLANSCHEDULE> {

	private String n_planschedulename_like;//[计划时刻设置名称]
	public void setN_planschedulename_like(String n_planschedulename_like) {
        this.n_planschedulename_like = n_planschedulename_like;
        if(!ObjectUtils.isEmpty(this.n_planschedulename_like)){
            this.getSearchCond().like("planschedulename", n_planschedulename_like);
        }
    }
	private String n_scheduletype_eq;//[时刻类型]
	public void setN_scheduletype_eq(String n_scheduletype_eq) {
        this.n_scheduletype_eq = n_scheduletype_eq;
        if(!ObjectUtils.isEmpty(this.n_scheduletype_eq)){
            this.getSearchCond().eq("scheduletype", n_scheduletype_eq);
        }
    }
	private String n_schedulestate_eq;//[时刻设置状态]
	public void setN_schedulestate_eq(String n_schedulestate_eq) {
        this.n_schedulestate_eq = n_schedulestate_eq;
        if(!ObjectUtils.isEmpty(this.n_schedulestate_eq)){
            this.getSearchCond().eq("schedulestate", n_schedulestate_eq);
        }
    }
	private String n_emplanid_eq;//[计划编号]
	public void setN_emplanid_eq(String n_emplanid_eq) {
        this.n_emplanid_eq = n_emplanid_eq;
        if(!ObjectUtils.isEmpty(this.n_emplanid_eq)){
            this.getSearchCond().eq("emplanid", n_emplanid_eq);
        }
    }
	private String n_emplanname_eq;//[计划名称]
	public void setN_emplanname_eq(String n_emplanname_eq) {
        this.n_emplanname_eq = n_emplanname_eq;
        if(!ObjectUtils.isEmpty(this.n_emplanname_eq)){
            this.getSearchCond().eq("emplanname", n_emplanname_eq);
        }
    }
	private String n_emplanname_like;//[计划名称]
	public void setN_emplanname_like(String n_emplanname_like) {
        this.n_emplanname_like = n_emplanname_like;
        if(!ObjectUtils.isEmpty(this.n_emplanname_like)){
            this.getSearchCond().like("emplanname", n_emplanname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("planschedulename", query)
            );
		 }
	}
}



