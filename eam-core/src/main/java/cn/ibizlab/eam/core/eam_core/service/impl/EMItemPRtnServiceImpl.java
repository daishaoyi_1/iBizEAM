package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPRtnSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMItemPRtnMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[还料单] 服务对象接口实现
 */
@Slf4j
@Service("EMItemPRtnServiceImpl")
public class EMItemPRtnServiceImpl extends ServiceImpl<EMItemPRtnMapper, EMItemPRtn> implements IEMItemPRtnService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStorePartService emstorepartService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStoreService emstoreService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMItemPRtn et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemprtnid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMItemPRtn> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMItemPRtn et) {
        fillParentData(et);
        emitemtradeService.update(emitemprtnInheritMapping.toEmitemtrade(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emitemprtnid", et.getEmitemprtnid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemprtnid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMItemPRtn> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emitemtradeService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMItemPRtn get(String key) {
        EMItemPRtn et = getById(key);
        if(et == null){
            et = new EMItemPRtn();
            et.setEmitemprtnid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMItemPRtn getDraft(EMItemPRtn et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMItemPRtn et) {
        return (!ObjectUtils.isEmpty(et.getEmitemprtnid())) && (!Objects.isNull(this.getById(et.getEmitemprtnid())));
    }
    @Override
    @Transactional
    public EMItemPRtn confirm(EMItemPRtn et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean confirmBatch(List<EMItemPRtn> etList) {
        for(EMItemPRtn et : etList) {
            confirm(et);
        }
        return true;
    }

    @Override
    @Transactional
    public EMItemPRtn formUpdateByEmitempuseid(EMItemPRtn et) {
         return et ;
    }

    @Override
    @Transactional
    public EMItemPRtn rejected(EMItemPRtn et) {
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMItemPRtn et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMItemPRtn et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMItemPRtn> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemPRtn> create = new ArrayList<>();
        List<EMItemPRtn> update = new ArrayList<>();
        for (EMItemPRtn et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemprtnid()) || ObjectUtils.isEmpty(getById(et.getEmitemprtnid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMItemPRtn> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemPRtn> create = new ArrayList<>();
        List<EMItemPRtn> update = new ArrayList<>();
        for (EMItemPRtn et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemprtnid()) || ObjectUtils.isEmpty(getById(et.getEmitemprtnid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMItemPRtn> selectByDeptid(String emitempuseid) {
        return baseMapper.selectByDeptid(emitempuseid);
    }
    @Override
    public void removeByDeptid(String emitempuseid) {
        this.remove(new QueryWrapper<EMItemPRtn>().eq("deptid",emitempuseid));
    }

	@Override
    public List<EMItemPRtn> selectByRid(String emitempuseid) {
        return baseMapper.selectByRid(emitempuseid);
    }
    @Override
    public void removeByRid(String emitempuseid) {
        this.remove(new QueryWrapper<EMItemPRtn>().eq("rid",emitempuseid));
    }

	@Override
    public List<EMItemPRtn> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }
    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMItemPRtn>().eq("itemid",emitemid));
    }

	@Override
    public List<EMItemPRtn> selectByStorepartid(String emstorepartid) {
        return baseMapper.selectByStorepartid(emstorepartid);
    }
    @Override
    public void removeByStorepartid(String emstorepartid) {
        this.remove(new QueryWrapper<EMItemPRtn>().eq("storepartid",emstorepartid));
    }

	@Override
    public List<EMItemPRtn> selectByStoreid(String emstoreid) {
        return baseMapper.selectByStoreid(emstoreid);
    }
    @Override
    public void removeByStoreid(String emstoreid) {
        this.remove(new QueryWrapper<EMItemPRtn>().eq("storeid",emstoreid));
    }

	@Override
    public List<EMItemPRtn> selectByEmpid(String pfempid) {
        return baseMapper.selectByEmpid(pfempid);
    }
    @Override
    public void removeByEmpid(String pfempid) {
        this.remove(new QueryWrapper<EMItemPRtn>().eq("empid",pfempid));
    }

	@Override
    public List<EMItemPRtn> selectBySempid(String pfempid) {
        return baseMapper.selectBySempid(pfempid);
    }
    @Override
    public void removeBySempid(String pfempid) {
        this.remove(new QueryWrapper<EMItemPRtn>().eq("sempid",pfempid));
    }


    /**
     * 查询集合 已确认
     */
    @Override
    public Page<EMItemPRtn> searchConfirmed(EMItemPRtnSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPRtn> pages=baseMapper.searchConfirmed(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPRtn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMItemPRtn> searchDefault(EMItemPRtnSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPRtn> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPRtn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 草稿
     */
    @Override
    public Page<EMItemPRtn> searchDraft(EMItemPRtnSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPRtn> pages=baseMapper.searchDraft(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPRtn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待确认
     */
    @Override
    public Page<EMItemPRtn> searchToConfirm(EMItemPRtnSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPRtn> pages=baseMapper.searchToConfirm(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPRtn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMItemPRtn et){
        //实体关系[DER1N_EMITEMPRTN_EMITEMPUSE_RID]
        if(!ObjectUtils.isEmpty(et.getRid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemPUse r=et.getR();
            if(ObjectUtils.isEmpty(r)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemPUse majorEntity=emitempuseService.get(et.getRid());
                et.setR(majorEntity);
                r=majorEntity;
            }
            et.setRname(r.getItempuseinfo());
            et.setPusetype(r.getPusetype());
        }
        //实体关系[DER1N_EMITEMPRTN_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setUnitname(item.getUnitname());
            et.setItemname(item.getEmitemname());
            et.setUnitid(item.getUnitid());
            et.setAvgprice(item.getPrice());
        }
        //实体关系[DER1N_EMITEMPRTN_EMSTOREPART_STOREPARTID]
        if(!ObjectUtils.isEmpty(et.getStorepartid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart=et.getStorepart();
            if(ObjectUtils.isEmpty(storepart)){
                cn.ibizlab.eam.core.eam_core.domain.EMStorePart majorEntity=emstorepartService.get(et.getStorepartid());
                et.setStorepart(majorEntity);
                storepart=majorEntity;
            }
            et.setStorepartname(storepart.getEmstorepartname());
        }
        //实体关系[DER1N_EMITEMPRTN_EMSTORE_STOREID]
        if(!ObjectUtils.isEmpty(et.getStoreid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStore store=et.getStore();
            if(ObjectUtils.isEmpty(store)){
                cn.ibizlab.eam.core.eam_core.domain.EMStore majorEntity=emstoreService.get(et.getStoreid());
                et.setStore(majorEntity);
                store=majorEntity;
            }
            et.setStorename(store.getEmstorename());
        }
        //实体关系[DER1N_EMITEMPRTN_PFEMP_EMPID]
        if(!ObjectUtils.isEmpty(et.getEmpid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid=et.getPfempid();
            if(ObjectUtils.isEmpty(pfempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getEmpid());
                et.setPfempid(majorEntity);
                pfempid=majorEntity;
            }
            et.setEmpname(pfempid.getPfempname());
        }
        //实体关系[DER1N_EMITEMPRTN_PFEMP_SEMPID]
        if(!ObjectUtils.isEmpty(et.getSempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp spfempid=et.getSpfempid();
            if(ObjectUtils.isEmpty(spfempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getSempid());
                et.setSpfempid(majorEntity);
                spfempid=majorEntity;
            }
            et.setSempname(spfempid.getEmpinfo());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMItemPRtnInheritMapping emitemprtnInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMItemPRtn et){
        if(ObjectUtils.isEmpty(et.getEmitemprtnid()))
            et.setEmitemprtnid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMItemTrade emitemtrade =emitemprtnInheritMapping.toEmitemtrade(et);
        emitemtrade.set("emitemtradetype","PRTN");
        emitemtradeService.create(emitemtrade);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMItemPRtn> getEmitemprtnByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMItemPRtn> getEmitemprtnByEntities(List<EMItemPRtn> entities) {
        List ids =new ArrayList();
        for(EMItemPRtn entity : entities){
            Serializable id=entity.getEmitemprtnid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMItemPRtnService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



