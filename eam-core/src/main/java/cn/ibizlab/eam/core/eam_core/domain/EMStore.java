package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[仓库]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMSTORE_BASE", resultMap = "EMStoreResultMap")
public class EMStore extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 仓库代码
     */
    @TableField(value = "storecode")
    @JSONField(name = "storecode")
    @JsonProperty("storecode")
    private String storecode;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 仓库信息
     */
    @TableField(exist = false)
    @JSONField(name = "storeinfo")
    @JsonProperty("storeinfo")
    private String storeinfo;
    /**
     * 标准价标志
     */
    @TableField(value = "standpriceflag")
    @JSONField(name = "standpriceflag")
    @JsonProperty("standpriceflag")
    private Integer standpriceflag;
    /**
     * 加权平均标志
     */
    @TableField(value = "poweravgflag")
    @JSONField(name = "poweravgflag")
    @JsonProperty("poweravgflag")
    private Integer poweravgflag;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 仓库标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emstoreid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emstoreid")
    @JsonProperty("emstoreid")
    private String emstoreid;
    /**
     * NEW仓库类型
     */
    @TableField(value = "newstoretypeid")
    @JSONField(name = "newstoretypeid")
    @JsonProperty("newstoretypeid")
    private String newstoretypeid;
    /**
     * 成本中心
     */
    @TableField(value = "costcenterid")
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    private String costcenterid;
    /**
     * 主管经理
     */
    @TableField(value = "mgrpersonid")
    @JSONField(name = "mgrpersonid")
    @JsonProperty("mgrpersonid")
    private String mgrpersonid;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 出入算法
     */
    @TableField(value = "ioalgo")
    @JSONField(name = "ioalgo")
    @JsonProperty("ioalgo")
    private String ioalgo;
    /**
     * 仓库类型
     */
    @TableField(value = "storetypeid")
    @JSONField(name = "storetypeid")
    @JsonProperty("storetypeid")
    private String storetypeid;
    /**
     * 地址
     */
    @TableField(value = "storeaddr")
    @JSONField(name = "storeaddr")
    @JsonProperty("storeaddr")
    private String storeaddr;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 仓库名称
     */
    @TableField(value = "emstorename")
    @JSONField(name = "emstorename")
    @JsonProperty("emstorename")
    private String emstorename;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 联系电话
     */
    @TableField(value = "storetel")
    @JSONField(name = "storetel")
    @JsonProperty("storetel")
    private String storetel;
    /**
     * 传真
     */
    @TableField(value = "storefax")
    @JSONField(name = "storefax")
    @JsonProperty("storefax")
    private String storefax;
    /**
     * 库管员
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 库管员
     */
    @TableField(exist = false)
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [仓库代码]
     */
    public void setStorecode(String storecode) {
        this.storecode = storecode;
        this.modify("storecode", storecode);
    }

    /**
     * 设置 [标准价标志]
     */
    public void setStandpriceflag(Integer standpriceflag) {
        this.standpriceflag = standpriceflag;
        this.modify("standpriceflag", standpriceflag);
    }

    /**
     * 设置 [加权平均标志]
     */
    public void setPoweravgflag(Integer poweravgflag) {
        this.poweravgflag = poweravgflag;
        this.modify("poweravgflag", poweravgflag);
    }

    /**
     * 设置 [NEW仓库类型]
     */
    public void setNewstoretypeid(String newstoretypeid) {
        this.newstoretypeid = newstoretypeid;
        this.modify("newstoretypeid", newstoretypeid);
    }

    /**
     * 设置 [成本中心]
     */
    public void setCostcenterid(String costcenterid) {
        this.costcenterid = costcenterid;
        this.modify("costcenterid", costcenterid);
    }

    /**
     * 设置 [主管经理]
     */
    public void setMgrpersonid(String mgrpersonid) {
        this.mgrpersonid = mgrpersonid;
        this.modify("mgrpersonid", mgrpersonid);
    }

    /**
     * 设置 [出入算法]
     */
    public void setIoalgo(String ioalgo) {
        this.ioalgo = ioalgo;
        this.modify("ioalgo", ioalgo);
    }

    /**
     * 设置 [仓库类型]
     */
    public void setStoretypeid(String storetypeid) {
        this.storetypeid = storetypeid;
        this.modify("storetypeid", storetypeid);
    }

    /**
     * 设置 [地址]
     */
    public void setStoreaddr(String storeaddr) {
        this.storeaddr = storeaddr;
        this.modify("storeaddr", storeaddr);
    }

    /**
     * 设置 [仓库名称]
     */
    public void setEmstorename(String emstorename) {
        this.emstorename = emstorename;
        this.modify("emstorename", emstorename);
    }

    /**
     * 设置 [联系电话]
     */
    public void setStoretel(String storetel) {
        this.storetel = storetel;
        this.modify("storetel", storetel);
    }

    /**
     * 设置 [传真]
     */
    public void setStorefax(String storefax) {
        this.storefax = storefax;
        this.modify("storefax", storefax);
    }

    /**
     * 设置 [库管员]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emstoreid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


