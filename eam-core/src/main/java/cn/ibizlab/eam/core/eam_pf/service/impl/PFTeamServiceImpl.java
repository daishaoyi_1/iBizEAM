package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFTeam;
import cn.ibizlab.eam.core.eam_pf.filter.PFTeamSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFTeamService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFTeamMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[班组] 服务对象接口实现
 */
@Slf4j
@Service("PFTeamServiceImpl")
public class PFTeamServiceImpl extends ServiceImpl<PFTeamMapper, PFTeam> implements IPFTeamService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMApplyService emapplyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssessMentService emassessmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssessService emassessService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDisableAssessService emdisableassessService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQAHService emeqahService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQCheckService emeqcheckService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQDebugService emeqdebugService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKeepService emeqkeepService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMaintanceService emeqmaintanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSetupService emeqsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMJYJLService emjyjlService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanTemplService emplantemplService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMProductService emproductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMSubListService emsublistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWeeklyPlanService emweeklyplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwoDpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwoEnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwoOscService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_PTService emwoPtService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpPostMapService pfemppostmapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFTeam et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfteamid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFTeam> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFTeam et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("pfteamid", et.getPfteamid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfteamid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFTeam> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFTeam get(String key) {
        PFTeam et = getById(key);
        if(et == null){
            et = new PFTeam();
            et.setPfteamid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PFTeam getDraft(PFTeam et) {
        return et;
    }

    @Override
    public boolean checkKey(PFTeam et) {
        return (!ObjectUtils.isEmpty(et.getPfteamid())) && (!Objects.isNull(this.getById(et.getPfteamid())));
    }
    @Override
    @Transactional
    public boolean save(PFTeam et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFTeam et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFTeam> list) {
        List<PFTeam> create = new ArrayList<>();
        List<PFTeam> update = new ArrayList<>();
        for (PFTeam et : list) {
            if (ObjectUtils.isEmpty(et.getPfteamid()) || ObjectUtils.isEmpty(getById(et.getPfteamid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFTeam> list) {
        List<PFTeam> create = new ArrayList<>();
        List<PFTeam> update = new ArrayList<>();
        for (PFTeam et : list) {
            if (ObjectUtils.isEmpty(et.getPfteamid()) || ObjectUtils.isEmpty(getById(et.getPfteamid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFTeam> searchDefault(PFTeamSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFTeam> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFTeam>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFTeam> getPfteamByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFTeam> getPfteamByEntities(List<PFTeam> entities) {
        List ids =new ArrayList();
        for(PFTeam entity : entities){
            Serializable id=entity.getPfteamid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPFTeamService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



