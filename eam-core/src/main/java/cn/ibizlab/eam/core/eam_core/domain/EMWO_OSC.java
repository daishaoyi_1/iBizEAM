package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[外委保养工单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMWO_OSC_BASE", resultMap = "EMWO_OSCResultMap")
public class EMWO_OSC extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 持续时间(H)
     */
    @DEField(defaultValue = "8")
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    private Double activelengths;
    /**
     * 工单名称
     */
    @DEField(name = "emwo_oscname")
    @TableField(value = "emwo_oscname")
    @JSONField(name = "emwo_oscname")
    @JsonProperty("emwo_oscname")
    private String emwoOscname;
    /**
     * 钢丝绳润滑油
     */
    @TableField(value = "arg4")
    @JSONField(name = "arg4")
    @JsonProperty("arg4")
    private Double arg4;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 工单分组
     */
    @TableField(exist = false)
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    private String emwotype;
    /**
     * 值
     */
    @TableField(value = "val")
    @JSONField(name = "val")
    @JsonProperty("val")
    private String val;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 执行结果
     */
    @TableField(value = "wresult")
    @JSONField(name = "wresult")
    @JsonProperty("wresult")
    private String wresult;
    /**
     * 起始时间
     */
    @TableField(value = "regionbegindate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionbegindate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    private Timestamp regionbegindate;
    /**
     * 柴油
     */
    @TableField(value = "arg3")
    @JSONField(name = "arg3")
    @JsonProperty("arg3")
    private Double arg3;
    /**
     * 实际工时(分)
     */
    @TableField(value = "worklength")
    @JSONField(name = "worklength")
    @JsonProperty("worklength")
    private Double worklength;
    /**
     * 工单编号
     */
    @DEField(name = "emwo_oscid", isKeyField = true)
    @TableId(value = "emwo_oscid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emwo_oscid")
    @JsonProperty("emwo_oscid")
    private String emwoOscid;
    /**
     * 制定时间
     */
    @TableField(value = "mdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "mdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    private Timestamp mdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 结束时间
     */
    @TableField(value = "regionenddate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionenddate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionenddate")
    private Timestamp regionenddate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 数值
     */
    @TableField(value = "nval")
    @JSONField(name = "nval")
    @JsonProperty("nval")
    private Double nval;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 约合材料费(￥)
     */
    @TableField(value = "mfee")
    @JSONField(name = "mfee")
    @JsonProperty("mfee")
    private Double mfee;
    /**
     * 过期日期
     */
    @TableField(value = "expiredate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "expiredate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expiredate")
    private Timestamp expiredate;
    /**
     * 黄油
     */
    @TableField(value = "arg1")
    @JSONField(name = "arg1")
    @JsonProperty("arg1")
    private Double arg1;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 详细内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 棉纱
     */
    @TableField(value = "arg2")
    @JSONField(name = "arg2")
    @JsonProperty("arg2")
    private Double arg2;
    /**
     * 归档
     */
    @TableField(value = "archive")
    @JSONField(name = "archive")
    @JsonProperty("archive")
    private String archive;
    /**
     * 工单分组
     */
    @DEField(defaultValue = "TASK")
    @TableField(value = "wogroup")
    @JSONField(name = "wogroup")
    @JsonProperty("wogroup")
    private String wogroup;
    /**
     * 工单组
     */
    @TableField(exist = false)
    @JSONField(name = "woteam")
    @JsonProperty("woteam")
    private String woteam;
    /**
     * 工单类型
     */
    @TableField(value = "wotype")
    @JSONField(name = "wotype")
    @JsonProperty("wotype")
    private String wotype;
    /**
     * 优先级
     */
    @DEField(defaultValue = "2")
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 预算(￥)
     */
    @TableField(value = "prefee")
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    private Double prefee;
    /**
     * 工单状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "wostate")
    @JSONField(name = "wostate")
    @JsonProperty("wostate")
    private Integer wostate;
    /**
     * 执行日期
     */
    @TableField(value = "wodate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "wodate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("wodate")
    private Timestamp wodate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 停运时间(分)
     */
    @TableField(value = "eqstoplength")
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    private Double eqstoplength;
    /**
     * 转计划标志
     */
    @TableField(value = "cplanflag")
    @JSONField(name = "cplanflag")
    @JsonProperty("cplanflag")
    private Integer cplanflag;
    /**
     * 倍率
     */
    @DEField(defaultValue = "1")
    @TableField(value = "vrate")
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    private Double vrate;
    /**
     * 金属去污粉
     */
    @TableField(value = "arg5")
    @JSONField(name = "arg5")
    @JsonProperty("arg5")
    private Double arg5;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 工单内容
     */
    @TableField(value = "wodesc")
    @JSONField(name = "wodesc")
    @JsonProperty("wodesc")
    private String wodesc;
    /**
     * 来源类型
     */
    @TableField(exist = false)
    @JSONField(name = "wooritype")
    @JsonProperty("wooritype")
    private String wooritype;
    /**
     * 方案
     */
    @TableField(exist = false)
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    private String rfoacname;
    /**
     * 上级工单
     */
    @TableField(exist = false)
    @JSONField(name = "wopname")
    @JsonProperty("wopname")
    private String wopname;
    /**
     * 现象
     */
    @TableField(exist = false)
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    private String rfodename;
    /**
     * 原因
     */
    @TableField(exist = false)
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    private String rfocaname;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    private String acclassname;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    private String rteamname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    private String rservicename;
    /**
     * 测点类型
     */
    @TableField(exist = false)
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    private String dptype;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipcode")
    @JsonProperty("equipcode")
    private String equipcode;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 模式
     */
    @TableField(exist = false)
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    private String rfomoname;
    /**
     * 工单来源
     */
    @TableField(exist = false)
    @JSONField(name = "wooriname")
    @JsonProperty("wooriname")
    private String wooriname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 测点
     */
    @TableField(exist = false)
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    private String dpname;
    /**
     * 上级工单
     */
    @TableField(value = "wopid")
    @JSONField(name = "wopid")
    @JsonProperty("wopid")
    private String wopid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    private String rteamid;
    /**
     * 原因
     */
    @TableField(value = "rfocaid")
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    private String rfocaid;
    /**
     * 模式
     */
    @TableField(value = "rfomoid")
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    private String rfomoid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 工单来源
     */
    @TableField(value = "wooriid")
    @JSONField(name = "wooriid")
    @JsonProperty("wooriid")
    private String wooriid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    private String acclassid;
    /**
     * 测点
     */
    @TableField(value = "dpid")
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    private String dpid;
    /**
     * 服务商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    private String rserviceid;
    /**
     * 方案
     */
    @TableField(value = "rfoacid")
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    private String rfoacid;
    /**
     * 现象
     */
    @TableField(value = "rfodeid")
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    private String rfodeid;
    /**
     * 负责人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    private String rempid;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    private String rempname;
    /**
     * 制定人
     */
    @TableField(value = "mpersonid")
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    private String mpersonid;
    /**
     * 制定人
     */
    @TableField(exist = false)
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    private String mpersonname;
    /**
     * 执行人
     */
    @TableField(value = "wpersonid")
    @JSONField(name = "wpersonid")
    @JsonProperty("wpersonid")
    private String wpersonid;
    /**
     * 执行人
     */
    @TableField(exist = false)
    @JSONField(name = "wpersonname")
    @JsonProperty("wpersonname")
    private String wpersonname;
    /**
     * 接收人
     */
    @TableField(value = "recvpersonid")
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    private String recvpersonid;
    /**
     * 接收人
     */
    @TableField(exist = false)
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    private String recvpersonname;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptid")
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    private String rdeptid;
    /**
     * 责任部门
     */
    @TableField(exist = false)
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    private String rdeptname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject dp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWOORI woori;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wop;

    /**
     * 责任部门
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptdid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp cvpersonid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp rpfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp empid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [工单名称]
     */
    public void setEmwoOscname(String emwoOscname) {
        this.emwoOscname = emwoOscname;
        this.modify("emwo_oscname", emwoOscname);
    }

    /**
     * 设置 [钢丝绳润滑油]
     */
    public void setArg4(Double arg4) {
        this.arg4 = arg4;
        this.modify("arg4", arg4);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [值]
     */
    public void setVal(String val) {
        this.val = val;
        this.modify("val", val);
    }

    /**
     * 设置 [执行结果]
     */
    public void setWresult(String wresult) {
        this.wresult = wresult;
        this.modify("wresult", wresult);
    }

    /**
     * 设置 [起始时间]
     */
    public void setRegionbegindate(Timestamp regionbegindate) {
        this.regionbegindate = regionbegindate;
        this.modify("regionbegindate", regionbegindate);
    }

    /**
     * 格式化日期 [起始时间]
     */
    public String formatRegionbegindate() {
        if (this.regionbegindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionbegindate);
    }
    /**
     * 设置 [柴油]
     */
    public void setArg3(Double arg3) {
        this.arg3 = arg3;
        this.modify("arg3", arg3);
    }

    /**
     * 设置 [实际工时(分)]
     */
    public void setWorklength(Double worklength) {
        this.worklength = worklength;
        this.modify("worklength", worklength);
    }

    /**
     * 设置 [制定时间]
     */
    public void setMdate(Timestamp mdate) {
        this.mdate = mdate;
        this.modify("mdate", mdate);
    }

    /**
     * 格式化日期 [制定时间]
     */
    public String formatMdate() {
        if (this.mdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(mdate);
    }
    /**
     * 设置 [结束时间]
     */
    public void setRegionenddate(Timestamp regionenddate) {
        this.regionenddate = regionenddate;
        this.modify("regionenddate", regionenddate);
    }

    /**
     * 格式化日期 [结束时间]
     */
    public String formatRegionenddate() {
        if (this.regionenddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionenddate);
    }
    /**
     * 设置 [数值]
     */
    public void setNval(Double nval) {
        this.nval = nval;
        this.modify("nval", nval);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [约合材料费(￥)]
     */
    public void setMfee(Double mfee) {
        this.mfee = mfee;
        this.modify("mfee", mfee);
    }

    /**
     * 设置 [过期日期]
     */
    public void setExpiredate(Timestamp expiredate) {
        this.expiredate = expiredate;
        this.modify("expiredate", expiredate);
    }

    /**
     * 格式化日期 [过期日期]
     */
    public String formatExpiredate() {
        if (this.expiredate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(expiredate);
    }
    /**
     * 设置 [黄油]
     */
    public void setArg1(Double arg1) {
        this.arg1 = arg1;
        this.modify("arg1", arg1);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [详细内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [棉纱]
     */
    public void setArg2(Double arg2) {
        this.arg2 = arg2;
        this.modify("arg2", arg2);
    }

    /**
     * 设置 [归档]
     */
    public void setArchive(String archive) {
        this.archive = archive;
        this.modify("archive", archive);
    }

    /**
     * 设置 [工单分组]
     */
    public void setWogroup(String wogroup) {
        this.wogroup = wogroup;
        this.modify("wogroup", wogroup);
    }

    /**
     * 设置 [工单类型]
     */
    public void setWotype(String wotype) {
        this.wotype = wotype;
        this.modify("wotype", wotype);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority) {
        this.priority = priority;
        this.modify("priority", priority);
    }

    /**
     * 设置 [预算(￥)]
     */
    public void setPrefee(Double prefee) {
        this.prefee = prefee;
        this.modify("prefee", prefee);
    }

    /**
     * 设置 [工单状态]
     */
    public void setWostate(Integer wostate) {
        this.wostate = wostate;
        this.modify("wostate", wostate);
    }

    /**
     * 设置 [执行日期]
     */
    public void setWodate(Timestamp wodate) {
        this.wodate = wodate;
        this.modify("wodate", wodate);
    }

    /**
     * 格式化日期 [执行日期]
     */
    public String formatWodate() {
        if (this.wodate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(wodate);
    }
    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [停运时间(分)]
     */
    public void setEqstoplength(Double eqstoplength) {
        this.eqstoplength = eqstoplength;
        this.modify("eqstoplength", eqstoplength);
    }

    /**
     * 设置 [转计划标志]
     */
    public void setCplanflag(Integer cplanflag) {
        this.cplanflag = cplanflag;
        this.modify("cplanflag", cplanflag);
    }

    /**
     * 设置 [倍率]
     */
    public void setVrate(Double vrate) {
        this.vrate = vrate;
        this.modify("vrate", vrate);
    }

    /**
     * 设置 [金属去污粉]
     */
    public void setArg5(Double arg5) {
        this.arg5 = arg5;
        this.modify("arg5", arg5);
    }

    /**
     * 设置 [工单内容]
     */
    public void setWodesc(String wodesc) {
        this.wodesc = wodesc;
        this.modify("wodesc", wodesc);
    }

    /**
     * 设置 [上级工单]
     */
    public void setWopid(String wopid) {
        this.wopid = wopid;
        this.modify("wopid", wopid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }

    /**
     * 设置 [原因]
     */
    public void setRfocaid(String rfocaid) {
        this.rfocaid = rfocaid;
        this.modify("rfocaid", rfocaid);
    }

    /**
     * 设置 [模式]
     */
    public void setRfomoid(String rfomoid) {
        this.rfomoid = rfomoid;
        this.modify("rfomoid", rfomoid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [工单来源]
     */
    public void setWooriid(String wooriid) {
        this.wooriid = wooriid;
        this.modify("wooriid", wooriid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [测点]
     */
    public void setDpid(String dpid) {
        this.dpid = dpid;
        this.modify("dpid", dpid);
    }

    /**
     * 设置 [服务商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [方案]
     */
    public void setRfoacid(String rfoacid) {
        this.rfoacid = rfoacid;
        this.modify("rfoacid", rfoacid);
    }

    /**
     * 设置 [现象]
     */
    public void setRfodeid(String rfodeid) {
        this.rfodeid = rfodeid;
        this.modify("rfodeid", rfodeid);
    }

    /**
     * 设置 [负责人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [制定人]
     */
    public void setMpersonid(String mpersonid) {
        this.mpersonid = mpersonid;
        this.modify("mpersonid", mpersonid);
    }

    /**
     * 设置 [执行人]
     */
    public void setWpersonid(String wpersonid) {
        this.wpersonid = wpersonid;
        this.modify("wpersonid", wpersonid);
    }

    /**
     * 设置 [接收人]
     */
    public void setRecvpersonid(String recvpersonid) {
        this.recvpersonid = recvpersonid;
        this.modify("recvpersonid", recvpersonid);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptid(String rdeptid) {
        this.rdeptid = rdeptid;
        this.modify("rdeptid", rdeptid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emwo_oscid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


