package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWOORI;
import cn.ibizlab.eam.core.eam_core.filter.EMWOORISearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWOORI] 服务对象接口
 */
public interface IEMWOORIService extends IService<EMWOORI> {

    boolean create(EMWOORI et);
    void createBatch(List<EMWOORI> list);
    boolean update(EMWOORI et);
    void updateBatch(List<EMWOORI> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWOORI get(String key);
    EMWOORI getDraft(EMWOORI et);
    boolean checkKey(EMWOORI et);
    boolean save(EMWOORI et);
    void saveBatch(List<EMWOORI> list);
    Page<EMWOORI> searchDefault(EMWOORISearchContext context);
    Page<EMWOORI> searchIndexDER(EMWOORISearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWOORI> getEmwooriByIds(List<String> ids);
    List<EMWOORI> getEmwooriByEntities(List<EMWOORI> entities);
}


