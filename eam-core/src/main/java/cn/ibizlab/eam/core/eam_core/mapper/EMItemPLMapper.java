package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPL;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPLSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMItemPLMapper extends BaseMapper<EMItemPL> {

    Page<EMItemPL> searchConfirmed(IPage page, @Param("srf") EMItemPLSearchContext context, @Param("ew") Wrapper<EMItemPL> wrapper);
    Page<EMItemPL> searchDefault(IPage page, @Param("srf") EMItemPLSearchContext context, @Param("ew") Wrapper<EMItemPL> wrapper);
    Page<EMItemPL> searchDraft(IPage page, @Param("srf") EMItemPLSearchContext context, @Param("ew") Wrapper<EMItemPL> wrapper);
    Page<EMItemPL> searchToConfirm(IPage page, @Param("srf") EMItemPLSearchContext context, @Param("ew") Wrapper<EMItemPL> wrapper);
    @Override
    EMItemPL selectById(Serializable id);
    @Override
    int insert(EMItemPL entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMItemPL entity);
    @Override
    int update(@Param(Constants.ENTITY) EMItemPL entity, @Param("ew") Wrapper<EMItemPL> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMItemPL> selectByRid(@Param("emitemrinid") Serializable emitemrinid);

    List<EMItemPL> selectByItemid(@Param("emitemid") Serializable emitemid);

    List<EMItemPL> selectByStorepartid(@Param("emstorepartid") Serializable emstorepartid);

    List<EMItemPL> selectByStoreid(@Param("emstoreid") Serializable emstoreid);

    List<EMItemPL> selectBySempid(@Param("pfempid") Serializable pfempid);

}
