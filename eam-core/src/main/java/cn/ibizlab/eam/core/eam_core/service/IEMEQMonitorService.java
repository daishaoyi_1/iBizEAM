package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQMonitor;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMonitorSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQMonitor] 服务对象接口
 */
public interface IEMEQMonitorService extends IService<EMEQMonitor> {

    boolean create(EMEQMonitor et);
    void createBatch(List<EMEQMonitor> list);
    boolean update(EMEQMonitor et);
    void updateBatch(List<EMEQMonitor> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQMonitor get(String key);
    EMEQMonitor getDraft(EMEQMonitor et);
    boolean checkKey(EMEQMonitor et);
    boolean save(EMEQMonitor et);
    void saveBatch(List<EMEQMonitor> list);
    Page<EMEQMonitor> searchDefault(EMEQMonitorSearchContext context);
    List<EMEQMonitor> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQMonitor> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQMonitor> getEmeqmonitorByIds(List<String> ids);
    List<EMEQMonitor> getEmeqmonitorByEntities(List<EMEQMonitor> entities);
}


