package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG;
/**
 * 关系型数据实体[EMDRWG] 查询条件对象
 */
@Slf4j
@Data
public class EMDRWGSearchContext extends QueryWrapperContext<EMDRWG> {

	private String n_bpersonid_eq;//[最后借阅人]
	public void setN_bpersonid_eq(String n_bpersonid_eq) {
        this.n_bpersonid_eq = n_bpersonid_eq;
        if(!ObjectUtils.isEmpty(this.n_bpersonid_eq)){
            this.getSearchCond().eq("bpersonid", n_bpersonid_eq);
        }
    }
	private String n_rempid_eq;//[保管人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_drwgcode_like;//[文档代码]
	public void setN_drwgcode_like(String n_drwgcode_like) {
        this.n_drwgcode_like = n_drwgcode_like;
        if(!ObjectUtils.isEmpty(this.n_drwgcode_like)){
            this.getSearchCond().like("drwgcode", n_drwgcode_like);
        }
    }
	private String n_rempname_eq;//[保管人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[保管人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_drwgtype_eq;//[文档类型]
	public void setN_drwgtype_eq(String n_drwgtype_eq) {
        this.n_drwgtype_eq = n_drwgtype_eq;
        if(!ObjectUtils.isEmpty(this.n_drwgtype_eq)){
            this.getSearchCond().eq("drwgtype", n_drwgtype_eq);
        }
    }
	private String n_emdrwgname_like;//[文档名称]
	public void setN_emdrwgname_like(String n_emdrwgname_like) {
        this.n_emdrwgname_like = n_emdrwgname_like;
        if(!ObjectUtils.isEmpty(this.n_emdrwgname_like)){
            this.getSearchCond().like("emdrwgname", n_emdrwgname_like);
        }
    }
	private String n_drwgstate_eq;//[文档状态]
	public void setN_drwgstate_eq(String n_drwgstate_eq) {
        this.n_drwgstate_eq = n_drwgstate_eq;
        if(!ObjectUtils.isEmpty(this.n_drwgstate_eq)){
            this.getSearchCond().eq("drwgstate", n_drwgstate_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_drwginfo_like;//[文档信息]
	public void setN_drwginfo_like(String n_drwginfo_like) {
        this.n_drwginfo_like = n_drwginfo_like;
        if(!ObjectUtils.isEmpty(this.n_drwginfo_like)){
            this.getSearchCond().like("drwginfo", n_drwginfo_like);
        }
    }
	private String n_bpersonname_eq;//[最后借阅人]
	public void setN_bpersonname_eq(String n_bpersonname_eq) {
        this.n_bpersonname_eq = n_bpersonname_eq;
        if(!ObjectUtils.isEmpty(this.n_bpersonname_eq)){
            this.getSearchCond().eq("bpersonname", n_bpersonname_eq);
        }
    }
	private String n_bpersonname_like;//[最后借阅人]
	public void setN_bpersonname_like(String n_bpersonname_like) {
        this.n_bpersonname_like = n_bpersonname_like;
        if(!ObjectUtils.isEmpty(this.n_bpersonname_like)){
            this.getSearchCond().like("bpersonname", n_bpersonname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emdrwgname", query)
            );
		 }
	}
}



