

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODE;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMRFODEInheritMapping {

    @Mappings({
        @Mapping(source ="emrfodeid",target = "emobjectid"),
        @Mapping(source ="emrfodename",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="rfodecode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMRFODE minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emrfodeid"),
        @Mapping(source ="emobjectname" ,target = "emrfodename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "rfodecode"),
    })
    EMRFODE toEmrfode(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMRFODE> minorEntities);

    List<EMRFODE> toEmrfode(List<EMObject> majorEntities);

}


