package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[备件包引用]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQSPAREMAP_BASE", resultMap = "EMEQSpareMapResultMap")
public class EMEQSpareMap extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 备件包引用标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqsparemapid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqsparemapid")
    @JsonProperty("emeqsparemapid")
    private String emeqsparemapid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 备件包引用
     */
    @DEField(defaultValue = "VAR_EQSPAREID||-||VAR_REFOBJID")
    @TableField(value = "emeqsparemapname")
    @JSONField(name = "emeqsparemapname")
    @JsonProperty("emeqsparemapname")
    private String emeqsparemapname;
    /**
     * 引用对象
     */
    @TableField(exist = false)
    @JSONField(name = "refobjname")
    @JsonProperty("refobjname")
    private String refobjname;
    /**
     * 备件包
     */
    @TableField(exist = false)
    @JSONField(name = "eqsparename")
    @JsonProperty("eqsparename")
    private String eqsparename;
    /**
     * 对象标识
     */
    @TableField(value = "refobjid")
    @JSONField(name = "refobjid")
    @JsonProperty("refobjid")
    private String refobjid;
    /**
     * 备件包
     */
    @TableField(value = "eqspareid")
    @JSONField(name = "eqspareid")
    @JsonProperty("eqspareid")
    private String eqspareid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQSpare eqspare;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject refobj;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [备件包引用]
     */
    public void setEmeqsparemapname(String emeqsparemapname) {
        this.emeqsparemapname = emeqsparemapname;
        this.modify("emeqsparemapname", emeqsparemapname);
    }

    /**
     * 设置 [对象标识]
     */
    public void setRefobjid(String refobjid) {
        this.refobjid = refobjid;
        this.modify("refobjid", refobjid);
    }

    /**
     * 设置 [备件包]
     */
    public void setEqspareid(String eqspareid) {
        this.eqspareid = eqspareid;
        this.modify("eqspareid", eqspareid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqsparemapid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


