package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[系统维护]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMSYSCTRL_BASE", resultMap = "EMSysCtrlResultMap")
public class EMSysCtrl extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 维护时间
     */
    @TableField(value = "whsj")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "whsj", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("whsj")
    private Timestamp whsj;
    /**
     * 系统维护标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emsysctrlid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emsysctrlid")
    @JsonProperty("emsysctrlid")
    private String emsysctrlid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 维护信息
     */
    @TableField(value = "whinfo")
    @JSONField(name = "whinfo")
    @JsonProperty("whinfo")
    private String whinfo;
    /**
     * 系统维护名称
     */
    @DEField(defaultValue = "系统维护")
    @TableField(value = "emsysctrlname")
    @JSONField(name = "emsysctrlname")
    @JsonProperty("emsysctrlname")
    private String emsysctrlname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;



    /**
     * 设置 [维护时间]
     */
    public void setWhsj(Timestamp whsj) {
        this.whsj = whsj;
        this.modify("whsj", whsj);
    }

    /**
     * 格式化日期 [维护时间]
     */
    public String formatWhsj() {
        if (this.whsj == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(whsj);
    }
    /**
     * 设置 [维护信息]
     */
    public void setWhinfo(String whinfo) {
        this.whinfo = whinfo;
        this.modify("whinfo", whinfo);
    }

    /**
     * 设置 [系统维护名称]
     */
    public void setEmsysctrlname(String emsysctrlname) {
        this.emsysctrlname = emsysctrlname;
        this.modify("emsysctrlname", emsysctrlname);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emsysctrlid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


