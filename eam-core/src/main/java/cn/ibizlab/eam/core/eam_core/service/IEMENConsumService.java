package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMENConsum;
import cn.ibizlab.eam.core.eam_core.filter.EMENConsumSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMENConsum] 服务对象接口
 */
public interface IEMENConsumService extends IService<EMENConsum> {

    boolean create(EMENConsum et);
    void createBatch(List<EMENConsum> list);
    boolean update(EMENConsum et);
    void updateBatch(List<EMENConsum> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMENConsum get(String key);
    EMENConsum getDraft(EMENConsum et);
    boolean checkKey(EMENConsum et);
    boolean save(EMENConsum et);
    void saveBatch(List<EMENConsum> list);
    Page<EMENConsum> searchDefault(EMENConsumSearchContext context);
    Page<Map> searchEqEnByYear(EMENConsumSearchContext context);
    List<EMENConsum> selectByEnid(String emenid);
    void removeByEnid(String emenid);
    List<EMENConsum> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMENConsum> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMENConsum> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMENConsum> getEmenconsumByIds(List<String> ids);
    List<EMENConsum> getEmenconsumByEntities(List<EMENConsum> entities);
}


