package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWO_DP;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_DPSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWO_DP] 服务对象接口
 */
public interface IEMWO_DPService extends IService<EMWO_DP> {

    boolean create(EMWO_DP et);
    void createBatch(List<EMWO_DP> list);
    boolean update(EMWO_DP et);
    void updateBatch(List<EMWO_DP> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWO_DP get(String key);
    EMWO_DP getDraft(EMWO_DP et);
    EMWO_DP acceptance(EMWO_DP et);
    boolean acceptanceBatch(List<EMWO_DP> etList);
    boolean checkKey(EMWO_DP et);
    EMWO_DP formUpdateByEmquipId(EMWO_DP et);
    boolean save(EMWO_DP et);
    void saveBatch(List<EMWO_DP> list);
    EMWO_DP submit(EMWO_DP et);
    EMWO_DP unAcceptance(EMWO_DP et);
    Page<EMWO_DP> searchCalendar(EMWO_DPSearchContext context);
    Page<EMWO_DP> searchConfirmed(EMWO_DPSearchContext context);
    Page<EMWO_DP> searchDefault(EMWO_DPSearchContext context);
    Page<EMWO_DP> searchDraft(EMWO_DPSearchContext context);
    Page<EMWO_DP> searchToConfirm(EMWO_DPSearchContext context);
    List<EMWO_DP> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMWO_DP> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMWO_DP> selectByDpid(String emobjectid);
    void removeByDpid(String emobjectid);
    List<EMWO_DP> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMWO_DP> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMWO_DP> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMWO_DP> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMWO_DP> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMWO_DP> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMWO_DP> selectByWooriid(String emwooriid);
    void removeByWooriid(String emwooriid);
    List<EMWO_DP> selectByWopid(String emwoid);
    void removeByWopid(String emwoid);
    List<EMWO_DP> selectByRdeptid(String pfdeptid);
    void removeByRdeptid(String pfdeptid);
    List<EMWO_DP> selectByMpersonid(String pfempid);
    void removeByMpersonid(String pfempid);
    List<EMWO_DP> selectByRecvpersonid(String pfempid);
    void removeByRecvpersonid(String pfempid);
    List<EMWO_DP> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMWO_DP> selectByWpersonid(String pfempid);
    void removeByWpersonid(String pfempid);
    List<EMWO_DP> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWO_DP> getEmwoDpByIds(List<String> ids);
    List<EMWO_DP> getEmwoDpByEntities(List<EMWO_DP> entities);
}


