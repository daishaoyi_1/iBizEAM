package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[序列号]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "T_SEQUENCE", resultMap = "SEQUENCEResultMap")
public class SEQUENCE extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 序列号名称
     */
    @TableField(value = "sequencename")
    @JSONField(name = "sequencename")
    @JsonProperty("sequencename")
    private String sequencename;
    /**
     * 序列号标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "sequenceid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "sequenceid")
    @JsonProperty("sequenceid")
    private String sequenceid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 实体名
     */
    @TableField(value = "code")
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * 实现
     */
    @TableField(value = "implementation")
    @JSONField(name = "implementation")
    @JsonProperty("implementation")
    private String implementation;
    /**
     * 前缀
     */
    @TableField(value = "prefix")
    @JSONField(name = "prefix")
    @JsonProperty("prefix")
    private String prefix;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 后缀
     */
    @TableField(value = "suffix")
    @JSONField(name = "suffix")
    @JsonProperty("suffix")
    private String suffix;
    /**
     * 序号增长
     */
    @TableField(value = "numincrement")
    @JSONField(name = "numincrement")
    @JsonProperty("numincrement")
    private Integer numincrement;
    /**
     * 序号长度
     */
    @TableField(value = "padding")
    @JSONField(name = "padding")
    @JsonProperty("padding")
    private Integer padding;
    /**
     * 下一号码
     */
    @TableField(value = "numnext")
    @JSONField(name = "numnext")
    @JsonProperty("numnext")
    private Integer numnext;



    /**
     * 设置 [序列号名称]
     */
    public void setSequencename(String sequencename) {
        this.sequencename = sequencename;
        this.modify("sequencename", sequencename);
    }

    /**
     * 设置 [实体名]
     */
    public void setCode(String code) {
        this.code = code;
        this.modify("code", code);
    }

    /**
     * 设置 [实现]
     */
    public void setImplementation(String implementation) {
        this.implementation = implementation;
        this.modify("implementation", implementation);
    }

    /**
     * 设置 [前缀]
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
        this.modify("prefix", prefix);
    }

    /**
     * 设置 [逻辑有效标志]
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
        this.modify("enable", enable);
    }

    /**
     * 设置 [后缀]
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
        this.modify("suffix", suffix);
    }

    /**
     * 设置 [序号增长]
     */
    public void setNumincrement(Integer numincrement) {
        this.numincrement = numincrement;
        this.modify("numincrement", numincrement);
    }

    /**
     * 设置 [序号长度]
     */
    public void setPadding(Integer padding) {
        this.padding = padding;
        this.modify("padding", padding);
    }

    /**
     * 设置 [下一号码]
     */
    public void setNumnext(Integer numnext) {
        this.numnext = numnext;
        this.modify("numnext", numnext);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("sequenceid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


