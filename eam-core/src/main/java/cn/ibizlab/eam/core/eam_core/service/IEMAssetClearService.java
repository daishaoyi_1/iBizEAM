package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMAssetClear;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetClearSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMAssetClear] 服务对象接口
 */
public interface IEMAssetClearService extends IService<EMAssetClear> {

    boolean create(EMAssetClear et);
    void createBatch(List<EMAssetClear> list);
    boolean update(EMAssetClear et);
    void updateBatch(List<EMAssetClear> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMAssetClear get(String key);
    EMAssetClear getDraft(EMAssetClear et);
    boolean checkKey(EMAssetClear et);
    EMAssetClear clear(EMAssetClear et);
    boolean clearBatch(List<EMAssetClear> etList);
    boolean save(EMAssetClear et);
    void saveBatch(List<EMAssetClear> list);
    Page<EMAssetClear> searchDefault(EMAssetClearSearchContext context);
    List<EMAssetClear> selectByEmassetid(String emassetid);
    void removeByEmassetid(String emassetid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMAssetClear> getEmassetclearByIds(List<String> ids);
    List<EMAssetClear> getEmassetclearByEntities(List<EMAssetClear> entities);
}


