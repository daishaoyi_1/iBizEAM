package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备运行日志]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQWL_BASE", resultMap = "EMEQWLResultMap")
public class EMEQWL extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 运行区间起始
     */
    @TableField(value = "bdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "bdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    private Timestamp bdate;
    /**
     * 运行时间(H)
     */
    @TableField(value = "nval")
    @JSONField(name = "nval")
    @JsonProperty("nval")
    private Double nval;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 运行区间截至
     */
    @TableField(value = "edate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "edate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    private Timestamp edate;
    /**
     * 设备运行日志名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emeqwlname")
    @JSONField(name = "emeqwlname")
    @JsonProperty("emeqwlname")
    private String emeqwlname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 设备运行日志标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqwlid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqwlid")
    @JsonProperty("emeqwlid")
    private String emeqwlid;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "woname")
    @JsonProperty("woname")
    private String woname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 工单
     */
    @TableField(value = "woid")
    @JSONField(name = "woid")
    @JsonProperty("woid")
    private String woid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wo;



    /**
     * 设置 [运行区间起始]
     */
    public void setBdate(Timestamp bdate) {
        this.bdate = bdate;
        this.modify("bdate", bdate);
    }

    /**
     * 格式化日期 [运行区间起始]
     */
    public String formatBdate() {
        if (this.bdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(bdate);
    }
    /**
     * 设置 [运行时间(H)]
     */
    public void setNval(Double nval) {
        this.nval = nval;
        this.modify("nval", nval);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [运行区间截至]
     */
    public void setEdate(Timestamp edate) {
        this.edate = edate;
        this.modify("edate", edate);
    }

    /**
     * 格式化日期 [运行区间截至]
     */
    public String formatEdate() {
        if (this.edate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(edate);
    }
    /**
     * 设置 [设备运行日志名称]
     */
    public void setEmeqwlname(String emeqwlname) {
        this.emeqwlname = emeqwlname;
        this.modify("emeqwlname", emeqwlname);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [工单]
     */
    public void setWoid(String woid) {
        this.woid = woid;
        this.modify("woid", woid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqwlid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


