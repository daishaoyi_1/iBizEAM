package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.SEQUENCE;
import cn.ibizlab.eam.core.eam_core.filter.SEQUENCESearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[SEQUENCE] 服务对象接口
 */
public interface ISEQUENCEService extends IService<SEQUENCE> {

    boolean create(SEQUENCE et);
    void createBatch(List<SEQUENCE> list);
    boolean update(SEQUENCE et);
    void updateBatch(List<SEQUENCE> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    SEQUENCE get(String key);
    SEQUENCE getDraft(SEQUENCE et);
    boolean checkKey(SEQUENCE et);
    boolean save(SEQUENCE et);
    void saveBatch(List<SEQUENCE> list);
    Page<SEQUENCE> searchDefault(SEQUENCESearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<SEQUENCE> getSequenceByIds(List<String> ids);
    List<SEQUENCE> getSequenceByEntities(List<SEQUENCE> entities);
}


