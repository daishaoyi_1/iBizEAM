package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[维修中心月度计划明细]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMMONTHLYDETAIL_BASE", resultMap = "EMMonthlyDetailResultMap")
public class EMMonthlyDetail extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 完成时间
     */
    @TableField(value = "finishtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "finishtime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("finishtime")
    private Timestamp finishtime;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 完成情况
     */
    @DEField(defaultValue = "0")
    @TableField(value = "execution")
    @JSONField(name = "execution")
    @JsonProperty("execution")
    private String execution;
    /**
     * 备注
     */
    @TableField(value = "remarks")
    @JSONField(name = "remarks")
    @JsonProperty("remarks")
    private String remarks;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 序号
     */
    @TableField(value = "num")
    @JSONField(name = "num")
    @JsonProperty("num")
    private Integer num;
    /**
     * 检修内容
     */
    @TableField(value = "maintenancecontent")
    @JSONField(name = "maintenancecontent")
    @JsonProperty("maintenancecontent")
    private String maintenancecontent;
    /**
     * 项目名称
     */
    @TableField(value = "emmonthlydetailname")
    @JSONField(name = "emmonthlydetailname")
    @JsonProperty("emmonthlydetailname")
    private String emmonthlydetailname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 维修月度计划明细标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emmonthlydetailid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emmonthlydetailid")
    @JsonProperty("emmonthlydetailid")
    private String emmonthlydetailid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 维修中心月度计划
     */
    @TableField(exist = false)
    @JSONField(name = "emmonthlyname")
    @JsonProperty("emmonthlyname")
    private String emmonthlyname;
    /**
     * 维修中心月度计划
     */
    @TableField(value = "emmonthlyid")
    @JSONField(name = "emmonthlyid")
    @JsonProperty("emmonthlyid")
    private String emmonthlyid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMMonthly emmonthly;



    /**
     * 设置 [完成时间]
     */
    public void setFinishtime(Timestamp finishtime) {
        this.finishtime = finishtime;
        this.modify("finishtime", finishtime);
    }

    /**
     * 格式化日期 [完成时间]
     */
    public String formatFinishtime() {
        if (this.finishtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(finishtime);
    }
    /**
     * 设置 [完成情况]
     */
    public void setExecution(String execution) {
        this.execution = execution;
        this.modify("execution", execution);
    }

    /**
     * 设置 [备注]
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
        this.modify("remarks", remarks);
    }

    /**
     * 设置 [序号]
     */
    public void setNum(Integer num) {
        this.num = num;
        this.modify("num", num);
    }

    /**
     * 设置 [检修内容]
     */
    public void setMaintenancecontent(String maintenancecontent) {
        this.maintenancecontent = maintenancecontent;
        this.modify("maintenancecontent", maintenancecontent);
    }

    /**
     * 设置 [项目名称]
     */
    public void setEmmonthlydetailname(String emmonthlydetailname) {
        this.emmonthlydetailname = emmonthlydetailname;
        this.modify("emmonthlydetailname", emmonthlydetailname);
    }

    /**
     * 设置 [维修中心月度计划]
     */
    public void setEmmonthlyid(String emmonthlyid) {
        this.emmonthlyid = emmonthlyid;
        this.modify("emmonthlyid", emmonthlyid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emmonthlydetailid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


