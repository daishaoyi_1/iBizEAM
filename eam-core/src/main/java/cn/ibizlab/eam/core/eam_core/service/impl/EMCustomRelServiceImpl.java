package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMCustomRel;
import cn.ibizlab.eam.core.eam_core.filter.EMCustomRelSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMCustomRelService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMCustomRelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[客户] 服务对象接口实现
 */
@Slf4j
@Service("EMCustomRelServiceImpl")
public class EMCustomRelServiceImpl extends ServiceImpl<EMCustomRelMapper, EMCustomRel> implements IEMCustomRelService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMCustomRel et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmcustomrelid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMCustomRel> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMCustomRel et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emcustomrelid", et.getEmcustomrelid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmcustomrelid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMCustomRel> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMCustomRel get(String key) {
        EMCustomRel et = getById(key);
        if(et == null){
            et = new EMCustomRel();
            et.setEmcustomrelid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMCustomRel getDraft(EMCustomRel et) {
        return et;
    }

    @Override
    public boolean checkKey(EMCustomRel et) {
        return (!ObjectUtils.isEmpty(et.getEmcustomrelid())) && (!Objects.isNull(this.getById(et.getEmcustomrelid())));
    }
    @Override
    @Transactional
    public boolean save(EMCustomRel et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMCustomRel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMCustomRel> list) {
        List<EMCustomRel> create = new ArrayList<>();
        List<EMCustomRel> update = new ArrayList<>();
        for (EMCustomRel et : list) {
            if (ObjectUtils.isEmpty(et.getEmcustomrelid()) || ObjectUtils.isEmpty(getById(et.getEmcustomrelid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMCustomRel> list) {
        List<EMCustomRel> create = new ArrayList<>();
        List<EMCustomRel> update = new ArrayList<>();
        for (EMCustomRel et : list) {
            if (ObjectUtils.isEmpty(et.getEmcustomrelid()) || ObjectUtils.isEmpty(getById(et.getEmcustomrelid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMCustomRel> searchDefault(EMCustomRelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMCustomRel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMCustomRel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMCustomRel> getEmcustomrelByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMCustomRel> getEmcustomrelByEntities(List<EMCustomRel> entities) {
        List ids =new ArrayList();
        for(EMCustomRel entity : entities){
            Serializable id=entity.getEmcustomrelid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMCustomRelService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



