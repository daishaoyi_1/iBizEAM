package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetClass;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetClassSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMAssetClassService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMAssetClassMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[资产类别] 服务对象接口实现
 */
@Slf4j
@Service("EMAssetClassServiceImpl")
public class EMAssetClassServiceImpl extends ServiceImpl<EMAssetClassMapper, EMAssetClass> implements IEMAssetClassService {


    protected cn.ibizlab.eam.core.eam_core.service.IEMAssetClassService emassetclassService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssetService emassetService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMAssetClass et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmassetclassid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMAssetClass> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMAssetClass et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emassetclassid", et.getEmassetclassid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmassetclassid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMAssetClass> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMAssetClass get(String key) {
        EMAssetClass et = getById(key);
        if(et == null){
            et = new EMAssetClass();
            et.setEmassetclassid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMAssetClass getDraft(EMAssetClass et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMAssetClass et) {
        return (!ObjectUtils.isEmpty(et.getEmassetclassid())) && (!Objects.isNull(this.getById(et.getEmassetclassid())));
    }
    @Override
    @Transactional
    public boolean save(EMAssetClass et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMAssetClass et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMAssetClass> list) {
        list.forEach(item->fillParentData(item));
        List<EMAssetClass> create = new ArrayList<>();
        List<EMAssetClass> update = new ArrayList<>();
        for (EMAssetClass et : list) {
            if (ObjectUtils.isEmpty(et.getEmassetclassid()) || ObjectUtils.isEmpty(getById(et.getEmassetclassid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMAssetClass> list) {
        list.forEach(item->fillParentData(item));
        List<EMAssetClass> create = new ArrayList<>();
        List<EMAssetClass> update = new ArrayList<>();
        for (EMAssetClass et : list) {
            if (ObjectUtils.isEmpty(et.getEmassetclassid()) || ObjectUtils.isEmpty(getById(et.getEmassetclassid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMAssetClass> selectByAssetclasspid(String emassetclassid) {
        return baseMapper.selectByAssetclasspid(emassetclassid);
    }
    @Override
    public void removeByAssetclasspid(String emassetclassid) {
        this.remove(new QueryWrapper<EMAssetClass>().eq("assetclasspid",emassetclassid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMAssetClass> searchDefault(EMAssetClassSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMAssetClass> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMAssetClass>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMAssetClass et){
        //实体关系[DER1N_EMASSETCLASS_EMASSETCLASS_ASSETCLASSPID]
        if(!ObjectUtils.isEmpty(et.getAssetclasspid())){
            cn.ibizlab.eam.core.eam_core.domain.EMAssetClass assetclassp=et.getAssetclassp();
            if(ObjectUtils.isEmpty(assetclassp)){
                cn.ibizlab.eam.core.eam_core.domain.EMAssetClass majorEntity=emassetclassService.get(et.getAssetclasspid());
                et.setAssetclassp(majorEntity);
                assetclassp=majorEntity;
            }
            et.setAssetclasspname(assetclassp.getEmassetclassname());
            et.setAssetclasspcode(assetclassp.getAssetclasscode());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMAssetClass> getEmassetclassByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMAssetClass> getEmassetclassByEntities(List<EMAssetClass> entities) {
        List ids =new ArrayList();
        for(EMAssetClass entity : entities){
            Serializable id=entity.getEmassetclassid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMAssetClassService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



