package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划条件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPLANCDT_BASE", resultMap = "EMPlanCDTResultMap")
public class EMPlanCDT extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 计划条件标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emplancdtid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emplancdtid")
    @JsonProperty("emplancdtid")
    private String emplancdtid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 临界值
     */
    @TableField(value = "triggerval")
    @JSONField(name = "triggerval")
    @JsonProperty("triggerval")
    private String triggerval;
    /**
     * 测点值类型
     */
    @DEField(defaultValue = "NEW")
    @TableField(value = "dpvaltype")
    @JSONField(name = "dpvaltype")
    @JsonProperty("dpvaltype")
    private String dpvaltype;
    /**
     * 上次触发值
     */
    @TableField(value = "lastval")
    @JSONField(name = "lastval")
    @JsonProperty("lastval")
    private String lastval;
    /**
     * 预警值
     */
    @TableField(value = "nowval")
    @JSONField(name = "nowval")
    @JsonProperty("nowval")
    private String nowval;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 触发操作
     */
    @TableField(value = "triggerdp")
    @JSONField(name = "triggerdp")
    @JsonProperty("triggerdp")
    private String triggerdp;
    /**
     * 计划条件名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emplancdtname")
    @JSONField(name = "emplancdtname")
    @JsonProperty("emplancdtname")
    private String emplancdtname;
    /**
     * 测点类型
     */
    @TableField(exist = false)
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    private String dptype;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    private String objname;
    /**
     * 测点
     */
    @TableField(exist = false)
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    private String dpname;
    /**
     * 计划
     */
    @TableField(exist = false)
    @JSONField(name = "planname")
    @JsonProperty("planname")
    private String planname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 测点
     */
    @TableField(value = "dpid")
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    private String dpid;
    /**
     * 计划
     */
    @TableField(value = "planid")
    @JSONField(name = "planid")
    @JsonProperty("planid")
    private String planid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject dp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPlan plan;



    /**
     * 设置 [临界值]
     */
    public void setTriggerval(String triggerval) {
        this.triggerval = triggerval;
        this.modify("triggerval", triggerval);
    }

    /**
     * 设置 [测点值类型]
     */
    public void setDpvaltype(String dpvaltype) {
        this.dpvaltype = dpvaltype;
        this.modify("dpvaltype", dpvaltype);
    }

    /**
     * 设置 [上次触发值]
     */
    public void setLastval(String lastval) {
        this.lastval = lastval;
        this.modify("lastval", lastval);
    }

    /**
     * 设置 [预警值]
     */
    public void setNowval(String nowval) {
        this.nowval = nowval;
        this.modify("nowval", nowval);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [触发操作]
     */
    public void setTriggerdp(String triggerdp) {
        this.triggerdp = triggerdp;
        this.modify("triggerdp", triggerdp);
    }

    /**
     * 设置 [计划条件名称]
     */
    public void setEmplancdtname(String emplancdtname) {
        this.emplancdtname = emplancdtname;
        this.modify("emplancdtname", emplancdtname);
    }

    /**
     * 设置 [测点]
     */
    public void setDpid(String dpid) {
        this.dpid = dpid;
        this.modify("dpid", dpid);
    }

    /**
     * 设置 [计划]
     */
    public void setPlanid(String planid) {
        this.planid = planid;
        this.modify("planid", planid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emplancdtid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


