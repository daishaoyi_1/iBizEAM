package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[物品]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMITEM_BASE", resultMap = "EMItemResultMap")
public class EMItem extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最新价格
     */
    @TableField(value = "lastprice")
    @JSONField(name = "lastprice")
    @JsonProperty("lastprice")
    private Double lastprice;
    /**
     * 对象编号
     */
    @TableField(exist = false)
    @JSONField(name = "objid")
    @JsonProperty("objid")
    private String objid;
    /**
     * 物品信息
     */
    @TableField(exist = false)
    @JSONField(name = "iteminfo")
    @JsonProperty("iteminfo")
    private String iteminfo;
    /**
     * 按资产
     */
    @TableField(value = "isassetflag")
    @JSONField(name = "isassetflag")
    @JsonProperty("isassetflag")
    private Integer isassetflag;
    /**
     * 最高库存
     */
    @DEField(defaultValue = "0")
    @TableField(value = "highsum")
    @JSONField(name = "highsum")
    @JsonProperty("highsum")
    private Double highsum;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 库存金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 按批次
     */
    @TableField(value = "isbatchflag")
    @JSONField(name = "isbatchflag")
    @JsonProperty("isbatchflag")
    private Integer isbatchflag;
    /**
     * 验收方法
     */
    @TableField(value = "checkmethod")
    @JSONField(name = "checkmethod")
    @JsonProperty("checkmethod")
    private String checkmethod;
    /**
     * 物品标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emitemid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emitemid")
    @JsonProperty("emitemid")
    private String emitemid;
    /**
     * 库存定额余量
     */
    @TableField(exist = false)
    @JSONField(name = "stockdesum")
    @JsonProperty("stockdesum")
    private Double stockdesum;
    /**
     * ABC分类
     */
    @TableField(value = "abctype")
    @JSONField(name = "abctype")
    @JsonProperty("abctype")
    private String abctype;
    /**
     * 平均税费
     */
    @TableField(value = "shfprice")
    @JSONField(name = "shfprice")
    @JsonProperty("shfprice")
    private Double shfprice;
    /**
     * 重订量
     */
    @TableField(value = "repsum")
    @JSONField(name = "repsum")
    @JsonProperty("repsum")
    private Double repsum;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 库存超期
     */
    @TableField(exist = false)
    @JSONField(name = "stockextime")
    @JsonProperty("stockextime")
    private Double stockextime;
    /**
     * 物品新旧标识
     */
    @TableField(value = "isnew")
    @JSONField(name = "isnew")
    @JsonProperty("isnew")
    private String isnew;
    /**
     * 物品编码(新)
     */
    @TableField(value = "itemnid")
    @JSONField(name = "itemnid")
    @JsonProperty("itemnid")
    private String itemnid;
    /**
     * 产品系列号
     */
    @TableField(value = "itemserialcode")
    @JSONField(name = "itemserialcode")
    @JsonProperty("itemserialcode")
    private String itemserialcode;
    /**
     * 登记日期
     */
    @TableField(value = "registerdat")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "registerdat", format = "yyyy-MM-dd")
    @JsonProperty("registerdat")
    private Timestamp registerdat;
    /**
     * 产品型号
     */
    @TableField(value = "itemmodelcode")
    @JSONField(name = "itemmodelcode")
    @JsonProperty("itemmodelcode")
    private String itemmodelcode;
    /**
     * 物品代码(新)
     */
    @TableField(value = "itemncode")
    @JSONField(name = "itemncode")
    @JsonProperty("itemncode")
    private String itemncode;
    /**
     * 物品代码
     */
    @TableField(value = "itemcode")
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    private String itemcode;
    /**
     * 不足3家供应商
     */
    @DEField(defaultValue = "0")
    @TableField(value = "no3q")
    @JSONField(name = "no3q")
    @JsonProperty("no3q")
    private Integer no3q;
    /**
     * 物品名称
     */
    @TableField(value = "emitemname")
    @JSONField(name = "emitemname")
    @JsonProperty("emitemname")
    private String emitemname;
    /**
     * 密度
     */
    @TableField(value = "dens")
    @JSONField(name = "dens")
    @JsonProperty("dens")
    private Double dens;
    /**
     * sap控制
     */
    @TableField(value = "sapcontrol")
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    private Integer sapcontrol;
    /**
     * 批次类型
     */
    @TableField(value = "batchtype")
    @JSONField(name = "batchtype")
    @JsonProperty("batchtype")
    private String batchtype;
    /**
     * 物品分组
     */
    @TableField(value = "itemgroup")
    @JSONField(name = "itemgroup")
    @JsonProperty("itemgroup")
    private Integer itemgroup;
    /**
     * 最低库存
     */
    @DEField(defaultValue = "0")
    @TableField(value = "lastsum")
    @JSONField(name = "lastsum")
    @JsonProperty("lastsum")
    private Double lastsum;
    /**
     * 库存量
     */
    @TableField(value = "stocksum")
    @JSONField(name = "stocksum")
    @JsonProperty("stocksum")
    private Double stocksum;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 最后入料时间
     */
    @TableField(value = "lastindate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "lastindate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("lastindate")
    private Timestamp lastindate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * sap控制代码
     */
    @TableField(value = "sapcontrolcode")
    @JSONField(name = "sapcontrolcode")
    @JsonProperty("sapcontrolcode")
    private String sapcontrolcode;
    /**
     * 寿命周期(天)
     */
    @TableField(value = "life")
    @JSONField(name = "life")
    @JsonProperty("life")
    private Integer life;
    /**
     * 平均价
     */
    @DEField(defaultValue = "0")
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;
    /**
     * 成本中心
     */
    @TableField(value = "costcenterid")
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    private String costcenterid;
    /**
     * 库存周期(天)
     */
    @DEField(defaultValue = "90")
    @TableField(value = "stockinl")
    @JSONField(name = "stockinl")
    @JsonProperty("stockinl")
    private Double stockinl;
    /**
     * 物品备注
     */
    @TableField(value = "itemdesc")
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    private String itemdesc;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 保修天数
     */
    @TableField(value = "warrantyday")
    @JSONField(name = "warrantyday")
    @JsonProperty("warrantyday")
    private Double warrantyday;
    /**
     * 最新存储仓库
     */
    @TableField(exist = false)
    @JSONField(name = "storename")
    @JsonProperty("storename")
    private String storename;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    private String unitname;
    /**
     * 货架
     */
    @TableField(exist = false)
    @JSONField(name = "emcabname")
    @JsonProperty("emcabname")
    private String emcabname;
    /**
     * 最新存储仓库
     */
    @TableField(exist = false)
    @JSONField(name = "storecode")
    @JsonProperty("storecode")
    private String storecode;
    /**
     * 最新存储库位
     */
    @TableField(exist = false)
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    private String storepartname;
    /**
     * 建议供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    private String labservicename;
    /**
     * 物品二级类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    private String itemmtypeid;
    /**
     * 制造商
     */
    @TableField(exist = false)
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    private String mservicename;
    /**
     * 物品一级类型
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    private String itembtypename;
    /**
     * 物品一级类型
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    private String itembtypeid;
    /**
     * 物品类型代码
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypecode")
    @JsonProperty("itemtypecode")
    private String itemtypecode;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    private String acclassname;
    /**
     * 物品二级类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    private String itemmtypename;
    /**
     * 物品类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypename")
    @JsonProperty("itemtypename")
    private String itemtypename;
    /**
     * 货架
     */
    @TableField(value = "emcabid")
    @JSONField(name = "emcabid")
    @JsonProperty("emcabid")
    private String emcabid;
    /**
     * 单位
     */
    @TableField(value = "unitid")
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    private String unitid;
    /**
     * 最新存储库位
     */
    @TableField(value = "storepartid")
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    private String storepartid;
    /**
     * 物品类型
     */
    @TableField(value = "itemtypeid")
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    private String itemtypeid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    private String acclassid;
    /**
     * 制造商
     */
    @TableField(value = "mserviceid")
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    private String mserviceid;
    /**
     * 建议供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    private String labserviceid;
    /**
     * 最新存储仓库
     */
    @TableField(value = "storeid")
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    private String storeid;
    /**
     * 最新采购员
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 最新采购员
     */
    @TableField(exist = false)
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 库管员
     */
    @TableField(value = "sempid")
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    private String sempid;
    /**
     * 库管员
     */
    @TableField(exist = false)
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    private String sempname;
    /**
     * 最新请购人
     */
    @TableField(value = "lastaempid")
    @JSONField(name = "lastaempid")
    @JsonProperty("lastaempid")
    private String lastaempid;
    /**
     * 最新请购人
     */
    @TableField(value = "lastaempname")
    @JSONField(name = "lastaempname")
    @JsonProperty("lastaempname")
    private String lastaempname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMCab emcab;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemType itemtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService mservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStore store;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfelastaempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfesempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFUnit unit;



    /**
     * 设置 [最新价格]
     */
    public void setLastprice(Double lastprice) {
        this.lastprice = lastprice;
        this.modify("lastprice", lastprice);
    }

    /**
     * 设置 [按资产]
     */
    public void setIsassetflag(Integer isassetflag) {
        this.isassetflag = isassetflag;
        this.modify("isassetflag", isassetflag);
    }

    /**
     * 设置 [最高库存]
     */
    public void setHighsum(Double highsum) {
        this.highsum = highsum;
        this.modify("highsum", highsum);
    }

    /**
     * 设置 [库存金额]
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [按批次]
     */
    public void setIsbatchflag(Integer isbatchflag) {
        this.isbatchflag = isbatchflag;
        this.modify("isbatchflag", isbatchflag);
    }

    /**
     * 设置 [验收方法]
     */
    public void setCheckmethod(String checkmethod) {
        this.checkmethod = checkmethod;
        this.modify("checkmethod", checkmethod);
    }

    /**
     * 设置 [ABC分类]
     */
    public void setAbctype(String abctype) {
        this.abctype = abctype;
        this.modify("abctype", abctype);
    }

    /**
     * 设置 [平均税费]
     */
    public void setShfprice(Double shfprice) {
        this.shfprice = shfprice;
        this.modify("shfprice", shfprice);
    }

    /**
     * 设置 [重订量]
     */
    public void setRepsum(Double repsum) {
        this.repsum = repsum;
        this.modify("repsum", repsum);
    }

    /**
     * 设置 [物品新旧标识]
     */
    public void setIsnew(String isnew) {
        this.isnew = isnew;
        this.modify("isnew", isnew);
    }

    /**
     * 设置 [物品编码(新)]
     */
    public void setItemnid(String itemnid) {
        this.itemnid = itemnid;
        this.modify("itemnid", itemnid);
    }

    /**
     * 设置 [产品系列号]
     */
    public void setItemserialcode(String itemserialcode) {
        this.itemserialcode = itemserialcode;
        this.modify("itemserialcode", itemserialcode);
    }

    /**
     * 设置 [登记日期]
     */
    public void setRegisterdat(Timestamp registerdat) {
        this.registerdat = registerdat;
        this.modify("registerdat", registerdat);
    }

    /**
     * 格式化日期 [登记日期]
     */
    public String formatRegisterdat() {
        if (this.registerdat == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(registerdat);
    }
    /**
     * 设置 [产品型号]
     */
    public void setItemmodelcode(String itemmodelcode) {
        this.itemmodelcode = itemmodelcode;
        this.modify("itemmodelcode", itemmodelcode);
    }

    /**
     * 设置 [物品代码(新)]
     */
    public void setItemncode(String itemncode) {
        this.itemncode = itemncode;
        this.modify("itemncode", itemncode);
    }

    /**
     * 设置 [物品代码]
     */
    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
        this.modify("itemcode", itemcode);
    }

    /**
     * 设置 [不足3家供应商]
     */
    public void setNo3q(Integer no3q) {
        this.no3q = no3q;
        this.modify("no3q", no3q);
    }

    /**
     * 设置 [物品名称]
     */
    public void setEmitemname(String emitemname) {
        this.emitemname = emitemname;
        this.modify("emitemname", emitemname);
    }

    /**
     * 设置 [密度]
     */
    public void setDens(Double dens) {
        this.dens = dens;
        this.modify("dens", dens);
    }

    /**
     * 设置 [sap控制]
     */
    public void setSapcontrol(Integer sapcontrol) {
        this.sapcontrol = sapcontrol;
        this.modify("sapcontrol", sapcontrol);
    }

    /**
     * 设置 [批次类型]
     */
    public void setBatchtype(String batchtype) {
        this.batchtype = batchtype;
        this.modify("batchtype", batchtype);
    }

    /**
     * 设置 [物品分组]
     */
    public void setItemgroup(Integer itemgroup) {
        this.itemgroup = itemgroup;
        this.modify("itemgroup", itemgroup);
    }

    /**
     * 设置 [最低库存]
     */
    public void setLastsum(Double lastsum) {
        this.lastsum = lastsum;
        this.modify("lastsum", lastsum);
    }

    /**
     * 设置 [库存量]
     */
    public void setStocksum(Double stocksum) {
        this.stocksum = stocksum;
        this.modify("stocksum", stocksum);
    }

    /**
     * 设置 [最后入料时间]
     */
    public void setLastindate(Timestamp lastindate) {
        this.lastindate = lastindate;
        this.modify("lastindate", lastindate);
    }

    /**
     * 格式化日期 [最后入料时间]
     */
    public String formatLastindate() {
        if (this.lastindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastindate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [sap控制代码]
     */
    public void setSapcontrolcode(String sapcontrolcode) {
        this.sapcontrolcode = sapcontrolcode;
        this.modify("sapcontrolcode", sapcontrolcode);
    }

    /**
     * 设置 [寿命周期(天)]
     */
    public void setLife(Integer life) {
        this.life = life;
        this.modify("life", life);
    }

    /**
     * 设置 [平均价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [成本中心]
     */
    public void setCostcenterid(String costcenterid) {
        this.costcenterid = costcenterid;
        this.modify("costcenterid", costcenterid);
    }

    /**
     * 设置 [库存周期(天)]
     */
    public void setStockinl(Double stockinl) {
        this.stockinl = stockinl;
        this.modify("stockinl", stockinl);
    }

    /**
     * 设置 [物品备注]
     */
    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
        this.modify("itemdesc", itemdesc);
    }

    /**
     * 设置 [保修天数]
     */
    public void setWarrantyday(Double warrantyday) {
        this.warrantyday = warrantyday;
        this.modify("warrantyday", warrantyday);
    }

    /**
     * 设置 [货架]
     */
    public void setEmcabid(String emcabid) {
        this.emcabid = emcabid;
        this.modify("emcabid", emcabid);
    }

    /**
     * 设置 [单位]
     */
    public void setUnitid(String unitid) {
        this.unitid = unitid;
        this.modify("unitid", unitid);
    }

    /**
     * 设置 [最新存储库位]
     */
    public void setStorepartid(String storepartid) {
        this.storepartid = storepartid;
        this.modify("storepartid", storepartid);
    }

    /**
     * 设置 [物品类型]
     */
    public void setItemtypeid(String itemtypeid) {
        this.itemtypeid = itemtypeid;
        this.modify("itemtypeid", itemtypeid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [制造商]
     */
    public void setMserviceid(String mserviceid) {
        this.mserviceid = mserviceid;
        this.modify("mserviceid", mserviceid);
    }

    /**
     * 设置 [建议供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [最新存储仓库]
     */
    public void setStoreid(String storeid) {
        this.storeid = storeid;
        this.modify("storeid", storeid);
    }

    /**
     * 设置 [最新采购员]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [库管员]
     */
    public void setSempid(String sempid) {
        this.sempid = sempid;
        this.modify("sempid", sempid);
    }

    /**
     * 设置 [最新请购人]
     */
    public void setLastaempid(String lastaempid) {
        this.lastaempid = lastaempid;
        this.modify("lastaempid", lastaempid);
    }

    /**
     * 设置 [最新请购人]
     */
    public void setLastaempname(String lastaempname) {
        this.lastaempname = lastaempname;
        this.modify("lastaempname", lastaempname);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emitemid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


