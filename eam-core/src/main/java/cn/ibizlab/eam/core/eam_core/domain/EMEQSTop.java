package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备停机监控表]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQSTOP_BASE", resultMap = "EMEQSTopResultMap")
public class EMEQSTop extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 28
     */
    @TableField(exist = false)
    @JSONField(name = "day28")
    @JsonProperty("day28")
    private String day28;
    /**
     * 3
     */
    @TableField(exist = false)
    @JSONField(name = "day3")
    @JsonProperty("day3")
    private String day3;
    /**
     * 17
     */
    @TableField(exist = false)
    @JSONField(name = "day17")
    @JsonProperty("day17")
    private String day17;
    /**
     * 12
     */
    @TableField(exist = false)
    @JSONField(name = "day12")
    @JsonProperty("day12")
    private String day12;
    /**
     * 6
     */
    @TableField(exist = false)
    @JSONField(name = "day6")
    @JsonProperty("day6")
    private String day6;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 26
     */
    @TableField(exist = false)
    @JSONField(name = "day26")
    @JsonProperty("day26")
    private String day26;
    /**
     * 组织
     */
    @DEField(preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 7
     */
    @TableField(exist = false)
    @JSONField(name = "day7")
    @JsonProperty("day7")
    private String day7;
    /**
     * 4
     */
    @TableField(exist = false)
    @JSONField(name = "day4")
    @JsonProperty("day4")
    private String day4;
    /**
     * 合计/小时
     */
    @TableField(exist = false)
    @JSONField(name = "hsum")
    @JsonProperty("hsum")
    private BigDecimal hsum;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 1
     */
    @TableField(exist = false)
    @JSONField(name = "day1")
    @JsonProperty("day1")
    private String day1;
    /**
     * 21
     */
    @TableField(exist = false)
    @JSONField(name = "day21")
    @JsonProperty("day21")
    private String day21;
    /**
     * 25
     */
    @TableField(exist = false)
    @JSONField(name = "day25")
    @JsonProperty("day25")
    private String day25;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 19
     */
    @TableField(exist = false)
    @JSONField(name = "day19")
    @JsonProperty("day19")
    private String day19;
    /**
     * 30
     */
    @TableField(exist = false)
    @JSONField(name = "day30")
    @JsonProperty("day30")
    private String day30;
    /**
     * 11
     */
    @TableField(exist = false)
    @JSONField(name = "day11")
    @JsonProperty("day11")
    private String day11;
    /**
     * 29
     */
    @TableField(exist = false)
    @JSONField(name = "day29")
    @JsonProperty("day29")
    private String day29;
    /**
     * 16
     */
    @TableField(exist = false)
    @JSONField(name = "day16")
    @JsonProperty("day16")
    private String day16;
    /**
     * 8
     */
    @TableField(exist = false)
    @JSONField(name = "day8")
    @JsonProperty("day8")
    private String day8;
    /**
     * 设备停机监控表标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqstopid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqstopid")
    @JsonProperty("emeqstopid")
    private String emeqstopid;
    /**
     * 14
     */
    @TableField(exist = false)
    @JSONField(name = "day14")
    @JsonProperty("day14")
    private String day14;
    /**
     * 10
     */
    @TableField(exist = false)
    @JSONField(name = "day10")
    @JsonProperty("day10")
    private String day10;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 15
     */
    @TableField(exist = false)
    @JSONField(name = "day15")
    @JsonProperty("day15")
    private String day15;
    /**
     * 24
     */
    @TableField(exist = false)
    @JSONField(name = "day24")
    @JsonProperty("day24")
    private String day24;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 5
     */
    @TableField(exist = false)
    @JSONField(name = "day5")
    @JsonProperty("day5")
    private String day5;
    /**
     * 22
     */
    @TableField(exist = false)
    @JSONField(name = "day22")
    @JsonProperty("day22")
    private String day22;
    /**
     * 18
     */
    @TableField(exist = false)
    @JSONField(name = "day18")
    @JsonProperty("day18")
    private String day18;
    /**
     * 23
     */
    @TableField(exist = false)
    @JSONField(name = "day23")
    @JsonProperty("day23")
    private String day23;
    /**
     * 27
     */
    @TableField(exist = false)
    @JSONField(name = "day27")
    @JsonProperty("day27")
    private String day27;
    /**
     * 20
     */
    @TableField(exist = false)
    @JSONField(name = "day20")
    @JsonProperty("day20")
    private String day20;
    /**
     * 31
     */
    @TableField(exist = false)
    @JSONField(name = "day31")
    @JsonProperty("day31")
    private String day31;
    /**
     * 设备停机监控表名称
     */
    @TableField(value = "emeqstopname")
    @JSONField(name = "emeqstopname")
    @JsonProperty("emeqstopname")
    private String emeqstopname;
    /**
     * 合计/分
     */
    @TableField(exist = false)
    @JSONField(name = "msum")
    @JsonProperty("msum")
    private BigDecimal msum;
    /**
     * 9
     */
    @TableField(exist = false)
    @JSONField(name = "day9")
    @JsonProperty("day9")
    private String day9;
    /**
     * 13
     */
    @TableField(exist = false)
    @JSONField(name = "day13")
    @JsonProperty("day13")
    private String day13;
    /**
     * 2
     */
    @TableField(exist = false)
    @JSONField(name = "day2")
    @JsonProperty("day2")
    private String day2;
    /**
     * 设备类型
     */
    @TableField(exist = false)
    @JSONField(name = "emeqtypename")
    @JsonProperty("emeqtypename")
    private String emeqtypename;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    private String emequipname;
    /**
     * 设备类型
     */
    @TableField(value = "emeqtypeid")
    @JSONField(name = "emeqtypeid")
    @JsonProperty("emeqtypeid")
    private String emeqtypeid;
    /**
     * 设备
     */
    @TableField(value = "emequipid")
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    private String emequipid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQType emeqtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip emequip;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [设备停机监控表名称]
     */
    public void setEmeqstopname(String emeqstopname) {
        this.emeqstopname = emeqstopname;
        this.modify("emeqstopname", emeqstopname);
    }

    /**
     * 设置 [设备类型]
     */
    public void setEmeqtypeid(String emeqtypeid) {
        this.emeqtypeid = emeqtypeid;
        this.modify("emeqtypeid", emeqtypeid);
    }

    /**
     * 设置 [设备]
     */
    public void setEmequipid(String emequipid) {
        this.emequipid = emequipid;
        this.modify("emequipid", emequipid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqstopid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


