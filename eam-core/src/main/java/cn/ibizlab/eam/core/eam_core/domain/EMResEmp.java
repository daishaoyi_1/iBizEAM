package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[员工资源]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMRESEMP_BASE", resultMap = "EMResEmpResultMap")
public class EMResEmp extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 单价
     */
    @DEField(defaultValue = "0")
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    private String price;
    /**
     * 员工资源标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emresempid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emresempid")
    @JsonProperty("emresempid")
    private String emresempid;
    /**
     * 总金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private String amount;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 结束时间
     */
    @TableField(value = "edate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "edate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    private Timestamp edate;
    /**
     * 员工
     */
    @TableField(value = "resid")
    @JSONField(name = "resid")
    @JsonProperty("resid")
    private String resid;
    /**
     * 员工
     */
    @TableField(value = "resname")
    @JSONField(name = "resname")
    @JsonProperty("resname")
    private String resname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 安排工时(分)
     */
    @TableField(value = "pnum")
    @JSONField(name = "pnum")
    @JsonProperty("pnum")
    private Double pnum;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 数据来源
     */
    @DEField(defaultValue = "输入")
    @TableField(value = "datafrom")
    @JSONField(name = "datafrom")
    @JsonProperty("datafrom")
    private String datafrom;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 员工资源名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emresempname")
    @JSONField(name = "emresempname")
    @JsonProperty("emresempname")
    private String emresempname;
    /**
     * 开始时间
     */
    @TableField(value = "bdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "bdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    private Timestamp bdate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 实际工时(分)
     */
    @TableField(value = "snum")
    @JSONField(name = "snum")
    @JsonProperty("snum")
    private Double snum;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "team_d")
    @JsonProperty("team_d")
    private String teamD;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 引用对象
     */
    @TableField(exist = false)
    @JSONField(name = "resrefobjname")
    @JsonProperty("resrefobjname")
    private String resrefobjname;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 引用对象
     */
    @TableField(value = "resrefobjid")
    @JSONField(name = "resrefobjid")
    @JsonProperty("resrefobjid")
    private String resrefobjid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMResRefObj resrefobj;



    /**
     * 设置 [单价]
     */
    public void setPrice(String price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [总金额]
     */
    public void setAmount(String amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [结束时间]
     */
    public void setEdate(Timestamp edate) {
        this.edate = edate;
        this.modify("edate", edate);
    }

    /**
     * 格式化日期 [结束时间]
     */
    public String formatEdate() {
        if (this.edate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(edate);
    }
    /**
     * 设置 [员工]
     */
    public void setResid(String resid) {
        this.resid = resid;
        this.modify("resid", resid);
    }

    /**
     * 设置 [员工]
     */
    public void setResname(String resname) {
        this.resname = resname;
        this.modify("resname", resname);
    }

    /**
     * 设置 [安排工时(分)]
     */
    public void setPnum(Double pnum) {
        this.pnum = pnum;
        this.modify("pnum", pnum);
    }

    /**
     * 设置 [数据来源]
     */
    public void setDatafrom(String datafrom) {
        this.datafrom = datafrom;
        this.modify("datafrom", datafrom);
    }

    /**
     * 设置 [员工资源名称]
     */
    public void setEmresempname(String emresempname) {
        this.emresempname = emresempname;
        this.modify("emresempname", emresempname);
    }

    /**
     * 设置 [开始时间]
     */
    public void setBdate(Timestamp bdate) {
        this.bdate = bdate;
        this.modify("bdate", bdate);
    }

    /**
     * 格式化日期 [开始时间]
     */
    public String formatBdate() {
        if (this.bdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(bdate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [实际工时(分)]
     */
    public void setSnum(Double snum) {
        this.snum = snum;
        this.modify("snum", snum);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [引用对象]
     */
    public void setResrefobjid(String resrefobjid) {
        this.resrefobjid = resrefobjid;
        this.modify("resrefobjid", resrefobjid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emresempid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


