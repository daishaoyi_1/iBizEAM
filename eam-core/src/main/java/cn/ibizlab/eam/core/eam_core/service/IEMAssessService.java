package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMAssess;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMAssess] 服务对象接口
 */
public interface IEMAssessService extends IService<EMAssess> {

    boolean create(EMAssess et);
    void createBatch(List<EMAssess> list);
    boolean update(EMAssess et);
    void updateBatch(List<EMAssess> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMAssess get(String key);
    EMAssess getDraft(EMAssess et);
    boolean checkKey(EMAssess et);
    boolean save(EMAssess et);
    void saveBatch(List<EMAssess> list);
    Page<EMAssess> searchDefault(EMAssessSearchContext context);
    List<EMAssess> selectByPfteamid(String pfteamid);
    void removeByPfteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMAssess> getEmassessByIds(List<String> ids);
    List<EMAssess> getEmassessByEntities(List<EMAssess> entities);
}


