package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_PT;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_PTSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMWO_PTMapper extends BaseMapper<EMWO_PT> {

    Page<EMWO_PT> searchDefault(IPage page, @Param("srf") EMWO_PTSearchContext context, @Param("ew") Wrapper<EMWO_PT> wrapper);
    @Override
    EMWO_PT selectById(Serializable id);
    @Override
    int insert(EMWO_PT entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMWO_PT entity);
    @Override
    int update(@Param(Constants.ENTITY) EMWO_PT entity, @Param("ew") Wrapper<EMWO_PT> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMWO_PT> selectByAcclassid(@Param("emacclassid") Serializable emacclassid);

    List<EMWO_PT> selectByEquipid(@Param("emequipid") Serializable emequipid);

    List<EMWO_PT> selectByDpid(@Param("emobjectid") Serializable emobjectid);

    List<EMWO_PT> selectByObjid(@Param("emobjectid") Serializable emobjectid);

    List<EMWO_PT> selectByRfoacid(@Param("emrfoacid") Serializable emrfoacid);

    List<EMWO_PT> selectByRfocaid(@Param("emrfocaid") Serializable emrfocaid);

    List<EMWO_PT> selectByRfodeid(@Param("emrfodeid") Serializable emrfodeid);

    List<EMWO_PT> selectByRfomoid(@Param("emrfomoid") Serializable emrfomoid);

    List<EMWO_PT> selectByRserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMWO_PT> selectByWooriid(@Param("emwooriid") Serializable emwooriid);

    List<EMWO_PT> selectByWopid(@Param("emwoid") Serializable emwoid);

    List<EMWO_PT> selectByRteamid(@Param("pfteamid") Serializable pfteamid);

}
