package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划及项目进程考核明细]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMASSESSMENTMX_BASE", resultMap = "EMAssessMentMXResultMap")
public class EMAssessMentMX extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 主要责任人
     */
    @TableField(value = "person")
    @JSONField(name = "person")
    @JsonProperty("person")
    private String person;
    /**
     * 主要责任人
     */
    @TableField(value = "pfempid")
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    private String pfempid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 考核
     */
    @TableField(value = "assessment")
    @JSONField(name = "assessment")
    @JsonProperty("assessment")
    private String assessment;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 计划及项目进程考核明细标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emassessmentmxid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emassessmentmxid")
    @JsonProperty("emassessmentmxid")
    private String emassessmentmxid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 序号
     */
    @TableField(value = "xh")
    @JSONField(name = "xh")
    @JsonProperty("xh")
    private Integer xh;
    /**
     * 时间设定
     */
    @TableField(value = "timeset")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "timeset", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("timeset")
    private Timestamp timeset;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 时间区段
     */
    @TableField(value = "time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "time", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("time")
    private Timestamp time;
    /**
     * 计划内容
     */
    @TableField(value = "emassessmentmxname")
    @JSONField(name = "emassessmentmxname")
    @JsonProperty("emassessmentmxname")
    private String emassessmentmxname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 主要责任人
     */
    @TableField(value = "pfempname")
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    private String pfempname;
    /**
     * 完成日期
     */
    @TableField(value = "finishdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "finishdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("finishdate")
    private Timestamp finishdate;
    /**
     * 计划及项目进程考核
     */
    @TableField(exist = false)
    @JSONField(name = "emassessmentname")
    @JsonProperty("emassessmentname")
    private String emassessmentname;
    /**
     * 计划及项目进程考核
     */
    @TableField(value = "emassessmentid")
    @JSONField(name = "emassessmentid")
    @JsonProperty("emassessmentid")
    private String emassessmentid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMAssessMent emassessment;



    /**
     * 设置 [主要责任人]
     */
    public void setPerson(String person) {
        this.person = person;
        this.modify("person", person);
    }

    /**
     * 设置 [主要责任人]
     */
    public void setPfempid(String pfempid) {
        this.pfempid = pfempid;
        this.modify("pfempid", pfempid);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [考核]
     */
    public void setAssessment(String assessment) {
        this.assessment = assessment;
        this.modify("assessment", assessment);
    }

    /**
     * 设置 [序号]
     */
    public void setXh(Integer xh) {
        this.xh = xh;
        this.modify("xh", xh);
    }

    /**
     * 设置 [时间设定]
     */
    public void setTimeset(Timestamp timeset) {
        this.timeset = timeset;
        this.modify("timeset", timeset);
    }

    /**
     * 格式化日期 [时间设定]
     */
    public String formatTimeset() {
        if (this.timeset == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(timeset);
    }
    /**
     * 设置 [时间区段]
     */
    public void setTime(Timestamp time) {
        this.time = time;
        this.modify("time", time);
    }

    /**
     * 格式化日期 [时间区段]
     */
    public String formatTime() {
        if (this.time == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }
    /**
     * 设置 [计划内容]
     */
    public void setEmassessmentmxname(String emassessmentmxname) {
        this.emassessmentmxname = emassessmentmxname;
        this.modify("emassessmentmxname", emassessmentmxname);
    }

    /**
     * 设置 [主要责任人]
     */
    public void setPfempname(String pfempname) {
        this.pfempname = pfempname;
        this.modify("pfempname", pfempname);
    }

    /**
     * 设置 [完成日期]
     */
    public void setFinishdate(Timestamp finishdate) {
        this.finishdate = finishdate;
        this.modify("finishdate", finishdate);
    }

    /**
     * 格式化日期 [完成日期]
     */
    public String formatFinishdate() {
        if (this.finishdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(finishdate);
    }
    /**
     * 设置 [计划及项目进程考核]
     */
    public void setEmassessmentid(String emassessmentid) {
        this.emassessmentid = emassessmentid;
        this.modify("emassessmentid", emassessmentid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emassessmentmxid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


