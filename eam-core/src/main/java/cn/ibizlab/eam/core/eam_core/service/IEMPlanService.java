package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPlan;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPlan] 服务对象接口
 */
public interface IEMPlanService extends IService<EMPlan> {

    boolean create(EMPlan et);
    void createBatch(List<EMPlan> list);
    boolean update(EMPlan et);
    void updateBatch(List<EMPlan> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMPlan get(String key);
    EMPlan getDraft(EMPlan et);
    boolean checkKey(EMPlan et);
    EMPlan createWO(EMPlan et);
    boolean createWOBatch(List<EMPlan> etList);
    EMPlan formUpdateByEQUIP(EMPlan et);
    boolean save(EMPlan et);
    void saveBatch(List<EMPlan> list);
    Page<EMPlan> searchDefault(EMPlanSearchContext context);
    List<EMPlan> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMPlan> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMPlan> selectByDpid(String emobjectid);
    void removeByDpid(String emobjectid);
    List<EMPlan> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMPlan> selectByPlantemplid(String emplantemplid);
    void removeByPlantemplid(String emplantemplid);
    List<EMPlan> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMPlan> selectByRdeptid(String pfdeptid);
    void removeByRdeptid(String pfdeptid);
    List<EMPlan> selectByMpersonid(String pfempid);
    void removeByMpersonid(String pfempid);
    List<EMPlan> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMPlan> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPlan> getEmplanByIds(List<String> ids);
    List<EMPlan> getEmplanByEntities(List<EMPlan> entities);
}


