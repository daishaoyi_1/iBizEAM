package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEICellReturn;
/**
 * 关系型数据实体[EMEICellReturn] 查询条件对象
 */
@Slf4j
@Data
public class EMEICellReturnSearchContext extends QueryWrapperContext<EMEICellReturn> {

	private String n_activeinfo_like;//[归还信息]
	public void setN_activeinfo_like(String n_activeinfo_like) {
        this.n_activeinfo_like = n_activeinfo_like;
        if(!ObjectUtils.isEmpty(this.n_activeinfo_like)){
            this.getSearchCond().like("activeinfo", n_activeinfo_like);
        }
    }
	private String n_emeicellreturnname_like;//[对讲机归还记录名称]
	public void setN_emeicellreturnname_like(String n_emeicellreturnname_like) {
        this.n_emeicellreturnname_like = n_emeicellreturnname_like;
        if(!ObjectUtils.isEmpty(this.n_emeicellreturnname_like)){
            this.getSearchCond().like("emeicellreturnname", n_emeicellreturnname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_empid_eq;//[归还人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_empname_eq;//[归还人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[归还人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_eiobjname_eq;//[对讲机]
	public void setN_eiobjname_eq(String n_eiobjname_eq) {
        this.n_eiobjname_eq = n_eiobjname_eq;
        if(!ObjectUtils.isEmpty(this.n_eiobjname_eq)){
            this.getSearchCond().eq("eiobjname", n_eiobjname_eq);
        }
    }
	private String n_eiobjname_like;//[对讲机]
	public void setN_eiobjname_like(String n_eiobjname_like) {
        this.n_eiobjname_like = n_eiobjname_like;
        if(!ObjectUtils.isEmpty(this.n_eiobjname_like)){
            this.getSearchCond().like("eiobjname", n_eiobjname_like);
        }
    }
	private String n_eiobjid_eq;//[对讲机]
	public void setN_eiobjid_eq(String n_eiobjid_eq) {
        this.n_eiobjid_eq = n_eiobjid_eq;
        if(!ObjectUtils.isEmpty(this.n_eiobjid_eq)){
            this.getSearchCond().eq("eiobjid", n_eiobjid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeicellreturnname", query)
            );
		 }
	}
}



