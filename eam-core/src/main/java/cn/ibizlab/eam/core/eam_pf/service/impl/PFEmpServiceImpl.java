package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmp;
import cn.ibizlab.eam.core.eam_pf.filter.PFEmpSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFEmpService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFEmpMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[职员] 服务对象接口实现
 */
@Slf4j
@Service("PFEmpServiceImpl")
public class PFEmpServiceImpl extends ServiceImpl<PFEmpMapper, PFEmp> implements IPFEmpService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemCSService emitemcsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPLService emitemplService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService emitemprtnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemRInService emitemrinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemROutService emitemroutService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPODetailService empodetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPOService empoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStoreService emstoreService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwoDpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwoEnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwoOscService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFDeptService pfdeptService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFEmp et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfempid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFEmp et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("pfempid", et.getPfempid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfempid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFEmp get(String key) {
        PFEmp et = getById(key);
        if(et == null){
            et = new PFEmp();
            et.setPfempid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PFEmp getDraft(PFEmp et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(PFEmp et) {
        return (!ObjectUtils.isEmpty(et.getPfempid())) && (!Objects.isNull(this.getById(et.getPfempid())));
    }
    @Override
    @Transactional
    public boolean save(PFEmp et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFEmp et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        List<PFEmp> create = new ArrayList<>();
        List<PFEmp> update = new ArrayList<>();
        for (PFEmp et : list) {
            if (ObjectUtils.isEmpty(et.getPfempid()) || ObjectUtils.isEmpty(getById(et.getPfempid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        List<PFEmp> create = new ArrayList<>();
        List<PFEmp> update = new ArrayList<>();
        for (PFEmp et : list) {
            if (ObjectUtils.isEmpty(et.getPfempid()) || ObjectUtils.isEmpty(getById(et.getPfempid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<PFEmp> selectByMajordeptid(String pfdeptid) {
        return baseMapper.selectByMajordeptid(pfdeptid);
    }
    @Override
    public void removeByMajordeptid(String pfdeptid) {
        this.remove(new QueryWrapper<PFEmp>().eq("majordeptid",pfdeptid));
    }

	@Override
    public List<PFEmp> selectByMajorteamid(String pfteamid) {
        return baseMapper.selectByMajorteamid(pfteamid);
    }
    @Override
    public void removeByMajorteamid(String pfteamid) {
        this.remove(new QueryWrapper<PFEmp>().eq("majorteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFEmp> searchDefault(PFEmpSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFEmp> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFEmp>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 部门下职员
     */
    @Override
    public Page<PFEmp> searchDeptEmp(PFEmpSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFEmp> pages=baseMapper.searchDeptEmp(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFEmp>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(PFEmp et){
        //实体关系[DER1N_PFEMP_PFDEPT_MAJORDEPTID]
        if(!ObjectUtils.isEmpty(et.getMajordeptid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptmp=et.getPfdeptmp();
            if(ObjectUtils.isEmpty(pfdeptmp)){
                cn.ibizlab.eam.core.eam_pf.domain.PFDept majorEntity=pfdeptService.get(et.getMajordeptid());
                et.setPfdeptmp(majorEntity);
                pfdeptmp=majorEntity;
            }
            et.setMajordeptname(pfdeptmp.getDeptinfo());
        }
        //实体关系[DER1N_PFEMP_PFTEAM_MAJORTEAMID]
        if(!ObjectUtils.isEmpty(et.getMajorteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorteam=et.getMajorteam();
            if(ObjectUtils.isEmpty(majorteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getMajorteamid());
                et.setMajorteam(majorEntity);
                majorteam=majorEntity;
            }
            et.setMajorteamname(majorteam.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFEmp> getPfempByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFEmp> getPfempByEntities(List<PFEmp> entities) {
        List ids =new ArrayList();
        for(PFEmp entity : entities){
            Serializable id=entity.getPfempid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPFEmpService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



