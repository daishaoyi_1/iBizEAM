package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备故障考核]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMASSESS_BASE", resultMap = "EMAssessResultMap")
public class EMAssess extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 指数
     */
    @TableField(value = "exponent")
    @JSONField(name = "exponent")
    @JsonProperty("exponent")
    private Double exponent;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 设备故障考核名称
     */
    @TableField(value = "emassessname")
    @JSONField(name = "emassessname")
    @JsonProperty("emassessname")
    private String emassessname;
    /**
     * 本月故障指数
     */
    @TableField(value = "faultindex")
    @JSONField(name = "faultindex")
    @JsonProperty("faultindex")
    private Double faultindex;
    /**
     * 故障数
     */
    @TableField(value = "faultamount")
    @JSONField(name = "faultamount")
    @JsonProperty("faultamount")
    private Double faultamount;
    /**
     * 填写人
     */
    @TableField(value = "pfempname")
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    private String pfempname;
    /**
     * 本月作业箱量
     */
    @TableField(value = "operationvolume")
    @JSONField(name = "operationvolume")
    @JsonProperty("operationvolume")
    private Double operationvolume;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 备注
     */
    @TableField(value = "remark")
    @JSONField(name = "remark")
    @JsonProperty("remark")
    private String remark;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 设备故障考核标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emassessid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emassessid")
    @JsonProperty("emassessid")
    private String emassessid;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 填写人
     */
    @TableField(value = "pfempid")
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    private String pfempid;
    /**
     * 箱量
     */
    @TableField(value = "volume")
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;
    /**
     * 设备故障次数
     */
    @TableField(value = "faulttime")
    @JSONField(name = "faulttime")
    @JsonProperty("faulttime")
    private Double faulttime;
    /**
     * 指数差额
     */
    @TableField(value = "indexbanlance")
    @JSONField(name = "indexbanlance")
    @JsonProperty("indexbanlance")
    private Double indexbanlance;
    /**
     * 设备班组
     */
    @TableField(exist = false)
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    private String pfteamname;
    /**
     * 设备班组
     */
    @TableField(value = "pfteamid")
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    private String pfteamid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam pfteam;



    /**
     * 设置 [指数]
     */
    public void setExponent(Double exponent) {
        this.exponent = exponent;
        this.modify("exponent", exponent);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [设备故障考核名称]
     */
    public void setEmassessname(String emassessname) {
        this.emassessname = emassessname;
        this.modify("emassessname", emassessname);
    }

    /**
     * 设置 [本月故障指数]
     */
    public void setFaultindex(Double faultindex) {
        this.faultindex = faultindex;
        this.modify("faultindex", faultindex);
    }

    /**
     * 设置 [故障数]
     */
    public void setFaultamount(Double faultamount) {
        this.faultamount = faultamount;
        this.modify("faultamount", faultamount);
    }

    /**
     * 设置 [填写人]
     */
    public void setPfempname(String pfempname) {
        this.pfempname = pfempname;
        this.modify("pfempname", pfempname);
    }

    /**
     * 设置 [本月作业箱量]
     */
    public void setOperationvolume(Double operationvolume) {
        this.operationvolume = operationvolume;
        this.modify("operationvolume", operationvolume);
    }

    /**
     * 设置 [备注]
     */
    public void setRemark(String remark) {
        this.remark = remark;
        this.modify("remark", remark);
    }

    /**
     * 设置 [填写人]
     */
    public void setPfempid(String pfempid) {
        this.pfempid = pfempid;
        this.modify("pfempid", pfempid);
    }

    /**
     * 设置 [箱量]
     */
    public void setVolume(Double volume) {
        this.volume = volume;
        this.modify("volume", volume);
    }

    /**
     * 设置 [设备故障次数]
     */
    public void setFaulttime(Double faulttime) {
        this.faulttime = faulttime;
        this.modify("faulttime", faulttime);
    }

    /**
     * 设置 [指数差额]
     */
    public void setIndexbanlance(Double indexbanlance) {
        this.indexbanlance = indexbanlance;
        this.modify("indexbanlance", indexbanlance);
    }

    /**
     * 设置 [设备班组]
     */
    public void setPfteamid(String pfteamid) {
        this.pfteamid = pfteamid;
        this.modify("pfteamid", pfteamid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emassessid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


