package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[加班工单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMWORK_BASE", resultMap = "EMWorkResultMap")
public class EMWork extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 加班工作内容
     */
    @TableField(value = "jbgznr")
    @JSONField(name = "jbgznr")
    @JsonProperty("jbgznr")
    private String jbgznr;
    /**
     * 加班日期
     */
    @TableField(value = "workdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "workdate", format = "yyyy-MM-dd")
    @JsonProperty("workdate")
    private Timestamp workdate;
    /**
     * 加班时间(分)
     */
    @TableField(value = "overtime")
    @JSONField(name = "overtime")
    @JsonProperty("overtime")
    private Double overtime;
    /**
     * 员工
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 加班区分
     */
    @TableField(value = "workstate")
    @JSONField(name = "workstate")
    @JsonProperty("workstate")
    private String workstate;
    /**
     * 上午结束时间
     */
    @TableField(value = "amendtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "amendtime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("amendtime")
    private Timestamp amendtime;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 员工
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 开始时间
     */
    @TableField(value = "ambegintime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "ambegintime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("ambegintime")
    private Timestamp ambegintime;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 加班工单状态
     */
    @TableField(value = "emworkstate")
    @JSONField(name = "emworkstate")
    @JsonProperty("emworkstate")
    private Integer emworkstate;
    /**
     * 加班工单名称
     */
    @TableField(value = "emworkname")
    @JSONField(name = "emworkname")
    @JsonProperty("emworkname")
    private String emworkname;
    /**
     * 结束时间
     */
    @TableField(value = "pmendtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "pmendtime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("pmendtime")
    private Timestamp pmendtime;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 加班工单标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emworkid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emworkid")
    @JsonProperty("emworkid")
    private String emworkid;
    /**
     * 岗位
     */
    @TableField(value = "post")
    @JSONField(name = "post")
    @JsonProperty("post")
    private String post;
    /**
     * 工作地点
     */
    @TableField(value = "workplace")
    @JSONField(name = "workplace")
    @JsonProperty("workplace")
    private String workplace;
    /**
     * 下午开始时间
     */
    @TableField(value = "pmbegintime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "pmbegintime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("pmbegintime")
    private Timestamp pmbegintime;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;



    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [加班工作内容]
     */
    public void setJbgznr(String jbgznr) {
        this.jbgznr = jbgznr;
        this.modify("jbgznr", jbgznr);
    }

    /**
     * 设置 [加班日期]
     */
    public void setWorkdate(Timestamp workdate) {
        this.workdate = workdate;
        this.modify("workdate", workdate);
    }

    /**
     * 格式化日期 [加班日期]
     */
    public String formatWorkdate() {
        if (this.workdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(workdate);
    }
    /**
     * 设置 [加班时间(分)]
     */
    public void setOvertime(Double overtime) {
        this.overtime = overtime;
        this.modify("overtime", overtime);
    }

    /**
     * 设置 [员工]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [加班区分]
     */
    public void setWorkstate(String workstate) {
        this.workstate = workstate;
        this.modify("workstate", workstate);
    }

    /**
     * 设置 [上午结束时间]
     */
    public void setAmendtime(Timestamp amendtime) {
        this.amendtime = amendtime;
        this.modify("amendtime", amendtime);
    }

    /**
     * 格式化日期 [上午结束时间]
     */
    public String formatAmendtime() {
        if (this.amendtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(amendtime);
    }
    /**
     * 设置 [员工]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [开始时间]
     */
    public void setAmbegintime(Timestamp ambegintime) {
        this.ambegintime = ambegintime;
        this.modify("ambegintime", ambegintime);
    }

    /**
     * 格式化日期 [开始时间]
     */
    public String formatAmbegintime() {
        if (this.ambegintime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(ambegintime);
    }
    /**
     * 设置 [加班工单状态]
     */
    public void setEmworkstate(Integer emworkstate) {
        this.emworkstate = emworkstate;
        this.modify("emworkstate", emworkstate);
    }

    /**
     * 设置 [加班工单名称]
     */
    public void setEmworkname(String emworkname) {
        this.emworkname = emworkname;
        this.modify("emworkname", emworkname);
    }

    /**
     * 设置 [结束时间]
     */
    public void setPmendtime(Timestamp pmendtime) {
        this.pmendtime = pmendtime;
        this.modify("pmendtime", pmendtime);
    }

    /**
     * 格式化日期 [结束时间]
     */
    public String formatPmendtime() {
        if (this.pmendtime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(pmendtime);
    }
    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [岗位]
     */
    public void setPost(String post) {
        this.post = post;
        this.modify("post", post);
    }

    /**
     * 设置 [工作地点]
     */
    public void setWorkplace(String workplace) {
        this.workplace = workplace;
        this.modify("workplace", workplace);
    }

    /**
     * 设置 [下午开始时间]
     */
    public void setPmbegintime(Timestamp pmbegintime) {
        this.pmbegintime = pmbegintime;
        this.modify("pmbegintime", pmbegintime);
    }

    /**
     * 格式化日期 [下午开始时间]
     */
    public String formatPmbegintime() {
        if (this.pmbegintime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(pmbegintime);
    }
    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emworkid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


