package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMWO;
import cn.ibizlab.eam.core.eam_core.filter.EMWOSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMWOMapper extends BaseMapper<EMWO> {

    Page<EMWO> searchDefault(IPage page, @Param("srf") EMWOSearchContext context, @Param("ew") Wrapper<EMWO> wrapper);
    Page<Map> searchEQYearWO(IPage page, @Param("srf") EMWOSearchContext context, @Param("ew") Wrapper<EMWO> wrapper);
    Page<EMWO> searchIndexDER(IPage page, @Param("srf") EMWOSearchContext context, @Param("ew") Wrapper<EMWO> wrapper);
    Page<Map> searchLaterThreeYear(IPage page, @Param("srf") EMWOSearchContext context, @Param("ew") Wrapper<EMWO> wrapper);
    Page<Map> searchWoTypeNum(IPage page, @Param("srf") EMWOSearchContext context, @Param("ew") Wrapper<EMWO> wrapper);
    Page<Map> searchYearWONumByPlan(IPage page, @Param("srf") EMWOSearchContext context, @Param("ew") Wrapper<EMWO> wrapper);
    @Override
    EMWO selectById(Serializable id);
    @Override
    int insert(EMWO entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMWO entity);
    @Override
    int update(@Param(Constants.ENTITY) EMWO entity, @Param("ew") Wrapper<EMWO> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMWO> selectByAcclassid(@Param("emacclassid") Serializable emacclassid);

    List<EMWO> selectByEquipid(@Param("emequipid") Serializable emequipid);

    List<EMWO> selectByDpid(@Param("emobjectid") Serializable emobjectid);

    List<EMWO> selectByObjid(@Param("emobjectid") Serializable emobjectid);

    List<EMWO> selectByRfoacid(@Param("emrfoacid") Serializable emrfoacid);

    List<EMWO> selectByRfocaid(@Param("emrfocaid") Serializable emrfocaid);

    List<EMWO> selectByRfodeid(@Param("emrfodeid") Serializable emrfodeid);

    List<EMWO> selectByRfomoid(@Param("emrfomoid") Serializable emrfomoid);

    List<EMWO> selectByRserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMWO> selectByWooriid(@Param("emwooriid") Serializable emwooriid);

    List<EMWO> selectByWopid(@Param("emwoid") Serializable emwoid);

    List<EMWO> selectByRdeptid(@Param("pfdeptid") Serializable pfdeptid);

    List<EMWO> selectByMpersonid(@Param("pfempid") Serializable pfempid);

    List<EMWO> selectByRecvpersonid(@Param("pfempid") Serializable pfempid);

    List<EMWO> selectByRempid(@Param("pfempid") Serializable pfempid);

    List<EMWO> selectByWpersonid(@Param("pfempid") Serializable pfempid);

    List<EMWO> selectByRteamid(@Param("pfteamid") Serializable pfteamid);

}
