package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFOrg;
import cn.ibizlab.eam.core.eam_pf.filter.PFOrgSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFOrg] 服务对象接口
 */
public interface IPFOrgService extends IService<PFOrg> {

    boolean create(PFOrg et);
    void createBatch(List<PFOrg> list);
    boolean update(PFOrg et);
    void updateBatch(List<PFOrg> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PFOrg get(String key);
    PFOrg getDraft(PFOrg et);
    boolean checkKey(PFOrg et);
    boolean save(PFOrg et);
    void saveBatch(List<PFOrg> list);
    Page<PFOrg> searchDefault(PFOrgSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFOrg> getPforgByIds(List<String> ids);
    List<PFOrg> getPforgByEntities(List<PFOrg> entities);
}


