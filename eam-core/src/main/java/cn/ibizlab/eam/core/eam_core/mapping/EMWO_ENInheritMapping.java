

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_EN;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMWO_ENInheritMapping {

    @Mappings({
        @Mapping(source ="emwoEnid",target = "emresrefobjid"),
        @Mapping(source ="emwoEnname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="wopid",target = "resrefobjpid"),
    })
    EMResRefObj toEmresrefobj(EMWO_EN minorEntity);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emwoEnid"),
        @Mapping(source ="emresrefobjname" ,target = "emwoEnname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="resrefobjpid",target = "wopid"),
    })
    EMWO_EN toEmwoEn(EMResRefObj majorEntity);

    List<EMResRefObj> toEmresrefobj(List<EMWO_EN> minorEntities);

    List<EMWO_EN> toEmwoEn(List<EMResRefObj> majorEntities);

}


