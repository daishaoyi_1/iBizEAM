package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备关键点关系]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQKPMAP_BASE", resultMap = "EMEQKPMapResultMap")
public class EMEQKPMap extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备关键点关系
     */
    @DEField(defaultValue = "VAR_KPID||-||VAR_REFOBJID")
    @TableField(value = "emeqkpmapname")
    @JSONField(name = "emeqkpmapname")
    @JsonProperty("emeqkpmapname")
    private String emeqkpmapname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 设备关键点关系标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqkpmapid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqkpmapid")
    @JsonProperty("emeqkpmapid")
    private String emeqkpmapid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 设备关键点归属
     */
    @TableField(exist = false)
    @JSONField(name = "refobjname")
    @JsonProperty("refobjname")
    private String refobjname;
    /**
     * 设备关键点
     */
    @TableField(exist = false)
    @JSONField(name = "kpname")
    @JsonProperty("kpname")
    private String kpname;
    /**
     * 设备关键点
     */
    @TableField(value = "kpid")
    @JSONField(name = "kpid")
    @JsonProperty("kpid")
    private String kpid;
    /**
     * 设备关键点归属
     */
    @TableField(value = "refobjid")
    @JSONField(name = "refobjid")
    @JsonProperty("refobjid")
    private String refobjid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQKP kp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject refobj;



    /**
     * 设置 [设备关键点关系]
     */
    public void setEmeqkpmapname(String emeqkpmapname) {
        this.emeqkpmapname = emeqkpmapname;
        this.modify("emeqkpmapname", emeqkpmapname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [设备关键点]
     */
    public void setKpid(String kpid) {
        this.kpid = kpid;
        this.modify("kpid", kpid);
    }

    /**
     * 设置 [设备关键点归属]
     */
    public void setRefobjid(String refobjid) {
        this.refobjid = refobjid;
        this.modify("refobjid", refobjid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqkpmapid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


