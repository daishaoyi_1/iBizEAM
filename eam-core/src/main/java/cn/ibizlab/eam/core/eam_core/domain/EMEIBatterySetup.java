package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[电瓶换装记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEIBATTERYSETUP_BASE", resultMap = "EMEIBatterySetupResultMap")
public class EMEIBatterySetup extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 换装人
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    private String empname;
    /**
     * 换装信息
     */
    @TableField(exist = false)
    @JSONField(name = "activeinfo")
    @JsonProperty("activeinfo")
    private String activeinfo;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 换装记录
     */
    @TableField(value = "activedesc")
    @JSONField(name = "activedesc")
    @JsonProperty("activedesc")
    private String activedesc;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 换装人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    private String empid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 换装时间
     */
    @TableField(value = "activedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "activedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activedate")
    private Timestamp activedate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 电瓶换装记录名称
     */
    @DEField(defaultValue = "name")
    @TableField(value = "emeibatterysetupname")
    @JSONField(name = "emeibatterysetupname")
    @JsonProperty("emeibatterysetupname")
    private String emeibatterysetupname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 电瓶换装记录标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeibatterysetupid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeibatterysetupid")
    @JsonProperty("emeibatterysetupid")
    private String emeibatterysetupid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 换下电瓶
     */
    @TableField(exist = false)
    @JSONField(name = "eiobjname")
    @JsonProperty("eiobjname")
    private String eiobjname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    private String eqlocationname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    private String equipname;
    /**
     * 换上电瓶
     */
    @TableField(exist = false)
    @JSONField(name = "eiobjnname")
    @JsonProperty("eiobjnname")
    private String eiobjnname;
    /**
     * 设备
     */
    @DEField(defaultValue = ";select EQUIPID into VAREX_EQUIPID from srft_emeibattery_base where emeibatteryid=var_eiobjid")
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    private String equipid;
    /**
     * 换上电瓶
     */
    @TableField(value = "eiobjnid")
    @JSONField(name = "eiobjnid")
    @JsonProperty("eiobjnid")
    private String eiobjnid;
    /**
     * 换下电瓶
     */
    @TableField(value = "eiobjid")
    @JSONField(name = "eiobjid")
    @JsonProperty("eiobjid")
    private String eiobjid;
    /**
     * 位置
     */
    @DEField(defaultValue = ";select EQLOCATIONID into VAREX_EQLOCATIONID from srfv_emeibattery where emeibatteryid=var_eiobjid")
    @TableField(value = "eqlocationid")
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    private String eqlocationid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEIBattery eiobj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEIBattery eiobjn;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;



    /**
     * 设置 [换装人]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [换装记录]
     */
    public void setActivedesc(String activedesc) {
        this.activedesc = activedesc;
        this.modify("activedesc", activedesc);
    }

    /**
     * 设置 [换装人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [换装时间]
     */
    public void setActivedate(Timestamp activedate) {
        this.activedate = activedate;
        this.modify("activedate", activedate);
    }

    /**
     * 格式化日期 [换装时间]
     */
    public String formatActivedate() {
        if (this.activedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activedate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [电瓶换装记录名称]
     */
    public void setEmeibatterysetupname(String emeibatterysetupname) {
        this.emeibatterysetupname = emeibatterysetupname;
        this.modify("emeibatterysetupname", emeibatterysetupname);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [换上电瓶]
     */
    public void setEiobjnid(String eiobjnid) {
        this.eiobjnid = eiobjnid;
        this.modify("eiobjnid", eiobjnid);
    }

    /**
     * 设置 [换下电瓶]
     */
    public void setEiobjid(String eiobjid) {
        this.eiobjid = eiobjid;
        this.modify("eiobjid", eiobjid);
    }

    /**
     * 设置 [位置]
     */
    public void setEqlocationid(String eqlocationid) {
        this.eqlocationid = eqlocationid;
        this.modify("eqlocationid", eqlocationid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeibatterysetupid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


