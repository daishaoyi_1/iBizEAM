package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMStorePart;
import cn.ibizlab.eam.core.eam_core.filter.EMStorePartSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMStorePartService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMStorePartMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[仓库库位] 服务对象接口实现
 */
@Slf4j
@Service("EMStorePartServiceImpl")
public class EMStorePartServiceImpl extends ServiceImpl<EMStorePartMapper, EMStorePart> implements IEMStorePartService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMCabService emcabService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemCSService emitemcsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPLService emitemplService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService emitemprtnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemRInService emitemrinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemROutService emitemroutService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStockService emstockService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStoreService emstoreService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMStorePart et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmstorepartid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMStorePart> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMStorePart et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emstorepartid", et.getEmstorepartid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmstorepartid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMStorePart> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMStorePart get(String key) {
        EMStorePart et = getById(key);
        if(et == null){
            et = new EMStorePart();
            et.setEmstorepartid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMStorePart getDraft(EMStorePart et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMStorePart et) {
        return (!ObjectUtils.isEmpty(et.getEmstorepartid())) && (!Objects.isNull(this.getById(et.getEmstorepartid())));
    }
    @Override
    @Transactional
    public boolean save(EMStorePart et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMStorePart et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMStorePart> list) {
        list.forEach(item->fillParentData(item));
        List<EMStorePart> create = new ArrayList<>();
        List<EMStorePart> update = new ArrayList<>();
        for (EMStorePart et : list) {
            if (ObjectUtils.isEmpty(et.getEmstorepartid()) || ObjectUtils.isEmpty(getById(et.getEmstorepartid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMStorePart> list) {
        list.forEach(item->fillParentData(item));
        List<EMStorePart> create = new ArrayList<>();
        List<EMStorePart> update = new ArrayList<>();
        for (EMStorePart et : list) {
            if (ObjectUtils.isEmpty(et.getEmstorepartid()) || ObjectUtils.isEmpty(getById(et.getEmstorepartid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMStorePart> selectByStoreid(String emstoreid) {
        return baseMapper.selectByStoreid(emstoreid);
    }
    @Override
    public void removeByStoreid(String emstoreid) {
        this.remove(new QueryWrapper<EMStorePart>().eq("storeid",emstoreid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMStorePart> searchDefault(EMStorePartSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMStorePart> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMStorePart>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMStorePart et){
        //实体关系[DER1N_EMSTOREPART_EMSTORE_STOREID]
        if(!ObjectUtils.isEmpty(et.getStoreid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStore store=et.getStore();
            if(ObjectUtils.isEmpty(store)){
                cn.ibizlab.eam.core.eam_core.domain.EMStore majorEntity=emstoreService.get(et.getStoreid());
                et.setStore(majorEntity);
                store=majorEntity;
            }
            et.setStorename(store.getEmstorename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMStorePart> getEmstorepartByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMStorePart> getEmstorepartByEntities(List<EMStorePart> entities) {
        List ids =new ArrayList();
        for(EMStorePart entity : entities){
            Serializable id=entity.getEmstorepartid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMStorePartService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



