package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[服务商]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMSERVICE_BASE", resultMap = "EMServiceResultMap")
public class EMService extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    private Timestamp createdate;
    /**
     * 帐号
     */
    @TableField(value = "accode")
    @JSONField(name = "accode")
    @JsonProperty("accode")
    private String accode;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    private String orgid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    private Integer enable;
    /**
     * 所在地区
     */
    @TableField(value = "lsareaid")
    @JSONField(name = "lsareaid")
    @JsonProperty("lsareaid")
    private String lsareaid;
    /**
     * 传真
     */
    @TableField(value = "fax")
    @JSONField(name = "fax")
    @JsonProperty("fax")
    private String fax;
    /**
     * 主营业务
     */
    @TableField(value = "zzyw")
    @JSONField(name = "zzyw")
    @JsonProperty("zzyw")
    private String zzyw;
    /**
     * 付款方式
     */
    @TableField(value = "payway")
    @JSONField(name = "payway")
    @JsonProperty("payway")
    private String payway;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    private String wfstep;
    /**
     * 服务商信息
     */
    @TableField(exist = false)
    @JSONField(name = "serviceinfo")
    @JsonProperty("serviceinfo")
    private String serviceinfo;
    /**
     * 上季度评估得分
     */
    @TableField(value = "pgrade")
    @JSONField(name = "pgrade")
    @JsonProperty("pgrade")
    private Double pgrade;
    /**
     * 帐号备注
     */
    @TableField(value = "accodedesc")
    @JSONField(name = "accodedesc")
    @JsonProperty("accodedesc")
    private String accodedesc;
    /**
     * 级别
     */
    @DEField(defaultValue = "3")
    @TableField(value = "labservicelevelid")
    @JSONField(name = "labservicelevelid")
    @JsonProperty("labservicelevelid")
    private String labservicelevelid;
    /**
     * 评估得分
     */
    @TableField(exist = false)
    @JSONField(name = "sums")
    @JsonProperty("sums")
    private Integer sums;
    /**
     * 服务商类型
     */
    @TableField(value = "labservicetypeid")
    @JSONField(name = "labservicetypeid")
    @JsonProperty("labservicetypeid")
    private String labservicetypeid;
    /**
     * 联系电话
     */
    @TableField(value = "tel")
    @JSONField(name = "tel")
    @JsonProperty("tel")
    private String tel;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    private Integer wfstate;
    /**
     * 联系人
     */
    @TableField(value = "prman")
    @JSONField(name = "prman")
    @JsonProperty("prman")
    private String prman;
    /**
     * 合同内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    private String content;
    /**
     * 服务商标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emserviceid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    private String emserviceid;
    /**
     * 质量管理体系附件
     */
    @TableField(value = "qualitymana")
    @JSONField(name = "qualitymana")
    @JsonProperty("qualitymana")
    private String qualitymana;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    private String createman;
    /**
     * 税码
     */
    @TableField(value = "taxcode")
    @JSONField(name = "taxcode")
    @JsonProperty("taxcode")
    private String taxcode;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    private String wfinstanceid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    private String updateman;
    /**
     * 邮编
     */
    @TableField(value = "zip")
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;
    /**
     * 服务商归属
     */
    @TableField(value = "range")
    @JSONField(name = "range")
    @JsonProperty("range")
    private Integer range;
    /**
     * 有效值备注
     */
    @TableField(value = "enablebz")
    @JSONField(name = "enablebz")
    @JsonProperty("enablebz")
    private String enablebz;
    /**
     * 服务商名称
     */
    @TableField(value = "emservicename")
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    private String emservicename;
    /**
     * 服务商代码
     */
    @TableField(value = "servicecode")
    @JSONField(name = "servicecode")
    @JsonProperty("servicecode")
    private String servicecode;
    /**
     * 付款方式备注
     */
    @TableField(value = "paywaydesc")
    @JSONField(name = "paywaydesc")
    @JsonProperty("paywaydesc")
    private String paywaydesc;
    /**
     * 服务商分组
     */
    @TableField(value = "servicegroup")
    @JSONField(name = "servicegroup")
    @JsonProperty("servicegroup")
    private Integer servicegroup;
    /**
     * 附件
     */
    @TableField(value = "att")
    @JSONField(name = "att")
    @JsonProperty("att")
    private String att;
    /**
     * 网址
     */
    @TableField(value = "website")
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    private Timestamp updatedate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 图标
     */
    @TableField(value = "logo")
    @JSONField(name = "logo")
    @JsonProperty("logo")
    private String logo;
    /**
     * 联系地址
     */
    @TableField(value = "addr")
    @JSONField(name = "addr")
    @JsonProperty("addr")
    private String addr;
    /**
     * 税类型
     */
    @TableField(value = "taxtypeid")
    @JSONField(name = "taxtypeid")
    @JsonProperty("taxtypeid")
    private String taxtypeid;
    /**
     * 税备注
     */
    @TableField(value = "taxdesc")
    @JSONField(name = "taxdesc")
    @JsonProperty("taxdesc")
    private String taxdesc;
    /**
     * 审核时间
     */
    @TableField(value = "shdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "shdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("shdate")
    private Timestamp shdate;
    /**
     * 资质附件
     */
    @TableField(value = "qualifications")
    @JSONField(name = "qualifications")
    @JsonProperty("qualifications")
    private String qualifications;
    /**
     * 服务商状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "servicestate")
    @JSONField(name = "servicestate")
    @JsonProperty("servicestate")
    private String servicestate;



    /**
     * 设置 [帐号]
     */
    public void setAccode(String accode) {
        this.accode = accode;
        this.modify("accode", accode);
    }

    /**
     * 设置 [所在地区]
     */
    public void setLsareaid(String lsareaid) {
        this.lsareaid = lsareaid;
        this.modify("lsareaid", lsareaid);
    }

    /**
     * 设置 [传真]
     */
    public void setFax(String fax) {
        this.fax = fax;
        this.modify("fax", fax);
    }

    /**
     * 设置 [主营业务]
     */
    public void setZzyw(String zzyw) {
        this.zzyw = zzyw;
        this.modify("zzyw", zzyw);
    }

    /**
     * 设置 [付款方式]
     */
    public void setPayway(String payway) {
        this.payway = payway;
        this.modify("payway", payway);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [上季度评估得分]
     */
    public void setPgrade(Double pgrade) {
        this.pgrade = pgrade;
        this.modify("pgrade", pgrade);
    }

    /**
     * 设置 [帐号备注]
     */
    public void setAccodedesc(String accodedesc) {
        this.accodedesc = accodedesc;
        this.modify("accodedesc", accodedesc);
    }

    /**
     * 设置 [级别]
     */
    public void setLabservicelevelid(String labservicelevelid) {
        this.labservicelevelid = labservicelevelid;
        this.modify("labservicelevelid", labservicelevelid);
    }

    /**
     * 设置 [服务商类型]
     */
    public void setLabservicetypeid(String labservicetypeid) {
        this.labservicetypeid = labservicetypeid;
        this.modify("labservicetypeid", labservicetypeid);
    }

    /**
     * 设置 [联系电话]
     */
    public void setTel(String tel) {
        this.tel = tel;
        this.modify("tel", tel);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [联系人]
     */
    public void setPrman(String prman) {
        this.prman = prman;
        this.modify("prman", prman);
    }

    /**
     * 设置 [合同内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [质量管理体系附件]
     */
    public void setQualitymana(String qualitymana) {
        this.qualitymana = qualitymana;
        this.modify("qualitymana", qualitymana);
    }

    /**
     * 设置 [税码]
     */
    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
        this.modify("taxcode", taxcode);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [邮编]
     */
    public void setZip(String zip) {
        this.zip = zip;
        this.modify("zip", zip);
    }

    /**
     * 设置 [服务商归属]
     */
    public void setRange(Integer range) {
        this.range = range;
        this.modify("range", range);
    }

    /**
     * 设置 [有效值备注]
     */
    public void setEnablebz(String enablebz) {
        this.enablebz = enablebz;
        this.modify("enablebz", enablebz);
    }

    /**
     * 设置 [服务商名称]
     */
    public void setEmservicename(String emservicename) {
        this.emservicename = emservicename;
        this.modify("emservicename", emservicename);
    }

    /**
     * 设置 [服务商代码]
     */
    public void setServicecode(String servicecode) {
        this.servicecode = servicecode;
        this.modify("servicecode", servicecode);
    }

    /**
     * 设置 [付款方式备注]
     */
    public void setPaywaydesc(String paywaydesc) {
        this.paywaydesc = paywaydesc;
        this.modify("paywaydesc", paywaydesc);
    }

    /**
     * 设置 [服务商分组]
     */
    public void setServicegroup(Integer servicegroup) {
        this.servicegroup = servicegroup;
        this.modify("servicegroup", servicegroup);
    }

    /**
     * 设置 [附件]
     */
    public void setAtt(String att) {
        this.att = att;
        this.modify("att", att);
    }

    /**
     * 设置 [网址]
     */
    public void setWebsite(String website) {
        this.website = website;
        this.modify("website", website);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [图标]
     */
    public void setLogo(String logo) {
        this.logo = logo;
        this.modify("logo", logo);
    }

    /**
     * 设置 [联系地址]
     */
    public void setAddr(String addr) {
        this.addr = addr;
        this.modify("addr", addr);
    }

    /**
     * 设置 [税类型]
     */
    public void setTaxtypeid(String taxtypeid) {
        this.taxtypeid = taxtypeid;
        this.modify("taxtypeid", taxtypeid);
    }

    /**
     * 设置 [税备注]
     */
    public void setTaxdesc(String taxdesc) {
        this.taxdesc = taxdesc;
        this.modify("taxdesc", taxdesc);
    }

    /**
     * 设置 [审核时间]
     */
    public void setShdate(Timestamp shdate) {
        this.shdate = shdate;
        this.modify("shdate", shdate);
    }

    /**
     * 格式化日期 [审核时间]
     */
    public String formatShdate() {
        if (this.shdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(shdate);
    }
    /**
     * 设置 [资质附件]
     */
    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
        this.modify("qualifications", qualifications);
    }

    /**
     * 设置 [服务商状态]
     */
    public void setServicestate(String servicestate) {
        this.servicestate = servicestate;
        this.modify("servicestate", servicestate);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emserviceid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


